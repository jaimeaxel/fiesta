# generic options for triolith
#

ENABLE_MPI   = 1
MKL_PATH     = /software/apps/intel/composer_xe_2015.1.133/mkl
IMPI_PATH    = /software/apps/intel/impi/5.0.2.044
LIBDIR      += -L$(IMPI_PATH)/lib64
# MPICXX       = $(IMPI_PATH)/bin64/mpic++

