# generic settings for beskow
MKL_PATH    = /pdc/vol/intel/18.0.0.128/compilers_and_libraries_2018.0.128/linux/mkl
DDT_PATH    = /pdc/vol/allinea-forge/7.0
MPICXX      = CC
PYTHON      = python3
BOOST_PATH  = /pdc/vol/boost/1.66.0-gcc7-py36
CXXFLAGS   += -march=haswell -std=c++11  
