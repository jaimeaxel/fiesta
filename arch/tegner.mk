# generic settings for tegner
#

CUDA_ARCH   = 30
CUDA_VER    = 9.1
CUDA_PATH   = /pdc/vol/cuda/cuda-$(CUDA_VER)
MKL_PATH    = /pdc/vol/i-compilers/17.0.1/mkl
INTEL_PATH  = /pdc/vol/i-compilers/17.0.1/compilers_and_libraries/linux
DDT_PATH    = /afs/pdc.kth.se/pdc/vol/allinea-forge/6.1/amd64_co7
IMPI_PATH   = /pdc/vol/i-compilers/17.0.1/impi/2017.1.132
LIBDIR     += -L$(IMPI_PATH)/lib64
CXXFLAGS   += -march=ivybridge
MPICXX      = mpigxx
