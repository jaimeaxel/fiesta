# generic settings for castor
#
CUDA_ARCH   = 21
CUDA_VER    = 6.0
ATLAS_PATH  = /usr
MKL_PATH    = /opt/intel/mkl
INTEL_PATH  = /opt/intel/bin
MPICXX      = mpicxx
