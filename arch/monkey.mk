# generic settings for monkey
#MKL_PATH    = /opt/intel/compilers_and_libraries_2018.1.163/linux/mkl
PYTHON      = python3
BOOST_PATH  = /home/xinli/software/boost
CUDA_PATH   = /usr/local/cuda
CUDA_ARCH   = 30
MPICXX      = mpicxx
CXXFLAGS   += -std=c++11
