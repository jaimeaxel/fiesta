#!/bin/bash
#SBATCH --time=6:00:00
#SBATCH --nodes=2
#SBATCH --account=pdc.staff
#SBATCH --ntasks-per-node=1

### SBATCH --gres=gpu:K80:2
### --mem=2000000

# SLURM batch for Tegner

# load the necessary modules (for DL libraries)
module add gcc/7.2.0
module add i-compilers/17.0.1
module add intelmpi/5.1.3
module add allinea-forge/6.1

export ALLINEA_LICENSE_FILE=/afs/pdc.kth.se/pdc/vol/allinea-forge/licences/License
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib

# if compiled with DDT support
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/pdc.kth.se/pdc/vol/allinea-forge/6.1/amd64_co7/lib/64

# executable
FIESTA=./fiesta
 
# Specify input files
#export FIESTA_ECP=./au249.ano/ECp_new.bin
#export FIESTA_MCP=./au249.ano/MCp_new.bin

#INPUT=./au249.ano/au249.xyz
#INPUT=./au249.ano/anotest.xyz
#INPUT=./au249.lda/au249.lda.xyz

#export FIESTA_ECP=./au249.ano/new/ECp_new.bin
#export FIESTA_MCP=./au249.ano/new/MCp_new.bin
#export INPUT=./au249.ano/au249.xyz

export INPUT=./patrick/geom.xyz
export PARMS=./patrick/calc2.fst

#export FIESTA_ECP=./au249.new/ECp_new.bin
#export FIESTA_MCP=./au249.new/MCp_new.bin

#export INPUT=./inputs/Au_20.xyz

# set number of threads per node & launcher
export OMP_MAX_THREADS=48
export OMP_NUM_THREADS=48
export MKL_NUM_THREADS=48
export MPI_NUM_PROCESS=2
export MPI_LAUNCHER=mpirun
#mpiexec.hydra
 
#map --profile
which mpirun
$MPI_LAUNCHER -np $MPI_NUM_PROCESS $FIESTA $INPUT $PARMS arch/tegner.hw

