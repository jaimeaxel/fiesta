#!/bin/bash

#SBATCH --time=3:00:00
#SBATCH --nodes=4
#SBATCH --account=pdc.staff
#SBATCH --ntasks-per-node=1

# SLURM batch for Beskow

# load the necessary modules (for DL libraries)
module unload PrgEnv-cray
module unload cray-libsci
module load PrgEnv-gnu/5.2.82
module swap gcc gcc/7.2.0
module load craype-hugepages8M

# executable
FIESTA=./fiesta
 
# Specify input files
ARCH=arch/beskow.hw
INPUT=au16.xyz

# set number of threads per node & launcher
export OMP_MAX_THREADS=64
export OMP_NUM_THREADS=64
export MKL_NUM_THREADS=32
export MPI_NUM_PROCESS=4
export MPI_LAUNCHER=aprun

#map --profile
$MPI_LAUNCHER -n $MPI_NUM_PROCESS -N 1 -d 32 -j 2 $FIESTA $ARCH $INPUT

