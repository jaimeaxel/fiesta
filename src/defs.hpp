
#ifndef __DEFS__ //se asegura de que no se dupliquen declaraciones
#define __DEFS__

#include <inttypes.h>  //integer types
#include <stdlib.h>    //for size_t

// program limits
// **************
const uint8_t  LMAX                =    5;  //up to h functions are permitted at the moment (still only up to g qic functions are present)
const uint8_t  LMAXG               =    2;  //maximum angular momentum treateable with GPU (note that the device's constant memory limit permits up to LMAXG=3)
const uint8_t  maxK                =   24;  //maximum contraction degree allowed (for now)
const uint8_t  maxJ                =    6;  //maximum number of contraction functions allowed


// limits for computation batches
// ******************************
const uint64_t MAX_TILES_PER_BLOCK =    32; // self-explanatory (allows 10^6 elements per list)
const uint64_t NTX                 =    32; // threads per warp, which is also the "vector" length used for GPU algorithms
const uint64_t NTY                 =     1; // not used at the moment; most kernels use specifically optimized number of warps per block (2 to 8)

// if defined, perform QR factorization of general contraction functions
//#define __QR_GC__

// machine-specific constants
// **************************
#define CACHE_LINE_SIZE         64
#define AVXD_PER_CACHE_LINE      2
#define MM128_PER_CACHE_LINE     4
#define DOUBLES_PER_CACHE_LINE   8
#define FLOATS_PER_CACHE_LINE   16

//#define DOUBLES_PER_BLOCK   8
#define DOUBLES_PER_BLOCK   (256/8)

const int MAX_GPUS = 4; // can't think of many more GPUs plugged to the same motherboard at the moment


// physical and mathematical constants
// ***********************************

const uint64_t Mword = 1024*1024;
const uint64_t Gword = 1024*Mword;

const double PI   = 3.1415926535897932384626433832795; //M_PI is not compiler-independant

const double AMS  = 1 / 0.529177249240;  // one angstrom in a.u. (bohr)


// 4-tuple geometries
// ******************
enum GEOM {ABCD, AACD, ABAD, AACC, ABAB, AAAD, AAAA,
           PCD, PCC, PPD, PPP,
           PQ, PP,
           NONE};


// constants derived from previous definitions
// *******************************************
const uint8_t    MMAX = (LMAX+1)*(LMAX+2)/2; //(h+1)*;
const uint8_t    LSP  = LMAX + 1; //code for SP GTOs

//in order to declare array sizes
const uint8_t    LM1  =  1*LMAX+1;
const uint8_t    LM2  =  2*LMAX+1;
const uint8_t    LM4  =  4*LMAX+1;
const uint8_t    LM6  =  6*LMAX+1; //needed
const uint8_t    LM8  =  8*LMAX+1; //needed
const uint8_t    LM12 = 12*LMAX+1; //needed


const uint16_t maxK2 = maxK*maxK;
const uint32_t maxK4 = maxK2*maxK2;
const uint16_t maxJ2 = maxJ*maxJ;
const uint32_t maxJ4 = maxJ2*maxJ2;

#endif
