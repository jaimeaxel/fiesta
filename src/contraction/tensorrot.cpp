#include "contractions.hpp"
#include "EFS/rotations.hpp"

static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline int Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}


// DOUBLE PACKED CONTRACTIONS
template <int maxL1, int maxL2> inline void Mprod (cacheline64 * __restrict__ MR, const RotationMatrices64 * __restrict__ RM) {
    const RotationMatrices64 & RM8 = *RM;
    cacheline64 MM[maxL1*maxL2];

    //SP
    if      (maxL1==4) {
        //S
        for (int i=0; i<1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
        //P
        for (int i=0; i<3; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k+1,j);
                    sum.fma(RM8.PP[ki], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i+1,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.PP[ki], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 5*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.DD[ki], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 7*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.FF[ki], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 9*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.GG[ki], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    /*
    else if (maxL1==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 11*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.HH[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
    }

    if      (maxL2==4) {
        //S
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<1; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
        //P
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<3; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k+1);
                    sum.fma(RM8.PP[kj], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j+1);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.PP[kj], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 5*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.DD[kj], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 7*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.FF[kj], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 9*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.GG[kj], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    /*
    else if (maxL2==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 11*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.HH[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
    }
}

template <int maxL1, int maxL2> inline void MprodT(cacheline64 * __restrict__ MR, const RotationMatrices64 * __restrict__ RM) {
    const RotationMatrices64 & RM8 = *RM;
    cacheline64 MM[maxL1*maxL2];

    if      (maxL1==4) {
        //S
        for (int i=0; i<1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
        //P
        for (int i=0; i<3; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k+1,j);
                    sum.fma(RM8.PP[ik], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i+1,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.PP[ik], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 5*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.DD[ik], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 7*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum.fma(RM8.FF[ik], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 9*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.GG[ik] * MR[kj];
                    sum.fma(RM8.GG[ik], MR[kj]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    /*
    else if (maxL1==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 11*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.HH[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
    }

    if      (maxL2==4) {
        //S
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<1; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
        //P
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<3; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k+1);
                    sum.fma(RM8.PP[jk], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j+1);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.PP[jk], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 5*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.DD[jk], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 7*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.FF[jk], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 9*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum.fma(RM8.GG[jk], MM[ik]);
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    /*
    else if (maxL2==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                register cacheline64 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 11*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.HH[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
    }
}


// FLOAT PACKED CONTRACTIONS
template <int maxL1, int maxL2> inline void Mprod (cacheline32 * __restrict__ MR, const RotationMatrices32 * __restrict__ RM) {
    const RotationMatrices32 & RM8 = *RM;
    cacheline32 MM[maxL1*maxL2];

    //SP
    if      (maxL1==4) {
        //S
        for (int i=0; i<1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
        //P
        for (int i=0; i<3; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k+1,j);
                    sum  += RM8.PP[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i+1,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.PP[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 5*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.DD[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 7*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.FF[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 9*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.GG[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    /*
    else if (maxL1==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 11*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.HH[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
    }

    if      (maxL2==4) {
        //S
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<1; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
        //P
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<3; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k+1);
                    sum  += RM8.PP[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j+1);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.PP[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 5*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.DD[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 7*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.FF[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 9*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.GG[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    /*
    else if (maxL2==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 11*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.HH[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
    }
}

template <int maxL1, int maxL2> inline void MprodT(cacheline32 * __restrict__ MR, const RotationMatrices32 * __restrict__ RM) {
    const RotationMatrices32 & RM8 = *RM;
    cacheline32 MM[maxL1*maxL2];

    if      (maxL1==4) {
        //S
        for (int i=0; i<1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
        //P
        for (int i=0; i<3; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k+1,j);
                    sum  += RM8.PP[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i+1,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.PP[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 5*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.DD[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 7*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.FF[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 9*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.GG[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    /*
    else if (maxL1==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 11*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.HH[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
    }

    if      (maxL2==4) {
        //S
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<1; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
        //P
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<3; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k+1);
                    sum  += RM8.PP[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j+1);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.PP[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 5*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.DD[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 7*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.FF[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 9*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.GG[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    /*
    else if (maxL2==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cacheline32 sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 11*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.HH[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
    }
}


// NON-SPECIFIC CACHELINE PACKED CONTRACTIONS
template <int maxL1, int maxL2> inline void Mprod (cachelineNB * __restrict__ MR, const RotationMatricesN * __restrict__ RM) {
    const RotationMatricesN & RM8 = *RM;
    cachelineNB MM[maxL1*maxL2];

    //SP
    if      (maxL1==4) {
        //S
        for (int i=0; i<1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
        //P
        for (int i=0; i<3; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k+1,j);
                    sum  += RM8.PP[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i+1,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.PP[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 5*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.DD[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 7*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.FF[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 9*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.GG[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    /*
    else if (maxL1==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 11*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.HH[ki] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
    }

    if      (maxL2==4) {
        //S
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<1; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
        //P
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<3; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k+1);
                    sum  += RM8.PP[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j+1);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.PP[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 5*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.DD[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 7*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.FF[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 9*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.GG[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    /*
    else if (maxL2==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 11*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.HH[kj] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
    }
}

template <int maxL1, int maxL2> inline void MprodT(cachelineNB * __restrict__ MR, const RotationMatricesN * __restrict__ RM) {
    const RotationMatricesN & RM8 = *RM;
    cachelineNB MM[maxL1*maxL2];

    if      (maxL1==4) {
        //S
        for (int i=0; i<1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
        //P
        for (int i=0; i<3; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k+1,j);
                    sum  += RM8.PP[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i+1,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.PP[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 5*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.DD[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 7*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.FF[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 9*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.GG[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    /*
    else if (maxL1==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 11*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM8.HH[ik] * MR[kj];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij];
            }
        }
    }

    if      (maxL2==4) {
        //S
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<1; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
        //P
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<3; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<3; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k+1);
                    sum  += RM8.PP[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j+1);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==3) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.PP[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==5) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 5*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.DD[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==7) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 7*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.FF[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    else if (maxL2==9) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 9*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.GG[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    /*
    else if (maxL2==11) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                cachelineNB sum; sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 11*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM8.HH[jk] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = sum;
            }
        }
    }
    */
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij] = MM[ij];
            }
        }
    }
}


// rotate index of tensor
template <int L> inline void Mrot (cacheline64 * __restrict__ MR, const RotationMatrices64 * __restrict__ RM, int pdim,  int qdim) {

    if (L==1) return;

    const RotationMatrices64 & RM8 = *RM;

    // loop over external indices
    for (int p=0; p<pdim; ++p) {
        //loop over collapsed internal indices
        for (int q=0; q<qdim; ++q) {

            int i0 = p*(L*qdim) + q;

            cacheline64 MM[L];
            cacheline64 RR[L];

            // copy
            for (int i=0; i<L; ++i) {
                MM[i] = MR[i0 + i*qdim];
            }

            for (int i1=0; i1<L; ++i1) {
                register cacheline64 sum; sum = 0;

                for (int i2=0; i2<L; ++i2) {
                    int i12 = i1*L + i2;
                    //int i12 = i2*L + i1;

                    if      (L==1)
                        sum   =               MM[i2];
                    else if (L==3)
                        sum  += RM8.PP[i12] * MM[i2];
                    else if (L==5)
                        sum  += RM8.DD[i12] * MM[i2];
                    else if (L==7)
                        sum  += RM8.FF[i12] * MM[i2];
                    else if (L==9)
                        sum  += RM8.GG[i12] * MM[i2];
                }
                RR[i1] = sum;
            }

            // copy result
            for (int i=0; i<L; ++i) {
                MR[i0 + i*qdim] = RR[i];
            }

        }
    }

}

inline void Mrot (cacheline64 * __restrict__ MR, const RotationMatrices64 * __restrict__ RM, int pdim,  int ldim, int qdim) {

    if      (ldim==1)    Mrot<1> (MR, RM, pdim, qdim);
    else if (ldim==3)    Mrot<3> (MR, RM, pdim, qdim);
    else if (ldim==5)    Mrot<5> (MR, RM, pdim, qdim);
    else if (ldim==7)    Mrot<7> (MR, RM, pdim, qdim);
    else if (ldim==9)    Mrot<9> (MR, RM, pdim, qdim);
    else    {}
}


void Mrot4 (cacheline64 * __restrict__ MR, const RotationMatrices64 * __restrict__ RM, int l1, int l2, int l3, int l4) {

    int L1 = 2*l1+1;
    int L2 = 2*l2+1;
    int L3 = 2*l3+1;
    int L4 = 2*l4+1;

    // rotate index i
    Mrot (MR, RM, L4*L3*L2, L1, 1);
    // rotate index j
    Mrot (MR, RM, L4*L3,    L2, L1);
    // rotate index k
    Mrot (MR, RM, L4,       L3, L2*L1);
    // rotate index l
    Mrot (MR, RM, 1,        L4, L3*L2*L1);
}


void DMD::LoadTensorRotations() {

    //rotations 64
    {
        R64 [0][0] = Mprod  <1,1>;
        R64 [0][1] = Mprod  <1,3>;
        R64 [0][2] = Mprod  <1,5>;
        R64 [0][3] = Mprod  <1,7>;
        R64 [0][4] = Mprod  <1,9>;

        R64 [1][0] = Mprod  <3,1>;
        R64 [1][1] = Mprod  <3,3>;
        R64 [1][2] = Mprod  <3,5>;
        R64 [1][3] = Mprod  <3,7>;
        R64 [1][4] = Mprod  <3,9>;

        R64 [2][0] = Mprod  <5,1>;
        R64 [2][1] = Mprod  <5,3>;
        R64 [2][2] = Mprod  <5,5>;
        R64 [2][3] = Mprod  <5,7>;
        R64 [2][4] = Mprod  <5,9>;

        R64 [3][0] = Mprod  <7,1>;
        R64 [3][1] = Mprod  <7,3>;
        R64 [3][2] = Mprod  <7,5>;
        R64 [3][3] = Mprod  <7,7>;
        R64 [3][4] = Mprod  <7,9>;

        R64 [4][0] = Mprod  <9,1>;
        R64 [4][1] = Mprod  <9,3>;
        R64 [4][2] = Mprod  <9,5>;
        R64 [4][3] = Mprod  <9,7>;
        R64 [4][4] = Mprod  <9,9>;



        RT64 [0][0] = MprodT  <1,1>;
        RT64 [0][1] = MprodT  <1,3>;
        RT64 [0][2] = MprodT  <1,5>;
        RT64 [0][3] = MprodT  <1,7>;
        RT64 [0][4] = MprodT  <1,9>;

        RT64 [1][0] = MprodT  <3,1>;
        RT64 [1][1] = MprodT  <3,3>;
        RT64 [1][2] = MprodT  <3,5>;
        RT64 [1][3] = MprodT  <3,7>;
        RT64 [1][4] = MprodT  <3,9>;

        RT64 [2][0] = MprodT  <5,1>;
        RT64 [2][1] = MprodT  <5,3>;
        RT64 [2][2] = MprodT  <5,5>;
        RT64 [2][3] = MprodT  <5,7>;
        RT64 [2][4] = MprodT  <5,9>;

        RT64 [3][0] = MprodT  <7,1>;
        RT64 [3][1] = MprodT  <7,3>;
        RT64 [3][2] = MprodT  <7,5>;
        RT64 [3][3] = MprodT  <7,7>;
        RT64 [3][4] = MprodT  <7,9>;

        RT64 [4][0] = MprodT  <9,1>;
        RT64 [4][1] = MprodT  <9,3>;
        RT64 [4][2] = MprodT  <9,5>;
        RT64 [4][3] = MprodT  <9,7>;
        RT64 [4][4] = MprodT  <9,9>;
    }

    //rotations 32
    {
        R32 [0][0] = Mprod  <1,1>;
        R32 [0][1] = Mprod  <1,3>;
        R32 [0][2] = Mprod  <1,5>;
        R32 [0][3] = Mprod  <1,7>;
        R32 [0][4] = Mprod  <1,9>;

        R32 [1][0] = Mprod  <3,1>;
        R32 [1][1] = Mprod  <3,3>;
        R32 [1][2] = Mprod  <3,5>;
        R32 [1][3] = Mprod  <3,7>;
        R32 [1][4] = Mprod  <3,9>;

        R32 [2][0] = Mprod  <5,1>;
        R32 [2][1] = Mprod  <5,3>;
        R32 [2][2] = Mprod  <5,5>;
        R32 [2][3] = Mprod  <5,7>;
        R32 [2][4] = Mprod  <5,9>;

        R32 [3][0] = Mprod  <7,1>;
        R32 [3][1] = Mprod  <7,3>;
        R32 [3][2] = Mprod  <7,5>;
        R32 [3][3] = Mprod  <7,7>;
        R32 [3][4] = Mprod  <7,9>;

        R32 [4][0] = Mprod  <9,1>;
        R32 [4][1] = Mprod  <9,3>;
        R32 [4][2] = Mprod  <9,5>;
        R32 [4][3] = Mprod  <9,7>;
        R32 [4][4] = Mprod  <9,9>;



        RT32 [0][0] = MprodT  <1,1>;
        RT32 [0][1] = MprodT  <1,3>;
        RT32 [0][2] = MprodT  <1,5>;
        RT32 [0][3] = MprodT  <1,7>;
        RT32 [0][4] = MprodT  <1,9>;

        RT32 [1][0] = MprodT  <3,1>;
        RT32 [1][1] = MprodT  <3,3>;
        RT32 [1][2] = MprodT  <3,5>;
        RT32 [1][3] = MprodT  <3,7>;
        RT32 [1][4] = MprodT  <3,9>;

        RT32 [2][0] = MprodT  <5,1>;
        RT32 [2][1] = MprodT  <5,3>;
        RT32 [2][2] = MprodT  <5,5>;
        RT32 [2][3] = MprodT  <5,7>;
        RT32 [2][4] = MprodT  <5,9>;

        RT32 [3][0] = MprodT  <7,1>;
        RT32 [3][1] = MprodT  <7,3>;
        RT32 [3][2] = MprodT  <7,5>;
        RT32 [3][3] = MprodT  <7,7>;
        RT32 [3][4] = MprodT  <7,9>;

        RT32 [4][0] = MprodT  <9,1>;
        RT32 [4][1] = MprodT  <9,3>;
        RT32 [4][2] = MprodT  <9,5>;
        RT32 [4][3] = MprodT  <9,7>;
        RT32 [4][4] = MprodT  <9,9>;
    }

    //free vector rotations
    {
        RN [0][0] = Mprod  <1,1>;
        RN [0][1] = Mprod  <1,3>;
        RN [0][2] = Mprod  <1,5>;
        RN [0][3] = Mprod  <1,7>;
        RN [0][4] = Mprod  <1,9>;

        RN [1][0] = Mprod  <3,1>;
        RN [1][1] = Mprod  <3,3>;
        RN [1][2] = Mprod  <3,5>;
        RN [1][3] = Mprod  <3,7>;
        RN [1][4] = Mprod  <3,9>;

        RN [2][0] = Mprod  <5,1>;
        RN [2][1] = Mprod  <5,3>;
        RN [2][2] = Mprod  <5,5>;
        RN [2][3] = Mprod  <5,7>;
        RN [2][4] = Mprod  <5,9>;

        RN [3][0] = Mprod  <7,1>;
        RN [3][1] = Mprod  <7,3>;
        RN [3][2] = Mprod  <7,5>;
        RN [3][3] = Mprod  <7,7>;
        RN [3][4] = Mprod  <7,9>;

        RN [4][0] = Mprod  <9,1>;
        RN [4][1] = Mprod  <9,3>;
        RN [4][2] = Mprod  <9,5>;
        RN [4][3] = Mprod  <9,7>;
        RN [4][4] = Mprod  <9,9>;



        RTN [0][0] = MprodT  <1,1>;
        RTN [0][1] = MprodT  <1,3>;
        RTN [0][2] = MprodT  <1,5>;
        RTN [0][3] = MprodT  <1,7>;
        RTN [0][4] = MprodT  <1,9>;

        RTN [1][0] = MprodT  <3,1>;
        RTN [1][1] = MprodT  <3,3>;
        RTN [1][2] = MprodT  <3,5>;
        RTN [1][3] = MprodT  <3,7>;
        RTN [1][4] = MprodT  <3,9>;

        RTN [2][0] = MprodT  <5,1>;
        RTN [2][1] = MprodT  <5,3>;
        RTN [2][2] = MprodT  <5,5>;
        RTN [2][3] = MprodT  <5,7>;
        RTN [2][4] = MprodT  <5,9>;

        RTN [3][0] = MprodT  <7,1>;
        RTN [3][1] = MprodT  <7,3>;
        RTN [3][2] = MprodT  <7,5>;
        RTN [3][3] = MprodT  <7,7>;
        RTN [3][4] = MprodT  <7,9>;

        RTN [4][0] = MprodT  <9,1>;
        RTN [4][1] = MprodT  <9,3>;
        RTN [4][2] = MprodT  <9,5>;
        RTN [4][3] = MprodT  <9,7>;
        RTN [4][4] = MprodT  <9,9>;
    }

}


