


#ifndef __CONTRACTIONS__
#define __CONTRACTIONS__

#include "defs.hpp"

class cacheline64;
class cacheline32;
template<int N> class cachelineN;

typedef cachelineN<DOUBLES_PER_BLOCK> cachelineNB;

class RotationMatrices64;
class RotationMatrices32;
class RotationMatricesN;


typedef void (*JXD64) (cacheline64 * F, const cacheline64 * T, const cacheline64 * D);
typedef void (*JXD32) (cacheline32 * F, const cacheline32 * T, const cacheline32 * D);
typedef void (*JXDN)  (cachelineNB * F, const cachelineNB * T, const cachelineNB * D);

typedef void (*JXrot64) (cacheline64 * MR, const RotationMatrices64 * __restrict__ RM);
typedef void (*JXrot32) (cacheline32 * MR, const RotationMatrices32 * __restrict__ RM);
typedef void (*JXrotN)  (cachelineNB * MR, const RotationMatricesN  * __restrict__ RM);


void Mrot4 (cacheline64 * __restrict__ MR, const RotationMatrices64 * __restrict__ RM, int L1, int L2, int L3, int L4);

// this should be a namespace or a quaject-like singleton class
struct DMD {

    JXrot64 R64  [LMAX][LMAX];
    JXrot64 RT64 [LMAX][LMAX];

    JXrot32 R32  [LMAX][LMAX];
    JXrot32 RT32 [LMAX][LMAX];

    JXrotN  RN   [LMAX][LMAX];
    JXrotN  RTN  [LMAX][LMAX];



    //coulomb
    //*******
    JXD64 J64abcd[LMAX][LMAX][LMAX][LMAX];
    JXD64 J64cdab[LMAX][LMAX][LMAX][LMAX];

    JXD64 J64abcc[LMAX][LMAX][LMAX];
    JXD64 J64ccab[LMAX][LMAX][LMAX];

    JXD64 J64aacd[LMAX][LMAX][LMAX];
    JXD64 J64cdaa[LMAX][LMAX][LMAX];

    JXD64 J64aacc[LMAX][LMAX];
    JXD64 J64ccaa[LMAX][LMAX];

    JXD64 J64abab[LMAX][LMAX]; //ABAB


    //exchange
    //********
    JXD64 X64acbd[LMAX][LMAX][LMAX][LMAX];
    JXD64 X64adbc[LMAX][LMAX][LMAX][LMAX];
    JXD64 X64bdac[LMAX][LMAX][LMAX][LMAX];
    JXD64 X64bcad[LMAX][LMAX][LMAX][LMAX];

    JXD64 X64acad[LMAX][LMAX][LMAX];
    JXD64 X64adac[LMAX][LMAX][LMAX];

    JXD64 X64acbc[LMAX][LMAX][LMAX];
    JXD64 X64bcac[LMAX][LMAX][LMAX];

    JXD64 X64acac[LMAX][LMAX];

    JXD64 X64aabb[LMAX][LMAX];
    JXD64 X64bbaa[LMAX][LMAX];
    JXD64 X64abab[LMAX][LMAX];

    JXD64 X64abba[LMAX][LMAX];
    JXD64 X64baab[LMAX][LMAX];



    //coulomb
    //*******
    JXD32 J32abcd[LMAX][LMAX][LMAX][LMAX];
    JXD32 J32cdab[LMAX][LMAX][LMAX][LMAX];

    JXD32 J32abcc[LMAX][LMAX][LMAX];
    JXD32 J32ccab[LMAX][LMAX][LMAX];

    JXD32 J32aacd[LMAX][LMAX][LMAX];
    JXD32 J32cdaa[LMAX][LMAX][LMAX];

    JXD32 J32aacc[LMAX][LMAX];
    JXD32 J32ccaa[LMAX][LMAX];

    JXD32 J32abab[LMAX][LMAX]; //ABAB


    //exchange
    //********
    JXD32 X32acbd[LMAX][LMAX][LMAX][LMAX];
    JXD32 X32adbc[LMAX][LMAX][LMAX][LMAX];
    JXD32 X32bdac[LMAX][LMAX][LMAX][LMAX];
    JXD32 X32bcad[LMAX][LMAX][LMAX][LMAX];

    JXD32 X32acad[LMAX][LMAX][LMAX];
    JXD32 X32adac[LMAX][LMAX][LMAX];

    JXD32 X32acbc[LMAX][LMAX][LMAX];
    JXD32 X32bcac[LMAX][LMAX][LMAX];

    JXD32 X32acac[LMAX][LMAX];

    JXD32 X32aabb[LMAX][LMAX];
    JXD32 X32bbaa[LMAX][LMAX];
    JXD32 X32abba[LMAX][LMAX];




    //coulomb
    //*******
    JXDN JNabcd[LMAX][LMAX][LMAX][LMAX];
    JXDN JNcdab[LMAX][LMAX][LMAX][LMAX];

    JXDN JNabcc[LMAX][LMAX][LMAX];
    JXDN JNccab[LMAX][LMAX][LMAX];

    JXDN JNaacd[LMAX][LMAX][LMAX];
    JXDN JNcdaa[LMAX][LMAX][LMAX];

    JXDN JNaacc[LMAX][LMAX];
    JXDN JNccaa[LMAX][LMAX];

    JXDN JNabab[LMAX][LMAX]; //ABAB


    //exchange
    //********
    JXDN XNacbd[LMAX][LMAX][LMAX][LMAX];
    JXDN XNadbc[LMAX][LMAX][LMAX][LMAX];
    JXDN XNbdac[LMAX][LMAX][LMAX][LMAX];
    JXDN XNbcad[LMAX][LMAX][LMAX][LMAX];

    JXDN XNacad[LMAX][LMAX][LMAX];
    JXDN XNadac[LMAX][LMAX][LMAX];

    JXDN XNacbc[LMAX][LMAX][LMAX];
    JXDN XNbcac[LMAX][LMAX][LMAX];


    JXDN XNacac[LMAX][LMAX];

    JXDN XNaabb[LMAX][LMAX];
    JXDN XNbbaa[LMAX][LMAX];
    JXDN XNabab[LMAX][LMAX];

    JXDN XNabba[LMAX][LMAX];
    JXDN XNbaab[LMAX][LMAX];



    // auxiliary initialization functions
    // help split the templates across multiple files
    void LoadTensorRotations();
    void LoadJ64();
    void LoadX64();
    void LoadJ32();
    void LoadX32();
    void LoadJN();
    void LoadXN();

    //initializer
    DMD() {
        LoadTensorRotations();

        LoadJ64();
        LoadX64();

        LoadJ32();
        LoadX32();

        LoadJN();
        LoadXN();
    }
};

#endif
