#include "contractions.hpp"
#include "low/cache64.hpp"

static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline int Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}



template <int maxLa, int maxLb, int maxLc, int maxLd> void jABCD(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLb; ++mb) {
            register cacheline64 sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLd,mc,md);
                    sum.fma(T[pos], DD[cd]);
                }
            }
            int ab = Tpos(maxLa,maxLb,ma,mb);
            FF[ab]+=sum*2;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void jCDAB(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLd; ++md) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLb; ++mb) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLb,ma,mb);
                    sum.fma(T[pos], DD[ab]);
                }
            }
            int cd = Tpos(maxLc,maxLd,mc,md);
            FF[cd]+=sum*2;
        }
    }

}




template <int maxLa, int maxLc, int maxLd> void jAACD(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLa; ++mb) {
            register cacheline64 sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLd, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLd,mc,md);
                    sum.fma(T[pos], DD[cd]);
                }
            }
            int ab = Tpos(maxLa, maxLa, ma, mb);
            FF[ab]+=sum;
        }
    }

}

template <int maxLa, int maxLc, int maxLd> void jCDAA(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLd; ++md) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLa; ++mb) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLd, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLa,ma,mb);
                    sum.fma(T[pos], DD[ab]);
                }
            }
            int cd = Tpos(maxLc,maxLd,mc,md);
            FF[cd]+=sum;
        }
    }

}


template <int maxLa, int maxLb, int maxLc> void jABCC(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLb; ++mb) {
            register cacheline64 sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLc; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLc, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLc,mc,md);
                    sum.fma(T[pos], DD[cd]);
                }
            }
            int ab = Tpos(maxLa,maxLb,ma,mb);
            FF[ab]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc> void jCCAB(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLc; ++md) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLb; ++mb) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLc, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLb,ma,mb);
                    sum.fma(T[pos], DD[ab]);
                }
            }
            int cd = Tpos(maxLc,maxLc,mc,md);
            FF[cd]+=sum;
        }
    }

}


template <int maxLa, int maxLc> void jAACC(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLa; ++mb) {
            register cacheline64 sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLc; ++md) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLc, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLc,mc,md);
                    sum.fma(T[pos], DD[cd]);
                }
            }
            int ab = Tpos(maxLa,maxLa,ma,mb);
            FF[ab]+=sum*0.5;
        }
    }

}

template <int maxLa, int maxLc> void jCCAA(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLc; ++md) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLa; ++mb) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLc, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLa,ma,mb);
                    sum.fma(T[pos], DD[ab]);
                }
            }
            int cd = Tpos(maxLc,maxLc,mc,md);
            FF[cd]+=sum*0.5;
        }
    }

}

void DMD::LoadJ64() {

    //JXC NN
    {
        J64abcd [0][0][0][0] = jABCD  <1,1,1,1>;
        J64cdab [0][0][0][0] = jCDAB  <1,1,1,1>;
        J64abcd [0][0][1][0] = jABCD  <1,1,3,1>;
        J64cdab [0][0][1][0] = jCDAB  <1,1,3,1>;
        J64abcd [0][0][1][1] = jABCD  <1,1,3,3>;
        J64cdab [0][0][1][1] = jCDAB  <1,1,3,3>;
        J64abcd [0][0][2][0] = jABCD  <1,1,5,1>;
        J64cdab [0][0][2][0] = jCDAB  <1,1,5,1>;
        J64abcd [0][0][2][1] = jABCD  <1,1,5,3>;
        J64cdab [0][0][2][1] = jCDAB  <1,1,5,3>;
        J64abcd [0][0][2][2] = jABCD  <1,1,5,5>;
        J64cdab [0][0][2][2] = jCDAB  <1,1,5,5>;
        J64abcd [0][0][3][0] = jABCD  <1,1,7,1>;
        J64cdab [0][0][3][0] = jCDAB  <1,1,7,1>;
        J64abcd [0][0][3][1] = jABCD  <1,1,7,3>;
        J64cdab [0][0][3][1] = jCDAB  <1,1,7,3>;
        J64abcd [0][0][3][2] = jABCD  <1,1,7,5>;
        J64cdab [0][0][3][2] = jCDAB  <1,1,7,5>;
        J64abcd [0][0][3][3] = jABCD  <1,1,7,7>;
        J64cdab [0][0][3][3] = jCDAB  <1,1,7,7>;
        J64abcd [0][0][4][0] = jABCD  <1,1,9,1>;
        J64cdab [0][0][4][0] = jCDAB  <1,1,9,1>;
        J64abcd [0][0][4][1] = jABCD  <1,1,9,3>;
        J64cdab [0][0][4][1] = jCDAB  <1,1,9,3>;
        J64abcd [0][0][4][2] = jABCD  <1,1,9,5>;
        J64cdab [0][0][4][2] = jCDAB  <1,1,9,5>;
        J64abcd [0][0][4][3] = jABCD  <1,1,9,7>;
        J64cdab [0][0][4][3] = jCDAB  <1,1,9,7>;
        J64abcd [0][0][4][4] = jABCD  <1,1,9,9>;
        J64cdab [0][0][4][4] = jCDAB  <1,1,9,9>;

        J64abcd [1][0][0][0] = jABCD  <3,1,1,1>;
        J64cdab [1][0][0][0] = jCDAB  <3,1,1,1>;
        J64abcd [1][0][1][0] = jABCD  <3,1,3,1>;
        J64cdab [1][0][1][0] = jCDAB  <3,1,3,1>;
        J64abcd [1][0][1][1] = jABCD  <3,1,3,3>;
        J64cdab [1][0][1][1] = jCDAB  <3,1,3,3>;
        J64abcd [1][0][2][0] = jABCD  <3,1,5,1>;
        J64cdab [1][0][2][0] = jCDAB  <3,1,5,1>;
        J64abcd [1][0][2][1] = jABCD  <3,1,5,3>;
        J64cdab [1][0][2][1] = jCDAB  <3,1,5,3>;
        J64abcd [1][0][2][2] = jABCD  <3,1,5,5>;
        J64cdab [1][0][2][2] = jCDAB  <3,1,5,5>;
        J64abcd [1][0][3][0] = jABCD  <3,1,7,1>;
        J64cdab [1][0][3][0] = jCDAB  <3,1,7,1>;
        J64abcd [1][0][3][1] = jABCD  <3,1,7,3>;
        J64cdab [1][0][3][1] = jCDAB  <3,1,7,3>;
        J64abcd [1][0][3][2] = jABCD  <3,1,7,5>;
        J64cdab [1][0][3][2] = jCDAB  <3,1,7,5>;
        J64abcd [1][0][3][3] = jABCD  <3,1,7,7>;
        J64cdab [1][0][3][3] = jCDAB  <3,1,7,7>;
        J64abcd [1][0][4][0] = jABCD  <3,1,9,1>;
        J64cdab [1][0][4][0] = jCDAB  <3,1,9,1>;
        J64abcd [1][0][4][1] = jABCD  <3,1,9,3>;
        J64cdab [1][0][4][1] = jCDAB  <3,1,9,3>;
        J64abcd [1][0][4][2] = jABCD  <3,1,9,5>;
        J64cdab [1][0][4][2] = jCDAB  <3,1,9,5>;
        J64abcd [1][0][4][3] = jABCD  <3,1,9,7>;
        J64cdab [1][0][4][3] = jCDAB  <3,1,9,7>;
        J64abcd [1][0][4][4] = jABCD  <3,1,9,9>;
        J64cdab [1][0][4][4] = jCDAB  <3,1,9,9>;

        J64abcd [1][1][0][0] = jABCD  <3,3,1,1>;
        J64cdab [1][1][0][0] = jCDAB  <3,3,1,1>;
        J64abcd [1][1][1][0] = jABCD  <3,3,3,1>;
        J64cdab [1][1][1][0] = jCDAB  <3,3,3,1>;
        J64abcd [1][1][1][1] = jABCD  <3,3,3,3>;
        J64cdab [1][1][1][1] = jCDAB  <3,3,3,3>;
        J64abcd [1][1][2][0] = jABCD  <3,3,5,1>;
        J64cdab [1][1][2][0] = jCDAB  <3,3,5,1>;
        J64abcd [1][1][2][1] = jABCD  <3,3,5,3>;
        J64cdab [1][1][2][1] = jCDAB  <3,3,5,3>;
        J64abcd [1][1][2][2] = jABCD  <3,3,5,5>;
        J64cdab [1][1][2][2] = jCDAB  <3,3,5,5>;
        J64abcd [1][1][3][0] = jABCD  <3,3,7,1>;
        J64cdab [1][1][3][0] = jCDAB  <3,3,7,1>;
        J64abcd [1][1][3][1] = jABCD  <3,3,7,3>;
        J64cdab [1][1][3][1] = jCDAB  <3,3,7,3>;
        J64abcd [1][1][3][2] = jABCD  <3,3,7,5>;
        J64cdab [1][1][3][2] = jCDAB  <3,3,7,5>;
        J64abcd [1][1][3][3] = jABCD  <3,3,7,7>;
        J64cdab [1][1][3][3] = jCDAB  <3,3,7,7>;
        J64abcd [1][1][4][0] = jABCD  <3,3,9,1>;
        J64cdab [1][1][4][0] = jCDAB  <3,3,9,1>;
        J64abcd [1][1][4][1] = jABCD  <3,3,9,3>;
        J64cdab [1][1][4][1] = jCDAB  <3,3,9,3>;
        J64abcd [1][1][4][2] = jABCD  <3,3,9,5>;
        J64cdab [1][1][4][2] = jCDAB  <3,3,9,5>;
        J64abcd [1][1][4][3] = jABCD  <3,3,9,7>;
        J64cdab [1][1][4][3] = jCDAB  <3,3,9,7>;
        J64abcd [1][1][4][4] = jABCD  <3,3,9,9>;
        J64cdab [1][1][4][4] = jCDAB  <3,3,9,9>;

        J64abcd [2][0][0][0] = jABCD  <5,1,1,1>;
        J64cdab [2][0][0][0] = jCDAB  <5,1,1,1>;
        J64abcd [2][0][1][0] = jABCD  <5,1,3,1>;
        J64cdab [2][0][1][0] = jCDAB  <5,1,3,1>;
        J64abcd [2][0][1][1] = jABCD  <5,1,3,3>;
        J64cdab [2][0][1][1] = jCDAB  <5,1,3,3>;
        J64abcd [2][0][2][0] = jABCD  <5,1,5,1>;
        J64cdab [2][0][2][0] = jCDAB  <5,1,5,1>;
        J64abcd [2][0][2][1] = jABCD  <5,1,5,3>;
        J64cdab [2][0][2][1] = jCDAB  <5,1,5,3>;
        J64abcd [2][0][2][2] = jABCD  <5,1,5,5>;
        J64cdab [2][0][2][2] = jCDAB  <5,1,5,5>;
        J64abcd [2][0][3][0] = jABCD  <5,1,7,1>;
        J64cdab [2][0][3][0] = jCDAB  <5,1,7,1>;
        J64abcd [2][0][3][1] = jABCD  <5,1,7,3>;
        J64cdab [2][0][3][1] = jCDAB  <5,1,7,3>;
        J64abcd [2][0][3][2] = jABCD  <5,1,7,5>;
        J64cdab [2][0][3][2] = jCDAB  <5,1,7,5>;
        J64abcd [2][0][3][3] = jABCD  <5,1,7,7>;
        J64cdab [2][0][3][3] = jCDAB  <5,1,7,7>;
        J64abcd [2][0][4][0] = jABCD  <5,1,9,1>;
        J64cdab [2][0][4][0] = jCDAB  <5,1,9,1>;
        J64abcd [2][0][4][1] = jABCD  <5,1,9,3>;
        J64cdab [2][0][4][1] = jCDAB  <5,1,9,3>;
        J64abcd [2][0][4][2] = jABCD  <5,1,9,5>;
        J64cdab [2][0][4][2] = jCDAB  <5,1,9,5>;
        J64abcd [2][0][4][3] = jABCD  <5,1,9,7>;
        J64cdab [2][0][4][3] = jCDAB  <5,1,9,7>;
        J64abcd [2][0][4][4] = jABCD  <5,1,9,9>;
        J64cdab [2][0][4][4] = jCDAB  <5,1,9,9>;

        J64abcd [2][1][0][0] = jABCD  <5,3,1,1>;
        J64cdab [2][1][0][0] = jCDAB  <5,3,1,1>;
        J64abcd [2][1][1][0] = jABCD  <5,3,3,1>;
        J64cdab [2][1][1][0] = jCDAB  <5,3,3,1>;
        J64abcd [2][1][1][1] = jABCD  <5,3,3,3>;
        J64cdab [2][1][1][1] = jCDAB  <5,3,3,3>;
        J64abcd [2][1][2][0] = jABCD  <5,3,5,1>;
        J64cdab [2][1][2][0] = jCDAB  <5,3,5,1>;
        J64abcd [2][1][2][1] = jABCD  <5,3,5,3>;
        J64cdab [2][1][2][1] = jCDAB  <5,3,5,3>;
        J64abcd [2][1][2][2] = jABCD  <5,3,5,5>;
        J64cdab [2][1][2][2] = jCDAB  <5,3,5,5>;
        J64abcd [2][1][3][0] = jABCD  <5,3,7,1>;
        J64cdab [2][1][3][0] = jCDAB  <5,3,7,1>;
        J64abcd [2][1][3][1] = jABCD  <5,3,7,3>;
        J64cdab [2][1][3][1] = jCDAB  <5,3,7,3>;
        J64abcd [2][1][3][2] = jABCD  <5,3,7,5>;
        J64cdab [2][1][3][2] = jCDAB  <5,3,7,5>;
        J64abcd [2][1][3][3] = jABCD  <5,3,7,7>;
        J64cdab [2][1][3][3] = jCDAB  <5,3,7,7>;
        J64abcd [2][1][4][0] = jABCD  <5,3,9,1>;
        J64cdab [2][1][4][0] = jCDAB  <5,3,9,1>;
        J64abcd [2][1][4][1] = jABCD  <5,3,9,3>;
        J64cdab [2][1][4][1] = jCDAB  <5,3,9,3>;
        J64abcd [2][1][4][2] = jABCD  <5,3,9,5>;
        J64cdab [2][1][4][2] = jCDAB  <5,3,9,5>;
        J64abcd [2][1][4][3] = jABCD  <5,3,9,7>;
        J64cdab [2][1][4][3] = jCDAB  <5,3,9,7>;
        J64abcd [2][1][4][4] = jABCD  <5,3,9,9>;
        J64cdab [2][1][4][4] = jCDAB  <5,3,9,9>;

        J64abcd [2][2][0][0] = jABCD  <5,5,1,1>;
        J64cdab [2][2][0][0] = jCDAB  <5,5,1,1>;
        J64abcd [2][2][1][0] = jABCD  <5,5,3,1>;
        J64cdab [2][2][1][0] = jCDAB  <5,5,3,1>;
        J64abcd [2][2][1][1] = jABCD  <5,5,3,3>;
        J64cdab [2][2][1][1] = jCDAB  <5,5,3,3>;
        J64abcd [2][2][2][0] = jABCD  <5,5,5,1>;
        J64cdab [2][2][2][0] = jCDAB  <5,5,5,1>;
        J64abcd [2][2][2][1] = jABCD  <5,5,5,3>;
        J64cdab [2][2][2][1] = jCDAB  <5,5,5,3>;
        J64abcd [2][2][2][2] = jABCD  <5,5,5,5>;
        J64cdab [2][2][2][2] = jCDAB  <5,5,5,5>;
        J64abcd [2][2][3][0] = jABCD  <5,5,7,1>;
        J64cdab [2][2][3][0] = jCDAB  <5,5,7,1>;
        J64abcd [2][2][3][1] = jABCD  <5,5,7,3>;
        J64cdab [2][2][3][1] = jCDAB  <5,5,7,3>;
        J64abcd [2][2][3][2] = jABCD  <5,5,7,5>;
        J64cdab [2][2][3][2] = jCDAB  <5,5,7,5>;
        J64abcd [2][2][3][3] = jABCD  <5,5,7,7>;
        J64cdab [2][2][3][3] = jCDAB  <5,5,7,7>;
        J64abcd [2][2][4][0] = jABCD  <5,5,9,1>;
        J64cdab [2][2][4][0] = jCDAB  <5,5,9,1>;
        J64abcd [2][2][4][1] = jABCD  <5,5,9,3>;
        J64cdab [2][2][4][1] = jCDAB  <5,5,9,3>;
        J64abcd [2][2][4][2] = jABCD  <5,5,9,5>;
        J64cdab [2][2][4][2] = jCDAB  <5,5,9,5>;
        J64abcd [2][2][4][3] = jABCD  <5,5,9,7>;
        J64cdab [2][2][4][3] = jCDAB  <5,5,9,7>;
        J64abcd [2][2][4][4] = jABCD  <5,5,9,9>;
        J64cdab [2][2][4][4] = jCDAB  <5,5,9,9>;

        J64abcd [3][0][0][0] = jABCD  <7,1,1,1>;
        J64cdab [3][0][0][0] = jCDAB  <7,1,1,1>;
        J64abcd [3][0][1][0] = jABCD  <7,1,3,1>;
        J64cdab [3][0][1][0] = jCDAB  <7,1,3,1>;
        J64abcd [3][0][1][1] = jABCD  <7,1,3,3>;
        J64cdab [3][0][1][1] = jCDAB  <7,1,3,3>;
        J64abcd [3][0][2][0] = jABCD  <7,1,5,1>;
        J64cdab [3][0][2][0] = jCDAB  <7,1,5,1>;
        J64abcd [3][0][2][1] = jABCD  <7,1,5,3>;
        J64cdab [3][0][2][1] = jCDAB  <7,1,5,3>;
        J64abcd [3][0][2][2] = jABCD  <7,1,5,5>;
        J64cdab [3][0][2][2] = jCDAB  <7,1,5,5>;
        J64abcd [3][0][3][0] = jABCD  <7,1,7,1>;
        J64cdab [3][0][3][0] = jCDAB  <7,1,7,1>;
        J64abcd [3][0][3][1] = jABCD  <7,1,7,3>;
        J64cdab [3][0][3][1] = jCDAB  <7,1,7,3>;
        J64abcd [3][0][3][2] = jABCD  <7,1,7,5>;
        J64cdab [3][0][3][2] = jCDAB  <7,1,7,5>;
        J64abcd [3][0][3][3] = jABCD  <7,1,7,7>;
        J64cdab [3][0][3][3] = jCDAB  <7,1,7,7>;
        J64abcd [3][0][4][0] = jABCD  <7,1,9,1>;
        J64cdab [3][0][4][0] = jCDAB  <7,1,9,1>;
        J64abcd [3][0][4][1] = jABCD  <7,1,9,3>;
        J64cdab [3][0][4][1] = jCDAB  <7,1,9,3>;
        J64abcd [3][0][4][2] = jABCD  <7,1,9,5>;
        J64cdab [3][0][4][2] = jCDAB  <7,1,9,5>;
        J64abcd [3][0][4][3] = jABCD  <7,1,9,7>;
        J64cdab [3][0][4][3] = jCDAB  <7,1,9,7>;
        J64abcd [3][0][4][4] = jABCD  <7,1,9,9>;
        J64cdab [3][0][4][4] = jCDAB  <7,1,9,9>;

        J64abcd [3][1][0][0] = jABCD  <7,3,1,1>;
        J64cdab [3][1][0][0] = jCDAB  <7,3,1,1>;
        J64abcd [3][1][1][0] = jABCD  <7,3,3,1>;
        J64cdab [3][1][1][0] = jCDAB  <7,3,3,1>;
        J64abcd [3][1][1][1] = jABCD  <7,3,3,3>;
        J64cdab [3][1][1][1] = jCDAB  <7,3,3,3>;
        J64abcd [3][1][2][0] = jABCD  <7,3,5,1>;
        J64cdab [3][1][2][0] = jCDAB  <7,3,5,1>;
        J64abcd [3][1][2][1] = jABCD  <7,3,5,3>;
        J64cdab [3][1][2][1] = jCDAB  <7,3,5,3>;
        J64abcd [3][1][2][2] = jABCD  <7,3,5,5>;
        J64cdab [3][1][2][2] = jCDAB  <7,3,5,5>;
        J64abcd [3][1][3][0] = jABCD  <7,3,7,1>;
        J64cdab [3][1][3][0] = jCDAB  <7,3,7,1>;
        J64abcd [3][1][3][1] = jABCD  <7,3,7,3>;
        J64cdab [3][1][3][1] = jCDAB  <7,3,7,3>;
        J64abcd [3][1][3][2] = jABCD  <7,3,7,5>;
        J64cdab [3][1][3][2] = jCDAB  <7,3,7,5>;
        J64abcd [3][1][3][3] = jABCD  <7,3,7,7>;
        J64cdab [3][1][3][3] = jCDAB  <7,3,7,7>;
        J64abcd [3][1][4][0] = jABCD  <7,3,9,1>;
        J64cdab [3][1][4][0] = jCDAB  <7,3,9,1>;
        J64abcd [3][1][4][1] = jABCD  <7,3,9,3>;
        J64cdab [3][1][4][1] = jCDAB  <7,3,9,3>;
        J64abcd [3][1][4][2] = jABCD  <7,3,9,5>;
        J64cdab [3][1][4][2] = jCDAB  <7,3,9,5>;
        J64abcd [3][1][4][3] = jABCD  <7,3,9,7>;
        J64cdab [3][1][4][3] = jCDAB  <7,3,9,7>;
        J64abcd [3][1][4][4] = jABCD  <7,3,9,9>;
        J64cdab [3][1][4][4] = jCDAB  <7,3,9,9>;

        J64abcd [3][2][0][0] = jABCD  <7,5,1,1>;
        J64cdab [3][2][0][0] = jCDAB  <7,5,1,1>;
        J64abcd [3][2][1][0] = jABCD  <7,5,3,1>;
        J64cdab [3][2][1][0] = jCDAB  <7,5,3,1>;
        J64abcd [3][2][1][1] = jABCD  <7,5,3,3>;
        J64cdab [3][2][1][1] = jCDAB  <7,5,3,3>;
        J64abcd [3][2][2][0] = jABCD  <7,5,5,1>;
        J64cdab [3][2][2][0] = jCDAB  <7,5,5,1>;
        J64abcd [3][2][2][1] = jABCD  <7,5,5,3>;
        J64cdab [3][2][2][1] = jCDAB  <7,5,5,3>;
        J64abcd [3][2][2][2] = jABCD  <7,5,5,5>;
        J64cdab [3][2][2][2] = jCDAB  <7,5,5,5>;
        J64abcd [3][2][3][0] = jABCD  <7,5,7,1>;
        J64cdab [3][2][3][0] = jCDAB  <7,5,7,1>;
        J64abcd [3][2][3][1] = jABCD  <7,5,7,3>;
        J64cdab [3][2][3][1] = jCDAB  <7,5,7,3>;
        J64abcd [3][2][3][2] = jABCD  <7,5,7,5>;
        J64cdab [3][2][3][2] = jCDAB  <7,5,7,5>;
        J64abcd [3][2][3][3] = jABCD  <7,5,7,7>;
        J64cdab [3][2][3][3] = jCDAB  <7,5,7,7>;
        J64abcd [3][2][4][0] = jABCD  <7,5,9,1>;
        J64cdab [3][2][4][0] = jCDAB  <7,5,9,1>;
        J64abcd [3][2][4][1] = jABCD  <7,5,9,3>;
        J64cdab [3][2][4][1] = jCDAB  <7,5,9,3>;
        J64abcd [3][2][4][2] = jABCD  <7,5,9,5>;
        J64cdab [3][2][4][2] = jCDAB  <7,5,9,5>;
        J64abcd [3][2][4][3] = jABCD  <7,5,9,7>;
        J64cdab [3][2][4][3] = jCDAB  <7,5,9,7>;
        J64abcd [3][2][4][4] = jABCD  <7,5,9,9>;
        J64cdab [3][2][4][4] = jCDAB  <7,5,9,9>;

        J64abcd [3][3][0][0] = jABCD  <7,7,1,1>;
        J64cdab [3][3][0][0] = jCDAB  <7,7,1,1>;
        J64abcd [3][3][1][0] = jABCD  <7,7,3,1>;
        J64cdab [3][3][1][0] = jCDAB  <7,7,3,1>;
        J64abcd [3][3][1][1] = jABCD  <7,7,3,3>;
        J64cdab [3][3][1][1] = jCDAB  <7,7,3,3>;
        J64abcd [3][3][2][0] = jABCD  <7,7,5,1>;
        J64cdab [3][3][2][0] = jCDAB  <7,7,5,1>;
        J64abcd [3][3][2][1] = jABCD  <7,7,5,3>;
        J64cdab [3][3][2][1] = jCDAB  <7,7,5,3>;
        J64abcd [3][3][2][2] = jABCD  <7,7,5,5>;
        J64cdab [3][3][2][2] = jCDAB  <7,7,5,5>;
        J64abcd [3][3][3][0] = jABCD  <7,7,7,1>;
        J64cdab [3][3][3][0] = jCDAB  <7,7,7,1>;
        J64abcd [3][3][3][1] = jABCD  <7,7,7,3>;
        J64cdab [3][3][3][1] = jCDAB  <7,7,7,3>;
        J64abcd [3][3][3][2] = jABCD  <7,7,7,5>;
        J64cdab [3][3][3][2] = jCDAB  <7,7,7,5>;
        J64abcd [3][3][3][3] = jABCD  <7,7,7,7>;
        J64cdab [3][3][3][3] = jCDAB  <7,7,7,7>;
        J64abcd [3][3][4][0] = jABCD  <7,7,9,1>;
        J64cdab [3][3][4][0] = jCDAB  <7,7,9,1>;
        J64abcd [3][3][4][1] = jABCD  <7,7,9,3>;
        J64cdab [3][3][4][1] = jCDAB  <7,7,9,3>;
        J64abcd [3][3][4][2] = jABCD  <7,7,9,5>;
        J64cdab [3][3][4][2] = jCDAB  <7,7,9,5>;
        J64abcd [3][3][4][3] = jABCD  <7,7,9,7>;
        J64cdab [3][3][4][3] = jCDAB  <7,7,9,7>;
        J64abcd [3][3][4][4] = jABCD  <7,7,9,9>;
        J64cdab [3][3][4][4] = jCDAB  <7,7,9,9>;

        J64abcd [4][0][0][0] = jABCD  <9,1,1,1>;
        J64cdab [4][0][0][0] = jCDAB  <9,1,1,1>;
        J64abcd [4][0][1][0] = jABCD  <9,1,3,1>;
        J64cdab [4][0][1][0] = jCDAB  <9,1,3,1>;
        J64abcd [4][0][1][1] = jABCD  <9,1,3,3>;
        J64cdab [4][0][1][1] = jCDAB  <9,1,3,3>;
        J64abcd [4][0][2][0] = jABCD  <9,1,5,1>;
        J64cdab [4][0][2][0] = jCDAB  <9,1,5,1>;
        J64abcd [4][0][2][1] = jABCD  <9,1,5,3>;
        J64cdab [4][0][2][1] = jCDAB  <9,1,5,3>;
        J64abcd [4][0][2][2] = jABCD  <9,1,5,5>;
        J64cdab [4][0][2][2] = jCDAB  <9,1,5,5>;
        J64abcd [4][0][3][0] = jABCD  <9,1,7,1>;
        J64cdab [4][0][3][0] = jCDAB  <9,1,7,1>;
        J64abcd [4][0][3][1] = jABCD  <9,1,7,3>;
        J64cdab [4][0][3][1] = jCDAB  <9,1,7,3>;
        J64abcd [4][0][3][2] = jABCD  <9,1,7,5>;
        J64cdab [4][0][3][2] = jCDAB  <9,1,7,5>;
        J64abcd [4][0][3][3] = jABCD  <9,1,7,7>;
        J64cdab [4][0][3][3] = jCDAB  <9,1,7,7>;
        J64abcd [4][0][4][0] = jABCD  <9,1,9,1>;
        J64cdab [4][0][4][0] = jCDAB  <9,1,9,1>;
        J64abcd [4][0][4][1] = jABCD  <9,1,9,3>;
        J64cdab [4][0][4][1] = jCDAB  <9,1,9,3>;
        J64abcd [4][0][4][2] = jABCD  <9,1,9,5>;
        J64cdab [4][0][4][2] = jCDAB  <9,1,9,5>;
        J64abcd [4][0][4][3] = jABCD  <9,1,9,7>;
        J64cdab [4][0][4][3] = jCDAB  <9,1,9,7>;
        J64abcd [4][0][4][4] = jABCD  <9,1,9,9>;
        J64cdab [4][0][4][4] = jCDAB  <9,1,9,9>;

        J64abcd [4][1][0][0] = jABCD  <9,3,1,1>;
        J64cdab [4][1][0][0] = jCDAB  <9,3,1,1>;
        J64abcd [4][1][1][0] = jABCD  <9,3,3,1>;
        J64cdab [4][1][1][0] = jCDAB  <9,3,3,1>;
        J64abcd [4][1][1][1] = jABCD  <9,3,3,3>;
        J64cdab [4][1][1][1] = jCDAB  <9,3,3,3>;
        J64abcd [4][1][2][0] = jABCD  <9,3,5,1>;
        J64cdab [4][1][2][0] = jCDAB  <9,3,5,1>;
        J64abcd [4][1][2][1] = jABCD  <9,3,5,3>;
        J64cdab [4][1][2][1] = jCDAB  <9,3,5,3>;
        J64abcd [4][1][2][2] = jABCD  <9,3,5,5>;
        J64cdab [4][1][2][2] = jCDAB  <9,3,5,5>;
        J64abcd [4][1][3][0] = jABCD  <9,3,7,1>;
        J64cdab [4][1][3][0] = jCDAB  <9,3,7,1>;
        J64abcd [4][1][3][1] = jABCD  <9,3,7,3>;
        J64cdab [4][1][3][1] = jCDAB  <9,3,7,3>;
        J64abcd [4][1][3][2] = jABCD  <9,3,7,5>;
        J64cdab [4][1][3][2] = jCDAB  <9,3,7,5>;
        J64abcd [4][1][3][3] = jABCD  <9,3,7,7>;
        J64cdab [4][1][3][3] = jCDAB  <9,3,7,7>;
        J64abcd [4][1][4][0] = jABCD  <9,3,9,1>;
        J64cdab [4][1][4][0] = jCDAB  <9,3,9,1>;
        J64abcd [4][1][4][1] = jABCD  <9,3,9,3>;
        J64cdab [4][1][4][1] = jCDAB  <9,3,9,3>;
        J64abcd [4][1][4][2] = jABCD  <9,3,9,5>;
        J64cdab [4][1][4][2] = jCDAB  <9,3,9,5>;
        J64abcd [4][1][4][3] = jABCD  <9,3,9,7>;
        J64cdab [4][1][4][3] = jCDAB  <9,3,9,7>;
        J64abcd [4][1][4][4] = jABCD  <9,3,9,9>;
        J64cdab [4][1][4][4] = jCDAB  <9,3,9,9>;

        J64abcd [4][2][0][0] = jABCD  <9,5,1,1>;
        J64cdab [4][2][0][0] = jCDAB  <9,5,1,1>;
        J64abcd [4][2][1][0] = jABCD  <9,5,3,1>;
        J64cdab [4][2][1][0] = jCDAB  <9,5,3,1>;
        J64abcd [4][2][1][1] = jABCD  <9,5,3,3>;
        J64cdab [4][2][1][1] = jCDAB  <9,5,3,3>;
        J64abcd [4][2][2][0] = jABCD  <9,5,5,1>;
        J64cdab [4][2][2][0] = jCDAB  <9,5,5,1>;
        J64abcd [4][2][2][1] = jABCD  <9,5,5,3>;
        J64cdab [4][2][2][1] = jCDAB  <9,5,5,3>;
        J64abcd [4][2][2][2] = jABCD  <9,5,5,5>;
        J64cdab [4][2][2][2] = jCDAB  <9,5,5,5>;
        J64abcd [4][2][3][0] = jABCD  <9,5,7,1>;
        J64cdab [4][2][3][0] = jCDAB  <9,5,7,1>;
        J64abcd [4][2][3][1] = jABCD  <9,5,7,3>;
        J64cdab [4][2][3][1] = jCDAB  <9,5,7,3>;
        J64abcd [4][2][3][2] = jABCD  <9,5,7,5>;
        J64cdab [4][2][3][2] = jCDAB  <9,5,7,5>;
        J64abcd [4][2][3][3] = jABCD  <9,5,7,7>;
        J64cdab [4][2][3][3] = jCDAB  <9,5,7,7>;
        J64abcd [4][2][4][0] = jABCD  <9,5,9,1>;
        J64cdab [4][2][4][0] = jCDAB  <9,5,9,1>;
        J64abcd [4][2][4][1] = jABCD  <9,5,9,3>;
        J64cdab [4][2][4][1] = jCDAB  <9,5,9,3>;
        J64abcd [4][2][4][2] = jABCD  <9,5,9,5>;
        J64cdab [4][2][4][2] = jCDAB  <9,5,9,5>;
        J64abcd [4][2][4][3] = jABCD  <9,5,9,7>;
        J64cdab [4][2][4][3] = jCDAB  <9,5,9,7>;
        J64abcd [4][2][4][4] = jABCD  <9,5,9,9>;
        J64cdab [4][2][4][4] = jCDAB  <9,5,9,9>;

        J64abcd [4][3][0][0] = jABCD  <9,7,1,1>;
        J64cdab [4][3][0][0] = jCDAB  <9,7,1,1>;
        J64abcd [4][3][1][0] = jABCD  <9,7,3,1>;
        J64cdab [4][3][1][0] = jCDAB  <9,7,3,1>;
        J64abcd [4][3][1][1] = jABCD  <9,7,3,3>;
        J64cdab [4][3][1][1] = jCDAB  <9,7,3,3>;
        J64abcd [4][3][2][0] = jABCD  <9,7,5,1>;
        J64cdab [4][3][2][0] = jCDAB  <9,7,5,1>;
        J64abcd [4][3][2][1] = jABCD  <9,7,5,3>;
        J64cdab [4][3][2][1] = jCDAB  <9,7,5,3>;
        J64abcd [4][3][2][2] = jABCD  <9,7,5,5>;
        J64cdab [4][3][2][2] = jCDAB  <9,7,5,5>;
        J64abcd [4][3][3][0] = jABCD  <9,7,7,1>;
        J64cdab [4][3][3][0] = jCDAB  <9,7,7,1>;
        J64abcd [4][3][3][1] = jABCD  <9,7,7,3>;
        J64cdab [4][3][3][1] = jCDAB  <9,7,7,3>;
        J64abcd [4][3][3][2] = jABCD  <9,7,7,5>;
        J64cdab [4][3][3][2] = jCDAB  <9,7,7,5>;
        J64abcd [4][3][3][3] = jABCD  <9,7,7,7>;
        J64cdab [4][3][3][3] = jCDAB  <9,7,7,7>;
        J64abcd [4][3][4][0] = jABCD  <9,7,9,1>;
        J64cdab [4][3][4][0] = jCDAB  <9,7,9,1>;
        J64abcd [4][3][4][1] = jABCD  <9,7,9,3>;
        J64cdab [4][3][4][1] = jCDAB  <9,7,9,3>;
        J64abcd [4][3][4][2] = jABCD  <9,7,9,5>;
        J64cdab [4][3][4][2] = jCDAB  <9,7,9,5>;
        J64abcd [4][3][4][3] = jABCD  <9,7,9,7>;
        J64cdab [4][3][4][3] = jCDAB  <9,7,9,7>;
        J64abcd [4][3][4][4] = jABCD  <9,7,9,9>;
        J64cdab [4][3][4][4] = jCDAB  <9,7,9,9>;

        J64abcd [4][4][0][0] = jABCD  <9,9,1,1>;
        J64cdab [4][4][0][0] = jCDAB  <9,9,1,1>;
        J64abcd [4][4][1][0] = jABCD  <9,9,3,1>;
        J64cdab [4][4][1][0] = jCDAB  <9,9,3,1>;
        J64abcd [4][4][1][1] = jABCD  <9,9,3,3>;
        J64cdab [4][4][1][1] = jCDAB  <9,9,3,3>;
        J64abcd [4][4][2][0] = jABCD  <9,9,5,1>;
        J64cdab [4][4][2][0] = jCDAB  <9,9,5,1>;
        J64abcd [4][4][2][1] = jABCD  <9,9,5,3>;
        J64cdab [4][4][2][1] = jCDAB  <9,9,5,3>;
        J64abcd [4][4][2][2] = jABCD  <9,9,5,5>;
        J64cdab [4][4][2][2] = jCDAB  <9,9,5,5>;
        J64abcd [4][4][3][0] = jABCD  <9,9,7,1>;
        J64cdab [4][4][3][0] = jCDAB  <9,9,7,1>;
        J64abcd [4][4][3][1] = jABCD  <9,9,7,3>;
        J64cdab [4][4][3][1] = jCDAB  <9,9,7,3>;
        J64abcd [4][4][3][2] = jABCD  <9,9,7,5>;
        J64cdab [4][4][3][2] = jCDAB  <9,9,7,5>;
        J64abcd [4][4][3][3] = jABCD  <9,9,7,7>;
        J64cdab [4][4][3][3] = jCDAB  <9,9,7,7>;
        J64abcd [4][4][4][0] = jABCD  <9,9,9,1>;
        J64cdab [4][4][4][0] = jCDAB  <9,9,9,1>;
        J64abcd [4][4][4][1] = jABCD  <9,9,9,3>;
        J64cdab [4][4][4][1] = jCDAB  <9,9,9,3>;
        J64abcd [4][4][4][2] = jABCD  <9,9,9,5>;
        J64cdab [4][4][4][2] = jCDAB  <9,9,9,5>;
        J64abcd [4][4][4][3] = jABCD  <9,9,9,7>;
        J64cdab [4][4][4][3] = jCDAB  <9,9,9,7>;
        J64abcd [4][4][4][4] = jABCD  <9,9,9,9>;
        J64cdab [4][4][4][4] = jCDAB  <9,9,9,9>;
    }

    //JXC NS
    {
        J64abcc [0][0][0] = jABCC  <1,1,1>;
        J64ccab [0][0][0] = jCCAB  <1,1,1>;
        J64abcc [0][0][1] = jABCC  <1,1,3>;
        J64ccab [0][0][1] = jCCAB  <1,1,3>;
        J64abcc [0][0][2] = jABCC  <1,1,5>;
        J64ccab [0][0][2] = jCCAB  <1,1,5>;
        J64abcc [0][0][3] = jABCC  <1,1,7>;
        J64ccab [0][0][3] = jCCAB  <1,1,7>;
        J64abcc [0][0][4] = jABCC  <1,1,9>;
        J64ccab [0][0][4] = jCCAB  <1,1,9>;

        J64abcc [1][0][0] = jABCC  <3,1,1>;
        J64ccab [1][0][0] = jCCAB  <3,1,1>;
        J64abcc [1][0][1] = jABCC  <3,1,3>;
        J64ccab [1][0][1] = jCCAB  <3,1,3>;
        J64abcc [1][0][2] = jABCC  <3,1,5>;
        J64ccab [1][0][2] = jCCAB  <3,1,5>;
        J64abcc [1][0][3] = jABCC  <3,1,7>;
        J64ccab [1][0][3] = jCCAB  <3,1,7>;
        J64abcc [1][0][4] = jABCC  <3,1,9>;
        J64ccab [1][0][4] = jCCAB  <3,1,9>;

        J64abcc [1][1][0] = jABCC  <3,3,1>;
        J64ccab [1][1][0] = jCCAB  <3,3,1>;
        J64abcc [1][1][1] = jABCC  <3,3,3>;
        J64ccab [1][1][1] = jCCAB  <3,3,3>;
        J64abcc [1][1][2] = jABCC  <3,3,5>;
        J64ccab [1][1][2] = jCCAB  <3,3,5>;
        J64abcc [1][1][3] = jABCC  <3,3,7>;
        J64ccab [1][1][3] = jCCAB  <3,3,7>;
        J64abcc [1][1][4] = jABCC  <3,3,9>;
        J64ccab [1][1][4] = jCCAB  <3,3,9>;

        J64abcc [2][0][0] = jABCC  <5,1,1>;
        J64ccab [2][0][0] = jCCAB  <5,1,1>;
        J64abcc [2][0][1] = jABCC  <5,1,3>;
        J64ccab [2][0][1] = jCCAB  <5,1,3>;
        J64abcc [2][0][2] = jABCC  <5,1,5>;
        J64ccab [2][0][2] = jCCAB  <5,1,5>;
        J64abcc [2][0][3] = jABCC  <5,1,7>;
        J64ccab [2][0][3] = jCCAB  <5,1,7>;
        J64abcc [2][0][4] = jABCC  <5,1,9>;
        J64ccab [2][0][4] = jCCAB  <5,1,9>;

        J64abcc [2][1][0] = jABCC  <5,3,1>;
        J64ccab [2][1][0] = jCCAB  <5,3,1>;
        J64abcc [2][1][1] = jABCC  <5,3,3>;
        J64ccab [2][1][1] = jCCAB  <5,3,3>;
        J64abcc [2][1][2] = jABCC  <5,3,5>;
        J64ccab [2][1][2] = jCCAB  <5,3,5>;
        J64abcc [2][1][3] = jABCC  <5,3,7>;
        J64ccab [2][1][3] = jCCAB  <5,3,7>;
        J64abcc [2][1][4] = jABCC  <5,3,9>;
        J64ccab [2][1][4] = jCCAB  <5,3,9>;

        J64abcc [2][2][0] = jABCC  <5,5,1>;
        J64ccab [2][2][0] = jCCAB  <5,5,1>;
        J64abcc [2][2][1] = jABCC  <5,5,3>;
        J64ccab [2][2][1] = jCCAB  <5,5,3>;
        J64abcc [2][2][2] = jABCC  <5,5,5>;
        J64ccab [2][2][2] = jCCAB  <5,5,5>;
        J64abcc [2][2][3] = jABCC  <5,5,7>;
        J64ccab [2][2][3] = jCCAB  <5,5,7>;
        J64abcc [2][2][4] = jABCC  <5,5,9>;
        J64ccab [2][2][4] = jCCAB  <5,5,9>;

        J64abcc [3][0][0] = jABCC  <7,1,1>;
        J64ccab [3][0][0] = jCCAB  <7,1,1>;
        J64abcc [3][0][1] = jABCC  <7,1,3>;
        J64ccab [3][0][1] = jCCAB  <7,1,3>;
        J64abcc [3][0][2] = jABCC  <7,1,5>;
        J64ccab [3][0][2] = jCCAB  <7,1,5>;
        J64abcc [3][0][3] = jABCC  <7,1,7>;
        J64ccab [3][0][3] = jCCAB  <7,1,7>;
        J64abcc [3][0][4] = jABCC  <7,1,9>;
        J64ccab [3][0][4] = jCCAB  <7,1,9>;

        J64abcc [3][1][0] = jABCC  <7,3,1>;
        J64ccab [3][1][0] = jCCAB  <7,3,1>;
        J64abcc [3][1][1] = jABCC  <7,3,3>;
        J64ccab [3][1][1] = jCCAB  <7,3,3>;
        J64abcc [3][1][2] = jABCC  <7,3,5>;
        J64ccab [3][1][2] = jCCAB  <7,3,5>;
        J64abcc [3][1][3] = jABCC  <7,3,7>;
        J64ccab [3][1][3] = jCCAB  <7,3,7>;
        J64abcc [3][1][4] = jABCC  <7,3,9>;
        J64ccab [3][1][4] = jCCAB  <7,3,9>;

        J64abcc [3][2][0] = jABCC  <7,5,1>;
        J64ccab [3][2][0] = jCCAB  <7,5,1>;
        J64abcc [3][2][1] = jABCC  <7,5,3>;
        J64ccab [3][2][1] = jCCAB  <7,5,3>;
        J64abcc [3][2][2] = jABCC  <7,5,5>;
        J64ccab [3][2][2] = jCCAB  <7,5,5>;
        J64abcc [3][2][3] = jABCC  <7,5,7>;
        J64ccab [3][2][3] = jCCAB  <7,5,7>;
        J64abcc [3][2][4] = jABCC  <7,5,9>;
        J64ccab [3][2][4] = jCCAB  <7,5,9>;

        J64abcc [3][3][0] = jABCC  <7,7,1>;
        J64ccab [3][3][0] = jCCAB  <7,7,1>;
        J64abcc [3][3][1] = jABCC  <7,7,3>;
        J64ccab [3][3][1] = jCCAB  <7,7,3>;
        J64abcc [3][3][2] = jABCC  <7,7,5>;
        J64ccab [3][3][2] = jCCAB  <7,7,5>;
        J64abcc [3][3][3] = jABCC  <7,7,7>;
        J64ccab [3][3][3] = jCCAB  <7,7,7>;
        J64abcc [3][3][4] = jABCC  <7,7,9>;
        J64ccab [3][3][4] = jCCAB  <7,7,9>;

        J64abcc [4][0][0] = jABCC  <9,1,1>;
        J64ccab [4][0][0] = jCCAB  <9,1,1>;
        J64abcc [4][0][1] = jABCC  <9,1,3>;
        J64ccab [4][0][1] = jCCAB  <9,1,3>;
        J64abcc [4][0][2] = jABCC  <9,1,5>;
        J64ccab [4][0][2] = jCCAB  <9,1,5>;
        J64abcc [4][0][3] = jABCC  <9,1,7>;
        J64ccab [4][0][3] = jCCAB  <9,1,7>;
        J64abcc [4][0][4] = jABCC  <9,1,9>;
        J64ccab [4][0][4] = jCCAB  <9,1,9>;

        J64abcc [4][1][0] = jABCC  <9,3,1>;
        J64ccab [4][1][0] = jCCAB  <9,3,1>;
        J64abcc [4][1][1] = jABCC  <9,3,3>;
        J64ccab [4][1][1] = jCCAB  <9,3,3>;
        J64abcc [4][1][2] = jABCC  <9,3,5>;
        J64ccab [4][1][2] = jCCAB  <9,3,5>;
        J64abcc [4][1][3] = jABCC  <9,3,7>;
        J64ccab [4][1][3] = jCCAB  <9,3,7>;
        J64abcc [4][1][4] = jABCC  <9,3,9>;
        J64ccab [4][1][4] = jCCAB  <9,3,9>;

        J64abcc [4][2][0] = jABCC  <9,5,1>;
        J64ccab [4][2][0] = jCCAB  <9,5,1>;
        J64abcc [4][2][1] = jABCC  <9,5,3>;
        J64ccab [4][2][1] = jCCAB  <9,5,3>;
        J64abcc [4][2][2] = jABCC  <9,5,5>;
        J64ccab [4][2][2] = jCCAB  <9,5,5>;
        J64abcc [4][2][3] = jABCC  <9,5,7>;
        J64ccab [4][2][3] = jCCAB  <9,5,7>;
        J64abcc [4][2][4] = jABCC  <9,5,9>;
        J64ccab [4][2][4] = jCCAB  <9,5,9>;

        J64abcc [4][3][0] = jABCC  <9,7,1>;
        J64ccab [4][3][0] = jCCAB  <9,7,1>;
        J64abcc [4][3][1] = jABCC  <9,7,3>;
        J64ccab [4][3][1] = jCCAB  <9,7,3>;
        J64abcc [4][3][2] = jABCC  <9,7,5>;
        J64ccab [4][3][2] = jCCAB  <9,7,5>;
        J64abcc [4][3][3] = jABCC  <9,7,7>;
        J64ccab [4][3][3] = jCCAB  <9,7,7>;
        J64abcc [4][3][4] = jABCC  <9,7,9>;
        J64ccab [4][3][4] = jCCAB  <9,7,9>;
        J64abcc [4][4][0] = jABCC  <9,9,1>;
        J64ccab [4][4][0] = jCCAB  <9,9,1>;
        J64abcc [4][4][1] = jABCC  <9,9,3>;
        J64ccab [4][4][1] = jCCAB  <9,9,3>;
        J64abcc [4][4][2] = jABCC  <9,9,5>;
        J64ccab [4][4][2] = jCCAB  <9,9,5>;
        J64abcc [4][4][3] = jABCC  <9,9,7>;
        J64ccab [4][4][3] = jCCAB  <9,9,7>;
        J64abcc [4][4][4] = jABCC  <9,9,9>;
        J64ccab [4][4][4] = jCCAB  <9,9,9>;
    }

    //JXC SN
    {
        J64aacd [0][0][0] = jAACD  <1,1,1>;
        J64cdaa [0][0][0] = jCDAA  <1,1,1>;
        J64aacd [0][1][0] = jAACD  <1,3,1>;
        J64cdaa [0][1][0] = jCDAA  <1,3,1>;
        J64aacd [0][1][1] = jAACD  <1,3,3>;
        J64cdaa [0][1][1] = jCDAA  <1,3,3>;
        J64aacd [0][2][0] = jAACD  <1,5,1>;
        J64cdaa [0][2][0] = jCDAA  <1,5,1>;
        J64aacd [0][2][1] = jAACD  <1,5,3>;
        J64cdaa [0][2][1] = jCDAA  <1,5,3>;
        J64aacd [0][2][2] = jAACD  <1,5,5>;
        J64cdaa [0][2][2] = jCDAA  <1,5,5>;
        J64aacd [0][3][0] = jAACD  <1,7,1>;
        J64cdaa [0][3][0] = jCDAA  <1,7,1>;
        J64aacd [0][3][1] = jAACD  <1,7,3>;
        J64cdaa [0][3][1] = jCDAA  <1,7,3>;
        J64aacd [0][3][2] = jAACD  <1,7,5>;
        J64cdaa [0][3][2] = jCDAA  <1,7,5>;
        J64aacd [0][3][3] = jAACD  <1,7,7>;
        J64cdaa [0][3][3] = jCDAA  <1,7,7>;
        J64aacd [0][4][0] = jAACD  <1,9,1>;
        J64cdaa [0][4][0] = jCDAA  <1,9,1>;
        J64aacd [0][4][1] = jAACD  <1,9,3>;
        J64cdaa [0][4][1] = jCDAA  <1,9,3>;
        J64aacd [0][4][2] = jAACD  <1,9,5>;
        J64cdaa [0][4][2] = jCDAA  <1,9,5>;
        J64aacd [0][4][3] = jAACD  <1,9,7>;
        J64cdaa [0][4][3] = jCDAA  <1,9,7>;
        J64aacd [0][4][4] = jAACD  <1,9,9>;
        J64cdaa [0][4][4] = jCDAA  <1,9,9>;
        J64aacd [1][0][0] = jAACD  <3,1,1>;
        J64cdaa [1][0][0] = jCDAA  <3,1,1>;
        J64aacd [1][1][0] = jAACD  <3,3,1>;
        J64cdaa [1][1][0] = jCDAA  <3,3,1>;
        J64aacd [1][1][1] = jAACD  <3,3,3>;
        J64cdaa [1][1][1] = jCDAA  <3,3,3>;
        J64aacd [1][2][0] = jAACD  <3,5,1>;
        J64cdaa [1][2][0] = jCDAA  <3,5,1>;
        J64aacd [1][2][1] = jAACD  <3,5,3>;
        J64cdaa [1][2][1] = jCDAA  <3,5,3>;
        J64aacd [1][2][2] = jAACD  <3,5,5>;
        J64cdaa [1][2][2] = jCDAA  <3,5,5>;
        J64aacd [1][3][0] = jAACD  <3,7,1>;
        J64cdaa [1][3][0] = jCDAA  <3,7,1>;
        J64aacd [1][3][1] = jAACD  <3,7,3>;
        J64cdaa [1][3][1] = jCDAA  <3,7,3>;
        J64aacd [1][3][2] = jAACD  <3,7,5>;
        J64cdaa [1][3][2] = jCDAA  <3,7,5>;
        J64aacd [1][3][3] = jAACD  <3,7,7>;
        J64cdaa [1][3][3] = jCDAA  <3,7,7>;
        J64aacd [1][4][0] = jAACD  <3,9,1>;
        J64cdaa [1][4][0] = jCDAA  <3,9,1>;
        J64aacd [1][4][1] = jAACD  <3,9,3>;
        J64cdaa [1][4][1] = jCDAA  <3,9,3>;
        J64aacd [1][4][2] = jAACD  <3,9,5>;
        J64cdaa [1][4][2] = jCDAA  <3,9,5>;
        J64aacd [1][4][3] = jAACD  <3,9,7>;
        J64cdaa [1][4][3] = jCDAA  <3,9,7>;
        J64aacd [1][4][4] = jAACD  <3,9,9>;
        J64cdaa [1][4][4] = jCDAA  <3,9,9>;
        J64aacd [2][0][0] = jAACD  <5,1,1>;
        J64cdaa [2][0][0] = jCDAA  <5,1,1>;
        J64aacd [2][1][0] = jAACD  <5,3,1>;
        J64cdaa [2][1][0] = jCDAA  <5,3,1>;
        J64aacd [2][1][1] = jAACD  <5,3,3>;
        J64cdaa [2][1][1] = jCDAA  <5,3,3>;
        J64aacd [2][2][0] = jAACD  <5,5,1>;
        J64cdaa [2][2][0] = jCDAA  <5,5,1>;
        J64aacd [2][2][1] = jAACD  <5,5,3>;
        J64cdaa [2][2][1] = jCDAA  <5,5,3>;
        J64aacd [2][2][2] = jAACD  <5,5,5>;
        J64cdaa [2][2][2] = jCDAA  <5,5,5>;
        J64aacd [2][3][0] = jAACD  <5,7,1>;
        J64cdaa [2][3][0] = jCDAA  <5,7,1>;
        J64aacd [2][3][1] = jAACD  <5,7,3>;
        J64cdaa [2][3][1] = jCDAA  <5,7,3>;
        J64aacd [2][3][2] = jAACD  <5,7,5>;
        J64cdaa [2][3][2] = jCDAA  <5,7,5>;
        J64aacd [2][3][3] = jAACD  <5,7,7>;
        J64cdaa [2][3][3] = jCDAA  <5,7,7>;
        J64aacd [2][4][0] = jAACD  <5,9,1>;
        J64cdaa [2][4][0] = jCDAA  <5,9,1>;
        J64aacd [2][4][1] = jAACD  <5,9,3>;
        J64cdaa [2][4][1] = jCDAA  <5,9,3>;
        J64aacd [2][4][2] = jAACD  <5,9,5>;
        J64cdaa [2][4][2] = jCDAA  <5,9,5>;
        J64aacd [2][4][3] = jAACD  <5,9,7>;
        J64cdaa [2][4][3] = jCDAA  <5,9,7>;
        J64aacd [2][4][4] = jAACD  <5,9,9>;
        J64cdaa [2][4][4] = jCDAA  <5,9,9>;
        J64aacd [3][0][0] = jAACD  <7,1,1>;
        J64cdaa [3][0][0] = jCDAA  <7,1,1>;
        J64aacd [3][1][0] = jAACD  <7,3,1>;
        J64cdaa [3][1][0] = jCDAA  <7,3,1>;
        J64aacd [3][1][1] = jAACD  <7,3,3>;
        J64cdaa [3][1][1] = jCDAA  <7,3,3>;
        J64aacd [3][2][0] = jAACD  <7,5,1>;
        J64cdaa [3][2][0] = jCDAA  <7,5,1>;
        J64aacd [3][2][1] = jAACD  <7,5,3>;
        J64cdaa [3][2][1] = jCDAA  <7,5,3>;
        J64aacd [3][2][2] = jAACD  <7,5,5>;
        J64cdaa [3][2][2] = jCDAA  <7,5,5>;
        J64aacd [3][3][0] = jAACD  <7,7,1>;
        J64cdaa [3][3][0] = jCDAA  <7,7,1>;
        J64aacd [3][3][1] = jAACD  <7,7,3>;
        J64cdaa [3][3][1] = jCDAA  <7,7,3>;
        J64aacd [3][3][2] = jAACD  <7,7,5>;
        J64cdaa [3][3][2] = jCDAA  <7,7,5>;
        J64aacd [3][3][3] = jAACD  <7,7,7>;
        J64cdaa [3][3][3] = jCDAA  <7,7,7>;
        J64aacd [3][4][0] = jAACD  <7,9,1>;
        J64cdaa [3][4][0] = jCDAA  <7,9,1>;
        J64aacd [3][4][1] = jAACD  <7,9,3>;
        J64cdaa [3][4][1] = jCDAA  <7,9,3>;
        J64aacd [3][4][2] = jAACD  <7,9,5>;
        J64cdaa [3][4][2] = jCDAA  <7,9,5>;
        J64aacd [3][4][3] = jAACD  <7,9,7>;
        J64cdaa [3][4][3] = jCDAA  <7,9,7>;
        J64aacd [3][4][4] = jAACD  <7,9,9>;
        J64cdaa [3][4][4] = jCDAA  <7,9,9>;
        J64aacd [4][0][0] = jAACD  <9,1,1>;
        J64cdaa [4][0][0] = jCDAA  <9,1,1>;
        J64aacd [4][1][0] = jAACD  <9,3,1>;
        J64cdaa [4][1][0] = jCDAA  <9,3,1>;
        J64aacd [4][1][1] = jAACD  <9,3,3>;
        J64cdaa [4][1][1] = jCDAA  <9,3,3>;
        J64aacd [4][2][0] = jAACD  <9,5,1>;
        J64cdaa [4][2][0] = jCDAA  <9,5,1>;
        J64aacd [4][2][1] = jAACD  <9,5,3>;
        J64cdaa [4][2][1] = jCDAA  <9,5,3>;
        J64aacd [4][2][2] = jAACD  <9,5,5>;
        J64cdaa [4][2][2] = jCDAA  <9,5,5>;
        J64aacd [4][3][0] = jAACD  <9,7,1>;
        J64cdaa [4][3][0] = jCDAA  <9,7,1>;
        J64aacd [4][3][1] = jAACD  <9,7,3>;
        J64cdaa [4][3][1] = jCDAA  <9,7,3>;
        J64aacd [4][3][2] = jAACD  <9,7,5>;
        J64cdaa [4][3][2] = jCDAA  <9,7,5>;
        J64aacd [4][3][3] = jAACD  <9,7,7>;
        J64cdaa [4][3][3] = jCDAA  <9,7,7>;
        J64aacd [4][4][0] = jAACD  <9,9,1>;
        J64cdaa [4][4][0] = jCDAA  <9,9,1>;
        J64aacd [4][4][1] = jAACD  <9,9,3>;
        J64cdaa [4][4][1] = jCDAA  <9,9,3>;
        J64aacd [4][4][2] = jAACD  <9,9,5>;
        J64cdaa [4][4][2] = jCDAA  <9,9,5>;
        J64aacd [4][4][3] = jAACD  <9,9,7>;
        J64cdaa [4][4][3] = jCDAA  <9,9,7>;
        J64aacd [4][4][4] = jAACD  <9,9,9>;
        J64cdaa [4][4][4] = jCDAA  <9,9,9>;
    }

    //JXC SS
    {
        J64aacc [0][0] = jAACC  <1,1>;
        J64ccaa [0][0] = jCCAA  <1,1>;
        J64aacc [0][1] = jAACC  <1,3>;
        J64ccaa [0][1] = jCCAA  <1,3>;
        J64aacc [0][2] = jAACC  <1,5>;
        J64ccaa [0][2] = jCCAA  <1,5>;
        J64aacc [0][3] = jAACC  <1,7>;
        J64ccaa [0][3] = jCCAA  <1,7>;
        J64aacc [0][4] = jAACC  <1,9>;
        J64ccaa [0][4] = jCCAA  <1,9>;
        J64aacc [1][0] = jAACC  <3,1>;
        J64ccaa [1][0] = jCCAA  <3,1>;
        J64aacc [1][1] = jAACC  <3,3>;
        J64ccaa [1][1] = jCCAA  <3,3>;
        J64aacc [1][2] = jAACC  <3,5>;
        J64ccaa [1][2] = jCCAA  <3,5>;
        J64aacc [1][3] = jAACC  <3,7>;
        J64ccaa [1][3] = jCCAA  <3,7>;
        J64aacc [1][4] = jAACC  <3,9>;
        J64ccaa [1][4] = jCCAA  <3,9>;
        J64aacc [2][0] = jAACC  <5,1>;
        J64ccaa [2][0] = jCCAA  <5,1>;
        J64aacc [2][1] = jAACC  <5,3>;
        J64ccaa [2][1] = jCCAA  <5,3>;
        J64aacc [2][2] = jAACC  <5,5>;
        J64ccaa [2][2] = jCCAA  <5,5>;
        J64aacc [2][3] = jAACC  <5,7>;
        J64ccaa [2][3] = jCCAA  <5,7>;
        J64aacc [2][4] = jAACC  <5,9>;
        J64ccaa [2][4] = jCCAA  <5,9>;
        J64aacc [3][0] = jAACC  <7,1>;
        J64ccaa [3][0] = jCCAA  <7,1>;
        J64aacc [3][1] = jAACC  <7,3>;
        J64ccaa [3][1] = jCCAA  <7,3>;
        J64aacc [3][2] = jAACC  <7,5>;
        J64ccaa [3][2] = jCCAA  <7,5>;
        J64aacc [3][3] = jAACC  <7,7>;
        J64ccaa [3][3] = jCCAA  <7,7>;
        J64aacc [3][4] = jAACC  <7,9>;
        J64ccaa [3][4] = jCCAA  <7,9>;
        J64aacc [4][0] = jAACC  <9,1>;
        J64ccaa [4][0] = jCCAA  <9,1>;
        J64aacc [4][1] = jAACC  <9,3>;
        J64ccaa [4][1] = jCCAA  <9,3>;
        J64aacc [4][2] = jAACC  <9,5>;
        J64ccaa [4][2] = jCCAA  <9,5>;
        J64aacc [4][3] = jAACC  <9,7>;
        J64ccaa [4][3] = jCCAA  <9,7>;
        J64aacc [4][4] = jAACC  <9,9>;
        J64ccaa [4][4] = jCCAA  <9,9>;
    }

    //JXC S
    {
        J64abab [0][0] = jABCD  <1,1,1,1>;
        J64abab [1][0] = jABCD  <3,1,3,1>;
        J64abab [1][1] = jABCD  <3,3,3,3>;
        J64abab [2][0] = jABCD  <5,1,5,1>;
        J64abab [2][1] = jABCD  <5,3,5,3>;
        J64abab [2][2] = jABCD  <5,5,5,5>;
        J64abab [3][0] = jABCD  <7,1,7,1>;
        J64abab [3][1] = jABCD  <7,3,7,3>;
        J64abab [3][2] = jABCD  <7,5,7,5>;
        J64abab [3][3] = jABCD  <7,7,7,7>;
        J64abab [4][0] = jABCD  <9,1,9,1>;
        J64abab [4][1] = jABCD  <9,3,9,3>;
        J64abab [4][2] = jABCD  <9,5,9,5>;
        J64abab [4][3] = jABCD  <9,7,9,7>;
        J64abab [4][4] = jABCD  <9,9,9,9>;
    }
}
