#include "contractions.hpp"
#include "EFS/rotations.hpp"
#include "low/cacheN.hpp"

static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline int Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}



template <int maxLa, int maxLb, int maxLc, int maxLd> void jABCD(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLb; ++mb) {
            cachelineNB sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLd,mc,md);
                    sum += T[pos] * DD[cd];
                }
            }
            int ab = Tpos(maxLa,maxLb,ma,mb);
            FF[ab]+=sum*2;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void jCDAB(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLd; ++md) {
            cachelineNB sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLb; ++mb) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLb,ma,mb);
                    sum += T[pos] * DD[ab];
                }
            }
            int cd = Tpos(maxLc,maxLd,mc,md);
            FF[cd]+=sum*2;
        }
    }

}




template <int maxLa, int maxLc, int maxLd> void jAACD(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLa; ++mb) {
            cachelineNB sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLd, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLd,mc,md);
                    sum += T[pos] * DD[cd];
                }
            }
            int ab = Tpos(maxLa, maxLa, ma, mb);
            FF[ab]+=sum;
        }
    }

}

template <int maxLa, int maxLc, int maxLd> void jCDAA(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLd; ++md) {
            cachelineNB sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLa; ++mb) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLd, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLa,ma,mb);
                    sum += T[pos] * DD[ab];
                }
            }
            int cd = Tpos(maxLc,maxLd,mc,md);
            FF[cd]+=sum;
        }
    }

}


template <int maxLa, int maxLb, int maxLc> void jABCC(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLb; ++mb) {
            cachelineNB sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLc; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLc, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLc,mc,md);
                    sum += T[pos] * DD[cd];
                }
            }
            int ab = Tpos(maxLa,maxLb,ma,mb);
            FF[ab]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc> void jCCAB(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLc; ++md) {
            cachelineNB sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLb; ++mb) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLc, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLb,ma,mb);
                    sum += T[pos] * DD[ab];
                }
            }
            int cd = Tpos(maxLc,maxLc,mc,md);
            FF[cd]+=sum;
        }
    }

}


template <int maxLa, int maxLc> void jAACC(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLa; ++mb) {
            cachelineNB sum; sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLc; ++md) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLc, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLc,mc,md);
                    sum += T[pos] * DD[cd];
                }
            }
            int ab = Tpos(maxLa,maxLa,ma,mb);
            FF[ab]+=sum*0.5;
        }
    }

}

template <int maxLa, int maxLc> void jCCAA(cachelineNB * __restrict__ FF, const cachelineNB * __restrict__ T, const cachelineNB * __restrict__ DD) {

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLc; ++md) {
            cachelineNB sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLa; ++mb) {
                    int pos = Tpos (maxLa, maxLa, maxLc, maxLc, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLa,ma,mb);
                    sum += T[pos] * DD[ab];
                }
            }
            int cd = Tpos(maxLc,maxLc,mc,md);
            FF[cd]+=sum*0.5;
        }
    }

}

void DMD::LoadJN() {

    //JXC NN
    {
        JNabcd [0][0][0][0] = jABCD  <1,1,1,1>;
        JNcdab [0][0][0][0] = jCDAB  <1,1,1,1>;
        JNabcd [0][0][1][0] = jABCD  <1,1,3,1>;
        JNcdab [0][0][1][0] = jCDAB  <1,1,3,1>;
        JNabcd [0][0][1][1] = jABCD  <1,1,3,3>;
        JNcdab [0][0][1][1] = jCDAB  <1,1,3,3>;
        JNabcd [0][0][2][0] = jABCD  <1,1,5,1>;
        JNcdab [0][0][2][0] = jCDAB  <1,1,5,1>;
        JNabcd [0][0][2][1] = jABCD  <1,1,5,3>;
        JNcdab [0][0][2][1] = jCDAB  <1,1,5,3>;
        JNabcd [0][0][2][2] = jABCD  <1,1,5,5>;
        JNcdab [0][0][2][2] = jCDAB  <1,1,5,5>;
        JNabcd [0][0][3][0] = jABCD  <1,1,7,1>;
        JNcdab [0][0][3][0] = jCDAB  <1,1,7,1>;
        JNabcd [0][0][3][1] = jABCD  <1,1,7,3>;
        JNcdab [0][0][3][1] = jCDAB  <1,1,7,3>;
        JNabcd [0][0][3][2] = jABCD  <1,1,7,5>;
        JNcdab [0][0][3][2] = jCDAB  <1,1,7,5>;
        JNabcd [0][0][3][3] = jABCD  <1,1,7,7>;
        JNcdab [0][0][3][3] = jCDAB  <1,1,7,7>;
        JNabcd [0][0][4][0] = jABCD  <1,1,9,1>;
        JNcdab [0][0][4][0] = jCDAB  <1,1,9,1>;
        JNabcd [0][0][4][1] = jABCD  <1,1,9,3>;
        JNcdab [0][0][4][1] = jCDAB  <1,1,9,3>;
        JNabcd [0][0][4][2] = jABCD  <1,1,9,5>;
        JNcdab [0][0][4][2] = jCDAB  <1,1,9,5>;
        JNabcd [0][0][4][3] = jABCD  <1,1,9,7>;
        JNcdab [0][0][4][3] = jCDAB  <1,1,9,7>;
        JNabcd [0][0][4][4] = jABCD  <1,1,9,9>;
        JNcdab [0][0][4][4] = jCDAB  <1,1,9,9>;

        JNabcd [1][0][0][0] = jABCD  <3,1,1,1>;
        JNcdab [1][0][0][0] = jCDAB  <3,1,1,1>;
        JNabcd [1][0][1][0] = jABCD  <3,1,3,1>;
        JNcdab [1][0][1][0] = jCDAB  <3,1,3,1>;
        JNabcd [1][0][1][1] = jABCD  <3,1,3,3>;
        JNcdab [1][0][1][1] = jCDAB  <3,1,3,3>;
        JNabcd [1][0][2][0] = jABCD  <3,1,5,1>;
        JNcdab [1][0][2][0] = jCDAB  <3,1,5,1>;
        JNabcd [1][0][2][1] = jABCD  <3,1,5,3>;
        JNcdab [1][0][2][1] = jCDAB  <3,1,5,3>;
        JNabcd [1][0][2][2] = jABCD  <3,1,5,5>;
        JNcdab [1][0][2][2] = jCDAB  <3,1,5,5>;
        JNabcd [1][0][3][0] = jABCD  <3,1,7,1>;
        JNcdab [1][0][3][0] = jCDAB  <3,1,7,1>;
        JNabcd [1][0][3][1] = jABCD  <3,1,7,3>;
        JNcdab [1][0][3][1] = jCDAB  <3,1,7,3>;
        JNabcd [1][0][3][2] = jABCD  <3,1,7,5>;
        JNcdab [1][0][3][2] = jCDAB  <3,1,7,5>;
        JNabcd [1][0][3][3] = jABCD  <3,1,7,7>;
        JNcdab [1][0][3][3] = jCDAB  <3,1,7,7>;
        JNabcd [1][0][4][0] = jABCD  <3,1,9,1>;
        JNcdab [1][0][4][0] = jCDAB  <3,1,9,1>;
        JNabcd [1][0][4][1] = jABCD  <3,1,9,3>;
        JNcdab [1][0][4][1] = jCDAB  <3,1,9,3>;
        JNabcd [1][0][4][2] = jABCD  <3,1,9,5>;
        JNcdab [1][0][4][2] = jCDAB  <3,1,9,5>;
        JNabcd [1][0][4][3] = jABCD  <3,1,9,7>;
        JNcdab [1][0][4][3] = jCDAB  <3,1,9,7>;
        JNabcd [1][0][4][4] = jABCD  <3,1,9,9>;
        JNcdab [1][0][4][4] = jCDAB  <3,1,9,9>;

        JNabcd [1][1][0][0] = jABCD  <3,3,1,1>;
        JNcdab [1][1][0][0] = jCDAB  <3,3,1,1>;
        JNabcd [1][1][1][0] = jABCD  <3,3,3,1>;
        JNcdab [1][1][1][0] = jCDAB  <3,3,3,1>;
        JNabcd [1][1][1][1] = jABCD  <3,3,3,3>;
        JNcdab [1][1][1][1] = jCDAB  <3,3,3,3>;
        JNabcd [1][1][2][0] = jABCD  <3,3,5,1>;
        JNcdab [1][1][2][0] = jCDAB  <3,3,5,1>;
        JNabcd [1][1][2][1] = jABCD  <3,3,5,3>;
        JNcdab [1][1][2][1] = jCDAB  <3,3,5,3>;
        JNabcd [1][1][2][2] = jABCD  <3,3,5,5>;
        JNcdab [1][1][2][2] = jCDAB  <3,3,5,5>;
        JNabcd [1][1][3][0] = jABCD  <3,3,7,1>;
        JNcdab [1][1][3][0] = jCDAB  <3,3,7,1>;
        JNabcd [1][1][3][1] = jABCD  <3,3,7,3>;
        JNcdab [1][1][3][1] = jCDAB  <3,3,7,3>;
        JNabcd [1][1][3][2] = jABCD  <3,3,7,5>;
        JNcdab [1][1][3][2] = jCDAB  <3,3,7,5>;
        JNabcd [1][1][3][3] = jABCD  <3,3,7,7>;
        JNcdab [1][1][3][3] = jCDAB  <3,3,7,7>;
        JNabcd [1][1][4][0] = jABCD  <3,3,9,1>;
        JNcdab [1][1][4][0] = jCDAB  <3,3,9,1>;
        JNabcd [1][1][4][1] = jABCD  <3,3,9,3>;
        JNcdab [1][1][4][1] = jCDAB  <3,3,9,3>;
        JNabcd [1][1][4][2] = jABCD  <3,3,9,5>;
        JNcdab [1][1][4][2] = jCDAB  <3,3,9,5>;
        JNabcd [1][1][4][3] = jABCD  <3,3,9,7>;
        JNcdab [1][1][4][3] = jCDAB  <3,3,9,7>;
        JNabcd [1][1][4][4] = jABCD  <3,3,9,9>;
        JNcdab [1][1][4][4] = jCDAB  <3,3,9,9>;

        JNabcd [2][0][0][0] = jABCD  <5,1,1,1>;
        JNcdab [2][0][0][0] = jCDAB  <5,1,1,1>;
        JNabcd [2][0][1][0] = jABCD  <5,1,3,1>;
        JNcdab [2][0][1][0] = jCDAB  <5,1,3,1>;
        JNabcd [2][0][1][1] = jABCD  <5,1,3,3>;
        JNcdab [2][0][1][1] = jCDAB  <5,1,3,3>;
        JNabcd [2][0][2][0] = jABCD  <5,1,5,1>;
        JNcdab [2][0][2][0] = jCDAB  <5,1,5,1>;
        JNabcd [2][0][2][1] = jABCD  <5,1,5,3>;
        JNcdab [2][0][2][1] = jCDAB  <5,1,5,3>;
        JNabcd [2][0][2][2] = jABCD  <5,1,5,5>;
        JNcdab [2][0][2][2] = jCDAB  <5,1,5,5>;
        JNabcd [2][0][3][0] = jABCD  <5,1,7,1>;
        JNcdab [2][0][3][0] = jCDAB  <5,1,7,1>;
        JNabcd [2][0][3][1] = jABCD  <5,1,7,3>;
        JNcdab [2][0][3][1] = jCDAB  <5,1,7,3>;
        JNabcd [2][0][3][2] = jABCD  <5,1,7,5>;
        JNcdab [2][0][3][2] = jCDAB  <5,1,7,5>;
        JNabcd [2][0][3][3] = jABCD  <5,1,7,7>;
        JNcdab [2][0][3][3] = jCDAB  <5,1,7,7>;
        JNabcd [2][0][4][0] = jABCD  <5,1,9,1>;
        JNcdab [2][0][4][0] = jCDAB  <5,1,9,1>;
        JNabcd [2][0][4][1] = jABCD  <5,1,9,3>;
        JNcdab [2][0][4][1] = jCDAB  <5,1,9,3>;
        JNabcd [2][0][4][2] = jABCD  <5,1,9,5>;
        JNcdab [2][0][4][2] = jCDAB  <5,1,9,5>;
        JNabcd [2][0][4][3] = jABCD  <5,1,9,7>;
        JNcdab [2][0][4][3] = jCDAB  <5,1,9,7>;
        JNabcd [2][0][4][4] = jABCD  <5,1,9,9>;
        JNcdab [2][0][4][4] = jCDAB  <5,1,9,9>;

        JNabcd [2][1][0][0] = jABCD  <5,3,1,1>;
        JNcdab [2][1][0][0] = jCDAB  <5,3,1,1>;
        JNabcd [2][1][1][0] = jABCD  <5,3,3,1>;
        JNcdab [2][1][1][0] = jCDAB  <5,3,3,1>;
        JNabcd [2][1][1][1] = jABCD  <5,3,3,3>;
        JNcdab [2][1][1][1] = jCDAB  <5,3,3,3>;
        JNabcd [2][1][2][0] = jABCD  <5,3,5,1>;
        JNcdab [2][1][2][0] = jCDAB  <5,3,5,1>;
        JNabcd [2][1][2][1] = jABCD  <5,3,5,3>;
        JNcdab [2][1][2][1] = jCDAB  <5,3,5,3>;
        JNabcd [2][1][2][2] = jABCD  <5,3,5,5>;
        JNcdab [2][1][2][2] = jCDAB  <5,3,5,5>;
        JNabcd [2][1][3][0] = jABCD  <5,3,7,1>;
        JNcdab [2][1][3][0] = jCDAB  <5,3,7,1>;
        JNabcd [2][1][3][1] = jABCD  <5,3,7,3>;
        JNcdab [2][1][3][1] = jCDAB  <5,3,7,3>;
        JNabcd [2][1][3][2] = jABCD  <5,3,7,5>;
        JNcdab [2][1][3][2] = jCDAB  <5,3,7,5>;
        JNabcd [2][1][3][3] = jABCD  <5,3,7,7>;
        JNcdab [2][1][3][3] = jCDAB  <5,3,7,7>;
        JNabcd [2][1][4][0] = jABCD  <5,3,9,1>;
        JNcdab [2][1][4][0] = jCDAB  <5,3,9,1>;
        JNabcd [2][1][4][1] = jABCD  <5,3,9,3>;
        JNcdab [2][1][4][1] = jCDAB  <5,3,9,3>;
        JNabcd [2][1][4][2] = jABCD  <5,3,9,5>;
        JNcdab [2][1][4][2] = jCDAB  <5,3,9,5>;
        JNabcd [2][1][4][3] = jABCD  <5,3,9,7>;
        JNcdab [2][1][4][3] = jCDAB  <5,3,9,7>;
        JNabcd [2][1][4][4] = jABCD  <5,3,9,9>;
        JNcdab [2][1][4][4] = jCDAB  <5,3,9,9>;

        JNabcd [2][2][0][0] = jABCD  <5,5,1,1>;
        JNcdab [2][2][0][0] = jCDAB  <5,5,1,1>;
        JNabcd [2][2][1][0] = jABCD  <5,5,3,1>;
        JNcdab [2][2][1][0] = jCDAB  <5,5,3,1>;
        JNabcd [2][2][1][1] = jABCD  <5,5,3,3>;
        JNcdab [2][2][1][1] = jCDAB  <5,5,3,3>;
        JNabcd [2][2][2][0] = jABCD  <5,5,5,1>;
        JNcdab [2][2][2][0] = jCDAB  <5,5,5,1>;
        JNabcd [2][2][2][1] = jABCD  <5,5,5,3>;
        JNcdab [2][2][2][1] = jCDAB  <5,5,5,3>;
        JNabcd [2][2][2][2] = jABCD  <5,5,5,5>;
        JNcdab [2][2][2][2] = jCDAB  <5,5,5,5>;
        JNabcd [2][2][3][0] = jABCD  <5,5,7,1>;
        JNcdab [2][2][3][0] = jCDAB  <5,5,7,1>;
        JNabcd [2][2][3][1] = jABCD  <5,5,7,3>;
        JNcdab [2][2][3][1] = jCDAB  <5,5,7,3>;
        JNabcd [2][2][3][2] = jABCD  <5,5,7,5>;
        JNcdab [2][2][3][2] = jCDAB  <5,5,7,5>;
        JNabcd [2][2][3][3] = jABCD  <5,5,7,7>;
        JNcdab [2][2][3][3] = jCDAB  <5,5,7,7>;
        JNabcd [2][2][4][0] = jABCD  <5,5,9,1>;
        JNcdab [2][2][4][0] = jCDAB  <5,5,9,1>;
        JNabcd [2][2][4][1] = jABCD  <5,5,9,3>;
        JNcdab [2][2][4][1] = jCDAB  <5,5,9,3>;
        JNabcd [2][2][4][2] = jABCD  <5,5,9,5>;
        JNcdab [2][2][4][2] = jCDAB  <5,5,9,5>;
        JNabcd [2][2][4][3] = jABCD  <5,5,9,7>;
        JNcdab [2][2][4][3] = jCDAB  <5,5,9,7>;
        JNabcd [2][2][4][4] = jABCD  <5,5,9,9>;
        JNcdab [2][2][4][4] = jCDAB  <5,5,9,9>;

        JNabcd [3][0][0][0] = jABCD  <7,1,1,1>;
        JNcdab [3][0][0][0] = jCDAB  <7,1,1,1>;
        JNabcd [3][0][1][0] = jABCD  <7,1,3,1>;
        JNcdab [3][0][1][0] = jCDAB  <7,1,3,1>;
        JNabcd [3][0][1][1] = jABCD  <7,1,3,3>;
        JNcdab [3][0][1][1] = jCDAB  <7,1,3,3>;
        JNabcd [3][0][2][0] = jABCD  <7,1,5,1>;
        JNcdab [3][0][2][0] = jCDAB  <7,1,5,1>;
        JNabcd [3][0][2][1] = jABCD  <7,1,5,3>;
        JNcdab [3][0][2][1] = jCDAB  <7,1,5,3>;
        JNabcd [3][0][2][2] = jABCD  <7,1,5,5>;
        JNcdab [3][0][2][2] = jCDAB  <7,1,5,5>;
        JNabcd [3][0][3][0] = jABCD  <7,1,7,1>;
        JNcdab [3][0][3][0] = jCDAB  <7,1,7,1>;
        JNabcd [3][0][3][1] = jABCD  <7,1,7,3>;
        JNcdab [3][0][3][1] = jCDAB  <7,1,7,3>;
        JNabcd [3][0][3][2] = jABCD  <7,1,7,5>;
        JNcdab [3][0][3][2] = jCDAB  <7,1,7,5>;
        JNabcd [3][0][3][3] = jABCD  <7,1,7,7>;
        JNcdab [3][0][3][3] = jCDAB  <7,1,7,7>;
        JNabcd [3][0][4][0] = jABCD  <7,1,9,1>;
        JNcdab [3][0][4][0] = jCDAB  <7,1,9,1>;
        JNabcd [3][0][4][1] = jABCD  <7,1,9,3>;
        JNcdab [3][0][4][1] = jCDAB  <7,1,9,3>;
        JNabcd [3][0][4][2] = jABCD  <7,1,9,5>;
        JNcdab [3][0][4][2] = jCDAB  <7,1,9,5>;
        JNabcd [3][0][4][3] = jABCD  <7,1,9,7>;
        JNcdab [3][0][4][3] = jCDAB  <7,1,9,7>;
        JNabcd [3][0][4][4] = jABCD  <7,1,9,9>;
        JNcdab [3][0][4][4] = jCDAB  <7,1,9,9>;

        JNabcd [3][1][0][0] = jABCD  <7,3,1,1>;
        JNcdab [3][1][0][0] = jCDAB  <7,3,1,1>;
        JNabcd [3][1][1][0] = jABCD  <7,3,3,1>;
        JNcdab [3][1][1][0] = jCDAB  <7,3,3,1>;
        JNabcd [3][1][1][1] = jABCD  <7,3,3,3>;
        JNcdab [3][1][1][1] = jCDAB  <7,3,3,3>;
        JNabcd [3][1][2][0] = jABCD  <7,3,5,1>;
        JNcdab [3][1][2][0] = jCDAB  <7,3,5,1>;
        JNabcd [3][1][2][1] = jABCD  <7,3,5,3>;
        JNcdab [3][1][2][1] = jCDAB  <7,3,5,3>;
        JNabcd [3][1][2][2] = jABCD  <7,3,5,5>;
        JNcdab [3][1][2][2] = jCDAB  <7,3,5,5>;
        JNabcd [3][1][3][0] = jABCD  <7,3,7,1>;
        JNcdab [3][1][3][0] = jCDAB  <7,3,7,1>;
        JNabcd [3][1][3][1] = jABCD  <7,3,7,3>;
        JNcdab [3][1][3][1] = jCDAB  <7,3,7,3>;
        JNabcd [3][1][3][2] = jABCD  <7,3,7,5>;
        JNcdab [3][1][3][2] = jCDAB  <7,3,7,5>;
        JNabcd [3][1][3][3] = jABCD  <7,3,7,7>;
        JNcdab [3][1][3][3] = jCDAB  <7,3,7,7>;
        JNabcd [3][1][4][0] = jABCD  <7,3,9,1>;
        JNcdab [3][1][4][0] = jCDAB  <7,3,9,1>;
        JNabcd [3][1][4][1] = jABCD  <7,3,9,3>;
        JNcdab [3][1][4][1] = jCDAB  <7,3,9,3>;
        JNabcd [3][1][4][2] = jABCD  <7,3,9,5>;
        JNcdab [3][1][4][2] = jCDAB  <7,3,9,5>;
        JNabcd [3][1][4][3] = jABCD  <7,3,9,7>;
        JNcdab [3][1][4][3] = jCDAB  <7,3,9,7>;
        JNabcd [3][1][4][4] = jABCD  <7,3,9,9>;
        JNcdab [3][1][4][4] = jCDAB  <7,3,9,9>;

        JNabcd [3][2][0][0] = jABCD  <7,5,1,1>;
        JNcdab [3][2][0][0] = jCDAB  <7,5,1,1>;
        JNabcd [3][2][1][0] = jABCD  <7,5,3,1>;
        JNcdab [3][2][1][0] = jCDAB  <7,5,3,1>;
        JNabcd [3][2][1][1] = jABCD  <7,5,3,3>;
        JNcdab [3][2][1][1] = jCDAB  <7,5,3,3>;
        JNabcd [3][2][2][0] = jABCD  <7,5,5,1>;
        JNcdab [3][2][2][0] = jCDAB  <7,5,5,1>;
        JNabcd [3][2][2][1] = jABCD  <7,5,5,3>;
        JNcdab [3][2][2][1] = jCDAB  <7,5,5,3>;
        JNabcd [3][2][2][2] = jABCD  <7,5,5,5>;
        JNcdab [3][2][2][2] = jCDAB  <7,5,5,5>;
        JNabcd [3][2][3][0] = jABCD  <7,5,7,1>;
        JNcdab [3][2][3][0] = jCDAB  <7,5,7,1>;
        JNabcd [3][2][3][1] = jABCD  <7,5,7,3>;
        JNcdab [3][2][3][1] = jCDAB  <7,5,7,3>;
        JNabcd [3][2][3][2] = jABCD  <7,5,7,5>;
        JNcdab [3][2][3][2] = jCDAB  <7,5,7,5>;
        JNabcd [3][2][3][3] = jABCD  <7,5,7,7>;
        JNcdab [3][2][3][3] = jCDAB  <7,5,7,7>;
        JNabcd [3][2][4][0] = jABCD  <7,5,9,1>;
        JNcdab [3][2][4][0] = jCDAB  <7,5,9,1>;
        JNabcd [3][2][4][1] = jABCD  <7,5,9,3>;
        JNcdab [3][2][4][1] = jCDAB  <7,5,9,3>;
        JNabcd [3][2][4][2] = jABCD  <7,5,9,5>;
        JNcdab [3][2][4][2] = jCDAB  <7,5,9,5>;
        JNabcd [3][2][4][3] = jABCD  <7,5,9,7>;
        JNcdab [3][2][4][3] = jCDAB  <7,5,9,7>;
        JNabcd [3][2][4][4] = jABCD  <7,5,9,9>;
        JNcdab [3][2][4][4] = jCDAB  <7,5,9,9>;

        JNabcd [3][3][0][0] = jABCD  <7,7,1,1>;
        JNcdab [3][3][0][0] = jCDAB  <7,7,1,1>;
        JNabcd [3][3][1][0] = jABCD  <7,7,3,1>;
        JNcdab [3][3][1][0] = jCDAB  <7,7,3,1>;
        JNabcd [3][3][1][1] = jABCD  <7,7,3,3>;
        JNcdab [3][3][1][1] = jCDAB  <7,7,3,3>;
        JNabcd [3][3][2][0] = jABCD  <7,7,5,1>;
        JNcdab [3][3][2][0] = jCDAB  <7,7,5,1>;
        JNabcd [3][3][2][1] = jABCD  <7,7,5,3>;
        JNcdab [3][3][2][1] = jCDAB  <7,7,5,3>;
        JNabcd [3][3][2][2] = jABCD  <7,7,5,5>;
        JNcdab [3][3][2][2] = jCDAB  <7,7,5,5>;
        JNabcd [3][3][3][0] = jABCD  <7,7,7,1>;
        JNcdab [3][3][3][0] = jCDAB  <7,7,7,1>;
        JNabcd [3][3][3][1] = jABCD  <7,7,7,3>;
        JNcdab [3][3][3][1] = jCDAB  <7,7,7,3>;
        JNabcd [3][3][3][2] = jABCD  <7,7,7,5>;
        JNcdab [3][3][3][2] = jCDAB  <7,7,7,5>;
        JNabcd [3][3][3][3] = jABCD  <7,7,7,7>;
        JNcdab [3][3][3][3] = jCDAB  <7,7,7,7>;
        JNabcd [3][3][4][0] = jABCD  <7,7,9,1>;
        JNcdab [3][3][4][0] = jCDAB  <7,7,9,1>;
        JNabcd [3][3][4][1] = jABCD  <7,7,9,3>;
        JNcdab [3][3][4][1] = jCDAB  <7,7,9,3>;
        JNabcd [3][3][4][2] = jABCD  <7,7,9,5>;
        JNcdab [3][3][4][2] = jCDAB  <7,7,9,5>;
        JNabcd [3][3][4][3] = jABCD  <7,7,9,7>;
        JNcdab [3][3][4][3] = jCDAB  <7,7,9,7>;
        JNabcd [3][3][4][4] = jABCD  <7,7,9,9>;
        JNcdab [3][3][4][4] = jCDAB  <7,7,9,9>;

        JNabcd [4][0][0][0] = jABCD  <9,1,1,1>;
        JNcdab [4][0][0][0] = jCDAB  <9,1,1,1>;
        JNabcd [4][0][1][0] = jABCD  <9,1,3,1>;
        JNcdab [4][0][1][0] = jCDAB  <9,1,3,1>;
        JNabcd [4][0][1][1] = jABCD  <9,1,3,3>;
        JNcdab [4][0][1][1] = jCDAB  <9,1,3,3>;
        JNabcd [4][0][2][0] = jABCD  <9,1,5,1>;
        JNcdab [4][0][2][0] = jCDAB  <9,1,5,1>;
        JNabcd [4][0][2][1] = jABCD  <9,1,5,3>;
        JNcdab [4][0][2][1] = jCDAB  <9,1,5,3>;
        JNabcd [4][0][2][2] = jABCD  <9,1,5,5>;
        JNcdab [4][0][2][2] = jCDAB  <9,1,5,5>;
        JNabcd [4][0][3][0] = jABCD  <9,1,7,1>;
        JNcdab [4][0][3][0] = jCDAB  <9,1,7,1>;
        JNabcd [4][0][3][1] = jABCD  <9,1,7,3>;
        JNcdab [4][0][3][1] = jCDAB  <9,1,7,3>;
        JNabcd [4][0][3][2] = jABCD  <9,1,7,5>;
        JNcdab [4][0][3][2] = jCDAB  <9,1,7,5>;
        JNabcd [4][0][3][3] = jABCD  <9,1,7,7>;
        JNcdab [4][0][3][3] = jCDAB  <9,1,7,7>;
        JNabcd [4][0][4][0] = jABCD  <9,1,9,1>;
        JNcdab [4][0][4][0] = jCDAB  <9,1,9,1>;
        JNabcd [4][0][4][1] = jABCD  <9,1,9,3>;
        JNcdab [4][0][4][1] = jCDAB  <9,1,9,3>;
        JNabcd [4][0][4][2] = jABCD  <9,1,9,5>;
        JNcdab [4][0][4][2] = jCDAB  <9,1,9,5>;
        JNabcd [4][0][4][3] = jABCD  <9,1,9,7>;
        JNcdab [4][0][4][3] = jCDAB  <9,1,9,7>;
        JNabcd [4][0][4][4] = jABCD  <9,1,9,9>;
        JNcdab [4][0][4][4] = jCDAB  <9,1,9,9>;

        JNabcd [4][1][0][0] = jABCD  <9,3,1,1>;
        JNcdab [4][1][0][0] = jCDAB  <9,3,1,1>;
        JNabcd [4][1][1][0] = jABCD  <9,3,3,1>;
        JNcdab [4][1][1][0] = jCDAB  <9,3,3,1>;
        JNabcd [4][1][1][1] = jABCD  <9,3,3,3>;
        JNcdab [4][1][1][1] = jCDAB  <9,3,3,3>;
        JNabcd [4][1][2][0] = jABCD  <9,3,5,1>;
        JNcdab [4][1][2][0] = jCDAB  <9,3,5,1>;
        JNabcd [4][1][2][1] = jABCD  <9,3,5,3>;
        JNcdab [4][1][2][1] = jCDAB  <9,3,5,3>;
        JNabcd [4][1][2][2] = jABCD  <9,3,5,5>;
        JNcdab [4][1][2][2] = jCDAB  <9,3,5,5>;
        JNabcd [4][1][3][0] = jABCD  <9,3,7,1>;
        JNcdab [4][1][3][0] = jCDAB  <9,3,7,1>;
        JNabcd [4][1][3][1] = jABCD  <9,3,7,3>;
        JNcdab [4][1][3][1] = jCDAB  <9,3,7,3>;
        JNabcd [4][1][3][2] = jABCD  <9,3,7,5>;
        JNcdab [4][1][3][2] = jCDAB  <9,3,7,5>;
        JNabcd [4][1][3][3] = jABCD  <9,3,7,7>;
        JNcdab [4][1][3][3] = jCDAB  <9,3,7,7>;
        JNabcd [4][1][4][0] = jABCD  <9,3,9,1>;
        JNcdab [4][1][4][0] = jCDAB  <9,3,9,1>;
        JNabcd [4][1][4][1] = jABCD  <9,3,9,3>;
        JNcdab [4][1][4][1] = jCDAB  <9,3,9,3>;
        JNabcd [4][1][4][2] = jABCD  <9,3,9,5>;
        JNcdab [4][1][4][2] = jCDAB  <9,3,9,5>;
        JNabcd [4][1][4][3] = jABCD  <9,3,9,7>;
        JNcdab [4][1][4][3] = jCDAB  <9,3,9,7>;
        JNabcd [4][1][4][4] = jABCD  <9,3,9,9>;
        JNcdab [4][1][4][4] = jCDAB  <9,3,9,9>;

        JNabcd [4][2][0][0] = jABCD  <9,5,1,1>;
        JNcdab [4][2][0][0] = jCDAB  <9,5,1,1>;
        JNabcd [4][2][1][0] = jABCD  <9,5,3,1>;
        JNcdab [4][2][1][0] = jCDAB  <9,5,3,1>;
        JNabcd [4][2][1][1] = jABCD  <9,5,3,3>;
        JNcdab [4][2][1][1] = jCDAB  <9,5,3,3>;
        JNabcd [4][2][2][0] = jABCD  <9,5,5,1>;
        JNcdab [4][2][2][0] = jCDAB  <9,5,5,1>;
        JNabcd [4][2][2][1] = jABCD  <9,5,5,3>;
        JNcdab [4][2][2][1] = jCDAB  <9,5,5,3>;
        JNabcd [4][2][2][2] = jABCD  <9,5,5,5>;
        JNcdab [4][2][2][2] = jCDAB  <9,5,5,5>;
        JNabcd [4][2][3][0] = jABCD  <9,5,7,1>;
        JNcdab [4][2][3][0] = jCDAB  <9,5,7,1>;
        JNabcd [4][2][3][1] = jABCD  <9,5,7,3>;
        JNcdab [4][2][3][1] = jCDAB  <9,5,7,3>;
        JNabcd [4][2][3][2] = jABCD  <9,5,7,5>;
        JNcdab [4][2][3][2] = jCDAB  <9,5,7,5>;
        JNabcd [4][2][3][3] = jABCD  <9,5,7,7>;
        JNcdab [4][2][3][3] = jCDAB  <9,5,7,7>;
        JNabcd [4][2][4][0] = jABCD  <9,5,9,1>;
        JNcdab [4][2][4][0] = jCDAB  <9,5,9,1>;
        JNabcd [4][2][4][1] = jABCD  <9,5,9,3>;
        JNcdab [4][2][4][1] = jCDAB  <9,5,9,3>;
        JNabcd [4][2][4][2] = jABCD  <9,5,9,5>;
        JNcdab [4][2][4][2] = jCDAB  <9,5,9,5>;
        JNabcd [4][2][4][3] = jABCD  <9,5,9,7>;
        JNcdab [4][2][4][3] = jCDAB  <9,5,9,7>;
        JNabcd [4][2][4][4] = jABCD  <9,5,9,9>;
        JNcdab [4][2][4][4] = jCDAB  <9,5,9,9>;

        JNabcd [4][3][0][0] = jABCD  <9,7,1,1>;
        JNcdab [4][3][0][0] = jCDAB  <9,7,1,1>;
        JNabcd [4][3][1][0] = jABCD  <9,7,3,1>;
        JNcdab [4][3][1][0] = jCDAB  <9,7,3,1>;
        JNabcd [4][3][1][1] = jABCD  <9,7,3,3>;
        JNcdab [4][3][1][1] = jCDAB  <9,7,3,3>;
        JNabcd [4][3][2][0] = jABCD  <9,7,5,1>;
        JNcdab [4][3][2][0] = jCDAB  <9,7,5,1>;
        JNabcd [4][3][2][1] = jABCD  <9,7,5,3>;
        JNcdab [4][3][2][1] = jCDAB  <9,7,5,3>;
        JNabcd [4][3][2][2] = jABCD  <9,7,5,5>;
        JNcdab [4][3][2][2] = jCDAB  <9,7,5,5>;
        JNabcd [4][3][3][0] = jABCD  <9,7,7,1>;
        JNcdab [4][3][3][0] = jCDAB  <9,7,7,1>;
        JNabcd [4][3][3][1] = jABCD  <9,7,7,3>;
        JNcdab [4][3][3][1] = jCDAB  <9,7,7,3>;
        JNabcd [4][3][3][2] = jABCD  <9,7,7,5>;
        JNcdab [4][3][3][2] = jCDAB  <9,7,7,5>;
        JNabcd [4][3][3][3] = jABCD  <9,7,7,7>;
        JNcdab [4][3][3][3] = jCDAB  <9,7,7,7>;
        JNabcd [4][3][4][0] = jABCD  <9,7,9,1>;
        JNcdab [4][3][4][0] = jCDAB  <9,7,9,1>;
        JNabcd [4][3][4][1] = jABCD  <9,7,9,3>;
        JNcdab [4][3][4][1] = jCDAB  <9,7,9,3>;
        JNabcd [4][3][4][2] = jABCD  <9,7,9,5>;
        JNcdab [4][3][4][2] = jCDAB  <9,7,9,5>;
        JNabcd [4][3][4][3] = jABCD  <9,7,9,7>;
        JNcdab [4][3][4][3] = jCDAB  <9,7,9,7>;
        JNabcd [4][3][4][4] = jABCD  <9,7,9,9>;
        JNcdab [4][3][4][4] = jCDAB  <9,7,9,9>;

        JNabcd [4][4][0][0] = jABCD  <9,9,1,1>;
        JNcdab [4][4][0][0] = jCDAB  <9,9,1,1>;
        JNabcd [4][4][1][0] = jABCD  <9,9,3,1>;
        JNcdab [4][4][1][0] = jCDAB  <9,9,3,1>;
        JNabcd [4][4][1][1] = jABCD  <9,9,3,3>;
        JNcdab [4][4][1][1] = jCDAB  <9,9,3,3>;
        JNabcd [4][4][2][0] = jABCD  <9,9,5,1>;
        JNcdab [4][4][2][0] = jCDAB  <9,9,5,1>;
        JNabcd [4][4][2][1] = jABCD  <9,9,5,3>;
        JNcdab [4][4][2][1] = jCDAB  <9,9,5,3>;
        JNabcd [4][4][2][2] = jABCD  <9,9,5,5>;
        JNcdab [4][4][2][2] = jCDAB  <9,9,5,5>;
        JNabcd [4][4][3][0] = jABCD  <9,9,7,1>;
        JNcdab [4][4][3][0] = jCDAB  <9,9,7,1>;
        JNabcd [4][4][3][1] = jABCD  <9,9,7,3>;
        JNcdab [4][4][3][1] = jCDAB  <9,9,7,3>;
        JNabcd [4][4][3][2] = jABCD  <9,9,7,5>;
        JNcdab [4][4][3][2] = jCDAB  <9,9,7,5>;
        JNabcd [4][4][3][3] = jABCD  <9,9,7,7>;
        JNcdab [4][4][3][3] = jCDAB  <9,9,7,7>;
        JNabcd [4][4][4][0] = jABCD  <9,9,9,1>;
        JNcdab [4][4][4][0] = jCDAB  <9,9,9,1>;
        JNabcd [4][4][4][1] = jABCD  <9,9,9,3>;
        JNcdab [4][4][4][1] = jCDAB  <9,9,9,3>;
        JNabcd [4][4][4][2] = jABCD  <9,9,9,5>;
        JNcdab [4][4][4][2] = jCDAB  <9,9,9,5>;
        JNabcd [4][4][4][3] = jABCD  <9,9,9,7>;
        JNcdab [4][4][4][3] = jCDAB  <9,9,9,7>;
        JNabcd [4][4][4][4] = jABCD  <9,9,9,9>;
        JNcdab [4][4][4][4] = jCDAB  <9,9,9,9>;
    }

    //JXC NS
    {
        JNabcc [0][0][0] = jABCC  <1,1,1>;
        JNccab [0][0][0] = jCCAB  <1,1,1>;
        JNabcc [0][0][1] = jABCC  <1,1,3>;
        JNccab [0][0][1] = jCCAB  <1,1,3>;
        JNabcc [0][0][2] = jABCC  <1,1,5>;
        JNccab [0][0][2] = jCCAB  <1,1,5>;
        JNabcc [0][0][3] = jABCC  <1,1,7>;
        JNccab [0][0][3] = jCCAB  <1,1,7>;
        JNabcc [0][0][4] = jABCC  <1,1,9>;
        JNccab [0][0][4] = jCCAB  <1,1,9>;

        JNabcc [1][0][0] = jABCC  <3,1,1>;
        JNccab [1][0][0] = jCCAB  <3,1,1>;
        JNabcc [1][0][1] = jABCC  <3,1,3>;
        JNccab [1][0][1] = jCCAB  <3,1,3>;
        JNabcc [1][0][2] = jABCC  <3,1,5>;
        JNccab [1][0][2] = jCCAB  <3,1,5>;
        JNabcc [1][0][3] = jABCC  <3,1,7>;
        JNccab [1][0][3] = jCCAB  <3,1,7>;
        JNabcc [1][0][4] = jABCC  <3,1,9>;
        JNccab [1][0][4] = jCCAB  <3,1,9>;

        JNabcc [1][1][0] = jABCC  <3,3,1>;
        JNccab [1][1][0] = jCCAB  <3,3,1>;
        JNabcc [1][1][1] = jABCC  <3,3,3>;
        JNccab [1][1][1] = jCCAB  <3,3,3>;
        JNabcc [1][1][2] = jABCC  <3,3,5>;
        JNccab [1][1][2] = jCCAB  <3,3,5>;
        JNabcc [1][1][3] = jABCC  <3,3,7>;
        JNccab [1][1][3] = jCCAB  <3,3,7>;
        JNabcc [1][1][4] = jABCC  <3,3,9>;
        JNccab [1][1][4] = jCCAB  <3,3,9>;

        JNabcc [2][0][0] = jABCC  <5,1,1>;
        JNccab [2][0][0] = jCCAB  <5,1,1>;
        JNabcc [2][0][1] = jABCC  <5,1,3>;
        JNccab [2][0][1] = jCCAB  <5,1,3>;
        JNabcc [2][0][2] = jABCC  <5,1,5>;
        JNccab [2][0][2] = jCCAB  <5,1,5>;
        JNabcc [2][0][3] = jABCC  <5,1,7>;
        JNccab [2][0][3] = jCCAB  <5,1,7>;
        JNabcc [2][0][4] = jABCC  <5,1,9>;
        JNccab [2][0][4] = jCCAB  <5,1,9>;

        JNabcc [2][1][0] = jABCC  <5,3,1>;
        JNccab [2][1][0] = jCCAB  <5,3,1>;
        JNabcc [2][1][1] = jABCC  <5,3,3>;
        JNccab [2][1][1] = jCCAB  <5,3,3>;
        JNabcc [2][1][2] = jABCC  <5,3,5>;
        JNccab [2][1][2] = jCCAB  <5,3,5>;
        JNabcc [2][1][3] = jABCC  <5,3,7>;
        JNccab [2][1][3] = jCCAB  <5,3,7>;
        JNabcc [2][1][4] = jABCC  <5,3,9>;
        JNccab [2][1][4] = jCCAB  <5,3,9>;

        JNabcc [2][2][0] = jABCC  <5,5,1>;
        JNccab [2][2][0] = jCCAB  <5,5,1>;
        JNabcc [2][2][1] = jABCC  <5,5,3>;
        JNccab [2][2][1] = jCCAB  <5,5,3>;
        JNabcc [2][2][2] = jABCC  <5,5,5>;
        JNccab [2][2][2] = jCCAB  <5,5,5>;
        JNabcc [2][2][3] = jABCC  <5,5,7>;
        JNccab [2][2][3] = jCCAB  <5,5,7>;
        JNabcc [2][2][4] = jABCC  <5,5,9>;
        JNccab [2][2][4] = jCCAB  <5,5,9>;

        JNabcc [3][0][0] = jABCC  <7,1,1>;
        JNccab [3][0][0] = jCCAB  <7,1,1>;
        JNabcc [3][0][1] = jABCC  <7,1,3>;
        JNccab [3][0][1] = jCCAB  <7,1,3>;
        JNabcc [3][0][2] = jABCC  <7,1,5>;
        JNccab [3][0][2] = jCCAB  <7,1,5>;
        JNabcc [3][0][3] = jABCC  <7,1,7>;
        JNccab [3][0][3] = jCCAB  <7,1,7>;
        JNabcc [3][0][4] = jABCC  <7,1,9>;
        JNccab [3][0][4] = jCCAB  <7,1,9>;

        JNabcc [3][1][0] = jABCC  <7,3,1>;
        JNccab [3][1][0] = jCCAB  <7,3,1>;
        JNabcc [3][1][1] = jABCC  <7,3,3>;
        JNccab [3][1][1] = jCCAB  <7,3,3>;
        JNabcc [3][1][2] = jABCC  <7,3,5>;
        JNccab [3][1][2] = jCCAB  <7,3,5>;
        JNabcc [3][1][3] = jABCC  <7,3,7>;
        JNccab [3][1][3] = jCCAB  <7,3,7>;
        JNabcc [3][1][4] = jABCC  <7,3,9>;
        JNccab [3][1][4] = jCCAB  <7,3,9>;

        JNabcc [3][2][0] = jABCC  <7,5,1>;
        JNccab [3][2][0] = jCCAB  <7,5,1>;
        JNabcc [3][2][1] = jABCC  <7,5,3>;
        JNccab [3][2][1] = jCCAB  <7,5,3>;
        JNabcc [3][2][2] = jABCC  <7,5,5>;
        JNccab [3][2][2] = jCCAB  <7,5,5>;
        JNabcc [3][2][3] = jABCC  <7,5,7>;
        JNccab [3][2][3] = jCCAB  <7,5,7>;
        JNabcc [3][2][4] = jABCC  <7,5,9>;
        JNccab [3][2][4] = jCCAB  <7,5,9>;

        JNabcc [3][3][0] = jABCC  <7,7,1>;
        JNccab [3][3][0] = jCCAB  <7,7,1>;
        JNabcc [3][3][1] = jABCC  <7,7,3>;
        JNccab [3][3][1] = jCCAB  <7,7,3>;
        JNabcc [3][3][2] = jABCC  <7,7,5>;
        JNccab [3][3][2] = jCCAB  <7,7,5>;
        JNabcc [3][3][3] = jABCC  <7,7,7>;
        JNccab [3][3][3] = jCCAB  <7,7,7>;
        JNabcc [3][3][4] = jABCC  <7,7,9>;
        JNccab [3][3][4] = jCCAB  <7,7,9>;

        JNabcc [4][0][0] = jABCC  <9,1,1>;
        JNccab [4][0][0] = jCCAB  <9,1,1>;
        JNabcc [4][0][1] = jABCC  <9,1,3>;
        JNccab [4][0][1] = jCCAB  <9,1,3>;
        JNabcc [4][0][2] = jABCC  <9,1,5>;
        JNccab [4][0][2] = jCCAB  <9,1,5>;
        JNabcc [4][0][3] = jABCC  <9,1,7>;
        JNccab [4][0][3] = jCCAB  <9,1,7>;
        JNabcc [4][0][4] = jABCC  <9,1,9>;
        JNccab [4][0][4] = jCCAB  <9,1,9>;

        JNabcc [4][1][0] = jABCC  <9,3,1>;
        JNccab [4][1][0] = jCCAB  <9,3,1>;
        JNabcc [4][1][1] = jABCC  <9,3,3>;
        JNccab [4][1][1] = jCCAB  <9,3,3>;
        JNabcc [4][1][2] = jABCC  <9,3,5>;
        JNccab [4][1][2] = jCCAB  <9,3,5>;
        JNabcc [4][1][3] = jABCC  <9,3,7>;
        JNccab [4][1][3] = jCCAB  <9,3,7>;
        JNabcc [4][1][4] = jABCC  <9,3,9>;
        JNccab [4][1][4] = jCCAB  <9,3,9>;

        JNabcc [4][2][0] = jABCC  <9,5,1>;
        JNccab [4][2][0] = jCCAB  <9,5,1>;
        JNabcc [4][2][1] = jABCC  <9,5,3>;
        JNccab [4][2][1] = jCCAB  <9,5,3>;
        JNabcc [4][2][2] = jABCC  <9,5,5>;
        JNccab [4][2][2] = jCCAB  <9,5,5>;
        JNabcc [4][2][3] = jABCC  <9,5,7>;
        JNccab [4][2][3] = jCCAB  <9,5,7>;
        JNabcc [4][2][4] = jABCC  <9,5,9>;
        JNccab [4][2][4] = jCCAB  <9,5,9>;

        JNabcc [4][3][0] = jABCC  <9,7,1>;
        JNccab [4][3][0] = jCCAB  <9,7,1>;
        JNabcc [4][3][1] = jABCC  <9,7,3>;
        JNccab [4][3][1] = jCCAB  <9,7,3>;
        JNabcc [4][3][2] = jABCC  <9,7,5>;
        JNccab [4][3][2] = jCCAB  <9,7,5>;
        JNabcc [4][3][3] = jABCC  <9,7,7>;
        JNccab [4][3][3] = jCCAB  <9,7,7>;
        JNabcc [4][3][4] = jABCC  <9,7,9>;
        JNccab [4][3][4] = jCCAB  <9,7,9>;
        JNabcc [4][4][0] = jABCC  <9,9,1>;
        JNccab [4][4][0] = jCCAB  <9,9,1>;
        JNabcc [4][4][1] = jABCC  <9,9,3>;
        JNccab [4][4][1] = jCCAB  <9,9,3>;
        JNabcc [4][4][2] = jABCC  <9,9,5>;
        JNccab [4][4][2] = jCCAB  <9,9,5>;
        JNabcc [4][4][3] = jABCC  <9,9,7>;
        JNccab [4][4][3] = jCCAB  <9,9,7>;
        JNabcc [4][4][4] = jABCC  <9,9,9>;
        JNccab [4][4][4] = jCCAB  <9,9,9>;
    }

    //JXC SN
    {
        JNaacd [0][0][0] = jAACD  <1,1,1>;
        JNcdaa [0][0][0] = jCDAA  <1,1,1>;
        JNaacd [0][1][0] = jAACD  <1,3,1>;
        JNcdaa [0][1][0] = jCDAA  <1,3,1>;
        JNaacd [0][1][1] = jAACD  <1,3,3>;
        JNcdaa [0][1][1] = jCDAA  <1,3,3>;
        JNaacd [0][2][0] = jAACD  <1,5,1>;
        JNcdaa [0][2][0] = jCDAA  <1,5,1>;
        JNaacd [0][2][1] = jAACD  <1,5,3>;
        JNcdaa [0][2][1] = jCDAA  <1,5,3>;
        JNaacd [0][2][2] = jAACD  <1,5,5>;
        JNcdaa [0][2][2] = jCDAA  <1,5,5>;
        JNaacd [0][3][0] = jAACD  <1,7,1>;
        JNcdaa [0][3][0] = jCDAA  <1,7,1>;
        JNaacd [0][3][1] = jAACD  <1,7,3>;
        JNcdaa [0][3][1] = jCDAA  <1,7,3>;
        JNaacd [0][3][2] = jAACD  <1,7,5>;
        JNcdaa [0][3][2] = jCDAA  <1,7,5>;
        JNaacd [0][3][3] = jAACD  <1,7,7>;
        JNcdaa [0][3][3] = jCDAA  <1,7,7>;
        JNaacd [0][4][0] = jAACD  <1,9,1>;
        JNcdaa [0][4][0] = jCDAA  <1,9,1>;
        JNaacd [0][4][1] = jAACD  <1,9,3>;
        JNcdaa [0][4][1] = jCDAA  <1,9,3>;
        JNaacd [0][4][2] = jAACD  <1,9,5>;
        JNcdaa [0][4][2] = jCDAA  <1,9,5>;
        JNaacd [0][4][3] = jAACD  <1,9,7>;
        JNcdaa [0][4][3] = jCDAA  <1,9,7>;
        JNaacd [0][4][4] = jAACD  <1,9,9>;
        JNcdaa [0][4][4] = jCDAA  <1,9,9>;
        JNaacd [1][0][0] = jAACD  <3,1,1>;
        JNcdaa [1][0][0] = jCDAA  <3,1,1>;
        JNaacd [1][1][0] = jAACD  <3,3,1>;
        JNcdaa [1][1][0] = jCDAA  <3,3,1>;
        JNaacd [1][1][1] = jAACD  <3,3,3>;
        JNcdaa [1][1][1] = jCDAA  <3,3,3>;
        JNaacd [1][2][0] = jAACD  <3,5,1>;
        JNcdaa [1][2][0] = jCDAA  <3,5,1>;
        JNaacd [1][2][1] = jAACD  <3,5,3>;
        JNcdaa [1][2][1] = jCDAA  <3,5,3>;
        JNaacd [1][2][2] = jAACD  <3,5,5>;
        JNcdaa [1][2][2] = jCDAA  <3,5,5>;
        JNaacd [1][3][0] = jAACD  <3,7,1>;
        JNcdaa [1][3][0] = jCDAA  <3,7,1>;
        JNaacd [1][3][1] = jAACD  <3,7,3>;
        JNcdaa [1][3][1] = jCDAA  <3,7,3>;
        JNaacd [1][3][2] = jAACD  <3,7,5>;
        JNcdaa [1][3][2] = jCDAA  <3,7,5>;
        JNaacd [1][3][3] = jAACD  <3,7,7>;
        JNcdaa [1][3][3] = jCDAA  <3,7,7>;
        JNaacd [1][4][0] = jAACD  <3,9,1>;
        JNcdaa [1][4][0] = jCDAA  <3,9,1>;
        JNaacd [1][4][1] = jAACD  <3,9,3>;
        JNcdaa [1][4][1] = jCDAA  <3,9,3>;
        JNaacd [1][4][2] = jAACD  <3,9,5>;
        JNcdaa [1][4][2] = jCDAA  <3,9,5>;
        JNaacd [1][4][3] = jAACD  <3,9,7>;
        JNcdaa [1][4][3] = jCDAA  <3,9,7>;
        JNaacd [1][4][4] = jAACD  <3,9,9>;
        JNcdaa [1][4][4] = jCDAA  <3,9,9>;
        JNaacd [2][0][0] = jAACD  <5,1,1>;
        JNcdaa [2][0][0] = jCDAA  <5,1,1>;
        JNaacd [2][1][0] = jAACD  <5,3,1>;
        JNcdaa [2][1][0] = jCDAA  <5,3,1>;
        JNaacd [2][1][1] = jAACD  <5,3,3>;
        JNcdaa [2][1][1] = jCDAA  <5,3,3>;
        JNaacd [2][2][0] = jAACD  <5,5,1>;
        JNcdaa [2][2][0] = jCDAA  <5,5,1>;
        JNaacd [2][2][1] = jAACD  <5,5,3>;
        JNcdaa [2][2][1] = jCDAA  <5,5,3>;
        JNaacd [2][2][2] = jAACD  <5,5,5>;
        JNcdaa [2][2][2] = jCDAA  <5,5,5>;
        JNaacd [2][3][0] = jAACD  <5,7,1>;
        JNcdaa [2][3][0] = jCDAA  <5,7,1>;
        JNaacd [2][3][1] = jAACD  <5,7,3>;
        JNcdaa [2][3][1] = jCDAA  <5,7,3>;
        JNaacd [2][3][2] = jAACD  <5,7,5>;
        JNcdaa [2][3][2] = jCDAA  <5,7,5>;
        JNaacd [2][3][3] = jAACD  <5,7,7>;
        JNcdaa [2][3][3] = jCDAA  <5,7,7>;
        JNaacd [2][4][0] = jAACD  <5,9,1>;
        JNcdaa [2][4][0] = jCDAA  <5,9,1>;
        JNaacd [2][4][1] = jAACD  <5,9,3>;
        JNcdaa [2][4][1] = jCDAA  <5,9,3>;
        JNaacd [2][4][2] = jAACD  <5,9,5>;
        JNcdaa [2][4][2] = jCDAA  <5,9,5>;
        JNaacd [2][4][3] = jAACD  <5,9,7>;
        JNcdaa [2][4][3] = jCDAA  <5,9,7>;
        JNaacd [2][4][4] = jAACD  <5,9,9>;
        JNcdaa [2][4][4] = jCDAA  <5,9,9>;
        JNaacd [3][0][0] = jAACD  <7,1,1>;
        JNcdaa [3][0][0] = jCDAA  <7,1,1>;
        JNaacd [3][1][0] = jAACD  <7,3,1>;
        JNcdaa [3][1][0] = jCDAA  <7,3,1>;
        JNaacd [3][1][1] = jAACD  <7,3,3>;
        JNcdaa [3][1][1] = jCDAA  <7,3,3>;
        JNaacd [3][2][0] = jAACD  <7,5,1>;
        JNcdaa [3][2][0] = jCDAA  <7,5,1>;
        JNaacd [3][2][1] = jAACD  <7,5,3>;
        JNcdaa [3][2][1] = jCDAA  <7,5,3>;
        JNaacd [3][2][2] = jAACD  <7,5,5>;
        JNcdaa [3][2][2] = jCDAA  <7,5,5>;
        JNaacd [3][3][0] = jAACD  <7,7,1>;
        JNcdaa [3][3][0] = jCDAA  <7,7,1>;
        JNaacd [3][3][1] = jAACD  <7,7,3>;
        JNcdaa [3][3][1] = jCDAA  <7,7,3>;
        JNaacd [3][3][2] = jAACD  <7,7,5>;
        JNcdaa [3][3][2] = jCDAA  <7,7,5>;
        JNaacd [3][3][3] = jAACD  <7,7,7>;
        JNcdaa [3][3][3] = jCDAA  <7,7,7>;
        JNaacd [3][4][0] = jAACD  <7,9,1>;
        JNcdaa [3][4][0] = jCDAA  <7,9,1>;
        JNaacd [3][4][1] = jAACD  <7,9,3>;
        JNcdaa [3][4][1] = jCDAA  <7,9,3>;
        JNaacd [3][4][2] = jAACD  <7,9,5>;
        JNcdaa [3][4][2] = jCDAA  <7,9,5>;
        JNaacd [3][4][3] = jAACD  <7,9,7>;
        JNcdaa [3][4][3] = jCDAA  <7,9,7>;
        JNaacd [3][4][4] = jAACD  <7,9,9>;
        JNcdaa [3][4][4] = jCDAA  <7,9,9>;
        JNaacd [4][0][0] = jAACD  <9,1,1>;
        JNcdaa [4][0][0] = jCDAA  <9,1,1>;
        JNaacd [4][1][0] = jAACD  <9,3,1>;
        JNcdaa [4][1][0] = jCDAA  <9,3,1>;
        JNaacd [4][1][1] = jAACD  <9,3,3>;
        JNcdaa [4][1][1] = jCDAA  <9,3,3>;
        JNaacd [4][2][0] = jAACD  <9,5,1>;
        JNcdaa [4][2][0] = jCDAA  <9,5,1>;
        JNaacd [4][2][1] = jAACD  <9,5,3>;
        JNcdaa [4][2][1] = jCDAA  <9,5,3>;
        JNaacd [4][2][2] = jAACD  <9,5,5>;
        JNcdaa [4][2][2] = jCDAA  <9,5,5>;
        JNaacd [4][3][0] = jAACD  <9,7,1>;
        JNcdaa [4][3][0] = jCDAA  <9,7,1>;
        JNaacd [4][3][1] = jAACD  <9,7,3>;
        JNcdaa [4][3][1] = jCDAA  <9,7,3>;
        JNaacd [4][3][2] = jAACD  <9,7,5>;
        JNcdaa [4][3][2] = jCDAA  <9,7,5>;
        JNaacd [4][3][3] = jAACD  <9,7,7>;
        JNcdaa [4][3][3] = jCDAA  <9,7,7>;
        JNaacd [4][4][0] = jAACD  <9,9,1>;
        JNcdaa [4][4][0] = jCDAA  <9,9,1>;
        JNaacd [4][4][1] = jAACD  <9,9,3>;
        JNcdaa [4][4][1] = jCDAA  <9,9,3>;
        JNaacd [4][4][2] = jAACD  <9,9,5>;
        JNcdaa [4][4][2] = jCDAA  <9,9,5>;
        JNaacd [4][4][3] = jAACD  <9,9,7>;
        JNcdaa [4][4][3] = jCDAA  <9,9,7>;
        JNaacd [4][4][4] = jAACD  <9,9,9>;
        JNcdaa [4][4][4] = jCDAA  <9,9,9>;
    }

    //JXC SS
    {
        JNaacc [0][0] = jAACC  <1,1>;
        JNccaa [0][0] = jCCAA  <1,1>;
        JNaacc [0][1] = jAACC  <1,3>;
        JNccaa [0][1] = jCCAA  <1,3>;
        JNaacc [0][2] = jAACC  <1,5>;
        JNccaa [0][2] = jCCAA  <1,5>;
        JNaacc [0][3] = jAACC  <1,7>;
        JNccaa [0][3] = jCCAA  <1,7>;
        JNaacc [0][4] = jAACC  <1,9>;
        JNccaa [0][4] = jCCAA  <1,9>;
        JNaacc [1][0] = jAACC  <3,1>;
        JNccaa [1][0] = jCCAA  <3,1>;
        JNaacc [1][1] = jAACC  <3,3>;
        JNccaa [1][1] = jCCAA  <3,3>;
        JNaacc [1][2] = jAACC  <3,5>;
        JNccaa [1][2] = jCCAA  <3,5>;
        JNaacc [1][3] = jAACC  <3,7>;
        JNccaa [1][3] = jCCAA  <3,7>;
        JNaacc [1][4] = jAACC  <3,9>;
        JNccaa [1][4] = jCCAA  <3,9>;
        JNaacc [2][0] = jAACC  <5,1>;
        JNccaa [2][0] = jCCAA  <5,1>;
        JNaacc [2][1] = jAACC  <5,3>;
        JNccaa [2][1] = jCCAA  <5,3>;
        JNaacc [2][2] = jAACC  <5,5>;
        JNccaa [2][2] = jCCAA  <5,5>;
        JNaacc [2][3] = jAACC  <5,7>;
        JNccaa [2][3] = jCCAA  <5,7>;
        JNaacc [2][4] = jAACC  <5,9>;
        JNccaa [2][4] = jCCAA  <5,9>;
        JNaacc [3][0] = jAACC  <7,1>;
        JNccaa [3][0] = jCCAA  <7,1>;
        JNaacc [3][1] = jAACC  <7,3>;
        JNccaa [3][1] = jCCAA  <7,3>;
        JNaacc [3][2] = jAACC  <7,5>;
        JNccaa [3][2] = jCCAA  <7,5>;
        JNaacc [3][3] = jAACC  <7,7>;
        JNccaa [3][3] = jCCAA  <7,7>;
        JNaacc [3][4] = jAACC  <7,9>;
        JNccaa [3][4] = jCCAA  <7,9>;
        JNaacc [4][0] = jAACC  <9,1>;
        JNccaa [4][0] = jCCAA  <9,1>;
        JNaacc [4][1] = jAACC  <9,3>;
        JNccaa [4][1] = jCCAA  <9,3>;
        JNaacc [4][2] = jAACC  <9,5>;
        JNccaa [4][2] = jCCAA  <9,5>;
        JNaacc [4][3] = jAACC  <9,7>;
        JNccaa [4][3] = jCCAA  <9,7>;
        JNaacc [4][4] = jAACC  <9,9>;
        JNccaa [4][4] = jCCAA  <9,9>;
    }

    //JXC S
    {
        JNabab [0][0] = jABCD  <1,1,1,1>;
        JNabab [1][0] = jABCD  <3,1,3,1>;
        JNabab [1][1] = jABCD  <3,3,3,3>;
        JNabab [2][0] = jABCD  <5,1,5,1>;
        JNabab [2][1] = jABCD  <5,3,5,3>;
        JNabab [2][2] = jABCD  <5,5,5,5>;
        JNabab [3][0] = jABCD  <7,1,7,1>;
        JNabab [3][1] = jABCD  <7,3,7,3>;
        JNabab [3][2] = jABCD  <7,5,7,5>;
        JNabab [3][3] = jABCD  <7,7,7,7>;
        JNabab [4][0] = jABCD  <9,1,9,1>;
        JNabab [4][1] = jABCD  <9,3,9,3>;
        JNabab [4][2] = jABCD  <9,5,9,5>;
        JNabab [4][3] = jABCD  <9,7,9,7>;
        JNabab [4][4] = jABCD  <9,9,9,9>;
    }
}
