#include "contractions.hpp"
#include "low/cache64.hpp"

static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline int Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}


template <int maxLa, int maxLb, int maxLc, int maxLd> void xACBD(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mc=0; mc<maxLc; ++mc) {
            register cacheline64 sum; sum = 0;
            for (int mb=0; mb<maxLb; ++mb) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int bd = Tpos(maxLb,maxLd,mb,md);
                    sum.fma(T[pos], DD[bd]);
                }
            }
            int ac = Tpos(maxLa,maxLc,ma,mc);
            FF[ac]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void xADBC(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int md=0; md<maxLd; ++md) {
            register cacheline64 sum; sum = 0;
            for (int mb=0; mb<maxLb; ++mb) {
                for (int mc=0; mc<maxLc; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int bc = Tpos(maxLb,maxLc,mb,mc);
                    sum.fma(T[pos], DD[bc]);
                }
            }
            int ad = Tpos(maxLa,maxLd,ma,md);
            FF[ad]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void xBDAC(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mb=0; mb<maxLb; ++mb) {
        for (int md=0; md<maxLd; ++md) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mc=0; mc<maxLc; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ac = Tpos(maxLa,maxLc,ma,mc);
                    sum.fma(T[pos], DD[ac]);
                }
            }
            int bd = Tpos(maxLb,maxLd,mb,md);
            FF[bd]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void xBCAD(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mb=0; mb<maxLb; ++mb) {
        for (int mc=0; mc<maxLc; ++mc) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ad = Tpos(maxLa,maxLd,ma,md);
                    sum.fma(T[pos], DD[ad]);
                }
            }
            int bc = Tpos(maxLb,maxLc,mb,mc);
            FF[bc]+=sum;
        }
    }

}



template <int maxLa, int maxLb> void xAABB(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mc=0; mc<maxLa; ++mc) {
            register cacheline64 sum; sum = 0;
            for (int mb=0; mb<maxLb; ++mb) {
                for (int md=0; md<maxLb; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, mc, md);
                    int bd = Tpos(maxLb,maxLb,mb,md);
                    sum.fma(T[pos], DD[bd]);
                }
            }
            int ac = Tpos(maxLa, maxLa,ma, mc);
            FF[ac]+=sum*0.5;
        }
    }

}

template <int maxLa, int maxLb> void xBBAA(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mb=0; mb<maxLb; ++mb) {
        for (int md=0; md<maxLb; ++md) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mc=0; mc<maxLa; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, mc, md);
                    int ac = Tpos(maxLa,maxLa,ma,mc);
                    sum.fma(T[pos], DD[ac]);
                }
            }
            int bd = Tpos(maxLb, maxLb, mb, md);
            FF[bd]+=sum*0.5;
        }
    }

}

template <int maxLa, int maxLb> void xABAB(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mc=0; mc<maxLa; ++mc) {
        for (int mb=0; mb<maxLb; ++mb) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int md=0; md<maxLb; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, mc, md);
                    int ad = Tpos(maxLa,maxLb,ma,md);
                    sum.fma(T[pos], DD[ad]);
                }
            }
            int cb = Tpos(maxLa,maxLb,mc,mb);
            FF[cb] += sum;
        }
    }

}


template <int maxLa, int maxLb> void xABBA(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int ma=0; ma<maxLa; ++ma) {
        for (int md=0; md<maxLb; ++md) {
            register cacheline64 sum; sum = 0;
            for (int mb=0; mb<maxLb; ++mb) {
                for (int mc=0; mc<maxLa; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, mc, md);
                    int bc = Tpos(maxLb,maxLa,mb,mc);
                    sum.fma(T[pos], DD[bc]);
                }
            }
            int ad = Tpos(maxLa,maxLb,ma,md);
            FF[ad]+=sum*0.5;
        }
    }

}

template <int maxLa, int maxLb> void xBAAB(cacheline64 * __restrict__ FF, const cacheline64 * __restrict__ T, const cacheline64 * __restrict__ DD) {

    for (int mb=0; mb<maxLb; ++mb) {
        for (int mc=0; mc<maxLa; ++mc) {
            register cacheline64 sum; sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int md=0; md<maxLb; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, mc, md);
                    int ad = Tpos(maxLa,maxLb,ma,md);
                    sum.fma(T[pos], DD[ad]);
                }
            }
            int bc = Tpos(maxLb,maxLa,mb,mc);
            FF[bc]+=sum*0.5;
        }
    }

}



void DMD::LoadX64() {

    //JXC NN
    {
        X64acbd [0][0][0][0] = xACBD  <1,1,1,1>;
        X64adbc [0][0][0][0] = xADBC  <1,1,1,1>;
        X64bdac [0][0][0][0] = xBDAC  <1,1,1,1>;
        X64bcad [0][0][0][0] = xBCAD  <1,1,1,1>;
        X64acbd [0][0][1][0] = xACBD  <1,1,3,1>;
        X64adbc [0][0][1][0] = xADBC  <1,1,3,1>;
        X64bdac [0][0][1][0] = xBDAC  <1,1,3,1>;
        X64bcad [0][0][1][0] = xBCAD  <1,1,3,1>;
        X64acbd [0][0][1][1] = xACBD  <1,1,3,3>;
        X64adbc [0][0][1][1] = xADBC  <1,1,3,3>;
        X64bdac [0][0][1][1] = xBDAC  <1,1,3,3>;
        X64bcad [0][0][1][1] = xBCAD  <1,1,3,3>;
        X64acbd [0][0][2][0] = xACBD  <1,1,5,1>;
        X64adbc [0][0][2][0] = xADBC  <1,1,5,1>;
        X64bdac [0][0][2][0] = xBDAC  <1,1,5,1>;
        X64bcad [0][0][2][0] = xBCAD  <1,1,5,1>;
        X64acbd [0][0][2][1] = xACBD  <1,1,5,3>;
        X64adbc [0][0][2][1] = xADBC  <1,1,5,3>;
        X64bdac [0][0][2][1] = xBDAC  <1,1,5,3>;
        X64bcad [0][0][2][1] = xBCAD  <1,1,5,3>;
        X64acbd [0][0][2][2] = xACBD  <1,1,5,5>;
        X64adbc [0][0][2][2] = xADBC  <1,1,5,5>;
        X64bdac [0][0][2][2] = xBDAC  <1,1,5,5>;
        X64bcad [0][0][2][2] = xBCAD  <1,1,5,5>;
        X64acbd [0][0][3][0] = xACBD  <1,1,7,1>;
        X64adbc [0][0][3][0] = xADBC  <1,1,7,1>;
        X64bdac [0][0][3][0] = xBDAC  <1,1,7,1>;
        X64bcad [0][0][3][0] = xBCAD  <1,1,7,1>;
        X64acbd [0][0][3][1] = xACBD  <1,1,7,3>;
        X64adbc [0][0][3][1] = xADBC  <1,1,7,3>;
        X64bdac [0][0][3][1] = xBDAC  <1,1,7,3>;
        X64bcad [0][0][3][1] = xBCAD  <1,1,7,3>;
        X64acbd [0][0][3][2] = xACBD  <1,1,7,5>;
        X64adbc [0][0][3][2] = xADBC  <1,1,7,5>;
        X64bdac [0][0][3][2] = xBDAC  <1,1,7,5>;
        X64bcad [0][0][3][2] = xBCAD  <1,1,7,5>;
        X64acbd [0][0][3][3] = xACBD  <1,1,7,7>;
        X64adbc [0][0][3][3] = xADBC  <1,1,7,7>;
        X64bdac [0][0][3][3] = xBDAC  <1,1,7,7>;
        X64bcad [0][0][3][3] = xBCAD  <1,1,7,7>;
        X64acbd [0][0][4][0] = xACBD  <1,1,9,1>;
        X64adbc [0][0][4][0] = xADBC  <1,1,9,1>;
        X64bdac [0][0][4][0] = xBDAC  <1,1,9,1>;
        X64bcad [0][0][4][0] = xBCAD  <1,1,9,1>;
        X64acbd [0][0][4][1] = xACBD  <1,1,9,3>;
        X64adbc [0][0][4][1] = xADBC  <1,1,9,3>;
        X64bdac [0][0][4][1] = xBDAC  <1,1,9,3>;
        X64bcad [0][0][4][1] = xBCAD  <1,1,9,3>;
        X64acbd [0][0][4][2] = xACBD  <1,1,9,5>;
        X64adbc [0][0][4][2] = xADBC  <1,1,9,5>;
        X64bdac [0][0][4][2] = xBDAC  <1,1,9,5>;
        X64bcad [0][0][4][2] = xBCAD  <1,1,9,5>;
        X64acbd [0][0][4][3] = xACBD  <1,1,9,7>;
        X64adbc [0][0][4][3] = xADBC  <1,1,9,7>;
        X64bdac [0][0][4][3] = xBDAC  <1,1,9,7>;
        X64bcad [0][0][4][3] = xBCAD  <1,1,9,7>;
        X64acbd [0][0][4][4] = xACBD  <1,1,9,9>;
        X64adbc [0][0][4][4] = xADBC  <1,1,9,9>;
        X64bdac [0][0][4][4] = xBDAC  <1,1,9,9>;
        X64bcad [0][0][4][4] = xBCAD  <1,1,9,9>;

        X64acbd [1][0][0][0] = xACBD  <3,1,1,1>;
        X64adbc [1][0][0][0] = xADBC  <3,1,1,1>;
        X64bdac [1][0][0][0] = xBDAC  <3,1,1,1>;
        X64bcad [1][0][0][0] = xBCAD  <3,1,1,1>;
        X64acbd [1][0][1][0] = xACBD  <3,1,3,1>;
        X64adbc [1][0][1][0] = xADBC  <3,1,3,1>;
        X64bdac [1][0][1][0] = xBDAC  <3,1,3,1>;
        X64bcad [1][0][1][0] = xBCAD  <3,1,3,1>;
        X64acbd [1][0][1][1] = xACBD  <3,1,3,3>;
        X64adbc [1][0][1][1] = xADBC  <3,1,3,3>;
        X64bdac [1][0][1][1] = xBDAC  <3,1,3,3>;
        X64bcad [1][0][1][1] = xBCAD  <3,1,3,3>;
        X64acbd [1][0][2][0] = xACBD  <3,1,5,1>;
        X64adbc [1][0][2][0] = xADBC  <3,1,5,1>;
        X64bdac [1][0][2][0] = xBDAC  <3,1,5,1>;
        X64bcad [1][0][2][0] = xBCAD  <3,1,5,1>;
        X64acbd [1][0][2][1] = xACBD  <3,1,5,3>;
        X64adbc [1][0][2][1] = xADBC  <3,1,5,3>;
        X64bdac [1][0][2][1] = xBDAC  <3,1,5,3>;
        X64bcad [1][0][2][1] = xBCAD  <3,1,5,3>;
        X64acbd [1][0][2][2] = xACBD  <3,1,5,5>;
        X64adbc [1][0][2][2] = xADBC  <3,1,5,5>;
        X64bdac [1][0][2][2] = xBDAC  <3,1,5,5>;
        X64bcad [1][0][2][2] = xBCAD  <3,1,5,5>;
        X64acbd [1][0][3][0] = xACBD  <3,1,7,1>;
        X64adbc [1][0][3][0] = xADBC  <3,1,7,1>;
        X64bdac [1][0][3][0] = xBDAC  <3,1,7,1>;
        X64bcad [1][0][3][0] = xBCAD  <3,1,7,1>;
        X64acbd [1][0][3][1] = xACBD  <3,1,7,3>;
        X64adbc [1][0][3][1] = xADBC  <3,1,7,3>;
        X64bdac [1][0][3][1] = xBDAC  <3,1,7,3>;
        X64bcad [1][0][3][1] = xBCAD  <3,1,7,3>;
        X64acbd [1][0][3][2] = xACBD  <3,1,7,5>;
        X64adbc [1][0][3][2] = xADBC  <3,1,7,5>;
        X64bdac [1][0][3][2] = xBDAC  <3,1,7,5>;
        X64bcad [1][0][3][2] = xBCAD  <3,1,7,5>;
        X64acbd [1][0][3][3] = xACBD  <3,1,7,7>;
        X64adbc [1][0][3][3] = xADBC  <3,1,7,7>;
        X64bdac [1][0][3][3] = xBDAC  <3,1,7,7>;
        X64bcad [1][0][3][3] = xBCAD  <3,1,7,7>;
        X64acbd [1][0][4][0] = xACBD  <3,1,9,1>;
        X64adbc [1][0][4][0] = xADBC  <3,1,9,1>;
        X64bdac [1][0][4][0] = xBDAC  <3,1,9,1>;
        X64bcad [1][0][4][0] = xBCAD  <3,1,9,1>;
        X64acbd [1][0][4][1] = xACBD  <3,1,9,3>;
        X64adbc [1][0][4][1] = xADBC  <3,1,9,3>;
        X64bdac [1][0][4][1] = xBDAC  <3,1,9,3>;
        X64bcad [1][0][4][1] = xBCAD  <3,1,9,3>;
        X64acbd [1][0][4][2] = xACBD  <3,1,9,5>;
        X64adbc [1][0][4][2] = xADBC  <3,1,9,5>;
        X64bdac [1][0][4][2] = xBDAC  <3,1,9,5>;
        X64bcad [1][0][4][2] = xBCAD  <3,1,9,5>;
        X64acbd [1][0][4][3] = xACBD  <3,1,9,7>;
        X64adbc [1][0][4][3] = xADBC  <3,1,9,7>;
        X64bdac [1][0][4][3] = xBDAC  <3,1,9,7>;
        X64bcad [1][0][4][3] = xBCAD  <3,1,9,7>;
        X64acbd [1][0][4][4] = xACBD  <3,1,9,9>;
        X64adbc [1][0][4][4] = xADBC  <3,1,9,9>;
        X64bdac [1][0][4][4] = xBDAC  <3,1,9,9>;
        X64bcad [1][0][4][4] = xBCAD  <3,1,9,9>;

        X64acbd [1][1][0][0] = xACBD  <3,3,1,1>;
        X64adbc [1][1][0][0] = xADBC  <3,3,1,1>;
        X64bdac [1][1][0][0] = xBDAC  <3,3,1,1>;
        X64bcad [1][1][0][0] = xBCAD  <3,3,1,1>;
        X64acbd [1][1][1][0] = xACBD  <3,3,3,1>;
        X64adbc [1][1][1][0] = xADBC  <3,3,3,1>;
        X64bdac [1][1][1][0] = xBDAC  <3,3,3,1>;
        X64bcad [1][1][1][0] = xBCAD  <3,3,3,1>;
        X64acbd [1][1][1][1] = xACBD  <3,3,3,3>;
        X64adbc [1][1][1][1] = xADBC  <3,3,3,3>;
        X64bdac [1][1][1][1] = xBDAC  <3,3,3,3>;
        X64bcad [1][1][1][1] = xBCAD  <3,3,3,3>;
        X64acbd [1][1][2][0] = xACBD  <3,3,5,1>;
        X64adbc [1][1][2][0] = xADBC  <3,3,5,1>;
        X64bdac [1][1][2][0] = xBDAC  <3,3,5,1>;
        X64bcad [1][1][2][0] = xBCAD  <3,3,5,1>;
        X64acbd [1][1][2][1] = xACBD  <3,3,5,3>;
        X64adbc [1][1][2][1] = xADBC  <3,3,5,3>;
        X64bdac [1][1][2][1] = xBDAC  <3,3,5,3>;
        X64bcad [1][1][2][1] = xBCAD  <3,3,5,3>;
        X64acbd [1][1][2][2] = xACBD  <3,3,5,5>;
        X64adbc [1][1][2][2] = xADBC  <3,3,5,5>;
        X64bdac [1][1][2][2] = xBDAC  <3,3,5,5>;
        X64bcad [1][1][2][2] = xBCAD  <3,3,5,5>;
        X64acbd [1][1][3][0] = xACBD  <3,3,7,1>;
        X64adbc [1][1][3][0] = xADBC  <3,3,7,1>;
        X64bdac [1][1][3][0] = xBDAC  <3,3,7,1>;
        X64bcad [1][1][3][0] = xBCAD  <3,3,7,1>;
        X64acbd [1][1][3][1] = xACBD  <3,3,7,3>;
        X64adbc [1][1][3][1] = xADBC  <3,3,7,3>;
        X64bdac [1][1][3][1] = xBDAC  <3,3,7,3>;
        X64bcad [1][1][3][1] = xBCAD  <3,3,7,3>;
        X64acbd [1][1][3][2] = xACBD  <3,3,7,5>;
        X64adbc [1][1][3][2] = xADBC  <3,3,7,5>;
        X64bdac [1][1][3][2] = xBDAC  <3,3,7,5>;
        X64bcad [1][1][3][2] = xBCAD  <3,3,7,5>;
        X64acbd [1][1][3][3] = xACBD  <3,3,7,7>;
        X64adbc [1][1][3][3] = xADBC  <3,3,7,7>;
        X64bdac [1][1][3][3] = xBDAC  <3,3,7,7>;
        X64bcad [1][1][3][3] = xBCAD  <3,3,7,7>;
        X64acbd [1][1][4][0] = xACBD  <3,3,9,1>;
        X64adbc [1][1][4][0] = xADBC  <3,3,9,1>;
        X64bdac [1][1][4][0] = xBDAC  <3,3,9,1>;
        X64bcad [1][1][4][0] = xBCAD  <3,3,9,1>;
        X64acbd [1][1][4][1] = xACBD  <3,3,9,3>;
        X64adbc [1][1][4][1] = xADBC  <3,3,9,3>;
        X64bdac [1][1][4][1] = xBDAC  <3,3,9,3>;
        X64bcad [1][1][4][1] = xBCAD  <3,3,9,3>;
        X64acbd [1][1][4][2] = xACBD  <3,3,9,5>;
        X64adbc [1][1][4][2] = xADBC  <3,3,9,5>;
        X64bdac [1][1][4][2] = xBDAC  <3,3,9,5>;
        X64bcad [1][1][4][2] = xBCAD  <3,3,9,5>;
        X64acbd [1][1][4][3] = xACBD  <3,3,9,7>;
        X64adbc [1][1][4][3] = xADBC  <3,3,9,7>;
        X64bdac [1][1][4][3] = xBDAC  <3,3,9,7>;
        X64bcad [1][1][4][3] = xBCAD  <3,3,9,7>;
        X64acbd [1][1][4][4] = xACBD  <3,3,9,9>;
        X64adbc [1][1][4][4] = xADBC  <3,3,9,9>;
        X64bdac [1][1][4][4] = xBDAC  <3,3,9,9>;
        X64bcad [1][1][4][4] = xBCAD  <3,3,9,9>;

        X64acbd [2][0][0][0] = xACBD  <5,1,1,1>;
        X64adbc [2][0][0][0] = xADBC  <5,1,1,1>;
        X64bdac [2][0][0][0] = xBDAC  <5,1,1,1>;
        X64bcad [2][0][0][0] = xBCAD  <5,1,1,1>;
        X64acbd [2][0][1][0] = xACBD  <5,1,3,1>;
        X64adbc [2][0][1][0] = xADBC  <5,1,3,1>;
        X64bdac [2][0][1][0] = xBDAC  <5,1,3,1>;
        X64bcad [2][0][1][0] = xBCAD  <5,1,3,1>;
        X64acbd [2][0][1][1] = xACBD  <5,1,3,3>;
        X64adbc [2][0][1][1] = xADBC  <5,1,3,3>;
        X64bdac [2][0][1][1] = xBDAC  <5,1,3,3>;
        X64bcad [2][0][1][1] = xBCAD  <5,1,3,3>;
        X64acbd [2][0][2][0] = xACBD  <5,1,5,1>;
        X64adbc [2][0][2][0] = xADBC  <5,1,5,1>;
        X64bdac [2][0][2][0] = xBDAC  <5,1,5,1>;
        X64bcad [2][0][2][0] = xBCAD  <5,1,5,1>;
        X64acbd [2][0][2][1] = xACBD  <5,1,5,3>;
        X64adbc [2][0][2][1] = xADBC  <5,1,5,3>;
        X64bdac [2][0][2][1] = xBDAC  <5,1,5,3>;
        X64bcad [2][0][2][1] = xBCAD  <5,1,5,3>;
        X64acbd [2][0][2][2] = xACBD  <5,1,5,5>;
        X64adbc [2][0][2][2] = xADBC  <5,1,5,5>;
        X64bdac [2][0][2][2] = xBDAC  <5,1,5,5>;
        X64bcad [2][0][2][2] = xBCAD  <5,1,5,5>;
        X64acbd [2][0][3][0] = xACBD  <5,1,7,1>;
        X64adbc [2][0][3][0] = xADBC  <5,1,7,1>;
        X64bdac [2][0][3][0] = xBDAC  <5,1,7,1>;
        X64bcad [2][0][3][0] = xBCAD  <5,1,7,1>;
        X64acbd [2][0][3][1] = xACBD  <5,1,7,3>;
        X64adbc [2][0][3][1] = xADBC  <5,1,7,3>;
        X64bdac [2][0][3][1] = xBDAC  <5,1,7,3>;
        X64bcad [2][0][3][1] = xBCAD  <5,1,7,3>;
        X64acbd [2][0][3][2] = xACBD  <5,1,7,5>;
        X64adbc [2][0][3][2] = xADBC  <5,1,7,5>;
        X64bdac [2][0][3][2] = xBDAC  <5,1,7,5>;
        X64bcad [2][0][3][2] = xBCAD  <5,1,7,5>;
        X64acbd [2][0][3][3] = xACBD  <5,1,7,7>;
        X64adbc [2][0][3][3] = xADBC  <5,1,7,7>;
        X64bdac [2][0][3][3] = xBDAC  <5,1,7,7>;
        X64bcad [2][0][3][3] = xBCAD  <5,1,7,7>;
        X64acbd [2][0][4][0] = xACBD  <5,1,9,1>;
        X64adbc [2][0][4][0] = xADBC  <5,1,9,1>;
        X64bdac [2][0][4][0] = xBDAC  <5,1,9,1>;
        X64bcad [2][0][4][0] = xBCAD  <5,1,9,1>;
        X64acbd [2][0][4][1] = xACBD  <5,1,9,3>;
        X64adbc [2][0][4][1] = xADBC  <5,1,9,3>;
        X64bdac [2][0][4][1] = xBDAC  <5,1,9,3>;
        X64bcad [2][0][4][1] = xBCAD  <5,1,9,3>;
        X64acbd [2][0][4][2] = xACBD  <5,1,9,5>;
        X64adbc [2][0][4][2] = xADBC  <5,1,9,5>;
        X64bdac [2][0][4][2] = xBDAC  <5,1,9,5>;
        X64bcad [2][0][4][2] = xBCAD  <5,1,9,5>;
        X64acbd [2][0][4][3] = xACBD  <5,1,9,7>;
        X64adbc [2][0][4][3] = xADBC  <5,1,9,7>;
        X64bdac [2][0][4][3] = xBDAC  <5,1,9,7>;
        X64bcad [2][0][4][3] = xBCAD  <5,1,9,7>;
        X64acbd [2][0][4][4] = xACBD  <5,1,9,9>;
        X64adbc [2][0][4][4] = xADBC  <5,1,9,9>;
        X64bdac [2][0][4][4] = xBDAC  <5,1,9,9>;
        X64bcad [2][0][4][4] = xBCAD  <5,1,9,9>;

        X64acbd [2][1][0][0] = xACBD  <5,3,1,1>;
        X64adbc [2][1][0][0] = xADBC  <5,3,1,1>;
        X64bdac [2][1][0][0] = xBDAC  <5,3,1,1>;
        X64bcad [2][1][0][0] = xBCAD  <5,3,1,1>;
        X64acbd [2][1][1][0] = xACBD  <5,3,3,1>;
        X64adbc [2][1][1][0] = xADBC  <5,3,3,1>;
        X64bdac [2][1][1][0] = xBDAC  <5,3,3,1>;
        X64bcad [2][1][1][0] = xBCAD  <5,3,3,1>;
        X64acbd [2][1][1][1] = xACBD  <5,3,3,3>;
        X64adbc [2][1][1][1] = xADBC  <5,3,3,3>;
        X64bdac [2][1][1][1] = xBDAC  <5,3,3,3>;
        X64bcad [2][1][1][1] = xBCAD  <5,3,3,3>;
        X64acbd [2][1][2][0] = xACBD  <5,3,5,1>;
        X64adbc [2][1][2][0] = xADBC  <5,3,5,1>;
        X64bdac [2][1][2][0] = xBDAC  <5,3,5,1>;
        X64bcad [2][1][2][0] = xBCAD  <5,3,5,1>;
        X64acbd [2][1][2][1] = xACBD  <5,3,5,3>;
        X64adbc [2][1][2][1] = xADBC  <5,3,5,3>;
        X64bdac [2][1][2][1] = xBDAC  <5,3,5,3>;
        X64bcad [2][1][2][1] = xBCAD  <5,3,5,3>;
        X64acbd [2][1][2][2] = xACBD  <5,3,5,5>;
        X64adbc [2][1][2][2] = xADBC  <5,3,5,5>;
        X64bdac [2][1][2][2] = xBDAC  <5,3,5,5>;
        X64bcad [2][1][2][2] = xBCAD  <5,3,5,5>;
        X64acbd [2][1][3][0] = xACBD  <5,3,7,1>;
        X64adbc [2][1][3][0] = xADBC  <5,3,7,1>;
        X64bdac [2][1][3][0] = xBDAC  <5,3,7,1>;
        X64bcad [2][1][3][0] = xBCAD  <5,3,7,1>;
        X64acbd [2][1][3][1] = xACBD  <5,3,7,3>;
        X64adbc [2][1][3][1] = xADBC  <5,3,7,3>;
        X64bdac [2][1][3][1] = xBDAC  <5,3,7,3>;
        X64bcad [2][1][3][1] = xBCAD  <5,3,7,3>;
        X64acbd [2][1][3][2] = xACBD  <5,3,7,5>;
        X64adbc [2][1][3][2] = xADBC  <5,3,7,5>;
        X64bdac [2][1][3][2] = xBDAC  <5,3,7,5>;
        X64bcad [2][1][3][2] = xBCAD  <5,3,7,5>;
        X64acbd [2][1][3][3] = xACBD  <5,3,7,7>;
        X64adbc [2][1][3][3] = xADBC  <5,3,7,7>;
        X64bdac [2][1][3][3] = xBDAC  <5,3,7,7>;
        X64bcad [2][1][3][3] = xBCAD  <5,3,7,7>;
        X64acbd [2][1][4][0] = xACBD  <5,3,9,1>;
        X64adbc [2][1][4][0] = xADBC  <5,3,9,1>;
        X64bdac [2][1][4][0] = xBDAC  <5,3,9,1>;
        X64bcad [2][1][4][0] = xBCAD  <5,3,9,1>;
        X64acbd [2][1][4][1] = xACBD  <5,3,9,3>;
        X64adbc [2][1][4][1] = xADBC  <5,3,9,3>;
        X64bdac [2][1][4][1] = xBDAC  <5,3,9,3>;
        X64bcad [2][1][4][1] = xBCAD  <5,3,9,3>;
        X64acbd [2][1][4][2] = xACBD  <5,3,9,5>;
        X64adbc [2][1][4][2] = xADBC  <5,3,9,5>;
        X64bdac [2][1][4][2] = xBDAC  <5,3,9,5>;
        X64bcad [2][1][4][2] = xBCAD  <5,3,9,5>;
        X64acbd [2][1][4][3] = xACBD  <5,3,9,7>;
        X64adbc [2][1][4][3] = xADBC  <5,3,9,7>;
        X64bdac [2][1][4][3] = xBDAC  <5,3,9,7>;
        X64bcad [2][1][4][3] = xBCAD  <5,3,9,7>;
        X64acbd [2][1][4][4] = xACBD  <5,3,9,9>;
        X64adbc [2][1][4][4] = xADBC  <5,3,9,9>;
        X64bdac [2][1][4][4] = xBDAC  <5,3,9,9>;
        X64bcad [2][1][4][4] = xBCAD  <5,3,9,9>;

        X64acbd [2][2][0][0] = xACBD  <5,5,1,1>;
        X64adbc [2][2][0][0] = xADBC  <5,5,1,1>;
        X64bdac [2][2][0][0] = xBDAC  <5,5,1,1>;
        X64bcad [2][2][0][0] = xBCAD  <5,5,1,1>;
        X64acbd [2][2][1][0] = xACBD  <5,5,3,1>;
        X64adbc [2][2][1][0] = xADBC  <5,5,3,1>;
        X64bdac [2][2][1][0] = xBDAC  <5,5,3,1>;
        X64bcad [2][2][1][0] = xBCAD  <5,5,3,1>;
        X64acbd [2][2][1][1] = xACBD  <5,5,3,3>;
        X64adbc [2][2][1][1] = xADBC  <5,5,3,3>;
        X64bdac [2][2][1][1] = xBDAC  <5,5,3,3>;
        X64bcad [2][2][1][1] = xBCAD  <5,5,3,3>;
        X64acbd [2][2][2][0] = xACBD  <5,5,5,1>;
        X64adbc [2][2][2][0] = xADBC  <5,5,5,1>;
        X64bdac [2][2][2][0] = xBDAC  <5,5,5,1>;
        X64bcad [2][2][2][0] = xBCAD  <5,5,5,1>;
        X64acbd [2][2][2][1] = xACBD  <5,5,5,3>;
        X64adbc [2][2][2][1] = xADBC  <5,5,5,3>;
        X64bdac [2][2][2][1] = xBDAC  <5,5,5,3>;
        X64bcad [2][2][2][1] = xBCAD  <5,5,5,3>;
        X64acbd [2][2][2][2] = xACBD  <5,5,5,5>;
        X64adbc [2][2][2][2] = xADBC  <5,5,5,5>;
        X64bdac [2][2][2][2] = xBDAC  <5,5,5,5>;
        X64bcad [2][2][2][2] = xBCAD  <5,5,5,5>;
        X64acbd [2][2][3][0] = xACBD  <5,5,7,1>;
        X64adbc [2][2][3][0] = xADBC  <5,5,7,1>;
        X64bdac [2][2][3][0] = xBDAC  <5,5,7,1>;
        X64bcad [2][2][3][0] = xBCAD  <5,5,7,1>;
        X64acbd [2][2][3][1] = xACBD  <5,5,7,3>;
        X64adbc [2][2][3][1] = xADBC  <5,5,7,3>;
        X64bdac [2][2][3][1] = xBDAC  <5,5,7,3>;
        X64bcad [2][2][3][1] = xBCAD  <5,5,7,3>;
        X64acbd [2][2][3][2] = xACBD  <5,5,7,5>;
        X64adbc [2][2][3][2] = xADBC  <5,5,7,5>;
        X64bdac [2][2][3][2] = xBDAC  <5,5,7,5>;
        X64bcad [2][2][3][2] = xBCAD  <5,5,7,5>;
        X64acbd [2][2][3][3] = xACBD  <5,5,7,7>;
        X64adbc [2][2][3][3] = xADBC  <5,5,7,7>;
        X64bdac [2][2][3][3] = xBDAC  <5,5,7,7>;
        X64bcad [2][2][3][3] = xBCAD  <5,5,7,7>;
        X64acbd [2][2][4][0] = xACBD  <5,5,9,1>;
        X64adbc [2][2][4][0] = xADBC  <5,5,9,1>;
        X64bdac [2][2][4][0] = xBDAC  <5,5,9,1>;
        X64bcad [2][2][4][0] = xBCAD  <5,5,9,1>;
        X64acbd [2][2][4][1] = xACBD  <5,5,9,3>;
        X64adbc [2][2][4][1] = xADBC  <5,5,9,3>;
        X64bdac [2][2][4][1] = xBDAC  <5,5,9,3>;
        X64bcad [2][2][4][1] = xBCAD  <5,5,9,3>;
        X64acbd [2][2][4][2] = xACBD  <5,5,9,5>;
        X64adbc [2][2][4][2] = xADBC  <5,5,9,5>;
        X64bdac [2][2][4][2] = xBDAC  <5,5,9,5>;
        X64bcad [2][2][4][2] = xBCAD  <5,5,9,5>;
        X64acbd [2][2][4][3] = xACBD  <5,5,9,7>;
        X64adbc [2][2][4][3] = xADBC  <5,5,9,7>;
        X64bdac [2][2][4][3] = xBDAC  <5,5,9,7>;
        X64bcad [2][2][4][3] = xBCAD  <5,5,9,7>;
        X64acbd [2][2][4][4] = xACBD  <5,5,9,9>;
        X64adbc [2][2][4][4] = xADBC  <5,5,9,9>;
        X64bdac [2][2][4][4] = xBDAC  <5,5,9,9>;
        X64bcad [2][2][4][4] = xBCAD  <5,5,9,9>;

        X64acbd [3][0][0][0] = xACBD  <7,1,1,1>;
        X64adbc [3][0][0][0] = xADBC  <7,1,1,1>;
        X64bdac [3][0][0][0] = xBDAC  <7,1,1,1>;
        X64bcad [3][0][0][0] = xBCAD  <7,1,1,1>;
        X64acbd [3][0][1][0] = xACBD  <7,1,3,1>;
        X64adbc [3][0][1][0] = xADBC  <7,1,3,1>;
        X64bdac [3][0][1][0] = xBDAC  <7,1,3,1>;
        X64bcad [3][0][1][0] = xBCAD  <7,1,3,1>;
        X64acbd [3][0][1][1] = xACBD  <7,1,3,3>;
        X64adbc [3][0][1][1] = xADBC  <7,1,3,3>;
        X64bdac [3][0][1][1] = xBDAC  <7,1,3,3>;
        X64bcad [3][0][1][1] = xBCAD  <7,1,3,3>;
        X64acbd [3][0][2][0] = xACBD  <7,1,5,1>;
        X64adbc [3][0][2][0] = xADBC  <7,1,5,1>;
        X64bdac [3][0][2][0] = xBDAC  <7,1,5,1>;
        X64bcad [3][0][2][0] = xBCAD  <7,1,5,1>;
        X64acbd [3][0][2][1] = xACBD  <7,1,5,3>;
        X64adbc [3][0][2][1] = xADBC  <7,1,5,3>;
        X64bdac [3][0][2][1] = xBDAC  <7,1,5,3>;
        X64bcad [3][0][2][1] = xBCAD  <7,1,5,3>;
        X64acbd [3][0][2][2] = xACBD  <7,1,5,5>;
        X64adbc [3][0][2][2] = xADBC  <7,1,5,5>;
        X64bdac [3][0][2][2] = xBDAC  <7,1,5,5>;
        X64bcad [3][0][2][2] = xBCAD  <7,1,5,5>;
        X64acbd [3][0][3][0] = xACBD  <7,1,7,1>;
        X64adbc [3][0][3][0] = xADBC  <7,1,7,1>;
        X64bdac [3][0][3][0] = xBDAC  <7,1,7,1>;
        X64bcad [3][0][3][0] = xBCAD  <7,1,7,1>;
        X64acbd [3][0][3][1] = xACBD  <7,1,7,3>;
        X64adbc [3][0][3][1] = xADBC  <7,1,7,3>;
        X64bdac [3][0][3][1] = xBDAC  <7,1,7,3>;
        X64bcad [3][0][3][1] = xBCAD  <7,1,7,3>;
        X64acbd [3][0][3][2] = xACBD  <7,1,7,5>;
        X64adbc [3][0][3][2] = xADBC  <7,1,7,5>;
        X64bdac [3][0][3][2] = xBDAC  <7,1,7,5>;
        X64bcad [3][0][3][2] = xBCAD  <7,1,7,5>;
        X64acbd [3][0][3][3] = xACBD  <7,1,7,7>;
        X64adbc [3][0][3][3] = xADBC  <7,1,7,7>;
        X64bdac [3][0][3][3] = xBDAC  <7,1,7,7>;
        X64bcad [3][0][3][3] = xBCAD  <7,1,7,7>;
        X64acbd [3][0][4][0] = xACBD  <7,1,9,1>;
        X64adbc [3][0][4][0] = xADBC  <7,1,9,1>;
        X64bdac [3][0][4][0] = xBDAC  <7,1,9,1>;
        X64bcad [3][0][4][0] = xBCAD  <7,1,9,1>;
        X64acbd [3][0][4][1] = xACBD  <7,1,9,3>;
        X64adbc [3][0][4][1] = xADBC  <7,1,9,3>;
        X64bdac [3][0][4][1] = xBDAC  <7,1,9,3>;
        X64bcad [3][0][4][1] = xBCAD  <7,1,9,3>;
        X64acbd [3][0][4][2] = xACBD  <7,1,9,5>;
        X64adbc [3][0][4][2] = xADBC  <7,1,9,5>;
        X64bdac [3][0][4][2] = xBDAC  <7,1,9,5>;
        X64bcad [3][0][4][2] = xBCAD  <7,1,9,5>;
        X64acbd [3][0][4][3] = xACBD  <7,1,9,7>;
        X64adbc [3][0][4][3] = xADBC  <7,1,9,7>;
        X64bdac [3][0][4][3] = xBDAC  <7,1,9,7>;
        X64bcad [3][0][4][3] = xBCAD  <7,1,9,7>;
        X64acbd [3][0][4][4] = xACBD  <7,1,9,9>;
        X64adbc [3][0][4][4] = xADBC  <7,1,9,9>;
        X64bdac [3][0][4][4] = xBDAC  <7,1,9,9>;
        X64bcad [3][0][4][4] = xBCAD  <7,1,9,9>;

        X64acbd [3][1][0][0] = xACBD  <7,3,1,1>;
        X64adbc [3][1][0][0] = xADBC  <7,3,1,1>;
        X64bdac [3][1][0][0] = xBDAC  <7,3,1,1>;
        X64bcad [3][1][0][0] = xBCAD  <7,3,1,1>;
        X64acbd [3][1][1][0] = xACBD  <7,3,3,1>;
        X64adbc [3][1][1][0] = xADBC  <7,3,3,1>;
        X64bdac [3][1][1][0] = xBDAC  <7,3,3,1>;
        X64bcad [3][1][1][0] = xBCAD  <7,3,3,1>;
        X64acbd [3][1][1][1] = xACBD  <7,3,3,3>;
        X64adbc [3][1][1][1] = xADBC  <7,3,3,3>;
        X64bdac [3][1][1][1] = xBDAC  <7,3,3,3>;
        X64bcad [3][1][1][1] = xBCAD  <7,3,3,3>;
        X64acbd [3][1][2][0] = xACBD  <7,3,5,1>;
        X64adbc [3][1][2][0] = xADBC  <7,3,5,1>;
        X64bdac [3][1][2][0] = xBDAC  <7,3,5,1>;
        X64bcad [3][1][2][0] = xBCAD  <7,3,5,1>;
        X64acbd [3][1][2][1] = xACBD  <7,3,5,3>;
        X64adbc [3][1][2][1] = xADBC  <7,3,5,3>;
        X64bdac [3][1][2][1] = xBDAC  <7,3,5,3>;
        X64bcad [3][1][2][1] = xBCAD  <7,3,5,3>;
        X64acbd [3][1][2][2] = xACBD  <7,3,5,5>;
        X64adbc [3][1][2][2] = xADBC  <7,3,5,5>;
        X64bdac [3][1][2][2] = xBDAC  <7,3,5,5>;
        X64bcad [3][1][2][2] = xBCAD  <7,3,5,5>;
        X64acbd [3][1][3][0] = xACBD  <7,3,7,1>;
        X64adbc [3][1][3][0] = xADBC  <7,3,7,1>;
        X64bdac [3][1][3][0] = xBDAC  <7,3,7,1>;
        X64bcad [3][1][3][0] = xBCAD  <7,3,7,1>;
        X64acbd [3][1][3][1] = xACBD  <7,3,7,3>;
        X64adbc [3][1][3][1] = xADBC  <7,3,7,3>;
        X64bdac [3][1][3][1] = xBDAC  <7,3,7,3>;
        X64bcad [3][1][3][1] = xBCAD  <7,3,7,3>;
        X64acbd [3][1][3][2] = xACBD  <7,3,7,5>;
        X64adbc [3][1][3][2] = xADBC  <7,3,7,5>;
        X64bdac [3][1][3][2] = xBDAC  <7,3,7,5>;
        X64bcad [3][1][3][2] = xBCAD  <7,3,7,5>;
        X64acbd [3][1][3][3] = xACBD  <7,3,7,7>;
        X64adbc [3][1][3][3] = xADBC  <7,3,7,7>;
        X64bdac [3][1][3][3] = xBDAC  <7,3,7,7>;
        X64bcad [3][1][3][3] = xBCAD  <7,3,7,7>;
        X64acbd [3][1][4][0] = xACBD  <7,3,9,1>;
        X64adbc [3][1][4][0] = xADBC  <7,3,9,1>;
        X64bdac [3][1][4][0] = xBDAC  <7,3,9,1>;
        X64bcad [3][1][4][0] = xBCAD  <7,3,9,1>;
        X64acbd [3][1][4][1] = xACBD  <7,3,9,3>;
        X64adbc [3][1][4][1] = xADBC  <7,3,9,3>;
        X64bdac [3][1][4][1] = xBDAC  <7,3,9,3>;
        X64bcad [3][1][4][1] = xBCAD  <7,3,9,3>;
        X64acbd [3][1][4][2] = xACBD  <7,3,9,5>;
        X64adbc [3][1][4][2] = xADBC  <7,3,9,5>;
        X64bdac [3][1][4][2] = xBDAC  <7,3,9,5>;
        X64bcad [3][1][4][2] = xBCAD  <7,3,9,5>;
        X64acbd [3][1][4][3] = xACBD  <7,3,9,7>;
        X64adbc [3][1][4][3] = xADBC  <7,3,9,7>;
        X64bdac [3][1][4][3] = xBDAC  <7,3,9,7>;
        X64bcad [3][1][4][3] = xBCAD  <7,3,9,7>;
        X64acbd [3][1][4][4] = xACBD  <7,3,9,9>;
        X64adbc [3][1][4][4] = xADBC  <7,3,9,9>;
        X64bdac [3][1][4][4] = xBDAC  <7,3,9,9>;
        X64bcad [3][1][4][4] = xBCAD  <7,3,9,9>;

        X64acbd [3][2][0][0] = xACBD  <7,5,1,1>;
        X64adbc [3][2][0][0] = xADBC  <7,5,1,1>;
        X64bdac [3][2][0][0] = xBDAC  <7,5,1,1>;
        X64bcad [3][2][0][0] = xBCAD  <7,5,1,1>;
        X64acbd [3][2][1][0] = xACBD  <7,5,3,1>;
        X64adbc [3][2][1][0] = xADBC  <7,5,3,1>;
        X64bdac [3][2][1][0] = xBDAC  <7,5,3,1>;
        X64bcad [3][2][1][0] = xBCAD  <7,5,3,1>;
        X64acbd [3][2][1][1] = xACBD  <7,5,3,3>;
        X64adbc [3][2][1][1] = xADBC  <7,5,3,3>;
        X64bdac [3][2][1][1] = xBDAC  <7,5,3,3>;
        X64bcad [3][2][1][1] = xBCAD  <7,5,3,3>;
        X64acbd [3][2][2][0] = xACBD  <7,5,5,1>;
        X64adbc [3][2][2][0] = xADBC  <7,5,5,1>;
        X64bdac [3][2][2][0] = xBDAC  <7,5,5,1>;
        X64bcad [3][2][2][0] = xBCAD  <7,5,5,1>;
        X64acbd [3][2][2][1] = xACBD  <7,5,5,3>;
        X64adbc [3][2][2][1] = xADBC  <7,5,5,3>;
        X64bdac [3][2][2][1] = xBDAC  <7,5,5,3>;
        X64bcad [3][2][2][1] = xBCAD  <7,5,5,3>;
        X64acbd [3][2][2][2] = xACBD  <7,5,5,5>;
        X64adbc [3][2][2][2] = xADBC  <7,5,5,5>;
        X64bdac [3][2][2][2] = xBDAC  <7,5,5,5>;
        X64bcad [3][2][2][2] = xBCAD  <7,5,5,5>;
        X64acbd [3][2][3][0] = xACBD  <7,5,7,1>;
        X64adbc [3][2][3][0] = xADBC  <7,5,7,1>;
        X64bdac [3][2][3][0] = xBDAC  <7,5,7,1>;
        X64bcad [3][2][3][0] = xBCAD  <7,5,7,1>;
        X64acbd [3][2][3][1] = xACBD  <7,5,7,3>;
        X64adbc [3][2][3][1] = xADBC  <7,5,7,3>;
        X64bdac [3][2][3][1] = xBDAC  <7,5,7,3>;
        X64bcad [3][2][3][1] = xBCAD  <7,5,7,3>;
        X64acbd [3][2][3][2] = xACBD  <7,5,7,5>;
        X64adbc [3][2][3][2] = xADBC  <7,5,7,5>;
        X64bdac [3][2][3][2] = xBDAC  <7,5,7,5>;
        X64bcad [3][2][3][2] = xBCAD  <7,5,7,5>;
        X64acbd [3][2][3][3] = xACBD  <7,5,7,7>;
        X64adbc [3][2][3][3] = xADBC  <7,5,7,7>;
        X64bdac [3][2][3][3] = xBDAC  <7,5,7,7>;
        X64bcad [3][2][3][3] = xBCAD  <7,5,7,7>;
        X64acbd [3][2][4][0] = xACBD  <7,5,9,1>;
        X64adbc [3][2][4][0] = xADBC  <7,5,9,1>;
        X64bdac [3][2][4][0] = xBDAC  <7,5,9,1>;
        X64bcad [3][2][4][0] = xBCAD  <7,5,9,1>;
        X64acbd [3][2][4][1] = xACBD  <7,5,9,3>;
        X64adbc [3][2][4][1] = xADBC  <7,5,9,3>;
        X64bdac [3][2][4][1] = xBDAC  <7,5,9,3>;
        X64bcad [3][2][4][1] = xBCAD  <7,5,9,3>;
        X64acbd [3][2][4][2] = xACBD  <7,5,9,5>;
        X64adbc [3][2][4][2] = xADBC  <7,5,9,5>;
        X64bdac [3][2][4][2] = xBDAC  <7,5,9,5>;
        X64bcad [3][2][4][2] = xBCAD  <7,5,9,5>;
        X64acbd [3][2][4][3] = xACBD  <7,5,9,7>;
        X64adbc [3][2][4][3] = xADBC  <7,5,9,7>;
        X64bdac [3][2][4][3] = xBDAC  <7,5,9,7>;
        X64bcad [3][2][4][3] = xBCAD  <7,5,9,7>;
        X64acbd [3][2][4][4] = xACBD  <7,5,9,9>;
        X64adbc [3][2][4][4] = xADBC  <7,5,9,9>;
        X64bdac [3][2][4][4] = xBDAC  <7,5,9,9>;
        X64bcad [3][2][4][4] = xBCAD  <7,5,9,9>;

        X64acbd [3][3][0][0] = xACBD  <7,7,1,1>;
        X64adbc [3][3][0][0] = xADBC  <7,7,1,1>;
        X64bdac [3][3][0][0] = xBDAC  <7,7,1,1>;
        X64bcad [3][3][0][0] = xBCAD  <7,7,1,1>;
        X64acbd [3][3][1][0] = xACBD  <7,7,3,1>;
        X64adbc [3][3][1][0] = xADBC  <7,7,3,1>;
        X64bdac [3][3][1][0] = xBDAC  <7,7,3,1>;
        X64bcad [3][3][1][0] = xBCAD  <7,7,3,1>;
        X64acbd [3][3][1][1] = xACBD  <7,7,3,3>;
        X64adbc [3][3][1][1] = xADBC  <7,7,3,3>;
        X64bdac [3][3][1][1] = xBDAC  <7,7,3,3>;
        X64bcad [3][3][1][1] = xBCAD  <7,7,3,3>;
        X64acbd [3][3][2][0] = xACBD  <7,7,5,1>;
        X64adbc [3][3][2][0] = xADBC  <7,7,5,1>;
        X64bdac [3][3][2][0] = xBDAC  <7,7,5,1>;
        X64bcad [3][3][2][0] = xBCAD  <7,7,5,1>;
        X64acbd [3][3][2][1] = xACBD  <7,7,5,3>;
        X64adbc [3][3][2][1] = xADBC  <7,7,5,3>;
        X64bdac [3][3][2][1] = xBDAC  <7,7,5,3>;
        X64bcad [3][3][2][1] = xBCAD  <7,7,5,3>;
        X64acbd [3][3][2][2] = xACBD  <7,7,5,5>;
        X64adbc [3][3][2][2] = xADBC  <7,7,5,5>;
        X64bdac [3][3][2][2] = xBDAC  <7,7,5,5>;
        X64bcad [3][3][2][2] = xBCAD  <7,7,5,5>;
        X64acbd [3][3][3][0] = xACBD  <7,7,7,1>;
        X64adbc [3][3][3][0] = xADBC  <7,7,7,1>;
        X64bdac [3][3][3][0] = xBDAC  <7,7,7,1>;
        X64bcad [3][3][3][0] = xBCAD  <7,7,7,1>;
        X64acbd [3][3][3][1] = xACBD  <7,7,7,3>;
        X64adbc [3][3][3][1] = xADBC  <7,7,7,3>;
        X64bdac [3][3][3][1] = xBDAC  <7,7,7,3>;
        X64bcad [3][3][3][1] = xBCAD  <7,7,7,3>;
        X64acbd [3][3][3][2] = xACBD  <7,7,7,5>;
        X64adbc [3][3][3][2] = xADBC  <7,7,7,5>;
        X64bdac [3][3][3][2] = xBDAC  <7,7,7,5>;
        X64bcad [3][3][3][2] = xBCAD  <7,7,7,5>;
        X64acbd [3][3][3][3] = xACBD  <7,7,7,7>;
        X64adbc [3][3][3][3] = xADBC  <7,7,7,7>;
        X64bdac [3][3][3][3] = xBDAC  <7,7,7,7>;
        X64bcad [3][3][3][3] = xBCAD  <7,7,7,7>;
        X64acbd [3][3][4][0] = xACBD  <7,7,9,1>;
        X64adbc [3][3][4][0] = xADBC  <7,7,9,1>;
        X64bdac [3][3][4][0] = xBDAC  <7,7,9,1>;
        X64bcad [3][3][4][0] = xBCAD  <7,7,9,1>;
        X64acbd [3][3][4][1] = xACBD  <7,7,9,3>;
        X64adbc [3][3][4][1] = xADBC  <7,7,9,3>;
        X64bdac [3][3][4][1] = xBDAC  <7,7,9,3>;
        X64bcad [3][3][4][1] = xBCAD  <7,7,9,3>;
        X64acbd [3][3][4][2] = xACBD  <7,7,9,5>;
        X64adbc [3][3][4][2] = xADBC  <7,7,9,5>;
        X64bdac [3][3][4][2] = xBDAC  <7,7,9,5>;
        X64bcad [3][3][4][2] = xBCAD  <7,7,9,5>;
        X64acbd [3][3][4][3] = xACBD  <7,7,9,7>;
        X64adbc [3][3][4][3] = xADBC  <7,7,9,7>;
        X64bdac [3][3][4][3] = xBDAC  <7,7,9,7>;
        X64bcad [3][3][4][3] = xBCAD  <7,7,9,7>;
        X64acbd [3][3][4][4] = xACBD  <7,7,9,9>;
        X64adbc [3][3][4][4] = xADBC  <7,7,9,9>;
        X64bdac [3][3][4][4] = xBDAC  <7,7,9,9>;
        X64bcad [3][3][4][4] = xBCAD  <7,7,9,9>;

        X64acbd [4][0][0][0] = xACBD  <9,1,1,1>;
        X64adbc [4][0][0][0] = xADBC  <9,1,1,1>;
        X64bdac [4][0][0][0] = xBDAC  <9,1,1,1>;
        X64bcad [4][0][0][0] = xBCAD  <9,1,1,1>;
        X64acbd [4][0][1][0] = xACBD  <9,1,3,1>;
        X64adbc [4][0][1][0] = xADBC  <9,1,3,1>;
        X64bdac [4][0][1][0] = xBDAC  <9,1,3,1>;
        X64bcad [4][0][1][0] = xBCAD  <9,1,3,1>;
        X64acbd [4][0][1][1] = xACBD  <9,1,3,3>;
        X64adbc [4][0][1][1] = xADBC  <9,1,3,3>;
        X64bdac [4][0][1][1] = xBDAC  <9,1,3,3>;
        X64bcad [4][0][1][1] = xBCAD  <9,1,3,3>;
        X64acbd [4][0][2][0] = xACBD  <9,1,5,1>;
        X64adbc [4][0][2][0] = xADBC  <9,1,5,1>;
        X64bdac [4][0][2][0] = xBDAC  <9,1,5,1>;
        X64bcad [4][0][2][0] = xBCAD  <9,1,5,1>;
        X64acbd [4][0][2][1] = xACBD  <9,1,5,3>;
        X64adbc [4][0][2][1] = xADBC  <9,1,5,3>;
        X64bdac [4][0][2][1] = xBDAC  <9,1,5,3>;
        X64bcad [4][0][2][1] = xBCAD  <9,1,5,3>;
        X64acbd [4][0][2][2] = xACBD  <9,1,5,5>;
        X64adbc [4][0][2][2] = xADBC  <9,1,5,5>;
        X64bdac [4][0][2][2] = xBDAC  <9,1,5,5>;
        X64bcad [4][0][2][2] = xBCAD  <9,1,5,5>;
        X64acbd [4][0][3][0] = xACBD  <9,1,7,1>;
        X64adbc [4][0][3][0] = xADBC  <9,1,7,1>;
        X64bdac [4][0][3][0] = xBDAC  <9,1,7,1>;
        X64bcad [4][0][3][0] = xBCAD  <9,1,7,1>;
        X64acbd [4][0][3][1] = xACBD  <9,1,7,3>;
        X64adbc [4][0][3][1] = xADBC  <9,1,7,3>;
        X64bdac [4][0][3][1] = xBDAC  <9,1,7,3>;
        X64bcad [4][0][3][1] = xBCAD  <9,1,7,3>;
        X64acbd [4][0][3][2] = xACBD  <9,1,7,5>;
        X64adbc [4][0][3][2] = xADBC  <9,1,7,5>;
        X64bdac [4][0][3][2] = xBDAC  <9,1,7,5>;
        X64bcad [4][0][3][2] = xBCAD  <9,1,7,5>;
        X64acbd [4][0][3][3] = xACBD  <9,1,7,7>;
        X64adbc [4][0][3][3] = xADBC  <9,1,7,7>;
        X64bdac [4][0][3][3] = xBDAC  <9,1,7,7>;
        X64bcad [4][0][3][3] = xBCAD  <9,1,7,7>;
        X64acbd [4][0][4][0] = xACBD  <9,1,9,1>;
        X64adbc [4][0][4][0] = xADBC  <9,1,9,1>;
        X64bdac [4][0][4][0] = xBDAC  <9,1,9,1>;
        X64bcad [4][0][4][0] = xBCAD  <9,1,9,1>;
        X64acbd [4][0][4][1] = xACBD  <9,1,9,3>;
        X64adbc [4][0][4][1] = xADBC  <9,1,9,3>;
        X64bdac [4][0][4][1] = xBDAC  <9,1,9,3>;
        X64bcad [4][0][4][1] = xBCAD  <9,1,9,3>;
        X64acbd [4][0][4][2] = xACBD  <9,1,9,5>;
        X64adbc [4][0][4][2] = xADBC  <9,1,9,5>;
        X64bdac [4][0][4][2] = xBDAC  <9,1,9,5>;
        X64bcad [4][0][4][2] = xBCAD  <9,1,9,5>;
        X64acbd [4][0][4][3] = xACBD  <9,1,9,7>;
        X64adbc [4][0][4][3] = xADBC  <9,1,9,7>;
        X64bdac [4][0][4][3] = xBDAC  <9,1,9,7>;
        X64bcad [4][0][4][3] = xBCAD  <9,1,9,7>;
        X64acbd [4][0][4][4] = xACBD  <9,1,9,9>;
        X64adbc [4][0][4][4] = xADBC  <9,1,9,9>;
        X64bdac [4][0][4][4] = xBDAC  <9,1,9,9>;
        X64bcad [4][0][4][4] = xBCAD  <9,1,9,9>;

        X64acbd [4][1][0][0] = xACBD  <9,3,1,1>;
        X64adbc [4][1][0][0] = xADBC  <9,3,1,1>;
        X64bdac [4][1][0][0] = xBDAC  <9,3,1,1>;
        X64bcad [4][1][0][0] = xBCAD  <9,3,1,1>;
        X64acbd [4][1][1][0] = xACBD  <9,3,3,1>;
        X64adbc [4][1][1][0] = xADBC  <9,3,3,1>;
        X64bdac [4][1][1][0] = xBDAC  <9,3,3,1>;
        X64bcad [4][1][1][0] = xBCAD  <9,3,3,1>;
        X64acbd [4][1][1][1] = xACBD  <9,3,3,3>;
        X64adbc [4][1][1][1] = xADBC  <9,3,3,3>;
        X64bdac [4][1][1][1] = xBDAC  <9,3,3,3>;
        X64bcad [4][1][1][1] = xBCAD  <9,3,3,3>;
        X64acbd [4][1][2][0] = xACBD  <9,3,5,1>;
        X64adbc [4][1][2][0] = xADBC  <9,3,5,1>;
        X64bdac [4][1][2][0] = xBDAC  <9,3,5,1>;
        X64bcad [4][1][2][0] = xBCAD  <9,3,5,1>;
        X64acbd [4][1][2][1] = xACBD  <9,3,5,3>;
        X64adbc [4][1][2][1] = xADBC  <9,3,5,3>;
        X64bdac [4][1][2][1] = xBDAC  <9,3,5,3>;
        X64bcad [4][1][2][1] = xBCAD  <9,3,5,3>;
        X64acbd [4][1][2][2] = xACBD  <9,3,5,5>;
        X64adbc [4][1][2][2] = xADBC  <9,3,5,5>;
        X64bdac [4][1][2][2] = xBDAC  <9,3,5,5>;
        X64bcad [4][1][2][2] = xBCAD  <9,3,5,5>;
        X64acbd [4][1][3][0] = xACBD  <9,3,7,1>;
        X64adbc [4][1][3][0] = xADBC  <9,3,7,1>;
        X64bdac [4][1][3][0] = xBDAC  <9,3,7,1>;
        X64bcad [4][1][3][0] = xBCAD  <9,3,7,1>;
        X64acbd [4][1][3][1] = xACBD  <9,3,7,3>;
        X64adbc [4][1][3][1] = xADBC  <9,3,7,3>;
        X64bdac [4][1][3][1] = xBDAC  <9,3,7,3>;
        X64bcad [4][1][3][1] = xBCAD  <9,3,7,3>;
        X64acbd [4][1][3][2] = xACBD  <9,3,7,5>;
        X64adbc [4][1][3][2] = xADBC  <9,3,7,5>;
        X64bdac [4][1][3][2] = xBDAC  <9,3,7,5>;
        X64bcad [4][1][3][2] = xBCAD  <9,3,7,5>;
        X64acbd [4][1][3][3] = xACBD  <9,3,7,7>;
        X64adbc [4][1][3][3] = xADBC  <9,3,7,7>;
        X64bdac [4][1][3][3] = xBDAC  <9,3,7,7>;
        X64bcad [4][1][3][3] = xBCAD  <9,3,7,7>;
        X64acbd [4][1][4][0] = xACBD  <9,3,9,1>;
        X64adbc [4][1][4][0] = xADBC  <9,3,9,1>;
        X64bdac [4][1][4][0] = xBDAC  <9,3,9,1>;
        X64bcad [4][1][4][0] = xBCAD  <9,3,9,1>;
        X64acbd [4][1][4][1] = xACBD  <9,3,9,3>;
        X64adbc [4][1][4][1] = xADBC  <9,3,9,3>;
        X64bdac [4][1][4][1] = xBDAC  <9,3,9,3>;
        X64bcad [4][1][4][1] = xBCAD  <9,3,9,3>;
        X64acbd [4][1][4][2] = xACBD  <9,3,9,5>;
        X64adbc [4][1][4][2] = xADBC  <9,3,9,5>;
        X64bdac [4][1][4][2] = xBDAC  <9,3,9,5>;
        X64bcad [4][1][4][2] = xBCAD  <9,3,9,5>;
        X64acbd [4][1][4][3] = xACBD  <9,3,9,7>;
        X64adbc [4][1][4][3] = xADBC  <9,3,9,7>;
        X64bdac [4][1][4][3] = xBDAC  <9,3,9,7>;
        X64bcad [4][1][4][3] = xBCAD  <9,3,9,7>;
        X64acbd [4][1][4][4] = xACBD  <9,3,9,9>;
        X64adbc [4][1][4][4] = xADBC  <9,3,9,9>;
        X64bdac [4][1][4][4] = xBDAC  <9,3,9,9>;
        X64bcad [4][1][4][4] = xBCAD  <9,3,9,9>;

        X64acbd [4][2][0][0] = xACBD  <9,5,1,1>;
        X64adbc [4][2][0][0] = xADBC  <9,5,1,1>;
        X64bdac [4][2][0][0] = xBDAC  <9,5,1,1>;
        X64bcad [4][2][0][0] = xBCAD  <9,5,1,1>;
        X64acbd [4][2][1][0] = xACBD  <9,5,3,1>;
        X64adbc [4][2][1][0] = xADBC  <9,5,3,1>;
        X64bdac [4][2][1][0] = xBDAC  <9,5,3,1>;
        X64bcad [4][2][1][0] = xBCAD  <9,5,3,1>;
        X64acbd [4][2][1][1] = xACBD  <9,5,3,3>;
        X64adbc [4][2][1][1] = xADBC  <9,5,3,3>;
        X64bdac [4][2][1][1] = xBDAC  <9,5,3,3>;
        X64bcad [4][2][1][1] = xBCAD  <9,5,3,3>;
        X64acbd [4][2][2][0] = xACBD  <9,5,5,1>;
        X64adbc [4][2][2][0] = xADBC  <9,5,5,1>;
        X64bdac [4][2][2][0] = xBDAC  <9,5,5,1>;
        X64bcad [4][2][2][0] = xBCAD  <9,5,5,1>;
        X64acbd [4][2][2][1] = xACBD  <9,5,5,3>;
        X64adbc [4][2][2][1] = xADBC  <9,5,5,3>;
        X64bdac [4][2][2][1] = xBDAC  <9,5,5,3>;
        X64bcad [4][2][2][1] = xBCAD  <9,5,5,3>;
        X64acbd [4][2][2][2] = xACBD  <9,5,5,5>;
        X64adbc [4][2][2][2] = xADBC  <9,5,5,5>;
        X64bdac [4][2][2][2] = xBDAC  <9,5,5,5>;
        X64bcad [4][2][2][2] = xBCAD  <9,5,5,5>;
        X64acbd [4][2][3][0] = xACBD  <9,5,7,1>;
        X64adbc [4][2][3][0] = xADBC  <9,5,7,1>;
        X64bdac [4][2][3][0] = xBDAC  <9,5,7,1>;
        X64bcad [4][2][3][0] = xBCAD  <9,5,7,1>;
        X64acbd [4][2][3][1] = xACBD  <9,5,7,3>;
        X64adbc [4][2][3][1] = xADBC  <9,5,7,3>;
        X64bdac [4][2][3][1] = xBDAC  <9,5,7,3>;
        X64bcad [4][2][3][1] = xBCAD  <9,5,7,3>;
        X64acbd [4][2][3][2] = xACBD  <9,5,7,5>;
        X64adbc [4][2][3][2] = xADBC  <9,5,7,5>;
        X64bdac [4][2][3][2] = xBDAC  <9,5,7,5>;
        X64bcad [4][2][3][2] = xBCAD  <9,5,7,5>;
        X64acbd [4][2][3][3] = xACBD  <9,5,7,7>;
        X64adbc [4][2][3][3] = xADBC  <9,5,7,7>;
        X64bdac [4][2][3][3] = xBDAC  <9,5,7,7>;
        X64bcad [4][2][3][3] = xBCAD  <9,5,7,7>;
        X64acbd [4][2][4][0] = xACBD  <9,5,9,1>;
        X64adbc [4][2][4][0] = xADBC  <9,5,9,1>;
        X64bdac [4][2][4][0] = xBDAC  <9,5,9,1>;
        X64bcad [4][2][4][0] = xBCAD  <9,5,9,1>;
        X64acbd [4][2][4][1] = xACBD  <9,5,9,3>;
        X64adbc [4][2][4][1] = xADBC  <9,5,9,3>;
        X64bdac [4][2][4][1] = xBDAC  <9,5,9,3>;
        X64bcad [4][2][4][1] = xBCAD  <9,5,9,3>;
        X64acbd [4][2][4][2] = xACBD  <9,5,9,5>;
        X64adbc [4][2][4][2] = xADBC  <9,5,9,5>;
        X64bdac [4][2][4][2] = xBDAC  <9,5,9,5>;
        X64bcad [4][2][4][2] = xBCAD  <9,5,9,5>;
        X64acbd [4][2][4][3] = xACBD  <9,5,9,7>;
        X64adbc [4][2][4][3] = xADBC  <9,5,9,7>;
        X64bdac [4][2][4][3] = xBDAC  <9,5,9,7>;
        X64bcad [4][2][4][3] = xBCAD  <9,5,9,7>;
        X64acbd [4][2][4][4] = xACBD  <9,5,9,9>;
        X64adbc [4][2][4][4] = xADBC  <9,5,9,9>;
        X64bdac [4][2][4][4] = xBDAC  <9,5,9,9>;
        X64bcad [4][2][4][4] = xBCAD  <9,5,9,9>;

        X64acbd [4][3][0][0] = xACBD  <9,7,1,1>;
        X64adbc [4][3][0][0] = xADBC  <9,7,1,1>;
        X64bdac [4][3][0][0] = xBDAC  <9,7,1,1>;
        X64bcad [4][3][0][0] = xBCAD  <9,7,1,1>;
        X64acbd [4][3][1][0] = xACBD  <9,7,3,1>;
        X64adbc [4][3][1][0] = xADBC  <9,7,3,1>;
        X64bdac [4][3][1][0] = xBDAC  <9,7,3,1>;
        X64bcad [4][3][1][0] = xBCAD  <9,7,3,1>;
        X64acbd [4][3][1][1] = xACBD  <9,7,3,3>;
        X64adbc [4][3][1][1] = xADBC  <9,7,3,3>;
        X64bdac [4][3][1][1] = xBDAC  <9,7,3,3>;
        X64bcad [4][3][1][1] = xBCAD  <9,7,3,3>;
        X64acbd [4][3][2][0] = xACBD  <9,7,5,1>;
        X64adbc [4][3][2][0] = xADBC  <9,7,5,1>;
        X64bdac [4][3][2][0] = xBDAC  <9,7,5,1>;
        X64bcad [4][3][2][0] = xBCAD  <9,7,5,1>;
        X64acbd [4][3][2][1] = xACBD  <9,7,5,3>;
        X64adbc [4][3][2][1] = xADBC  <9,7,5,3>;
        X64bdac [4][3][2][1] = xBDAC  <9,7,5,3>;
        X64bcad [4][3][2][1] = xBCAD  <9,7,5,3>;
        X64acbd [4][3][2][2] = xACBD  <9,7,5,5>;
        X64adbc [4][3][2][2] = xADBC  <9,7,5,5>;
        X64bdac [4][3][2][2] = xBDAC  <9,7,5,5>;
        X64bcad [4][3][2][2] = xBCAD  <9,7,5,5>;
        X64acbd [4][3][3][0] = xACBD  <9,7,7,1>;
        X64adbc [4][3][3][0] = xADBC  <9,7,7,1>;
        X64bdac [4][3][3][0] = xBDAC  <9,7,7,1>;
        X64bcad [4][3][3][0] = xBCAD  <9,7,7,1>;
        X64acbd [4][3][3][1] = xACBD  <9,7,7,3>;
        X64adbc [4][3][3][1] = xADBC  <9,7,7,3>;
        X64bdac [4][3][3][1] = xBDAC  <9,7,7,3>;
        X64bcad [4][3][3][1] = xBCAD  <9,7,7,3>;
        X64acbd [4][3][3][2] = xACBD  <9,7,7,5>;
        X64adbc [4][3][3][2] = xADBC  <9,7,7,5>;
        X64bdac [4][3][3][2] = xBDAC  <9,7,7,5>;
        X64bcad [4][3][3][2] = xBCAD  <9,7,7,5>;
        X64acbd [4][3][3][3] = xACBD  <9,7,7,7>;
        X64adbc [4][3][3][3] = xADBC  <9,7,7,7>;
        X64bdac [4][3][3][3] = xBDAC  <9,7,7,7>;
        X64bcad [4][3][3][3] = xBCAD  <9,7,7,7>;
        X64acbd [4][3][4][0] = xACBD  <9,7,9,1>;
        X64adbc [4][3][4][0] = xADBC  <9,7,9,1>;
        X64bdac [4][3][4][0] = xBDAC  <9,7,9,1>;
        X64bcad [4][3][4][0] = xBCAD  <9,7,9,1>;
        X64acbd [4][3][4][1] = xACBD  <9,7,9,3>;
        X64adbc [4][3][4][1] = xADBC  <9,7,9,3>;
        X64bdac [4][3][4][1] = xBDAC  <9,7,9,3>;
        X64bcad [4][3][4][1] = xBCAD  <9,7,9,3>;
        X64acbd [4][3][4][2] = xACBD  <9,7,9,5>;
        X64adbc [4][3][4][2] = xADBC  <9,7,9,5>;
        X64bdac [4][3][4][2] = xBDAC  <9,7,9,5>;
        X64bcad [4][3][4][2] = xBCAD  <9,7,9,5>;
        X64acbd [4][3][4][3] = xACBD  <9,7,9,7>;
        X64adbc [4][3][4][3] = xADBC  <9,7,9,7>;
        X64bdac [4][3][4][3] = xBDAC  <9,7,9,7>;
        X64bcad [4][3][4][3] = xBCAD  <9,7,9,7>;
        X64acbd [4][3][4][4] = xACBD  <9,7,9,9>;
        X64adbc [4][3][4][4] = xADBC  <9,7,9,9>;
        X64bdac [4][3][4][4] = xBDAC  <9,7,9,9>;
        X64bcad [4][3][4][4] = xBCAD  <9,7,9,9>;

        X64acbd [4][4][0][0] = xACBD  <9,9,1,1>;
        X64adbc [4][4][0][0] = xADBC  <9,9,1,1>;
        X64bdac [4][4][0][0] = xBDAC  <9,9,1,1>;
        X64bcad [4][4][0][0] = xBCAD  <9,9,1,1>;
        X64acbd [4][4][1][0] = xACBD  <9,9,3,1>;
        X64adbc [4][4][1][0] = xADBC  <9,9,3,1>;
        X64bdac [4][4][1][0] = xBDAC  <9,9,3,1>;
        X64bcad [4][4][1][0] = xBCAD  <9,9,3,1>;
        X64acbd [4][4][1][1] = xACBD  <9,9,3,3>;
        X64adbc [4][4][1][1] = xADBC  <9,9,3,3>;
        X64bdac [4][4][1][1] = xBDAC  <9,9,3,3>;
        X64bcad [4][4][1][1] = xBCAD  <9,9,3,3>;
        X64acbd [4][4][2][0] = xACBD  <9,9,5,1>;
        X64adbc [4][4][2][0] = xADBC  <9,9,5,1>;
        X64bdac [4][4][2][0] = xBDAC  <9,9,5,1>;
        X64bcad [4][4][2][0] = xBCAD  <9,9,5,1>;
        X64acbd [4][4][2][1] = xACBD  <9,9,5,3>;
        X64adbc [4][4][2][1] = xADBC  <9,9,5,3>;
        X64bdac [4][4][2][1] = xBDAC  <9,9,5,3>;
        X64bcad [4][4][2][1] = xBCAD  <9,9,5,3>;
        X64acbd [4][4][2][2] = xACBD  <9,9,5,5>;
        X64adbc [4][4][2][2] = xADBC  <9,9,5,5>;
        X64bdac [4][4][2][2] = xBDAC  <9,9,5,5>;
        X64bcad [4][4][2][2] = xBCAD  <9,9,5,5>;
        X64acbd [4][4][3][0] = xACBD  <9,9,7,1>;
        X64adbc [4][4][3][0] = xADBC  <9,9,7,1>;
        X64bdac [4][4][3][0] = xBDAC  <9,9,7,1>;
        X64bcad [4][4][3][0] = xBCAD  <9,9,7,1>;
        X64acbd [4][4][3][1] = xACBD  <9,9,7,3>;
        X64adbc [4][4][3][1] = xADBC  <9,9,7,3>;
        X64bdac [4][4][3][1] = xBDAC  <9,9,7,3>;
        X64bcad [4][4][3][1] = xBCAD  <9,9,7,3>;
        X64acbd [4][4][3][2] = xACBD  <9,9,7,5>;
        X64adbc [4][4][3][2] = xADBC  <9,9,7,5>;
        X64bdac [4][4][3][2] = xBDAC  <9,9,7,5>;
        X64bcad [4][4][3][2] = xBCAD  <9,9,7,5>;
        X64acbd [4][4][3][3] = xACBD  <9,9,7,7>;
        X64adbc [4][4][3][3] = xADBC  <9,9,7,7>;
        X64bdac [4][4][3][3] = xBDAC  <9,9,7,7>;
        X64bcad [4][4][3][3] = xBCAD  <9,9,7,7>;
        X64acbd [4][4][4][0] = xACBD  <9,9,9,1>;
        X64adbc [4][4][4][0] = xADBC  <9,9,9,1>;
        X64bdac [4][4][4][0] = xBDAC  <9,9,9,1>;
        X64bcad [4][4][4][0] = xBCAD  <9,9,9,1>;
        X64acbd [4][4][4][1] = xACBD  <9,9,9,3>;
        X64adbc [4][4][4][1] = xADBC  <9,9,9,3>;
        X64bdac [4][4][4][1] = xBDAC  <9,9,9,3>;
        X64bcad [4][4][4][1] = xBCAD  <9,9,9,3>;
        X64acbd [4][4][4][2] = xACBD  <9,9,9,5>;
        X64adbc [4][4][4][2] = xADBC  <9,9,9,5>;
        X64bdac [4][4][4][2] = xBDAC  <9,9,9,5>;
        X64bcad [4][4][4][2] = xBCAD  <9,9,9,5>;
        X64acbd [4][4][4][3] = xACBD  <9,9,9,7>;
        X64adbc [4][4][4][3] = xADBC  <9,9,9,7>;
        X64bdac [4][4][4][3] = xBDAC  <9,9,9,7>;
        X64bcad [4][4][4][3] = xBCAD  <9,9,9,7>;
        X64acbd [4][4][4][4] = xACBD  <9,9,9,9>;
        X64adbc [4][4][4][4] = xADBC  <9,9,9,9>;
        X64bdac [4][4][4][4] = xBDAC  <9,9,9,9>;
        X64bcad [4][4][4][4] = xBCAD  <9,9,9,9>;
    }

    //JXC NS
    {
        X64acbc [0][0][0] = xACBD  <1,1,1,1>;
        X64bcac [0][0][0] = xBDAC  <1,1,1,1>;
        X64acbc [0][0][1] = xACBD  <1,1,3,3>;
        X64bcac [0][0][1] = xBDAC  <1,1,3,3>;
        X64acbc [0][0][2] = xACBD  <1,1,5,5>;
        X64bcac [0][0][2] = xBDAC  <1,1,5,5>;
        X64acbc [0][0][3] = xACBD  <1,1,7,7>;
        X64bcac [0][0][3] = xBDAC  <1,1,7,7>;
        X64acbc [0][0][4] = xACBD  <1,1,9,9>;
        X64bcac [0][0][4] = xBDAC  <1,1,9,9>;

        X64acbc [1][0][0] = xACBD  <3,1,1,1>;
        X64bcac [1][0][0] = xBDAC  <3,1,1,1>;
        X64acbc [1][0][1] = xACBD  <3,1,3,3>;
        X64bcac [1][0][1] = xBDAC  <3,1,3,3>;
        X64acbc [1][0][2] = xACBD  <3,1,5,5>;
        X64bcac [1][0][2] = xBDAC  <3,1,5,5>;
        X64acbc [1][0][3] = xACBD  <3,1,7,7>;
        X64bcac [1][0][3] = xBDAC  <3,1,7,7>;
        X64acbc [1][0][4] = xACBD  <3,1,9,9>;
        X64bcac [1][0][4] = xBDAC  <3,1,9,9>;

        X64acbc [1][1][0] = xACBD  <3,3,1,1>;
        X64bcac [1][1][0] = xBDAC  <3,3,1,1>;
        X64acbc [1][1][1] = xACBD  <3,3,3,3>;
        X64bcac [1][1][1] = xBDAC  <3,3,3,3>;
        X64acbc [1][1][2] = xACBD  <3,3,5,5>;
        X64bcac [1][1][2] = xBDAC  <3,3,5,5>;
        X64acbc [1][1][3] = xACBD  <3,3,7,7>;
        X64bcac [1][1][3] = xBDAC  <3,3,7,7>;
        X64acbc [1][1][4] = xACBD  <3,3,9,9>;
        X64bcac [1][1][4] = xBDAC  <3,3,9,9>;

        X64acbc [2][0][0] = xACBD  <5,1,1,1>;
        X64bcac [2][0][0] = xBDAC  <5,1,1,1>;
        X64acbc [2][0][1] = xACBD  <5,1,3,3>;
        X64bcac [2][0][1] = xBDAC  <5,1,3,3>;
        X64acbc [2][0][2] = xACBD  <5,1,5,5>;
        X64bcac [2][0][2] = xBDAC  <5,1,5,5>;
        X64acbc [2][0][3] = xACBD  <5,1,7,7>;
        X64bcac [2][0][3] = xBDAC  <5,1,7,7>;
        X64acbc [2][0][4] = xACBD  <5,1,9,9>;
        X64bcac [2][0][4] = xBDAC  <5,1,9,9>;

        X64acbc [2][1][0] = xACBD  <5,3,1,1>;
        X64bcac [2][1][0] = xBDAC  <5,3,1,1>;
        X64acbc [2][1][1] = xACBD  <5,3,3,3>;
        X64bcac [2][1][1] = xBDAC  <5,3,3,3>;
        X64acbc [2][1][2] = xACBD  <5,3,5,5>;
        X64bcac [2][1][2] = xBDAC  <5,3,5,5>;
        X64acbc [2][1][3] = xACBD  <5,3,7,7>;
        X64bcac [2][1][3] = xBDAC  <5,3,7,7>;
        X64acbc [2][1][4] = xACBD  <5,3,9,9>;
        X64bcac [2][1][4] = xBDAC  <5,3,9,9>;

        X64acbc [2][2][0] = xACBD  <5,5,1,1>;
        X64bcac [2][2][0] = xBDAC  <5,5,1,1>;
        X64acbc [2][2][1] = xACBD  <5,5,3,3>;
        X64bcac [2][2][1] = xBDAC  <5,5,3,3>;
        X64acbc [2][2][2] = xACBD  <5,5,5,5>;
        X64bcac [2][2][2] = xBDAC  <5,5,5,5>;
        X64acbc [2][2][3] = xACBD  <5,5,7,7>;
        X64bcac [2][2][3] = xBDAC  <5,5,7,7>;
        X64acbc [2][2][4] = xACBD  <5,5,9,9>;
        X64bcac [2][2][4] = xBDAC  <5,5,9,9>;

        X64acbc [3][0][0] = xACBD  <7,1,1,1>;
        X64bcac [3][0][0] = xBDAC  <7,1,1,1>;
        X64acbc [3][0][1] = xACBD  <7,1,3,3>;
        X64bcac [3][0][1] = xBDAC  <7,1,3,3>;
        X64acbc [3][0][2] = xACBD  <7,1,5,5>;
        X64bcac [3][0][2] = xBDAC  <7,1,5,5>;
        X64acbc [3][0][3] = xACBD  <7,1,7,7>;
        X64bcac [3][0][3] = xBDAC  <7,1,7,7>;
        X64acbc [3][0][4] = xACBD  <7,1,9,9>;
        X64bcac [3][0][4] = xBDAC  <7,1,9,9>;

        X64acbc [3][1][0] = xACBD  <7,3,1,1>;
        X64bcac [3][1][0] = xBDAC  <7,3,1,1>;
        X64acbc [3][1][1] = xACBD  <7,3,3,3>;
        X64bcac [3][1][1] = xBDAC  <7,3,3,3>;
        X64acbc [3][1][2] = xACBD  <7,3,5,5>;
        X64bcac [3][1][2] = xBDAC  <7,3,5,5>;
        X64acbc [3][1][3] = xACBD  <7,3,7,7>;
        X64bcac [3][1][3] = xBDAC  <7,3,7,7>;
        X64acbc [3][1][4] = xACBD  <7,3,9,9>;
        X64bcac [3][1][4] = xBDAC  <7,3,9,9>;

        X64acbc [3][2][0] = xACBD  <7,5,1,1>;
        X64bcac [3][2][0] = xBDAC  <7,5,1,1>;
        X64acbc [3][2][1] = xACBD  <7,5,3,3>;
        X64bcac [3][2][1] = xBDAC  <7,5,3,3>;
        X64acbc [3][2][2] = xACBD  <7,5,5,5>;
        X64bcac [3][2][2] = xBDAC  <7,5,5,5>;
        X64acbc [3][2][3] = xACBD  <7,5,7,7>;
        X64bcac [3][2][3] = xBDAC  <7,5,7,7>;
        X64acbc [3][2][4] = xACBD  <7,5,9,9>;
        X64bcac [3][2][4] = xBDAC  <7,5,9,9>;

        X64acbc [3][3][0] = xACBD  <7,7,1,1>;
        X64bcac [3][3][0] = xBDAC  <7,7,1,1>;
        X64acbc [3][3][1] = xACBD  <7,7,3,3>;
        X64bcac [3][3][1] = xBDAC  <7,7,3,3>;
        X64acbc [3][3][2] = xACBD  <7,7,5,5>;
        X64bcac [3][3][2] = xBDAC  <7,7,5,5>;
        X64acbc [3][3][3] = xACBD  <7,7,7,7>;
        X64bcac [3][3][3] = xBDAC  <7,7,7,7>;
        X64acbc [3][3][4] = xACBD  <7,7,9,9>;
        X64bcac [3][3][4] = xBDAC  <7,7,9,9>;

        X64acbc [4][0][0] = xACBD  <9,1,1,1>;
        X64bcac [4][0][0] = xBDAC  <9,1,1,1>;
        X64acbc [4][0][1] = xACBD  <9,1,3,3>;
        X64bcac [4][0][1] = xBDAC  <9,1,3,3>;
        X64acbc [4][0][2] = xACBD  <9,1,5,5>;
        X64bcac [4][0][2] = xBDAC  <9,1,5,5>;
        X64acbc [4][0][3] = xACBD  <9,1,7,7>;
        X64bcac [4][0][3] = xBDAC  <9,1,7,7>;
        X64acbc [4][0][4] = xACBD  <9,1,9,9>;
        X64bcac [4][0][4] = xBDAC  <9,1,9,9>;

        X64acbc [4][1][0] = xACBD  <9,3,1,1>;
        X64bcac [4][1][0] = xBDAC  <9,3,1,1>;
        X64acbc [4][1][1] = xACBD  <9,3,3,3>;
        X64bcac [4][1][1] = xBDAC  <9,3,3,3>;
        X64acbc [4][1][2] = xACBD  <9,3,5,5>;
        X64bcac [4][1][2] = xBDAC  <9,3,5,5>;
        X64acbc [4][1][3] = xACBD  <9,3,7,7>;
        X64bcac [4][1][3] = xBDAC  <9,3,7,7>;
        X64acbc [4][1][4] = xACBD  <9,3,9,9>;
        X64bcac [4][1][4] = xBDAC  <9,3,9,9>;

        X64acbc [4][2][0] = xACBD  <9,5,1,1>;
        X64bcac [4][2][0] = xBDAC  <9,5,1,1>;
        X64acbc [4][2][1] = xACBD  <9,5,3,3>;
        X64bcac [4][2][1] = xBDAC  <9,5,3,3>;
        X64acbc [4][2][2] = xACBD  <9,5,5,5>;
        X64bcac [4][2][2] = xBDAC  <9,5,5,5>;
        X64acbc [4][2][3] = xACBD  <9,5,7,7>;
        X64bcac [4][2][3] = xBDAC  <9,5,7,7>;
        X64acbc [4][2][4] = xACBD  <9,5,9,9>;
        X64bcac [4][2][4] = xBDAC  <9,5,9,9>;

        X64acbc [4][3][0] = xACBD  <9,7,1,1>;
        X64bcac [4][3][0] = xBDAC  <9,7,1,1>;
        X64acbc [4][3][1] = xACBD  <9,7,3,3>;
        X64bcac [4][3][1] = xBDAC  <9,7,3,3>;
        X64acbc [4][3][2] = xACBD  <9,7,5,5>;
        X64bcac [4][3][2] = xBDAC  <9,7,5,5>;
        X64acbc [4][3][3] = xACBD  <9,7,7,7>;
        X64bcac [4][3][3] = xBDAC  <9,7,7,7>;
        X64acbc [4][3][4] = xACBD  <9,7,9,9>;
        X64bcac [4][3][4] = xBDAC  <9,7,9,9>;
        X64acbc [4][4][0] = xACBD  <9,9,1,1>;
        X64bcac [4][4][0] = xBDAC  <9,9,1,1>;
        X64acbc [4][4][1] = xACBD  <9,9,3,3>;
        X64bcac [4][4][1] = xBDAC  <9,9,3,3>;
        X64acbc [4][4][2] = xACBD  <9,9,5,5>;
        X64bcac [4][4][2] = xBDAC  <9,9,5,5>;
        X64acbc [4][4][3] = xACBD  <9,9,7,7>;
        X64bcac [4][4][3] = xBDAC  <9,9,7,7>;
        X64acbc [4][4][4] = xACBD  <9,9,9,9>;
        X64bcac [4][4][4] = xBDAC  <9,9,9,9>;
    }

    //JXC SN
    {
        X64acad [0][0][0] = xACBD  <1,1,1,1>;
        X64adac [0][0][0] = xADBC  <1,1,1,1>;
        X64acad [0][1][0] = xACBD  <1,1,3,1>;
        X64adac [0][1][0] = xADBC  <1,1,3,1>;
        X64acad [0][1][1] = xACBD  <1,1,3,3>;
        X64adac [0][1][1] = xADBC  <1,1,3,3>;
        X64acad [0][2][0] = xACBD  <1,1,5,1>;
        X64adac [0][2][0] = xADBC  <1,1,5,1>;
        X64acad [0][2][1] = xACBD  <1,1,5,3>;
        X64adac [0][2][1] = xADBC  <1,1,5,3>;
        X64acad [0][2][2] = xACBD  <1,1,5,5>;
        X64adac [0][2][2] = xADBC  <1,1,5,5>;
        X64acad [0][3][0] = xACBD  <1,1,7,1>;
        X64adac [0][3][0] = xADBC  <1,1,7,1>;
        X64acad [0][3][1] = xACBD  <1,1,7,3>;
        X64adac [0][3][1] = xADBC  <1,1,7,3>;
        X64acad [0][3][2] = xACBD  <1,1,7,5>;
        X64adac [0][3][2] = xADBC  <1,1,7,5>;
        X64acad [0][3][3] = xACBD  <1,1,7,7>;
        X64adac [0][3][3] = xADBC  <1,1,7,7>;
        X64acad [0][4][0] = xACBD  <1,1,9,1>;
        X64adac [0][4][0] = xADBC  <1,1,9,1>;
        X64acad [0][4][1] = xACBD  <1,1,9,3>;
        X64adac [0][4][1] = xADBC  <1,1,9,3>;
        X64acad [0][4][2] = xACBD  <1,1,9,5>;
        X64adac [0][4][2] = xADBC  <1,1,9,5>;
        X64acad [0][4][3] = xACBD  <1,1,9,7>;
        X64adac [0][4][3] = xADBC  <1,1,9,7>;
        X64acad [0][4][4] = xACBD  <1,1,9,9>;
        X64adac [0][4][4] = xADBC  <1,1,9,9>;
        X64acad [1][0][0] = xACBD  <3,3,1,1>;
        X64adac [1][0][0] = xADBC  <3,3,1,1>;
        X64acad [1][1][0] = xACBD  <3,3,3,1>;
        X64adac [1][1][0] = xADBC  <3,3,3,1>;
        X64acad [1][1][1] = xACBD  <3,3,3,3>;
        X64adac [1][1][1] = xADBC  <3,3,3,3>;
        X64acad [1][2][0] = xACBD  <3,3,5,1>;
        X64adac [1][2][0] = xADBC  <3,3,5,1>;
        X64acad [1][2][1] = xACBD  <3,3,5,3>;
        X64adac [1][2][1] = xADBC  <3,3,5,3>;
        X64acad [1][2][2] = xACBD  <3,3,5,5>;
        X64adac [1][2][2] = xADBC  <3,3,5,5>;
        X64acad [1][3][0] = xACBD  <3,3,7,1>;
        X64adac [1][3][0] = xADBC  <3,3,7,1>;
        X64acad [1][3][1] = xACBD  <3,3,7,3>;
        X64adac [1][3][1] = xADBC  <3,3,7,3>;
        X64acad [1][3][2] = xACBD  <3,3,7,5>;
        X64adac [1][3][2] = xADBC  <3,3,7,5>;
        X64acad [1][3][3] = xACBD  <3,3,7,7>;
        X64adac [1][3][3] = xADBC  <3,3,7,7>;
        X64acad [1][4][0] = xACBD  <3,3,9,1>;
        X64adac [1][4][0] = xADBC  <3,3,9,1>;
        X64acad [1][4][1] = xACBD  <3,3,9,3>;
        X64adac [1][4][1] = xADBC  <3,3,9,3>;
        X64acad [1][4][2] = xACBD  <3,3,9,5>;
        X64adac [1][4][2] = xADBC  <3,3,9,5>;
        X64acad [1][4][3] = xACBD  <3,3,9,7>;
        X64adac [1][4][3] = xADBC  <3,3,9,7>;
        X64acad [1][4][4] = xACBD  <3,3,9,9>;
        X64adac [1][4][4] = xADBC  <3,3,9,9>;
        X64acad [2][0][0] = xACBD  <5,5,1,1>;
        X64adac [2][0][0] = xADBC  <5,5,1,1>;
        X64acad [2][1][0] = xACBD  <5,5,3,1>;
        X64adac [2][1][0] = xADBC  <5,5,3,1>;
        X64acad [2][1][1] = xACBD  <5,5,3,3>;
        X64adac [2][1][1] = xADBC  <5,5,3,3>;
        X64acad [2][2][0] = xACBD  <5,5,5,1>;
        X64adac [2][2][0] = xADBC  <5,5,5,1>;
        X64acad [2][2][1] = xACBD  <5,5,5,3>;
        X64adac [2][2][1] = xADBC  <5,5,5,3>;
        X64acad [2][2][2] = xACBD  <5,5,5,5>;
        X64adac [2][2][2] = xADBC  <5,5,5,5>;
        X64acad [2][3][0] = xACBD  <5,5,7,1>;
        X64adac [2][3][0] = xADBC  <5,5,7,1>;
        X64acad [2][3][1] = xACBD  <5,5,7,3>;
        X64adac [2][3][1] = xADBC  <5,5,7,3>;
        X64acad [2][3][2] = xACBD  <5,5,7,5>;
        X64adac [2][3][2] = xADBC  <5,5,7,5>;
        X64acad [2][3][3] = xACBD  <5,5,7,7>;
        X64adac [2][3][3] = xADBC  <5,5,7,7>;
        X64acad [2][4][0] = xACBD  <5,5,9,1>;
        X64adac [2][4][0] = xADBC  <5,5,9,1>;
        X64acad [2][4][1] = xACBD  <5,5,9,3>;
        X64adac [2][4][1] = xADBC  <5,5,9,3>;
        X64acad [2][4][2] = xACBD  <5,5,9,5>;
        X64adac [2][4][2] = xADBC  <5,5,9,5>;
        X64acad [2][4][3] = xACBD  <5,5,9,7>;
        X64adac [2][4][3] = xADBC  <5,5,9,7>;
        X64acad [2][4][4] = xACBD  <5,5,9,9>;
        X64adac [2][4][4] = xADBC  <5,5,9,9>;
        X64acad [3][0][0] = xACBD  <7,7,1,1>;
        X64adac [3][0][0] = xADBC  <7,7,1,1>;
        X64acad [3][1][0] = xACBD  <7,7,3,1>;
        X64adac [3][1][0] = xADBC  <7,7,3,1>;
        X64acad [3][1][1] = xACBD  <7,7,3,3>;
        X64adac [3][1][1] = xADBC  <7,7,3,3>;
        X64acad [3][2][0] = xACBD  <7,7,5,1>;
        X64adac [3][2][0] = xADBC  <7,7,5,1>;
        X64acad [3][2][1] = xACBD  <7,7,5,3>;
        X64adac [3][2][1] = xADBC  <7,7,5,3>;
        X64acad [3][2][2] = xACBD  <7,7,5,5>;
        X64adac [3][2][2] = xADBC  <7,7,5,5>;
        X64acad [3][3][0] = xACBD  <7,7,7,1>;
        X64adac [3][3][0] = xADBC  <7,7,7,1>;
        X64acad [3][3][1] = xACBD  <7,7,7,3>;
        X64adac [3][3][1] = xADBC  <7,7,7,3>;
        X64acad [3][3][2] = xACBD  <7,7,7,5>;
        X64adac [3][3][2] = xADBC  <7,7,7,5>;
        X64acad [3][3][3] = xACBD  <7,7,7,7>;
        X64adac [3][3][3] = xADBC  <7,7,7,7>;
        X64acad [3][4][0] = xACBD  <7,7,9,1>;
        X64adac [3][4][0] = xADBC  <7,7,9,1>;
        X64acad [3][4][1] = xACBD  <7,7,9,3>;
        X64adac [3][4][1] = xADBC  <7,7,9,3>;
        X64acad [3][4][2] = xACBD  <7,7,9,5>;
        X64adac [3][4][2] = xADBC  <7,7,9,5>;
        X64acad [3][4][3] = xACBD  <7,7,9,7>;
        X64adac [3][4][3] = xADBC  <7,7,9,7>;
        X64acad [3][4][4] = xACBD  <7,7,9,9>;
        X64adac [3][4][4] = xADBC  <7,7,9,9>;
        X64acad [4][0][0] = xACBD  <9,9,1,1>;
        X64adac [4][0][0] = xADBC  <9,9,1,1>;
        X64acad [4][1][0] = xACBD  <9,9,3,1>;
        X64adac [4][1][0] = xADBC  <9,9,3,1>;
        X64acad [4][1][1] = xACBD  <9,9,3,3>;
        X64adac [4][1][1] = xADBC  <9,9,3,3>;
        X64acad [4][2][0] = xACBD  <9,9,5,1>;
        X64adac [4][2][0] = xADBC  <9,9,5,1>;
        X64acad [4][2][1] = xACBD  <9,9,5,3>;
        X64adac [4][2][1] = xADBC  <9,9,5,3>;
        X64acad [4][2][2] = xACBD  <9,9,5,5>;
        X64adac [4][2][2] = xADBC  <9,9,5,5>;
        X64acad [4][3][0] = xACBD  <9,9,7,1>;
        X64adac [4][3][0] = xADBC  <9,9,7,1>;
        X64acad [4][3][1] = xACBD  <9,9,7,3>;
        X64adac [4][3][1] = xADBC  <9,9,7,3>;
        X64acad [4][3][2] = xACBD  <9,9,7,5>;
        X64adac [4][3][2] = xADBC  <9,9,7,5>;
        X64acad [4][3][3] = xACBD  <9,9,7,7>;
        X64adac [4][3][3] = xADBC  <9,9,7,7>;
        X64acad [4][4][0] = xACBD  <9,9,9,1>;
        X64adac [4][4][0] = xADBC  <9,9,9,1>;
        X64acad [4][4][1] = xACBD  <9,9,9,3>;
        X64adac [4][4][1] = xADBC  <9,9,9,3>;
        X64acad [4][4][2] = xACBD  <9,9,9,5>;
        X64adac [4][4][2] = xADBC  <9,9,9,5>;
        X64acad [4][4][3] = xACBD  <9,9,9,7>;
        X64adac [4][4][3] = xADBC  <9,9,9,7>;
        X64acad [4][4][4] = xACBD  <9,9,9,9>;
        X64adac [4][4][4] = xADBC  <9,9,9,9>;
    }

    //JXC SS
    {
        X64acac [0][0] = xACBD  <1,1,1,1>;
        X64acac [0][1] = xACBD  <1,1,3,3>;
        X64acac [0][2] = xACBD  <1,1,5,5>;
        X64acac [0][3] = xACBD  <1,1,7,7>;
        X64acac [0][4] = xACBD  <1,1,9,9>;
        X64acac [1][0] = xACBD  <3,3,1,1>;
        X64acac [1][1] = xACBD  <3,3,3,3>;
        X64acac [1][2] = xACBD  <3,3,5,5>;
        X64acac [1][3] = xACBD  <3,3,7,7>;
        X64acac [1][4] = xACBD  <3,3,9,9>;
        X64acac [2][0] = xACBD  <5,5,1,1>;
        X64acac [2][1] = xACBD  <5,5,3,3>;
        X64acac [2][2] = xACBD  <5,5,5,5>;
        X64acac [2][3] = xACBD  <5,5,7,7>;
        X64acac [2][4] = xACBD  <5,5,9,9>;
        X64acac [3][0] = xACBD  <7,7,1,1>;
        X64acac [3][1] = xACBD  <7,7,3,3>;
        X64acac [3][2] = xACBD  <7,7,5,5>;
        X64acac [3][3] = xACBD  <7,7,7,7>;
        X64acac [3][4] = xACBD  <7,7,9,9>;
        X64acac [4][0] = xACBD  <9,9,1,1>;
        X64acac [4][1] = xACBD  <9,9,3,3>;
        X64acac [4][2] = xACBD  <9,9,5,5>;
        X64acac [4][3] = xACBD  <9,9,7,7>;
        X64acac [4][4] = xACBD  <9,9,9,9>;
    }

    //JXC S
    {
        X64aabb [0][0] = xAABB  <1,1>;
        X64bbaa [0][0] = xBBAA  <1,1>;
        X64abab [0][0] = xABAB  <1,1>;
        X64aabb [1][0] = xAABB  <3,1>;
        X64bbaa [1][0] = xBBAA  <3,1>;
        X64abab [1][0] = xABAB  <3,1>;
        X64aabb [1][1] = xAABB  <3,3>;
        X64bbaa [1][1] = xBBAA  <3,3>;
        X64abab [1][1] = xABAB  <3,3>;
        X64aabb [2][0] = xAABB  <5,1>;
        X64bbaa [2][0] = xBBAA  <5,1>;
        X64abab [2][0] = xABAB  <5,1>;
        X64aabb [2][1] = xAABB  <5,3>;
        X64bbaa [2][1] = xBBAA  <5,3>;
        X64abab [2][1] = xABAB  <5,3>;
        X64aabb [2][2] = xAABB  <5,5>;
        X64bbaa [2][2] = xBBAA  <5,5>;
        X64abab [2][2] = xABAB  <5,5>;
        X64aabb [3][0] = xAABB  <7,1>;
        X64bbaa [3][0] = xBBAA  <7,1>;
        X64abab [3][0] = xABAB  <7,1>;
        X64aabb [3][1] = xAABB  <7,3>;
        X64bbaa [3][1] = xBBAA  <7,3>;
        X64abab [3][1] = xABAB  <7,3>;
        X64aabb [3][2] = xAABB  <7,5>;
        X64bbaa [3][2] = xBBAA  <7,5>;
        X64abab [3][2] = xABAB  <7,5>;
        X64aabb [3][3] = xAABB  <7,7>;
        X64bbaa [3][3] = xBBAA  <7,7>;
        X64abab [3][3] = xABAB  <7,7>;
        X64aabb [4][0] = xAABB  <9,1>;
        X64bbaa [4][0] = xBBAA  <9,1>;
        X64abab [4][0] = xABAB  <9,1>;
        X64aabb [4][1] = xAABB  <9,3>;
        X64bbaa [4][1] = xBBAA  <9,3>;
        X64abab [4][1] = xABAB  <9,3>;
        X64aabb [4][2] = xAABB  <9,5>;
        X64bbaa [4][2] = xBBAA  <9,5>;
        X64abab [4][2] = xABAB  <9,5>;
        X64aabb [4][3] = xAABB  <9,7>;
        X64bbaa [4][3] = xBBAA  <9,7>;
        X64abab [4][3] = xABAB  <9,7>;
        X64aabb [4][4] = xAABB  <9,9>;
        X64bbaa [4][4] = xBBAA  <9,9>;
        X64abab [4][4] = xABAB  <9,9>;


        X64abba [0][0] = xABBA  <1,1>;
        X64abba [1][0] = xABBA  <3,1>;
        X64abba [1][1] = xABBA  <3,3>;
        X64abba [2][0] = xABBA  <5,1>;
        X64abba [2][1] = xABBA  <5,3>;
        X64abba [2][2] = xABBA  <5,5>;
        X64abba [3][0] = xABBA  <7,1>;
        X64abba [3][1] = xABBA  <7,3>;
        X64abba [3][2] = xABBA  <7,5>;
        X64abba [3][3] = xABBA  <7,7>;
        X64abba [4][0] = xABBA  <9,1>;
        X64abba [4][1] = xABBA  <9,3>;
        X64abba [4][2] = xABBA  <9,5>;
        X64abba [4][3] = xABBA  <9,7>;
        X64abba [4][4] = xABBA  <9,9>;

        X64baab [0][0] = xBAAB  <1,1>;
        X64baab [1][0] = xBAAB  <3,1>;
        X64baab [1][1] = xBAAB  <3,3>;
        X64baab [2][0] = xBAAB  <5,1>;
        X64baab [2][1] = xBAAB  <5,3>;
        X64baab [2][2] = xBAAB  <5,5>;
        X64baab [3][0] = xBAAB  <7,1>;
        X64baab [3][1] = xBAAB  <7,3>;
        X64baab [3][2] = xBAAB  <7,5>;
        X64baab [3][3] = xBAAB  <7,7>;
        X64baab [4][0] = xBAAB  <9,1>;
        X64baab [4][1] = xBAAB  <9,3>;
        X64baab [4][2] = xBAAB  <9,5>;
        X64baab [4][3] = xBAAB  <9,7>;
        X64baab [4][4] = xBAAB  <9,9>;
    }

}
