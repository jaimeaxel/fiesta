#include <Python.h>

#include <iostream>
#include <map>
#include <string>

#include "fiesta/fiesta.hpp"
#include "linear/tensors.hpp"
#include "math/gamma.hpp"
#include "math/angular.hpp"
#include "libquimera/libquimera.hpp"
#include "libechidna/libechidna.hpp"

using namespace std;
using namespace LibAngular;
using namespace LibIGamma;

/*
    PERIODIC TABLE
*/
// this exists to assign nuclear charges; finish if necessary
string PeriodicTable[]={"H",                                                                           "HE",
                        "LI","BE",                                                 "B","C","N","O","F","NE",
                        "NA","MG",                                                "AL","SI","P","S","CL","AR",
                        "K","CA","SC","TI","V","CR","MN","FE","CO","NI","CU","ZN","GA","GE","AS","SE","BR","KR",
                        "RB","SR","Y","ZR","NB","MO","TC","RU","RH","PD","AG","CD","IN","SN","SB","TE","I","XE"};
/*
    AUXILLIARY FUNCTIONS
*/

static string ucase(const string & name) {
    string ret;
    ret = name;
    for (unsigned int i=0; i<=ret.size(); ++i)
        if ((ret[i] > 96) && (ret[i] < 123)) ret[i] -= 32;
    return ret;
}

inline double RadialIntegral(double k, int L) {

    const double hhGamma[] =
        {1.77245385090551603, 0.886226925452758014, 1.32934038817913702, 3.32335097044784255, 11.6317283965674489, 52.3427777845535202,
        287.885277815044361, 1871.25430579778835, 14034.4072934834126, 119292.461994609007, 1.13327838894878557e6,
         1.18994230839622485e7, 1.36843365465565857e8, 1.71054206831957322e9, 2.30923179223142384e10,
         3.34838609873556457e11, 5.18999845304012508e12, 8.56349744751620639e13, 1.49861205331533612e15,
         2.77243229863337182e16, 5.40624298233507504e17, 1.10827981137869038e19, 2.38280159446418433e20,
         5.36130358754441473e21, 1.25990634307293746e23, 3.08677054052869678e24, 7.87126487834817680e25,
         2.08588519276226685e27, 5.73618428009623384e28, 1.63481251982742664e30, 4.82269693349090860e31,
         1.47092256471472712e33};

    double kn = 1;
    for (int i=0; i<2*L+3; ++i)
        kn *= k;
    return hhGamma[L+1] / (2*sqrt(kn));
}

int nBasis (const GTO & g) {
    // return basis count
    return g.J*(2*g.l+1);
}

int nBasis (const rAtom & atom) {

    // count number of function in elemental basis
    int neb = 0;

    for (int g=0; g<atom.rAP->basis.N(); ++g) neb += nBasis(atom.rAP->basis[g]);

    return neb; // return basis count
}

int nBasis (const rAtom * Atoms, int nAtoms) {

    // count the total amount of basis functions
    int nbasis = 0;

    for (int n=0; n<nAtoms; ++n) nbasis += nBasis(Atoms[n]);

    return nbasis;
}

void SetCharges(map <string, rAtomPrototype*> & basis) {
    map <string, int> charges;

    for (int z=0; z<2+8+8+18+18; ++z) {
        charges[PeriodicTable[z]] = z+1;
    }

    map <string, rAtomPrototype*>::iterator it;

    for (it=basis.begin(); it!=basis.end(); ++it) {
        string el = it->first;
        //FIXME: check whether the atom exists in the list
        it->second->Z = charges[el];
    }

}

int nElectrons (const rAtom * Atoms, int nAtoms) {

    int nel=0;
    for (int n=0; n<nAtoms; ++n) nel += Atoms[n].rAP->Z;

    return nel;
}


const int UNSET = 65535;

//        EXTRACT BASIS SET INFORMATION
//        =============================

static void ParseBasis (map <string, rAtomPrototype*> & basis, PyObject * ibset) {

    PyObject * pbasis = PyObject_GetAttrString(ibset, "ebasis");

    // extract the info from the parsed struct
    int Natypes = PyDict_Size(pbasis);
    PyObject * elist = PyDict_Keys   (pbasis);  // list of elements
    PyObject * blist = PyDict_Values (pbasis);  // list of basis

    // iterate over all elements in basis set
    for (int id=0; id<Natypes; ++id) {
        rAtomPrototype * ebasis = new rAtomPrototype;

        ebasis->name  = ucase(PyBytes_AS_STRING(PyList_GetItem(elist,id))); // get element name
        ebasis->mass  =  0 ; // not needed
        ebasis->Z     =  0 ; // not needed

        ebasis->DId   = UNSET; //id;           // Id in the original definition list
        ebasis->RId   = UNSET;           // Id in the reduced element list

        // extract shell list
        PyObject * slist = PyList_GetItem(blist,id);
        int nsh = PyList_Size(slist);

        ebasis->basis.set(nsh);

        // parse all GTOs
        for (int i=0; i<nsh; ++i) {
            PyObject * shell = PyList_GetItem(slist,i); //
            GTO & g = ebasis->basis[i];

            int J,K,L;
            PyObject * as, * cs;

            if (!PyArg_ParseTuple (shell, "iiiOO:bss", &L, &K, &J, &as, &cs)) continue;

            g.l = L;
            g.K = K;
            g.J = J;

            // gaussian exponents
            for (int k=0; k<K; ++k)
                g.k[k] = PyFloat_AsDouble(PyList_GetItem(as,k));

            // list over functions
            for (int j=0; j<J; ++j) {
                PyObject * fs = PyList_GetItem(cs,j);

                for (int k=0; k<K; ++k) {
                    g.N[j][k] = PyFloat_AsDouble(PyList_GetItem(fs,k));
                }
            }

            g.SP = false;

            //radial normalization && SORT EXPONENTS!!!!
            g.Normalize();
        }

        // insert in basis set
        basis[ebasis->name] = ebasis;
    }

}


    //        EXTRACT MOLECULE INFORMATION
    //        ============================
static void ParseMol (rAtom * (&Atoms), int & nAtoms, map <string, rAtomPrototype*> & basis, PyObject * imol) {

    PyObject * patoms = PyObject_GetAttrString(imol, "atoms");

    nAtoms = PyList_Size(patoms);
    Atoms = new rAtom[nAtoms];

    for (int n=0; n<nAtoms; ++n) {
        // extract the atom object
        PyObject * at = PyList_GetItem(patoms,n);

        // parse the atom object
        PyObject * name;
        double rx, ry, rz;
        if (!PyArg_ParseTuple (at, "Sddd:mol", &name, &rx, &ry, &rz)) continue;
        string ename = ucase(PyBytes_AS_STRING(name));

        // assign data to mol structure
        Atoms[n].c.x = AMS*rx;
        Atoms[n].c.y = AMS*ry;
        Atoms[n].c.z = AMS*rz;
        Atoms[n].rAP = basis[ename]; // search by (uppercase) element name
    }
}


inline void SRR(double (&S)[LMAX+2][LMAX+2], double AP, double BP, double k12, int la, int lb) {

    for (int ab=0; ab<(LMAX+2)*(LMAX+2); ++ab)  (&(S[0][0]))[ab] = 0;

    double ik2 = 1/(2*k12);

    //(0,0)
    S[0][0] = sqrt(PI/k12);

    //(0,lb)
    S[0][1] = BP * S[0][0];
    for (int b=1; b<=lb; ++b)
        S[0][b+1] = BP * S[0][b] + b * ik2 * S[0][b-1];

    //(la,lb)
    {
        S[1][0] = AP * S[0][0];
        for (int a=1; a<=la; ++a)
            S[a+1][0] = AP * S[a][0] + a * ik2 * S[a-1][0];
    }

    for (int b=1; b<=lb+1; ++b) {
        S[1][b] = AP * S[0][b] + b * ik2 * S[0][b-1];
        for (int a=1; a<=la; ++a)
            S[a+1][b] = AP * S[a][b] + a * ik2 * S[a-1][b]  + b * ik2 * S[a][b-1];
    }
}

//not really a RR, just named TRR because its formation is parallel to that of SRR
inline void TRR(double (&T)[LMAX+1][LMAX+1], const double (&S)[LMAX+2][LMAX+2], double k1, double k2, int la, int lb) {

    for (int ab=0; ab<(LMAX+1)*(LMAX+1); ++ab)  (&(T[0][0]))[ab] = 0;

    for (int a=0; a<=la; ++a)
        for (int b=0; b<=lb; ++b)
            T[a][b] = 4*k1*k2*S[a+1][b+1];

    for (int a=0; a<=la; ++a)
        for (int b=1; b<=lb; ++b)
            T[a][b] -= 2*k1*b*S[a+1][b-1];

    for (int a=1; a<=la; ++a)
        for (int b=0; b<=lb; ++b)
            T[a][b] -= 2*k2*a*S[a-1][b+1];

    for (int a=1; a<=la; ++a)
        for (int b=1; b<=lb; ++b)
            T[a][b] +=    a*b*S[a-1][b-1];
}



typedef double Triple  [2*LMAX+1][2*LMAX+1][2*LMAX+1];
typedef double TripleX [2*LMAX+3][2*LMAX+3][2*LMAX+3];

typedef void (*Rint)  (Triple &  , double, double, const point &, const point &);
typedef void (*RintX) (TripleX & , double, double, const point &, const point &);

template <int L> void RIntegralXX(TripleX & Rtuv, double k, double RQ, const point & P, const point & N) {

    double Fn[L+1];

    vector3 R;
    R.x = N.x-P.x;
    R.y = N.y-P.y;
    R.z = N.z-P.z;

    double R2 = R.x*R.x + R.y*R.y + R.z*R.z;

    double ** gamma = IncompleteGamma.gamma_table;
    double *  gamma2 = IncompleteGammas[L].gamma_table_vals;

    //calcula las 'F's
    {
        double Sn[L+1];

        double kR2 = k*R2;

        if (kR2>LibIGamma::vg_max-LibIGamma::vg_step) {
            double iR2 = 1/R2;
            Sn[0] = 0.5*sqrt(PI*iR2);

            for (int i=1; i<=L; ++i) {
                Sn[i] = Sn[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = LibIGamma::ivg_step*(kR2-LibIGamma::vg_min);
            int pos = int(p+0.5);
            double x0 = LibIGamma::vg_min + LibIGamma::vg_step*double(pos);
            double Ax1 = x0-kR2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Sn[L] = (gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Sn[i] = (2*kR2*Sn[i+1] + expx)*(1./double(2*i+1));
            }

            double kn   = sqrt(k);

            for (int n=0; n<=L; ++n) {
                Sn[n] *= kn;
                kn    *= 2*k;
            }
        }

        for (int n=0; n<=L; ++n) Fn[n] = Sn[n]; // - Ln[n];
    }


    //calcula R^{n}_{000}
    double ac = (2*PI)/(k*sqrt(k));
    for (int n=0; n<=L; ++n) {
        Fn[n] *= ac;
    }


    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }

}

static inline double R2E(int ax, int ay, int az, const vector3 & PA,           int bx, int by, int bz, const vector3 & PB,           double iz, int px, int py, int pz, Triple & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px-1,py,pz, R) +
               PA.x * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py-1,pz, R) +
               PA.y * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz-1, R) +
               PA.z * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz+1, R);
    }

    if (bx>0) {
        return px   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px-1,py,pz, R) +
               PB.x * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (by>0) {
        return py   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py-1,pz, R) +
               PB.y * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py+1,pz, R);
    }
    if (bz>0) {
        return pz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz-1, R) +
               PB.z * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz+1, R);
    }


    return R[px][py][pz];
}


#define PY_ARRAY_UNIQUE_SYMBOL PyEfsSymbol

extern "C" {
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

static PyObject * efs_getS(PyObject * self, PyObject * args) {

    // parse input objects
    PyObject * imol, * ibset;
    if (!PyArg_ParseTuple (args, "OO", &imol, &ibset)) return NULL;

    rAtom * Atoms;
    int   nAtoms;
    map <string, rAtomPrototype*> basis;

    ParseBasis (               basis, ibset);
    ParseMol   (Atoms, nAtoms, basis, imol);

    int nb = nBasis(Atoms, nAtoms);

    // overlap matrix
    tensor2 S;
    S.setsize(nb);
    S.zeroize();


    // loop over all atoms

    for (int at2=0, it2=0; at2<nAtoms; ++at2) {
        for (int at1=0, it1=0; at1<nAtoms; ++at1) {

            int nb1 = Atoms[at1].rAP->basis.N();
            int nb2 = Atoms[at2].rAP->basis.N();

            point & A = Atoms[at1].c;
            point & B = Atoms[at2].c;
            vector3 AB = B-A;
            double R2 = AB*AB;

            // loop over all elements of the basis
            for (int b2=0,iit2=0; b2<nb2; ++b2) {

                GTO & g2 = Atoms[at2].rAP->basis[b2];

                for (int b1=0,iit1=0; b1<nb1; ++b1) {

                    GTO & g1 = Atoms[at1].rAP->basis[b1];

                    int La = g1.l;
                    int Lb = g2.l;
                    int Ja = g1.J;
                    int Jb = g2.J;

                    // loop over primitives

                    for (int b=0; b<g2.K; ++b) {
                        for (int a=0; a<g1.K; ++a) {

                            double k1, k2, k12, ik2;

                            k1  = g1.k[a];
                            k2  = g2.k[b];
                            k12 = k1+k2;
                            ik2 = 1/(2*k12);

                            point P;
                            vector3 Av;
                            vector3 Bv;

                            P.x = (k1*A.x + k2*B.x)/k12;
                            P.y = (k1*A.y + k2*B.y)/k12;
                            P.z = (k1*A.z + k2*B.z)/k12;

                            Av = P - A;
                            Bv = P - B;

                            // calcula los tensores de propiedades monoelectronicas
                            double Sx[LMAX+2][LMAX+2];
                            double Sy[LMAX+2][LMAX+2];
                            double Sz[LMAX+2][LMAX+2];

                            SRR (Sx, Av.x, Bv.x, k12, La, Lb);
                            SRR (Sy, Av.y, Bv.y, k12, La, Lb);
                            SRR (Sz, Av.z, Bv.z, k12, La, Lb);


                            double wab = exp(-(k1*k2/k12) * R2);

                            double SS[2*LMAX+1][2*LMAX+1];


                            for (int ma=0; ma<nmS[La]; ++ma) {
                                for (int mb=0; mb<nmS[Lb]; ++mb) {
                                    double sumS = 0;

                                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                            const SHTerm & sha = SHList[La][ma].T[i];
                                            const SHTerm & shb = SHList[Lb][mb].T[j];

                                            sumS += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                                        }
                                    }

                                    SS[ma][mb] = sumS;
                                }
                            }


                            // S | X,Y,Z
                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {

                                    double w12 = wab * g1.N[ja][a] * g2.N[jb][b];

                                    for (int ma=0; ma<nmS[La]; ++ma) {
                                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                                            int ffa = it1 + iit1 + ja*nmS[La] + ma;
                                            int ffb = it2 + iit2 + jb*nmS[Lb] + mb;

                                            S(ffa,ffb) += w12 * SS[ma][mb];
                                        }
                                    }
                                }
                            }

                        }
                    }

                    iit1 += nBasis(g1);
                }
                iit2 += nBasis(g2);
            }

            it1 += nBasis(Atoms[at1]);
        }
        it2 += nBasis(Atoms[at2]);
    }

    // return
    npy_intp dims[] = {nb,nb};
    PyObject * Spy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, S.c2);
    S.c2 = NULL; // don't deallocate the array when tensor2 goes out of scope

    return Spy;
}

static PyObject * efs_getT(PyObject * self, PyObject * args) {

    // parse input objects
    PyObject * imol, * ibset;
    if (!PyArg_ParseTuple (args, "OO", &imol, &ibset)) return NULL;

    rAtom * Atoms;
    int   nAtoms;
    map <string, rAtomPrototype*> basis;

    ParseBasis (               basis, ibset);
    ParseMol   (Atoms, nAtoms, basis, imol);

    int nb = nBasis(Atoms, nAtoms);

    // overlap matrix
    tensor2 T;
    T.setsize(nb);
    T.zeroize();


    // loop over all atoms

    for (int at2=0, it2=0; at2<nAtoms; ++at2) {
        for (int at1=0, it1=0; at1<nAtoms; ++at1) {

            int nb1 = Atoms[at1].rAP->basis.N();
            int nb2 = Atoms[at2].rAP->basis.N();

            point & A = Atoms[at1].c;
            point & B = Atoms[at2].c;
            vector3 AB = B-A;
            double R2 = AB*AB;

            // loop over all elements of the basis
            for (int b2=0,iit2=0; b2<nb2; ++b2) {

                GTO & g2 = Atoms[at2].rAP->basis[b2];

                for (int b1=0,iit1=0; b1<nb1; ++b1) {

                    GTO & g1 = Atoms[at1].rAP->basis[b1];

                    int La = g1.l;
                    int Lb = g2.l;
                    int Ja = g1.J;
                    int Jb = g2.J;

                    // loop over primitives

                    for (int b=0; b<g2.K; ++b) {
                        for (int a=0; a<g1.K; ++a) {

                            double k1, k2, k12, ik2;

                            k1  = g1.k[a];
                            k2  = g2.k[b];
                            k12 = k1+k2;
                            ik2 = 1/(2*k12);

                            point P;
                            vector3 Av;
                            vector3 Bv;

                            P.x = (k1*A.x + k2*B.x)/k12;
                            P.y = (k1*A.y + k2*B.y)/k12;
                            P.z = (k1*A.z + k2*B.z)/k12;

                            Av = P - A;
                            Bv = P - B;

                            // calcula los tensores de propiedades monoelectronicas
                            double Sx[LMAX+2][LMAX+2];
                            double Sy[LMAX+2][LMAX+2];
                            double Sz[LMAX+2][LMAX+2];

                            SRR (Sx, Av.x, Bv.x, k12, La, Lb);
                            SRR (Sy, Av.y, Bv.y, k12, La, Lb);
                            SRR (Sz, Av.z, Bv.z, k12, La, Lb);

                            double Tx[LMAX+1][LMAX+1];
                            double Ty[LMAX+1][LMAX+1];
                            double Tz[LMAX+1][LMAX+1];

                            TRR(Tx, Sx, k1, k2, La, Lb);
                            TRR(Ty, Sy, k1, k2, La, Lb);
                            TRR(Tz, Sz, k1, k2, La, Lb);


                            double wab = exp(-(k1*k2/k12) * R2);

                            double TT[2*LMAX+1][2*LMAX+1];


                            for (int ma=0; ma<nmS[La]; ++ma) {
                                for (int mb=0; mb<nmS[Lb]; ++mb) {
                                    double sumT = 0;

                                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                            const SHTerm & sha = SHList[La][ma].T[i];
                                            const SHTerm & shb = SHList[Lb][mb].T[j];

                                            sumT += sha.cN * shb.cN * (Tx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                                       Sx[sha.nx][shb.nx] * Ty[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                                       Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Tz[sha.nz][shb.nz]);
                                        }
                                    }

                                    TT[ma][mb] = 0.5*sumT;
                                }
                            }


                            // S | X,Y,Z
                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {

                                    double w12 = wab * g1.N[ja][a] * g2.N[jb][b];

                                    for (int ma=0; ma<nmS[La]; ++ma) {
                                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                                            int ffa = it1 + iit1 + ja*nmS[La] + ma;
                                            int ffb = it2 + iit2 + jb*nmS[Lb] + mb;

                                            T(ffa,ffb) += w12 * TT[ma][mb];
                                        }
                                    }
                                }
                            }

                        }
                    }

                    iit1 += nBasis(g1);
                }
                iit2 += nBasis(g2);
            }

            it1 += nBasis(Atoms[at1]);
        }
        it2 += nBasis(Atoms[at2]);
    }

    // return
    npy_intp dims[] = {nb,nb};
    PyObject * Tpy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, T.c2);
    T.c2 = NULL; // don't deallocate the array when tensor2 goes out of scope

    return Tpy;
}

static PyObject * efs_getV(PyObject * self, PyObject * args) {

    // parse input objects
    PyObject * imol, * ibset;
    if (!PyArg_ParseTuple (args, "OO", &imol, &ibset)) return NULL;

    rAtom * Atoms;
    int   nAtoms;
    map <string, rAtomPrototype*> basis;

    ParseBasis (               basis, ibset);
    ParseMol   (Atoms, nAtoms, basis, imol);
    SetCharges (basis);

    int nb = nBasis(Atoms, nAtoms);

    // overlap matrix
    tensor2 V;
    V.setsize(nb);
    V.zeroize();


    // loop over all atoms

    for (int at2=0, it2=0; at2<nAtoms; ++at2) {
        for (int at1=0, it1=0; at1<nAtoms; ++at1) {

            int nb1 = Atoms[at1].rAP->basis.N();
            int nb2 = Atoms[at2].rAP->basis.N();

            point & A = Atoms[at1].c;
            point & B = Atoms[at2].c;
            vector3 AB = B-A;
            double R2 = AB*AB;

            // loop over all elements of the basis
            for (int b2=0,iit2=0; b2<nb2; ++b2) {

                GTO & g2 = Atoms[at2].rAP->basis[b2];

                for (int b1=0,iit1=0; b1<nb1; ++b1) {

                    GTO & g1 = Atoms[at1].rAP->basis[b1];

                    int La = g1.l;
                    int Lb = g2.l;
                    int Ja = g1.J;
                    int Jb = g2.J;


                    int Ltot = La+Lb;


                    RintX MRint;

                    switch (Ltot) {
                        case  0: MRint = RIntegralXX< 0>; break;
                        case  1: MRint = RIntegralXX< 1>; break;
                        case  2: MRint = RIntegralXX< 2>; break;
                        case  3: MRint = RIntegralXX< 3>; break;
                        case  4: MRint = RIntegralXX< 4>; break;
                        case  5: MRint = RIntegralXX< 5>; break;
                        case  6: MRint = RIntegralXX< 6>; break;
                        case  7: MRint = RIntegralXX< 7>; break;
                        case  8: MRint = RIntegralXX< 8>; break;
                        case  9: MRint = RIntegralXX< 9>; break;
                        case 10: MRint = RIntegralXX<10>; break;
                    }



                    // loop over primitives

                    for (int b=0; b<g2.K; ++b) {
                        for (int a=0; a<g1.K; ++a) {


                            Triple R;
                            {
                                for (int px=0; px<=Ltot; ++px)
                                    for (int py=0; py<=Ltot-px; ++py)
                                        for (int pz=0; pz<=Ltot-px-py; ++pz)
                                            R[px][py][pz] = 0;
                            }


                            double k1, k2, k12, ik2;

                            k1  = g1.k[a];
                            k2  = g2.k[b];
                            k12 = k1+k2;
                            ik2 = 1/(2*k12);

                            point P;
                            vector3 Av;
                            vector3 Bv;

                            P.x = (k1*A.x + k2*B.x)/k12;
                            P.y = (k1*A.y + k2*B.y)/k12;
                            P.z = (k1*A.z + k2*B.z)/k12;

                            vector3 PA;
                            PA = P - A;

                            vector3 PB;
                            PB = P - B;



                            TripleX TR;

                            double RRR = 0;

                            // monopole contribution
                            for (int i=0; i<nAtoms; ++i) {

                                //add the damped point charge contribution
                                MRint(TR, k12, RRR, P, Atoms[i].c);

                                for (int px=0; px<=Ltot; ++px) {
                                    for (int py=0; py<=Ltot-px; ++py) {
                                        for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                            R[px][py][pz] -= Atoms[i].rAP->Z * TR[px][py][pz]; //
                                        }
                                    }
                                }
                            }


                            double wab = exp(-(k1*k2/k12) * R2);

                            double VV[2*LMAX+1][2*LMAX+1];


                            for (int ma=0; ma<nmS[La]; ++ma) {
                                for (int mb=0; mb<nmS[Lb]; ++mb) {
                                    double sumV = 0;

                                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                            const SHTerm & sha = SHList[La][ma].T[i];
                                            const SHTerm & shb = SHList[Lb][mb].T[j];

                                            double r2e = R2E(sha.nx,sha.ny,sha.nz,PA, shb.nx,shb.ny,shb.nz,PB,  ik2, 0,0,0, R);

                                            sumV += sha.cN * shb.cN * r2e;
                                        }
                                    }

                                    VV[ma][mb] = sumV;
                                }
                            }


                            // S | X,Y,Z
                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {

                                    double w12 = wab * g1.N[ja][a] * g2.N[jb][b];

                                    for (int ma=0; ma<nmS[La]; ++ma) {
                                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                                            int ffa = it1 + iit1 + ja*nmS[La] + ma;
                                            int ffb = it2 + iit2 + jb*nmS[Lb] + mb;

                                            V(ffa,ffb) += w12 * VV[ma][mb];
                                        }
                                    }
                                }
                            }


                        }
                    }

                    iit1 += nBasis(g1);
                }
                iit2 += nBasis(g2);
            }

            it1 += nBasis(Atoms[at1]);
        }
        it2 += nBasis(Atoms[at2]);
    }

    // return
    npy_intp dims[] = {nb,nb};
    PyObject * Vpy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, V.c2);
    V.c2 = NULL; // don't deallocate the array when tensor2 goes out of scope

    return Vpy;
}


static PyObject * efs_Enuc(PyObject * self, PyObject * args) {

    // load charges
    map <string, int> charges;

    for (int z=0; z<2+8+8+18+18; ++z) {
        charges[PeriodicTable[z]] = z+1;
    }

    // parse input objects
    PyObject * imol;
    if (!PyArg_ParseTuple (args, "O", &imol)) return NULL;
    PyObject * patoms = PyObject_GetAttrString(imol, "atoms");

    int nAtoms = PyList_Size(patoms);

    double Enuc = 0;

    for (int n=0; n<nAtoms; ++n) {
        // extract the atom object
        PyObject * el1; point c1;
        if (!PyArg_ParseTuple (PyList_GetItem(patoms,n), "Sddd:mol", &el1, &c1.x, &c1.y, &c1.z)) continue;
        double Z1 = charges[ucase(PyBytes_AS_STRING(el1))];

        for (int m=0; m<n; ++m) {
            // extract the atom object
            PyObject * el2; point c2;
            if (!PyArg_ParseTuple (PyList_GetItem(patoms,m), "Sddd:mol", &el2, &c2.x, &c2.y, &c2.z)) continue;
            double Z2 = charges[ucase(PyBytes_AS_STRING(el2))];

            vector3 R = (c2-c1)*AMS;
            Enuc += (Z1*Z2) / sqrt(R*R);
        }
    }

    return Py_BuildValue("f", Enuc);
}


static PyObject * efs_get1e(PyObject * self, PyObject * args) {

    // parse input objects
    PyObject * imol, * ibset;
    if (!PyArg_ParseTuple (args, "OO", &imol, &ibset)) return NULL;

    rAtom * Atoms;
    int   nAtoms;
    map <string, rAtomPrototype*> basis;

    ParseBasis (               basis, ibset);
    ParseMol   (Atoms, nAtoms, basis, imol);
    SetCharges (basis);

    int nb  = nBasis     (Atoms, nAtoms);
    int nel = nElectrons (Atoms, nAtoms);

    // 1e matrices
    tensor2 S,T,V;
    S.setsize(nb);
    T.setsize(nb);
    V.setsize(nb);
    S.zeroize();
    T.zeroize();
    V.zeroize();


    // loop over all atoms

    for (int at2=0, it2=0; at2<nAtoms; ++at2) {
        for (int at1=0, it1=0; at1<nAtoms; ++at1) {

            int nb1 = Atoms[at1].rAP->basis.N();
            int nb2 = Atoms[at2].rAP->basis.N();

            point & A = Atoms[at1].c;
            point & B = Atoms[at2].c;
            vector3 AB = B-A;
            double R2 = AB*AB;

            // loop over all elements of the basis
            for (int b2=0,iit2=0; b2<nb2; ++b2) {

                GTO & g2 = Atoms[at2].rAP->basis[b2];

                for (int b1=0,iit1=0; b1<nb1; ++b1) {

                    GTO & g1 = Atoms[at1].rAP->basis[b1];

                    int La = g1.l;
                    int Lb = g2.l;
                    int Ja = g1.J;
                    int Jb = g2.J;



                    // OVERLAP
                    // =======

                    // loop over primitives

                    for (int b=0; b<g2.K; ++b) {
                        for (int a=0; a<g1.K; ++a) {

                            double k1, k2, k12, ik2;

                            k1  = g1.k[a];
                            k2  = g2.k[b];
                            k12 = k1+k2;
                            ik2 = 1/(2*k12);

                            point P;
                            vector3 Av;
                            vector3 Bv;

                            P.x = (k1*A.x + k2*B.x)/k12;
                            P.y = (k1*A.y + k2*B.y)/k12;
                            P.z = (k1*A.z + k2*B.z)/k12;

                            Av = P - A;
                            Bv = P - B;

                            // calcula los tensores de propiedades monoelectronicas
                            double Sx[LMAX+2][LMAX+2];
                            double Sy[LMAX+2][LMAX+2];
                            double Sz[LMAX+2][LMAX+2];

                            SRR (Sx, Av.x, Bv.x, k12, La, Lb);
                            SRR (Sy, Av.y, Bv.y, k12, La, Lb);
                            SRR (Sz, Av.z, Bv.z, k12, La, Lb);


                            double wab = exp(-(k1*k2/k12) * R2);

                            double SS[2*LMAX+1][2*LMAX+1];


                            for (int ma=0; ma<nmS[La]; ++ma) {
                                for (int mb=0; mb<nmS[Lb]; ++mb) {
                                    double sumS = 0;

                                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                            const SHTerm & sha = SHList[La][ma].T[i];
                                            const SHTerm & shb = SHList[Lb][mb].T[j];

                                            sumS += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                                        }
                                    }

                                    SS[ma][mb] = sumS;
                                }
                            }


                            // S | X,Y,Z
                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {

                                    double w12 = wab * g1.N[ja][a] * g2.N[jb][b];

                                    for (int ma=0; ma<nmS[La]; ++ma) {
                                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                                            int ffa = it1 + iit1 + ja*nmS[La] + ma;
                                            int ffb = it2 + iit2 + jb*nmS[Lb] + mb;

                                            S(ffa,ffb) += w12 * SS[ma][mb];
                                        }
                                    }
                                }
                            }

                        }
                    }


                    // KINETIC
                    // =======

                    // loop over primitives

                    for (int b=0; b<g2.K; ++b) {
                        for (int a=0; a<g1.K; ++a) {

                            double k1, k2, k12, ik2;

                            k1  = g1.k[a];
                            k2  = g2.k[b];
                            k12 = k1+k2;
                            ik2 = 1/(2*k12);

                            point P;
                            vector3 Av;
                            vector3 Bv;

                            P.x = (k1*A.x + k2*B.x)/k12;
                            P.y = (k1*A.y + k2*B.y)/k12;
                            P.z = (k1*A.z + k2*B.z)/k12;

                            Av = P - A;
                            Bv = P - B;

                            // calcula los tensores de propiedades monoelectronicas
                            double Sx[LMAX+2][LMAX+2];
                            double Sy[LMAX+2][LMAX+2];
                            double Sz[LMAX+2][LMAX+2];

                            SRR (Sx, Av.x, Bv.x, k12, La, Lb);
                            SRR (Sy, Av.y, Bv.y, k12, La, Lb);
                            SRR (Sz, Av.z, Bv.z, k12, La, Lb);

                            double Tx[LMAX+1][LMAX+1];
                            double Ty[LMAX+1][LMAX+1];
                            double Tz[LMAX+1][LMAX+1];

                            TRR(Tx, Sx, k1, k2, La, Lb);
                            TRR(Ty, Sy, k1, k2, La, Lb);
                            TRR(Tz, Sz, k1, k2, La, Lb);


                            double wab = exp(-(k1*k2/k12) * R2);

                            double TT[2*LMAX+1][2*LMAX+1];


                            for (int ma=0; ma<nmS[La]; ++ma) {
                                for (int mb=0; mb<nmS[Lb]; ++mb) {
                                    double sumT = 0;

                                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                            const SHTerm & sha = SHList[La][ma].T[i];
                                            const SHTerm & shb = SHList[Lb][mb].T[j];

                                            sumT += sha.cN * shb.cN * (Tx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                                       Sx[sha.nx][shb.nx] * Ty[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                                       Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Tz[sha.nz][shb.nz]);
                                        }
                                    }

                                    TT[ma][mb] = 0.5*sumT;
                                }
                            }


                            // S | X,Y,Z
                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {

                                    double w12 = wab * g1.N[ja][a] * g2.N[jb][b];

                                    for (int ma=0; ma<nmS[La]; ++ma) {
                                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                                            int ffa = it1 + iit1 + ja*nmS[La] + ma;
                                            int ffb = it2 + iit2 + jb*nmS[Lb] + mb;

                                            T(ffa,ffb) += w12 * TT[ma][mb];
                                        }
                                    }
                                }
                            }

                        }
                    }



                    // POTENTIAL
                    // =========

                    int Ltot = La+Lb;


                    RintX MRint;

                    switch (Ltot) {
                        case  0: MRint = RIntegralXX< 0>; break;
                        case  1: MRint = RIntegralXX< 1>; break;
                        case  2: MRint = RIntegralXX< 2>; break;
                        case  3: MRint = RIntegralXX< 3>; break;
                        case  4: MRint = RIntegralXX< 4>; break;
                        case  5: MRint = RIntegralXX< 5>; break;
                        case  6: MRint = RIntegralXX< 6>; break;
                        case  7: MRint = RIntegralXX< 7>; break;
                        case  8: MRint = RIntegralXX< 8>; break;
                        case  9: MRint = RIntegralXX< 9>; break;
                        case 10: MRint = RIntegralXX<10>; break;
                    }



                    // loop over primitives

                    for (int b=0; b<g2.K; ++b) {
                        for (int a=0; a<g1.K; ++a) {


                            Triple R;
                            {
                                for (int px=0; px<=Ltot; ++px)
                                    for (int py=0; py<=Ltot-px; ++py)
                                        for (int pz=0; pz<=Ltot-px-py; ++pz)
                                            R[px][py][pz] = 0;
                            }


                            double k1, k2, k12, ik2;

                            k1  = g1.k[a];
                            k2  = g2.k[b];
                            k12 = k1+k2;
                            ik2 = 1/(2*k12);

                            point P;
                            vector3 Av;
                            vector3 Bv;

                            P.x = (k1*A.x + k2*B.x)/k12;
                            P.y = (k1*A.y + k2*B.y)/k12;
                            P.z = (k1*A.z + k2*B.z)/k12;

                            vector3 PA;
                            PA = P - A;

                            vector3 PB;
                            PB = P - B;

                            TripleX TR;

                            double RRR = 0;

                            // monopole contribution
                            for (int i=0; i<nAtoms; ++i) {

                                //add the damped point charge contribution
                                MRint(TR, k12, RRR, P, Atoms[i].c);

                                for (int px=0; px<=Ltot; ++px) {
                                    for (int py=0; py<=Ltot-px; ++py) {
                                        for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                            R[px][py][pz] -= Atoms[i].rAP->Z * TR[px][py][pz]; //
                                        }
                                    }
                                }
                            }


                            double wab = exp(-(k1*k2/k12) * R2);

                            double VV[2*LMAX+1][2*LMAX+1];


                            for (int ma=0; ma<nmS[La]; ++ma) {
                                for (int mb=0; mb<nmS[Lb]; ++mb) {
                                    double sumV = 0;

                                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                            const SHTerm & sha = SHList[La][ma].T[i];
                                            const SHTerm & shb = SHList[Lb][mb].T[j];

                                            double r2e = R2E(sha.nx,sha.ny,sha.nz,PA, shb.nx,shb.ny,shb.nz,PB,  ik2, 0,0,0, R);

                                            sumV += sha.cN * shb.cN * r2e;
                                        }
                                    }

                                    VV[ma][mb] = sumV;
                                }
                            }


                            // S | X,Y,Z
                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {

                                    double w12 = wab * g1.N[ja][a] * g2.N[jb][b];

                                    for (int ma=0; ma<nmS[La]; ++ma) {
                                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                                            int ffa = it1 + iit1 + ja*nmS[La] + ma;
                                            int ffb = it2 + iit2 + jb*nmS[Lb] + mb;

                                            V(ffa,ffb) += w12 * VV[ma][mb];
                                        }
                                    }
                                }
                            }


                        }
                    }

                    iit1 += nBasis(g1);
                }
                iit2 += nBasis(g2);
            }

            it1 += nBasis(Atoms[at1]);
        }
        it2 += nBasis(Atoms[at2]);
    }

    // return
    npy_intp dims[] = {nb,nb};

    PyObject * Spy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, S.c2);
    PyObject * Tpy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, T.c2);
    PyObject * Vpy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, V.c2);

    S.c2 = T.c2 = V.c2 = NULL; // don't deallocate the arrays when tensor2 goes out of scope

    PyObject * pack = Py_BuildValue("(OOOii)", Spy,Tpy,Vpy,nb,nel);

    return pack;
}

static PyObject * efs_fock2e(PyObject * self, PyObject * args) {


    // parse input objects
    PyObject * imol, * ibset;
    PyArrayObject * idm;
    if (!PyArg_ParseTuple (args, "OOO", &imol, &ibset, &idm)) return NULL;

    rAtom * Atoms;
    int   nAtoms;
    map <string, rAtomPrototype*> basis;

    ParseBasis (               basis, ibset);
    ParseMol   (Atoms, nAtoms, basis, imol);

    int nb = nBasis(Atoms, nAtoms);

    double * d = (double*)PyArray_DATA(idm);

    tensor2 D(nb,d);
    tensor2 F(nb);


    //        PERFORM CALCULATION
    //        ==================

    Echidna FockSolver;

    double GDO = (1e-25);
    double CS  = (1e-12);
    double logGDO = -log(GDO);


    r2tensor<int> AtomPairs;


    // use all N^2 interactions (for the moment)
    AtomPairs.set(nAtoms, nAtoms); {
        for (int at1=0; at1<nAtoms; ++at1)
            for (int at2=0; at2<nAtoms; ++at2)
                    AtomPairs[at1][at2] = at2;
    }

    // set phasers to party
    {
        int  nid=0;
        for (int n=0; n<nAtoms; ++n) {
            if (Atoms[n].rAP->RId==UNSET) {
                Atoms[n].rAP->RId = nid;
                Atoms[n].rAP->DId = nid;
                ++nid;
            }
        }
    }

        // set values for buffers & IC code
    Echidna::MAXMEM = 128 * Mword;
    Echidna::MEMBUFFER = 1024 * Mword;
    LibQuimera::Quimera::SetQICdir("./qic");


    // compute!
    FockSolver.Init             (Atoms, nAtoms);
    FockSolver.ComputeAtomPairs (AtomPairs, logGDO);
    FockSolver.InitFock2e       ();
    FockSolver.FockUpdate       (F, D, CS, CS);

    //        CLEANUP
    //        =======
    delete[] Atoms;
    map <string, rAtomPrototype*>::iterator it;
    for (it=basis.begin(); it!=basis.end(); ++it) delete it->second;
    D.c = NULL; // set to NULL so that the destructor doesn't try to deallocate memory belonging to the Python object


    //     EXIT
    npy_intp dims[] = {nb,nb};
    PyObject * Fpy = PyArray_SimpleNewFromData (2, dims,  NPY_DOUBLE, F.c2);
    F.c2 = NULL; // don't deallocate the array when tensor2 goes out of scope

    return Fpy;
}


static PyMethodDef efsMethods[] = {
    //{"test"  ,  efs_test,   METH_VARARGS, "Test numpy"},
    {"getS"  ,  efs_getS,   METH_VARARGS, "Retrieve overlap matrix"},
    {"getT"  ,  efs_getT,   METH_VARARGS, "Retrieve kinetic energy matrix"},
    {"getV"  ,  efs_getV,   METH_VARARGS, "Retrieve nuclear potential matrix"},
    {"Enuc"  ,  efs_Enuc,   METH_VARARGS, "Compute nuclear potential energy"},
    {"get1e" ,  efs_get1e,  METH_VARARGS, "Retrieve all 1-electronic matrices, their dimension, and number of electrons"},
    {"fock2e",  efs_fock2e, METH_VARARGS, "Compute 2-electronic contributions to the Fock operator"},
    {NULL    ,  NULL     ,  0           , NULL}
};

static struct PyModuleDef efsModule = {
    PyModuleDef_HEAD_INIT,
    "efs",
    "efs module",
    -1,
    efsMethods
};

PyMODINIT_FUNC PyInit_efs() {
    // init numpy
    import_array();

    // init angular
    LibAngular::InitAngular();

    // init igamma tables
    LibIGamma::InitIncompleteGammas();

    return PyModule_Create(&efsModule);
}

}
