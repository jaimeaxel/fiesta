import numpy
import scipy.linalg as la
import efs
import os

class molecule:
    def __init__(self, filename):
        #default values
        self.charge =  0
        self.spin   =  1
        self.atoms  = []
        #load molecule
        mol = open(filename,'r')
        for line in mol:
            #parse line
            words = line.split(' ')
            #remove \n in last word
            last = words[-1][:-1]
            words[-1] = last
            #remove extra separators
            while '' in words:
                words.remove('')
            #
            if len(words)==4 and len(words[0])<3:
                atom = (words[0], float(words[1]), float(words[2]), float(words[3]))
                self.atoms.append(atom)
        mol.close()


class basis:
    name = ''
    def __init__(self, filename):
        #empty basis
        self.ebasis = {}
        #load set
        fset = open(filename,'r')
        element = ''
        nprims  = 0
        J = 0
        K = 0
        L = 0
        aa = []
        cc = [[]]
        for line in fset:
            #parse line
            words = line.split(' ')
            #remove \n in last word
            last = words[-1][:-1]
            words[-1] = last
            #remove extra separators
            while '' in words:
                words.remove('')
            if (len(words)==0):
                continue
            #skip comments
            if words[0][0]=='!':
                continue
            #get name of the basis
            if words[0][:5]=='BASIS':
                bkeys = words[0].split('"')
                self.name = bkeys[1]
                continue
            #skip segmented/general tag
            if words[0]=='SEGMENTED':
                continue
            if words[0]=='GENERAL':
                continue
            #get elements
            if element=='':
                element = words[0]
                self.ebasis[element] = []
                continue
            if words[0]=='****':
                element = ''
                continue
            #read  angular momentum, contraction parameters; set lists
            if nprims==0:
                #error
                if len(words)<3:
                    break;
                #get angular momentum
                if words[0]=='S':
                    L = 0
                elif words[0]=='SP':
                    L = -1
                elif words[0]=='P':
                    L = 1
                elif words[0]=='D':
                    L = 2
                elif words[0]=='F':
                    L = 3
                elif words[0]=='G':
                    L = 4
                elif words[0]=='H':
                    L = 5
                elif words[0]=='I':
                    L = 6
                elif words[0]=='K':
                    L = 7
                #interpret K,J
                K = int(words[1])
                if L==-1:
                    J=2
                else:
                    J = int(float(words[2]))
                nprims = K
                aa = []
                cc = [[]]
                for j in range(J):
                    cc.append([])
                continue
            #parse everything else
            aa.append(float(words[0]))
            for j in range(J):
                cc[j].append(float(words[j+1]))
            nprims = nprims-1
            #add shell
            if nprims==0:
                if L==-1:
                    shellS=(0,K,1,aa,[cc[0]])
                    shellP=(1,K,1,aa,[cc[1]])
                    self.ebasis[element].append(shellS)
                    self.ebasis[element].append(shellP)
                else:
                    shell=(L,K,J,aa,cc)
                    self.ebasis[element].append(shell)
        fset.close()


#import basis and molecule
bss = basis   ('basis/STO-3G.bs')
mol = molecule('inputs/h2o.simple.xyz') #('inputs/noctane.xyz') #


#print molecule
print ""
print "Input molecule:"
for atom in mol.atoms:
    print atom[0], atom[1], atom[2], atom[3]
print ""

#compute some basic quantities
S,T,V,nd,ne = efs.get1e(mol,bss) # 1-electron integrals & dimension & electron number
En = efs.Enuc(mol)               # nuclear energy
F0 = T+V                         # core fock operator
no = ne/2                        # number of occupied orbitals
print "Molecule contains", ne, "electrons in", no, "orbitals"
diag = numpy.diagflat( [1]*no + [0]*(nd-no))

#initial values for SCF
E,C = la.eigh(F0,S) #guess orbitals form core fock diagonalization
El2 = 0             #electronic energy

#SCF cycle
for ncyc in range(1,50):
    print "SCF cycle", ncyc,
    D   = C.dot(diag.dot(C.transpose())) #generate density matrix
    JX  = efs.fock2e(mol, bss, D)        #compute coulomb and exchange contributions
    F   = F0 + 2*JX                      #new fock matrix
    E,C = la.eigh(F,S)                   #solve secular equation
    El  = 2*numpy.trace(D.dot(F0+JX))    #compute MP1 energy
    print "; energy",El,"hartree"
    if abs(El2-El) < 1e-10:
        break
    El2 = El
print ""

#final print
numpy.set_printoptions(formatter={'float': '{: 0.6f}'.format})
print "Orbital energies:\n", E
print ""
print "Electronic energy: ", El
print "Nuclear energy:      ", En
print "Total energy:      ", El+En
print ""

