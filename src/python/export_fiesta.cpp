#include <mpi4py/mpi4py.h>
#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include <boost/python/extract.hpp>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "defs.hpp"
#include "low/MPIwrap.hpp"
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "linear/tensors.hpp"
#include "interface/interface.hpp"

namespace bp = boost::python;

#ifdef C_MPI

// ==> fiesta init, run and finalize <==

void fiesta_init(bp::list inputs, bp::object py_comm, bool print_output)
{
    // prepare argc and argv from boost python list
    int argc = len(inputs) + 1;
    if (argc < 1) return;

    char* argv[argc];

    argv[0] = (char*)malloc(sizeof(char) * (strlen("exe")+1));
    memset(argv[0], '\0', sizeof(char) * (strlen("exe")+1));
    strcpy(argv[0], "exe");

    for (int iarg = 1; iarg < argc; iarg++) {
        std::string line = bp::extract<std::string>(inputs[iarg - 1]);
        const char* text = line.c_str();
        argv[iarg] = (char*)malloc(sizeof(char) * (strlen(text) + 1));
        memset(argv[iarg], '\0', sizeof(char) * (strlen(text) + 1));
        memcpy(argv[iarg], text, sizeof(char) * strlen(text));
    }

    // prepare communicator from mpi4py python object
    PyObject* py_obj = py_comm.ptr();
    MPI_Comm *comm_p = PyMPIComm_Get(py_obj);
    if (comm_p == NULL) bp::throw_error_already_set();

    // initialize fiesta
    NodeGroup.Init(comm_p);
    Fiesta::InitJob(argc, argv, print_output);
}

void fiesta_init_ov(bp::list inputs, bp::object py_comm)
{
    fiesta_init(inputs, py_comm, true);
}

void fiesta_run()
{
    Fiesta::RunJob();
}

void fiesta_finalize(bp::object py_comm)
{
    // prepare communicator from mpi4py python object
    PyObject* py_obj = py_comm.ptr();
    MPI_Comm *comm_p = PyMPIComm_Get(py_obj);
    if (comm_p == NULL) bp::throw_error_already_set();

    // finalize fiesta
    Fiesta::FinalizeJob();
    NodeGroup.End(comm_p);
}

// ==> fiesta full run <==

void fiesta_full_run(bp::list inputs, bp::object py_comm, bool print_output)
{
    fiesta_init(inputs, py_comm, print_output);
    Fiesta::RunJob();
    fiesta_finalize(py_comm);
}

void fiesta_full_run_ov(bp::list inputs, bp::object py_comm)
{
    fiesta_full_run(inputs, py_comm, true);
}

// ==> interface <==

// run fiesta task according to request
std::shared_ptr<DataResult> fiesta_process_task(const std::shared_ptr<DataRequest>& req)
{
    Fiesta::RunTask(req);

    return Fiesta::Result();
}

BOOST_PYTHON_MODULE(fiesta)
{
    // initialize mpi4py's C-API
    if (import_mpi4py() < 0) return;

    // ==> initialize module <==

    // fiesta
    bp::def("init", fiesta_init);
    bp::def("init", fiesta_init_ov);
    bp::def("finalize", fiesta_finalize);
    bp::def("run", fiesta_run);
    bp::def("run", fiesta_full_run);
    bp::def("run", fiesta_full_run_ov);

    // interface classes
    bp::class_< DataInfo, std::shared_ptr<DataInfo> > ("DataInfo",bp::init<const DataInfo&>())
        .def("__str__",&DataInfo_str_)
        .def("get_num_atoms",&DataInfo::get_num_atoms)
        .def("get_input_xyz",&DataInfo::get_input_xyz)
    ;

    bp::class_< DataResult, std::shared_ptr<DataResult> > ("DataResult",bp::init<const DataResult&>())
        .def("__str__",&DataResult_str_)
    ;

    bp::class_< DataRequest, std::shared_ptr<DataRequest> > ("DataRequest",bp::init<const DataRequest&>())
        .def("__str__",&DataRequest_str_)
    ;

    // interface
    bp::def("info", Fiesta::Info);
    bp::def("process_task", fiesta_process_task);

}
#endif
