#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include "interface/interface.hpp"
#include "exciton/exciton_model.hpp"

namespace bp = boost::python;

bp::list ExcitonModel_get_data_(const std::shared_ptr<ExcitonModel>& self)
{
    bp::list py_data;

    Matrix& H = self->H;
    Matrix& e_tr_dip = self->diab_e_tr_dip;
    Matrix& v_tr_dip = self->diab_v_tr_dip;
    Matrix& m_tr_dip = self->diab_m_tr_dip;

    for (int i = 0; i < H.size(); i++) { py_data.append(H.data()[i]); }
    for (int i = 0; i < e_tr_dip.size(); i++) { py_data.append(e_tr_dip.data()[i]); }
    for (int i = 0; i < v_tr_dip.size(); i++) { py_data.append(v_tr_dip.data()[i]); }
    for (int i = 0; i < m_tr_dip.size(); i++) { py_data.append(m_tr_dip.data()[i]); }

    return py_data;
}

void ExcitonModel_update_data_(const std::shared_ptr<ExcitonModel>& self, bp::list py_data)
{
    Matrix& H = self->H;
    Matrix& e_tr_dip = self->diab_e_tr_dip;
    Matrix& v_tr_dip = self->diab_v_tr_dip;
    Matrix& m_tr_dip = self->diab_m_tr_dip;

    int count = 0;

    for (int i = 0; i < H.size(); i++, count++) {
        double val = bp::extract<double>(py_data[count]);
        if (val != 0.0 && H.data()[i] == 0.0) { H.data()[i] = val; }
    }

    for (int i = 0; i < e_tr_dip.size(); i++, count++) {
        double val = bp::extract<double>(py_data[count]);
        if (val != 0.0 && e_tr_dip.data()[i] == 0.0) { e_tr_dip.data()[i] = val; }
    }

    for (int i = 0; i < v_tr_dip.size(); i++, count++) {
        double val = bp::extract<double>(py_data[count]);
        if (val != 0.0 && v_tr_dip.data()[i] == 0.0) { v_tr_dip.data()[i] = val; }
    }

    for (int i = 0; i < m_tr_dip.size(); i++, count++) {
        double val = bp::extract<double>(py_data[count]);
        if (val != 0.0 && m_tr_dip.data()[i] == 0.0) { m_tr_dip.data()[i] = val; }
    }
}

BOOST_PYTHON_MODULE(exciton)
{
    // ==> initialize module <==

    bp::class_<ExcitonModel>("ExcitonModel", bp::init<>())
        .def("init", &ExcitonModel::init)
        .def("request", &ExcitonModel::request)
        .def("finalize_task", &ExcitonModel::finalize_task)
        .def("read_monomer_file", &ExcitonModel::read_monomer_file)
        .def("pair_index_a", &ExcitonModel::get_pair_index_a)
        .def("pair_index_b", &ExcitonModel::get_pair_index_b)
        .def("get_data", &ExcitonModel_get_data_)
        .def("update_data", &ExcitonModel_update_data_)
        .def("finish", &ExcitonModel::finish)
        .add_property("total_natoms", &ExcitonModel::get_total_natoms)
        .add_property("total_nfrags", &ExcitonModel::get_total_nfrags)
        .add_property("total_npairs", &ExcitonModel::get_total_npairs)
        .add_property("has_dft", &ExcitonModel::has_dft)
        ;
}
