

#ifndef __ANGULAR__
#define __ANGULAR__

#include "defs.hpp"

struct vector3;

typedef double CoeffMatrix [LMAX+1][2*LMAX+1][LMAX+1][LMAX+1][LMAX+1]; // l,m,lx,ly,lz
typedef double AngularMatrix [LMAX+1][2*LMAX+1][LMAX+1][2*LMAX+1][LMAX+1][LMAX+1][LMAX+1]; //l1,m1,l2,m2,lx,ly,lz

namespace LibAngular {

    // terms in the expansion of a spherical harmonic
    struct SHTerm {
        double cN;   // weight (normalized)
        double cN2;  // peso del armonico normalzado - whatever that means without the sum at the end
        double norm; // just normalization
        char   nc;   // index for Cartesian term
        char   nx;   // x power
        char   ny;   // y power
        char   nz;   // z power
    };

    // real spherical harmonic expanded in Cartesian coordinates
    struct SphHar {
        int l; // angular momentum
        int m; // magnetic number

        int nps;     // number of cartesian primitives in term
        SHTerm T[6]; // cartesian primitive
                     // allow only up to 6 at the moment, which is enough for up to h-functions;

        // initialize
        void SetNPS(int n);
        void SetT(int t, double c, int x, int y, int z);

        double operator() (const vector3 & r) const;
    };

    //this is used for metaprogrammed routines which require a compile-time static value
    template<int N> struct nmCT {
        enum {n = ((N+2)*(N+1))/2};
    };

    //last one is for SP shells
    const int nmS[LMAX+2] = {1,3,5,7,9,11,   4};
    const int nmC[LMAX+2] = {1,3,6,10,15,21, 4};

    extern int ExyzC[LMAX+2][LMAX+2][MMAX][MMAX];
    extern int ExyzT[LMAX+2][LMAX+2];

    extern SHTerm CartList[LMAX+2][MMAX];
    extern SphHar SHList  [LMAX+2][2*LMAX+1];

    extern double SHrot [LMAX+1][2*LMAX+1][2*LMAX+1];
    extern double SHrotI[LMAX+1][2*LMAX+1][2*LMAX+1];
    extern AngularMatrix Omega; //needed for semi-numerical ECPs.

    // inicia las tablas de esfericos harmonicos y las seminormaliza
    void InitSHList();
    void InitCartList();
    void InitAngularMatrix();

    namespace details{
      double AngularIntegralPoly(const int& arLx, const int& arLy, const int& arLz);
      double ExpansionCoeffU(const int& arL, const int& arM, const int& arLx, const int& arLy, const int& arLz);
      double ExpansionBackCoeffV(const int& arL, const int& arM, const int& arLx, const int& arLy, const int& arLz, const CoeffMatrix& arU);
    }
    // capital letter representing the angular momentum
    char L2S (int  L);
    int S2L (char S);

    //gamma[n+1/2]
    const double hGamma[] =
    {1.77245385090551603, 0.886226925452758014, 1.32934038817913702,
    3.32335097044784255, 11.6317283965674489, 52.3427777845535202,
    287.885277815044361, 1871.25430579778835, 14034.4072934834126,
    119292.461994609007, 1.13327838894878557e6,
     1.18994230839622485e7, 1.36843365465565857e8,
     1.71054206831957322e9, 2.30923179223142384e10,
     3.34838609873556457e11, 5.18999845304012508e12,
     8.56349744751620639e13, 1.49861205331533612e15,
     2.77243229863337182e16, 5.40624298233507504e17,
     1.10827981137869038e19, 2.38280159446418433e20,
     5.36130358754441473e21, 1.25990634307293746e23,
     3.08677054052869678e24, 7.87126487834817680e25,
     2.08588519276226685e27, 5.73618428009623384e28,
     1.63481251982742664e30, 4.82269693349090860e31,
     1.47092256471472712e33};

    void InitAngular();

    extern bool LibAngularInit;
}

//initialize the constant memory for GPU interpreted MIRROR transforms
void InitDeviceSphH();

//init the device's constant SH rotations
void InitDeviceSHrot();


#endif


