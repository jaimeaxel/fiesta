#include <iostream>
#include "math.hpp"

double math::TaylorCoeff(const int& arI)
{
   return 1/factorial(arI);
}

double math::pow(double x, int n) 
{

    double r=1;

    while (n) {
         if (n%2) r*=x;
         x*=x;
         n>>=1;
    }

    return r;
}

int math::factorial(const int& n) 
{
    // sanity check
    if (n < 0 || n>13) {
       std::cout << "got " << n << " in the factorial ?!" << std::endl;
       return 0;
    }
    const int fact[] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600};
    return fact[n];
}

double math::doublefactorialrec(int n) 
{
    const double dfact[] = {1, 1, 2, 3, 8, 15, 48, 105, 384, 945, 3840, 10395, 46080, 135135,
        645120, 2027025, 10321920, 34459425, 185794560, 654729075,
        3715891200, 13749310575, 81749606400, 316234143225, 1961990553600,
        7905853580625, 51011754393600, 213458046676875, 1428329123020800,
        6190283353629375, 42849873690624000};
    if (n < 0 || n>30) {
       std::cout << "got " << n << " in the factorial ?!" << std::endl;
       return 0;
    }
    return dfact[n];
}

// not sure this is ever called
double math::doublefactorialnegrec(int n)
{
   if ((!n % 2))
   {
      std::cout << " I cannot calculate the double factorial of a negative even number and will quit.. " << std::endl;
      exit(0);
   }
   if (n < -1)
      std::cout << " unexpected n: " << n <<  std::endl;
    //
    const double dfact[] = {1., -1., 0.3333333333333333, -0.06666666666666667, 0.009523809523809525, -0.0010582010582010583, 0.0000962000962000962, -7.4000074000074e-6, 4.9333382666716e-7,
                        -2.901963686277412e-8, 1.5273493085670588e-9, -7.273091945557423e-11, 3.1622138893727926e-12, -1.264885555749117e-13, 4.684761317589322e-15, -1.6154349370997664e-16};
    return dfact[(-n)/2];
}

double math::doublefactorial(int n)
{
   if (n < 0) return doublefactorialnegrec(n);
   else       return doublefactorialrec(n);
}



long int math::factorial2(const int& n)
{
   if (n < 0)
   {
      std::cout << "got a negative number " << n << " in the factorial ?!" << std::endl;
      return 0;
   }

   long int f=0;

   if(n <= 1) return 1;

   f = n * factorial(n - 1);
   return f;
}

long double math::doublefactorialrec2(const int& n)
{
   if(n <= 1) return 1;

   long double f = n * doublefactorialrec(n - 2);

   return f;
}

long double math::doublefactorialnegrec2(const int& n)
{
   if ((!n % 2))
   {
      std::cout << " I cannot calculate the double factorial of a negative even number and will quit.. " << std::endl;
      exit(0);
   }
   if (n < -1) // ?????
      std::cout << " unexpected n: " << n <<  std::endl;

   if(n >= 0) return 1;

   long double f = doublefactorialnegrec(n + 2);

   return f/(n+2);
}

long double math::doublefactorial2(const int& n)
{
   if (n < 0)
        return doublefactorialnegrec(n);
   else
      return doublefactorialrec(n);
}


