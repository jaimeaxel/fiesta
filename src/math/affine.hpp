
#ifndef __AFFINE__
#define __AFFINE__

#include <cmath>
#include <iostream>


class vector3;
class point;

//vectoir, que en francés significa "vector"
class vector3 {
    friend class point;


  public:
    double x;
    double y;
    double z;

    vector3() {}

    vector3(double xx, double yy, double zz) {
        x = xx;
        y = yy;
        z = zz;
    }

    inline void operator()(double xx, double yy, double zz) {
        x = xx;
        y = yy;
        z = zz;
    }

    inline vector3 & operator=(const vector3 & rhs) {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }

    inline vector3 & operator+=(const vector3 & rhs) {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    };

    inline vector3 & operator-=(const vector3 & rhs) {
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    };

    inline vector3 & operator*=(double rhs) {
        x *= rhs;
        y *= rhs;
        z *= rhs;
        return *this;
    };

    inline vector3 & operator/=(double rhs) {
        double irhs = 1./rhs;
        x *= irhs;
        y *= irhs;
        z *= irhs;
        return *this;
    };

    inline const vector3 operator+(const vector3 & rhs) const {
        vector3 v = *this;
        v += rhs;
        return v;
    };

    inline const vector3 operator-(const vector3 & rhs) const {
        vector3 v = *this;
        v -= rhs;
        return v;
    };

    inline const vector3 operator*(double rhs) const {
        vector3 v = *this;
        v *= rhs;
        return v;
    };

    inline const vector3 operator/(double rhs) const {
        vector3 v = *this;
        v /= rhs;
        return v;
    };

    //inner product
    inline double operator*(const vector3 & rhs) const {
        double r;
        r  = x*rhs.x;
        r += y*rhs.y;
        r += z*rhs.z;

        return r;
    };

    //cross product (operator* cannot be overloaded for different return types)
    inline const vector3 operator|(const vector3 & rhs) const {
        vector3 v;
        v.x = y*rhs.z - z*rhs.y;
        v.y = z*rhs.x - x*rhs.z;
        v.z = x*rhs.y - y*rhs.x;

        return v;
    };

};


//should be fixed point (resolution decreases far from the (0,0,0) arbitrary origin, which is a bad thing)
class point {
  public:
    double x;
    double y;
    double z;

  public:
    point() {
        x = y = z = 0;
    }

    point(double ix, double iy, double iz) {
        x = ix;
        y = iy;
        z = iz;
    }

    inline point & operator=(const point & rhs) {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }

    inline point & operator+=(const vector3 & rhs) {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }

    inline point & operator-=(const vector3 & rhs) {
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    }


    inline const point operator+(const vector3 & rhs) const {
        point v = *this;
        v += rhs;
        return v;
    }

    inline const point operator-(const vector3 & rhs) const {
        point v = *this;
        v -= rhs;
        return v;
    };

    inline const vector3 operator-(const point & rhs) const {
        vector3 r;
        r.x = x - rhs.x;
        r.y = y - rhs.y;
        r.z = z - rhs.z;

        return r;
    }
};

inline vector3 Xprod (const vector3 & v1, const vector3 & v2) {
	vector3 vt;
	vt.x = v1.y*v2.z - v1.z*v2.y;
	vt.y = v1.z*v2.x - v1.x*v2.z;
	vt.z = v1.x*v2.y - v1.y*v2.x;
	return vt;
}

inline const vector3 operator* (double d, const vector3 & v) {
    vector3 r;
    r.x = d*v.x;
    r.y = d*v.y;
    r.z = d*v.z;

    return r;
}

inline double norm(const vector3 & v) {
    return sqrt(v*v);
}

inline void normalize(vector3 & v) {
    v /= norm(v);
}

inline vector3 FrameRot (const vector3 & x, const vector3 & y, const vector3 & z, const vector3 & v) {
    vector3 r;
    r.x = v*x;
    r.y = v*y;
    r.z = v*z;
    return r;
}

inline vector3 FrameRotT(const vector3 & x, const vector3 & y, const vector3 & z, const vector3 & v) {
    vector3 r;
    r.x = v.x*x.x + v.y*y.x + v.z*z.x;
    r.y = v.x*x.y + v.y*y.y + v.z*z.y;
    r.z = v.x*x.z + v.y*y.z + v.z*z.z;
    return r;
}


#endif
