#ifndef __SMATH__
#define __SMATH__
#include <cmath>
#include "defs.hpp"

struct icoord {
	double R;
	double a;
	double b;
	uint32_t at1;
	uint32_t at2;
	uint32_t at3;

	icoord() {
	    R = a = b = 0;
	    at1 = at2 = at3 = 0;
	}

	icoord & operator=(const icoord & rhs) {
	    R=rhs.R;
	    a=rhs.a;
	    b=rhs.b;
	    at1=rhs.at1;
	    at2=rhs.at2;
	    at3=rhs.at3;
	    return *this;
    }
};


double pown(double v, int n);

#endif
