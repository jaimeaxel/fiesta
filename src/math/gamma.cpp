


#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "math/gamma.hpp"
#include "defs.hpp"
using namespace std;


static inline void LenzUpdate(double & A, double & Ap, const double a, const double b) {
    double tmp = A;
    A = b*A + a*Ap;
    Ap = tmp;
}

//computational chemists' "lower gamma function"
//Table[Integrate[t^(2n) exp[-x t^2],{x,0,1}],{n,0,...}]
static inline void calcF(double * F, int n, double x) {
    double expx = exp(-x);

    //calcula gamma(n + 1/2, x) / x^(n+1/2) y el resto por recurrencia descendente
    if (x < double(n) + 1.5) {
        double ac  = 1./(double(n)+0.5);
        double sum = ac;

        //loop until machine precission convergence
        for (int i=1; (sum+ac)!=sum; ++i) {
            ac *= (x)/(double(n+i)+0.5);
            sum +=ac;
        }

        F[n] = 0.5 * sum;

        //downward recursion
        for (int i=n-1; i>=0; --i)
            F[i] = (2*x*F[i+1] + 1.)/double(2*i+1);

        for (int i=n; i>=0; --i)
            F[i] *= expx;
    }

    //calcula F0[x] a partir de la fraccion continua y el resto por recurrencia ascendente
    else {
        double Ap = 1;
        double A  = 0; //b0=0

        double Bp = 0;
        double B  = 1;

        LenzUpdate(A,Ap,1.,x);
        LenzUpdate(B,Bp,1.,x);

        //error rose to 10^-12 around x~1.5 with old value
        for (int i=0; i<64; ++i) {
            LenzUpdate(A,Ap,double(i)+0.5,1.);
            LenzUpdate(B,Bp,double(i)+0.5,1.);

            LenzUpdate(A,Ap,double(i+1),x);
            LenzUpdate(B,Bp,double(i+1),x);
        }

        F[0] = sqrt(PI/(4*x)) -0.5 * expx * (A/B);

        //upward recursion
        for (int i=0; i<n; ++i)
            F[i+1] = (double(2*i+1)*F[i] - expx) / (2*x);
    }
}


namespace LibIGamma {

    GammaFunction::GammaFunction() {
        gamma_table = NULL;
        gamma_table_vals = NULL;
    }

    GammaFunction::~GammaFunction() {
        free(gamma_table);
        free(gamma_table_vals);
    }

    //nueva funcion gamma
    void GammaFunction::CalcGammas(double * F, int n, double x) const{
        //busca el valor mas cercano de F
        //cout << vg_max << " " << vg_step << endl;
        if (x>vg_max-vg_step) {
            F[0] = 0.5*sqrt(PI/x);

            double ix = 0.5/x;
            for (int i=1; i<=n; ++i) {
                F[i] = F[i-1]*ix*double(2*i-1);
            }
        }
        else {
            double p = ivg_step*(x-vg_min);
            int pos = int(p+0.5);
            double x0 = vg_min + vg_step*double(pos);
            double Ax = x0-x;

            double Axn[NEXP];
            Axn[0] = 1;
            for (int i=1; i<NEXP; ++i) {
                Axn[i] = (Ax * Axn[i-1])/double(i);
            }

            {
                double sum = 0;
                for (int j=0; j<NEXP; ++j)
                    sum += gamma_table[pos][n+j+1] * Axn[j];
                F[n] = sum;
            }

            if (n>0)
            {
                double expx = 1;

                for (int i=1; i<NEXP; ++i) {
                    expx += Axn[i];
                }

                expx *= gamma_table[pos][0];

                for (int i=n-1; i>=0; --i)
                    F[i] = (2*x*F[i+1] + expx)/double(2*i+1);
            }


        }
    }


    void GammaFunction::InitIncompleteGammaTable() {
        if (gamma_table!=NULL) return;

        //init arrays and array positions
        posix_memalign((void**)(&gamma_table)     , CACHE_LINE_SIZE, NVIG    *sizeof(double*));
        posix_memalign((void**)(&gamma_table_vals), CACHE_LINE_SIZE, NVIG*NGD*sizeof(double));

        //gamma_table      = new double*[NVIG];
        //gamma_table_vals = new double[NGD*NVIG];

        for (int i=0; i<NVIG; ++i) gamma_table[i] = gamma_table_vals + i*NGD;

        double Av = (vg_max-vg_min) / double (NVIG);
        for (int i=0; i<NVIG; ++i) {
            double v = vg_min + i*Av;

            gamma_table[i][0] = exp(-v);
            calcF(gamma_table[i]+1, 4*LMAX+1+NEXP, v);
        }
    }

    void GammaFunction::InitIncompleteGammaTable(int L) {
        if (gamma_table!=NULL) return;

        //init arrays and array positions
        posix_memalign((void**)(&gamma_table)     , CACHE_LINE_SIZE, NVIG     *sizeof(double*));
        posix_memalign((void**)(&gamma_table_vals), CACHE_LINE_SIZE, NVIG*NGD2*sizeof(double));

        for (int i=0; i<NVIG; ++i) gamma_table[i] = gamma_table_vals + i*NGD2;

        double Av = (vg_max-vg_min) / double (NVIG);
        for (int i=0; i<NVIG; ++i) {
            double v = vg_min + i*Av;

            double vv[32];
            calcF(vv, L+7, v);

            //first value is the exponential
            gamma_table[i][0] = exp(-v);
            //rest of values are the derivatives up to order 6
            for (int j=0; j<7; ++j) gamma_table[i][j+1] = vv[L+j];
        }
    }


    GammaFunction IncompleteGamma;
    GammaFunction IncompleteGammas[4*LMAX+3];
    ChebyshevGamma * IGammaChebyshev; //[4*LMAX+3];


    void InitChebyshevGammas(int Lmax) {

        //values of X to use in inteerpolation
        double Xn[NCHEB]; {
            for (int j=0; j<NCHEB; ++j)
                Xn[j] = cos(PI*(double(j)+0.5)/double(NCHEB));
        }

        //Chebyshev polynomials evaluated at the given X's
        double TjXk[NCHEB][NCHEB]; {
            for (int j=0; j<NCHEB; ++j)
                for (int k=0; k<NCHEB; ++k)
                    TjXk[j][k] = cos(PI*double(j)*(double(k)+0.5)/double(NCHEB));
        }

        //Chebyshev coefficients
        double Tn[NCHEB][NCHEB]; {
            for (int n=0; n<NCHEB; ++n) for (int k=0; k<NCHEB; ++k) Tn[n][k] = 0;
            Tn[0][0] = 1;
            Tn[1][1] = 1;

            for (int n=2; n<NCHEB; ++n) {
                for (int k=1; k<NCHEB; ++k) Tn[n][k]  = 2*Tn[n-1][k-1];
                for (int k=0; k<NCHEB; ++k) Tn[n][k] -=   Tn[n-2][k];
            }
        }


        double Az = FSTEP; //distance between piecewise definition of the function

        double fstep_k[NEXPC];
        for (int k=0; k<NEXPC; ++k) fstep_k[k] = pow(2*IFSTEP, k);


        for (int i=0; i<NVIGF; ++i) {

            double Fnn[32]; // array for storing the results from calling gamma

            double z = i*Az;

            //evaluate gamma
            double G[32][NCHEB]; {
                for (int j=0; j<NCHEB; ++j) {
                    calcF(Fnn, Lmax, z + 0.5*Az*Xn[j]);
                    for (int l=0; l<=Lmax; ++l)
                        G[l][j] = Fnn[l];
                }
            }

            //compute Chebyshev coefficients
            double Cj[32][NCHEB]; {

                for (int l=0; l<=Lmax; ++l)
                    for (int j=0; j<NCHEB; ++j) {
                        double sum=0;

                        for (int k=0; k<NCHEB; ++k)
                            sum += G[l][k] * TjXk[j][k];

                        Cj[l][j] = 2*sum/double(NCHEB);
                    }

                for (int l=0; l<=Lmax; ++l)
                    Cj[l][0] *= 0.5;
            }

            //scale everything
            double Pj[32][NEXPC]; {
                for (int l=0; l<=Lmax; ++l)
                    for (int k=0; k<NEXPC; ++k) {
                        double Csum=0;

                        for (int j=0; j<NEXPC; ++j)
                            Csum += Cj[l][j] * Tn[j][k];

                        Pj[l][k] = Csum * fstep_k[k]; // pow(2*IFSTEP, k);
                    }
            }

            //copy to coefficient matrix
            for (int l=0; l<=Lmax; ++l)
                for (int k=0; k<NEXPC; ++k)
                    IGammaChebyshev[l].gamma_cfs[i][k] = Pj[l][k];
        }

        #ifdef USE_GPU
        InitDeviceGammaCheb();
        #endif
    }

    bool LibIGammaInit = false;

    void InitIncompleteGammas() {

        if (LibIGammaInit) return;
        LibIGammaInit = true;

        const int Lmax = 4*LMAX+2;

        //initialize all
        IncompleteGamma.InitIncompleteGammaTable();

        for (int l=0; l<=Lmax; ++l)
            IncompleteGammas[l].InitIncompleteGammaTable(l);

        //initialize chebyshev gamma expansions
        IGammaChebyshev = new ChebyshevGamma[Lmax+1]; {
            //initialize all intermediate arrays which cannot be declared static because of their size

            double * p1 = new double[NCHEB*NCHEB];
            double * p2 = new double[NCHEB*NCHEB];
            double * p3 = new double[   32*NCHEB];
            double * p4 = new double[   32*NCHEB];

            double * TjXk [NCHEB];
            double * Tn   [NCHEB];
            double * G    [32];
            double * Cj   [32];

            for (int k=0; k<NCHEB; ++k) TjXk[k] = p1 + k*NCHEB;
            for (int k=0; k<NCHEB; ++k) Tn  [k] = p2 + k*NCHEB;
            for (int k=0; k<32;    ++k) G   [k] = p3 + k*NCHEB;
            for (int k=0; k<32;    ++k) Cj  [k] = p4 + k*NCHEB;


            //this one fits for sure
            double Pj[32][NEXPC];

            //values of X to use in inteerpolation
            double Xn[NCHEB];
            {
                for (int j=0; j<NCHEB; ++j)
                    Xn[j] = cos(PI*(double(j)+0.5)/double(NCHEB));
            }

            //Chebyshev polynomials evaluated at the given X's
            {
                for (int j=0; j<NCHEB; ++j)
                    for (int k=0; k<NCHEB; ++k)
                        TjXk[j][k] = cos(PI*double(j)*(double(k)+0.5)/double(NCHEB));
            }

            //Chebyshev coefficients
            {
                for (int n=0; n<NCHEB; ++n) for (int k=0; k<NCHEB; ++k) Tn[n][k] = 0;
                Tn[0][0] = 1;
                Tn[1][1] = 1;

                for (int n=2; n<NCHEB; ++n) {
                    for (int k=1; k<NCHEB; ++k) Tn[n][k]  = 2*Tn[n-1][k-1];
                    for (int k=0; k<NCHEB; ++k) Tn[n][k] -=   Tn[n-2][k];
                }
            }


            double Az = FSTEP; //distance between piecewise definition of the function

            double fstep_k[NEXPC];
            for (int k=0; k<NEXPC; ++k) fstep_k[k] = pow(2*IFSTEP, k);


            for (int i=0; i<NVIGF; ++i) {

                double Fnn[32]; // array for storing the results from calling gamma

                double z = i*Az;

                //evaluate gamma
                {
                    for (int j=0; j<NCHEB; ++j) {
                        calcF(Fnn, Lmax, z + 0.5*Az*Xn[j]);
                        for (int l=0; l<=Lmax; ++l)
                            G[l][j] = Fnn[l];
                    }
                }

                //compute Chebyshev coefficients
                {

                    for (int l=0; l<=Lmax; ++l)
                        for (int j=0; j<NCHEB; ++j) {
                            double sum=0;

                            for (int k=0; k<NCHEB; ++k)
                                sum += G[l][k] * TjXk[j][k];

                            Cj[l][j] = 2*sum/double(NCHEB);
                        }

                    for (int l=0; l<=Lmax; ++l)
                        Cj[l][0] *= 0.5;
                }

                //scale everything
                {
                    for (int l=0; l<=Lmax; ++l)
                        for (int k=0; k<NEXPC; ++k) {
                            double Csum=0;

                            for (int j=0; j<NEXPC; ++j)
                                Csum += Cj[l][j] * Tn[j][k];

                            Pj[l][k] = Csum * fstep_k[k]; // pow(2*IFSTEP, k);
                        }
                }

                //copy to coefficient matrix
                for (int l=0; l<=Lmax; ++l)
                    for (int k=0; k<NEXPC; ++k)
                        IGammaChebyshev[l].gamma_cfs[i][k] = Pj[l][k];
            }

            //free the memory
            delete[] p1;
            delete[] p2;
            delete[] p3;
            delete[] p4;
        }

        //initialize __constant__ memory in GPU device with
        #ifdef USE_GPU
        InitDeviceGammaCheb();
        #endif
    }

}

