


#ifndef __GAMMA__
#define __GAMMA__
#include "defs.hpp"

namespace LibIGamma {

    const double PI3  = 31.0062766802998202;   // = PI^3
    const double PI54 = 34.986836655249725693; // = 2*PI^(5/2)

    const float fPI3  = 31.0062766802998202;   // = PI^3
    const float fPI54 = 34.986836655249725693; // = 2*PI^(5/2)

    // everything is made public since
    // the inlined and specialized algorithms perform better

    // since gamma is evaluated by downward recursion, it is in principle better to have
    // one GammaFunction for each Fm[z] and adjust the number of points in the grid, grid step
    // and critical gamma acordingly; this will be eventually implemented

    const int NEXP = 4;
    const int NVIG = 1024;
    const int NGD = 32; // >= 4*LMAX + 1 + NEXP + 2;
    const int NGD2 = 8;

    const double vg_min  = 0;
    const double vg_max  = 32;
    const double vg_step  = 32./1024.;  //(vg_max-vg_min)/double(NVIG);
    const double ivg_step = 1024./32.;    //double(NVIG)/(vg_max-vg_min);

    const float fvg_min  = 0;
    const float fvg_max  = 32;
    const float fvg_step  = 32./1024.;  //(vg_max-vg_min)/double(NVIG);
    const float fivg_step = 1024./32.;    //double(NVIG)/(vg_max-vg_min);

    class GammaFunction {
      public:
        double ** gamma_table;
        double *  gamma_table_vals;
      public:
        GammaFunction();
        ~GammaFunction();
        void InitIncompleteGammaTable();
        void InitIncompleteGammaTable(int L);
        void CalcGammas(double * F, int n, double x) const;
    };

    extern GammaFunction IncompleteGamma;
    extern GammaFunction IncompleteGammas[4*LMAX+3];


    //control these paramenters
    const int MAXZ  = 32;
    const int FDENS = 2;
    const int NEXPC = 8;
    const int NCHEB = 512;


    const int     NVIGF = MAXZ*FDENS;
    const double FSTEP  = 1./double(FDENS);
    const double IFSTEP =    double(FDENS);


    class ChebyshevGamma {
      public:
        double gamma_cfs[NVIGF][NEXPC];
      public:
        void InitChebyshevGamma(int L);
    };


    extern ChebyshevGamma * IGammaChebyshev;

    void InitChebyshevGammas(int Lmax);
    void InitIncompleteGammas();

    extern bool LibIGammaInit;
}

void InitDeviceGammaCheb(); //this routine is in IICgpuK4.cu

#endif
