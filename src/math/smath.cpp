#include <cmath>
#include "../math/smath.hpp"

double pown(double v, int n) {
    if (n==1) return v;
    else if (n==0) return 1.;

    double v2 = pown(v, n/2);

    if (n%2 == 0) return v2*v2;
    else return v2*v2*v;
}


