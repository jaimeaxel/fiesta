
namespace math{
   double TaylorCoeff(const int& arI);
   double pow(double x, int n);
   int factorial(const int& n);
   double doublefactorialrec(int n);
   double doublefactorialnegrec(int n);
   double doublefactorial(int n);
   long int factorial2(const int& n);
   long double doublefactorialrec2(const int& n);
   long double doublefactorialnegrec2(const int& n);
   long double doublefactorial2(const int& n);
}

