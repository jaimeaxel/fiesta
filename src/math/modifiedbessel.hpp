
#ifndef __BESSEL__
#define __BESSEL__

#include "../defs.hpp"

template <int N, int T> 
class ModBessel
{
   private:
      bool Initialized = false;
      double R1 = 1.0e-7;       
      double R2 = 16.0e0;       
      double Step = 0.01;
      double ModBesselTable[N][2*LMAX+1][T];

      double ModifiedBesselKoester(const int& arLambda, const double& arZ) const ;
      double ApFirstTermModifiedBessel(const int& arLambda, const double& arZ) const ;
      double ApModifiedBesselLargeZ(const int& arLambda, const double& arZ) const ;
      double ModifiedBesselPowerSeries(const int& arLambda, const double& arZ, const double& arTresh) const;
      double ModifiedBesselTaylor(const int& arLambda, const double& arZ) const ;
      
   public:
      ModBessel(): Step(R2/(N-1)) {;}
      void InitTable();
      double operator() (const int& arLambda, const double& arZ)  const {return ModifiedBesselKoester(arLambda,arZ);} 
      double ModifiedBesselKoesterPowerSeries(const int& arLambda, const double& arZ) const;
      double ModifiedBesselKoesterTaylor(const int& arLambda, const double& arZ) const;
};



namespace math{
   namespace bessel{
      extern ModBessel<16001,5> ModifiedBesselEcp;
      double R(const int& arLambda, const double& arZ);
      double M(const int& arLambda, const double& arZ);
      double Menz(const int& arLambda, const double& arZ, const double& arEta);
   }
}

#include "../math/modifiedbessel.icpp"
#endif
