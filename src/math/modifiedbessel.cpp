
#include <iostream>
#include "../math/modifiedbessel.hpp"

namespace math{
   namespace bessel{
      ModBessel<16001,5> ModifiedBesselEcp;


      /* *****************************************************************
      *          Eq. (39) in J Comput Chem 27: 1009-1019, 2006.
      * *****************************************************************/
      double R(const int& arLambda, const double& arZ)
      {
         double v =  0.e0;
         double c = 1.0; // turns into (2z)^k
         double n = math::factorial(arLambda); // turns into (lambda+k)!
         double fk = 1.0e0; // turns into (k)!
         double twoz = 2.0e0*arZ;
         int k = 0;
         while (k <= arLambda)
         {
            double tmp = fk*math::factorial(arLambda-k)*c ;
            v += n/tmp;
      
            ++k;
            n *= (arLambda+k);
            fk *= (k);
            c *= twoz;
         }
      
         return v;
      }
      /* *****************************************************************
      *   Returns value of the modified spherical bessel function of first kind
      *          Eq. (38) in J Comput Chem 27: 1009-1019, 2006.
      *   commented out due to severe instabilities for low z
      * *****************************************************************/
      /*
      double M(const int& arLambda, const double& arZ)
      {
         if (fabs(arZ)< 1.0e-12)
            return 1.0e0;
         double v =  R(arLambda,-arZ)*std::exp(arZ);
         v -=  math::pow(-1,arLambda)*R(arLambda,arZ)*std::exp(-arZ);
         v /= (2.0*arZ);
         return v;
      }
      */
      /* *****************************************************************
      *   Returns value of the modified spherical bessel function of first kind
      *        multiplied by E^(-eta) 
      *          Eq. (38) in J Comput Chem 27: 1009-1019, 2006.
      *   commented out due to severe instabilities for low z
      * *****************************************************************/
      /*
      double Menz(const int& arLambda, const double& arZ, const double& arEta)
      {
         if (fabs(arZ)< 1.0e-10)
         {
            if (arLambda==0)
               return std::exp(-arEta);
            else 
               return 0.0e0;
         }
         double v = 0.0e0;
         if (arLambda <=1)
         {
            v =  R(arLambda,-arZ)*std::exp(arZ-arEta);
            //std::cout << " M1 " << v << std::endl;
            v -=  math::pow(-1,arLambda)*R(arLambda,arZ)*std::exp(-arZ-arEta);
            // std::cout << " M2 " << v << std::endl;
            v /= (2.0*arZ);
            std::cout << " arLamba " << arLambda << " " <<  v << std::endl;
         }
         else
         {
            v = Menz(arLambda-2, arZ, arEta) - (2.0e0*(arLambda-1)+1)/arZ* Menz(arLambda-1, arZ, arEta);
            std::cout <<  "arLambda " << arLambda << " : " <<  Menz(arLambda-2, arZ, arEta)  << " - " <<  (2.0e0*(arLambda-1)+1)/arZ << " * " <<  Menz(arLambda-1, arZ, arEta) << std::endl;
         }
         return v;
      }
      */
   }
}
