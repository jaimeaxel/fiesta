


#ifndef __SHELL_PAIR__
#define __SHELL_PAIR__

#include "defs.hpp"

class ShellPairPrototype;
class point;

struct ShellPair {
    const ShellPairPrototype * GP;                                  // 8 bytes
    double                   * WW;                                  // 8 bytes

	uint16_t ata;       // number of atom A                              // 2 bytes
	uint16_t atb;       // number of atom B                              // 2 bytes

	uint32_t cdos;      // cholesky decomposition matrix offset          // 4 bytes

	float logCS;        // Cauchy-Schwarz parameter logarithm            // 4 bytes        // use half float, i.e. 2 bytes

	uint16_t nK2;       // number of significant gaussians of the pair   // 2 bytes        // 1 1/2 byte should suffice; 64*64 = 4096
	uint16_t flags;     // samefunction sameatom, invert prototype       // 2 bytes        // 1 nibble (4 bits) suffice




	void Form (const ShellPairPrototype & prototype, const point & c1, const point & c2, double * ww, double lmo);
	void Form (const ShellPairPrototype & prototype, const point & c1, double * ww);

    double  getW(int a, int b) const;
    int     getKb() const;
    int     getKa(int Kb) const;
    int     getLa() const;
    int     getLb() const;
    int     getFa() const;
    int     getFb() const;
    bool    sameF() const;
    bool    sameAtom() const;
    bool    Inverted() const;

} __attribute__((aligned(32)));

#endif
