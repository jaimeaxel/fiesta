
#include <sstream>
using namespace std;

#include "defs.hpp"
#include "fiesta/fiesta.hpp"
#include "basis/atomdef.hpp"
#include "low/strings.hpp"
#include "low/io.hpp"
#include "low/mem.hpp"

#include "defs.hpp"
#include "math/affine.hpp"
#include "math/smath.hpp"

//definiciones atomicas
AtomDefList Fiesta::AtomDefs;


AtomDefList::AtomDefList() {
    nAtomDefs = 0;
    DefList = NULL;
}

ElementID AtomDefList::AtomType (const string & name) const {
    string key = ucase(name);
    map<string, ElementID>::const_iterator it;

    it = AtomTypes.find(key);

    if (it!=AtomTypes.end()) return it->second;
    else                     return InvalidElementID;
}


AtomDef & AtomDefList::operator[](ElementID id) {
    if (id==InvalidElementID) {
        throw Exception("Attempting to access atom definitions with invalid key");
    }
    return DefList[id]; //this->operator[](id); //return DefList[n-1];
}

AtomDef & AtomDefList::operator[] (const string & name) {
    ElementID id = AtomType(name);
    return this->operator[](id); //return DefList[n-1];
}

const AtomDef & AtomDefList::operator[](ElementID id) const {
    if (id==InvalidElementID) {
        throw Exception("Attempting to access atom definitions with invalid key");
    }
    return DefList[id]; //return this->operator[](id); //DefList[n-1];
}

const AtomDef & AtomDefList::operator[] (const string & name) const {
    ElementID id = AtomType(name);
    return this->operator[](id); // DefList[n-1];
}


// read atom definitions
void AtomDefList::Load(const string & file) {

    // read the file
    FileBuffer AtomsFile(file);

    std::list<Line>::iterator it = AtomsFile.LineArray.begin();
    std::list<Line>::iterator iend = AtomsFile.LineArray.end();

	DefList = new AtomDef[AtomsFile.LineArray.size()]; // this is an upper bound

    int n = 0;

    //ciclo de lectura de definiciones
    while (it!=iend){

        istringstream sline;
        sline.str(it->str);
        sline >> DefList[n].name >> DefList[n].Z >> DefList[n].mass;
        sline.clear();

        string key = ucase(DefList[n].name);
        AtomTypes[key] = n; // 16 bits

		// too many elements (65535 should suffice)
		if (n>65534) throw Exception("Too many element definitions in '" + file + "'");

        ++it;
        ++n;
    }

    nAtomDefs = n;
}


AtomDefList::~AtomDefList() {
    if (nAtomDefs > 0)
        delete[] DefList;
}

