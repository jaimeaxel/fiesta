#ifndef __ATOM_DEFS__
#define __ATOM_DEFS__

#include <string>
#include <map>


typedef uint16_t ElementID;

const ElementID InvalidElementID = -1; // maxmium uint16_t value

// definition of the element / isotope / dummy
struct AtomDef {
    double      Z;   // charge
    double      mass;
    std::string name;
};

struct AtomDefList {

    AtomDef * DefList;    // list of atomic definitions
    uint16_t nAtomDefs;   // number of definitions

    std::map<std::string, ElementID> AtomTypes;

    AtomDefList();
   ~AtomDefList();

    void      Load     (const std::string & file); // read elements from file
    ElementID AtomType (const std::string & name) const;

          AtomDef & operator[] (ElementID n);
    const AtomDef & operator[] (ElementID n) const;
          AtomDef & operator[] (const std::string & name);
    const AtomDef & operator[] (const std::string & name) const;
};


#endif
