


#ifndef __ShellPairPrototype__
#define __ShellPairPrototype__

#include "defs.hpp"
#include "low/rtensors.hpp"

class GTO;
class GTObasis;
class ShellPairPrototype;


/*
     some of the classes below (perhaps all but ShellPairs)
     should be moved to LibQuimera, since the batch constants
     are exclusively used to accelerate the evaluation of ERIs
*/

class BatchConstantsPair {
  public:

    double Na[maxK][maxJ];
    double Nb[maxK][maxJ];

    //double U[maxK][LM4];
    //double V[maxK][maxK][LM6];

    double r[maxK][maxK];
    double k[maxK][maxK];

    double K[maxK][maxK];

    double b1[maxK];
    double b2[maxK];

    double ika[maxK];
    double ikb[maxK];

    double ka[maxK];
    double kb[maxK];

    double UV[32]; //32>=6*LMAX+1

    BatchConstantsPair();
   ~BatchConstantsPair();

    void From(const ShellPairPrototype & AB);
};

class PrimitiveSet {
  public:
    uint8_t nKa[maxK];
    double maxD2;
    uint16_t nK2;
    uint8_t nKb;

    uint8_t align[21];
};
//64 bytes

struct BCP_GPU {
    double * ddp;  //device double pointer (holds all following variables)

    double * dka;
    double * dkb;

    double * dika;
    double * dikb;

    double * dikab;

    double * diks3;
    double * dkab;

    double * dNva;
    double * dNub;

    double * dNa;
    double * dNb;
};

class ShellPairPrototype {

  public:
    double Na[maxJ][maxK];  // weights/coefficients for primitive contraction
    double Nb[maxJ][maxK];  //

    double Nap[maxJ][maxK]; // weights/coefficients for primitive contraction for SP shells (not used anymoar)
    double Nbp[maxJ][maxK]; //

    double ka[maxK];        // primitive coefficients
    double kb[maxK];

    double kr[maxK][maxK];  // a*b / (a+b)

    double kmin;


    PrimitiveSet Psets[maxK2];    // not the most elegant solution; the solution used in the GPU code is better
    BatchConstantsPair BCP;       // constants for K4 contraction
    BCP_GPU BCPgpu[MAX_GPUS];     // constants for K4 contraction on device (GPU)


    //integers
    uint16_t  atype1; // original element IDs
    uint16_t  atype2;
    uint8_t   eid1;   // element IDs in the reduced basis
    uint8_t   eid2;

    uint8_t   nb1;    // number of shell within element
    uint8_t   nb2;
    uint32_t  num;

    uint8_t l1;      // angular momenta
    uint8_t l2;      //

    uint8_t ll1;      //
    uint8_t ll2;      //

    uint16_t f1;      // shell 1 offset within tensor (for cartesian shells)
    uint16_t f2;      //

    uint16_t fs1;     // shell 1 offset within tensor (for spherical shells)
    uint16_t fs2;

    uint16_t wb1;     // total number of functions from center 1
    uint16_t wb2;     //numero total de funciones 2

    uint16_t Kt;      // total degree of contraction
    uint8_t Ka;       // contraction degree a
    uint8_t Kb;       // contraction degree b

    uint8_t Ja;       // number of primitives center a
    uint8_t Jb;       // number of primitives center b


    uint64_t nShellPairs; // number of shell pairs associated with this prototype


    bool samef;    // product of shell with itself (same center)
    bool sameatom; // product of shells centered im same atom
    bool inverted; // whether or not the product is inverted (if A & B must be swapped)

    bool SP1;      // Pople-style SP shell ?
    bool SP2;


    ShellPairPrototype();
    void BasisProd (const GTO & A);
    void BasisProd (const GTO & A, const GTO & B);
    void InitGPU();
};

class ShellPairs {
  public:
    ShellPairPrototype **** GP;
    ShellPairPrototype *** GP2;
    ShellPairPrototype ** GP3;
    ShellPairPrototype * GP4;

    ShellPairPrototype ** GPsame;
    ShellPairPrototype * GPsame2;

    int nGP;
    int nGPsame;

    Array<uint16_t> AType;

    void GenerateFrom (const Array<uint16_t> & AtomTypes, const GTObasis & basis);
};


#endif
