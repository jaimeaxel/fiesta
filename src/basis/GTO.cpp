/*
    GTO.cpp

    definici�n de funcion base GTO y de base.
*/

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <tuple>


#include "basis/GTO.hpp"
using namespace std;

#include "math/angular.hpp"
using namespace LibAngular;

#include "linear/tensors.hpp"
#include "linear/eigen.hpp"

#include "low/io.hpp"
#include "low/mem.hpp"
#include "low/strings.hpp"   // exc�ption

#include "fiesta/fiesta.hpp" // messenger
using namespace Fiesta;

#include "defs.hpp"
#include "basis/atomdef.hpp"
#include "fiesta/calculate.hpp"



bool ExplicitSPsupport = false;

/*
inline double ipow(double v, int i) {
    if (i<0) {Messenger << "error in ipow; called with i="<<i << endl; return 1;}

    double r=1;

    while (i) {
        if (i%2) r*=v;
        v*=v;
        i/=2;
    }
    return r;
}
*/

GTO & GTO::operator=(const GTO & rhs) {
    for (int kk=0; kk<maxK; ++kk) k[kk] = rhs.k[kk];

    for (int jj=0; jj<maxJ; ++jj)
        for (int kk=0; kk<maxK; ++kk) N[jj][kk] = rhs.N[jj][kk];

    for (int jj=0; jj<maxJ; ++jj)
        for (int kk=0; kk<maxK; ++kk) Np[jj][kk] = rhs.Np[jj][kk];

    l  = rhs.l;
    K  = rhs.K;
    J  = rhs.J;
    SP = rhs.SP;

    return *this;
}

static inline double ipow(double v, int i) {

    if (i<0) {cout << "error in ipow; called with i="<<i << endl; return 1;}
    if (i==0) return 1;

    // skip all zeros
    while (i%2==0) {
        v*=v;
        i/=2;
    }

    // first bit
    double r=v;
    i/=2;

    // do rest
    while (i) {
        v*=v;
        if (i%2) r*=v;
        i/=2;
    }
    return r;
}


double GTO::operator()(const vector3 & v, int j) const {

    double r2 = v*v;
    double r  = sqrt(r2);
    double ir = 1/r;
    //v*=ir;

    // radial part
    /*
    double fr; {
        fr=0;
        for (int i=0; i<K; ++i) fr += N[j][i] * exp(-k[i]*r2);
        fr *= ipow(r,l);
    }
    */

    /*
    // angular part
    for (int m=0; m<=2*l+1; ++m) {

        double s=0;

        for (int p=0; p<SHList[l][m].nps; ++p) {

            int i=SHList[l][m].T[p].nx;
            int j=SHList[l][m].T[p].ny;
            int k=SHList[l][m].T[p].nz;

            double w=SHList[l][m].T[p].cN;

            s += w * ipow(v.x,i) * ipow(v.y,j) * ipow(v.z,k);
        }

        fr*s;
    }
    */

    return 0; // fr;
}


inline double RadialIntegral(double k, int L) {
    double kn = 1;
    for (int i=0; i<2*L+3; ++i)
        kn *= k;
    return hGamma[L+1] / (2*sqrt(kn));
}


//for some reason the definition in defs.hpp is not detected here or in the K4 algorithm
#define __QR_GC__


//seminormaliza (solo parte radial) una funcion base de tipo GTO
void GTO::Normalize() {

    //sort primitives by ascending gaussian exponent
    for (int kk=0; kk<K; ++kk) {
        double minW = k[kk];
        int kmin = kk;

        for (int jj=kk; jj<K; ++jj) if (k[jj]<minW) {minW = k[jj]; kmin = jj;}

        //swap everything
        swap(k[kk], k[kmin]);

        for (int jj=0; jj<J; ++jj)
            swap(N[jj][kk], N[jj][kmin]);

        if (SP)
        for (int jj=0; jj<J; ++jj)
            swap(Np[jj][kk], Np[jj][kmin]);

    }


    // loop over all contracted functions
    // and normalize the radial part
    for (int jj=0; jj<J; ++jj) {

        if (SP) {
            //S
            {
                //primero normaliza las funciones gausianas
                for (int kk=0; kk<K; ++kk) {
                    double I = RadialIntegral(2*k[kk],0);
                    N[jj][kk] *= 1/sqrt(I);
                }

                //ahora termina de ajustar los coeficientes
                double cp=0.;
                for (int kk=0; kk<K; ++kk) {
                    cp += N[jj][kk]*N[jj][kk]*RadialIntegral(2*k[kk],0);

                    for (int jk=0;jk<kk;++jk)
                        cp += 2*N[jj][kk]*N[jj][jk]*RadialIntegral(k[kk]+k[jk],0);
                }

                cp = 1./sqrt(cp);

                for (int kk=0; kk<K; ++kk)
                    N[jj][kk] *= cp;
            }

            //P
            {
                //primero normaliza las funciones gausianas
                for (int kk=0; kk<K; ++kk) {
                    double I = RadialIntegral(2*k[kk],1);
                    Np[jj][kk] *= 1/sqrt(I);
                }

                //ahora termina de ajustar los coeficientes
                double cp=0.;
                for (int kk=0; kk<K; ++kk) {
                    cp += Np[jj][kk]*Np[jj][kk]*RadialIntegral(2*k[kk],1);

                    for (int jk=0; jk<kk; ++jk)
                        cp += 2*Np[jj][kk]*Np[jj][jk]*RadialIntegral(k[kk]+k[jk],1);
                }

                cp = 1./sqrt(cp);

                for (int kk=0; kk<K; ++kk)
                    Np[jj][kk] *= cp;
            }

        }
        else {
            //primero normaliza (radialmente) las funciones gausianas
            for (int kk=0; kk<K; ++kk) {
                double I = RadialIntegral(2*k[kk],l);
                N[jj][kk] *= 1/sqrt(I);
            }

            //normaliza radialmente toda la funcion base (en caso de errores en el archivo de entrada)
            double cp=0.;
            for (int kk=0;kk<K;++kk) {
                cp += N[jj][kk]*N[jj][kk]*RadialIntegral(2*k[kk],l);

                for (int j=0;j<kk;++j) {
                    cp += 2*N[jj][kk]*N[jj][j]*RadialIntegral(k[kk]+k[j],l);
                }
            }

            cp = 1./sqrt(cp);

            //Messenger << " normalization factor " << cp << endl;

            for (int kk=0; kk<K; ++kk)
            {
                N[jj][kk] *= cp;
            }
        }

    }


    tensor2 S;
    S.setsize(J);

    // make sure they're orthogonal (which is necessary in other modules) // which ones?
    double rms1 = 0;
    for (int jj=0; jj<J; ++jj) {
        for (int kk=0; kk<J; ++kk) {

            double cp=0.;
            for (int ki=0; ki<K; ++ki) {
                for (int kj=0; kj<K; ++kj) {
                    cp += N[jj][ki]*N[kk][kj]*RadialIntegral(k[ki]+k[kj],l);
                }
            }

            S(jj,kk) = cp;

            if (jj==kk) cp -= 1.;
            rms1 += cp*cp;
        }
    }


    double s[J];
    DiagonalizeV(S, s);


    for (int jj=0; jj<J; ++jj) s[jj] = 1./sqrt(s[jj]);

    //orthonormalize the functions
    for (int kk=0; kk<K; ++kk) {

        double t[J];

        for (int jj=0; jj<J; ++jj) {
            double sum = 0;
            for (int jk=0; jk<J; ++jk) {
                sum += S(jj,jk) * N[jk][kk];
            }
            t[jj] = sum * s[jj];
            //Messenger << "t[jj] " << t[jj] << endl;
        }

        for (int jj=0; jj<J; ++jj) {
            N[jj][kk] = t[jj];
        }
    }



    //generate a linear combination of generally contracted functions to minimize calculation time
    //have to think about a different way of defining GC functions

    //#ifdef __QR_GC__


    double u[J];
    double p[K];

    //Hausholder QR with sorting
    for (int jk=0; jk<J; ++jk) {

        //find the function with the largest k-th component
        int jmax=jk;
        for (int jj=jk+1; jj<J; ++jj)
            if (abs(N[jj][jk]) > abs(N[jmax][jk])) jmax = jk;

        //swap basis functions
        for (int kk=jk; kk<K; ++kk) swap(N[jmax][kk], N[jk][kk]);

        double sum = 0;

        //create vector u [i+1 ... n]
        for (int jj=jk; jj<J; ++jj) u[jj] = N[jj][jk];
        for (int jj=jk; jj<J; ++jj) sum += u[jj]*u[jj];

         //try to minimize errors and avoid overflows
         double H;
         if (u[jk]>0) {
             H = sum + sqrt(sum)*u[jk];
             u[jk] += sqrt(sum);
         }
         else {
             H = sum - sqrt(sum)*u[jk];
             u[jk] -= sqrt(sum);
         }

         //compute p = (A*u)/H
         for (int kk=jk; kk<K; ++kk) {
             double tot = 0;

             for (int jj=jk; jj<J; ++jj) tot += N[jj][kk] * u[jj];

             p[kk] = tot/H;
         }

         // updates matrix
         for (int jj=jk; jj<J; ++jj)
             for (int kk=jk; kk<K; ++kk)
                 N[jj][kk] -= p[kk]*u[jj];

        // set to 0 the coeficients
        for (int jj=jk+1; jj<J; ++jj) N[jj][jk] = 0;
     }

    //#endif



    // test for orthonormality (necessary elsewhere)
    double rms2 = 0.;

    for (int jj=0; jj<J; ++jj) {
        for (int jk=0; jk<J; ++jk) {
            double cp=0.;
            for (int ki=0;ki<K;++ki) {
                for (int kj=0;kj<K;++kj) {
                    cp += N[jj][ki]*N[jk][kj]*RadialIntegral(k[ki]+k[kj],l);
                }
            }
            //S(jj,kk) = cp;
            if (jj==jk) cp -= 1.;
            rms2 += cp*cp;
        }
    }

    /*
    if (rms1>100000*rms2) {
        Debugger.precision(16);
        Debugger << "Warning! A generally contracted set of functions was orthonormalized" << endl;
        Debugger << "  Old block RMS = " << sqrt(rms1) << endl;
        Debugger << "  New block RMS = " << sqrt(rms2) << endl;
        Debugger << endl;
    }
    */

    /*
    //LAST TEST

    //makes sure they're orthogonal (necesario en varias partes del calculo (cuales?) )
    rms1 = 0;
    for (int jj=0; jj<J; ++jj) {
        for (int kk=0; kk<J; ++kk) {


            double cp=0.;
            for (int i=0;i<K;++i) {
                for (int j=0;j<K;++j) {
                    cp += N[jj][i]*N[kk][j]*RadialIntegral(k[i]+k[j],l);
                }
            }

            S(jj,kk) = cp;

            if (jj==kk) cp -= 1.;
            rms1 += cp*cp;
        }
    }
    cout << S << endl;



    cout.precision(16);
    for (int p=0; p<K; ++p) {
        cout << k[p] << "  ";
        for (int j=0; j<J; ++j) {
            cout << "  " << N[j][p];
        }
        cout << endl;
    }
    cout << endl;
    */

}

// append another basis
ElementBasis & ElementBasis::operator+= (const ElementBasis & rhs) {

    // just adds the shell
    shell += rhs.shell;

    if (Zcore!=rhs.Zcore) {
        Messenger << "Warning; mixing basis sets optimized for different pseudopotentials!" << endl;    }

    return *this;
}

const Array<GTO> & GTObasis::operator[] (ElementID i) const {

    if (i==0) Messenger << i << endl; // this is an error

    //check everything is in order
    if (AtomBasis.count(i) == 0) {
        Messenger << "Error; basis set not defined for element" << endl;
        throw (643);
    }
    return AtomBasis.at(i).shell;
}

void GTObasis::Load(const std::string & file) {
    name = file;
    Load();
}

// load a basis file formatted in gaussian '94 style with a few extensions;
// 1) support for general contracted functions
// 2)
void GTObasis::Load() {

    // which angular momenta are used?
    LSP = false;
    for (int l=0; l<=LMAX; ++l) LS[l] = false;

    // read the file
    FileBuffer BasisFile(name);

    std::list<Line>::iterator it = BasisFile.LineArray.begin();
    std::list<Line>::iterator iend = BasisFile.LineArray.end();


    // find the name of the basis set
    // and print it
	{
        if (it == iend) {
            throw Exception("Basis set file format error: file broken");
        }

        //cuidado: el 'if' incrementa la linea
        if (it->str.substr(0,6) != "BASIS=") {
            throw Exception("Basis set file format error: basis set name undefined");
        }

        else {
            name = it->str.substr(6);
            name = cleanstr(name);

            // drop quotation marks if present
            int first = name.find_first_not_of("\"");
            int last  = name.find_last_not_of("\" ");

            name = name.substr(first, last-first+1);
        }

        Messenger << "Using basis set: " << name << endl << endl;

        ++it;
	}

	// find out  whether the basis
	// is segmented or GC
	{
        if      (it->str.substr(0,7) == "GENERAL")   GC = true;
        else if (it->str.substr(0,9) == "SEGMENTED") GC = false;
        else    throw Exception("Basis set file does not specify whether it is segmented or generally contracted");
        ++it;
	}

	// Pople-style basis sets
    bool SP = false;

    // for storing the data
    map <ElementID, list<GTO*> > GTOpool;

    // process the basis set file
    while (it != iend) {

        // read the element's name
        string name; int Q; {
            istringstream sline;
            sline.str(it->str);
            sline >> name;
            sline >> Q;
            sline.clear();
            ++it;
        }

        // find its ID
        ElementID at = AtomDefs.AtomType(name);

        // skip basis sets for unknown atoms
        if (at == InvalidElementID) {
            Messenger << "Warning: atom '" << name << "' can't be found in the atom definitions file" << endl;

            while (it != iend) {
                if (it->str.substr(0,4) == "****") {
                    ++it;
                    break;
                }
                ++it;
            }
            continue;
        }

        AtomBasis[at].Zcore = Q;


        // read the basis of atom 'at'
        while (it != iend) {

            string sh; // shell type
            int nks;   // number of primitives
            int njs;   // number of GC shells

            // figure out the type of shell and
            // its parameters
            {
                istringstream sline;
                sline.str(it->str);
                sline >> sh >> nks >> njs;
                sline.clear();
                ++it;
            }

            // GTOpool[at].is_empty(); //dummy statement to force adding a basis queue, even if the atom contains no basis 2013/04/02
            if (sh.substr(0,4) == "****") break; // end of atom basis info

            if (nks>maxK)
                throw Exception (int2str(nks)+" primitives in contracted function exceeds program limitation of "+ int2str(maxK) );

            if (njs>maxJ)
                throw Exception (int2str(njs)+" general contracted functions exceeds program limitation of "+ int2str(maxJ) );

            // Pople-style shells & extensions
            int nLs = int(sh.size());
            int Ls[nLs];

            for (int i=0; i<nLs; ++i) {
                Ls[i] = S2L(sh[i]);
                if (Ls[i]>LMAX)
                    throw Exception ("Angular momentum "+int2str(Ls[i])+" exceeds program limitation of "+ int2str(LMAX) );
                if (Ls[i]<0)
                    throw Exception ("Angular momentum "+int2str(Ls[i])+" is meaningless" );
            }

            // restirct fused batches only to SP ones
            if (nLs>2)
                throw Exception ("Pople-style function with more than 2 subshells found" );

            else if (nLs==2) {
                if (Ls[0]!=0 || Ls[1]!=1)
                    throw Exception ("Pople-style function is not an SP shell");
                else
                    SP = true;
            }

            // copy data to gto
            {
                GTO * ngto = new GTO;

                // set basic shell parameters
                if (nLs==2) {
                    ngto->SP = true;
                    ngto->l = LSP;
                }
                else {
                    ngto->SP = false;
                    ngto->l = Ls[0];
                }

                ngto->K = nks;
                ngto->J = njs;

                // set the shell flag to true (for later initialization)
                LS[ngto->l] = true;


                // read data block
                for (int i=0; i<nks; ++i) {

                    if (it==iend)
                        throw Exception ("Basis set file format error: file broken");

                    istringstream sline;
                    sline.str(it->str);

                    //set guassian exponent
                    sline >> ngto->k[i];

                    //set coefficients
                    for (int j=0; j<njs; ++j)
                        sline >> ngto->N[j][i];

                    //set P coefficients for mixed SP shells
                    if (ngto->SP)
                        for (int j=0; j<njs; ++j)
                            sline >> ngto->Np[j][i];

                    sline.clear();
                    ++it;

                    if (ngto->k[i]<=0)
                        throw Exception ("Basis set file error: invalid gaussian exponent "+float2str(ngto->k[i]));
                }

                // split SP shells if code does not
                // support explicit SP batches
                if (ngto->SP && !ExplicitSPsupport) {

                    GTO * ngtop = new GTO;

                    ngto->SP  = false;
                    ngto->l   = 0;

                    ngtop->SP = false;
                    ngtop->l  = 1;

                    ngtop->K = ngto->K;
                    ngtop->J = ngto->J;

                    // move primitives to P shell
                    for (int i=0; i<ngtop->K; ++i) {
                        // set guassian exponent
                        ngtop->k[i] = ngto->k[i];

                        // set coefficients
                        for (int j=0; j<ngtop->J; ++j) {
                            ngtop->N[j][i] = ngto->Np[j][i];
                        }
                    }

                    GTOpool[at].push_back(ngto);
                    GTOpool[at].push_back(ngtop);
                }
                //push the pointer to a stack
                else
                    GTOpool[at].push_back(ngto);
            }
        }
    }



	if (SP && !ExplicitSPsupport) {
	    Messenger << "Basis set contains Pople-style SP shells, not natively supported by the present ERI module; S and P shells have been split" << endl;
	}

    //todo lo siguiente es necesario debido a dos factores:
    //1) los atomos pueden no estar en orden, ya sea en el archivo de definiciones atomicas o en las de la base
    //2) pueden haber atomos repetidos; en este caso se deben juntar las definiciones de base

	//copia los GTOs a un array estatico
	map <ElementID, list<GTO*> >::iterator git = GTOpool.begin();

	while (git != GTOpool.end()) {

		ElementID Id          = git->first;
		list<GTO*> & oGTOlist = git->second;
		int nGTO = oGTOlist.size();

		// copy everything to a contiguous array
		list<GTO*>::iterator lit = oGTOlist.begin();
		AtomBasis[Id].shell.set(nGTO);

		for (int i=0; i<nGTO; ++i) {
            AtomBasis[Id][i] = *(*lit);
			delete (*lit);
			++lit;
		}
		++git;
	}

	GTOpool.clear();


	// normalize the basis
	map <ElementID, ElementBasis >::iterator ait = AtomBasis.begin();

	while (ait != AtomBasis.end()) {
		for (int i=0; i<ait->second.Nshells(); ++i)
		    ait->second[i].Normalize();
		++ait;
    }

}

GTObasis & GTObasis::operator+=(const GTObasis & rhs) {

    name += " + " + rhs.name;

	map <ElementID, ElementBasis>::const_iterator it = rhs.AtomBasis.begin();

	for (it; it!=rhs.AtomBasis.end(); ++it) {
	    ElementID ID = it->first;

	    if (AtomBasis.count(ID)==1)  AtomBasis[ID] += it->second; // merge basis
	    else                         AtomBasis[ID]  = it->second; // add basis
	}

	for (int l=0; l<LMAX+2; ++l) LS[l] |= rhs.LS[l];

	GC  |= rhs.GC;
	LSP |= rhs.LSP;

	return *this;
}

GTObasis::~GTObasis () {

	// destroy all arrays
	map <ElementID, ElementBasis>::iterator ait = AtomBasis.begin();
	int i=0;
	while (ait != AtomBasis.end()) {
		ait->second.shell.clear();
		++i;
		++ait;
    }
}



void ShellPairs::GenerateFrom (const Array<uint16_t> & AtomTypes, const GTObasis & BasisSet) {

    //copia
    AType = AtomTypes;

    {
        int totnb = 0;
        for (int i=0; i<AtomTypes.n; ++i) {
            totnb += BasisSet[AtomTypes[i]].n;
        }
        nGP = (totnb+1)*totnb /2;

        GP4 = new ShellPairPrototype[nGP];


        int itot=0;

        //inicia las estructuras
        GP = new ShellPairPrototype***[AtomTypes.n];

        for (int i=0; i<AtomTypes.n; ++i) {
            GP[i] = new ShellPairPrototype**[i+1];
            int nb1 = BasisSet[AtomTypes[i]].n;

            for (int j=0; j<i; ++j) {
                GP[i][j] = new ShellPairPrototype*[nb1];
                int nb2 = BasisSet[AtomTypes[j]].n;

                for (int b1=0; b1<nb1; ++b1) {
                    GP[i][j][b1] = GP4 + itot; //new ShellPairPrototype[nb2];
                    itot += nb2;
                }
            }

            GP[i][i] = new ShellPairPrototype*[nb1];

            for (int b1=0; b1<nb1; ++b1) {
                GP[i][i][b1] = GP4 + itot; //new ShellPairPrototype[b1+1];
                itot += b1+1;
            }
        }
    }

    {
        nGPsame = 0;
        for (int i=0; i<AtomTypes.n; ++i) {
            int n = BasisSet[AtomTypes[i]].n;
            nGPsame += n;
        }
        GPsame2 = new ShellPairPrototype[nGPsame];

        int itot = 0;

        //same atom
        GPsame = new ShellPairPrototype*[AtomTypes.n];

        for (int i=0; i<AtomTypes.n; ++i) {
            GPsame[i] = GPsame2 + itot;
            int id1 = AtomTypes[i];
            int nb1 = BasisSet[id1].n;
            itot += nb1;
        }
    }



    int n  = 0;
    int ns = 0;

    //inicia los productos
    for (int i=0; i<AtomTypes.n; ++i) {
        int id1 = AtomTypes[i];
        int nb1 = BasisSet[id1].n;

        for (int j=0; j<i; ++j) {
            int id2 = AtomTypes[j];
            int nb2 = BasisSet[id2].n;

            int offset1 = 0;
            for (int b1=0; b1<nb1; ++b1) {
                const GTO & g1 = BasisSet[id1][b1];

                int offset2 = 0;
                for (int b2=0; b2<nb2; ++b2) {
                    const GTO & g2 = BasisSet[id2][b2];

                    GP4[n].BasisProd(g1, g2);
                    GP4[n].atype1 = AtomTypes[i];
                    GP4[n].atype2 = AtomTypes[j];
                    GP4[n].nb1    = b1;
                    GP4[n].nb2    = b2;
                    //GP4[n].num    = n;

                    GP4[n].f1 = offset1;
                    GP4[n].f2 = offset2;

                    ++n;

                    offset2 += nmS[g2.l];
                }
                offset1 += nmS[g1.l];
            }

        }

        //i==j
        {
            int offset1 = 0;
            for (int b1=0; b1<nb1; ++b1) {
                const GTO & g1 = BasisSet[id1][b1];

                int offset2 = 0;
                for (int b2=0; b2<b1; ++b2) {
                    const GTO & g2 = BasisSet[id1][b2];

                    GP4[n].BasisProd(g1, g2);
                    GP4[n].atype1 = AtomTypes[i];
                    GP4[n].atype2 = AtomTypes[i];
                    GP4[n].nb1    = b1;
                    GP4[n].nb2    = b2;
                    //GP4[n].num    = n;

                    GP4[n].f1 = offset1;
                    GP4[n].f2 = offset2;

                    ++n;

                    offset2 += nmS[g2.l];
                }

                //b1==b2

                GP4[n].BasisProd(g1, g1);
                GP4[n].atype1 = AtomTypes[i];
                GP4[n].atype2 = AtomTypes[i];
                GP4[n].nb1    = b1;
                GP4[n].nb2    = b1;
                //GP4[n].num    = n;

                GP4[n].f1 = offset1;
                GP4[n].f2 = offset2;

                offset1 += nmS[g1.l];
                ++n;
            }
        }
    }



    for (int i=0; i<AtomTypes.n; ++i) {
        int id1 = AtomTypes[i];
        int nb1 = BasisSet[id1].n;

        int offset1 = 0;
        for (int b1=0; b1<nb1; ++b1) {
            const GTO & g1 = BasisSet[id1][b1];

            GPsame2[ns].BasisProd(g1);
            GPsame2[ns].atype1 = AtomTypes[i];
            GPsame2[ns].atype2 = AtomTypes[i];
            GPsame2[ns].nb1    = b1;
            GPsame2[ns].nb2    = b1;
            //GPsame2[ns].num    = ns;

            GPsame2[ns].f1 = offset1;
            GPsame2[ns].f2 = offset1;

            offset1 += nmS[g1.l];
            ++ns;
        }
    }

}


void GTOMCPECPbasis::Load() {

    // which angular momenta are used?
    pGTOBasis.LSP = false;
    for (int l=0; l<=LMAX; ++l) pGTOBasis.LS[l] = false;

    // if basis set does not exist, look at FIESTA_DIR/basis
    ifstream inp_basis (pGTOBasis.name);
    if (! inp_basis.is_open()) {
        if (const char * path_c = getenv("FIESTA_DIR")) {
            string path_s (path_c);
            if (pGTOBasis.name.substr(0,6) == "basis/") {
                pGTOBasis.name = path_s + "/" + pGTOBasis.name;
            } else {
                pGTOBasis.name = path_s + "/basis/" + pGTOBasis.name;
            }
        }
    } else {
        inp_basis.close();
    }

    // read the file
    FileBuffer BasisFile(pGTOBasis.name);

    std::list<Line>::iterator it = BasisFile.LineArray.begin();
    std::list<Line>::iterator iend = BasisFile.LineArray.end();


    // find the name of the basis set
    // and print it
	{
        if (it == iend) {
            throw Exception("Basis set file format error: file broken");
        }

        //cuidado: el 'if' incrementa la linea
        if (it->str.substr(0,6) != "BASIS=") {
            throw Exception("Basis set file format error: basis set name undefined");
        }

        else {
            pGTOBasis.name = it->str.substr(6);
            pGTOBasis.name = cleanstr(pGTOBasis.name);

            // drop quotation marks if present
            int first = pGTOBasis.name.find_first_not_of("\"");
            int last  = pGTOBasis.name.find_last_not_of("\" ");

            pGTOBasis.name = pGTOBasis.name.substr(first, last-first+1);
        }

        Messenger << "Using basis set: " << pGTOBasis.name << endl << endl;

        ++it;
	}

	// find out  whether the basis
	// is segmented or GC
	{
        if      (it->str.substr(0,7) == "GENERAL")    pGTOBasis.GC = true;
        else if (it->str.substr(0,9) == "SEGMENTED")  pGTOBasis.GC = false;
        else    throw Exception("Basis set file does not specify whether it is segmented or generally contracted");
        ++it;
	}

	// Pople-style basis sets
    bool SP = false;

    // for storing the data
    map <ElementID, list<GTO*> > GTOpool;

    // process the basis set file
    while (it != iend)
    {

        // read the element's name
        string name; int Q; {
            istringstream sline;
            sline.str(it->str);
            sline >> name;
            sline >> Q;
            sline.clear();
            ++it;
        }

        bool hasmcp=false;
        bool hasecp=false;

        // find its ID
        ElementID at = AtomDefs.AtomType(name);

        // skip basis sets for unknown atoms
        if (at == InvalidElementID) {
            Messenger << "Warning: atom '" << name << "' can't be found in the atom definitions file" << endl;

            while (it != iend) {
                if (it->str.substr(0,4) == "****") {
                    ++it;
                    break;
                }
                ++it;
            }
            continue;
        }

        pGTOBasis.AtomBasis[at].Zcore = Q;

        {
           std::size_t found_at=name.find("Mcp");
           if (found_at!=std::string::npos)
           {
              hasmcp = true;
              Messenger << " Expect to find model core potential for " << name <<  std::endl;
           }
           found_at=name.find("Ecp");
           if (found_at!=std::string::npos)
           {
              hasecp = true;
              Messenger << " Expect to find effective core potential for " << name <<  std::endl;
           }
        }

        // read the basis of atom 'at'
        while (it != iend)
        {
            string sh; // shell type
            int nks;   // number of primitives
            int njs;   // number of GC shells

            // figure out the type of shell and
            // its parameters
            {
                istringstream sline;
                sline.str(it->str);
                sline >> sh >> nks >> njs;
                sline.clear();
                ++it;
            }

            // GTOpool[at].is_empty(); //dummy statement to force adding a basis queue, even if the atom contains no basis 2013/04/02
            if (sh.substr(0,4) == "****") break; // end of atom basis info

            if (nks>maxK)
                throw Exception (int2str(nks)+" primitives in contracted function exceeds program limitation of "+ int2str(maxK) );

            if (njs>maxJ)
                throw Exception (int2str(njs)+" general contracted functions exceeds program limitation of "+ int2str(maxJ) );

            // Pople-style shells & extensions
            int nLs = int(sh.size());
            int Ls[nLs];

            for (int i=0; i<nLs; ++i) {
                Ls[i] = S2L(sh[i]);
                if (Ls[i]>LMAX)
                    throw Exception ("Angular momentum "+int2str(Ls[i])+" exceeds program limitation of "+ int2str(LMAX) );
                if (Ls[i]<0)
                    throw Exception ("Angular momentum "+int2str(Ls[i])+" is meaningless" );
            }

            // restirct fused batches only to SP ones
            if (nLs>2)
                throw Exception ("Pople-style function with more than 2 subshells found" );

            else if (nLs==2) {
                if (Ls[0]!=0 || Ls[1]!=1)
                    throw Exception ("Pople-style function is not an SP shell");
                else
                    SP = true;
            }

            // copy data to gto
            {
                GTO * ngto = new GTO;

                // set basic shell parameters
                if (nLs==2) {
                    ngto->SP = true;
                    ngto->l = LSP;
                }
                else {
                    ngto->SP = false;
                    ngto->l = Ls[0];
                }

                ngto->K = nks;
                ngto->J = njs;

                // set the shell flag to true (for later initialization)
                pGTOBasis.LS[ngto->l] = true;


                // read data block
                for (int i=0; i<nks; ++i)
                {

                    if (it==iend)
                        throw Exception ("Basis set file format error: file broken");

                    istringstream sline;
                    sline.str(it->str);


                    //set guassian exponent
                    sline >> ngto->k[i];
                    //cout << "ngto->k[i]" << ngto->k[i] << endl;

                    //set coefficients
                    for (int j=0; j<njs; ++j)
                        sline >> ngto->N[j][i];

                    //set P coefficients for mixed SP shells
                    if (ngto->SP)
                        for (int j=0; j<njs; ++j)
                            sline >> ngto->Np[j][i];

                    sline.clear();
                    ++it;

                    if (ngto->k[i]<=0)
                        throw Exception ("Basis set file error: invalid gaussian exponent "+float2str(ngto->k[i]));
                }

                // split SP shells if code does not
                // support explicit SP batches
                if (ngto->SP && !ExplicitSPsupport)
                {

                    GTO * ngtop = new GTO;

                    ngto->SP  = false;
                    ngto->l   = 0;

                    ngtop->SP = false;
                    ngtop->l  = 1;

                    ngtop->K = ngto->K;
                    ngtop->J = ngto->J;

                    // move primitives to P shell
                    for (int i=0; i<ngtop->K; ++i) {
                        // set guassian exponent
                        ngtop->k[i] = ngto->k[i];

                        // set coefficients
                        for (int j=0; j<ngtop->J; ++j) {
                            ngtop->N[j][i] = ngto->Np[j][i];
                        }
                    }

                    GTOpool[at].push_back(ngto);
                    GTOpool[at].push_back(ngtop);
                }
                //push the pointer to a stack
                else
                    GTOpool[at].push_back(ngto);
            }

        }

        // now read in the according model core potential
        if (hasmcp)
        {
           MCP mcp;
           int test=0;
           istringstream sline;
           sline.str(it->str);
           string flag;
           sline >> flag;
           if (cleanstr(flag)!="MCP")
           {
              throw Exception ("no MCP flag found where expected for atom " + name);
           }
           ++it;
           sline.clear();
           sline.str(it->str);
           if (!(sline >> flag))
           {
              throw Exception ("String stream failed to write " + sline.str());
           }
           if (cleanstr(flag)!="ZEFF")
           {
              throw Exception ("no ZEFF flag found where expected for atom " + name);
           }
           else
           {
              sline >> mcp.Zeff ;
           }
           ++it;
           bool hasexp=false;
           bool hasexpr=false;
           bool hasorbs=false;
           while (it != iend)
           {
               int no=0;
               int dummy=0;
               sline.clear();
               sline.str(it->str);
               sline >> flag;
               flag=cleanstr(flag);
               if (flag.substr(0,4)=="****")
               {
                  break;
               }
               else if (flag=="Exp")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.nExp=no;
                  mcp.ExpExp.resize(mcp.nExp,0.0e0);
                  mcp.CoefExp.resize(mcp.nExp,0.0e0);
                  int i=0;
                  while (i < no && (it != iend ))
                  {
                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     sline >> dummy;
                     sline >> mcp.ExpExp.at(i);
                     sline >> mcp.CoefExp.at(i);
                     ++i;
                  }
                  hasexp=true;
               }
               else if (flag=="ExpR")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.nExpR=no;
                  mcp.ExpExpR.resize(mcp.nExpR,0.0e0);
                  mcp.CoefExpR.resize(mcp.nExpR,0.0e0);
                  int i=0;
                  while (i < no && (it != iend ))
                  {
                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     sline >> dummy;
                     sline >> mcp.ExpExpR.at(i);
                     sline >> mcp.CoefExpR.at(i);
                     ++i;
                  }
                  hasexpr=true;
               }
               else if (flag=="CoreOrbs")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.ProjOrbs.resize(no);
                  int i=0;
                  while (i < no && (it != iend ))
                  {

                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     int noorbs=0;
                     sline >> noorbs;
                     mcp.ProjOrbs.at(i).resize(noorbs);

                     int o=0;
                     while (o < noorbs && (it != iend ))
                     {
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int tmp=0;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).l = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).K = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).J = (uint8_t)tmp;
                        if ((unsigned int)(unsigned char)(mcp.ProjOrbs.at(i).at(o).J) > 1)
                        {
                           throw Exception ("cannot read generally contracted stuff in (YET)");
                        }

                        int k=(unsigned int)(unsigned char)(mcp.ProjOrbs.at(i).at(o).K);
                        int j=0;
                        ++it;
                        //Messenger << " number of parameter " << k << endl;
                        while (j < k && (it != iend ))
                        {
                           sline.clear();
                           sline.str(it->str);
                           int dummy=0;
                           sline >> dummy;
                           sline >>  mcp.ProjOrbs.at(i).at(o).k[j];
                           sline >> mcp.ProjOrbs.at(i).at(o).N[0][j];
                           //Messenger << "parameter " << j << " " << mcp.ProjOrbs.at(i).at(o).k[j] << " " <<  mcp.ProjOrbs.at(i).at(o).N[0][j]  << std::endl;
                           ++it;
                           ++j;
                        }
                        mcp.ProjOrbs.at(i).at(o).SP=false;
                        //Messenger << " ProjOrb before normalization" <<endl;
                        /*
                        for (int y = 0 ; y < (int) mcp.ProjOrbs.at(i).at(o).K; ++y)
                        {
                           Messenger << y << " " << mcp.ProjOrbs.at(i).at(o).k[y] << " " << mcp.ProjOrbs.at(i).at(o).N[0][y] << endl;
                           Messenger  << " test " <<  std::pow((2* mcp.ProjOrbs.at(i).at(o).k[y] )/PI,0.75e0)*sqrt(4*PI) << endl;
                        }
                        */
                        mcp.ProjOrbs.at(i).at(o).Normalize();
                        //Messenger << " ProjOrb after normalization" << endl;
                        //for (int y = 0 ; y < (int) mcp.ProjOrbs.at(i).at(o).K; ++y)
                        //{
                           //Messenger << y << " " << mcp.ProjOrbs.at(i).at(o).k[y] << " " << mcp.ProjOrbs.at(i).at(o).N[0][y] << endl;
                        //}
                        sline.clear();
                        sline.str(it->str);
                        string en;
                        sline >> en;
                        if (en !="en")
                        {
                           throw Exception ("found "+en+" when looking for orbital energies");
                        }
                        sline >> mcp.ProjOrbs.at(i).at(o).energy;
                        ++o;
                     }
                     ++i;
                  }
                  if (it != iend ) hasorbs=true;
               }
               else if (flag=="EcpOrbs")
               {
                  ++it;
                  // for normalization issues
                  GTO tmp_gto;
                  tmp_gto.K = 1;
                  tmp_gto.J = 1;
                  it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.EcpOrbs.resize(no);
                  mcp.EcpMatrices.resize(no);
                  int i=0;
                  while (i < no && (it != iend ))
                  {

                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     int noorbs=0;
                     sline >> noorbs;
                     mcp.EcpOrbs.at(i).resize(noorbs);

                     int o=0;
                     while (o < noorbs && (it != iend ))
                     {
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int tmp=0;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).l = (uint8_t)tmp;
                        tmp_gto.l = mcp.EcpOrbs.at(i).at(o).l;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).K = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).J = (uint8_t)tmp;
                        if ((unsigned int)(unsigned char)(mcp.EcpOrbs.at(i).at(o).J) > 1)
                        {
                           throw Exception ("cannot read generally contracted stuff in (YET)");
                        }

                        int k=(unsigned int)(unsigned char)(mcp.EcpOrbs.at(i).at(o).K);
                        int j=0;
                        ++it;
                        //Messenger << " number of parameter " << k << endl;
                        while (j < k && (it != iend ))
                        {
                           sline.clear();
                           sline.str(it->str);
                           int dummy=0;
                           sline >> dummy;
                           sline >>  mcp.EcpOrbs.at(i).at(o).k[j];
                           //sline >> mcp.EcpOrbs.at(i).at(o).N[0][j];
                           tmp_gto.k[0] =  mcp.EcpOrbs.at(i).at(o).k[j];
                           tmp_gto.N[0][0] = 1.0e0;
                           tmp_gto.Normalize();
                           mcp.EcpOrbs.at(i).at(o).N[0][j] =   tmp_gto.N[0][0];// std::pow((2* mcp.EcpOrbs.at(i).at(o).k[j] )/PI,0.75e0)*sqrt(4*PI);
                           //Messenger << "parameter " << j << " " << mcp.EcpOrbs.at(i).at(o).k[j] << " " <<  mcp.EcpOrbs.at(i).at(o).N[0][j]  << std::endl;
                           //Messenger << "parameter test s " << std::pow((2* mcp.EcpOrbs.at(i).at(o).k[j] )/PI,0.75e0)*sqrt(4*PI)  << std::endl;
                           ++it;
                           ++j;
                        }
                        mcp.EcpOrbs.at(i).at(o).SP=false;
                        //Messenger << " read in first orbital to be normalized now" << std::endl;
                        //std::cout <<  " before " <<mcp.EcpOrbs.at(i).at(o).N[0][0] << std::endl;
                        //mcp.EcpOrbs.at(i).at(o).Normalize();
                        //std::cout <<  " after " <<mcp.EcpOrbs.at(i).at(o).N[0][0] << std::endl;
                        sline.clear();
                        sline.str(it->str);
                        string en;
                        sline >> en;
                        if (en !="en")
                        {
                           throw Exception ("found "+en+" when looking for orbital energies");
                        }
                        sline >> mcp.EcpOrbs.at(i).at(o).energy;
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        string matrix;
                        sline >> matrix;
                        if (matrix !="Matrix")
                        {
                           throw Exception ("found "+matrix+" when looking for ECP matrix");
                        }
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int dim=0;
                        sline >> dim;
                        mcp.EcpMatrices.at(i).setsize(dim,dim);
                        for (int x=0; x< dim;++x)
                        {
                           ++it;
                           sline.clear();
                           sline.str(it->str);
                           for (int y=0; y < dim;++y)
                           {
                                sline >> mcp.EcpMatrices.at(i).operator()(x,y);
                           }
                        }

                        ++o;
                     }
                     ++i;
                  }
                  if (it != iend ) hasorbs=true;
               }
               ++it;
           }
           if (!hasexp || !hasexpr || !hasorbs)
           {
              throw Exception ("Something went wrong when reading the MCP for " + name);
           }
           pMCPBasis.maxterms = std::max({pMCPBasis.maxterms, mcp.nExp,mcp.nExpR});
           pMCPBasis.AtomMCP.emplace(std::make_pair(at,mcp));
           ++it;
       }
       //Messenger << " MCP is read in " << endl;
       if (hasecp)
       {
          ECP ecp;
          istringstream sline;
          sline.str(it->str);
          string flag;
          sline >> flag;
          if (cleanstr(flag)!="ECP")
          {
             throw Exception ("no ECP flag found where expected for atom " + name);
          }
          //Messenger << " Now reading in the ECP " << endl;
          ++it;
          sline.clear();
          sline.str(it->str);
          int no = 0;
          sline >> no;
          ecp.EcpContr.resize(no);
          int i=0;
          ++it;
          while (i < no && (it != iend ))
          {
             //Messenger << " enter while 1" <<  endl;
             sline.clear();
             sline.str(it->str);
             int tmp=0;
             sline >> tmp;
             ecp.EcpContr.at(i).l = (uint8_t)tmp;
             sline >> tmp;
             ecp.EcpContr.at(i).K = (uint8_t)tmp;
             sline >> tmp;
             ecp.EcpContr.at(i).J = (uint8_t)tmp;
             if ((unsigned int)(unsigned char)(ecp.EcpContr.at(i).J) > 1)
             {
                throw Exception ("cannot read generally contracted stuff in Ecps (YET)");
             }

             int k=(unsigned int)(unsigned char)(ecp.EcpContr.at(i).K);
             int j=0;
             ++it;
             while (j < k && (it != iend ))
             {
                //Messenger << " enter while 2" <<  endl;
                //Messenger << j << " " << k <<  endl;
                sline.clear();
                sline.str(it->str);
                int dummy=0;
                if (j == 0)
                   sline >> ecp.EcpContr.at(i).Nexp;
                else
                {
                   sline >> dummy;
                   if (dummy !=ecp.EcpContr.at(i).Nexp)
                   {
                      Messenger << dummy << " " << ecp.EcpContr.at(i).Nexp << endl;
                      throw Exception ("The first numbers for one ECP orb have to be identical");
                   }
                }
                sline >>  ecp.EcpContr.at(i).k[j];
                sline >>  ecp.EcpContr.at(i).N[0][j];
                ++j;
                ++it;
                //Messenger << " incremented j " <<  j <<  endl;
             }
             ecp.EcpContr.at(i).SP=false;
             ++i;
          }
          ++it;
          pECPBasis.AtomECP.emplace(std::make_pair(at,ecp));
       }
       //Messenger << " ECP should have been read in " << endl;
    }


    if (SP && !ExplicitSPsupport) {
       Messenger << "Basis set contains Pople-style SP shells" << endl;
       Messenger << "S and P shells have been split" << endl;
       Messenger << endl;
    }

     //todo lo siguiente es necesario debido a dos factores:
     //1) los atomos pueden no estar en orden, ya sea en el archivo de definiciones atomicas o en las de la base
     //2) pueden haber atomos repetidos; en este caso se deben juntar las definiciones de base

    //copia los GTOs a un array estatico
    map <ElementID, list<GTO*> >::iterator git = GTOpool.begin();

    while (git != GTOpool.end()) {

    	ElementID    Id       = git->first;
    	list<GTO*> & oGTOlist = git->second;
    	int nGTO = oGTOlist.size();

    	// copy everything to a contiguous array
    	list<GTO*>::iterator lit = oGTOlist.begin();


    	pGTOBasis.AtomBasis[Id].shell.set(nGTO);

    	for (int i=0; i<nGTO; ++i) {
            pGTOBasis.AtomBasis[Id][i] = *(*lit);
    	   delete (*lit);
    	   ++lit;
    	}
    	++git;
    }

    GTOpool.clear();


    // normalize the basis
    map <ElementID, ElementBasis >::iterator ait = pGTOBasis.AtomBasis.begin();

    while (ait != pGTOBasis.AtomBasis.end()) {
   	    for (int i=0; i<ait->second.Nshells(); ++i)
   	        ait->second[i].Normalize();
   	    ++ait;
    }

}

/*
    The idea is to standardize the input
    The current format for the routine is;

GENERAL
Au  78
S   2   1.00
     30.1965370              0.0047330
      9.7259730             -0.3543820
S   1   1.00
      5.0804060              1.0000000
S   1   1.00
      1.7226570              1.0000000
S   1   1.00
      0.7264590              1.0000000
S   1   1.00
      0.0903540              1.0000000
S   1   1.00
      0.0221060              1.0000000
S   1   1.00
      0.0064150              1.0000000
P   4   1.00
     13.8382190              0.0361790
      5.1957870             -0.3283030
      1.7980450              0.6653880
      0.6661050              0.5526660
P   1   1.00
      0.1543360              1.0000000
P   1   1.00
      0.0340000              1.0000000
D   2   1.00
      6.3370010             -0.0441030
      1.4806970              0.4621150
D   1   1.00
      0.5283820              1.0000000
D   1   1.00
      0.1711170              1.0000000
D   1   1.00
      0.0455120              1.0000000

ECP 4 // n ECPs
S   2   1.00 2
     13.205100000          426.709840000
      6.602550000           35.938824000
P   2   1.00 2
     10.452020000          261.161023000
      5.226010000           26.626284000
D   2   1.00 2
      7.851100000          124.756831000
      3.925550000           15.772260000
F   2   1.00 2
      4.789800000           30.568475000
      2.394910000            5.183774000

MCP 1 // Zeff

Exp
  0

ExpR
  1
0    0.501661     -8.67716

CoreOrbs
  3

  1
0  8  1
   1      30.1965370         0.004720
   2      9.7259730         -0.354845
   3      5.0804060          1.098592
   4      1.7226570         -0.901451
   5      0.7264590         -0.617419
   6      0.0903540         -0.017618
   7      0.0221060          0.007503
   8      0.0064150         -0.002623
en -4.7021

  1
1  6  1
   1      13.8382190              0.036059
   2      5.1957870              -0.327602
   3      1.7980450               0.663884
   4      0.6661050               0.552152
   5      0.1543360               0.029207
   6      0.0340000              -0.001617
en -2.7261
i
  1
2  5  1
   1      6.3370010             -0.043603
   2      1.4806970              0.456429
   3      0.5283820              0.516836
   4      0.1711170              0.235923
   5      0.0455120              0.013842
en -0.4626

EcpOrbs
  0
****
*/

/*
void ElementalCenter::Load(const std::string & file) {


    // read the file
    FileBuffer BasisFile(file);


    auto it   = BasisFile.LineArray.begin();
    auto iend = BasisFile.LineArray.end();

    bool GC=false, SP = false;

    // sanity check
	{
        if (it == iend) {
            throw Exception("Basis set file format error: file broken");
        }

        //cuidado: el 'if' incrementa la linea
        if (it->str.substr(0,6) != "BASIS=") {
            throw Exception("Basis set file format error: basis set name undefined");
        }

        ++it;
	}

	// find out  whether the basis
	// is segmented or GC
	{
        if      (it->str.substr(0,7) == "GENERAL")    GC = true;
        else if (it->str.substr(0,9) == "SEGMENTED")  GC = false;
        else    throw Exception("Basis set file does not specify whether it is segmented or generally contracted");
        ++it;
	}


    // for storing the data
    list<GTO*> GTOlist;

    // process the basis set file
    while (it != iend) {

        // read the element's name & its core charge, if any
        string name; int Q; {
            istringstream sline;
            sline.str(it->str);
            sline >> name;
            sline >> Q;
            ++it;
        }

        // skip unless it's the element we are looking for
        ElementID at = AtomDefs.AtomType(name);

        if (at != eID) {
            // FIXME : skip ECPs and MCPs too
            while (it != iend) {
                if (it->str.substr(0,4) == "****") {
                    ++it;
                    break;
                }
                ++it;
            }
            continue;
        }

        eBasis.Zcore = Q;

        // read the basis of atom 'at'
        // ===========================
        while (it != iend) {

            // read the head without advancing the line
            string head;
            {
                istringstream sline;
                sline.str(it->str);
                sline >> head;
            }


            if (head.substr(0,4) == "****") {++it; break;}


            // read ECP
            // ========
            else if (head.substr(0,3) == "ECP") {

                int nshells; {
                    istringstream sline;
                    sline.str(it->str);
                    string ecp; // "ECP"
                    sline >> ecp >> nshells;
                    ++it;
                }

                eECP.EcpContr.resize(nshells);

                for (int i=0; i<nshells; ++i) {

                    int ll;    // angular momentum
                    int nks;   // number of primitives
                    int njs;   // number of GC shells
                    int nn;    // power of r^nn

                    // figure out the type of shell and
                    // its parameters
                    {
                        istringstream sline;
                        sline.str(it->str);
                        string sh; // shell type
                        sline >> sh >> nks >> njs >> nn;
                        ll = S2L(sh[0]);

                        // sanity check

                        if (ll>LMAX)  throw Exception ("Angular momentum "+int2str(ll)+" exceeds program limitation of "+ int2str(LMAX) );

                        if (nks>maxK) throw Exception (int2str(nks)+" primitives in ECP function exceeds program limitation of "+ int2str(maxK) );
                        // ignore this value for the moment
                        //if (njs>1)    throw Exception ("general contracted functions not implemented within ECPs");

                        if (nn<0)     throw Exception ("bad r exponent in ECP");
                        // check maximum, too

                        ++it;
                    }


                    GTOforECP & ecpgto = eECP.EcpContr[i];

                    ecpgto.l    = ll;
                    ecpgto.K    = nks;
                    ecpgto.J    = 1;
                    ecpgto.Nexp = nn;
                    ecpgto.SP   = false;

                    // virtually identical to the GTO, for J==1
                    for (int k=0; k<nks; ++k) {

                        if (it==iend) throw Exception ("Basis set file format error: file broken");

                        istringstream sline;
                        sline.str(it->str);

                        sline >>  ecpgto.k[k] >> ecpgto.N[0][k];
                        ++it;
                    }
                }
            }


            // read MCP
            // ========
            else if (head.substr(0,3) == "MCP") {

                int zeff; {
                    istringstream sline;
                    sline.str(it->str);
                    string mcp; // "ECP"
                    sline >> mcp >> zeff;
                    ++it;
                }

                eMCP.Zeff = zeff;

               bool hasexp=false;
               bool hasexpr=false;
               bool hasorbs=false;

                while (true) {

                    string key; int K; {
                        istringstream sline;
                        sline.str(it->str);
                        sline >> key >> K;
                        ++it;
                    }


                    if (key=="Exp") {
                        eMCP.nExp = K;
                        eMCP.ExpExp.resize(eMCP.nExp,0.0e0);
                        eMCP.CoefExp.resize(mcp.nExp,0.0e0);

                        for (int k=0: k<K; ++k) {
                            istringstream sline;
                            sline.str(it->str);
                            sline >> eMCP.ExpExp[k] >> eMCP,CoefExp[k];
                            ++it;
                        }
                        hasexp=true;
                    }

                    if (key=="CoreOrbs") {

                    }

                    if (key=="EcpOrbs") {
                    }



               }


            }


            // read GTO
            // ========
            else {
                string sh; // shell type
                int nks;   // number of primitives
                int njs;   // number of GC shells

                // figure out the type of shell and
                // its parameters
                {
                    istringstream sline;
                    sline.str(it->str);
                    sline >> sh >> nks >> njs;
                    sline.clear();
                    ++it;
                }

                if (nks>maxK) throw Exception (int2str(nks)+" primitives in contracted function exceeds program limitation of "+ int2str(maxK) );
                if (njs>maxJ) throw Exception (int2str(njs)+" general contracted functions exceeds program limitation of "+ int2str(maxJ) );

                int LL; bool SP; {
                    // Pople-style shells & extensions
                    int nLs = int(sh.size());
                    int Ls[nLs];

                    for (int i=0; i<nLs; ++i) {
                        Ls[i] = S2L(sh[i]);
                        if (Ls[i]>LMAX) throw Exception ("Angular momentum "+int2str(Ls[i])+" exceeds program limitation of "+ int2str(LMAX) );
                        if (Ls[i]<0)    throw Exception ("Angular momentum "+int2str(Ls[i])+" is meaningless" );
                    }

                    // restirct fused batches only to SP ones
                    if       (nLs>2)                            throw Exception ("Pople-style function with more than 2 subshells found" );
                    else if ((nLs==2)&&(Ls[0]!=0 || Ls[1]!=1) ) throw Exception ("Pople-style function is not an SP shell");
                    else if ((nLs==2)&&(njs>1))                 throw Exception ("Pople-style function incompatible with general contraction");
                    else if  (nLs==2)                           SP = true;
                    else                                       {SP = false; LL=Ls[0];}
                }

                // read GTO data
                // =============

                GTO * ngto = new GTO;

                // read normal shell
                {
                    // set basic shell parameters
                    if (SP) {
                        ngto->SP = true;
                        ngto->l = LSP; // LMAX+1
                    }
                    else {
                        ngto->SP = false;
                        ngto->l = LL; // whatever has been read before
                    }

                    ngto->K = nks;
                    ngto->J = njs;

                    // read the Gaussian exponents and their coefficients
                    for (int k=0; k<nks; ++k) {

                        if (it==iend) throw Exception ("Basis set file format error: file broken");

                        istringstream sline;
                        sline.str(it->str);


                        sline >> ngto->k[k]; // gaussian exponent
                        for (int j=0; j<njs; ++j) sline >> ngto->N[j][k]; // generally contracted coefficients
                        if (ngto->SP) for (int j=0; j<njs; ++j) sline >> ngto->Np[j][k]; //set generally contracted P coefficients for mixed SP shells

                        sline.clear();
                        ++it;

                        if (ngto->k[k]<=0) throw Exception ("Basis set file error: invalid gaussian exponent "+float2str(ngto->k[k]));
                    }

                    GTOlist.push_back(ngto); // pushes the pointer
                }

                // split SP shells if code does not
                // support explicit SP batches
                if (ngto->SP && !ExplicitSPsupport) {


                    // modifies the contents of the freshly pushed pointer
                    ngto->SP  = false;
                    ngto->l   = 0;

                    // create new GTO with the P component
                    GTO * ngtop = new GTO;

                    ngtop->SP = false;
                    ngtop->l  = 1;

                    ngtop->K = ngto->K;
                    ngtop->J = ngto->J;

                    // move primitives to P shell
                    for (int k=0; k<ngtop->K; ++k) {
                        // set guassian exponent
                        ngtop->k[k] = ngto->k[k];
                        for (int j=0; j<ngtop->J; ++j) ngtop->N[j][k] = ngto->Np[j][k]; // J == 1
                    }

                    GTOlist.push_back(ngtop); // pushes the pointer
                }

            }
        }


        // now read in the according model core potential
        if (nMCPs>0) {

           MCP mcp;

           // ZEFF should be deduced from Q

           //int test=0;
           //istringstream sline;
           //sline.str(it->str);
           //if (!(sline >> flag)) throw Exception ("String stream failed to write " + sline.str());
           //if (cleanstr(flag)!="ZEFF") throw Exception ("no ZEFF flag found where expected for atom " + name);
           //else sline >> mcp.Zeff;
           //++it;

           bool hasexp=false;
           bool hasexpr=false;
           bool hasorbs=false;

           while (it != iend)
           {
               int no=0;
               int dummy=0;
               sline.clear();
               sline.str(it->str);
               sline >> flag;
               flag=cleanstr(flag);
               if (flag.substr(0,4)=="****")
               {
                  break;
               }
               else if (flag=="Exp")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.nExp=no;
                  mcp.ExpExp.resize(mcp.nExp,0.0e0);
                  mcp.CoefExp.resize(mcp.nExp,0.0e0);
                  int i=0;
                  while (i < no && (it != iend ))
                  {
                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     sline >> dummy;
                     sline >> mcp.ExpExp.at(i);
                     sline >> mcp.CoefExp.at(i);
                     ++i;
                  }
                  hasexp=true;
               }
               else if (flag=="ExpR")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.nExpR=no;
                  mcp.ExpExpR.resize(mcp.nExpR,0.0e0);
                  mcp.CoefExpR.resize(mcp.nExpR,0.0e0);
                  int i=0;
                  while (i < no && (it != iend ))
                  {
                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     sline >> dummy;
                     sline >> mcp.ExpExpR.at(i);
                     sline >> mcp.CoefExpR.at(i);
                     ++i;
                  }
                  hasexpr=true;
               }
               else if (flag=="CoreOrbs")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.ProjOrbs.resize(no);
                  int i=0;
                  while (i < no && (it != iend ))
                  {

                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     int noorbs=0;
                     sline >> noorbs;
                     mcp.ProjOrbs.at(i).resize(noorbs);

                     int o=0;
                     while (o < noorbs && (it != iend ))
                     {
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int tmp=0;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).l = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).K = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).J = (uint8_t)tmp;
                        if ((unsigned int)(unsigned char)(mcp.ProjOrbs.at(i).at(o).J) > 1)
                        {
                           throw Exception ("cannot read generally contracted stuff in (YET)");
                        }

                        int k=(unsigned int)(unsigned char)(mcp.ProjOrbs.at(i).at(o).K);
                        int j=0;
                        ++it;
                        //Messenger << " number of parameter " << k << endl;
                        while (j < k && (it != iend ))
                        {
                           sline.clear();
                           sline.str(it->str);
                           int dummy=0;
                           sline >> dummy;
                           sline >>  mcp.ProjOrbs.at(i).at(o).k[j];
                           sline >> mcp.ProjOrbs.at(i).at(o).N[0][j];
                           ++it;
                           ++j;
                        }
                        mcp.ProjOrbs.at(i).at(o).SP=false;
                        mcp.ProjOrbs.at(i).at(o).Normalize();

                        sline.clear();
                        sline.str(it->str);
                        string en;
                        sline >> en;
                        if (en !="en")
                        {
                           throw Exception ("found "+en+" when looking for orbital energies");
                        }
                        sline >> mcp.ProjOrbs.at(i).at(o).energy;
                        ++o;
                     }
                     ++i;
                  }
                  if (it != iend ) hasorbs=true;
               }
               else if (flag=="EcpOrbs")
               {
                  ++it;
                  // for normalization issues
                  GTO tmp_gto;
                  tmp_gto.K = 1;
                  tmp_gto.J = 1;
                  it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.EcpOrbs.resize(no);
                  mcp.EcpMatrices.resize(no);
                  int i=0;
                  while (i < no && (it != iend ))
                  {

                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     int noorbs=0;
                     sline >> noorbs;
                     mcp.EcpOrbs.at(i).resize(noorbs);

                     int o=0;
                     while (o < noorbs && (it != iend ))
                     {
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int tmp=0;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).l = (uint8_t)tmp;
                        tmp_gto.l = mcp.EcpOrbs.at(i).at(o).l;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).K = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).J = (uint8_t)tmp;
                        if ((unsigned int)(unsigned char)(mcp.EcpOrbs.at(i).at(o).J) > 1)
                        {
                           throw Exception ("cannot read generally contracted stuff in (YET)");
                        }

                        int k=(unsigned int)(unsigned char)(mcp.EcpOrbs.at(i).at(o).K);
                        int j=0;
                        ++it;
                        //Messenger << " number of parameter " << k << endl;
                        while (j < k && (it != iend ))
                        {
                           sline.clear();
                           sline.str(it->str);
                           int dummy=0;
                           sline >> dummy;
                           sline >>  mcp.EcpOrbs.at(i).at(o).k[j];
                           //sline >> mcp.EcpOrbs.at(i).at(o).N[0][j];
                           tmp_gto.k[0] =  mcp.EcpOrbs.at(i).at(o).k[j];
                           tmp_gto.N[0][0] = 1.0e0;
                           tmp_gto.Normalize();
                           mcp.EcpOrbs.at(i).at(o).N[0][j] =   tmp_gto.N[0][0];// std::pow((2* mcp.EcpOrbs.at(i).at(o).k[j] )/PI,0.75e0)*sqrt(4*PI);
                           //Messenger << "parameter " << j << " " << mcp.EcpOrbs.at(i).at(o).k[j] << " " <<  mcp.EcpOrbs.at(i).at(o).N[0][j]  << std::endl;
                           //Messenger << "parameter test s " << std::pow((2* mcp.EcpOrbs.at(i).at(o).k[j] )/PI,0.75e0)*sqrt(4*PI)  << std::endl;
                           ++it;
                           ++j;
                        }
                        mcp.EcpOrbs.at(i).at(o).SP=false;
                        //Messenger << " read in first orbital to be normalized now" << std::endl;
                        //std::cout <<  " before " <<mcp.EcpOrbs.at(i).at(o).N[0][0] << std::endl;
                        //mcp.EcpOrbs.at(i).at(o).Normalize();
                        //std::cout <<  " after " <<mcp.EcpOrbs.at(i).at(o).N[0][0] << std::endl;
                        sline.clear();
                        sline.str(it->str);
                        string en;
                        sline >> en;
                        if (en !="en")
                        {
                           throw Exception ("found "+en+" when looking for orbital energies");
                        }
                        sline >> mcp.EcpOrbs.at(i).at(o).energy;
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        string matrix;
                        sline >> matrix;
                        if (matrix !="Matrix")
                        {
                           throw Exception ("found "+matrix+" when looking for ECP matrix");
                        }
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int dim=0;
                        sline >> dim;
                        mcp.EcpMatrices.at(i).setsize(dim,dim);
                        for (int x=0; x< dim;++x)
                        {
                           ++it;
                           sline.clear();
                           sline.str(it->str);
                           for (int y=0; y < dim;++y)
                           {
                                sline >> mcp.EcpMatrices.at(i).operator()(x,y);
                           }
                        }

                        ++o;
                     }
                     ++i;
                  }
                  if (it != iend ) hasorbs=true;
               }
               ++it;
           }
           if (!hasexp || !hasexpr || !hasorbs)
           {
              throw Exception ("Something went wrong when reading the MCP for " + name);
           }
           pMCPBasis.maxterms = std::max({pMCPBasis.maxterms, mcp.nExp,mcp.nExpR});
           pMCPBasis.AtomMCP.emplace(std::make_pair(at,mcp));
           ++it;
       }


    }


    if (SP && !ExplicitSPsupport) {
       Messenger << "Basis set contains Pople-style SP shells, not natively supported by the present ERI module; S and P shells have been split" << endl;
    }

     //todo lo siguiente es necesario debido a dos factores:
     //1) los atomos pueden no estar en orden, ya sea en el archivo de definiciones atomicas o en las de la base
     //2) pueden haber atomos repetidos; en este caso se deben juntar las definiciones de base


    int nGTO = GTOlist.size();
    // copy everything to a contiguous array
    list<GTO*>::iterator lit = GTOlist.begin();

    eBasis.shell.set(nGTO);

    for (int i=0; i<nGTO; ++i) {
       eBasis.shell[i] = *(*lit);
       delete (*lit);
       ++lit;
    }

    GTOlist.clear();

    // normalize the basis
    for (int i=0; i<nGTO; ++i)
        eBasis.shell[i].Normalize();

}
*/

// loads individual elements
// this skips everything that is repeated on each call
void GTOMCPECPbasis::Load (ElementID sID, const std::string & file, ElementID eID) {

    // read the file
    FileBuffer BasisFile(file);

    std::list<Line>::iterator it = BasisFile.LineArray.begin();
    std::list<Line>::iterator iend = BasisFile.LineArray.end();


    // find the name of the basis set
    // and print it
	{
	    string name;
        if (it == iend) {
            throw Exception("Basis set file format error: file broken");
        }

        //cuidado: el 'if' incrementa la linea
        if (it->str.substr(0,6) != "BASIS=") {
            throw Exception("Basis set file format error: basis set name undefined");
        }

        else {
            /*
            pGTOBasis.name = it->str.substr(6);
            pGTOBasis.name = cleanstr(pGTOBasis.name);

            // drop quotation marks if present
            int first = pGTOBasis.name.find_first_not_of("\"");
            int last  = pGTOBasis.name.find_last_not_of("\" ");

            pGTOBasis.name = pGTOBasis.name.substr(first, last-first+1);
            */

            name = it->str.substr(6);
            name = cleanstr(name);

            // drop quotation marks if present
            int first = name.find_first_not_of("\"");
            int last  = name.find_last_not_of("\" ");

            name = name.substr(first, last-first+1);

        }

        Messenger << "Using basis set: " << name << endl << endl;

        ++it;
	}

	// find out  whether the basis
	// is segmented or GC
	{
        if      (it->str.substr(0,7) == "GENERAL")    {} //pGTOBasis.GC = true;
        else if (it->str.substr(0,9) == "SEGMENTED")  {} //pGTOBasis.GC = false;
        else    throw Exception("Basis set file does not specify whether it is segmented or generally contracted");
        ++it;
	}

	// Pople-style basis sets
    bool SP = false;

    // for storing the data
    map <ElementID, list<GTO*> > GTOpool;

    // process the basis set file
    while (it != iend) {

        // read the element's name
        string name; int Q; {
            istringstream sline;
            sline.str(it->str);
            sline >> name;
            sline >> Q;
            sline.clear();
            ++it;
        }

        // skip all basis sets except for the atom we are looking for
        if (AtomDefs.AtomType(name) != eID) {
            //Messenger << "Warning: atom '" << name << "' can't be found in the atom definitions file" << endl;

            // skips the element definition
            // and any MCP or ECP blocks afterwards
            bool next=false;
            do {
                while (it != iend) {
                    if (it->str.substr(0,4) == "****") {
                        ++it;
                        break;
                    }
                    ++it;
                }

                if (it==iend) break;

                {
                    string flag;
                    istringstream sline;
                    sline.str(it->str);
                    sline >> flag;
                    next = (flag=="MCP") || (flag=="ECP");
                }
            } while (next);


            continue;
        }



        pGTOBasis.AtomBasis[sID].Zcore = Q;

        /*
        {
           std::size_t found_at=name.find("Mcp");
           if (found_at!=std::string::npos)
           {
              hasmcp = true;
              Messenger << " Expect to find model core potential for " << name <<  std::endl;
           }
           found_at=name.find("Ecp");
           if (found_at!=std::string::npos)
           {
              hasecp = true;
              Messenger << " Expect to find effective core potential for " << name <<  std::endl;
           }
        }
        */

        // read the basis of atom 'at'
        while (it != iend)
        {
            string sh; // shell type
            int nks;   // number of primitives
            int njs;   // number of GC shells

            // figure out the type of shell and
            // its parameters
            {
                istringstream sline;
                sline.str(it->str);
                sline >> sh >> nks >> njs;
                sline.clear();
                ++it;
            }

            // GTOpool[at].is_empty(); //dummy statement to force adding a basis queue, even if the atom contains no basis 2013/04/02
            if (sh.substr(0,4) == "****") break; // end of atom basis info

            if (nks>maxK)
                throw Exception (int2str(nks)+" primitives in contracted function exceeds program limitation of "+ int2str(maxK) );

            if (njs>maxJ)
                throw Exception (int2str(njs)+" general contracted functions exceeds program limitation of "+ int2str(maxJ) );

            // Pople-style shells & extensions
            int nLs = int(sh.size());
            int Ls[nLs];

            for (int i=0; i<nLs; ++i) {
                Ls[i] = S2L(sh[i]);
                if (Ls[i]>LMAX)
                    throw Exception ("Angular momentum "+int2str(Ls[i])+" exceeds program limitation of "+ int2str(LMAX) );
                if (Ls[i]<0)
                    throw Exception ("Angular momentum "+int2str(Ls[i])+" is meaningless" );
            }

            // restirct fused batches only to SP ones
            if (nLs>2)
                throw Exception ("Pople-style function with more than 2 subshells found" );

            else if (nLs==2) {
                if (Ls[0]!=0 || Ls[1]!=1)
                    throw Exception ("Pople-style function is not an SP shell");
                else
                    SP = true;
            }

            // copy data to gto
            {
                GTO * ngto = new GTO;

                // set basic shell parameters
                if (nLs==2) {
                    ngto->SP = true;
                    ngto->l = LSP;
                }
                else {
                    ngto->SP = false;
                    ngto->l = Ls[0];
                }

                ngto->K = nks;
                ngto->J = njs;

                // set the shell flag to true (for later initialization)
                pGTOBasis.LS[ngto->l] = true;


                // read data block
                for (int i=0; i<nks; ++i)
                {

                    if (it==iend)
                        throw Exception ("Basis set file format error: file broken");

                    istringstream sline;
                    sline.str(it->str);


                    //set guassian exponent
                    sline >> ngto->k[i];
                    //cout << "ngto->k[i]" << ngto->k[i] << endl;

                    //set coefficients
                    for (int j=0; j<njs; ++j)
                        sline >> ngto->N[j][i];

                    //set P coefficients for mixed SP shells
                    if (ngto->SP)
                        for (int j=0; j<njs; ++j)
                            sline >> ngto->Np[j][i];

                    sline.clear();
                    ++it;

                    if (ngto->k[i]<=0)
                        throw Exception ("Basis set file error: invalid gaussian exponent "+float2str(ngto->k[i]));
                }

                // split SP shells if code does not
                // support explicit SP batches
                if (ngto->SP && !ExplicitSPsupport)
                {

                    GTO * ngtop = new GTO;

                    ngto->SP  = false;
                    ngto->l   = 0;

                    ngtop->SP = false;
                    ngtop->l  = 1;

                    ngtop->K = ngto->K;
                    ngtop->J = ngto->J;

                    // move primitives to P shell
                    for (int i=0; i<ngtop->K; ++i) {
                        // set guassian exponent
                        ngtop->k[i] = ngto->k[i];

                        // set coefficients
                        for (int j=0; j<ngtop->J; ++j) {
                            ngtop->N[j][i] = ngto->Np[j][i];
                        }
                    }

                    GTOpool[sID].push_back(ngto);
                    GTOpool[sID].push_back(ngtop);
                }
                //push the pointer to a stack
                else
                    GTOpool[sID].push_back(ngto);
            }

        }

        bool hasmcp; {
            string key;
            istringstream sline;
            sline.str(it->str);
            sline >> key;
            hasmcp = (key.substr(0,3) == "MCP");
        }

        // now read in the according model core potential
        if (hasmcp)
        {
           MCP mcp;
           int test=0;
           istringstream sline;
           sline.str(it->str);
           string flag;
           sline >> flag;
           if (cleanstr(flag)!="MCP")
           {
              throw Exception ("no MCP flag found where expected for atom " + name);
           }
           ++it;
           sline.clear();
           sline.str(it->str);
           if (!(sline >> flag))
           {
              throw Exception ("String stream failed to write " + sline.str());
           }
           if (cleanstr(flag)!="ZEFF")
           {
              throw Exception ("no ZEFF flag found where expected for atom " + name);
           }
           else
           {
              sline >> mcp.Zeff ;
           }
           ++it;
           bool hasexp=false;
           bool hasexpr=false;
           bool hasorbs=false;
           while (it != iend)
           {
               int no=0;
               int dummy=0;
               sline.clear();
               sline.str(it->str);
               sline >> flag;
               flag=cleanstr(flag);
               if (flag.substr(0,4)=="****")
               {
                  break;
               }
               else if (flag=="Exp")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.nExp=no;
                  mcp.ExpExp.resize(mcp.nExp,0.0e0);
                  mcp.CoefExp.resize(mcp.nExp,0.0e0);
                  int i=0;
                  while (i < no && (it != iend ))
                  {
                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     sline >> dummy;
                     sline >> mcp.ExpExp.at(i);
                     sline >> mcp.CoefExp.at(i);
                     ++i;
                  }
                  hasexp=true;
               }
               else if (flag=="ExpR")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.nExpR=no;
                  mcp.ExpExpR.resize(mcp.nExpR,0.0e0);
                  mcp.CoefExpR.resize(mcp.nExpR,0.0e0);
                  int i=0;
                  while (i < no && (it != iend ))
                  {
                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     sline >> dummy;
                     sline >> mcp.ExpExpR.at(i);
                     sline >> mcp.CoefExpR.at(i);
                     ++i;
                  }
                  hasexpr=true;
               }
               else if (flag=="CoreOrbs")
               {
                  ++it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.ProjOrbs.resize(no);
                  int i=0;
                  while (i < no && (it != iend ))
                  {

                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     int noorbs=0;
                     sline >> noorbs;
                     mcp.ProjOrbs.at(i).resize(noorbs);

                     int o=0;
                     while (o < noorbs && (it != iend ))
                     {
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int tmp=0;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).l = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).K = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.ProjOrbs.at(i).at(o).J = (uint8_t)tmp;
                        if ((unsigned int)(unsigned char)(mcp.ProjOrbs.at(i).at(o).J) > 1)
                        {
                           throw Exception ("cannot read generally contracted stuff in (YET)");
                        }

                        int k=(unsigned int)(unsigned char)(mcp.ProjOrbs.at(i).at(o).K);
                        int j=0;
                        ++it;
                        //Messenger << " number of parameter " << k << endl;
                        while (j < k && (it != iend ))
                        {
                           sline.clear();
                           sline.str(it->str);
                           int dummy=0;
                           sline >> dummy;
                           sline >>  mcp.ProjOrbs.at(i).at(o).k[j];
                           sline >> mcp.ProjOrbs.at(i).at(o).N[0][j];
                           //Messenger << "parameter " << j << " " << mcp.ProjOrbs.at(i).at(o).k[j] << " " <<  mcp.ProjOrbs.at(i).at(o).N[0][j]  << std::endl;
                           ++it;
                           ++j;
                        }
                        mcp.ProjOrbs.at(i).at(o).SP=false;
                        //Messenger << " ProjOrb before normalization" <<endl;
                        /*
                        for (int y = 0 ; y < (int) mcp.ProjOrbs.at(i).at(o).K; ++y)
                        {
                           Messenger << y << " " << mcp.ProjOrbs.at(i).at(o).k[y] << " " << mcp.ProjOrbs.at(i).at(o).N[0][y] << endl;
                           Messenger  << " test " <<  std::pow((2* mcp.ProjOrbs.at(i).at(o).k[y] )/PI,0.75e0)*sqrt(4*PI) << endl;
                        }
                        */
                        mcp.ProjOrbs.at(i).at(o).Normalize();
                        //Messenger << " ProjOrb after normalization" << endl;
                        //for (int y = 0 ; y < (int) mcp.ProjOrbs.at(i).at(o).K; ++y)
                        //{
                           //Messenger << y << " " << mcp.ProjOrbs.at(i).at(o).k[y] << " " << mcp.ProjOrbs.at(i).at(o).N[0][y] << endl;
                        //}
                        sline.clear();
                        sline.str(it->str);
                        string en;
                        sline >> en;
                        if (en !="en")
                        {
                           throw Exception ("found "+en+" when looking for orbital energies");
                        }
                        sline >> mcp.ProjOrbs.at(i).at(o).energy;
                        ++o;
                     }
                     ++i;
                  }
                  if (it != iend ) hasorbs=true;
               }
               else if (flag=="EcpOrbs")
               {
                  ++it;
                  // for normalization issues
                  GTO tmp_gto;
                  tmp_gto.K = 1;
                  tmp_gto.J = 1;
                  it;
                  sline.clear();
                  sline.str(it->str);
                  sline >> no;
                  mcp.EcpOrbs.resize(no);
                  mcp.EcpMatrices.resize(no);
                  int i=0;
                  while (i < no && (it != iend ))
                  {

                     ++it;
                     sline.clear();
                     sline.str(it->str);
                     int noorbs=0;
                     sline >> noorbs;
                     mcp.EcpOrbs.at(i).resize(noorbs);

                     int o=0;
                     while (o < noorbs && (it != iend ))
                     {
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int tmp=0;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).l = (uint8_t)tmp;
                        tmp_gto.l = mcp.EcpOrbs.at(i).at(o).l;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).K = (uint8_t)tmp;
                        sline >> tmp;
                        mcp.EcpOrbs.at(i).at(o).J = (uint8_t)tmp;
                        if ((unsigned int)(unsigned char)(mcp.EcpOrbs.at(i).at(o).J) > 1)
                        {
                           throw Exception ("cannot read generally contracted stuff in (YET)");
                        }

                        int k=(unsigned int)(unsigned char)(mcp.EcpOrbs.at(i).at(o).K);
                        int j=0;
                        ++it;
                        //Messenger << " number of parameter " << k << endl;
                        while (j < k && (it != iend ))
                        {
                           sline.clear();
                           sline.str(it->str);
                           int dummy=0;
                           sline >> dummy;
                           sline >>  mcp.EcpOrbs.at(i).at(o).k[j];
                           //sline >> mcp.EcpOrbs.at(i).at(o).N[0][j];
                           tmp_gto.k[0] =  mcp.EcpOrbs.at(i).at(o).k[j];
                           tmp_gto.N[0][0] = 1.0e0;
                           tmp_gto.Normalize();
                           mcp.EcpOrbs.at(i).at(o).N[0][j] =   tmp_gto.N[0][0];// std::pow((2* mcp.EcpOrbs.at(i).at(o).k[j] )/PI,0.75e0)*sqrt(4*PI);
                           //Messenger << "parameter " << j << " " << mcp.EcpOrbs.at(i).at(o).k[j] << " " <<  mcp.EcpOrbs.at(i).at(o).N[0][j]  << std::endl;
                           //Messenger << "parameter test s " << std::pow((2* mcp.EcpOrbs.at(i).at(o).k[j] )/PI,0.75e0)*sqrt(4*PI)  << std::endl;
                           ++it;
                           ++j;
                        }
                        mcp.EcpOrbs.at(i).at(o).SP=false;
                        //Messenger << " read in first orbital to be normalized now" << std::endl;
                        //std::cout <<  " before " <<mcp.EcpOrbs.at(i).at(o).N[0][0] << std::endl;
                        //mcp.EcpOrbs.at(i).at(o).Normalize();
                        //std::cout <<  " after " <<mcp.EcpOrbs.at(i).at(o).N[0][0] << std::endl;
                        sline.clear();
                        sline.str(it->str);
                        string en;
                        sline >> en;
                        if (en !="en")
                        {
                           throw Exception ("found "+en+" when looking for orbital energies");
                        }
                        sline >> mcp.EcpOrbs.at(i).at(o).energy;
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        string matrix;
                        sline >> matrix;
                        if (matrix !="Matrix")
                        {
                           throw Exception ("found "+matrix+" when looking for ECP matrix");
                        }
                        ++it;
                        sline.clear();
                        sline.str(it->str);
                        int dim=0;
                        sline >> dim;
                        mcp.EcpMatrices.at(i).setsize(dim,dim);
                        for (int x=0; x< dim;++x)
                        {
                           ++it;
                           sline.clear();
                           sline.str(it->str);
                           for (int y=0; y < dim;++y)
                           {
                                sline >> mcp.EcpMatrices.at(i).operator()(x,y);
                           }
                        }

                        ++o;
                     }
                     ++i;
                  }
                  if (it != iend ) hasorbs=true;
               }
               ++it;
           }
           if (!hasexp || !hasexpr || !hasorbs)
           {
              throw Exception ("Something went wrong when reading the MCP for " + name);
           }
           pMCPBasis.maxterms = std::max({pMCPBasis.maxterms, mcp.nExp,mcp.nExpR});
           pMCPBasis.AtomMCP.emplace(std::make_pair(sID,mcp));
           ++it;
        }

        bool hasecp; {
            string key;
            istringstream sline;
            sline.str(it->str);
            sline >> key;
            hasecp = (key.substr(0,3) == "ECP");
        }

        if (hasecp)
        {
          ECP ecp;
          istringstream sline;
          sline.str(it->str);
          string flag;
          sline >> flag;
          if (cleanstr(flag)!="ECP")
          {
             throw Exception ("no ECP flag found where expected for atom " + name);
          }
          //Messenger << " Now reading in the ECP " << endl;
          ++it;
          sline.clear();
          sline.str(it->str);
          int no = 0;
          sline >> no;
          ecp.EcpContr.resize(no);
          int i=0;
          ++it;
          while (i < no && (it != iend ))
          {
             //Messenger << " enter while 1" <<  endl;
             sline.clear();
             sline.str(it->str);
             int tmp=0;
             sline >> tmp;
             ecp.EcpContr.at(i).l = (uint8_t)tmp;
             sline >> tmp;
             ecp.EcpContr.at(i).K = (uint8_t)tmp;
             sline >> tmp;
             ecp.EcpContr.at(i).J = (uint8_t)tmp;
             if ((unsigned int)(unsigned char)(ecp.EcpContr.at(i).J) > 1)
             {
                throw Exception ("cannot read generally contracted stuff in Ecps (YET)");
             }

             int k=(unsigned int)(unsigned char)(ecp.EcpContr.at(i).K);
             int j=0;
             ++it;
             while (j < k && (it != iend ))
             {
                //Messenger << " enter while 2" <<  endl;
                //Messenger << j << " " << k <<  endl;
                sline.clear();
                sline.str(it->str);
                int dummy=0;
                if (j == 0)
                   sline >> ecp.EcpContr.at(i).Nexp;
                else
                {
                   sline >> dummy;
                   if (dummy !=ecp.EcpContr.at(i).Nexp)
                   {
                      Messenger << dummy << " " << ecp.EcpContr.at(i).Nexp << endl;
                      throw Exception ("The first numbers for one ECP orb have to be identical");
                   }
                }
                sline >>  ecp.EcpContr.at(i).k[j];
                sline >>  ecp.EcpContr.at(i).N[0][j];
                ++j;
                ++it;
                //Messenger << " incremented j " <<  j <<  endl;
             }
             ecp.EcpContr.at(i).SP=false;
             ++i;
          }
          ++it;
          pECPBasis.AtomECP.emplace(std::make_pair(sID,ecp));
        }

    }

    /*
    if (SP && !ExplicitSPsupport)
    {
       Messenger << "Basis set contains Pople-style SP shells, not natively supported by the present ERI module; S and P shells have been split" << endl;
    }
    */

     //todo lo siguiente es necesario debido a dos factores:
     //1) los atomos pueden no estar en orden, ya sea en el archivo de definiciones atomicas o en las de la base
     //2) pueden haber atomos repetidos; en este caso se deben juntar las definiciones de base

    //copia los GTOs a un array estatico
    map <ElementID, list<GTO*> >::iterator git = GTOpool.begin();

    while (git != GTOpool.end()) {

    	ElementID    Id       = git->first;
    	list<GTO*> & oGTOlist = git->second;
    	int nGTO = oGTOlist.size();

    	// copy everything to a contiguous array
    	list<GTO*>::iterator lit = oGTOlist.begin();


    	pGTOBasis.AtomBasis[Id].shell.set(nGTO);

    	for (int i=0; i<nGTO; ++i) {
            pGTOBasis.AtomBasis[Id][i] = *(*lit);
            pGTOBasis.AtomBasis[Id][i].Normalize();
    	    delete (*lit);
    	    ++lit;
    	}
    	++git;
    }

    GTOpool.clear();

    /*
    // normalize the basis
    map <ElementID, ElementBasis >::iterator ait = pGTOBasis.AtomBasis.begin();

    while (ait != pGTOBasis.AtomBasis.end()) {
   	    for (int i=0; i<ait->second.Nshells(); ++i)
   	        ait->second[i].Normalize();
   	    ++ait;
    }
    */
}


