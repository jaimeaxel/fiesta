#ifndef __GTO__
#define __GTO__

#include <string>
#include <map>
#include <tuple>
#include <memory>

#include "defs.hpp"
#include "low/mem.hpp"
#include "low/rtensors.hpp"
#include "linear/tensors.hpp"
#include "basis/atomdef.hpp"

struct vector3;

/*
    SHELL-LEVEL
*/

// GTO 'elemental' shell
// gaussian exponents must be sorted from smallest (more diffuse) to largest (more localized)
struct GTO {
    double k [maxK];        // gaussian exponents
    double N [maxJ][maxK];  // contraction weights
    double Np[maxJ][maxK];  // contraction weights (only SP batches; not used)

    uint8_t l;      // angular moment
    uint8_t K;      // contraction degree
    uint8_t J;      // number of general contraction functions

    bool SP;   // is an SP shell?

    void Normalize();
    GTO & operator=(const GTO & rhs);

    double operator()(const vector3 & v, int j=1) const;
};

struct GTOforMCP: GTO {
     double energy;
     GTOforMCP(): GTO() { ;}
    ~GTOforMCP(){ ;}
};

struct GTOforECP: GTO {
     int Nexp = 2; // exponent of the r^{2-N} part
     GTOforECP(): GTO() { ;}
    ~GTOforECP(){ ;}
};


/*
    ELEMENT-LEVEL
*/

// elemental  basis
struct ElementBasis {

    Array<GTO> shell; // the GTOs themselves
    int Zcore;         // number of lowest occupied (spin)orbitals; for PPs

          GTO & operator[](int n)       {return shell[n];}
    const GTO & operator[](int n) const {return shell[n];}
    int Nshells()                 const {return shell.n;}

    ElementBasis & operator+=(const ElementBasis & rhs); // union of bases
};

// elemental MCP
struct MCP {
    double Zeff =0.0e0;

    int nExp=0;
    std::vector<double> CoefExp ; //Coefficient for the exp^(ar^2) term
    std::vector<double> ExpExp ; //exponent for the exp^(ar^2) term

    int nExpR=0;
    std::vector<double> CoefExpR ; //Coefficient for the exp^(ar^2)/r term
    std::vector<double> ExpExpR ; //exponent for the exp^(ar^2)/r term

    std::vector<std::vector<GTOforMCP>> ProjOrbs ; // Orbitals to project against
    std::vector<std::vector<GTOforMCP>> EcpOrbs ; // Orbitals to project against

    std::vector<tensor2> EcpMatrices;




    double GetZeff() const {return Zeff;}

    MCP() {;}
    MCP(const MCP& McpIn)
    {
       Zeff =McpIn.Zeff;
       CoefExp = McpIn.CoefExp;
       ExpExp = McpIn.ExpExp;
       CoefExpR = McpIn.CoefExpR;
       ExpExpR = McpIn.ExpExpR;
       nExp = McpIn.nExp;
       nExpR = McpIn.nExpR;
       ProjOrbs = McpIn.ProjOrbs;
       EcpOrbs = McpIn.EcpOrbs;
       EcpMatrices.resize(McpIn.EcpMatrices.size());
       for (int i=0; i < EcpMatrices.size(); i++)
       {
          EcpMatrices.at(i)=McpIn.EcpMatrices.at(i);
       }
    }

    ~MCP()
    {
       //std::cout << " In MCP destructor " << std::endl;
       while (!EcpMatrices.empty())
       {
          EcpMatrices.pop_back();
          //std::cout << " destroyed.. " << std::endl;
       }
       //std::cout << " In MCP destructor done " << std::endl;
    }
};

// elemental ECP
struct ECP {

    std::vector<GTOforECP> EcpContr ;

    ECP() {;}
    ECP(const ECP& EcpIn)
    {
       EcpContr = EcpIn.EcpContr;
    }

    ~ECP()
    {
    }
};



struct ElementalCenter {
    ElementBasis eBasis;
    MCP          eMCP;
    ECP          eECP;

    ElementID eID; // original element's ID
    ElementID sID; // "elemental" ID in the system

    void Load (const string & name);
};



/*
    COMPLETE BASIS LEVEL
*/

// Gaussian-Type Orbital basis set loaded from file
struct GTObasis {
    std::string name;
	std::map <ElementID, ElementBasis> AtomBasis;

	bool LS[LMAX+2]; // whether the basis set includes functions of a given angular momentum
	bool GC;         // is it or does it include general contraction?
	bool LSP;        // does it include  mixed shell sp functions

	GTObasis() {
	    GC = false;
	    LSP = false;
        for (int l=0; l<=LMAX; ++l) LS[l] = false;
    }

	void Load  ();                             // load from file
	void Load  (const std::string & file);     // load from file
    const Array<GTO> & operator[] (ElementID id)   const;  // return the elemental basis of element 'id'
            GTObasis & operator+= (const GTObasis & rhs);  // union of bases

   ~GTObasis ();
};

struct MCPbasis {

    int maxterms=0;
    std::map<ElementID, MCP> AtomMCP;

    int Size() const {return AtomMCP.size();}
    bool Empty() const  {return Size()==0 ;}
    bool Contains(const int& no) const {return (AtomMCP.find(no)!=AtomMCP.end());}
    double GetZeff(const int& no) const {return AtomMCP.at(no).GetZeff();}
    std::vector<double> GetCoefExp(const int& no) const {return AtomMCP.at(no).CoefExp;}
    std::vector<double> GetExpExp(const int& no) const {return AtomMCP.at(no).ExpExp;}
    std::vector<double> GetCoefExpR(const int& no) const {return AtomMCP.at(no).CoefExpR;}
    std::vector<double> GetExpExpR(const int& no) const {return AtomMCP.at(no).ExpExpR;}
    std::vector<std::vector<GTOforMCP>> GetProjOrbs(const int& no) const {return AtomMCP.at(no).ProjOrbs;}
    std::vector<std::vector<GTOforMCP>> GetEcpOrbs(const int& no) const {return AtomMCP.at(no).EcpOrbs;}
    std::vector<tensor2> GetEcpMats(const int& no) const {return AtomMCP.at(no).EcpMatrices;}
    int GetnExp(const int& no) const {return AtomMCP.at(no).nExp;}
    int GetnExpR(const int& no) const {return AtomMCP.at(no).nExpR;}
    ~MCPbasis() {/*std::cout << "in McpBasis destructor" << std::endl;*/}
};


struct ECPbasis {
    int maxterms=0;
    std::map<ElementID, ECP> AtomECP;

    int  Size()  const {return AtomECP.size();}
    bool Empty() const {return Size()==0 ;}
    bool Contains (const int& no) const {return (AtomECP.find(no)!=AtomECP.end());}
    std::vector<GTOforECP> GetEcpContr(const int& no) const {return AtomECP.at(no).EcpContr;}
    ~ECPbasis() {/*std::cout << "in EcpBasis destructor" << std::endl;*/}
};

/*
    LOAD COMPLETE BS+MCP+ECP  BASIS FROM FILE
*/


struct GTOMCPECPbasis {
    GTObasis& pGTOBasis;
    MCPbasis& pMCPBasis;
    ECPbasis& pECPBasis;

    GTOMCPECPbasis() = delete;
    GTOMCPECPbasis(GTObasis& arGtobasis,  MCPbasis& arMcpBasis, ECPbasis& arEcpBasis):
       pGTOBasis(arGtobasis), pMCPBasis(arMcpBasis), pECPBasis(arEcpBasis) {;}

    void Load(); // load all the file set in GTObasis
    void Load(ElementID sID, const std::string & file, ElementID eID); // load from specified file, only a subset
};



#endif
