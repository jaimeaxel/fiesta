
#include "atomprod.hpp"

#include <iostream>
#include "basis/GTO.hpp"
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"
#include "math/angular.hpp"
#include "libechidna/libechidna.hpp"

using namespace std;
using namespace LibAngular;

AtomProdPrototypes::AtomProdPrototypes() {
    AP = NULL;
    APsame = NULL;
    APpool = NULL;
    GPall = NULL;
}

void AtomProdPrototypes::GenerateFrom (const r1tensor<rAtomPrototype*> & AtomTypes) {

     //inicia el array de atomos
    {
        const int n = AtomTypes.N();
        //copia la lista de tipos atomicos
        //ATypes = AtomTypes;

        nA = n;
        nAP = ((n+1)*n)/2;

        //inicia tensores
        APpool = new AtomProdPrototype[nAP+n];
        AP = new AtomProdPrototype*[n];

        int it = 0;
        for (int i=0; i<n; ++i) {
            it += i;
            AP[i] = APpool + it;
        }

        it += n;
        APsame = APpool + it;
    }

    // initializes all shell pair prototypes
    {
        int t=0;

        // atom pairs
        for (int i=0; i<AtomTypes.N(); ++i) {
            for (int j=0; j<=i; ++j) {
                t += AtomTypes[i]->basis.N() * AtomTypes[j]->basis.N();
            }
        }

        // same atom
        for (int i=0; i<AtomTypes.N(); ++i) {
            t += (AtomTypes[i]->basis.N()+1) * AtomTypes[i]->basis.N() / 2;
        }

        GPall = new ShellPairPrototype[t];
    }


    //calcula los productos para productos de atomos distintos
    int n=0;
    short int num=0;

    for (int i=0; i<AtomTypes.N(); ++i) {
        int id1 = AtomTypes[i]->DId; //AtomTypes[i];
        int ir1 = AtomTypes[i]->RId; //AtomTypes[i];

        int nb1 = AtomTypes[i]->basis.N(); //BasisSet[id1].n;

        for (int j=0; j<=i; ++j) {
            int id2 = AtomTypes[j]->DId; //AtomTypes[j];
            int ir2 = AtomTypes[j]->RId; //AtomTypes[i];

            int nb2 = AtomTypes[j]->basis.N(); //BasisSet[id2].n;

            AtomProdPrototype & APP = AP[i][j];
            APP.Atype1 = id1;
            APP.Atype2 = id2;
            APP.eid1   = ir1;
            APP.eid2   = ir2;
            APP.nGP1   = nb1;
            APP.nGP2   = nb2;
            APP.nGPt   = nb1*nb2;
            APP.num    = num;
            APP.same   = false;
            APP.Gs = GPall + n;
            APP.Rs = new float[APP.nGPt];

            int p=0;

            int offset1, offset2;
            offset1 = 0;

            for (int b1=0; b1<nb1; ++b1) {
                const GTO & g1 = AtomTypes[i]->basis[b1]; //BasisSet[id1][b1];

                offset2 = 0;

                for (int b2=0; b2<nb2; ++b2) {
                    const GTO & g2 = AtomTypes[j]->basis[b2]; //BasisSet[id2][b2];

                    APP.Gs[p].BasisProd(g1, g2);

                    if (!APP.Gs[p].inverted) {
                        APP.Gs[p].atype1 = id1; //AtomTypes[i];
                        APP.Gs[p].atype2 = id2; //AtomTypes[j];
                        APP.Gs[p].eid1   = ir1;
                        APP.Gs[p].eid2   = ir2;
                        APP.Gs[p].nb1    = b1;
                        APP.Gs[p].nb2    = b2;
                        APP.Gs[p].f1 = offset1;
                        APP.Gs[p].f2 = offset2;
                    }
                    else {
                        APP.Gs[p].atype1 = id2; //AtomTypes[j];
                        APP.Gs[p].atype2 = id1; //AtomTypes[i];
                        APP.Gs[p].eid1   = ir2;
                        APP.Gs[p].eid2   = ir1;
                        APP.Gs[p].nb1    = b2;
                        APP.Gs[p].nb2    = b1;
                        APP.Gs[p].f1 = offset2;
                        APP.Gs[p].f2 = offset1;
                    }

                    APP.Gs[p].sameatom = false;
                    APP.Rs[p] = 1./g1.k[0] + 1./g2.k[0];

                    ++p;

                    offset2 += g2.J * nmS[g2.l];
                }
                offset1 += g1.J * nmS[g1.l];
            }

            //int offset2 = 0;
            //for (int b2=0; b2<nb2; ++b2) offset2 += nmS[BasisSet[id2][b2].l];
            APP.wa1 = offset1;
            APP.wa2 = offset2;

            //genera y ordena las Rs de mayor a menor
            for (int p=0; p<APP.nGPt; ++p) {
                int    pmax = p;
                float  rmax = APP.Rs[p];

                for (int q=p+1; q<APP.nGPt; ++q) {
                    if (APP.Rs[q]>rmax) {
                        pmax = q;
                        rmax = APP.Rs[q];
                    }
                }

                //for some reason valgrind keeps bitchin about these ones
                if (p!=pmax) {
                    swap(APP.Rs[p], APP.Rs[pmax]);
                    swap(APP.Gs[p], APP.Gs[pmax]);
                }
            }

            //assigns the corresponding numbers in the tensor
            for (int p=0; p<APP.nGPt; ++p)
                APP.Gs[p].num = n + p;

            n += APP.nGPt;

            //sets highest l
            APP.maxL = 0;

            for (int b1=0; b1<nb1; ++b1) {
                const GTO & g1 = AtomTypes[i]->basis[b1]; //BasisSet[id1][b1];
                int l = (g1.l==LSP)?1:g1.l;
                if (l>APP.maxL) APP.maxL = l;
            }

            for (int b2=0; b2<nb2; ++b2) {
                const GTO & g2 = AtomTypes[j]->basis[b2]; //BasisSet[id2][b2];
                int l = (g2.l==LSP)?1:g2.l;
                if (l>APP.maxL) APP.maxL = l;
            }

            ++num;
        }
    }

    nGP = n;

    // same-atom products
    for (int i=0; i<AtomTypes.N(); ++i) {

        int id1 = AtomTypes[i]->DId; //AtomTypes[i];
        int ir1 = AtomTypes[i]->RId; //AtomTypes[i];

        int nb1 = AtomTypes[i]->basis.N(); //BasisSet[id1].n;

        //i==j
        {
            AtomProdPrototype & APP = APsame[i];

            APP.Atype1 = id1;
            APP.Atype2 = id1;
            APP.eid1   = ir1;
            APP.eid2   = ir1;
            APP.nGP1   = nb1;
            APP.nGP2   = nb1;
            APP.nGPt   = (nb1+1)*nb1/2;
            APP.num    = num;
            APP.same   = true;
            APP.Rs = new float[APP.nGPt];
            APP.Gs = GPall + n;
            int p=0;

            int offset1 = 0;
            for (int b1=0; b1<nb1; ++b1) {
                const GTO & g1 = AtomTypes[i]->basis[b1]; //BasisSet[id1][b1];

                int offset2 = 0;
                for (int b2=0; b2<=b1; ++b2) {
                    const GTO & g2 = AtomTypes[i]->basis[b2]; //BasisSet[id1][b2];

                    if (b1>b2) APP.Gs[p].BasisProd(g1, g2);
                    else       APP.Gs[p].BasisProd(g1);

                    if (!APP.Gs[p].inverted) {
                        APP.Gs[p].nb1    = b1;
                        APP.Gs[p].nb2    = b2;
                        APP.Gs[p].f1 = offset1;
                        APP.Gs[p].f2 = offset2;

                    }
                    else {
                        APP.Gs[p].nb1    = b2;
                        APP.Gs[p].nb2    = b1;
                        APP.Gs[p].f1 = offset2;
                        APP.Gs[p].f2 = offset1;
                    }

                    APP.Gs[p].atype1 = id1; //AtomTypes[i];
                    APP.Gs[p].atype2 = id1; //AtomTypes[i];
                    APP.Gs[p].eid1   = ir1; //AtomTypes[i];
                    APP.Gs[p].eid2   = ir1; //AtomTypes[i];

                    APP.Gs[p].sameatom = true;
                    APP.Rs[p] = 1./g1.k[0] + 1./g2.k[0];

                    ++p;

                    offset2 += g2.J * nmS[g2.l];
                }
                offset1 += g1.J * nmS[g1.l];
            }

            APP.wa1 = offset1;
            APP.wa2 = offset1;


            //genera y ordena las Rs de mayor a menor
            for (int p=0; p<APP.nGPt; ++p) {
                int    pmax = p;
                float  rmax = APP.Rs[p];

                for (int q=p+1; q<APP.nGPt; ++q) {
                    if (APP.Rs[q]>rmax) {
                        pmax = q;
                        rmax = APP.Rs[q];
                    }
                }
                if (p!=pmax) {
                    swap(APP.Rs[p], APP.Rs[pmax]);
                    swap(APP.Gs[p], APP.Gs[pmax]);
                }
            }


            //assigns the correspondent numbers in the tensor
            for (int p=0; p<APP.nGPt; ++p)
                APP.Gs[p].num = n + p;

            n += APP.nGPt;

            //sets highest l
            APP.maxL = 0;

            for (int b1=0; b1<nb1; ++b1) {
                const GTO & g1 = AtomTypes[i]->basis[b1]; //BasisSet[id1][b1];
                int l = (g1.l==LSP)?1:g1.l;
                if (l>APP.maxL) APP.maxL = l;
            }

            ++num;
        }
    }


    nGPsame = n - nGP;
}

AtomProdPrototypes::~AtomProdPrototypes() {
    delete[] APpool;
    delete[] AP;
    delete[] GPall;
}


APlists::APlists() {
    SPpool = NULL;
    WWpool = NULL;

    nGTOps = 0;
}

APlists::~APlists() {
    delete[] SPpool;
    delete[] WWpool;
}

static inline int Id12(int id1, int id2) {
    if (id1>=id2) return id1*(id1+1)/2 + id2;
    else          return id2*(id2+1)/2 + id1;
}


struct apsort {
    float v;
    int   n;
};

static void QFsort (apsort * arr, int N) {

    // skip short remainders
    if (N<32) return;

    // find median
    double kmed = 0;
    for (int i=0; i<N; ++i) kmed += arr[i].v;
    kmed /= N;

    int l=0, h=N;

    while(1) {
        while(arr[l].v<=kmed) ++l;
        if (l>=N) return; // the list contains the same value repeated
        while(arr[h].v> kmed) --h;
        if (h<=0) return; // the list contains the same value repeated
        if (l>h) break;
        swap(arr[l],arr[h]);
    }

    QFsort(arr  ,   l); // lower
    QFsort(arr+l, N-l); // upper
}

static void FlashSort(AtomProd * prods, int N) {

    // use a lightweight struct with the distance information
    // instead of moving AtomProd objects all the time
    apsort * keys = new apsort[N+2];
    apsort * arr = keys+1;

    for (int i=0; i<N; ++i) {
        // we expect a priori a uniform distribution,
        // so we must use a value proportional to the volume
        arr[i].v = float(prods[i].r2*sqrt(prods[i].r2));
        arr[i].n = i;
    }
    // safeguards, which simplify
    // QFsort loop
    arr[-1].v = -1e38;
    arr[N].v  =  1e38;
    arr[-1].n = -1;
    arr[N].n  =  N;

    // quasi-flashsort
    QFsort(arr, N);

    // finish local sorting
    // using insertion sort
    for (int i=1; i<N; ++i) {
        apsort x = arr[i];
        int j = i;
        while((j>0) && (arr[j-1].v>x.v)) {
            arr[j] = arr[j-1];
            --j;
        }
        arr[j] = x;
    }

    // check the array is really sorted
    {
        bool sorted = true;
        for (int i=1; i<N; ++i)
            if (arr[i].v<arr[i-1].v)
                sorted = false;
        if (!sorted) {
            cout << "Fsort failed!" << endl;
            for (int i=0; i<N; ++i) cout << " " << arr[i].v; cout << endl;
        }
    }

    // permute by copying to intermediate storage;
    // could be done with cycles, but unlikely to
    // improve performance drastically
    AtomProd * aux = new AtomProd[N];
    for (int i=0; i<N; ++i) aux[i] = prods[arr[i].n];
    for (int i=0; i<N; ++i) prods[i] = aux[i];

    // release mem
    delete[] aux;
    delete[] keys;
}


//symtensor & Sp, symtensor & Tp, symtensor & Xp, symtensor & Yp, symtensor & Zp) {
void APlists::From(const r1tensor<rAtomPrototype*> & AtomTypes, const AtomProdPrototypes & APprototypes, const r2tensor<int> & IL, const rAtom * Atoms, int nAtoms, double logGDO) {

    uint16_t nA = AtomTypes.N();
    uint32_t nAP = nA*(nA+1)/2;

    //generates sorted lists of interacting atom pairs
    //************************************************
    {
        AtomSameBatch.set(nA);
        AtomPairBatch.set(nAP);

        uint32_t na[nA+nAP];
        uint32_t * nap = na + nA;

        for (int a=0; a<nA;  ++a) na[a]  = 0;
        for (int a=0; a<nAP; ++a) nap[a] = 0;

        for (int at1=0; at1<IL.N(); ++at1) {
            int id = Atoms[at1].rAP->RId;
            ++na[id];
        }

        for (int at1=0; at1<IL.N(); ++at1) {
            int id1 = Atoms[at1].rAP->RId;

            //at1 > at2
            for (int j=0; j<IL.M(at1); ++j) {
                int at2 = IL[at1][j];
                if (at2>=at1) break;
                int id2  = Atoms[at2].rAP->RId;
                int n12 = Id12(id1, id2);
                ++nap[n12];
            }
        }

        //sets the prototypes for each list
        for (int id1=0; id1<nA; ++id1) {
            AtomProdPrototype * APP = &APprototypes.APsame[id1];
            AtomSameBatch[id1].APP = APP;
        }

        for (int id1=0; id1<nA; ++id1) {
            for (int id2=0; id2<=id1; ++id2) {
                int n12 = Id12(id1, id2);
                AtomProdPrototype * APP = &(APprototypes.AP[id1][id2]);
                AtomPairBatch[n12].APP = APP;
            }
        }

        for (int a=0; a<nA;  ++a) AtomSameBatch[a].APlist.set(na[a]);
        for (int a=0; a<nAP; ++a) AtomPairBatch[a].APlist.set(nap[a]);

        //populates the lists
        for (int a=0; a<nA;  ++a) na[a]  = 0;
        for (int a=0; a<nAP; ++a) nap[a] = 0;

        //same atom
        for (int at1=0; at1<nAtoms; ++at1) {
            int id = Atoms[at1].rAP->RId;

            AtomProd & AP = AtomSameBatch[id].APlist[na[id]];
            AtomProdPrototype * APP = &APprototypes.APsame[id];
            AP.APprototype       = APP;
            AP.reverse_prototype = false;
            AP.nAP               = APP->num;
            AP.r2                = 0;
            AP.norm              = 0;
            AP.at1               = at1;
            AP.at2               = at1;
            AP.A                 = Atoms[at1].c;
            AP.B                 = Atoms[at1].c;

            ++na[id];
        }

        //different atoms
        for (int at1=0; at1<IL.N(); ++at1) {
            int id1 = Atoms[at1].rAP->RId;

            //at1 > at2
            for (int j=0; j<IL.M(at1); ++j) {
                int at2 = IL[at1][j];
                if (at2>=at1) break;

                int id2  = Atoms[at2].rAP->RId;

                //distance, squared
                double r2; {
                    vector3 c = Atoms[at1].c - Atoms[at2].c;
                    r2 = c*c;
                }

                int n12 = Id12(id1, id2);
                AtomProdPrototype * APP;

                if (id1>=id2) {
                    APP = &(APprototypes.AP[id1][id2]);

                    AtomProd & AP = AtomPairBatch[n12].APlist[nap[n12]];
                    AP.APprototype       = APP;
                    AP.nAP               = APP->num;
                    AP.r2                = r2;
                    AP.norm              = sqrt(r2);
                    AP.at1               = at1;
                    AP.at2               = at2;
                    AP.A                 = Atoms[at1].c;
                    AP.B                 = Atoms[at2].c;
                    AP.reverse_prototype = false;
                    AP.MakeRotation();
                }
                else {
                    APP = &(APprototypes.AP[id2][id1]);

                    AtomProd & AP = AtomPairBatch[n12].APlist[nap[n12]];
                    AP.APprototype       = APP;
                    AP.nAP               = APP->num;
                    AP.r2                = r2;
                    AP.norm              = sqrt(r2);
                    AP.at1               = at2;
                    AP.at2               = at1;
                    AP.A                 = Atoms[at2].c;
                    AP.B                 = Atoms[at1].c;
                    AP.reverse_prototype = true;
                    AP.MakeRotation();
                }

                ++nap[n12];
            }
        }

        // sorts the atom pairs by increasing distance
        // in O(N log N)
        for (int n12=0; n12<AtomPairBatch.n; ++n12) {
            Array<AtomProd> & APlist = AtomPairBatch[n12].APlist;
            FlashSort(APlist.keys, APlist.n);
        }
        //delete[] na;
    }


    //calculates the memory required for the shell pairs and the expansions
    //*********************************************************************
    {
        nGTOps = 0;

        for (int id=0; id<nA; ++id) {
            AtomProdPrototype & APP = APprototypes.APsame[id];

            int maxGP = APP.nGPt;
            AtomSameBatch[id].nAPS.set(maxGP);
            AtomSameBatch[id].SP.set(maxGP);

            nGTOps += AtomSameBatch[id].APlist.n * APprototypes.APsame[id].nGPt;

            for (int i=0; i<AtomSameBatch[id].APlist.n; ++i)
                AtomSameBatch[id].APlist[i].nGTOps = APprototypes.APsame[id].nGPt;

            for (int i=0; i<maxGP; ++i) AtomSameBatch[id].nAPS[i] = AtomSameBatch[id].APlist.n;
        }

        for (int id=0; id<nAP; ++id) {
            AtomProdPrototype & APP = APprototypes.APpool[id];

            AtomPairBatch[id].nAPS.set(APP.nGPt);
            AtomPairBatch[id].SP.set(APP.nGPt);

            int maxGP = APP.nGPt;
            // for each atom pair, discard all shell pairs which dont survive GDO screening
            for (int i=0; i<AtomPairBatch[id].APlist.n; ++i) {

                while(maxGP>0 && AtomPairBatch[id].APlist[i].r2 > logGDO*APP.Rs[maxGP-1]) {
                    AtomPairBatch[id].nAPS[maxGP-1] = i;
                    --maxGP;
                }

                AtomPairBatch[id].APlist[i].nGTOps = maxGP;
                nGTOps += maxGP;
            }

            // for a given elemental shell pair, discard all atom pairs with large enough distance
            for (int i=0; i<APP.nGPt; ++i) {

                int jmax;
                for (jmax=0; jmax<AtomPairBatch[id].APlist.n; ++jmax) {
                    if (AtomPairBatch[id].APlist[jmax].r2 * APP.Gs[i].kr[0][0] > logGDO)
                        break;
                }

                AtomPairBatch[id].nAPS[i] = jmax;
            }
        }
    }


    //initializes and calculates the array of GTO products
    //****************************************************
    {
        ////////////////////////

        for (int id=0; id<nA; ++id) {
            AtomProdPrototype & APP = APprototypes.APsame[id];
            for (int i=0; i<AtomSameBatch[id].APlist.n; ++i) {
                AtomSameBatch[id].APlist[i].pSPs = new ShellPair*[AtomSameBatch[id].APlist[i].nGTOps];
            }
        }

        for (int id=0; id<nAP; ++id) {
            AtomProdPrototype & APP = APprototypes.APpool[id];
            for (int i=0; i<AtomPairBatch[id].APlist.n; ++i) {
                AtomPairBatch[id].APlist[i].pSPs = new ShellPair*[AtomPairBatch[id].APlist[i].nGTOps];
            }
        }



        uint64_t Wmem = 0;

        for (int id=0; id<nA; ++id) {
            AtomProdPrototype & APP = APprototypes.APsame[id];
            for (int b12=0; b12<APP.nGPt; ++b12) {
                int s2 = APP.Gs[b12].Ka * APP.Gs[b12].Kb;
                Wmem += AtomSameBatch[id].APlist.n * s2;
            }
        }

        for (int id=0; id<nAP; ++id) {
            AtomProdPrototype & APP = APprototypes.APpool[id];
            for (int b12=0; b12<APP.nGPt; ++b12) {
                int s2 = APP.Gs[b12].Ka * APP.Gs[b12].Kb;
                Wmem += AtomPairBatch[id].nAPS[b12] * s2;
            }
        }


        //
        int nSP = 0;
        SPpool = new ShellPair[nGTOps];
        uint64_t wSP = 0;
        WWpool = new double[Wmem];


        for (int id=0; id<nA; ++id) {
            AtomProdPrototype & APP = APprototypes.APsame[id];

            for (int b12=0; b12<APP.nGPt; ++b12) {
                int s2 = APP.Gs[b12].Ka * APP.Gs[b12].Kb;

                AtomSameBatch[id].SP[b12] = SPpool + nSP;

                for (int i=0; i<AtomSameBatch[id].APlist.n; ++i) {
                    AtomSameBatch[id].APlist[i].pSPs[b12] = &(SPpool[nSP]);
                    int at1 = AtomSameBatch[id].APlist[i].at1;

                    SPpool[nSP].ata = at1;
                    SPpool[nSP].atb = at1;
                    SPpool[nSP].Form (APP.Gs[b12], Atoms[at1].c, WWpool + wSP);

                    wSP += s2;
                    ++nSP;
                }

                //save this number, as it is important to access it from the tiles
                APP.Gs[b12].nShellPairs = AtomSameBatch[id].APlist.n;
            }
        }

        for (int id=0; id<nAP; ++id) {
            AtomProdPrototype & APP = APprototypes.APpool[id];

            for (int b12=0; b12<APP.nGPt; ++b12) {
                int s2 = APP.Gs[b12].Ka * APP.Gs[b12].Kb;

                AtomPairBatch[id].SP[b12] = SPpool + nSP;

                for (int i=0; i<AtomPairBatch[id].nAPS[b12]; ++i) {
                    AtomPairBatch[id].APlist[i].pSPs[b12] = &(SPpool[nSP]);
                    int at1 = AtomPairBatch[id].APlist[i].at1;
                    int at2 = AtomPairBatch[id].APlist[i].at2;

                    if (!APP.Gs[b12].inverted) {
                        SPpool[nSP].ata = at1;
                        SPpool[nSP].atb = at2;
                    }
                    else {
                        SPpool[nSP].ata = at2;
                        SPpool[nSP].atb = at1;
                    }

                    SPpool[nSP].Form (APP.Gs[b12], Atoms[at1].c, Atoms[at2].c, WWpool + wSP, logGDO);

                    wSP += s2;
                    ++nSP;
                }

                //save this number, as it is important to access it from the tiles
                APP.Gs[b12].nShellPairs = AtomPairBatch[id].nAPS[b12];
            }
        }

    }

}



