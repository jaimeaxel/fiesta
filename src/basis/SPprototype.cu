#include <iostream>
#include "SPprototype.hpp"

//it is assumed that  K*J < 256
//write this in defs.hpp!

void ShellPairPrototype::InitGPU() {

    cudaError_t cudaResult;

    cudaResult = cudaGetLastError();
    if (cudaResult!=cudaSuccess) {
        std::cout << std::endl;
        std::cout << "Error before SPP initialization" << std::endl;
        std::cout << cudaGetErrorString(cudaResult) << std::endl;

        throw(13251);
    }


    //double * NKs = new double[4*maxK + 3*maxK2 + 16*maxK2*maxJ + 16*maxK*maxJ + 2*maxK*maxJ];
    //int nsize;
    int nsize = 2*Ka+2*Kb + 3*Ka*Kb + Ka*Kb*Ja*16 + Kb*Jb*16 + Ka*Ja + Kb*Jb;
    double * NKs = new double[nsize];

    //gather all relevant data
    {
        nsize = 0;
        for (int k=0; k<Ka; ++k) NKs[nsize+k] = ka[k]; nsize += Ka;
        for (int k=0; k<Kb; ++k) NKs[nsize+k] = kb[k]; nsize += Kb;
        for (int k=0; k<Ka; ++k) NKs[nsize+k] = 1./ka[k]; nsize += Ka;
        for (int k=0; k<Kb; ++k) NKs[nsize+k] = 1./kb[k]; nsize += Kb;

        for (int k2=0; k2<Kb; ++k2) for (int k1=0; k1<Ka; ++k1)
            NKs[nsize + k2*Ka + k1] = 1./(ka[k1]+kb[k2]); nsize += Ka*Kb;

        for (int k2=0; k2<Kb; ++k2) for (int k1=0; k1<Ka; ++k1)
            NKs[nsize + k2*Ka + k1] = (1./(ka[k1]+kb[k2])) * sqrt(1./(ka[k1]+kb[k2])); nsize += Ka*Kb;

        for (int k2=0; k2<Kb; ++k2) for (int k1=0; k1<Ka; ++k1)
            NKs[nsize + k2*Ka + k1] = (ka[k1]*kb[k2])/(ka[k1]+kb[k2]); nsize += Ka*Kb;

        for (int k2=0; k2<Kb; ++k2)
            for (int k1=0; k1<Ka; ++k1) {
                double ikab = 0.5/(ka[k1]+kb[k2]);

                for (int j=0; j<Ja; ++j) {
                    double ikn =  BCP.Na[k1][j];
                    for (int v=0; v<16; ++v) {
                        NKs[nsize + ((k2*Ka + k1)*Ja+j)*16+v] = ikn;
                        ikn *= ikab;
                    }
                }
            }
        nsize += Ka*Kb*Ja*16;

        for (int k2=0; k2<Kb; ++k2) {
            for (int j=0; j<Jb; ++j) {
                double ikn =  BCP.Nb[k2][j];

                for (int u=0; u<16; ++u) {
                    NKs[nsize + (k2*Jb+j)*16+u] = ikn;
                    ikn *= (2*kb[k2]);
                }
            }
        }
        nsize += Kb*Jb*16;

        for (int k=0; k<Ka; ++k) for (int j=0; j<Ja; ++j) NKs[nsize + k*Ja + j] = BCP.Na[k][j]; nsize += Ka*Ja;
        for (int k=0; k<Kb; ++k) for (int j=0; j<Jb; ++j) NKs[nsize + k*Jb + j] = BCP.Nb[k][j]; nsize += Kb*Jb;
    }


    int NumGPUs;
    cudaGetDeviceCount(&NumGPUs);

    for (int n=0; n<NumGPUs; ++n) {

        //select device
        cudaSetDevice(n);

        //allocate in device and copy data
        cudaMalloc ((void**)&BCPgpu[n].ddp, nsize*sizeof(double));
        cudaMemcpy (BCPgpu[n].ddp, NKs, nsize*sizeof(double), cudaMemcpyHostToDevice);

        //initialize device pointers
        nsize = 0;
        BCPgpu[n].dka  = BCPgpu[n].ddp + nsize; nsize += Ka;
        BCPgpu[n].dkb  = BCPgpu[n].ddp + nsize; nsize += Kb;
        BCPgpu[n].dika = BCPgpu[n].ddp + nsize; nsize += Ka;
        BCPgpu[n].dikb = BCPgpu[n].ddp + nsize; nsize += Kb;

        BCPgpu[n].dikab = BCPgpu[n].ddp + nsize; nsize += Ka*Kb;
        BCPgpu[n].diks3 = BCPgpu[n].ddp + nsize; nsize += Ka*Kb;
        BCPgpu[n].dkab  = BCPgpu[n].ddp + nsize; nsize += Ka*Kb;

        BCPgpu[n].dNva  = BCPgpu[n].ddp + nsize; nsize += Ka*Kb*Ja*16;
        BCPgpu[n].dNub  = BCPgpu[n].ddp + nsize; nsize += Kb*Jb*16;

        BCPgpu[n].dNa = BCPgpu[n].ddp + nsize; nsize += Ka*Ja;
        BCPgpu[n].dNb = BCPgpu[n].ddp + nsize; nsize += Kb*Jb;
    }


    delete[] NKs;



    cudaResult = cudaGetLastError();
    if (cudaResult!=cudaSuccess) {
        std::cout << "Error in SPP initialization" << std::endl;
        std::cout << cudaGetErrorString(cudaResult) << std::endl;
    }
}


/*
ShellPairPrototype::~ShellPairPrototype() {

    cudaError_t cudaResult;
    cudaResult = cudaGetLastError();

    if (cudaResult!=cudaSuccess) {
        std::cout << std::endl;
        std::cout << "Error before SPP destruction" << std::endl;
        std::cout << cudaGetErrorString(cudaResult) << std::endl;

        throw(13251);
    }

    if (ddp!=NULL) {
        cudaFree(ddp);
        ddp = dka = dkb = dNa = dNb = NULL;
    }
}
*/
