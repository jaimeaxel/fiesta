#include <fstream>
#include <sstream>
#include <string>
#include <cassert>

#include "exciton/exciton_monomer.hpp"
#include "exciton/exciton_dimer.hpp"

using namespace std;

ExcitonDimer::ExcitonDimer() {}
ExcitonDimer::~ExcitonDimer() {}

void ExcitonDimer::init(const vector<string> & inp_options,
                        const ExcitonMonomer & mono_a,
                        const ExcitonMonomer & mono_b,
                        int   dimer_id)
{
    options = inp_options;

    coords = mono_a.GetCoords();
    coords.insert(coords.end(), mono_b.GetCoords().begin(), mono_b.GetCoords().end());

    natoms = coords.size();

    index_a = mono_a.GetIndex();
    index_b = mono_b.GetIndex();

    index_pair = dimer_id;

    nocc_A = mono_a.nocc;
    nvir_A = mono_a.nvir;
    nmo_A  = mono_a.nmo;
    nao_A  = mono_a.nao;

    nocc_B = mono_b.nocc;
    nvir_B = mono_b.nvir;
    nmo_B  = mono_b.nmo ;
    nao_B  = mono_b.nao ;

    nocc = nocc_A + nocc_B;
    nvir = nvir_A + nvir_B;
    nmo  = nmo_A  + nmo_B ;
    nao  = nao_A  + nao_B ;

    // MO coefficients
    C.setsize(nmo, nao);
    assemble_mo_coef(C, mono_a.GetMOs(), mono_b.GetMOs(), nocc_A, nvir_A, nocc_B, nvir_B);

    // assemble density matrices
    D.setsize(nao, nao);
    assemble_density(D, mono_a.GetDensity(), mono_b.GetDensity());

    D_ca_a.setsize(nao, nao);
    D_ca_b.setsize(nao, nao);
    assemble_density(D_ca_a, mono_a.GetDensCat(0), mono_b.GetDensAni(1));
    assemble_density(D_ca_b, mono_a.GetDensCat(1), mono_b.GetDensAni(0));

    D_ac_a.setsize(nao, nao);
    D_ac_b.setsize(nao, nao);
    assemble_density(D_ac_a, mono_a.GetDensAni(0), mono_b.GetDensCat(1));
    assemble_density(D_ac_b, mono_a.GetDensAni(1), mono_b.GetDensCat(0));

    // process system input files and update dimer input file
    dimer_input = "frag_" + to_string(index_a+1) + "_" + to_string(index_b+1) + ".xyz";
}

void ExcitonDimer::clear()
{
    natoms = 0;
    index_a = -1;
    index_b = -1;
    index_pair = -1;
    options.resize(0);
    coords.resize(0);
    C.setsize(0);
    D.setsize(0);
    D_ca_a.setsize(0);
    D_ca_b.setsize(0);
    D_ac_a.setsize(0);
    D_ac_b.setsize(0);
    LE_LE.setsize(0);
    LE_CT.setsize(0);
    CT_CT.setsize(0);
}

void ExcitonDimer::assemble_mo_coef (Matrix & C, const Matrix & CA, const Matrix & CB,
                                     int nocc_A, int nvir_A, int nocc_B, int nvir_B)
{
    int nmo_A = CA.nrows();
    int nao_A = CA.ncols();
    int nmo_B = CB.nrows();
    int nao_B = CB.ncols();
    assert(nocc_A + nvir_A == nmo_A);
    assert(nocc_B + nvir_B == nmo_B);
    //                  AO(A)          AO(B) 
    //            +--------------+--------------+
    // occ. MO(A) |      CA      |      0       |
    // occ. MO(B) |      0       |      CB      |
    //            +--------------+--------------+
    // vir. MO(A) |      CA      |      0       | 
    // vir. MO(B) |      0       |      CB      | 
    //            +--------------+--------------+
    C.zeroize();
    for (int i = 0; i < nocc_A; i++) {
        for (int nu = 0; nu < nao_A; nu++) {
            C[i][nu] = CA[i][nu];
        }
    }
    for (int i = 0; i < nocc_B; i++) {
        for (int nu = 0; nu < nao_B; nu++) {
            C[i + nocc_A][nu + nao_A] = CB[i][nu];
        }
    }
    for (int a = 0; a < nvir_A; a++) {
        for (int nu = 0; nu < nao_A; nu++) {
            C[a + nocc_A + nocc_B][nu] = CA[a + nocc_A][nu];
        }
    }
    for (int a = 0; a < nvir_B; a++) {
        for (int nu = 0; nu < nao_B; nu++) {
            C[a + nocc_A + nocc_B + nvir_A][nu + nao_A] = CB[a + nocc_B][nu];
        }
    }
}

void ExcitonDimer::assemble_density (Matrix& D, const Matrix& DA, const Matrix& DB)
{
    //            AO(A)        AO(B)
    //       +------------+------------+
    // AO(A) |     DA     |     0      |
    //       +------------+------------+
    // AO(B) |     0      |     DB     | 
    //       +------------+------------+
    D.zeroize();
    for (int mu = 0; mu < DA.nrows(); mu++) {
        for (int nu = 0; nu < DA.ncols(); nu++) {
            D[mu][nu] = DA[mu][nu];
        }
    }
    for (int mu = 0; mu < DB.nrows(); mu++) {
        for (int nu = 0; nu < DB.ncols(); nu++) {
            D[DA.nrows() + mu][DA.ncols() + nu] = DB[mu][nu];
        }
    }
}

void ExcitonDimer::create_input(string flag)
{
    // create dimer input file
    ofstream f_out (dimer_input);
    for (int i = 0; i < options.size(); i++) {
        stringstream ss (options[i]);
        string key, tmp, val;
        ss >> key >> tmp >> val;

        if (key.find("METHOD") != string::npos) {
            if (flag == "ca" || flag == "ac") {
                if (val.find("HF") != string::npos) { f_out << key << " = " << "ROHF" << endl; }
                //if (val.find("KS") != string::npos) { f_out << key << " = " << "ROKS" << endl; }
            } else {
                f_out << options[i] << endl;
            }
        }

        else {
            f_out << options[i] << endl;
        }
    }
    for (int a = 0; a < coords.size(); a ++) {
        f_out << coords[a] << endl;
    }
    f_out.close();
}
