#ifndef _EXCITON_MODEL_HPP_
#define _EXCITON_MODEL_HPP_

#include <vector>
#include <string>
#include <sstream>
#include <memory>

#include "interface/interface.hpp"
#include "exciton_monomer.hpp"
#include "exciton_dimer.hpp"

using namespace std;

class ExcitonModel {
  private:
    bool is_mpi_master;

    int total_natoms=0;
    int total_nfrags=0;
    int total_npairs=0;

    int total_num_LE=0;
    int total_num_CT=0;

    vector<int> frag_natoms;
    vector<int> frag_start;

    vector< vector<int> > pairs;
    double min_dist_cutoff=10.0; // Angstrom

    vector<string> options;
    vector<string> coords;

    vector<ExcitonMonomer> monomers;
    ExcitonDimer dimer;

    vector<double> energies;

    Matrix adia_e_tr_dip;
    Matrix adia_v_tr_dip;
    Matrix adia_m_tr_dip;

    bool use_dft=false;

    bool print_output=true;

    void read_frag_info(int num_atoms, string frag_info);
    void read_input(string input_file);

  public:
    ExcitonModel();
    ~ExcitonModel();

    int ct_nocc;
    int ct_nvir;

    int num_LE;
    int num_CT;

    Matrix H;

    Matrix diab_e_tr_dip;
    Matrix diab_v_tr_dip;
    Matrix diab_m_tr_dip;

    const int get_total_natoms() const { return total_natoms; }
    const int get_total_nfrags() const { return total_nfrags; }
    const int get_total_npairs() const { return total_npairs; }

    const bool has_dft() const { return use_dft; }

    const int get_pair_index_a(int i_pair) const { return pairs[i_pair][0]; }
    const int get_pair_index_b(int i_pair) const { return pairs[i_pair][1]; }

    std::shared_ptr<DataRequest> request(int task_id, std::string task_flag);

    void init(const std::shared_ptr<DataInfo>& info, bool print=true);

    void init_task(int task_id, std::string task_flag);
    void finalize_task(int task_id, std::string task_flag, const std::shared_ptr<DataResult>& result);
    void finish();

    void init_monomer(int monomer_id, string flag);
    void finalize_monomer(int monomer_id, string flag, const std::shared_ptr<DataResult>& res);

    void read_monomer_file(int monomer_id);

    void init_dimer(int dimer_id, string flag);
    void finalize_dimer(string flag, const std::shared_ptr<DataResult>& res);

    const vector<double>& get_energies()  { return energies; }

    // help functions for interface
    std::shared_ptr<DataRequest> next_task();
    bool collect_result(const std::shared_ptr<DataResult>& res);
};

#endif
