#ifndef _EXCITON_MONOMER_HPP_
#define _EXCITON_MONOMER_HPP_

#include <vector>
#include <string>

#include "interface/matrix.hpp"

using namespace std;

class ExcitonMonomer {
  private:
    int natoms;
    int index;

    vector<string> options;
    vector<string> coords;

  public:
    string monomer_input;

    Matrix C, D;
    Matrix D_cat_a, D_cat_b;
    Matrix D_ani_a, D_ani_b;

    int nmo, nocc, nvir, nao;

    Matrix E_cis;
    Matrix CI_vecs;

    Matrix e_tr_dip;
    Matrix v_tr_dip;
    Matrix m_tr_dip;

    double e_scf;
    double e_cat;
    double e_ani;

    ExcitonMonomer();
    ~ExcitonMonomer();

    void init (const vector<string> & inp_options,
               const vector<string> & inp_coords,
               const int frag_ind);

    const int get_natoms() { return natoms; }

    const vector<string>& get_coords() { return coords; }
    const string& get_coords(int i) { return coords[i]; }

    const double GetEscf () const { return e_scf; }
    const double GetEcat () const { return e_cat; }
    const double GetEani () const { return e_ani; }
    const int    GetIndex() const { return index; }

    const vector<string>& GetOptions() const { return options; }
    const vector<string>& GetCoords () const { return coords;  }

    const Matrix& GetMOs()     const { return C; }
    const Matrix& GetDensity() const { return D; }
    const Matrix& GetDensCat(bool beta=false) const { return beta ? D_cat_b : D_cat_a; }
    const Matrix& GetDensAni(bool beta=false) const { return beta ? D_ani_b : D_ani_a; }

    const Matrix& GetEcis()    const { return E_cis; }
    const Matrix& GetCIvecs()  const { return CI_vecs; }

    void create_input(string flag);
};

#endif
