#ifndef _EXCITON_DIMER_HPP_
#define _EXCITON_DIMER_HPP_

#include <string>
#include <vector>

#include "interface/matrix.hpp"

using namespace std;

class ExcitonDimer {
  private:
    int natoms;
    int index_a, index_b;
    int index_pair;

    vector<string> options;
    vector<string> coords;

    void assemble_density (Matrix& D, const Matrix& DA, const Matrix& DB);
    void assemble_mo_coef (Matrix& C, const Matrix& CA, const Matrix& CB, int oA, int vA, int oB, int vB);

  public:
    string dimer_input;

    Matrix C, D, F_MO;
    Matrix D_ca_a, D_ca_b;
    Matrix D_ac_a, D_ac_b;

    Matrix ct_e_tr_dip;
    Matrix ct_v_tr_dip;
    Matrix ct_m_tr_dip;

    Matrix LE_LE;
    Matrix LE_CT;
    Matrix CT_CT;

    int nocc_A, nvir_A, nmo_A, nao_A;
    int nocc_B, nvir_B, nmo_B, nao_B;
    int nocc, nvir, nmo, nao;

    double e_gg;
    double e_ca;
    double e_ac;

    ExcitonDimer();
    ~ExcitonDimer();

    void init(const vector<string>& inp_options,
              const ExcitonMonomer& mono_a,
              const ExcitonMonomer& mono_b,
              int   dimer_id);
    void clear();

    const int GetIndexA () const { return index_a; }
    const int GetIndexB () const { return index_b; }

    const int GetIndexPair () const { return index_pair; }

    const double GetE_gg () const { return e_gg; }
    const double GetE_ca () const { return e_ca; }
    const double GetE_ac () const { return e_ac; }

    void create_input(string flag);
};

#endif
