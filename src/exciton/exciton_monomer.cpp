#include <fstream>
#include <sstream>
#include <string>

#include "exciton/exciton_monomer.hpp"

using namespace std;

ExcitonMonomer::ExcitonMonomer()
{
    natoms = 0;
    index = -1;
}

ExcitonMonomer::~ExcitonMonomer() {}

void ExcitonMonomer::init(
        const vector<string> & inp_options,
        const vector<string> & inp_coords,
        const int frag_ind)
{
    options = inp_options;
    coords  = inp_coords;
    natoms  = coords.size();
    index   = frag_ind;

    // process system input files and update monomer input file
    monomer_input = "frag_" + to_string(index+1) + ".xyz";
}

void ExcitonMonomer::create_input(string flag)
{
    // create monomer input file
    ofstream f_out (monomer_input);
    for (int i = 0; i < options.size(); i++) {
        stringstream ss (options[i]);
        string key, tmp, val;
        ss >> key >> tmp >> val;

        if (key.find("CHARGE") != string::npos) {
            if (flag == "cation") {
                f_out << key << " = " << (stoi(val)+1) << endl;
            } else if (flag == "anion") {
                f_out << key << " = " << (stoi(val)-1) << endl;
            } else {
                f_out << options[i] << endl;
            }
        }

        else if (key.find("MULTIPLICITY") != string::npos) {
            if (flag == "cation" || flag == "anion") {
                f_out << key << " = " << (stoi(val)+1) << endl;
            } else {
                f_out << options[i] << endl;
            }
        }

        else if (key.find("METHOD") != string::npos) {
            if (flag == "cation" || flag == "anion") {
                if (val.find("HF") != string::npos) { f_out << key << " = " << "ROHF" << endl; }
                //if (val.find("KS") != string::npos) { f_out << key << " = " << "ROKS" << endl; }
            } else {
                f_out << options[i] << endl;
            }
        }

        else {
            f_out << options[i] << endl;
        }
    }
    for (int a = 0; a < coords.size(); a ++) {
        f_out << coords[a] << endl;
    }
    f_out.close();
}
