#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <exception>
#include <cassert>
#include <cmath>

#include "exciton/exciton_model.hpp"
#include "exciton/exciton_monomer.hpp"
#include "exciton/exciton_dimer.hpp"

using namespace std;

ExcitonModel::ExcitonModel() {}
ExcitonModel::~ExcitonModel() {}

std::shared_ptr<DataRequest> ExcitonModel::request(int task_id, std::string task_flag)
{
    init_task(task_id, task_flag);

    std::string task_type = (task_id < total_nfrags ? "Monomer" : "Dimer");

    std::shared_ptr<DataRequest> req =
        std::shared_ptr<DataRequest>(new DataRequest);

    if (task_type == "Monomer") {
        int monomer_id = task_id;
        req->input_xyz = monomers[monomer_id].monomer_input;
        req->job_type  = "EXMOD_MONOMER";
        req->num_dens  = 0;
    }

    else if (task_type == "Dimer") {
        req->input_xyz = dimer.dimer_input;
        req->job_type  = "EXMOD_DIMER";
        req->num_dens  = 3;

        if (task_flag == "ca") {
            req->Da = dimer.D_ca_a;
            req->Db = dimer.D_ca_b;
            req->C  = dimer.C;
        }

        else if (task_flag == "ac") {
            req->Da = dimer.D_ac_a;
            req->Db = dimer.D_ac_b;
            req->C  = dimer.C;
        }

        else if (task_flag == ""  ) {
            req->num_dens = 5;

            req->Da = dimer.D;
            req->Db = dimer.D;
            req->C  = dimer.C;

            int frag_a = dimer.GetIndexA();
            int frag_b = dimer.GetIndexB();
            req->CI_vecs_A = monomers[frag_a].GetCIvecs();
            req->CI_vecs_B = monomers[frag_b].GetCIvecs();

            req->nocc_A = dimer.nocc_A;
            req->nvir_A = dimer.nvir_A;

            req->nocc_B = dimer.nocc_B;
            req->nvir_B = dimer.nvir_B;

            req->index_A = dimer.GetIndexA();
            req->index_B = dimer.GetIndexB();
        }
    }

    return req;
}

void ExcitonModel::init(const std::shared_ptr<DataInfo>& info, bool print)
{
    is_mpi_master = info->is_master;

    print_output = print;
    print_output &= is_mpi_master;

    read_frag_info(info->num_atoms, info->input_frag);
    read_input(info->input_xyz);

    // set up monomers

    monomers.resize(total_nfrags);
    for (int frag_ind = 0; frag_ind < total_nfrags; frag_ind++) {
        vector<string> mono_coords;
        int start_ind = frag_start[frag_ind];
        int end_ind = start_ind + frag_natoms[frag_ind];
        for (int a = start_ind; a < end_ind; a ++) {
            mono_coords.push_back(coords[a]);
        }
        monomers[frag_ind].init (options, mono_coords, frag_ind);
    }

    // are we doing DFT ?

    use_dft = false;
    for (int i = 0; i < options.size(); i++) {
        stringstream ss (options[i]);
        string key, tmp, val;
        ss >> key >> tmp >> val;
        if (key.find("METHOD") != string::npos &&
            val.find("KS")     != string::npos) {
            use_dft = true;
        }
    }

    // set up dimers

    pairs.resize(0);

    for (int a = 0; a < total_nfrags; a ++) {
        for (int b = a+1; b < total_nfrags; b ++) {

            // calculate minimal distance between a pair of monomers

            double min_dist = 1.0e+99;

            for (int i = 0; i < monomers[a].get_natoms(); i++) {

                stringstream ss_a (monomers[a].get_coords(i));
                string elem_a;
                double xa, ya, za;
                ss_a >> elem_a >> xa >> ya >> za;

                for (int j = 0; j < monomers[b].get_natoms(); j++) {

                    stringstream ss_b (monomers[b].get_coords(j));
                    string elem_b;
                    double xb, yb, zb;
                    ss_b >> elem_b >> xb >> yb >> zb;

                    double rij = sqrt(pow(xa-xb,2) + pow(ya-yb,2) + pow(za-zb,2));
                    if (rij < min_dist) {
                        min_dist = rij;
                    }
                }
            }

            // add dimer with minimal distance below cutoff

            if (min_dist < min_dist_cutoff) {
                vector<int> pair;
                pair.push_back(a);
                pair.push_back(b);
                pairs.push_back(pair);
            }
        }
    }

    total_npairs = pairs.size();

    // get number of LE and CT states

    num_LE  = 5; // default number of locally excited states
    ct_nocc = 2; // default number of occ. orbitals involved in charge-transfer
    ct_nvir = 2; // default number of vir. orbitals involved in charge-transfer

    for (int i = 0; i < options.size(); i++) {
        stringstream ss (options[i]);
        string key, tmp;
        ss >> key >> tmp;
        if (key.find("CIS:NSTATES") != string::npos) {
            ss >> num_LE;
        }
        if (key.find("CIS:CT_NOCC") != string::npos) {
            ss >> ct_nocc;
        }
        if (key.find("CIS:CT_NVIR") != string::npos) {
            ss >> ct_nvir;
        }
    }

    num_CT = ct_nocc * ct_nvir * 2; // number of occ->vir transitions on both directions (A->B and B->A)

    total_num_LE = total_nfrags * num_LE;
    total_num_CT = total_npairs * num_CT;

    if (print_output) {
        stringstream ss;
        ss << "Exciton model:" << endl;
        ss << "  Total number of atoms:      " << total_natoms << endl;
        ss << "  Total number of fragments:  " << total_nfrags << endl;
        ss << "  Total number of pairs:      " << total_npairs << endl;
        ss << "  Total number of LE states:  " << total_num_LE << endl;
        ss << "  Total number of CT states:  " << total_num_CT << endl;
        ss << endl;
        std::cout << ss.str();
    }

    H.setsize(num_LE * total_nfrags + num_CT * total_npairs);

    int dim_H = H.nrows();

    diab_e_tr_dip.setsize(dim_H, 3);
    diab_v_tr_dip.setsize(dim_H, 3);
    diab_m_tr_dip.setsize(dim_H, 3);

    diab_e_tr_dip.zeroize();
    diab_v_tr_dip.zeroize();
    diab_m_tr_dip.zeroize();
}

void ExcitonModel::init_task(int task_id, std::string task_flag)
{
    // monomer task
    if (task_id < total_nfrags) {
        int monomer_id = task_id;
        std::string task_type = "Monomer";
        init_monomer(monomer_id, task_flag);
    }
    // dimer task
    else if (task_id < total_nfrags + total_npairs) {
        int dimer_id = task_id - total_nfrags;
        std::string task_type = "Dimer";
        init_dimer(dimer_id, task_flag);
    }
    // out-of-bound error
    else {
        throw ("Error: out-of-bound task_id " + std::to_string(task_id));
    }
}

void ExcitonModel::finalize_task(int task_id, std::string task_flag, const std::shared_ptr<DataResult>& res)
{
    // monomer task
    if (task_id < total_nfrags) {
        int monomer_id = task_id;
        std::string task_type = "Monomer";
        finalize_monomer(monomer_id, task_flag, res);
    }
    // dimer task
    else {
        int dimer_id = task_id - total_nfrags;
        std::string task_type = "Dimer";
        finalize_dimer(task_flag, res);
    }
}

void ExcitonModel::finish()
{
    if (print_output) {
        std::cout << "#=================================#" << std::endl;
        std::cout << "#         Exciton  Model          #" << std::endl;
        std::cout << "#=================================#" << std::endl;
        std::cout << std::endl;

        std::cout << "Exciton Model Hamiltonian:\n" << std::endl;
        std::cout << H << std::endl;
    }

    Matrix evec;
    std::vector<double> eval;
    DiagonalizeMatrix(H, evec, eval);

    if (print_output) {
        Matrix evec_T;
        evec_T.setsize(evec.ncols(), evec.nrows());
        for (int i = 0; i < evec_T.nrows(); i++) {
        for (int j = 0; j < evec_T.ncols(); j++) {
            evec_T[i][j] = evec[j][i];
        }}
        std::cout << "Eigenvectors (column-wise):\n" << std::endl;
        std::cout << evec_T << std::endl;
    }

    int n = eval.size();

    adia_e_tr_dip.setsize(n, 3);
    adia_v_tr_dip.setsize(n, 3);
    adia_m_tr_dip.setsize(n, 3);

    adia_e_tr_dip.zeroize();
    adia_v_tr_dip.zeroize();
    adia_m_tr_dip.zeroize();

    // TODO: use matrix mutliplication here
    for (int k = 0; k < n; k++) {
    for (int d = 0; d < n; d++) {
        double coef = evec[k][d];

        adia_e_tr_dip[k][0] += coef * diab_e_tr_dip[d][0];
        adia_e_tr_dip[k][1] += coef * diab_e_tr_dip[d][1];
        adia_e_tr_dip[k][2] += coef * diab_e_tr_dip[d][2];

        adia_v_tr_dip[k][0] += coef * diab_v_tr_dip[d][0];
        adia_v_tr_dip[k][1] += coef * diab_v_tr_dip[d][1];
        adia_v_tr_dip[k][2] += coef * diab_v_tr_dip[d][2];

        adia_m_tr_dip[k][0] += coef * diab_m_tr_dip[d][0];
        adia_m_tr_dip[k][1] += coef * diab_m_tr_dip[d][1];
        adia_m_tr_dip[k][2] += coef * diab_m_tr_dip[d][2];
    }}

    if (print_output) {
        std::cout << "Eigenvalues:" << std::endl;
    }

    for (int k = 0; k < n; k++) {

        double exc_ene = eval[k];

        double dip_str = adia_e_tr_dip[k][0] * adia_e_tr_dip[k][0] +
                         adia_e_tr_dip[k][1] * adia_e_tr_dip[k][1] +
                         adia_e_tr_dip[k][2] * adia_e_tr_dip[k][2];

        double osc_str = 2.0 / 3.0 * dip_str * exc_ene;

        double rot_str_len = -(adia_e_tr_dip[k][0] * adia_m_tr_dip[k][0] +
                               adia_e_tr_dip[k][1] * adia_m_tr_dip[k][1] +
                               adia_e_tr_dip[k][2] * adia_m_tr_dip[k][2]);

        double rot_str_vel =  (adia_v_tr_dip[k][0] * adia_m_tr_dip[k][0] +
                               adia_v_tr_dip[k][1] * adia_m_tr_dip[k][1] +
                               adia_v_tr_dip[k][2] * adia_m_tr_dip[k][2]) / exc_ene;

        if (print_output) {

            std::stringstream ss;
            ss << "E[" << k+1 << "]=";
            std::cout << std::left << setw(8) << ss.str();

            std::cout << std::right
                      << setw(10) << setprecision(6) << eval[k] << "  a.u. "
                      << setw(10) << setprecision(5) << eval[k] * 27.211385 << "  eV"
                      << "   f= " << setw(11) << setprecision(6) << osc_str
                      << "   R= " << setw(11) << setprecision(6) << rot_str_vel
                      //<< "   R= " << setw(12) << setprecision(6) << rot_str_len
                      //<< "    "   << setw(12) << setprecision(6) << rot_str_vel
                      << std::endl;

        }

    }

    if (print_output) {
        std::cout << std::endl;
    }
}

// read fragment information
void ExcitonModel::read_frag_info(int num_atoms, string frag_info)
{
    total_natoms = num_atoms;

    ifstream f_info (frag_info);
    if (! f_info.is_open()) {
        throw ("Error: cannot open file " + frag_info);
    }

    string line;
    total_nfrags = 0;
    if (! getline(f_info, line).eof()) {
        stringstream ss (line);
        ss >> total_nfrags;
    } else {
        throw ("Error while reading file " + frag_info);
    }

    frag_natoms.resize(total_nfrags);
    frag_start.resize(total_nfrags);

    stringstream log_ss;

    int count_natoms = 0;
    for (int frag_ind = 0; frag_ind < total_nfrags; frag_ind++) {

        int natoms=0, charge=0, spinmult=1;
        if (! getline(f_info, line).eof()) {
            stringstream ss (line);
            ss >> natoms >> charge >> spinmult;
        } else {
            throw ("Error while reading file " + frag_info);
        }

        if (natoms <= 0) {
            throw ("Error: number of atoms (" + to_string(natoms) + ") is not positive");
        }

        frag_natoms[frag_ind] = natoms;
        frag_start[frag_ind] = count_natoms;

        log_ss << "Fragment " << setw(6) << (frag_ind+1) << ":" << endl;
        log_ss << "  Number of Atoms:     " << natoms << endl;
        log_ss << "  Total Charge:        " << charge << endl;
        log_ss << "  Spin Multiplicity:   " << spinmult << endl;
        log_ss << endl;

        count_natoms += natoms;
    }

    if (print_output) { std::cout << log_ss.str(); }

    if (count_natoms != total_natoms) {
        throw ("Error: inconsistent number of atoms (" +
               to_string(total_natoms) + " vs " +
               to_string(count_natoms) + ")!");
    }

    f_info.close();
}

// go through input file
void ExcitonModel::read_input(string input_file)
{
    options.resize(0);
    coords.resize(0);

    ifstream f_inp (input_file);
    if (! f_inp.is_open()) {
        throw ("Error: cannot open file " + input_file);
    }

    string line;
    while (! getline(f_inp, line).eof()) {
        if (0 == line.length()) {
            continue;
        } else if ('#' == line[0] || '/' == line[0]) {
            options.push_back(line);
        } else {
            coords.push_back(line);
        }
    }

    stringstream log_ss;

    if (coords.size() != total_natoms) {
        for (int i = 0; i < coords.size(); i ++) {
            log_ss << coords[i] << endl;
        }
        throw ("Error: incorrect number of atoms (" + to_string(coords.size()) + ")!");
    }

    if (print_output) { std::cout << log_ss.str(); }

    f_inp.close();
}

// run fragment monomer calculations
void ExcitonModel::init_monomer(int monomer_id, string flag)
{
    if (is_mpi_master) {
        monomers[monomer_id].create_input(flag);
    }

    stringstream log_ss;

    if (flag == "") {
        log_ss << endl;
        log_ss << "#=================================#" << endl;
        log_ss << "#     Fragment Monomer " << setw(6) << (monomer_id+1) << "     #" << endl;
        log_ss << "#=================================#" << endl;
        log_ss << endl;

        log_ss << "+---------------------------------+" << endl;
        log_ss << "|          Ground State           |" << endl;
        log_ss << "+---------------------------------+" << endl;
        log_ss << endl;
    }

    else if (flag == "cation") {
        log_ss << "+---------------------------------+" << endl;
        log_ss << "|             Cation              |" << endl;
        log_ss << "+---------------------------------+" << endl;
        log_ss << endl;
    }

    else if (flag == "anion") {
        log_ss << "+---------------------------------+" << endl;
        log_ss << "|              Anion              |" << endl;
        log_ss << "+---------------------------------+" << endl;
        log_ss << endl;
    }

    if (print_output) { std::cout << log_ss.str(); }
}

void ExcitonModel::finalize_monomer(int monomer_id, string flag, const std::shared_ptr<DataResult>& res)
{
    ExcitonMonomer& mono = monomers[monomer_id];

    if (flag == "cation") {
        mono.e_cat   = res->E;
        mono.D_cat_a = res->Da;
        mono.D_cat_b = res->Db;
        energies.push_back(mono.GetEcat());
    }
    
    else if (flag == "anion") {
        mono.e_ani   = res->E;
        mono.D_ani_a = res->Da;
        mono.D_ani_b = res->Db;
        energies.push_back(mono.GetEani());
    }
    
    else {
        mono.C = res->C;
        mono.D = res->Da;

        mono.e_scf = res->E;
        mono.E_cis = res->E_cis;
        mono.CI_vecs = res->CI_vecs;

        mono.e_tr_dip = res->e_tr_dip;
        mono.v_tr_dip = res->v_tr_dip;
        mono.m_tr_dip = res->m_tr_dip;

        mono.nmo  = res->C.nrows();
        mono.nocc = res->nocc;
        mono.nvir = mono.nmo - mono.nocc;
        mono.nao  = res->C.ncols();

        energies.push_back(mono.GetEscf());

        assert(res->E_cis.nrows() == num_LE);
        for (int k = 0; k < num_LE; k++) {
            int s = k + monomer_id * num_LE;
            H[s][s] = res->E_cis[k][0];

            for (int d = 0; d < 3; d++) {
                diab_e_tr_dip[s][d] = mono.e_tr_dip[k][d];
                diab_v_tr_dip[s][d] = mono.v_tr_dip[k][d];
                diab_m_tr_dip[s][d] = mono.m_tr_dip[k][d];
            }
        }

        if (is_mpi_master) {

            // write to text file
            std::stringstream ss;
            ss << "frag_" << (monomer_id+1) << ".data.txt";

            std::ofstream f_txt(ss.str());
            assert(f_txt.is_open());

            f_txt << std::fixed;
            f_txt.precision(12);

            f_txt << "n_cis   " << mono.E_cis.size() << std::endl;
            f_txt << "n_occ   " << mono.nocc << std::endl;
            f_txt << "n_vir   " << mono.nvir << std::endl;
            f_txt << "n_mo    " << mono.nmo  << std::endl;
            f_txt << "n_ao    " << mono.nao  << std::endl;

            f_txt.close();

            // write to binary file
            ss.str("");
            ss << "frag_" << (monomer_id+1) << ".data.bin";

            std::ofstream f_bin(ss.str(), std::ofstream::binary);
            assert(f_bin.is_open());

            f_bin.write((char*)(&mono.e_scf), sizeof(double));
            f_bin.write((char*)(mono.C.data()), sizeof(double)*mono.C.size());
            f_bin.write((char*)(mono.D.data()), sizeof(double)*mono.D.size());
            f_bin.write((char*)(mono.E_cis.data()), sizeof(double)*mono.E_cis.size());
            f_bin.write((char*)(mono.CI_vecs.data()), sizeof(double)*mono.CI_vecs.size());
            f_bin.write((char*)(mono.e_tr_dip.data()), sizeof(double)*mono.e_tr_dip.size());
            f_bin.write((char*)(mono.v_tr_dip.data()), sizeof(double)*mono.v_tr_dip.size());
            f_bin.write((char*)(mono.m_tr_dip.data()), sizeof(double)*mono.m_tr_dip.size());

            f_bin.close();
        }
    }
}

void ExcitonModel::read_monomer_file(int monomer_id)
{
    ExcitonMonomer& mono = monomers[monomer_id];

    int n_cis;

    // read text file
    std::stringstream ss;
    ss << "frag_" << (monomer_id+1) << ".data.txt";

    std::ifstream f_txt(ss.str());
    assert(f_txt.is_open());

    std::string str_cis, str_occ, str_vir, str_mo, str_ao;

    f_txt >> str_cis >> n_cis;
    f_txt >> str_occ >> mono.nocc;
    f_txt >> str_vir >> mono.nvir;
    f_txt >> str_mo  >> mono.nmo;
    f_txt >> str_ao  >> mono.nao;

    assert(str_cis == "n_cis");
    assert(str_occ == "n_occ");
    assert(str_vir == "n_vir");
    assert(str_mo  == "n_mo");
    assert(str_ao  == "n_ao");

    f_txt.close();

    // resize matrices
    mono.C.setsize(mono.nmo, mono.nao);
    mono.D.setsize(mono.nao, mono.nao);
    mono.E_cis.setsize(n_cis, 1);
    mono.CI_vecs.setsize(n_cis, mono.nocc * mono.nvir);
    mono.e_tr_dip.setsize(n_cis, 3);
    mono.v_tr_dip.setsize(n_cis, 3);
    mono.m_tr_dip.setsize(n_cis, 3);

    // read binary file
    ss.str("");
    ss << "frag_" << (monomer_id+1) << ".data.bin";

    std::ifstream f_bin(ss.str(), std::ifstream::binary);
    assert(f_bin.is_open());

    f_bin.read((char*)(&mono.e_scf), sizeof(double));
    f_bin.read((char*)(mono.C.data()), sizeof(double)*mono.C.size());
    f_bin.read((char*)(mono.D.data()), sizeof(double)*mono.D.size());
    f_bin.read((char*)(mono.E_cis.data()), sizeof(double)*mono.E_cis.size());
    f_bin.read((char*)(mono.CI_vecs.data()), sizeof(double)*mono.CI_vecs.size());
    f_bin.read((char*)(mono.e_tr_dip.data()), sizeof(double)*mono.e_tr_dip.size());
    f_bin.read((char*)(mono.v_tr_dip.data()), sizeof(double)*mono.v_tr_dip.size());
    f_bin.read((char*)(mono.m_tr_dip.data()), sizeof(double)*mono.m_tr_dip.size());

    f_bin.close();
}

void ExcitonModel::init_dimer(int dimer_id, string flag)
{
    int frag_a = pairs[dimer_id][0];
    int frag_b = pairs[dimer_id][1];

    if (flag == "") {
        dimer.clear();
        ExcitonMonomer & mono_a = monomers[frag_a];
        ExcitonMonomer & mono_b = monomers[frag_b];
        dimer.init(options, mono_a, mono_b, dimer_id);
    }

    if (is_mpi_master) {
        dimer.create_input(flag);
    }

    stringstream log_ss;

    if (flag == "") {
        log_ss << endl;
        log_ss << "#=================================#" << endl;
        log_ss << "#     Fragment Dimer " << setw(4) << (frag_a+1) << " " << setw(4) << (frag_b+1) << "    #" << endl;
        log_ss << "#=================================#" << endl;
        log_ss << endl;

        log_ss << "+---------------------------------+" << endl;
        log_ss << "|          Ground State           |" << endl;
        log_ss << "+---------------------------------+" << endl;
        log_ss << endl;
    }

    else if (flag == "ca") {
        log_ss << "+---------------------------------+" << endl;
        log_ss << "|      Charge Transfer (A->B)     |" << endl;
        log_ss << "+---------------------------------+" << endl;
        log_ss << endl;
    }

    else if (flag == "ac") {
        log_ss << "+---------------------------------+" << endl;
        log_ss << "|      Charge Transfer (B->A)     |" << endl;
        log_ss << "+---------------------------------+" << endl;
        log_ss << endl;
    }

    if (print_output) { std::cout << log_ss.str(); }
}

void ExcitonModel::finalize_dimer(string flag, const std::shared_ptr<DataResult>& res)
{
    if (flag == "") {
        dimer.e_gg = res->E;
        energies.push_back(dimer.GetE_gg());

        dimer.ct_e_tr_dip = res->e_tr_dip;
        dimer.ct_v_tr_dip = res->v_tr_dip;
        dimer.ct_m_tr_dip = res->m_tr_dip;

        dimer.LE_LE = res->LE_LE;
        dimer.LE_CT = res->LE_CT;
        dimer.CT_CT = res->CT_CT;

        assert(res->LE_LE.nrows() == num_LE);
        assert(res->LE_LE.ncols() == num_LE);

        assert(res->LE_CT.nrows() == num_LE + num_LE);
        assert(res->LE_CT.ncols() == num_CT);

        assert(res->CT_CT.nrows() == num_CT);
        assert(res->CT_CT.ncols() == num_CT);

        int frag_a = dimer.GetIndexA();
        int frag_b = dimer.GetIndexB();

        int i_pair = dimer.GetIndexPair();

        assert(frag_a == pairs[i_pair][0]);
        assert(frag_b == pairs[i_pair][1]);

        // CT transition dipoles
        for (int k = 0; k < num_CT; k++) {
            int ct = k + total_nfrags * num_LE + i_pair * num_CT;
            for (int d = 0; d < 3; d++) {
                diab_e_tr_dip[ct][d] = dimer.ct_e_tr_dip[k][d];
                diab_v_tr_dip[ct][d] = dimer.ct_v_tr_dip[k][d];
                diab_m_tr_dip[ct][d] = dimer.ct_m_tr_dip[k][d];
            }
        }

        // CT energy and CT-CT coupling
        for (int kA = 0; kA < num_CT; kA++) {
        for (int kB = 0; kB < num_CT; kB++) {
            int ctA = kA + total_nfrags * num_LE + i_pair * num_CT;
            int ctB = kB + total_nfrags * num_LE + i_pair * num_CT;
            H[ctA][ctB] = res->CT_CT[kA][kB];
        }}

        // LE-LE coupling
        for (int mA = 0; mA < num_LE; mA++) {
        for (int mB = 0; mB < num_LE; mB++) {
            int sA = mA + frag_a * num_LE;
            int sB = mB + frag_b * num_LE;
            H[sA][sB] = res->LE_LE[mA][mB];
            H[sB][sA] = res->LE_LE[mA][mB];
        }}

        // LE-CT coupling
        for (int k = 0; k < num_CT; k++)  {
            int ct = k + total_nfrags * num_LE + i_pair * num_CT;

            for (int mA = 0; mA < num_LE; mA++) {
                int sA = mA + frag_a * num_LE;
                H[sA][ct] = res->LE_CT[mA][k];
                H[ct][sA] = res->LE_CT[mA][k];
            }

            for (int mB = 0; mB < num_LE; mB++) {
                int sB = mB + frag_b * num_LE;
                H[sB][ct] = res->LE_CT[mB + num_LE][k];
                H[ct][sB] = res->LE_CT[mB + num_LE][k];
            }
        }

        // three-body CT-CT coupling
        dimer.F_MO = res->F_MO;

        for (int frag_c = 0; frag_c < total_nfrags; frag_c++) {

            bool found_CA = false;
            int  ind_CA = -1;
            for (int ind = 0; ind < pairs.size(); ind++) {
                const std::vector<int>& pair = pairs[ind];
                found_CA = found_CA || (pair[0] == frag_c && pair[1] == frag_a);
                found_CA = found_CA || (pair[1] == frag_c && pair[0] == frag_a);
                if (found_CA) { ind_CA = ind; break; }
            }
            if (! found_CA) { continue; }

            bool found_CB = false;
            int  ind_CB = -1;
            for (int ind = 0; ind < pairs.size(); ind++) {
                const std::vector<int>& pair = pairs[ind];
                found_CB = found_CB || (pair[0] == frag_c && pair[1] == frag_b);
                found_CB = found_CB || (pair[1] == frag_c && pair[0] == frag_b);
                if (found_CB) { ind_CB = ind; break; }
            }
            if (! found_CB) { continue; }

            int nocc_A = dimer.nocc_A;
            int nvir_A = dimer.nvir_A;
            int nocc_B = dimer.nocc_B;
            int nvir_B = dimer.nvir_B;

            int nocc = nocc_A + nocc_B;
            int nvir = nvir_A + nvir_B;

            if (print_output) { std::cout << std::endl; }

            int ct_ov = ct_nocc * ct_nvir;

            // C(i)->A(a) vs C(i)->B(b): f_ab
            for (int i = 0; i < ct_nocc; i++) {
            for (int a = 0; a < ct_nvir; a++) {
            for (int b = 0; b < ct_nvir; b++) {

                int ct_ia = (i * ct_nvir + a) + (frag_c < frag_a ? 0 : ct_ov);
                int ct_ib = (i * ct_nvir + b) + (frag_c < frag_b ? 0 : ct_ov);

                double f_vA_vB = res->F_MO[a + nocc][b + dimer.nvir_A + nocc];
                int ct_CA = total_num_LE + ind_CA * num_CT + ct_ia;
                int ct_CB = total_num_LE + ind_CB * num_CT + ct_ib;
                H[ct_CA][ct_CB] = f_vA_vB;
                H[ct_CB][ct_CA] = f_vA_vB;

                if (print_output) {
                    std::cout << "  CT-CT coupling:  "
                              << (frag_c+1) << "+(H" << i << ")" << (frag_a+1) << "-(L" << a << ")  "
                              << (frag_c+1) << "+(H" << i << ")" << (frag_b+1) << "-(L" << b << ")  "
                              << std::setw(20) << std::setprecision(12) << f_vA_vB << std::endl;
                }

            }}}

            // A(i)->C(a) vs B(j)->C(a): -f_ij
            for (int a = 0; a < ct_nvir; a++) {
            for (int i = 0; i < ct_nocc; i++) {
            for (int j = 0; j < ct_nocc; j++) {

                int ct_ia = (i * ct_nvir + a) + (frag_a < frag_c ? 0 : ct_ov);
                int ct_ja = (j * ct_nvir + a) + (frag_b < frag_c ? 0 : ct_ov);

                double f_oA_oB = res->F_MO[nocc_A-1 - i][nocc-1 - j];
                int ct_AC = total_num_LE + ind_CA * num_CT + ct_ia;
                int ct_BC = total_num_LE + ind_CB * num_CT + ct_ja;
                H[ct_AC][ct_BC] = -f_oA_oB;
                H[ct_BC][ct_AC] = -f_oA_oB;

                if (print_output) {
                    std::cout << "  CT-CT coupling:  "
                              << (frag_c+1) << "-(L" << a << ")" << (frag_a+1) << "+(H" << i << ")  "
                              << (frag_c+1) << "-(L" << a << ")" << (frag_b+1) << "+(H" << j << ")  "
                              << std::setw(20) << std::setprecision(12) << -f_oA_oB << std::endl;
                }

            }}}

            if (print_output) { std::cout << std::endl; }
        }
    }

    else if (flag == "ca") {
        dimer.e_ca = res->E;
        energies.push_back(dimer.GetE_ca());
    }

    else if (flag == "ac") {
        dimer.e_ac = res->E;
        energies.push_back(dimer.GetE_ac());
    }
}
