/*
    strings.cpp

    rutinas de operaciones comunes con strings
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "low/strings.hpp"
using namespace std;


string cleanstr(const string & in) {
    string out;
    int fp = in.find_first_not_of(' ');
    int lp = in.find_last_not_of(' ');
    out = in.substr(fp,1+lp-fp);

    //out = ucase(out);

    return out;
}

double s2d (const string & in) {
    if (!is_numeric(in)) throw Exception("Expected real value in place of '" + in + "'");
    double v;
    istringstream val;
    val.str(in);
    val >> v;
    return v;
}

int s2i (const string & in) {
    if (!is_numeric(in)) throw Exception("Expected integer value in place of '" + in + "'");
    int v;
    istringstream val;
    val.str(in);
    val >> v;
    return v;
}

bool s2b (const string & in) {
    if (in=="ON"  || in=="TRUE"  || in=="1") return true;
    if (in=="OFF" || in=="FALSE" || in=="0") return false;
    throw Exception("Expected boolean value in place of '" + in + "'");; // unrecognized sequence
    return false;  // just a formality
}


bool is_numeric(const string & in) {
    bool num   = true;
    bool comma = false;
    bool expp  = false;
    bool negm  = false;
    bool nege  = false;
    bool posm  = false;
    bool pose  = false;

    for (int i=0; i<int(in.length()); ++i) {
        // check if there is a comma, and whether it makes sense
        if (in[i] == '.') {
            if (comma or expp) {num = false; break;}
            else {comma = true;}
        }
        // check if it is in scientific notation
        else if ((in[i] == 'e') || (in[i] == 'E')) {
            if (expp) {num = false; break;}
            else {expp = true;}
        }
        // check that the minuses make sense
        else if (in[i] == '-') {
            if (i==0) negm = true;
            else if (expp && !nege) nege = true;
            else {num = false; break;}
        }
        // check that the pluses make sense
        else if (in[i] == '+') {
            if (i==0) posm = true;
            else if (expp && !pose) pose = true;
            else {num = false; break;}
        }
        else if ((in[i] > 57) || (in[i]<48)) {num = false; break;}
    }
    return num;
}

char ucase(const char & c) {
    if ((c > 96) && (c < 123)) return c-32;
    return c;
}

string ucase(const string & name) {
    string ret;
    ret = name;
    for (unsigned int i=0; i<ret.size(); ++i)
        if ((ret[i] > 96) && (ret[i] < 123)) ret[i] -= 32;
    return ret;
}

string int2str(int number) {
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

string float2str(double number) {
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}
