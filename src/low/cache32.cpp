#include "cache32.hpp"
using namespace std;

ostream & operator<<(ostream & os, const cacheline32 & rhs) {
    float a[FLOATS_PER_CACHE_LINE];
    store(a, rhs);

    for (int i=0; i<FLOATS_PER_CACHE_LINE; ++i) os << a[i] << " ";
    return os;
}

void PackArrays     (const float * __restrict__  arrays, uint32_t ArraySize, cacheline32 * __restrict__  array16) {
    float * darr16 = (float*) array16;

    for (uint32_t k=0; k<ArraySize; ++k) {
        for (uint32_t n=0; n<FLOATS_PER_CACHE_LINE; ++n) {
            darr16[FLOATS_PER_CACHE_LINE*k+n] = arrays[n*ArraySize + k];
        }
    }
}

void UnPackArrays   (const cacheline32 * __restrict__ array16,  uint32_t ArraySize, float * arrays) {
    const float * darr16 = (float*) array16;

    for (uint32_t n=0; n<FLOATS_PER_CACHE_LINE; ++n) {
        for (uint32_t s=0; s<ArraySize; ++s) {
            arrays[n * ArraySize + s] = darr16[FLOATS_PER_CACHE_LINE*s + n]; //0; //
        }
    }
}
