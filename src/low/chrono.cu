
#include "low/chrono.hpp"
#include <cmath>
using namespace std;

const int USECSPERSEC = 1000000;


StreamChrono::StreamChrono() {
    cudaEventCreate (&start);
    cudaEventCreate (&stop);

    total_secs = 0;
    total_usecs = 0;
    running = false;
    first   = true;
}

StreamChrono::~StreamChrono() {
    cudaEventDestroy (start);
    cudaEventDestroy (stop);
}

void StreamChrono::Start (cudaStream_t * stream) {

    if (!first) {
        float ms = 0;
        cudaEventSynchronize (stop);
        cudaEventElapsedTime (&ms, start, stop);

        last_secs  = int(0.001*ms);
        last_usecs = int(1000*ms)%USECSPERSEC;

        total_secs  += last_secs;
        total_usecs += last_usecs;
    }
    else
        first = false;

    cudaEventRecord (start, *stream);
    running = true;
}

void StreamChrono::Stop (cudaStream_t * stream) {
    cudaEventRecord (stop, *stream);
    running = false;
}


double StreamChrono::GetLastTime() const {

    double sec = double(last_usecs);
    sec /= 1000000.;
    sec += double(last_secs);

    return sec;
}


double StreamChrono::GetTotalTime() {

    if (!first) {
        float ms = 0;
        cudaEventSynchronize (stop);
        cudaEventElapsedTime (&ms, start, stop);

        last_secs  = int(0.001*ms);
        last_usecs = int(1000*ms)%USECSPERSEC;

        total_secs  += last_secs;
        total_usecs += last_usecs;
    }
    else
        first = false;

    double sec = double(total_usecs);
    sec /= 1000000.;
    sec += double(total_secs);

    return sec;
}
