#include <iostream>
#include <cmath>
#include "plotters.hpp"
#include "fiesta/fiesta.hpp"
using namespace std;


MessagePlotter::MessagePlotter() {
  #if __PLOTTERS__PLOT__==true
    os = &(std::cout);
    openfile = false;

    os->setf(std::ios::fixed);
    Precision = os->precision();

    Init();
  #endif
}

MessagePlotter::~MessagePlotter() {
  #if __PLOTTERS__PLOT__==true
    if (openfile) file.close();

    delete LastChrono;

    while (!Chronos.empty()) {
        LastChrono = Chronos.top();
        Chronos.pop();
        delete LastChrono;
    }
  #endif
}

void MessagePlotter::SetFile(const std::string & filename) {
  #if __PLOTTERS__PLOT__==true
    string filepath = Fiesta::OutputDir + filename;
    file.open(filepath.c_str());
    openfile = true;

    os = &(file);

    os->setf(std::ios::fixed);
    Precision = os->precision();
  #endif
}

void MessagePlotter::Init() {
  #if __PLOTTERS__PLOT__==true
    MaxDepth = 100;
    Depth      = 0;
    ExtraDepth = 0;
    status = true;
    Link = NULL;
    Width     = 6; //std::cout.width();
    LastChrono = new Chronometer;
    LastChrono->Start();
    newline = true;
    count = 0;
    LastPush = 0;
    freshlevel = true;
    p = NULL;
  #endif
}

void MessagePlotter::SetMaxDepth(int iDepth) {
  #if __PLOTTERS__PLOT__==true
    MaxDepth = iDepth;
  #endif
}

void MessagePlotter::Push(int level) {
  #if __PLOTTERS__PLOT__==true
    MaxDepth += level;
  #endif
}

void MessagePlotter::Push() {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    os->flush();

    if (CheckStatus()) {
        LastPush = TotDepth();
        freshlevel = true;
        if (Link!=NULL) Link->LastPush = LastPush;

        Chronos.push(LastChrono);
        LastChrono = new Chronometer;
        LastChrono->Start();
    }

    ++Depth;
  #endif
}

void MessagePlotter::Push(const std::string & str) {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    *this << str;
    Push();
  #endif
}


void MessagePlotter::Pop() {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;
    --Depth;

    // nothing was printed between last push and this pop
    if (CheckStatus()) {
        bool same = (LastPush==TotDepth() && freshlevel);

        LastChrono->Stop();

        if (!same) {
            for (int i=0; i<TotDepth(); ++i) *os << "  ";
            *os << " Time: " << *LastChrono << endl;
        }
        else {
            for (int i=count; i<80; ++i)    *os << " ";
            *os << *LastChrono << endl;
        }
        newline = true;

        delete LastChrono;
        LastChrono = Chronos.top();
        Chronos.pop();
        freshlevel = false;
    }
  #endif
}

void MessagePlotter::Pop(int level) {
  #if __PLOTTERS__PLOT__==true
    MaxDepth -= level;
  #endif
}

//maybe it has to break
void MessagePlotter::NewLine() {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    if (Link!=NULL && Depth==0 && Link->freshlevel) {
        Link->freshlevel ;
        freshlevel = true;
        LastPush = Link->LastPush;
    }

    // if last action was a push
    if (freshlevel) {
        *os << endl;
        newline = true;
    }

    freshlevel=false;

    if (newline) {
        count = TotDepth();
        for (int i=0; i<TotDepth(); ++i) (*os) << "  ";
        newline = false;
    }
  #endif
}

int MessagePlotter::TotDepth() const {
  #if __PLOTTERS__PLOT__==true
    int pDepth = Depth + ExtraDepth;
    if (Link!=NULL) pDepth += Link->TotDepth();

    return max(0,pDepth);
  #else
    return 0;
  #endif
}

void MessagePlotter::precision(int p) {
    Precision = p;
}

int MessagePlotter::width() {
    return Width;
}

void MessagePlotter::width(int p) {
    Width = p;
}

void MessagePlotter::Out(const char * c) const {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    if (CheckStatus()) {
        *os << *c;
        (*os).flush();
    }
  #endif
}

void MessagePlotter::SetModule(const std::string & rhs) {
  #if __PLOTTERS__PLOT__==true
    module = rhs;
  #endif
}

void MessagePlotter::SetParent(MessagePlotter * parent) {
  #if __PLOTTERS__PLOT__==true
    Link = parent;
  #endif
}

void MessagePlotter::Disable() {
    status = false;
}

void MessagePlotter::Enable() {
    status = true;
}

bool MessagePlotter::IsEnabled() {
    return status;
}

bool MessagePlotter::CheckStatus() const {
  #if __PLOTTERS__PLOT__==true
    bool ret = status;
    if (Link!=NULL) ret &= Link->CheckStatus();
    ret &= (TotDepth()<MaxDepth);
    return ret;
  #else
    return true;
  #endif
}

// this specializes the previous class for double pointers, which will be dereferenced;
// the code must be accessible everywhere the parent template's is
template<> MessagePlotter & operator<< (MessagePlotter & MP, const double * const & out) {

    #if __PLOTTERS__PLOT__==true
    if (NodeGroup.IsMaster()) {
        MP.Print(out); // store the pointer until next input
    }

    #endif

    return MP;
}

// this specializes the previous class for double pointers, which will be dereferenced;
// the code must be accessible everywhere the parent template's is
template<> MessagePlotter & operator<< (MessagePlotter & MP, double * const & out) {

    #if __PLOTTERS__PLOT__==true
    if (NodeGroup.IsMaster()) {
        MP.Print(out); // store the pointer until next input
    }

    #endif

    return MP;
}


void MessagePlotter::Print(const std::string & out) {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    NewLine();
    (*os) << out;

    count += out.length();
  #endif
}

void MessagePlotter::Print(const char * out) {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    std::string str = out;

    Print(str);
  #endif
}

#include "float.h"

bool IsNumber(double x) {
    return (x == x);
}

bool IsFinite(double x) {
    return (x<=DBL_MAX && x>=-DBL_MAX);
}

void MessagePlotter::Print(double out) {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return;

    NewLine();

    int oPrecision = (*os).precision();
    int oWidth     = (*os).width();

    (*os).precision(Precision);

    //align, etc
    if (IsNumber(out) && IsFinite(out)) {
        double cc = out;
        if (cc>=0) (*os) << " "; //for the minus sign
        cc = fabs(cc);
        int w = Width;
        for (;cc>=1.; cc*=0.1) --w;
        if (fabs(out) < 1.0) --w;
        for (;w>0; --w) (*os) << " ";
    }

    (*os) << out;

    (*os).precision(oPrecision);
  #endif
}


template<> void MessagePlotter::Print<int>(const int & N) {
  #if __PLOTTERS__PLOT__==true
    NewLine();
    if (newline) for (int i=0; i<TotDepth(); ++i) {(*os) << "  "; ++count;} newline = false;

    int oPrecision = (*os).precision();
    int oWidth     = (*os).width();

    (*os).precision(Precision);
    (*os).width(Width);

    if (p!=NULL) {
        for (int n=0; n<N; ++n) (*os) << p[n] << "  ";
        p=NULL;
    }
    else
        (*os) << N;

    (*os).precision(oPrecision);
    (*os).width(oWidth);
  #endif
}


// only for endl, which is template <typename T> T& endl (T&) and produces ambiguous calls
MessagePlotter & MessagePlotter::operator<<(std::ostream & (*f)(std::ostream & ) ) {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return *this;

    if (CheckStatus()) {
        f( (*os) );
        newline = true;
        count = 0;
    }

    if (Link!=NULL && Link->os != os) *(Link) << f;

    ExtraDepth = 0; // after go back to regular state
  #endif

    return *this;
}

MessagePlotter & MessagePlotter::operator()(int level) {
  #if __PLOTTERS__PLOT__==true
    if (!NodeGroup.IsMaster()) return *this;

    ExtraDepth = level;
  #endif
    return *this;
}
