/*
    mem.hpp

    estructuras de datos y rutinas para un uso mas o menos efectivo de la memoria;
    debido a que la mayoria de clases son 'templates', se deben implementar en un archivo de cabecera
*/

#ifndef __MEM__
#define __MEM__

#include <iostream>
#include <set>
using namespace std;


#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
#endif



struct slock {
    #ifdef _OPENMP
    omp_lock_t tlock;
    #endif

    slock() {
        #ifdef _OPENMP
        omp_init_lock(&tlock);
        #endif
    }

    void lock() {
        #ifdef _OPENMP
        omp_set_lock(&tlock);
        #endif
    }

    void unlock() {
        #ifdef _OPENMP
        omp_unset_lock(&tlock);
        #endif
    }

    void wait() {
        #ifdef _OPENMP
        omp_set_lock(&tlock);
        omp_unset_lock(&tlock);
        #endif
    }

    ~slock() {
        #ifdef _OPENMP
        omp_destroy_lock(&tlock);
        #endif
    }
};


/*
template <class T> struct Array {
    int n;
    T * keys;

    Array() {
        n = 0;
        keys = NULL;
    }

    Array(int nelem) {
        n = nelem;
        keys = new T[n];
    }

    void set(int nelem) {
        //if (n!=0) delete[] keys;
        n = nelem;
        keys = new T[n];
    }

    int search(const T & val) const {
        for (int i=0; i<n; ++i)
            if (keys[i] == val) return i;
        return -1; // should not happen
    }

    bool isin(const T & val, int nmax) const {
        for (int i=0; i<nmax; ++i)
            if (keys[i] == val) return true;
        return false;
    }

	T & operator[] (int i) {
		return keys[i];
	}

    const T & operator[] (int i) const {
		return keys[i];
	}

//	Array<T> & operator=(const std::set<T> & TheSet) {
//	    typedef std::set<T> set_T;
//	    set_T::iterator it;
//	    int i=0;
//	    n = TheSet.size();
//	    keys = new T[n];
//	    for (it=TheSet.begin(); it!=TheSet.end(); ++it) {
//	        keys[i] = *it;
//	        ++i;
//	    }
//	    return *this;
//	}


    void clear() {
        if (n!=0) delete[] keys;
        n = 0;
    }

    ~Array() {
        if (n!=0) delete[] keys;
    }

    Array<T> & operator=(const Array<T> & rhs) {
        set(rhs.n);
        for (int i=0; i<n; ++i)
            keys[i] = rhs.keys[i];
        return *this;
    }
};
*/

template <class T> struct LinkedList {
    LinkedList<T> * next;
    T key;
};

template <class T> struct DLinkedList {
    DLinkedList<T> * prev;
    DLinkedList<T> * next;
    T key;
};

template <class T> struct Queue {
    DLinkedList<T> * root;
    DLinkedList<T> * last;
    T err;
    int n;

    Queue() {
        root = NULL;
        last = NULL;
        n = 0;
    }

    void SetErr(const T & element) {
        err = element;
    }

    void PushBack(const T & element) {
        //std::cout << element << std::endl;
        if (root == NULL) {
            root = new DLinkedList<T>;
            last = root;
            last->prev = NULL;
            last->next = NULL;
        }
        else {
            last->next = new DLinkedList<T>;
            DLinkedList<T> * tmp = last->next;
            tmp->prev = last;
            tmp->next = NULL;
            last = tmp;
        }
        //cout << root << " " << last << endl;
        ++n;
        last->key = element;
    }

    T PopBack() {
        if (last == NULL) {
            return err;
        }
        else {
            T el = last->key;
            DLinkedList<T> * tmp = last;
            last = last->prev;
            delete tmp;
            if (last!=NULL) last->next = NULL;
            --n;
            return el;
        }
    }

    bool IsVoid() {
        return (n==0);
    }

    Queue<T> & operator=(const Queue<T> & rhs) {
        Clear();
        return (*this+=rhs);
    }

    //append
    Queue<T> & operator+=(const Queue<T> & rhs) {

        const DLinkedList<T> * tmp = rhs.root;

        for (int i=0; i<rhs.n; ++i) {
            PushBack(tmp->key);
            tmp = tmp->next;
        }

        return *this;
    }

    //ordered merge (if possible)
    const Queue<T> operator|(const Queue<T> & rhs) const {
        Queue<T> ret;

        const DLinkedList<T> * ll = root;
        const DLinkedList<T> * lr = rhs.root;

        while (ll!=NULL && lr!=NULL) {
            if      (ll->key < lr->key) {
                ret.PushBack(ll->key);
                ll = ll->next;
            }
            else if (ll->key > lr->key) {
                ret.PushBack(lr->key);
                lr = lr->next;
            }
            else {
                ret.PushBack(ll->key);
                ll = ll->next;
                lr = lr->next;
            }
        }

        while (ll!=NULL) {
            ret.PushBack(ll->key);
            ll = ll->next;
        }

        while (lr!=NULL) {
            ret.PushBack(lr->key);
            lr = lr->next;
        }

        return ret;
    }

    void Clear() {
       for (int i=0; i<n; ++i) {
            DLinkedList<T> *tmp = root;
            root = root->next;
            delete tmp;
        }
        root = NULL;
        n=0;
    }

    ~Queue() {
       Clear();
    }

};

template <class T> std::ostream & operator<< (std::ostream & os, const Queue<T> & q) {
    const DLinkedList<T> * theel;
    for (theel = q.root; theel!=NULL; theel = theel->next)
        os << theel->key << " ";
    os << std::endl;
    return os;
}


template <class T> struct DeQueue {
    DLinkedList<T*> * tfirst;
    DLinkedList<T*> * tlast;

    int nelem;
    DLinkedList<T*> * selec;

    DeQueue();
    ~DeQueue(); //no destruye las T's

    void PushBack(T * elem);
    void PopFront();
    void DelAndNext();

    bool is_empty()   const;
    bool is_last()    const;
	int  nElements()  const;
    void GoFirst();
    void Next();
    T * GetAndNext();
    T * Get()         const;
};

template <class T> DeQueue<T>::DeQueue() {
    nelem = 0;
    tfirst = NULL;
    tlast = NULL;
    selec = NULL;
}

template <class T> DeQueue<T>::~DeQueue() {
    while(tfirst != NULL) {
        tlast = tfirst;
        tfirst = tfirst->next;
        delete tlast;
    }
}

template <class T> void DeQueue<T>::PushBack(T * elem) {
    if (nelem == 0) {
        tfirst = new DLinkedList<T*>;
        tlast = tfirst;
        selec = tfirst;
        tlast->prev = NULL;
    }
    else {
        tlast->next = new DLinkedList<T*>;
        tlast->next->prev = tlast;
        tlast = tlast->next;
    }

    tlast->key = elem;
    tlast->next = NULL;
    ++nelem;
}

template <class T> void DeQueue<T>::PopFront() {
    if (nelem == 0) return;

    delete tfirst->key;

    if (nelem == 1) {
        delete tfirst;
        tfirst = tlast = selec = NULL;
    }
    else {
        DLinkedList<T*> * todel = tfirst;

        if (selec == tfirst)
            selec = tfirst->next;

        tfirst = tfirst->next;
        tfirst->prev = NULL;
        delete todel;
    }
    --nelem;
}

template <class T> void DeQueue<T>::DelAndNext() {
    if (nelem == 0) return;

    delete selec->key;

    if (nelem == 1) {
        delete tfirst;
        tfirst = tlast = selec = NULL;
    }
    else {
        DLinkedList<T*> * todel = selec;

        if (selec!=tfirst)
            selec->prev->next = selec->next;
		else
			tfirst = selec->next;


        if (selec!=tlast)
            selec->next->prev = selec->prev;
		else
			tlast = selec->prev;

        selec = selec->next;
        delete todel;
    }
    --nelem;
}

template <class T> void DeQueue<T>::GoFirst() {
    selec = tfirst;
}

template <class T> T * DeQueue<T>::GetAndNext() {
    if (selec == NULL)
        return 0;
    T * ret = selec->key;
    selec = selec->next;
    return ret;
}

template <class T> void DeQueue<T>::Next() {
    if (selec != NULL)
        selec = selec->next;
}

template <class T> T * DeQueue<T>::Get() const {
    if (selec == NULL)
        return 0;
    return selec->key;
}

template <class T> bool DeQueue<T>::is_empty() const {
    return(nelem==0);
}

template <class T> bool DeQueue<T>::is_last() const {
    return(selec == NULL);
}

template <class T> int DeQueue<T>::nElements() const {
    return nelem;
}

template <class T> struct AVLTreeNode;
template <class T> void ReBalance( AVLTreeNode<T> * & branch );
template <class T> void RightRot ( AVLTreeNode<T> * & branch );
template <class T> void LeftRot  ( AVLTreeNode<T> * & branch );


template <class T> struct AVLTree {
    int nkeys;
    AVLTreeNode<T> * root;
    T errorkey;

    AVLTree () {
        nkeys = 0;
        root = NULL;
    }

    void SetError(T err) {
        errorkey = err;
    }

    void AddKey( int nnum, T nkey);
    void Delete( int nnum);
    void Dump      ( int * & iarray, T * & tarray);
    void FromArrays( int  *  iarray, T  *  tarray, int n);

    T & operator() ( int nnum);
    AVLTreeNode<T> & operator[] ( int nord);
    void Clear();
    void Plot();
    void Plot2();

    AVLTreeNode<T> & GetFirst();
    AVLTreeNode<T> & GetNext();

    ~AVLTree() {
        Clear();
    }
};

template <class T> void AVLTree<T>::AddKey( int nnum, T nkey) {
    if (root == NULL) {
        root = new AVLTreeNode<T>;
        root->num = nnum;
        root->key = nkey;
        ++nkeys;
    }
    else {
        bool ret = root->AddKey(nnum, nkey);
        if (ret) {
            ReBalance(root);
            ++nkeys;
        }
    }
}

template <class T> void AVLTree<T>::Delete( int nnum) {
    if (root != NULL) {
        bool del = root->Delete(nnum, root);
        if (del) {
            --nkeys;
            if (root!=NULL)
                ReBalance(root);
        }
    }
}

template <class T> T & AVLTree<T>::operator() ( int nnum) {
    if (root == NULL) return errorkey;
    return root->GetKey(nnum, errorkey);
}

template <class T> AVLTreeNode<T> & AVLTree<T>::operator[] ( int ord) {
    return root->GetOrd(ord);
}

template <class T> void AVLTree<T>::Dump( int * & iarray, T * & tarray) {
    if (iarray == NULL) iarray = new  int[nkeys];
    if (tarray == NULL) tarray = new T[nkeys];
    int i = 0;

	if (root != NULL) {
        root->Dump(iarray, tarray, i);
        delete root;
        root = NULL;
        nkeys = 0;
	}
}

template <class T> void AVLTree<T>::FromArrays( int  *  iarray, T  *  tarray, int n) {
    //borra cualquier informacion previa
    Clear();

    nkeys = n;

    if (n>0) {
        root = new AVLTreeNode<T>;
        root->FromArrays( iarray, tarray,  n, 0);
    }
}

template <class T> void AVLTree<T>::Plot() {
    if (root != NULL) root->Plot(0);
}

template <class T> void AVLTree<T>::Plot2() {
    if (root != NULL) root->Plot2(0);
}

template <class T> void AVLTree<T>::Clear() {
    if (root!=NULL) {
        root->Clear();
        delete root;
		root = NULL;
        nkeys = 0;
    }
}

/*
template <class T>
AVLTreeNode<T> & AVLTree<T>::GetFirst() {

}

template <class T>
AVLTreeNode<T> & AVLTree<T>::GetNext() {

}
*/

//se pueden eliminar dos ints
template <class T>  struct AVLTreeNode {
    AVLTreeNode<T> * Left;
    AVLTreeNode<T> * Right;

    int num; //numero por el que se puede buscar
    int ord; //numero de elementos a la izquierda de este

    //la memoria es importante, ya que hay un key por cada producto
    T key;
    char height;
    char balance;

    AVLTreeNode() {
        Left  = NULL;
        Right = NULL;
        height = 0;
        balance = 0;
        ord = 0;
    }

    bool AddKey( int nnum, T nkey);
    bool Delete( int num, AVLTreeNode<T> * & th);
    void DelSwap (AVLTreeNode<T> * tos, AVLTreeNode<T> * & th);
    void DelSwapR(AVLTreeNode<T> * tos, AVLTreeNode<T> * & th);
    void DelSwapL(AVLTreeNode<T> * tos, AVLTreeNode<T> * & th);
    void SetHeight();
    void Dump      ( int * iarray, T * tarray,  int & ival);
    void FromArrays( int * iarray, T * tarray,  int n, int h);
    T & GetKey ( int nnum, T & errorkey);
    AVLTreeNode<T> & GetOrd ( int nord);
    void Clear();
    void Plot(int c);
    void Plot2(int c);
};

//rebalancea una subrama (simple o doble rotacion)
//devuelve el nuevo puntero a la cabeza de la subrama
template <class T> void ReBalance( AVLTreeNode<T> * & branch ) {
    if (branch->balance == 2) {
        //rotacion doble RL
        if (branch->Right->balance == -1)
            LeftRot(branch->Right);
        //rotacion simple RR
        RightRot(branch);
    }
    else if (branch->balance == -2) {
        //rotacion doble LR
        if (branch->Left->balance == 1)
            RightRot(branch->Left);
        //rotacion simple LL
        LeftRot(branch);
    }
}

//rebalancea una rama derecha y devuelve el nuevo puntero
template <class T> void RightRot( AVLTreeNode<T> * & branch ) {
    branch->Right->ord += branch->ord + 1;
    AVLTreeNode<T> * pivot = branch->Right;
    branch->Right = pivot->Left;
    pivot->Left = branch;
    branch->SetHeight();
    pivot->SetHeight();
    branch = pivot;
}

//rebalancea una rama izquierda y devuelve el nuevo puntero
template <class T> void LeftRot( AVLTreeNode<T> * & branch ) {
    branch->ord -= branch->Left->ord + 1;
    AVLTreeNode<T> * pivot = branch->Left;
    branch->Left = pivot->Right;
    pivot->Right = branch;
    branch->SetHeight();
    pivot->SetHeight();
    branch = pivot;
}

template <class T> bool AVLTreeNode<T>::AddKey( int nnum, T nkey) {
     //evalua a 0 para no punteros
     if (nnum<num) {
         if (Left  == NULL) {
             Left  = new AVLTreeNode();
             Left->num = nnum;
             Left->key = nkey;
			 SetHeight();
             ++ord;
             return true;
         }
         else {
             if (Left->AddKey(nnum, nkey)) {
                 ReBalance(Left);
                 SetHeight();
                 ++ord;
                 return true;
             }
             else
                 return false;
         }
     }
     else if (nnum>num) {
         if (Right == NULL) {
             Right = new AVLTreeNode();
             Right->num = nnum;
             Right->key = nkey;
			 SetHeight();
             return true;
         }
         else {
             if (Right->AddKey(nnum, nkey)) {
                 ReBalance(Right);
                 SetHeight();
                 return true;
             }
             else
                 return false;
         }
     }
     else
         return false;

}

template <class T> bool AVLTreeNode<T>::Delete( int nnum, AVLTreeNode<T> * & th) {
    if (nnum == num) {
        DelSwap(this, th);
        return true;
    }
    else if ((nnum<num) && (Left!=NULL)) {
        bool  ret = Left->Delete(nnum, Left);
        if (ret) {
            if (Left != NULL)
                ReBalance(Left);
            SetHeight();
            --ord;
        }
        return ret;
    }
    else if ((nnum>num) && (Right!=NULL)) {
        bool ret = Right->Delete(nnum, Right);
        if (ret) {
            if (Right != NULL)
                ReBalance(Right);
            SetHeight();
        }
        return ret;
    }
    else
        return false; //no existia el elemento
}

//encuentra el elemento mas cercano, lo intercambia con el entrante y lo elimina
template <class T> void AVLTreeNode<T>::DelSwap(AVLTreeNode<T> * tos, AVLTreeNode<T> * & th) {
    if (Right!=NULL) {
        Right->DelSwapL(this, Right);

        if (Right != NULL)
            ReBalance(Right);

        SetHeight();
    }
    else if (Left!=NULL) {
        Left->DelSwapR(this, Left);

        if (Left != NULL)
            ReBalance(Left);

        SetHeight();
        --ord;
    }
    else {
        tos->num = num;
        tos->key = key;
        th = NULL;
        delete this;
    }
}

//elimina el elemento mas a la derecha de una rama, reemplazando los datos por el elemento *par
template <class T> void AVLTreeNode<T>::DelSwapR(AVLTreeNode<T> * tos, AVLTreeNode<T> * & th) {
    if (Right!=NULL) {
        Right->DelSwapR(tos, Right);

        if (Right != NULL)
            ReBalance(Right);

        SetHeight();
    }
    else {
        tos->num = num;
        tos->key = key;

        if (Left!=NULL) {
           Left->DelSwap(this, Left);

           if (Left != NULL)
               ReBalance(Left);

           SetHeight();
           --ord;
        }
        else {
            th = NULL;
            delete this;
        }
    }
}

//elimina el elemento mas a la derecha de una rama, reemplazando los datos por el elemento *par
template <class T> void AVLTreeNode<T>::DelSwapL(AVLTreeNode<T> * tos, AVLTreeNode<T> * & th) {
    if (Left!=NULL) {
        Left->DelSwapL(tos, Left);

        if (Left != NULL)
            ReBalance(Left);

        SetHeight();
        --ord;
    }
    else {
        tos->num = num;
        tos->key = key;

        if (Right!=NULL) {
           Right->DelSwap(this, Right);

           if (Right != NULL)
               ReBalance(Right);

           SetHeight();
        }
        else {
            th = NULL;
            delete this;
        }
    }
}

//calcula la nueva altura y balance
template <class T> void AVLTreeNode<T>::SetHeight() {
    char hr = (Right==NULL)?(0):(Right->height+1);
    char hl = (Left ==NULL)?(0):(Left->height +1);
    height = (hr>hl)?hr:hl;
    balance = hr-hl;
}

template <class T> T & AVLTreeNode<T>::GetKey ( int nnum, T & errorkey) {
    if (nnum > num){
        if (Right!=NULL)
			return Right->GetKey(nnum, errorkey);
		else
			return errorkey;
    }
    else if (nnum < num){
        if (Left!=NULL)
			return  Left->GetKey(nnum, errorkey);
		else
			return errorkey;
    }
    else // (num == nnum)
        return key;

}

template <class T> AVLTreeNode<T> & AVLTreeNode<T>::GetOrd ( int nord) {
    if (nord > ord) {
        if (Right!=NULL) return Right->GetOrd(nord-ord-1);
    }
    else if (nord < ord) {
        if (Left!=NULL)  return Left->GetOrd(nord);
    }
    return (*this);
}

template <class T> void AVLTreeNode<T>::Clear() {
    if (Right!=NULL) {
        Right->Clear();
        delete Right;
    }
    if (Left!=NULL) {
        Left->Clear();
        delete Left;
    }
}

template <class T> void AVLTreeNode<T>::Dump( int * iarray, T * tarray, int & ival) {
    if (Left!=NULL) {
        Left->Dump(iarray, tarray, ival);
        delete Left;
    }

    iarray[ival] = num;
    tarray[ival] = key;
    ++ival;

    if (Right!=NULL) {
        Right->Dump(iarray, tarray, ival);
        delete Right;
    }
}

template <class T> void AVLTreeNode<T>::FromArrays( int * iarray, T * tarray,  int n, int h) {
    int nh = n/2;

    num = iarray[nh];
    ord = nh-1;

    key = tarray[nh];
    height = h;
    balance = 0;

    if (nh>0) {
        Left  = new AVLTreeNode<T>;
        Left->FromArrays(iarray,       tarray,      nh    , h+1);
        ++balance;
    }
    if (n-nh > 1) {
        Right = new AVLTreeNode<T>;
        Right->FromArrays(iarray+nh+1, tarray+nh+1, n-nh-1, h+1);
        --balance;
    }
}

/*
template <class T>
void AVLTreeNode<T>::Plot(int c) {
    Messenger << c+ord << " " << num << " " << key << "    " << int(balance) << "  " << int(height) << "    ";

    if (Left!=NULL)
        Messenger << Left->num << "  ";
    else
        Messenger << "   ";

    if (Right!=NULL)
        Messenger << Right->num << "  ";
    else
        Messenger << "   ";

    Messenger << endl;

    if (Left!=NULL)
        Left->Plot(c);

    if (Right!=NULL)
        Right->Plot(c+ord+1);
}

template <class T>
void AVLTreeNode<T>::Plot2(int c) {
    if (Left!=NULL)
        Left->Plot2(c);

    cout << c+ord << " " << num << " " << key << endl;

    if (Right!=NULL)
        Right->Plot2(c+ord+1);
}
*/

///////////////////////////////////////////////////////////////////////////////////////////////////

template <class T>
struct SkipList {
    int ** nums;
    T    * keys;
    bool * even; //si la lista es par o impar

    int nkeys;
    char levels;

    T errorkey;

    SkipList() {nkeys=0;}
    void SetError(T error) {errorkey = error;}
    void FromAVLTree(AVLTree<T> & tree);
    T & operator() (int nnum);
    void Clear();
};

template <class T>
void SkipList<T>::FromAVLTree(AVLTree<T> & tree) {
    int tmp;
    nkeys = tree.nkeys;

    //calcula los niveles necesarios
    levels = 0;
    tmp = nkeys;
    while(tmp>0) {
        tmp = (tmp)/2;
        ++levels;
    }

	if (nkeys == 1) {
	    nums = new  int*[1];
        tree.Dump(nums[0], keys);
		return;
	}

    --levels;
    even = new bool[levels];
    nums = new int*[levels];

	//nivel base
    tree.Dump(nums[levels-1], keys);
    even[levels-1] = (nkeys%2 == 1);

	//resto de niveles
    tmp = nkeys;
    for (int i=levels-2; i>=0; --i) {
        tmp = tmp/2;
		even[i] = (tmp%2 == 1); //lista completa (numero par de elementos)
        nums[i] = new  int[tmp];

        //copia la mitad de valores a la lista superior
        for (int p=0; p<tmp; ++p)
            nums[i][p] = nums[i+1][2*p];
    }
}

template <class T>
T & SkipList<T>::operator() (int nnum) {

	//caso unico
	if (nkeys == 1) {
	    if (nnum == nums[0][0])
			return keys[0];
		else
			return errorkey;
	}

	//ejecucion normal
     int s = 0;
     int maxs = 1;

	for (int p=0; p<levels; ++p) {
        s *= 2;
		maxs *= 2;
		if (even[p]) ++maxs;

		if (even[p] && (s+3 == maxs) && (nnum >= nums[p][s+2]))
            s+=2;
		else if (nnum >= nums[p][s+1])
		    s+=1;
		else if (nnum >= nums[p][s]) { }

		else
		    return errorkey;
	}

    if (nums[levels-1][s] == nnum)
        return keys[s];
    else
        return errorkey;
}

template <class T>
void SkipList<T>::Clear() {
    if (nkeys==0) return;

    nkeys = 0;
    levels = 0;

    delete[] even;

    for (int i=0; i<levels; ++i)
        delete[] nums[i];

    delete[] nums;
    delete[] keys;
}



template <class T>
struct BiList {
    T   * keys;
    int * nums;

    int nkeys;
    T errorkey;

    BiList() {nkeys=0;}
    void SetError(T error) {errorkey = error;}

    void FromAVLTree(AVLTree<T> & tree);
    void FromArrays(int * onums, T * okeys, int onum);

    T & operator() (int nnum);
    void Clear();
};


template <class T>
T & BiList<T>::operator() (int nnum){
    int nmin = 0;
    int nmax = nkeys-1;

    int n;

    while(nmin <= nmax) {
        n = (nmin+nmax)/2;
        //elemento encontrado
        if (nums[n] == nnum)
			return keys[n];

        //elemento  no encontrado
        if (nmin == nmax)
			return errorkey;

        if (nnum < nums[n]) nmax = n-1;
        else                nmin = n+1;
    }

    //fuera del bound
    return errorkey;
}

template <class T>
void BiList<T>::FromAVLTree (AVLTree<T> & tree) {
    nkeys = tree.nkeys;
    tree.Dump(nums, keys);
}

template <class T>
void BiList<T>::FromArrays(int * onums, T * okeys, int onum) {
    nums  = onums;
    keys  = okeys;
    nkeys = onum;
}

template <class T>
void BiList<T>::Clear() {
    if (nkeys>0) {
        delete[] keys;
        delete[] nums;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template <class X, class Y>
struct TriList {
    X   * Xkeys;
    Y   * Ykeys;
    int * nums;

    X Xerrorkey;
    Y Yerrorkey;

    int nkeys;

    TriList() {nkeys=0;}
    void SetError(X Xerror, Y Yerror) {Xerrorkey = Xerror; Yerrorkey = Yerror;}
    void FromArrays(int * onums, X * oXkeys, Y * oYkeys, int onum);
    void MergeWith(TriList<X,Y> & tom);

    X & operator() (int nnum);
    Y & operator[] (int nnum);
    void Clear();
};


template <class X, class Y>
X & TriList<X,Y>::operator() (int nnum){
    int nmin = 0;
    int nmax = nkeys-1;

    int n;

    while(nmin <= nmax) {
        n = (nmin+nmax)/2;
        //elemento encontrado
        if (nums[n] == nnum)
			return Xkeys[n];

        //elemento  no encontrado
        if (nmin == nmax)
			return Xerrorkey;

        if (nnum < nums[n]) nmax = n-1;
        else                nmin = n+1;
    }

    //fuera del bound
    return Xerrorkey;
}

template <class X, class Y>
Y & TriList<X,Y>::operator[] (int nnum){
    int nmin = 0;
    int nmax = nkeys-1;

    int n;

    while(nmin <= nmax) {
        n = (nmin+nmax)/2;
        //elemento encontrado
        if (nums[n] == nnum)
			return Ykeys[n];

        //elemento  no encontrado
        if (nmin == nmax)
			return Yerrorkey;

        if (nnum < nums[n]) nmax = n-1;
        else                nmin = n+1;
    }

    //fuera del bound
    return Yerrorkey;
}


template <class X, class Y>
void TriList<X,Y>::FromArrays(int * onums, X * oXkeys, Y * oYkeys, int onum) {
    nums   = onums;
    Xkeys  = oXkeys;
    Ykeys  = oYkeys;
    nkeys  = onum;
}

template <class X, class Y>
void TriList<X,Y>::MergeWith(TriList<X,Y> & tom) {

    int nnkeys = nkeys + tom.nkeys;

    int * nnums  = new int[nnkeys];
    X   * nXkeys = new   X[nnkeys];
    Y   * nYkeys = new   Y[nnkeys];

    int n1 = 0;
    int n2 = 0;

    for (int nt=0; nt<nnkeys; ++nt) {
        if (n1 == nkeys) {
            nnums[nt]  = tom.nums[n2];
            nXkeys[nt] = tom.Xkeys[n2];
            nYkeys[nt] = tom.Ykeys[n2];
            ++n2;
        }
        else if (nums[n1] > tom.nums[n2]){
            nnums[nt]  = tom.nums[n2];
            nXkeys[nt] = tom.Xkeys[n2];
            nYkeys[nt] = tom.Ykeys[n2];
            ++n2;
        }
        else if (n2 == tom.nkeys) {
            nnums[nt]  = nums[n1];
            nXkeys[nt] = Xkeys[n1];
            nYkeys[nt] = Ykeys[n1];
            ++n1;
        }
        else {  //if (nums[n1] <= tom.nums[n2])
            nnums[nt]  = nums[n1];
            nXkeys[nt] = Xkeys[n1];
            nYkeys[nt] = Ykeys[n1];
            ++n1;
        }
    }


    //reemplaza los tensores
    Clear();
    tom.Clear();

    nkeys = nnkeys;
    nums  = nnums;
    Xkeys = nXkeys;
    Ykeys = nYkeys;
}

template <class X, class Y>
void TriList<X,Y>::Clear() {
    if (nkeys>0) {
        nkeys = 0;
        delete[] Xkeys;
        delete[] Ykeys;
        delete[] nums;
    }
}


//linked list desenrrollada: mejora la localidad de cache y
//reduce memoria
template <class T, int N>
struct ULinkedList {
    int nblocks;
    int bnkey;

    LinkedList<T*> * List;
    LinkedList<T*> * Last;

    ULinkedList<T, N> () {
        nblocks = -1;
        bnkey = 0;
        Last = NULL;
        List = NULL;
    }

    void Push (T & val) {
        if (bnkey == N)
            bnkey = 0;

        if (bnkey == 0) {
            if (nblocks == -1) {
                List = new LinkedList<T*>;
                Last = List;
            }
            else {
                Last->next = new LinkedList<T*>;
                Last = Last->next;
            }
            Last->key = new T[N];
			Last->next = NULL;
			++nblocks;
        }

        Last->key[bnkey] = val;
        ++bnkey;
    }

    //vuelca a un vector y borra el anterior
    void Dump (int & nkeys, T * & tarray) {
        nkeys = N*nblocks + bnkey;
        if (nkeys<1) {
            nkeys = 0;
            return;
        }
        tarray = new T[nkeys];

        for (int p=0; p<nblocks; ++p) {
            for (int i=0; i<N; ++i)
                tarray[p*N + i] = List->key[i];
            Last = List;
            List = List->next;
			delete[] Last->key;
            delete Last;
        }

        for (int i=0; i<bnkey; ++i)
            tarray[N*nblocks + i] = List->key[i];

		delete[] List->key;
        delete List;

        nblocks = -1;
        bnkey = 0;
        Last = NULL;
        List = NULL;
    }
};
#endif
