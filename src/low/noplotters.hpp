
/*
    DUMMY CLASS
*/

#ifndef __PLOTTERS__
#define __PLOTTERS__

#include <iostream>
#include <string>

class MessagePlotter {
    template<class T>
    friend MessagePlotter & operator<< (MessagePlotter & MP, const T & out);

  public:
    MessagePlotter() {};
   ~MessagePlotter() {};
    void SetFile(const std::string & filename) {};
    void Init() {};

    void SetMaxDepth(int iDepth) {};
    void Push() {};
    void Pop(bool same=false) {};
    int TotDepth() const {return 0;};
    void precision(int p) {};
    void width(int p) {};
    void Out(const char * c) const {};
    void SetModule(const std::string & rhs) {};
    void SetParent(MessagePlotter * parent) {};
    void Disable() {};
    void Enable()  {};
    bool CheckStatus() const {return true;};
    void NewLine()  {};
    template<class T> void Print(const T & out) {}
    void Print(const std::string & out) {};
    void Print(const char * out) {};
    void Print(double out) {};
    MessagePlotter & operator<<(std::ostream & (*f)(std::ostream & ) ) {return *this;};
};

template<class T> MessagePlotter & operator<< (MessagePlotter & MP, const T & out) {return MP;}

#endif
