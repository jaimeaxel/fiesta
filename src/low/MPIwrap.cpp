#ifdef C_MPI
  #include <mpi.h>
#endif

#ifdef _OPENMP
  #include <omp.h>
#else
  #define omp_get_thread_num() 0
  #define omp_get_max_threads() 1
  #define omp_get_num_threads() 1
#endif

#include <iostream>
#include <cstring>
#include <unistd.h>

#include "low/MPIwrap.hpp"
#include "low/chrono.hpp"

using namespace std;

// private implementation
class pProcessGroup {
    friend class ProcessGroup;

  private:
  #ifdef C_MPI
    MPI_Comm  ParentComm;
    MPI_Comm  NodesMastersComm;
    MPI_Comm  IntraNodeComm;

    int nmProcs; // processes in the active communicator
    int inRank;  // intranode rank

    int nParent;
    int nNodes;
    int nIntra;

    int rParent;
    int rNodes;
    int rIntra;

    int nThreads; // number of OMP threads
  #endif

    Chronometer chrono;

  public:
    pProcessGroup();
   ~pProcessGroup();

    void Init ();
    void End  ();

  #ifdef C_MPI
    void Init (MPI_Comm* comm_ptr);
    void End  (MPI_Comm* comm_ptr);
  #endif

  #ifdef C_MPI
    void InitHybrid   (MPI_Comm mpicomm, bool contiguous); // Init hybrid MPI+OpenMP scheme
  #endif

    //bool Run          (); // test if process is included
    void Barrier      (); // merge processes that form part of the group
                                // with processes that are spinning
    int  GetRank      () const;
    void GetTime      () const;

    int  GetNodeProcs () const;
    int  GetThreads   () const;
    int  GetTotalNodes() const;


    bool IsMaster    () const;
    bool CheckMod    (int n) const;
    void Sync        () const;
    void Broadcast   (int & val);
    void Broadcast   (long long int & val);
    void Broadcast   (double & val);
    void Broadcast   (int * ival, long long int len);
    void Broadcast   (double * pval, long long int len);
    void Reduce      (double *  val, long long int len);
    void Reduce      (double * ival, double * oval, long long int len);
    void AllReduce   (double *  val, long long int len);
    void AllReduce   (double * ival, double * oval, long long int len);
    void AllGather   (double * ival, long long int ilen, double * oval, long long int olen);
};


#ifdef C_MPI

void mpiCommSplitShared   (MPI_Comm pComm,  MPI_Comm & nComm,  MPI_Comm & iComm) {

    // this groups all processes within the same node
    MPI_Comm_split_type (pComm, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &iComm);

    // gather all iComm ranks
    int procs;
    MPI_Comm_size  (pComm, &procs);

    int irank, prank;
    MPI_Comm_rank(iComm, &irank);
    MPI_Comm_rank(pComm, &prank);

    int nranks[procs];

    MPI_Allgather ( &irank, 1, MPI_INT, nranks, 1, MPI_INT, pComm);

    // all processes must execute this, even if they don't participate in the group
    {
        // overwrite the node rank matrix with parent master ranks
        int n=0;
        for (int i=0; i<procs; ++i)
            if (nranks[i]==0) {
                nranks[n] = i;
                ++n;
            }

        // extract the parent group, split it, and make a communicator
        MPI_Group nGroup, pGroup;

        MPI_Comm_group   (pComm, &pGroup);
        MPI_Group_incl   (pGroup, n, nranks, &nGroup);
        MPI_Comm_create  (pComm,  nGroup, &nComm);

        MPI_Group_free   (&nGroup);
        MPI_Group_free   (&pGroup);
    }
}

void mpiCommUnique          (MPI_Comm pComm,  MPI_Comm & uComm) {

    MPI_Group pGroup, uGroup;

    // create an mpi group with only current process
    int urank;

    MPI_Comm_group (pComm, &pGroup);
    MPI_Comm_rank  (pComm, &urank);

    MPI_Group_incl  (pGroup, 1, &urank, &uGroup);
    MPI_Comm_create (pComm,  uGroup, &uComm);

    // free the copies
    MPI_Group_free   (&pGroup);
    MPI_Group_free   (&uGroup);
}

pProcessGroup::pProcessGroup() {
    // NULL communicators
    NodesMastersComm = MPI_COMM_NULL;
    IntraNodeComm    = MPI_COMM_NULL;

    // get the number of threads OMP has been configured with
    #pragma omp parallel
    if (omp_get_thread_num()==0) nThreads = omp_get_num_threads();

    #ifdef _OPENMP
    omp_set_num_threads(1);
    #endif
}

pProcessGroup::~pProcessGroup() {}

// this works with Barrier;
// processes that return false should not run anything until Barrier
/*
bool pProcessGroup::Run() {
    chrono.Restart();
    chrono.Start();
    #ifdef _OPENMP
    omp_set_num_threads(nThreads);
    #endif
    if (NodesMastersComm != MPI_COMM_NULL) {
        return true;
    }
    else return false;
}
*/

void pProcessGroup::Barrier() {

    // objects for communication
    MPI_Request  request;
    MPI_Status   status;
    int flag = 0;
    int buf  = -1;   // nothing of interest
    int tag  = 8398; // arbitrary
    int source = 0;  // source is id=0 in the communicator

    // check if there's only one MPI processs
    if (nIntra==1) {
        chrono.Stop();
        return;
    }

    // BROADCAST NONBLOCKING
    // MPI_Ibcast is part of the MPI 3 standard; current implementation uses MPI 2 compatible calls
    // MPI_Ibcast ((void*)&buf, 1, MPI_INT, source, IntraNodeComm, &request);

    if (rIntra==0) {
        for (int n=1; n<nIntra; ++n)
            MPI_Isend (&buf, 1, MPI_INT, n, tag, IntraNodeComm, &request);
    }
    else {
        MPI_Irecv (&flag, 1, MPI_INT, 0, tag, IntraNodeComm, &request);
    }

    // the processes not belonging to the group spin-wait for the node master to send a signal
    if (rIntra>0) {
        MPI_Test (&request, &flag, &status);
        while (flag==0) {
            usleep(25000); //sleep for 25 milliseconds
            MPI_Test (&request, &flag, &status);
        }
    }

    // synchronize all processes
    MPI_Wait (&request,        &status);

    #ifdef _OPENMP
    omp_set_num_threads(1);
    #endif

    chrono.Stop();
}

void pProcessGroup::InitHybrid   (MPI_Comm mpicomm, bool nested) {

    ParentComm = mpicomm;

    try {
        mpiCommSplitShared (mpicomm, NodesMastersComm, IntraNodeComm);
    }
    catch(string s) {
        cout << s << endl;
        return;
    }

    // notify the wait-spinning threads which is the rank
    // of the node master within the working group
    inRank = GetRank();
    if (inRank!=-1) MPI_Comm_size  (NodesMastersComm, &nmProcs);
    MPI_Bcast (&inRank , 1, MPI_INT, 0, IntraNodeComm); // broadcasts the rank of the node master to the rest of the node
    MPI_Bcast (&nmProcs, 1, MPI_INT, 0, IntraNodeComm); // broadcasts the number of nodes to the node slaves

    nNodes = nmProcs;
    rNodes = inRank;

    // set communicator dimensions
    MPI_Comm_size  (      mpicomm, &nParent);
    MPI_Comm_size  (IntraNodeComm, &nIntra);

    // set ranks
    MPI_Comm_rank  (      mpicomm, &rParent);
    MPI_Comm_rank  (IntraNodeComm, &rIntra);
}

void pProcessGroup::Init () {
    MPI_Init(NULL,NULL);
    NodesMastersComm = ParentComm = MPI_COMM_WORLD;
    mpiCommUnique(NodesMastersComm, IntraNodeComm);
    inRank = GetRank();
    MPI_Comm_size  (NodesMastersComm, &nmProcs);
    nNodes = nmProcs;
    rNodes = inRank;
}

void pProcessGroup::Init (MPI_Comm* comm_ptr) {
    NodesMastersComm = ParentComm = *comm_ptr;
    mpiCommUnique(NodesMastersComm, IntraNodeComm);
    inRank = GetRank();
    MPI_Comm_size  (NodesMastersComm, &nmProcs);
    nNodes = nmProcs;
    rNodes = inRank;
}

void pProcessGroup::End() {
    Sync();
    if (NodesMastersComm != MPI_COMM_WORLD) MPI_Comm_free(&NodesMastersComm);
    if (IntraNodeComm    != MPI_COMM_NULL)  MPI_Comm_free(&IntraNodeComm);
    MPI_Finalize();
}

void pProcessGroup::End(MPI_Comm* comm_ptr) {
    Sync();
    if (NodesMastersComm != *comm_ptr)     MPI_Comm_free(&NodesMastersComm);
    if (IntraNodeComm    != MPI_COMM_NULL) MPI_Comm_free(&IntraNodeComm);
}

int pProcessGroup::GetRank() const {
    if (NodesMastersComm==MPI_COMM_NULL) return -1;
    int nrank;
    MPI_Comm_rank  (NodesMastersComm, &nrank);
    return nrank;
}

void pProcessGroup::GetTime() const {

    if (NodesMastersComm != MPI_COMM_NULL) {
        int nnodes, nrank;
        MPI_Comm_size  (NodesMastersComm, &nnodes);
        MPI_Comm_rank  (NodesMastersComm, &nrank);
        double time = chrono.GetTotalTime();
        double * times = NULL;
        if (nrank==0) times = new double[nnodes];

        MPI_Gather (&time,       1, MPI_DOUBLE,
                     times,      1, MPI_DOUBLE,
                     0, NodesMastersComm);

        if (nrank==0) {
            cout << "Elapsed times: " << endl;
            for (int n=0; n<nnodes; ++n) cout << "  " << times[n] << " seconds" << endl;
            delete[] times;
        }
    }

}

bool pProcessGroup::IsMaster() const {
    return (GetRank()==0);
}

int  pProcessGroup::GetNodeProcs () const {
    return nIntra;
}

int pProcessGroup::GetTotalNodes() const {
    return nNodes;
}

int pProcessGroup::GetThreads() const {
    return nThreads;
}

// this is used to evely split tasks between MPI processes without communication
bool pProcessGroup::CheckMod(int n) const {
    return ((n%nNodes) == rNodes);
}

void pProcessGroup::Sync() const {
    MPI_Barrier(NodesMastersComm);
}

void pProcessGroup::Broadcast(int & val) {
     MPI_Bcast (&val, 1, MPI_INT, 0, NodesMastersComm);
}

void pProcessGroup::Broadcast(long long int & val) {
     MPI_Bcast (&val, 1, MPI_LONG_LONG_INT , 0, NodesMastersComm);
}

void pProcessGroup::Broadcast(double & val) {
    MPI_Bcast (&val, 1, MPI_DOUBLE, 0, NodesMastersComm);
}

void pProcessGroup::Broadcast(int * ival, long long int len) {

    static const int MXL = 128*1024*1024; // 2^30 bytes
    while (len>MXL) {
        MPI_Bcast (ival, MXL, MPI_INT, 0, NodesMastersComm);
        ival += MXL;
        len  -= MXL;
    }
    int len32 = len;
    MPI_Bcast (ival, len32, MPI_INT, 0, NodesMastersComm);
}

void pProcessGroup::Broadcast(double * pval, long long int len) {

    static const int MXL = 128*1024*1024; // 2^30 bytes
    while (len>MXL) {
        MPI_Bcast (pval, MXL, MPI_DOUBLE, 0, NodesMastersComm);
        pval += MXL;
        len  -= MXL;
    }
    int len32 = len;
    MPI_Bcast (pval, len32, MPI_DOUBLE, 0, NodesMastersComm);
}

void pProcessGroup::Reduce(double * val, long long int len) {

    static const int MXL = 128*1024*1024; // 2^30 bytes
    while (len>MXL) {
        if (NodeGroup.IsMaster()) {
            MPI_Reduce(MPI_IN_PLACE, val, MXL, MPI_DOUBLE, MPI_SUM,0,NodesMastersComm);
        } else {
            MPI_Reduce(val, val, MXL, MPI_DOUBLE, MPI_SUM,0,NodesMastersComm);
        }
        val += MXL;
        len -= MXL;
    }
    int len32 = len;
    if (NodeGroup.IsMaster()) {
        MPI_Reduce(MPI_IN_PLACE, val, len32, MPI_DOUBLE, MPI_SUM,0,NodesMastersComm);
    } else {
        MPI_Reduce(val, val, len32, MPI_DOUBLE, MPI_SUM,0,NodesMastersComm);
    }
}

void pProcessGroup::Reduce(double * ival, double * oval, long long int len) {

    static const int MXL = 128*1024*1024; // 2^30 bytes
    while (len>MXL) {
        MPI_Reduce(ival, oval, MXL, MPI_DOUBLE, MPI_SUM,0,NodesMastersComm);
        ival += MXL;
        oval += MXL;
        len  -= MXL;
    }
    int len32 = len;
    MPI_Reduce(ival, oval, len32, MPI_DOUBLE, MPI_SUM,0,NodesMastersComm);
}

void pProcessGroup::AllReduce(double * val, long long int len) {

    static const int MXL = 128*1024*1024; // 2^30 bytes
    while (len>MXL) {
        MPI_Allreduce(MPI_IN_PLACE, val, MXL, MPI_DOUBLE, MPI_SUM, NodesMastersComm);
        val += MXL;
        len  -= MXL;
    }
    int len32 = len;
    MPI_Allreduce(MPI_IN_PLACE, val, len32, MPI_DOUBLE, MPI_SUM, NodesMastersComm);
}

void pProcessGroup::AllReduce(double * ival, double * oval, long long int len) {

    static const int MXL = 128*1024*1024; // 2^30 bytes
    while (len>MXL) {
        MPI_Allreduce(ival, oval, MXL, MPI_DOUBLE, MPI_SUM, NodesMastersComm);
        ival += MXL;
        oval += MXL;
        len  -= MXL;
    }
    int len32 = len;
    MPI_Allreduce(ival, oval, len32, MPI_DOUBLE, MPI_SUM, NodesMastersComm);
}

void pProcessGroup::AllGather (double * ival, long long int ilen, double * oval, long long int olen) {

    int N = GetTotalNodes();
    int R = GetRank();

    int counts[N];

    int len32 = ilen;

    // scatter the counts
    // The receive count argument in MPI_Allgather is per process, not total.
    MPI_Allgather(&len32, 1, MPI_INT, counts, 1, MPI_INT, NodesMastersComm);

    // count offsets
    int offset[N];
    offset[0] = 0;
    for (int n=0; n<N-1; ++n) offset[n+1] = counts[n] + offset[n];

    MPI_Allgatherv (ival, len32, MPI_DOUBLE,
                    oval, counts, offset, MPI_DOUBLE,
                    NodesMastersComm);

}

#else

pProcessGroup::pProcessGroup()  {}
pProcessGroup::~pProcessGroup() {}
void pProcessGroup::Init       () {}
void pProcessGroup::End        () {}
void pProcessGroup::GetTime() const {
    double time = chrono.GetTotalTime();
    cout << "Elapsed times: " << endl;
    cout << "  " << time << " seconds" << endl;
}
//bool pProcessGroup::Run()     {chrono.Start();}
void pProcessGroup::Barrier() {chrono.Stop();}
int  pProcessGroup::GetRank()       const {return 0;}
bool pProcessGroup::IsMaster()      const {return true;}
bool pProcessGroup::CheckMod(int n) const {return true;}
int  pProcessGroup::GetNodeProcs()  const {return 1;}
int  pProcessGroup::GetThreads()    const {return 1;}
void pProcessGroup::Sync()          const {}
int  pProcessGroup::GetTotalNodes() const {return 1;}
void pProcessGroup::Broadcast(int & val) {}
void pProcessGroup::Broadcast(long long int & val) {}
void pProcessGroup::Broadcast(double & val) {}
void pProcessGroup::Broadcast(int * ival, long long int len) {}
void pProcessGroup::Broadcast(double * pval, long long int len) {}
void pProcessGroup::Reduce    (double *  val,                long long int len) {} // in == out
void pProcessGroup::Reduce    (double * ival, double * oval, long long int len) {memcpy((void*)oval, (void*)ival, len*sizeof(double));}
void pProcessGroup::AllReduce (double *  val,                long long int len) {} // in == out
void pProcessGroup::AllReduce (double * ival, double * oval, long long int len) {Reduce(ival, oval, len);}
void pProcessGroup::AllGather (double * ival, long long int ilen, double * oval, long long int olen) {memcpy((void*)oval, (void*)ival, olen*sizeof(double));}

#endif


ProcessGroup::ProcessGroup() {
    p = new pProcessGroup;
}

ProcessGroup::~ProcessGroup() {
    delete p;
}

// this works with Barrier;
// processes that return false should not run anything until Barrier
/*
bool ProcessGroup::Run() {
    return p->Run();
}
*/

void ProcessGroup::Barrier() {
    p->Barrier();
}

#ifdef C_MPI
void ProcessGroup::InitHybrid   (const void * mpicomm, bool contiguous) {
    const MPI_Comm * comm = (MPI_Comm*)mpicomm;
    p->InitHybrid (*comm, contiguous);
}
#endif

void ProcessGroup::Init         () {
    p->Init();
}

void ProcessGroup::End() {
    p->End();
}

#ifdef C_MPI
void ProcessGroup::Init(MPI_Comm* comm_ptr) {
    p->Init(comm_ptr);
}

void ProcessGroup::End(MPI_Comm* comm_ptr) {
    p->End(comm_ptr);
}
#endif

int ProcessGroup::GetRank() const {
    return p->GetRank();
}

void ProcessGroup::GetTime() const {
    p->GetTime();
}

bool ProcessGroup::IsMaster() const {
    return p->IsMaster();
}

int  ProcessGroup::GetNodeProcs () const {
    return p->GetNodeProcs();
}

int ProcessGroup::GetTotalNodes() const {
    return p->GetTotalNodes();
}

int ProcessGroup::GetThreads() const {
    return p->GetThreads();
}

// this is used to evely split tasks between MPI processes without communication
bool ProcessGroup::CheckMod(int n) const {
    return p->CheckMod(n);
}

void ProcessGroup::Sync() const {
    p->Sync();
}

void ProcessGroup::Broadcast(int & val) {
    p->Broadcast(val);
}

void ProcessGroup::Broadcast(long long int & val) {
    p->Broadcast(val);
}

void ProcessGroup::Broadcast(double & val) {
    p->Broadcast(val);
}

void ProcessGroup::Broadcast(int * ival, long long int len) {
    p->Broadcast(ival, len);
}

void ProcessGroup::Broadcast(double * pval, long long int len) {
    p->Broadcast(pval, len);
}

void ProcessGroup::Reduce    (double * val, long long int len) {
    p->Reduce(val,len);
}

void ProcessGroup::Reduce    (double * ival, double * oval, long long int len) {
    p->Reduce(ival,oval,len);
}

void ProcessGroup::AllReduce (double * val, long long int len) {
    p->AllReduce(val,len);
}

void ProcessGroup::AllReduce (double * ival, double * oval, long long int len) {
    p->AllReduce(ival,oval,len);
}

void ProcessGroup::AllGather   (double * ival, long long int ilen, double * oval, long long int olen) {
    p->AllGather(ival, ilen, oval, olen);
}

// exported symbol
// expected to be a singleton
ProcessGroup NodeGroup;
