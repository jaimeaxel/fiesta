#ifndef __IO__ //se asegura de que no se dupliquen declaraciones
#define __IO__

#include <string>
#include <list>

//estructura de linea de un archivo
struct Line {
    std::string str;
    int nline;
};

// a text file, composed of lines of text
struct FileBuffer {
    std::list<Line> LineArray;

    FileBuffer ();
    FileBuffer (const std::string & file);
    FileBuffer (const std::string * files, int N);
    void Read  (const std::string & file);
    void Read  (const std::string * files, int N);
    void Clear();
};


enum ERRFILE {ERR_NOFILE, ERR_UNMATCHED_COMMENT};

class Molecule;
class AtomDefList;
class tensor2;
class symtensor2;
class symtensor;

void WriteTensor(const std::string & file, tensor2 & array);
void WriteTensor(const std::string & file, symtensor2 & array);
void WriteTensor(const std::string & file, symtensor & array);

void WriteTensorKD(const std::string & file, tensor2 & array);

// read / write matrices in binary format
void WriteTensorBin (const std::string & file, symtensor2 & D);
void ReadTensorBin  (symtensor2 & D, const std::string & file);

void WriteTensorBin (const std::string & file, tensor2 & D);
void ReadTensorBin  (tensor2 & D, const std::string & file);


//mensajes
void PlotRandomJoke();
#endif
