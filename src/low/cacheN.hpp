#ifndef __CACHE_N__
#define __CACHE_N__

#include <iostream>

template <int N> class cachelineN {

  public:
    double d[N];

    inline double & operator()(int i) {
        return d[i];
    }

    inline const double & operator()(int i) const {
        return d[i];
    }

    inline cachelineN & operator=(double rhs) {
        for (int i=0; i<N; ++i) d[i]=rhs;
        return *this;
    }

    /*
    inline void set (const cacheline64 & rhs1, const cacheline64 & rhs2) {
        double v[FLOATS_PER_CACHE_LINE]  __attribute__((aligned(CACHE_LINE_SIZE)));

        for (int i=0;i<DOUBLES_PER_CACHE_LINE; ++i) v[i] = rhs1(i);
        for (int i=0;i<DOUBLES_PER_CACHE_LINE; ++i) v[DOUBLES_PER_CACHE_LINE+i] = rhs2(i);

        for (int i=0; i<MM128_PER_CACHE_LINE; ++i)
            d2[i] = _mm_load_ps(v+4*i);
    }
    */

    inline cachelineN & operator+=(const cachelineN & rhs) {
        for (int i=0; i<N; ++i) d[i] += rhs.d[i];
        return *this;
    }

    inline cachelineN & operator-=(const cachelineN & rhs) {
        for (int i=0; i<N; ++i) d[i] -= rhs.d[i];
        return *this;
    }

    inline cachelineN & operator*=(const cachelineN & rhs) {
        for (int i=0; i<N; ++i) d[i] *= rhs.d[i];
        return *this;
    }

    inline cachelineN & operator*=(double rhs) {
        for (int i=0; i<N; ++i) d[i] *= rhs;
        return *this;
    }


    inline cachelineN operator+(const cachelineN & rhs) const {
        cachelineN ret = *this;
        ret += rhs;
        return ret;
    }

    inline cachelineN operator-(const cachelineN & rhs) const {
        cachelineN ret = *this;
        ret -= rhs;
        return ret;
    }

    inline cachelineN operator*(const cachelineN & rhs) const {
        cachelineN ret = *this;
        ret *= rhs;
        return ret;
    }

    inline cachelineN operator*(double rhs) const {
        cachelineN ret = *this;
        ret *= rhs;
        return ret;
    }

    inline void set (double a) {
        for (int i=0; i<N; ++i) d[i] = a;
    }

    inline void set (double a[N]) {
        for (int i=0; i<N; ++i) d[i] = a[i];
    }

}  __attribute__((aligned(CACHE_LINE_SIZE)));






template<int N>
static inline void store(double * p, const cachelineN<N> & rhs) {
    for (int i=0; i<N; ++i) p[i] = rhs.d[i];
}

template<int N>
static inline void store(cachelineN<N> * p, const cachelineN<N> & rhs) {
    for (int i=0; i<N; ++i) p->d[i] = rhs.d[i];
}

template<int N>
static inline cachelineN<N> loadN(const double * p) {
    cachelineN<N> ret;
    for (int i=0; i<N; ++i) ret.d[i] = p[i];
    return ret;
}

template<int N>
static inline cachelineN<N> load(const cachelineN<N> * p) {
    cachelineN<N> ret;
    for (int i=0; i<N; ++i) ret.d[i] = p->d[i];
    return ret;
}

template<int N>
inline cachelineN<N> sqrt(const cachelineN<N> & rhs) {
    cachelineN<N> ret;
    for (int i=0; i<N; ++i) ret.d[i] = sqrt(rhs.d[i]);
    return ret;
}

template<int N>
inline cachelineN<N> inv(const cachelineN<N> & rhs) {
    cachelineN<N> ret;
    for (int i=0; i<N; ++i) ret.d[i] = 1./rhs.d[i];
    return ret;
}


template<int N>
void PackArrays     (const double * __restrict__  arrays, uint32_t ArraySize, cachelineN<N> * __restrict__  arrayN) {
    for (uint32_t k=0; k<ArraySize; ++k) {
        for (uint32_t n=0; n<N; ++n) {
            arrayN[k].d[n] = arrays[n*ArraySize + k];
        }
    }
}

template<int N>
void UnPackArrays   (const cachelineN<N> * __restrict__ arrayN,  uint32_t ArraySize, double * arrays) {
    for (uint32_t k=0; k<ArraySize; ++k) {
        for (uint32_t n=0; n<N; ++n) {
            arrays[n*ArraySize + k] = arrayN[k].d[n];
        }
    }
}


template<int N>
std::ostream & operator<<(std::ostream & os, const cachelineN<N> & rhs) {
    for (int i=0; i<N; ++i) os << rhs.d[i] << " ";
    return os;
}

#endif
