
#define __PLOTTERS__PLOT__ true

#ifndef __PLOTTERS__ //se asegura de que no se dupliquen declaraciones
#define __PLOTTERS__

#if __PLOTTERS__PLOT__==true
  #include <iostream>
  #include <fstream>
  #include <string>
  #include <stdio.h> //problema con openMP y cout en ICC 10.1
  #include <stack>

  #include "low/chrono.hpp"
  #include "low/MPIwrap.hpp"
#endif

class MessagePlotter {
    template<class T>
    friend MessagePlotter & operator<< (MessagePlotter & MP, const T & out);
    //template<const double*>
    //friend MessagePlotter & operator<< (MessagePlotter & MP, const double * const & out);

  protected:
  #if __PLOTTERS__PLOT__==true
    std::string module;
    MessagePlotter * Link;
    std::stack<Chronometer*> Chronos;
    Chronometer * LastChrono;
    std::ostream * os;
    std::ofstream file;
    bool openfile;

    int Depth;
    int MaxDepth;
    int ExtraDepth;
    int Precision;
    int Width;

    int count;
    int LastPush;

    bool status;
    bool newline;
    bool freshlevel;

    const double * p;
  #endif

  public:

    MessagePlotter();
   ~MessagePlotter();
    void SetFile(const std::string & filename);
    void Init();

    void SetMaxDepth(int iDepth);
    void Push();
    void Push(const std::string & message);
    void Push(int level);
    void Pop();
    void Pop(int level);
    int  TotDepth() const;
    void precision(int p);
    int  width();
    void width(int p);
    void Out(const char * c) const;
    void SetModule(const std::string & rhs);
    void SetParent(MessagePlotter * parent);
    void Disable();
    void Enable();
    bool IsEnabled();
    bool CheckStatus() const;

    void NewLine();

    template<class T> void Print(const T & out) {
      #if __PLOTTERS__PLOT__==true
        NewLine();
        if (newline) for (int i=0; i<TotDepth(); ++i) {(*os) << "  "; ++count;} newline = false;

        int oPrecision = (*os).precision();
        int oWidth     = (*os).width();

        (*os).precision(Precision);
        (*os).width(Width);

        if (p!=NULL) {
            (*os) << (void*)p << "  ";
            p=NULL;
        }

        (*os) << out;

        (*os).precision(oPrecision);
        (*os).width(oWidth);
      #endif
    }

    void Print(const double * v) {
      #if __PLOTTERS__PLOT__==true
      p = v;
      #endif
    }

    void Print(const std::string & out);
    void Print(const char * out);
    void Print(double out);

    MessagePlotter & operator<<(std::ostream & (*f)(std::ostream & ) );
    MessagePlotter & operator()(int level); // set the level of priority for the current message
};

template<> void MessagePlotter::Print<int>(const int & N);

template<class T> MessagePlotter & operator<< (MessagePlotter & MP, const T & out) {

    #if __PLOTTERS__PLOT__==true
    // only print messages to screen and/or file if it is the master MPI process
    if (NodeGroup.IsMaster()) {
        if (MP.CheckStatus())
            MP.Print(out);

        if (MP.Link!=NULL && MP.Link->os != MP.os) *(MP.Link) << out;

        //MP.ExtraDepth = 0;
    }
    #endif

    return MP;
}

// this specializes the previous class for double pointers, which will be dereferenced;
// the code must be accessible everywhere the parent template's is
template<> MessagePlotter & operator<< (MessagePlotter & MP, const double * const & out);

// this specializes the previous class for double pointers, which will be dereferenced;
// the code must be accessible everywhere the parent template's is
template<> MessagePlotter & operator<< (MessagePlotter & MP, double * const & out);

/*
template<class T> MessagePlotter & operator<< (MessagePlotter & MP, T out) {

    #if __PLOTTERS__PLOT__==true
    // only print messages to screen and/or file if it is the master MPI process
    if (NodeGroup.IsMaster()) {
        if (MP.CheckStatus())
            MP.Print(out);

        if (MP.Link!=NULL && MP.Link->os != MP.os) *(MP.Link) << out;

        //MP.ExtraDepth = 0;
    }
    #endif

    return MP;
}
*/

#endif
