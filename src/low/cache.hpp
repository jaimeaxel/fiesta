


#ifndef __CACHE__
#define __CACHE__

#include "defs.hpp"

class cacheline {
  public:
    char d2[CACHE_LINE_SIZE];
}  __attribute__((aligned(CACHE_LINE_SIZE)));

#endif

