#ifndef __LOCKS__
#define __LOCKS__
#include <algorithm>

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
#endif

//simple bakey lock, which does not require syncronization primitives
//space is O(N), but it is ok since the number of threads per node is low (<<2^16) and is still much better than using OpenMP's critical sections
class BakeryLock {
    private:
        //bool * flag;
        //long long int  * label; //this should take care of the unbounded problem

        volatile bool            flag [64];
        volatile long long int  label[64];


    public:
        BakeryLock() {
            //flag  = new bool[omp_get_max_threads()];
            //label = new long long int[omp_get_max_threads()];

            //for (int i=0; i<omp_get_max_threads(); ++i) {
            for (int i=0; i<64; ++i) {
                flag[i] = false;
                label[i] = 0;
            }
        }

        ~BakeryLock() {
            //delete[] flag;
            //delete[] label;
        }

        inline void Lock() {

            int me = omp_get_thread_num();
            int n = omp_get_max_threads();

            flag[me] = true;
            // Acquire ticket
            volatile long long int maxl = label[0];
            for (int i=0; i<n; ++i) maxl = std::max(maxl, label[i]);
            label[me] = maxl+1;

            // Wait until no
            // lower ticket exists
            for (int i=0; i<n; ++i) {
                while(i != me && flag[i] && (label[i] < label[me] || (label[i] == label[me] && i<me)));
            }
        }

        inline void Unlock() {
            int me = omp_get_thread_num();
            flag[me] = false;
        }
};

#endif
