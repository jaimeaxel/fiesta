#ifndef __STRINGS__
#define __STRINGS__
#include <string>
#include <exception>

class Exception: public std::exception {
  protected:
    std::string msg_;
  public:
    explicit Exception (const char        * message):msg_(message) {}
    explicit Exception (const std::string & message):msg_(message) {}
    virtual ~Exception() throw () {}

    virtual const char* what() const throw () {
       return msg_.c_str();
    }
};


double s2d (const std::string & in);
int    s2i (const std::string & in);
bool   s2b (const std::string & in);


std::string cleanstr   (const std::string & in);
bool        is_numeric (const std::string & in);
std::string ucase      (const std::string & str);
char        ucase      (const char & c);

std::string int2str    (int    number);
std::string float2str  (double number);

#endif
