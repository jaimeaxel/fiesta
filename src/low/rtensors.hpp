

#ifndef __RTENSORS__
#define __RTENSORS__

#include <queue>
#include <cstdlib>
#include <algorithm>

template<class T> class r3tensor {

  private:
    T * v3;
    T ** v2;
    T *** v;
    int Nx;
    int Ny;
    int Nz;

  public:
    r3tensor() {
        Nx = Ny = Nz = 0;
        v3 = NULL;
        v2 = NULL;
        v  = NULL;
    }

    int N1() const {
        return Nx;
    }

    int N2() const {
        return Ny;
    }

    int N3() const {
        return Nz;
    }



    void set(int nx, int ny, int nz) {
        if (v3!=NULL) delete[] v3;
        if (v2!=NULL) delete[] v2;
        if (v !=NULL) delete[] v;

        v3 = new T[nx*ny*nz];
        v2 = new T*[nx*ny];
        v  = new T**[nx];

        Nx = nx;
        Ny = ny;
        Nz = nz;

        for (int i=0; i<Nx; ++i) {
            v[i] = v2 + i*Ny;
        }

        for (int i=0; i<Nx; ++i) {
            for (int j=0; j<Ny; ++j) {
                v[i][j] = v3 + (i*Ny+j)*Nz;
            }
        }

    }

    r3tensor(int nx, int ny, int nz) {
        v3 = NULL;
        v2 = NULL;
        v  = NULL;
        set(nx,ny,nz);
    }

    inline const T & operator()(int i, int j, int k) const {
        return v[i][j][k];
    }

    inline T & operator()(int i, int j, int k) {
        return v[i][j][k];
    }

    ~r3tensor() {
        delete[] v3;
        delete[] v2;
        delete[] v;
    }
};

#include <iostream>

template<class T> class r2tensor {

  protected:
    T * v2;
    T ** v;
    int n;
    int m;
    int * mm;  // for tensors with variable number of elements per row (or column)
    bool root; // whether it owns the pointers or not

  public:

    r2tensor() {
        n = m = 0;
        v2 = NULL;
        v  = NULL;
        mm = NULL;
        root = false;
    }

    r2tensor(int nnn, int mmm) {
        v  = NULL;
        v2 = NULL;
        mm = NULL;
        root = false;
        set(nnn,mmm);
    }

    ~r2tensor() {
        clear();
    }

    int N() const {
        return n;
    }

    int M() const {
        return m;
    }

    int M(int i) const {
        return mm[i];
    }

    void clear() {
        if (root) {
            delete[] v2;
            delete[] v;
            delete[] mm;
        }
        v2 = NULL;
        v  = NULL;
        mm = NULL;
    }

    void set(unsigned short int * lens, int nn) {

        clear();
        root = true;

        unsigned int tlen=0;
        for (int i=0; i<nn; ++i) tlen += lens[i];

        v2 = new T[tlen];
        v  = new T*[nn];
        mm = new int[nn];

        v[0] = v2;
        for (int i=0; i<nn-1; ++i) {
            v[i+1] = v[i] + lens[i];
        }

        n  = nn;
        m=0;
        for (int i=0; i<nn; ++i) {
            mm[i] = lens[i];
            m = std::max(m, mm[i]);
        }

    }

    void set(int nnn, int mmm) {

        clear();
        root = true;

        v2 = new T[nnn*mmm];
        v  = new T*[nnn];
        mm = new int[nnn];

        n  = nnn;
        m  = mmm;

        for (int i=0; i<nnn; ++i) v[i] = v2 + i*mmm;
        for (int i=0; i<nnn; ++i) mm[i] = mmm;
    }

    void set(int nnn, int mmm, T**w, T*w2) {

        clear();
        root = false;

        v2 = w2;
        v  = w;
        n  = nnn;
        m  = mmm;

        for (int i=0; i<nnn; ++i) v[i] = v2 + i*mmm;
    }

    inline const T & operator()(int i, int j) const {
        //if ( (i<0) || (i>=n) || (j<0) || (j>=mm[i]) ) std::cout << "tensor2 of dimension " << n <<","<<m << " was accesset with indices " << i << "," <<j << std::endl;
        return v[i][j];
    }

    inline T & operator()(int i, int j) {
        //if ( (i<0) || (i>=n) || (j<0) || (j>=mm[i]) ) std::cout << "tensor2 of dimension " << n <<","<<m << " was accesset with indices " << i << "," <<j << std::endl;
        return v[i][j];
    }

    inline const T * operator[](int i) const {
        //if ( (i<0) || (i>=n) ) std::cout << "tensor2 of dimension " << n <<","<<m << " was accesset with indices " << i << std::endl;
        return v[i];
    }

    inline T * operator[](int i) {
        //if ( (i<0) || (i>=n) ) std::cout << "tensor2 of dimension " << n <<","<<m << " was accesset with indices " << i << std::endl;
        return v[i];
    }

};

template<class T> class r1tensor {

  private:
    T * v;
    int n;

  public:
    r1tensor() {
        v  = NULL;
        n  = 0;
    }

    void set(int nn) {
        delete[] v;
        v  = new T[nn];
        n  = nn;
    }

    r1tensor(int nn) {
        v  = new T[nn];
        n  = nn;
    }

    int N() const {
        return n;
    }

    inline const T & operator[](int i) const {
        return v[i];
    }

    inline T & operator[](int i) {
        return v[i];
    }

    inline const T & operator()(int i) const {
        return v[i];
    }

    inline T & operator()(int i) {
        return v[i];
    }

    ~r1tensor() {
        // AtomTypeList's destructor crshes here
        delete[] v;
    }


    const r2tensor<T> operator*(const r1tensor<T> & rhs) const {
        r2tensor<T> ret;
        ret.set(n, rhs.n);

        for (int i=0; i<n; ++i)
            for (int j=0; j<n; ++j)
                ret(i,j) = v[i] * rhs.v[j];

        return ret;
    }
};

template<class T> class r2tensor<r2tensor<T> > {

  protected:
    r2tensor<T> * v2;
    r2tensor<T> ** v;
    T * w2;
    T ** w;

    int n;
    int m;

  public:

    r2tensor() {
        n = m = 0;
        v2 = NULL;
        v  = NULL;
        w2 = NULL;
        w  = NULL;
    }

    r2tensor(int nn, int mm) {

        v2 = NULL;
        v  = NULL;
        w2 = NULL;
        w  = NULL;
        set(nn,mm);
    }

   ~r2tensor() {
        clear();
    }

    void clear() {

        delete[] v;  // delete the first index pointer array
        delete[] v2; // delete alñ the r2tensor<T> array; if it was initialized via r1tensor, the r2tensor destructor will ignore the pointers
        delete[] w;  // delete the first index global pointer array if the object was initialized with r1tensor; will be NULL otherwise
        delete[] w2; // delete all the T elements in one go

        w2 = NULL;
        w  = NULL;
        v2 = NULL;
        v  = NULL;
    }



    int N() const {
        return n;
    }

    int M() const {
        return m;
    }

    void set(int nnn, int mmm) {
        clear();

        v2 = new r2tensor<T>[nnn*mmm];
        v  = new r2tensor<T>*[nnn];
        n  = nnn;
        m  = mmm;

        for (int i=0; i<nnn; ++i) v[i] = v2 + i*mmm;
    }

    template<typename INT>  void set(const r1tensor<INT> & ns, const r1tensor<INT> & ms) {

        // set empry tensor arrays
        set(ns.N(), ms.N());

        // reserve memory for flat T objects
        {
            uint64_t nw = 0;
            for (int nn=0; nn<n;  ++nn) nw  += ns(nn);

            uint64_t mw  = 0;
            for (int mm=0; mm<m;  ++mm) mw  += ms(mm);

            w2 = new T[nw*mw];
            w  = new T*[nw*m];
        }

        // initialize nested tensor pointers to the flat T arrays
        {
            T *   p2 = w2;
            T **  p1  = w;

            for (int nn=0; nn<n;  ++nn) {
                for (int mm=0; mm<m;  ++mm) {
                    v[nn][mm].set( ns(nn), ms(mm), p1, p2);

                    p2 += ns(nn) * ms(mm);
                    p1 += ns(nn);
                }
            }
        }

    }


    inline const r2tensor<T> & operator()(int i, int j) const {
        return v[i][j];
    }

    inline r2tensor<T> & operator()(int i, int j) {
        return v[i][j];
    }

    inline const r2tensor<T> * operator[](int i) const {
        return v[i];
    }

    inline r2tensor<T> * operator[](int i) {
        return v[i];
    }

};


template <class T> struct Array {
    T * keys;
    int n;

    Array() {
        n = 0;
        keys = NULL;
    }

    Array(int nelem) {
        n = nelem;
        keys = new T[n];
    }

    void set(int nelem) {
        if (keys!=NULL) delete[] keys;
        n = nelem;
        keys = new T[n];
    }

    int search(const T & val) const {
        for (int i=0; i<n; ++i)
            if (keys[i] == val) return i;
        return -1; // should not happen
    }

    bool isin(const T & val, int nmax) const {
        for (int i=0; i<nmax; ++i)
            if (keys[i] == val) return true;
        return false;
    }

	T & operator[] (int i) {
		return keys[i];
	}

    const T & operator[] (int i) const {
		return keys[i];
	}

    /*
	Array<T> & operator=(const std::set<T> & TheSet) {

	    typedef std::set<T> set_T;

	    set_T::iterator it;
	    int i=0;

	    n = TheSet.size();
	    keys = new T[n];

	    for (it=TheSet.begin(); it!=TheSet.end(); ++it) {
	        keys[i] = *it;
	        ++i;
	    }

	    return *this;
	}
	*/


    void clear() {
        if (keys!=NULL) delete[] keys;
        keys = NULL;
        n = 0;
    }

    ~Array() {
        if (keys!=NULL) delete[] keys;
    }

    Array<T> & operator=(const Array<T> & rhs) {
        set(rhs.n);
        for (int i=0; i<n; ++i) keys[i] = rhs.keys[i];
        return *this;
    }

    Array<T> & operator+=(const Array<T> & rhs) {

        T * v = new T[n+rhs.n];

        for (int i=0; i<    n; ++i) v[i  ] =     keys[i];
        for (int i=0; i<rhs.n; ++i) v[n+i] = rhs.keys[i];

        n+=rhs.n; // increment

        swap(keys, v); // replace old array with new one
        delete[] v;

        return *this;
    }
};

#include <iostream>

template <class T> std::ostream & operator<< (std::ostream & os, const Array<T> & q) {
    for (int i=0; i<q.n; ++i)
        os << q.keys[i] << " ";
    os << std::endl;
    return os;
}




#include <map>
#include <inttypes.h>

inline uint64_t inter(uint32_t i) {
    uint64_t r=0;
    for (int d=0; d<32; ++d)
        r |= (i&(1<<d))<<d;
    return r;
}

template<class T> class r2sparse {

  protected:
    T * v;
    T * z;
    uint32_t n;
    uint32_t m;
    std::map<uint64_t, T*> indx;
    T null;

  public:
    r2sparse() {
        n = m = 0;
        v = z = NULL;
    }

    int N() const {
        return n;
    }

    int M() const {
        return m;
    }

    void set(uint32_t nnn, uint32_t mmm, const T & tnull) {

        indx.clear();
        delete[] v;

        v = new T[nnn*mmm];
        z = v;

        n  = nnn;
        m  = mmm;

        null = tnull;
    }

    r2sparse(uint32_t nnn, uint32_t mmm, const T & tnull) {
        v = NULL;
        set(nnn,mmm,tnull);
    }

    inline const T & operator()(uint32_t i, uint32_t j) const {

        uint64_t ij = (inter(i)<<1) + inter(j);

        typename std::map<uint64_t, T*>::const_iterator it;
        it = indx.find(ij);

        // not found
        if (it==indx.end()) {
            return null;
        }
        else return *(it->second);
    }

    inline T & operator()(uint32_t i, uint32_t j) {

        uint64_t ij = (inter(i)<<1) + inter(j);

        typename std::map<uint64_t, T*>::iterator it;
        it = indx.find(ij);

        // not found
        if (it==indx.end()) {
            indx[ij] = z;
            ++z;
            *z = null;
            return *(z-1);
        }
        else return *(it->second);
    }

    ~r2sparse() {
        delete[] v;
        indx.clear();
    }

};


#endif
