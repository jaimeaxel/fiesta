/*
    less.cpp

    less-numerical methods:
    ordering, packaging, random number generator, etc.
*/

#include <utility>

using namespace std;

// order a list of positive integers
void RadixSort(int * array, int n, int sep) {
    if (sep == 0)
        return;
    if (n<2)
        return;
    else if (n==2) {
        if (array[0] > array[1]) {
            swap(array[0], array[1]);
        }
    }

    int imin = 0;
    int imax = n;

    while(true) {

        while (imax>imin) {
            if ((array[imin] & sep) != 0) break;
            ++imin;
        }
        if (imin==imax) break;


        do {
            --imax;
            if ((array[imax] & sep) == 0) break;
        } while (imax>imin);
        if (imin==imax) break;

        //esguapea
        swap(array[imin], array[imax]);
    }

    // order the sublists
    RadixSort(array,        imin, sep/2);
    RadixSort(array+imin, n-imin, sep/2);
}


//busca dicotomicamente un entero en la lista ordenada
bool is_in (int * array, int n, int num) {
    int nmin = 0;
    int nmax = n-1;

    while(nmin <= nmax) {
        n = (nmin+nmax)/2;
        //elemento encontrado
        if (array[n] == num)
			return true;

        //elemento  no encontrado
        if (nmin == nmax)
			return false;

        if (num < array[n]) nmax = n-1;
        else                nmin = n+1;
    }

    //fuera del bound
    return false;
}

//elimina los elementos repetidos en un array
void Pack(int * & arr, int & n) {
    //cuenta elementos
    int l  = arr[0];
    int nn = 1;

    for (int i=1; i<n; ++i)
        if (arr[i] != l) {
            ++nn;
            l = arr[i];
        }

    int * narr = new int[nn];

    //guarda elementos
    l = arr[0];
    narr[0] = l;
    int ni = 1;

    for (int i=1; i<n; ++i)
        if (arr[i] != l) {
            l = arr[i];
            narr[ni] = l;
            ++ni;
        }

    //destruye la informacion anterior y guarda la nueva
    delete[] arr;
    arr = narr;
    n = nn;
}


/*
generador de numeros aleatorios sacado de numerical recipes

Long period (> 2 ^ 1018) random number generator of L'Ecuyer with Bays-Durham shue
and added safeguards. Returns a uniform random deviate between 0.0 and 1.0 (exclusive of
the endpoint values). Call with idum a negative integer to initialize; thereafter, do not alter
idum between successive deviates in a sequence. RNMX should approximate the largest floating
value that is less than 1.
*/

double ran2(int *idum) {
    const int    IM1 = 2147483563;
    const int    IM2 = 2147483399;
    const double AM  = (1.0/IM1);
    const int    IMM1= (IM1-1);
    const int    IA1 = 40014;
    const int    IA2 = 40692;
    const int    IQ1 = 53668;
    const int    IQ2 = 52774;
    const int    IR1 = 12211;
    const int    IR2 = 3791;
    const int    NTAB = 32;
    const int    NDIV = (1+IMM1/NTAB);
    const double EPS2 = 1.2e-7;
    const double RNMX = (1.0-EPS2);


    int k;
    static int idum2=123456789;
    static int iy=0;
    static int iv[NTAB];
    double temp;

    if (*idum <= 0) { //Initialize.
        if (-(*idum) < 1) *idum=1; //Be sure to prevent idum = 0.
        else *idum = -(*idum);
        idum2=(*idum);

        for (int j=NTAB+7; j>=0; j--) { //Load the shue table (after 8 warm-ups).
            k=(*idum)/IQ1;
            *idum=IA1*(*idum-k*IQ1)-k*IR1;
            if (*idum < 0) *idum += IM1;
            if (j < NTAB) iv[j] = *idum;
        }
        iy=iv[0];
    }

    k=(*idum)/IQ1; //Start here when not initializing.
    *idum=IA1*(*idum-k*IQ1)-k*IR1; //Compute idum=(IA1*idum) % IM1 without
    if (*idum < 0) *idum += IM1; //overflows by Schrage's method.
    k=idum2/IQ2;
    idum2=IA2*(idum2-k*IQ2)-k*IR2; //Compute idum2=(IA2*idum) % IM2 likewise.
    if (idum2 < 0) idum2 += IM2;
    int j=iy/NDIV; //Will be in the range 0..NTAB-1.
    iy=iv[j]-idum2; //Here idum is shued, idum and idum2 are
    iv[j] = *idum; //combined to generate output.
    if (iy < 1) iy += IMM1;
    if ((temp=AM*iy) > RNMX) return RNMX; //Because users don't expect endpoint values.
    else return temp;
}

