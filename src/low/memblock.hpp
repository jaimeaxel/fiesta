#ifndef __MEMBLOCK__
#define __MEMBLOCK__

#include <queue>
#include <cstdlib>

#include <inttypes.h>

/*
    Defines a chunk of adjacent memory to which arrays of objects can allocate its dinamically allocated
    memory, improving spatial locality and thus reducing cache misses.
*/



class Chunk {
    friend class ChunkArray;
    friend class MemBlock;
  private:
    void * pointer;
    uint64_t nbytes;
};


class PooledArray {
  public:
    void * pointer; //64
    uint32_t nbytes; //32
    uint32_t stride; //32
    uint8_t rank;   //8
};

const int ChunkArraySize = 1024 - sizeof(int*)/sizeof(int); //this way each node occupies exactly 2KB; can be changed depending on the OS/compiler/memory in order to improve performance

class ChunkArray {
    friend class MemBlock;

  private:
    Chunk List[ChunkArraySize];
    ChunkArray * next; //pointer to next ChunkList  (1024:0)

  public:
    ChunkArray() {
        next = NULL;
    }

    ~ChunkArray() {
        delete next;
    }
};

//the structure is only used twice:
//once for calculating the total amount of memory required and then for assigning it
class MemBlock {
  private:
    //for keeping track of pointers
    ChunkArray * root;
    ChunkArray * last;

    int nlast;  //numero de chunks ocupados del ultimo nodo
    int itlast; //numero de chunks ocupados del ultimo nodo


    //para arrayes bidimensionales
    ChunkArray * root2;
    ChunkArray * last2;

    int nlast2; //numero de chunks ocupados del ultimo nodo
    int itlast2; //numero de chunks ocupados del ultimo nodo

    //for keeping the memory
    int nbytes;
    char * buffer;

  public:
    MemBlock() {
        root = new ChunkArray;
        last = root;
        nlast = 0;

        root2 = new ChunkArray;
        last2 = root2;
        nlast2 = 0;

        nbytes = 0;
        buffer = NULL;
    }

    template <class T> void Add(T* & array, int n) {
        last->List[nlast].pointer = &array;
        last->List[nlast].nbytes  = n * sizeof(T);
        nbytes += n * sizeof(T);
        ++nlast;

        if (nlast == ChunkArraySize) {
            nlast = 0;
            last->next = new ChunkArray;
            last = last->next;
        }
    }


    template <class T> void Add( T** & array, int n, int m) {
        last->List[nlast].pointer = &array;
        last->List[nlast].nbytes  = n * sizeof(T*);
        nbytes += n * sizeof(T*);

        last2->List[nlast2].pointer = &(last->List[nlast]);
        last2->List[nlast2].nbytes  = m * sizeof(T);
        nbytes += n * m * sizeof(T);

        ++nlast;
        ++nlast2;

        if (nlast == ChunkArraySize) {
            nlast = 0;
            last->next = new ChunkArray;
            last = last->next;
        }

        if (nlast2 == ChunkArraySize) {
            nlast2 = 0;
            last2->next = new ChunkArray;
            last2 = last2->next;
        }
    }

    //reserva la memoria y asigna los punteros
    void Reserve() {
        //reserva
        //*******
        buffer = new char[nbytes];

        //asigna
        //******
        char * p = buffer;

        //arrays lineales
        ChunkArray * list = root;

        //bloques enlazados
        while (list!=last) {
            for (int i=0; i<ChunkArraySize; ++i) {
                *(char**)list->List[i].pointer = p;
                p += list->List[i].nbytes;
            }
            list = list->next;
        }

        //ultimo bloque
        for (int i=0; i<nlast; ++i) {
            //cout << list->List[i].pointer << endl;
            *(char**)list->List[i].pointer = p;
            p += list->List[i].nbytes;
            //cout << (void*)p << endl;
        }


        list = root2;

        //bloques enlazados
        while (list!=last2) {
            for (int i=0; i<ChunkArraySize; ++i) {
                Chunk *  fp = (Chunk*)list->List[i].pointer;
                char  ** cp = *(char***)(fp->pointer);

                int n = (fp->nbytes)/(sizeof(void*));

                for (int j=0; j<n; ++j)
                    cp[j] = p + j*list->List[i].nbytes;

                p += n*list->List[i].nbytes;
            }
            list = list->next;
        }

        //ultimo bloque
        for (int i=0; i<nlast2; ++i) {
            Chunk *  fp = (Chunk*)list->List[i].pointer;
            char  ** cp = *(char***)(fp->pointer);

            int n = (fp->nbytes)/(sizeof(void*));

            for (int j=0; j<n; ++j)
                cp[j] = p + j*list->List[i].nbytes;

            p += n*list->List[i].nbytes;
            //cout << (void*)p << endl;
        }

     }

    ~MemBlock() {
        /*

        // problemas con el destructor

        return;

        // pone a NULL todos los punteros para evitar desgracias
        // *****************************************************
        ChunkArray * list = root;

        //bloques enlazados
        while (list!=last) {
            for (int i=0; i<ChunkArraySize; ++i)
                *(char**)list->List[i].pointer = NULL;
            list = list->next;
        }

        //ultimo bloque
        for (int i=0; i<nlast; ++i)
            *(char**)list->List[i].pointer = NULL;
        delete root;


        list = root2;

        //bloques enlazados
        while (list!=last) {
            for (int i=0; i<ChunkArraySize; ++i)
                *(char**)list->List[i].pointer = NULL;
            list = list->next;
        }

        //ultimo bloque
        for (int i=0; i<nlast; ++i)
            *(char**)list->List[i].pointer = NULL;
        delete root;

        delete[] buffer;

        root   = NULL;
        root2  = NULL;
        last   = NULL;
        last2  = NULL;
        buffer = NULL;
        */
    }
};



static inline uint64_t align(uint64_t address, uint64_t size) {
    uint64_t ret;
    //get the upper power of two
    size--;
    size |= size >> 1;
    size |= size >> 2;
    size |= size >> 4;
    size |= size >> 8;
    size |= size >> 16;
    size++;

    if (size>64) size=64;

    //now find the next
    //ret = address%size;
    //ret = (size-ret)
    ret = (address+size)&(!(size-1));

    return ret;
}

static inline uint64_t addaligned(uint64_t & address, uint64_t size) {
    address = align(address, size) + size;
    return address;
}


//the structure is only used twice:
//once for calculating the total amount of memory required and then for assigning it
class MemAllocator {
    private:
        std::queue<PooledArray> pointers; //could be more nicely done with trees and reducing internal fragmentation

        //for keeping the memory
        uint64_t nbytes;
        char * buffer;

    public:
        MemAllocator() {
            nbytes = 0;
            buffer = NULL;
        }


        template <class T> void Add(T* & element) {
            PooledArray chunk;
            chunk.nbytes  = sizeof(T);
            chunk.pointer = &element;
            chunk.stride  = 0;
            chunk.rank = 0;

            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);
        }

        template <class T> void Add(T* & array, int n) {
            PooledArray chunk;
            chunk.nbytes  = n * sizeof(T);
            chunk.pointer = &array;
            chunk.stride  = 0;
            chunk.rank = 1;

            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);
        }

        template <class T> void Add( T** & array, int n, int m) {
            PooledArray chunk;

            chunk.nbytes  = n*m* sizeof(T);
            chunk.pointer = NULL;
            chunk.stride  = 0;
            chunk.rank = 2;
            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);

            //first level of the array
            chunk.nbytes  = n * sizeof(T*);
            chunk.pointer = &array;
            chunk.stride  = m*sizeof(T);
            chunk.rank = 1;
            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);
        }

        template <class T> void Add( T*** & array, int n, int m, int o) {
            PooledArray chunk;

            //first level of the array
            chunk.nbytes  = n*m*o * sizeof(T);
            chunk.pointer = NULL;
            chunk.stride  = 0;
            chunk.rank = 3;
            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);

            chunk.nbytes  = n*m* sizeof(T*);
            chunk.pointer = NULL;
            chunk.stride  = o*sizeof(T);
            chunk.rank = 2;
            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);

            chunk.nbytes  = n * sizeof(T**);
            chunk.pointer = &array;
            chunk.stride  = m*sizeof(T*);
            chunk.rank = 1;
            pointers.push(chunk);
            addaligned(nbytes, chunk.nbytes);
        }




        //reserva la memoria y asigna los punteros
        void Reserve() {
            //add memory
            //buffer = new char[nbytes];
            if (posix_memalign((void**)(&buffer), 4096, nbytes) != 0) throw -1;

            //asigna
            //******
            char * p = buffer;
            char * q = NULL; //previous pointer

            uint64_t offset = 0;

            while(pointers.size()!=0) {
                PooledArray chunk = pointers.front();

                if (chunk.rank==0) {
                    char* & pelement = *(char**)(chunk.pointer);

                    pelement = p;
                    q=NULL;
                }
                else if (chunk.rank==1) {
                    if (q!=NULL) {
                        for (int i=0; i<chunk.nbytes/sizeof(void*); ++i)
                            *((char**)(p+i)) = q + i*chunk.stride;
                        q = NULL;
                    }
                    char* & parray = *(char**)(chunk.pointer);
                    parray = p;
                }
                else if (chunk.rank==2) {
                    if (q!=NULL) {
                        for (int i=0; i<chunk.nbytes/sizeof(void*); ++i)
                            *((char**)(p+i)) = q + i*chunk.stride;
                        q = NULL;
                    }
                    q = p;
                }
                else if (chunk.rank==3) {
                    q = p;
                }

                addaligned(offset, chunk.nbytes);
                p = buffer + offset;

                pointers.pop();
            }
         }

        ~MemAllocator() {
            free(buffer);
            buffer = NULL;
        }
};

#endif
