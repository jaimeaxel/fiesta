#ifndef __LESS__
#define __LESS__

//template <class T> inline void fswap(T & e1, T & e2) {
//    T temp = e1;
//    e1 = e2;
//    e2 = temp;
//}

void RadixSort(int * array, int n, int sep = 1073741824); // 2^30
void Pack(int * & arr, int & n);

bool is_in (int * array, int n, int num);
double ran2(int *idum); // generator of random numbers of long period

template <class T> void ShellSort(T * a, int n) {
    int cols[] = {861, 336, 112, 48, 21, 7, 3, 1};

    for (int k=0; k<8; ++k) {
        int h=cols[k];

        for (int i=h; i<n; ++i) {
            T v = a[i];
            int j=i;

            while (j>=h && a[j-h]>v) {
                a[j] = a[j-h];
                j=j-h;
            }
            a[j] = v;
        }
    }
}

// O(N) sort // adaptative bucket sort (?)
// ***************************************
template <class T> void XSort(T * v, int m) {
    //if (m<2) return;

    if (m<1981) {ShellSort(v,m); return;}

    // find min and max
    int imin = 0;
    int imax = 0;
    double vmin = v[0]();
    double vmax = v[0]();

    for (int i=1; i<m; ++i) {
        if (v[i]<vmin) {imin = i; vmin = v[imin]();}
        if (v[i]>vmax) {imax = i; vmax = v[imax]();}
    }
    if (vmin == vmax) return;

    const double c = 0.51;
    double vmid = vmin + c*(vmax-vmin); //find a better guess for the mid-point according to the atomic distribution

    imin=0;
    imax=m-1;
    while(1) {
        for (; imin<m  && imin<=imax; ++imin) if (v[imin]() > vmid) break;
        for (; imax>=0 && imax> imin; --imax) if (v[imax]() < vmid) break;
        if (imin<imax) swap(v[imin], v[imax]);
        else           break;
    }
    XSort(v      ,   imax);
    XSort(v+imax , m-imax);
}

#endif
