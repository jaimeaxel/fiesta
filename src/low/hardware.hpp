
#ifndef __HW_CONFIGURATION__
#define __HW_CONFIGURATION__

class Buffer {
  private:
    unsigned char * p;
    size_t size;
    bool own;     // is the memory its own?

  public:
    void Allocate (size_t mem);
    void Allocate (size_t mem, const Buffer & parent);
};

class Stream {
    int id;
};

class Thread {
    int id;
};

class GPU {
    Buffer DeviceBuffer;
    Buffer HostBuffer;

    Stream * Streams;
    int nStreams;
};

class CPU {
    Buffer LocalBuffer;

    Thread * Threads;
    int nThreads;
};

class HardwareConfig {

  private:
    int nGPUs; // number of GPUs available
    int nCPUs; // number of CPUs (not necessarily physical)

    int nStreams; //number of CUDA streams
    int nThreads; //number of OpenMP/POSIX threads

    Buffer HostMem;    // available (pageable) memory (albeit other parts of the code allocate and deallocate their own)
    Buffer PinnedMem;  // available pinned memory

    CPU * CPUs;     // CPUs' own individual buffers and such
    GPU * GPUs;     // GPUs' buffers

  public:

    HardwareConfig() {
      //default configuration
        nGPUs = 0;
        nCPUs = 1;

        nStreams = 1;
        nThreads = 1;

        CPUs = NULL;
        GPUs = NULL;
    }
   ~HardwareConfig() {}

    //setter methods
    void SetCPUs    (int cpus) {nCPUs=cpus;}
    void SetGPUs    (int gpus) {nGPUs=gpus;}
    void SetStreams (int str)  {nStreams=str;}
    void SetThreads (int thr)  {nThreads=thr;}
    void SetMem     (size_t mem);
    void SetGpuMem  (size_t mem);

    //getter methods
    int GetCPUs () const {return nCPUs;}
    int GetGPUs () const {return nGPUs;}
    int GetStreams () const {return nStreams;}
    int GetThreads () const {return nThreads;}

    //set stuff
    void Initialize ();
};

#endif
