#ifndef __CHRONO__
#define __CHRONO__

#include <ostream>
#include <sys/time.h>

class Chronometer {
    friend std::ostream & operator<<(std::ostream & os, const Chronometer & chrono);

  private:
    timeval timer[2];
    int total_secs;
    int total_usecs;
    bool running;

  public:
    Chronometer  ();
    void Start   ();
    void Stop    ();
    void Restart ();
    double GetTotalTime() const;
};

std::ostream & operator<<(std::ostream & os, const Chronometer & chrono);


#ifdef __CUDACC__

class StreamChrono {

  private:
    cudaEvent_t start, stop;
    int total_secs;
    int total_usecs;
    int last_secs;
    int last_usecs;
    bool running;
    bool first;


  public:
    StreamChrono  ();
   ~StreamChrono  ();
    void Start    (cudaStream_t * stream);
    void Stop     (cudaStream_t * stream);
    double GetLastTime() const;
    double GetTotalTime();
};
#endif

#endif


