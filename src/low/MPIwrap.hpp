#ifndef __MPIWRAP__
#define __MPIWRAP__

#ifdef C_MPI
#include <mpi.h>
#endif

class pProcessGroup;

class ProcessGroup {
  private:
    pProcessGroup * p;

  public:
    ProcessGroup();
   ~ProcessGroup();

    void Init         ();
    void End          ();

    #ifdef C_MPI
    void Init         (MPI_Comm* comm_ptr);
    void End          (MPI_Comm* comm_ptr);
    #endif

    // MPI_Comm
    void InitHybrid   (const void * mpicomm, bool nested); // Init hybrid MPI+OpenMP scheme

    //bool Run          (); // test if process is included
    void Barrier      (); // merge processes that form part of the group
                                // with processes that are spinning
    int  GetRank      () const;
    void GetTime      () const;

    int  GetNodeProcs () const;

    int  GetTotalNodes() const;
    int  GetThreads   () const;


    bool IsMaster    () const;
    bool CheckMod    (int n) const;
    void Sync        () const;

    void Broadcast   (int & val);
    void Broadcast   (long long int & val);
    void Broadcast   (double & val);

    void Broadcast   (int * ival, long long int len);
    void Broadcast   (double * pval, long long int len);

    void Reduce      (double * val, long long int len); // reduce in-place
    void Reduce      (double * ival, double * oval, long long int len);

    void AllReduce   (double *  val, long long int len); // reduce in-place
    void AllReduce   (double * ival, double * oval, long long int len);

    void AllGather   (double * ival, long long int ilen, double * oval, long long int olen);
};

extern ProcessGroup NodeGroup;

#endif

