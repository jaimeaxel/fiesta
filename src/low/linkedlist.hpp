#ifndef __LINKEDLIST
#define __LINKEDLIST


template <class T> struct LinkedListNode {
    LinkedListNode<T> * next;
    T key;
};

template <class T> struct List {
    LinkedListNode<T> * root;
	LinkedListNode<T> * last;

	List() {
		root = NULL;
		last = NULL;
	}

	void push_back(T & val) {
		if (root == NULL) {
			root = new LinkedListNode<T>;
			last = root;
		}
		else {
			last->next = new LinkedListNode<T>;
			last = last->next;
		}
		last->next = NULL;
		last->key = val;
	}

	T * push_back_null() {
		if (root == NULL) {
			root = new LinkedListNode<T>;
			last = root;
		}
		else {
			last->next = new LinkedListNode<T>;
			last = last->next;
		}
		return &(last->key);
	}

	T * get_last() const {
		if (root == NULL)
			return NULL;
		else
			return &(last->key);
	}

	T * get_first() const {
		if (root == NULL)
			return NULL;
		else
			return &(root->key);
	}

	LinkedListNode<T> * begin() const {
        return root;
	}

	~List() {
	    while(root!=NULL) {
	        last = root;
	        root = root->next;
	        delete last;
	    }
	}
};

#endif
