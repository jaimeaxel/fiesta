


#include "cache64.hpp"
using namespace std;

ostream & operator<<(ostream & os, const cacheline64 & rhs) {
    double a[DOUBLES_PER_CACHE_LINE];
    store(a, rhs);

    for (int i=0; i<DOUBLES_PER_CACHE_LINE; ++i) os << a[i] << " ";
    return os;
}

void PackArrays     (const double * __restrict__  arrays, uint32_t ArraySize, cacheline64 * __restrict__  array8) {
    double * darr8 = (double*) array8;

    for (uint32_t k=0; k<ArraySize; ++k) {
        for (uint32_t n=0; n<DOUBLES_PER_CACHE_LINE; ++n) {
            darr8[DOUBLES_PER_CACHE_LINE*k+n] = arrays[n*ArraySize + k];
        }
    }
}

void UnPackArrays   (const cacheline64 * __restrict__ array8,  uint32_t ArraySize, double * arrays) {
    const double * darr8 = (double*) array8;

    for (uint32_t n=0; n<DOUBLES_PER_CACHE_LINE; ++n) {
        for (uint32_t s=0; s<ArraySize; ++s) {
            arrays[n * ArraySize + s] = darr8[DOUBLES_PER_CACHE_LINE*s + n]; //0; //
        }
    }
}

