#ifndef __SORT__
#define __SORT__

#include <algorithm>

using namespace std;

template <class T> static inline void Switch(T & a,T & b) {
    register T aa = a;
    register T bb = b;

    a = min(aa,bb);
    b = max(aa,bb);

    /*
    if (bb>=aa) {
        a = aa;
        b = bb;
    }
    else {
        b = aa;
        a = bb;
    }
    */
}

template <class T, class K> static inline void Switch(T & a,T & b, K & ka, K & kb) {
    register T aa = a;
    register T bb = b;

    register K kaa = ka;
    register K kbb = kb;

    if (bb>=aa) {
        a = aa;
        b = bb;
        ka = kaa;
        kb = kbb;
    }
    else {
        b = aa;
        a = bb;
        kb = kaa;
        ka = kbb;
    }
}

// sorts an array of 8 using in-place (bitonic) algorithm
template <class T, class K> void Sort8 (T * keys, K * vals) {

    Switch (keys[0], keys[1], vals[0], vals[1]);
    Switch (keys[2], keys[3], vals[2], vals[3]);
    Switch (keys[4], keys[5], vals[4], vals[5]);
    Switch (keys[6], keys[7], vals[6], vals[7]);

    Switch (keys[0], keys[2], vals[0], vals[2]);
    Switch (keys[1], keys[3], vals[1], vals[3]);
    Switch (keys[4], keys[6], vals[4], vals[6]);
    Switch (keys[5], keys[7], vals[5], vals[7]);

    Switch (keys[1], keys[2], vals[1], vals[2]);
    Switch (keys[5], keys[6], vals[5], vals[6]);

    Switch (keys[0], keys[4], vals[0], vals[4]);
    Switch (keys[1], keys[5], vals[1], vals[5]);
    Switch (keys[2], keys[6], vals[2], vals[6]);
    Switch (keys[3], keys[7], vals[3], vals[7]);

    Switch (keys[2], keys[4], vals[2], vals[4]);
    Switch (keys[3], keys[5], vals[3], vals[5]);

    Switch (keys[1], keys[2], vals[1], vals[2]);
    Switch (keys[3], keys[4], vals[3], vals[4]);
    Switch (keys[5], keys[6], vals[5], vals[6]);
}


// return the median of an array of 8
template <class T> T MedianOf8 (const T * k) {
    T keys[8];

    keys[0] = k[0];
    keys[1] = k[1];
    keys[2] = k[2];
    keys[3] = k[3];
    keys[4] = k[4];
    keys[5] = k[5];
    keys[6] = k[6];
    keys[7] = k[7];

    Switch (keys[0], keys[1]);
    Switch (keys[2], keys[3]);
    Switch (keys[4], keys[5]);
    Switch (keys[6], keys[7]);

    Switch (keys[0], keys[2]);
    Switch (keys[1], keys[3]);
    Switch (keys[4], keys[6]);
    Switch (keys[5], keys[7]);

    Switch (keys[1], keys[2]);
    Switch (keys[5], keys[6]);

    Switch (keys[0], keys[4]);
    Switch (keys[1], keys[5]);
    Switch (keys[2], keys[6]);
    Switch (keys[3], keys[7]);

    Switch (keys[2], keys[4]);
    Switch (keys[3], keys[5]);

    //Switch (keys[1], keys[2]);
    //Switch (keys[3], keys[4]);
    //Switch (keys[5], keys[6]);

    return  (keys[3]+keys[4])/2;
}

// returns the median of an array of N<=8 elements
template <class T> void SortN (T * keys, int N) {

    Switch (keys[0], keys[1]);
    Switch (keys[2], keys[3]);
    Switch (keys[4], keys[5]);
    Switch (keys[6], keys[7]);

    Switch (keys[0], keys[2]);
    Switch (keys[1], keys[3]);
    Switch (keys[4], keys[6]);
    Switch (keys[5], keys[7]);

    Switch (keys[1], keys[2]);
    Switch (keys[5], keys[6]);

    Switch (keys[0], keys[4]);
    Switch (keys[1], keys[5]);
    Switch (keys[2], keys[6]);
    Switch (keys[3], keys[7]);

    Switch (keys[2], keys[4]);
    Switch (keys[3], keys[5]);

    Switch (keys[1], keys[2]);
    Switch (keys[3], keys[4]);
    Switch (keys[5], keys[6]);
}

//will not necessarily work exactly due to roundoffs, but will give the true median most of the time, or a value close to it
template <class T> void Median (const T * keys, int N, T * aux) {

    // find the median of 8 and write it to aux
    for (int i=0; i<N/8; ++i) aux[i] = MedianOf8(keys+8*i);

    // find the Median of aux
    T maux = Median(aux, N/8, aux+N/8);

    // make a new list discarding lower and upper quadrants
    T * outk = aux+N/8;
    int n=0;

    for (int i=0; i<N/8; ++i) {
        if      (aux[i]<maux) {
            // write the 4 (or less) values higher than the median of the group of 8
            for (int j=0; j<8;++j)
                if (keys[8*i+j]>aux[i])
                    outk[n++] = keys[8*i+j];

        }
        else if (aux[i]>maux) {
            // write the 4 (or lesS) values lower  than the median of the group of 8
            for (int j=0; j<8;++j)
                if (keys[8*i+j]<aux[i])
                    outk[n++] = keys[8*i+j];
        }
        else outk[n++] = maux;
    }

    //take the median of the reduced list
    return Median (outk, n, outk+n);
}



//will not necessarily work exactly due to roundoffs, but will give the true median most of the time, or a value close to it
template <class T> T FastMedian (T * aux, const T * keys, int N) {

    if      (N==1) return (aux[0] = keys[0]);
    else if (N==2) return (aux[0] = (keys[0]+keys[1])/2);
    else if (N <9) {
        SortN (keys, N);
        return (aux[0] = (keys[N-1-N/2]+keys[N/2])/2);
    }

    int M = N/8;

    T aux2[8];

    aux[0] = FastMedian (aux2, keys      ,     M);
    aux[1] = FastMedian (aux2, keys +   M,     M);
    aux[2] = FastMedian (aux2, keys + 2*M,     M);
    aux[3] = FastMedian (aux2, keys + 3*M,     M);
    aux[4] = FastMedian (aux2, keys + 4*M,     M);
    aux[5] = FastMedian (aux2, keys + 5*M,     M);
    aux[6] = FastMedian (aux2, keys + 6*M,     M);
    aux[7] = FastMedian (aux2, keys + 7*M, N-7*M);

    SortN (aux,8);
    return (keys[3]+keys[4])/2;
}


//this is just a quicksort implementation
template <class T, class K> void QuickSort2(T * keys, K * elements, int N) {

    //for short lists default to selection sort
    if (N<9) {
        for (int i=0; i<N; ++i)  {
            T best = keys[i];
            int   k   = i;

            for (int j=i+1; j<N; ++j)
                if (keys[j]<best) {best = keys[j]; k = j;}

            swap(keys[i]    , keys[k]);
            swap(elements[i], elements[k]);
        }
        return;
    }

    int nl;
    T kpiv;

    // find a suitable pivot; count elements below it
    {
        register T kpivs[8];
        FastMedian(kpivs,  keys, N);

        //count number of elements lower than the pivot
        register int nls[] = {0,0,0,0, 0,0,0,0};
        for (int i=0; i<N; ++i) {
            if (keys[i] < kpivs[0]) ++nls[0];
            if (keys[i] < kpivs[1]) ++nls[1];
            if (keys[i] < kpivs[2]) ++nls[2];
            if (keys[i] < kpivs[3]) ++nls[3];
            if (keys[i] < kpivs[4]) ++nls[4];
            if (keys[i] < kpivs[5]) ++nls[5];
            if (keys[i] < kpivs[6]) ++nls[6];
            if (keys[i] < kpivs[7]) ++nls[7];
        }

        //int nl=0; for (int i=0; i<N; ++i) if (keys[i] < kpiv) ++nl;

        int nl = nls[0];
        T kpiv = kpivs[0];

        int os = abs(N-2*nl);

        for (int i=1; i<8; ++i) {
            int os2 = abs(N-2*nls[i]);

            if (os2<os) {
                nl = nls[i];
                kpiv = kpivs[i];
                os = os2;
            }
        }
    }


    T * kl = keys;
    T * kg = keys+nl;
    K * el = elements;
    K * eg = elements+nl;

    T * keys2     = kg;
    K * elements2 = eg;

    while(1) {
        while(*kl< kpiv) {++kl; ++el;}
        if (kl>=keys2) break;
        while(*kg>=kpiv) {++kg; ++eg;}

        swap(*kl, *kg);
        swap(*el, *eg);
    }

    //FlashSort both sublists (skipping the pivot)
    QuickSort2(keys ,  elements,     nl);
    QuickSort2(keys2,  elements2,  N-nl);
}


template <int L,  int N, int M, class T, class K> static inline void Switch(T * p, K * v) {

    if ((N<L)&&(M<L)) {
        register T aa = p[N];
        register T bb = p[M];

        register K kaa = v[N];
        register K kbb = v[M];

        if (bb>=aa) {
            p[N] = aa;
            p[M] = bb;
            v[N] = kaa;
            v[M] = kbb;
        }
        else {
            p[M] = aa;
            p[N] = bb;
            v[M] = kaa;
            v[N] = kbb;
        }
    }
}


//faster, since all steps within an 8-block can be carried in parallel
template <class T, class K,  int N> void BTSort (T * p, K * v) {

    Switch <N, 0, 1,T,K> (p,v);
    Switch <N, 2, 3,T,K> (p,v);
    Switch <N, 4, 5,T,K> (p,v);
    Switch <N, 6, 7,T,K> (p,v);
    Switch <N, 8, 9,T,K> (p,v);
    Switch <N,10,11,T,K> (p,v);
    Switch <N,12,13,T,K> (p,v);
    Switch <N,14,15,T,K> (p,v);



    Switch <N, 0, 3,T,K> (p,v);
    Switch <N, 1, 2,T,K> (p,v);
    Switch <N, 4, 7,T,K> (p,v);
    Switch <N, 5, 6,T,K> (p,v);
    Switch <N, 8,11,T,K> (p,v);
    Switch <N, 9,10,T,K> (p,v);
    Switch <N,12,15,T,K> (p,v);
    Switch <N,13,14,T,K> (p,v);

    Switch <N, 0, 1,T,K> (p,v);
    Switch <N, 2, 3,T,K> (p,v);
    Switch <N, 4, 5,T,K> (p,v);
    Switch <N, 6, 7,T,K> (p,v);
    Switch <N, 8, 9,T,K> (p,v);
    Switch <N,10,11,T,K> (p,v);
    Switch <N,12,13,T,K> (p,v);
    Switch <N,14,15,T,K> (p,v);



    Switch <N, 0, 7,T,K> (p,v);
    Switch <N, 1, 6,T,K> (p,v);
    Switch <N, 2, 5,T,K> (p,v);
    Switch <N, 3, 4,T,K> (p,v);
    Switch <N, 8,15,T,K> (p,v);
    Switch <N, 9,14,T,K> (p,v);
    Switch <N,10,13,T,K> (p,v);
    Switch <N,11,12,T,K> (p,v);

    Switch <N, 0, 2,T,K> (p,v);
    Switch <N, 1, 3,T,K> (p,v);
    Switch <N, 4, 6,T,K> (p,v);
    Switch <N, 5, 7,T,K> (p,v);
    Switch <N, 8,10,T,K> (p,v);
    Switch <N, 9,11,T,K> (p,v);
    Switch <N,12,14,T,K> (p,v);
    Switch <N,13,15,T,K> (p,v);

    Switch <N, 0, 1,T,K> (p,v);
    Switch <N, 2, 3,T,K> (p,v);
    Switch <N, 4, 5,T,K> (p,v);
    Switch <N, 6, 7,T,K> (p,v);
    Switch <N, 8, 9,T,K> (p,v);
    Switch <N,10,11,T,K> (p,v);
    Switch <N,12,13,T,K> (p,v);
    Switch <N,14,15,T,K> (p,v);



    Switch <N, 0,15,T,K> (p,v);
    Switch <N, 1,14,T,K> (p,v);
    Switch <N, 2,13,T,K> (p,v);
    Switch <N, 3,12,T,K> (p,v);
    Switch <N, 4,11,T,K> (p,v);
    Switch <N, 5,10,T,K> (p,v);
    Switch <N, 6, 9,T,K> (p,v);
    Switch <N, 7, 8,T,K> (p,v);

    Switch <N, 0, 4,T,K> (p,v);
    Switch <N, 1, 5,T,K> (p,v);
    Switch <N, 2, 6,T,K> (p,v);
    Switch <N, 3, 7,T,K> (p,v);
    Switch <N, 8,12,T,K> (p,v);
    Switch <N, 9,13,T,K> (p,v);
    Switch <N,10,14,T,K> (p,v);
    Switch <N,11,15,T,K> (p,v);

    Switch <N, 0, 2,T,K> (p,v);
    Switch <N, 1, 3,T,K> (p,v);
    Switch <N, 4, 6,T,K> (p,v);
    Switch <N, 5, 7,T,K> (p,v);
    Switch <N, 8,10,T,K> (p,v);
    Switch <N, 9,11,T,K> (p,v);
    Switch <N,12,14,T,K> (p,v);
    Switch <N,13,15,T,K> (p,v);

    Switch <N, 0, 1,T,K> (p,v);
    Switch <N, 2, 3,T,K> (p,v);
    Switch <N, 4, 5,T,K> (p,v);
    Switch <N, 6, 7,T,K> (p,v);
    Switch <N, 8, 9,T,K> (p,v);
    Switch <N,10,11,T,K> (p,v);
    Switch <N,12,13,T,K> (p,v);
    Switch <N,14,15,T,K> (p,v);
}

//faster, since all steps within an 8-block can be carried in parallel
template <class T, class K> void ShortSort (T * keys, K * elements, int N) {
    if (N>16) return;

    if (N>8) {
        if (N>12) {
            if (N>14) {
                if (N==16) BTSort <T,K,16> (keys,elements);
                else       BTSort <T,K,15> (keys,elements);
            }
            else {
                if (N==14) BTSort <T,K,14> (keys,elements);
                else       BTSort <T,K,13> (keys,elements);
            }
        }
        else {
            if (N>10) {
                if (N==12) BTSort <T,K,12> (keys,elements);
                else       BTSort <T,K,11> (keys,elements);
            }
            else {
                if (N==10) BTSort <T,K,10> (keys,elements);
                else       BTSort <T,K, 9> (keys,elements);
            }
        }
    }
    else {
        if (N> 4) {
            if (N> 6) {
                if (N== 8) BTSort <T,K, 8> (keys,elements);
                else       BTSort <T,K, 7> (keys,elements);
            }
            else {
                if (N== 6) BTSort <T,K, 6> (keys,elements);
                else       BTSort <T,K, 5> (keys,elements);
            }
        }
        else if (N==4) BTSort <T,K, 4> (keys,elements);
        else if (N==3) BTSort <T,K, 3> (keys,elements);
        else if (N==2) BTSort <T,K, 2> (keys,elements);
    }


}




//this is just a quicksort implementation
template <class T, class K> void QuickSort3(T * keys, K * elements, int N) {

    //for short lists default to selection sort
    if (N<17) {
        ShortSort(keys, elements, N);
        return;
    }

    int nl;
    T kpiv;

    // find a suitable pivot; count elements below it
    {
        // optimize this to avoid chain dependencies
        register T kmins[] = {keys[0],keys[0],keys[0],keys[0]};
        register T kmaxs[] = {keys[0],keys[0],keys[0],keys[0]};

        //compiler should optimize the inner loop conditionals
        for (int i=0; i<N; i+=4) {
                       kmins[0] = min(kmins[0], keys[i  ]);
            if (i+1<N) kmins[1] = min(kmins[1], keys[i+1]);
            if (i+2<N) kmins[2] = min(kmins[2], keys[i+2]);
            if (i+3<N) kmins[3] = min(kmins[3], keys[i+3]);

                       kmaxs[0] = max(kmaxs[0], keys[i  ]);
            if (i+1<N) kmaxs[1] = max(kmaxs[1], keys[i+1]);
            if (i+2<N) kmaxs[2] = max(kmaxs[2], keys[i+2]);
            if (i+3<N) kmaxs[3] = max(kmaxs[3], keys[i+3]);
        }

        //reduce
        const T kmin = min(min(kmins[0],kmins[1]),min(kmins[2],kmins[3]));
        const T kmax = max(max(kmaxs[0],kmaxs[1]),max(kmaxs[2],kmaxs[3]));

        if (kmin==kmax) return;

        register const T ks = (kmax-kmin)/4;
        register const T k1 = kmin +   ks;
        register const T k2 = kmin + 2*ks;
        register const T k3 = kmin + 3*ks;

        register int n1[4] = {0,0,0,0};
        register int n2[4] = {0,0,0,0};
        register int n3[4] = {0,0,0,0};

        // optimize this, too
        for (int i=0; i<N; i+=4) {
                       if (keys[i  ]<k1) ++n1[0];
                       if (keys[i  ]<k2) ++n2[0];
                       if (keys[i  ]<k3) ++n3[0];

            if (i+1<N) if (keys[i+1]<k1) ++n1[1];
            if (i+1<N) if (keys[i+1]<k2) ++n2[1];
            if (i+1<N) if (keys[i+1]<k3) ++n3[1];

            if (i+2<N) if (keys[i+2]<k1) ++n1[2];
            if (i+2<N) if (keys[i+2]<k2) ++n2[2];
            if (i+2<N) if (keys[i+2]<k3) ++n3[2];

            if (i+3<N) if (keys[i+3]<k1) ++n1[3];
            if (i+3<N) if (keys[i+3]<k2) ++n2[3];
            if (i+3<N) if (keys[i+3]<k3) ++n3[3];
        }

        int n1s = n1[0]+n1[1]+n1[2]+n1[3];
        int n2s = n2[0]+n2[1]+n2[2]+n2[3];
        int n3s = n3[0]+n3[1]+n3[2]+n3[3];

        // closest ?
        if      (abs(N-2*n1s)<abs(N-2*n2s)) {
            kpiv = k1;
            nl   = n1s;
        }
        else if (abs(N-2*n3s)<abs(N-2*n2s)) {
            kpiv = k3;
            nl   = n3s;
        }
        else {
            kpiv = k2;
            nl   = n2s;
        }
    }

    T * kl = keys;
    T * kg = keys+nl;
    K * el = elements;
    K * eg = elements+nl;

    T * keys2     = kg;
    K * elements2 = eg;

    while(1) {
        while(*kl< kpiv) {++kl; ++el;}
        if (kl>=keys2) break;
        while(*kg>=kpiv) {++kg; ++eg;}

        swap(*kl, *kg);
        swap(*el, *eg);
    }

    //FlashSort both sublists (skipping the pivot)
    QuickSort3(keys ,  elements,     nl);
    QuickSort3(keys2,  elements2,  N-nl);
}

//this is just a quicksort implementation
template <class T, class K> void FlashSort4(T * keys, K * elements, int N) {

    // default to
    if (N<17) {
        ShortSort(keys, elements, N);
        return;
    }

    // do some presorting in O (N log N) (can be reduced, since not everything is needed)
    for (int e=1; e<N; e<<=1) {
        for (int b=0; b<N; b+=2*e) {
            for (int i=b; i+e<min(b+2*e,N); ++i) {
                Switch (keys[i], keys[i+e], elements[i], elements[i+e]);
            }
        }
    }

    // THE PROBLEM HERE IS THAT WHEN N != 2^n, keys[N-1] doesn't hold the maximum;
    // using the binary representation of 1/3 or 2/5 to get a chain of max-min-max...
    // doesn't work in that case, either

    if (keys[0]==keys[N-1]) return; // min==max

    // gives a very good approximation to the median for N = 2^n
    // should be reasonable in other cases, although the mathematical
    // wizardry is completely lost
    T kpiv; int nl; {
        int i13 = (  N) / 3;
        int i23 = (N-1) - i13;
        int i25 = (2*N) / 5;
        int i35 = (N-1) - i25;

        T app13 = (keys[i13] + keys[i23])/2;
        T app25 = (keys[i25] + keys[i35])/2;

        //actually, we should count several buckets

        //kpiv = (app13+app25)/2;
        kpiv = keys[i35];

        // count elements below the pivot

        // count, but avoid chain dependencies
        register int n0, n1, n2, n3;
        n0 = n1 = n2 = n3 = 0;

        for (int i=0; i<N; i+=4) {
                       if (keys[i  ]<=kpiv) ++n0;
            if (i+1<N) if (keys[i+1]<=kpiv) ++n1;
            if (i+2<N) if (keys[i+2]<=kpiv) ++n2;
            if (i+3<N) if (keys[i+3]<=kpiv) ++n3;
        }

        nl = n0+n1+n2+n3;


        /*
        #pragma omp critical
        {
            cout << omp_get_thread_num() << "   " <<N << "    "  << i13<< " " << i23 << "    " << i25 << " " << i35 <<  "    " << nl << endl;
            cout << omp_get_thread_num() << "   " <<N << "    "  << keys[0] << " " << keys[N-1] << "    " << keys[i13] << " " << keys[i23] << "     "  << keys[i25] << " " << keys[i35] << endl;
            for (int i=0; i<N; ++i) cout << keys[i] << " "; cout << endl;
        }
        */
    }

    if (keys[0]==keys[N-1]) return; // min==max



    T * kl = keys;
    T * kg = keys+nl;
    K * el = elements;
    K * eg = elements+nl;

    T * keys2     = kg;
    K * elements2 = eg;

    while(1) {
        while(*kl<=kpiv) {++kl; ++el;}
        if (kl>=keys2) break;
        while(*kg> kpiv) {++kg; ++eg;}

        swap(*kl, *kg);
        swap(*el, *eg);
    }

    //FlashSort both sublists
    FlashSort4 (keys ,  elements,     nl);
    FlashSort4 (keys2,  elements2,  N-nl);
}




//this is just a quicksort implementation
template <class T, class K> void FlashSort(T * keys, K * elements, int N) {

    //for short lists default to selection sort
    if (N<32) {
        for (int i=0; i<N; ++i)  {
            T best = keys[i];
            int   k   = i;

            for (int j=i+1; j<N; ++j)
                if (keys[j]<best) {best = keys[j]; k = j;}

            swap(keys[i]    , keys[k]);
            swap(elements[i], elements[k]);
        }
        return;
    }

    T & k1 = keys[0];
    T & k2 = keys[N/2];
    T & k3 = keys[N-1];
    T kpiv;

    if      (k1<=k2 && k2<=k3) kpiv = k2;
    else if (k3<=k2 && k2<=k1) kpiv = k2;
    else if (k1<=k3 && k3<=k2) kpiv = k3;
    else if (k2<=k3 && k3<=k1) kpiv = k3;
    else                       kpiv = k1;
    //else if (k2<k1 && k1<k3) kpiv = k1;
    //else if (k3<k1 && k1<k2) kpiv = k1;


    //count number of elements lower than the pivot
    int nl=0;
    int ng=0;
    for (int i=0; i<N; ++i) {
        if      (keys[i] < kpiv) ++nl;
        else if (keys[i] > kpiv) ++ng;
    }

    int i,j,k;


    i=0;
    j=nl;

    while (1) {
        //find the first useful place in the first half of the list
        while (i<nl && keys[i]<kpiv) ++i;
        if (i>=nl) break;
        while (j<N  && keys[j]>=kpiv) ++j;
        if (j>=N) break;

        swap(keys[i]    , keys[j]);
        swap(elements[i], elements[j]);
    }

    j = nl;
    k = N-ng;

    while (1) {
        //find the first useful place in the first half of the list
        while (j<N-ng && keys[j]==kpiv) ++j;
        if (j>=N-ng) break;
        while (k<N  && keys[k]>kpiv) ++k;
        if (k>=N) break;

        swap(keys[j]    , keys[k]);
        swap(elements[j], elements[k]);
    }


    //FlashSort both sublists (skipping the pivot)
    FlashSort(keys         ,  elements         ,  nl);

    FlashSort(keys + (N-ng),  elements + (N-ng),  ng);
}

template <class T> void FlashSort(T * keys, int N) {

    //for short lists default to selection sort
    if (N<32) {
        for (int i=0; i<N; ++i)  {
            T best = keys[i];
            int   k   = i;

            for (int j=i+1; j<N; ++j)
                if (keys[j]<best) {best = keys[j]; k = j;}

            swap(keys[i]    , keys[k]);
        }
        return;
    }

    T & k1 = keys[0];
    T & k2 = keys[N/2];
    T & k3 = keys[N-1];
    T kpiv;

    if      (k1<=k2 && k2<=k3) kpiv = k2;
    else if (k3<=k2 && k2<=k1) kpiv = k2;
    else if (k1<=k3 && k3<=k2) kpiv = k3;
    else if (k2<=k3 && k3<=k1) kpiv = k3;
    else                       kpiv = k1;
    //else if (k2<k1 && k1<k3) kpiv = k1;
    //else if (k3<k1 && k1<k2) kpiv = k1;


    //count number of elements lower than the pivot
    int nl=0;
    int ng=0;
    for (int i=0; i<N; ++i) {
        if      (keys[i] < kpiv) ++nl;
        else if (keys[i] > kpiv) ++ng;
    }

    int i,j,k;


    i=0;
    j=nl;

    while (1) {
        //find the first useful place in the first half of the list
        while (i<nl && keys[i]<kpiv) ++i;
        if (i>=nl) break;
        while (j<N  && keys[j]>=kpiv) ++j;
        if (j>=N) break;

        swap(keys[i]    , keys[j]);
    }

    j = nl;
    k = N-ng;

    while (1) {
        //find the first useful place in the first half of the list
        while (j<N-ng && keys[j]==kpiv) ++j;
        if (j>=N-ng) break;
        while (k<N  && keys[k]>kpiv) ++k;
        if (k>=N) break;

        swap(keys[j]    , keys[k]);
    }


    //FlashSort both sublists (skipping the pivot)
    FlashSort(keys         ,  nl);
    FlashSort(keys + (N-ng),  ng);
}



#endif
