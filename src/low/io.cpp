/*
    io.cpp

    rutinas para la lectura y preproceso de de ficheros de entrada y escritura de mensajes de salida y ficheros
*/


#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "defs.hpp"
#include "fiesta/fiesta.hpp"
#include "low/io.hpp"
#include "low/mem.hpp"
#include "low/strings.hpp"
#include "low/plotters.hpp"
#include "low/MPIwrap.hpp"
#include "linear/tensors.hpp"
#include "linear/newtensors.hpp"

#ifdef _OPENMP
    #include <omp.h>
#endif

using namespace std;


FileBuffer::FileBuffer() {}

FileBuffer::FileBuffer(const std::string & file) {
    Read(file);
}

FileBuffer::FileBuffer(const std::string * file, int N) {
    Read(file,N);
}

void FileBuffer::Read(const std::string & file) {

    // copy the file to memory
    {
        const char * cp = file.c_str();
        ifstream ifile;
        ifile.open(cp);

        if (! ifile)
            throw Exception("Error: file '"+file+"' does not exist"); //unmatched

        LineArray.clear();

        int nlines = 0;

        //lee el archivo entero
        while (! ifile.eof() ){
            ++nlines;

            Line NewLine;
            getline(ifile, NewLine.str);
            NewLine.nline = nlines;

            LineArray.push_back(NewLine);
        }
        ifile.close();
    }


    std::list<Line>::iterator it = LineArray.begin();
    bool commented = false; // ignore C-style comments

    // preprocess file: delete blank lines, comments and whitespace
    while (it!=LineArray.end()) {

        //elimina comentarios
        string & theline = it->str;

        int pos;

		if (!commented) {

            // c-style comment
            pos = theline.find("//");
            if (pos >= 0)
                theline = theline.substr(0, pos);

            // used in Gaussian-style basis set files
            pos = theline.find("!");
            if (pos >= 0)
                theline = theline.substr(0, pos);

            // c++-style block comment
            pos = theline.find("/*");
            if (pos >= 0) {

                int posf = theline.find("*/");

                // open and shut case
                if (posf >= 0) {
                    theline = theline.substr(0, pos)
                            + theline.substr(posf+2, theline.length() - (posf+2) );
                    continue; // parse line again
                }
                //comment finished in other line
                else {
                    theline = theline.substr(0, pos);
                    commented = true;
                }
            }

            // unmatched comment closing
            pos = theline.find("*/");
            if (pos >= 0)
                throw Exception("Error: unmatched comment block closing in file '"+file+"'");
	    }
	    else {
            // matched comment closing
            pos = theline.find("*/");
            if (pos >= 0) {
                commented = false;
                theline = theline.substr(pos+2, theline.length() - (pos+2) );
                continue; //process line again
            }

            // nested comment?
            pos = theline.find("/*");
            if (pos >= 0)
                throw Exception("Error: unmatched comment block opening in file '"+file+"'");

            // rest of line is commented;
            // erase it and move to next line
            it = LineArray.erase(it);
            continue;
	    }


        // substitute tabs for spaces
        for (int i=0; i<int(theline.length()); ++i)
            if (theline[i] == 9) theline[i] = 32;

        // delete consecutive spaces
        {
            pos = theline.find("  ");
            // remove one space at a time
            while(pos>=0) {
                theline = theline.substr(0, pos)
                        + theline.substr(pos+1, theline.length() - (pos+1) );
                pos = theline.find("  ");
            }
            // remove first space if present
            if (theline[0]==32) theline = theline.substr(1, theline.length() - 1);
            // remove last space if present
            if (theline[theline.length()-1]==32) theline = theline.substr(0, theline.length() - 1);
        }

        // delete if line is empty
        if (theline.length() == 0) {
            it = LineArray.erase(it);
            continue;
        }

        // process next line
        ++it;
    }

    // unmatched comment
    if (commented)
        throw Exception("Error: unmatched comment block opening in file '"+file+"'");
}

void FileBuffer::Read(const std::string * file, int N) {

    LineArray.clear();
    int nlines = 0;

    // copy the file to memory
    for (int n=0; n<N; ++n) {

        const char * cp = file[n].c_str();
        ifstream ifile;
        ifile.open(cp);

        if (! ifile)
            throw Exception("Error: file '"+file[n]+"' does not exist"); //unmatched

        //lee el archivo entero
        while (! ifile.eof() ){
            ++nlines;

            Line NewLine;
            getline(ifile, NewLine.str);
            NewLine.nline = nlines;

            LineArray.push_back(NewLine);
        }
        ifile.close();
    }


    std::list<Line>::iterator it = LineArray.begin();
    bool commented = false; // ignore C-style comments

    // preprocess file: delete blank lines, comments and whitespace
    while (it!=LineArray.end()) {

        //elimina comentarios
        string & theline = it->str;

        int pos;

		if (!commented) {

            // c-style comment
            pos = theline.find("//");
            if (pos >= 0)
                theline = theline.substr(0, pos);

            // used in Gaussian-style basis set files
            pos = theline.find("!");
            if (pos >= 0)
                theline = theline.substr(0, pos);

            // c++-style block comment
            pos = theline.find("/*");
            if (pos >= 0) {

                int posf = theline.find("*/");

                // open and shut case
                if (posf >= 0) {
                    theline = theline.substr(0, pos)
                            + theline.substr(posf+2, theline.length() - (posf+2) );
                    continue; // parse line again
                }
                //comment finished in other line
                else {
                    theline = theline.substr(0, pos);
                    commented = true;
                }
            }

            // unmatched comment closing
            pos = theline.find("*/");
            if (pos >= 0)
                throw Exception("Error: unmatched comment block terminator");
	    }
	    else {
            // matched comment closing
            pos = theline.find("*/");
            if (pos >= 0) {
                commented = false;
                theline = theline.substr(pos+2, theline.length() - (pos+2) );
                continue; //process line again
            }

            // nested comment?
            pos = theline.find("/*");
            if (pos >= 0)
                throw Exception("Error: unmatched comment block opening");

            // rest of line is commented;
            // erase it and move to next line
            it = LineArray.erase(it);
            continue;
	    }


        // substitute tabs for spaces
        for (int i=0; i<int(theline.length()); ++i)
            if (theline[i] == 9) theline[i] = 32;

        // delete consecutive spaces
        {
            pos = theline.find("  ");
            // remove one space at a time
            while(pos>=0) {
                theline = theline.substr(0, pos)
                        + theline.substr(pos+1, theline.length() - (pos+1) );
                pos = theline.find("  ");
            }
            // remove first space if present
            if (theline[0]==32) theline = theline.substr(1, theline.length() - 1);
            // remove last space if present
            if (theline[theline.length()-1]==32) theline = theline.substr(0, theline.length() - 1);
        }

        // delete if line is empty
        if (theline.length() == 0) {
            it = LineArray.erase(it);
            continue;
        }

        // process next line
        ++it;
    }

    // unmatched comment
    if (commented)
        throw Exception("Error: unmatched comment block opening");
}


void FileBuffer::Clear() {
    LineArray.clear();
}


void WriteTensor(const string & file, tensor2 & array) {

    if (!NodeGroup.IsMaster()) return;

    string filepath = Fiesta::OutputDir + file;

    fstream ffile(filepath.c_str(), ios::out);

    ffile.precision(16);
    ffile.setf(ios::fixed);

    ffile << "{ ";

    for (int i=0; i<array.n; ++i) {
        ffile << "{ ";
        for (int j=0; j<array.m-1; ++j)
            ffile << array(i,j) << ", ";
            ffile << array(i,array.m-1);

        if (i!=array.n-1) ffile << " }," << endl;
        else              ffile << " } }" << endl;
        //ffile << endl << endl;
    }

    //ffile << "} ";
    ffile.close();
}

void WriteTensor(const string & file, symtensor2 & array) {

    if (!NodeGroup.IsMaster()) return;

    string filepath = Fiesta::OutputDir + file;
    fstream ffile(filepath.c_str(), ios::out);

    ffile.precision(10);
    ffile.setf(ios::fixed);

    for (int i=0; i<array.n; ++i) {
        for (int j=0; j<array.n; ++j)
            ffile << array(i,j) << " ";
       ffile << endl << endl;
    }
    ffile.close();
}

void WriteTensor(const string & file, symtensor & array) {

    if (!NodeGroup.IsMaster()) return;

    string filepath = Fiesta::OutputDir + file;
    fstream ffile(filepath.c_str(), ios::out);

    ffile.precision(10);
    ffile.setf(ios::fixed);

    ffile << array <<  endl;

    ffile.close();
}

void WriteTensorKD0(const string & file, tensor2 & array) {

    const double il10 = 1./log(10.);

    string filepath = Fiesta::OutputDir + file;
    fstream ffile(filepath.c_str(), ios::out);

    ffile.precision(5);
    ffile.setf(ios::fixed);

    int n2 = (array.n*array.n-array.n)/2;

    double * p = new double[n2];
    n2=0;
    for (int i=0; i<array.n; ++i)
      for (int j=0; j<i; ++j) {
        double m = log(abs(array(i,j)));
        p[n2++] = m;
      }

    for (int ii=0; ii<n2; ++ii) {
        int kk=ii;
        for (int jj=ii+1; jj<n2; ++jj)
            if (p[jj]>p[kk]) kk = jj;
        swap(p[ii],p[kk]);
        double mm = p[ii]*il10;
        ffile << 1+ii << " " << mm << " " << mm/double(1+ii) << endl;
    }

    delete[] p;

    ffile.close();
}

// only works for C_14XXX TZ systems
void WriteTensorKD(const string & file, tensor2 & array) {

    const double il10 = 1./log(10.);

    string filepath = Fiesta::OutputDir + file;
    fstream ffile(filepath.c_str(), ios::out);

    ffile.precision(5);
    ffile.setf(ios::fixed);

    tensor2 logs(14*5);

    for (int n=0; n<14; ++n) {
        for (int m=0; m<14; ++m) {

            int ii=9*n;
            for (int p=0; p<5; ++p) {
                int pp;
                if (p==2 || p==4) pp=3;
                else pp=1;

                int jj=9*m;
                for (int q=0; q<5; ++q) {
                    int qq;
                    if (q==2 || q==4) qq=3;
                    else qq=1;

                    double ss = 0.;
                    for (int i=ii; i<ii+pp; ++i)
                        for (int j=jj; j<jj+qq; ++j)
                            ss += array(i,j)*array(i,j);
                    logs(5*n+p, 5*m+q) = 0.5*log(ss)*il10;

                    jj += qq;
                }
                ii += pp;
            }
        }
    }

    //cout << logs << endl;

    const int n2 = logs.n*logs.n;
    double * p = logs.c2;

    for (int ii=0; ii<n2; ++ii) {
        int kk=ii;
        for (int jj=ii+1; jj<n2; ++jj)
            if (p[jj]>p[kk]) kk = jj;
        swap(p[ii],p[kk]);
        double mm = p[ii];
        ffile << 1+ii << " " << mm << " " << mm/double(1+ii) << endl;
    }

    ffile.close();
}

// store a symmetric matrix to disk
void WriteTensorBin (const string & file, symtensor2 & D) {

    if (NodeGroup.IsMaster()) {
        string filepath = Fiesta::OutputDir + file;
        ofstream ofile (filepath.c_str(), ios::out | ios::binary);
        int n = D.n;
        ofile.write ((char*)&n, sizeof(int));
        ofile.write((char*) D.c2, D.n2*sizeof(double));
        ofile.close();
    }
}

// read a symmetric matrix from file
void ReadTensorBin  (symtensor2 & D, const string & file) {
    if (NodeGroup.IsMaster()) {
        string filepath = Fiesta::OutputDir + file;
        ifstream ifile (filepath.c_str(), ios::in | ios::binary);
        int n;
        ifile.read((char*) &n, sizeof(int));
        //if (n!=D.n)  return; //error
        D.setsize(n);
        ifile.read((char*) D.c2, D.n2*sizeof(double));
        ifile.close();
    }
    Tensor2Broadcast(D);
}

// store a matrix to disk
void WriteTensorBin (const string & file, tensor2 & D) {

    if (NodeGroup.IsMaster()) {
        string filepath = Fiesta::OutputDir + file;
        ofstream ofile (filepath.c_str(), ios::out | ios::binary);
        int n = D.n;
        int m = D.m;
        ofile.write ((char*)&n, sizeof(int));
        ofile.write ((char*)&m, sizeof(int));
        ofile.write((char*) D[0], n*m*sizeof(double));
        ofile.close();
    }
}

// read a matrix from file
void ReadTensorBin  (tensor2 & D, const string & file) {

    if (NodeGroup.IsMaster()) {
        string filepath = Fiesta::OutputDir + file;
        ifstream ifile (filepath.c_str(), ios::in | ios::binary);
        int n,m;
        ifile.read((char*) &n, sizeof(int));
        ifile.read((char*) &m, sizeof(int));
        D.setsize(n,m);
        ifile.read((char*) D[0], n*m*sizeof(double));
        ifile.close();
    }
    Tensor2Broadcast(D);
}

void PlotRandomJoke()
{
    vector<string> jokes;

    jokes.push_back("Dead pixels may go to heaven");
    jokes.push_back(string("A logicist, a physicist and a chemist meet outside a conference room and start to talk.\n") +
                    string("The logicist starts laughing and asks 'Do you know the joke about us?'"));
    jokes.push_back("No electrons were harmed in any way during this calculation");
    jokes.push_back("My computer spent hours calculating and all I got was this lousy output");
    jokes.push_back("God is neither real nor imaginary. There is a complex argument somewhere");
    jokes.push_back("Better Nate than Lever");

    srand(time(NULL));

    stringstream ss;
    ss << "\n" << jokes[rand() % jokes.size()] << "\n";
    Fiesta::Messenger << ss.str() << endl;
}
