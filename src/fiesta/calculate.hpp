#ifndef __CALCULATE__
#define __CALCULATE__

#include <string>

#include "low/hardware.hpp"
#include "low/strings.hpp"
#include "basis/GTO.hpp"
#include "molecule/molecule.hpp"

class Elements {
  private:
    map <string, ElementID> Charges;

  public:
    Elements() {
        const char* PeriodicTable[119] = {
        "X",
        "H",                                                                                                                                                       "He",
        "Li","Be",                                                                                                                        "B" ,"C" ,"N" ,"O" ,"F" ,"Ne",
        "Na","Mg",                                                                                                                        "Al","Si","P" ,"S" ,"Cl","Ar",
        "K" ,"Ca",                                                                      "Sc","Ti","V" ,"Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
        "Rb","Sr",                                                                      "Y" ,"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I", "Xe",
        "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W" ,"Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn",
        "Fr","Ra","Ac","Th","Pa","U" ,"Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Nh","Fl","Mc","Lv","Ts","Og"
        };

        for (int n=0; n<119; ++n) {
            string elem = ucase(PeriodicTable[n]);
            Charges[elem] = n;
        }
    }

    ~Elements() {
        Charges.clear();
    }

    // returns charge of element, -1 if not available
    ElementID operator() (const string & e) const {
        string u = ucase(e);
        auto it = Charges.find(u);
        if (it!=Charges.end()) return it->second;
        else return InvalidElementID;
    }

    // inserts a new element of symbol 'e';
    ElementID & operator[] (const string & e) {

        if (IsDefined(e))
            throw Exception("Attempting to overwrite element with symbol " + e);

        string u = ucase(e);
        Charges[u] = InvalidElementID; // introduces the element, but assigns invalid Z

        return Charges[u];
    }

    bool IsDefined(const string & e) const {
        string u = ucase(e);
        auto it = Charges.find(u);
        return (it!=Charges.end());
    }

    // adds a new element ID
    void Add(const string & e) {
        if (IsDefined(e)) return;
        ElementID id = Charges.size();
        string u = ucase(e);
        Charges[u] = id;
    }

    int size() const {
        return Charges.size();
    }
};

struct Calculation {

    double SCFprecision;
    double logCauchySchwarz;
    double logGaussOverlap;
    double CauchySchwarz;
    double GaussOverlap;
    double CDprecision;

    double InputUnits;

    enum eMethod   ElectronMethod;
    enum eGuess    InitialGuess;

    int maxSCFc;

    bool CalcON;

    Elements ElementList;
    map<string, string> Parameters;

    // for exciton model
    std::string JobType;
    std::string FragInfo;
    tensor2* DensityPointer=NULL;

    // for integral test
    std::string InpDens;

    std::string DMsave;
    std::string DMload;
    std::string ERIUseLib;
    std::string ERIFileName;

    GTObasis    gBasis;   // Gaussian basis
    GTObasis    dfBasis;  // density fitting basis
    MCPbasis    McpBasis; // details for the model core potentials
    ECPbasis    EcpBasis; // details for the effective core potentials

    Molecule    mol;      //geometria, etc

    MolElData   SystemData;

    Calculation();
   ~Calculation();

    double  FloatConv (const string & in);
    int32_t IntConv   (const string & in);
    point   PointConv (const string & in1, const string & in2, const string & in3, double unit);
    vector3 VectorConv(const string & in1, const string & in2, const string & in3, double unit);

    void SetKeyword (const std::string & in);
    void Load(const std::string * files, int N);
    void LoadBasis();
    void Start();
    void Start(const std::vector<double>& center);

    // for exciton model
    void SetDensityPointer(tensor2* D) { DensityPointer = D;    }
    void ResetDensityPointer()         { DensityPointer = NULL; }
};

#endif
