/*
    calculate.cpp

    basic structure of calculation
    reads and interprets the input file; start the calculation
*/

#include <sstream>
#include <map>
#include <cmath>
#include <cstring>
#include <exception>

#include "defs.hpp"
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "basis/atomdef.hpp"
#include "low/io.hpp"
#include "low/strings.hpp"
#include "molecule/molecule.hpp"
#include "molecule/geometry.hpp"
#include "molecule/atoms.hpp"
#include "molecule/prescreen.hpp"
#include "linear/tensors.hpp"
#include "orbitals/OAObasis.hpp"
#include "libquimera/libquimera.hpp"

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_max_threads() 1
#endif

using namespace std;
using namespace Fiesta;

// initialize calculation parameters
Calculation::Calculation() {

    SCFprecision      =      5.e-8;   // threshold for SCF convergence; [F,D]^2 < SCFprecision
    logCauchySchwarz  = -log(1.e-12); // default CS threshold for ERI contraction
    logGaussOverlap   = -log(1.e-20); // threshold for discarding primitive Gaussian products in 1e- integrals
    CDprecision       =            0; // no Cholesky decomposition of the ERI tensor

    gBasis.name  = "";

    maxSCFc = 50;

    ElectronMethod      = eMethod::RHF;
    InitialGuess        = eGuess::CORE;

    InputUnits          = AMS;   // default input units

    JobType  = "";
    FragInfo = "frag.info";
    DensityPointer = NULL;
    InpDens  = "";

    DMsave = "";
    DMload = "";
}

// destructor
Calculation::~Calculation() {
}

// either casts the string to a FP number or
// returns the value of the variable
double Calculation::FloatConv(const string & in) {

    if (is_numeric(in)) return s2d(in);

    map<string, string>::const_iterator it = Parameters.find(in);

	if (it != Parameters.end())
		return FloatConv(it->second);

    Messenger << "Warning: undefined parameter '" << in << "'" <<endl;
    return 0;
}

int32_t Calculation::IntConv(const string & in) {

    if (is_numeric(in)) return s2i(in);

    map<string, string>::const_iterator it = Parameters.find(in);

	if (it != Parameters.end())
		return IntConv(it->second);

    Messenger << "Warning: undefined parameter '" << in << "'" <<endl;
    return 0;
}

point Calculation::PointConv(const string & in1, const string & in2, const string & in3, double unit) {
    point p;
    p.x = unit * FloatConv(in1);
    p.y = unit * FloatConv(in2);
    p.z = unit * FloatConv(in3);
    return p;
}

vector3 Calculation::VectorConv(const string & in1, const string & in2, const string & in3, double unit) {
    vector3 p;
    p.x = unit * FloatConv(in1);
    p.y = unit * FloatConv(in2);
    p.z = unit * FloatConv(in3);
    return p;
}


// IMPLEMENT A PROPER PARSER!!!


// read parameters from a line
void Calculation::SetKeyword (const string & in) {

    string key, val;
    int poseq = in.find("=");

    if (poseq != -1) {

        if ((poseq == 0) || (poseq == in.length()-1)) {
            throw Exception("Incorrect format: "+in);
        }

        key = cleanstr(in.substr(0, poseq));
        val = cleanstr(in.substr(poseq+1, in.length()-1));


        // MOLECULE DATA

        if      (key == "CHARGE")
            mol.SetCharge(s2i(val));
        else if (key == "MULTIPLICITY")
            mol.SetMultiplicity(s2i(val));

        else if (key == "MONOPOLES")
            SystemData.ExternalV.InitMonopoles( s2i(val) );
        else if (key == "DIPOLES")
            SystemData.ExternalV.InitDipoles( s2i(val) );
        else if (key == "QUADRUPOLE")
            SystemData.ExternalV.InitQuadrupoles( s2i(val) );

        else if (key == "UNITS") {
            if      (val == "A" || val == "ANG")    InputUnits = AMS;
            else if (val == "BOHR" || val == "AU")  InputUnits = 1.;
            else throw Exception("Unrecognized input nits'"+val+"'");
        }


        // JOB TYPE
        else if (key == "RUN") {
            if      (val == "TEST")  JobType = "TEST";
            else if (val == "EXMOD") JobType = "EXMOD";
            else if (val == "CIS")   JobType = "CIS";
            else if (val == "TDA")   JobType = "CIS";
        }
        // FOR TEST
        else if (key == "INPDENS") {
            InpDens = val;
        }
        // FOR EXCITON MODEL
        else if (key == "FRAGINFO") {
            FragInfo = val;
        }


        // FOR CIS/TDA
        else if (key == "CIS:NSTATES") {
            SystemData.cis_nstates = s2d(val);
        }
        else if (key == "CIS:CONV") {
            SystemData.cis_conv = s2d(val);
        }
        // FOR EXCITON DIMER CT
        else if (key == "CIS:CT_NOCC") {
            SystemData.dimer_ct_nocc = s2d(val);
        }
        else if (key == "CIS:CT_NVIR") {
            SystemData.dimer_ct_nvir = s2d(val);
        }


        // HARDWARE
        else if (key == "CPUS") {
            //int ncpus = s2i(val);
            //HWconfig.SetCPUs(ncpus);
        }
        else if (key == "THREADS") {
            int nthreads = 1;
            #ifdef _OPENMP
            nthreads = s2i(val);
            if (nthreads>0) omp_set_num_threads(nthreads);
            #endif
            //HWconfig.SetThreads(nthreads);
        }
        else if (key == "ERIMEM")
            Echidna::MAXMEM    = s2i(val)*Mword;
        else if (key == "BUFFMEM")
            Echidna::MEMBUFFER = s2i(val)*Mword;

        #ifdef USE_GPU
        else if (key == "GPU") {
            Echidna::UseGPU = s2b(val);
        }
        else if (key == "GPUS") {
            int ngpus = s2i(val);
            //HWconfig.SetGPUs(ngpus);
            Echidna::nGPUs = ngpus;
        }
        else if (key == "STREAMS") {
            int nstreams = s2i(val);
            //HWconfig.SetStreams(nstreams);
        }
        else if (key == "GPUMEM")
            Echidna::MAXMEMGPU = s2i(val)*Mword;
        #else
        else if ( key == "GPU" || (key == "GPUS") || (key == "STREAMS") || (key == "GPUMEM") ) {
            if (!(key == "GPU" && !s2b(val))) {
                // does not complain if GPU=OFF
                Messenger << "Warning: keyword '"<<key<<"' will be ignored. GPU acceleration requires recompilation." << endl;
            }
        }
        #endif


        // BASIS
        else if (key == "BASIS") {
            gBasis.name = val;
        }
        // ELEMENTAL BASIS
        // NOTE: Elemental basis does not work. -Xin 2018-03-22
        else if (key.substr(0,6) == "BASIS:") {
            string ename = key.substr(6,key.length()-1);
            ElementID id = ElementList(ename);
            if (id==InvalidElementID)
                Messenger << "Warning: basis defined for unused or undeclared element '" << ename <<"' will be ignored" << endl;
        }

        else  if (key == "GDO") {
            logGaussOverlap = log(10) * s2d(val);
        }
        else  if (key == "SCF:CS") {
            logCauchySchwarz = log(10) * s2d(val);
        }
        else  if (key == "OAOTHRESH") {
            SAOTHRESH = s2d(val);
        }


        // METHOD

        else if (key == "METHOD") {
            if      (val == "RHF")   ElectronMethod = eMethod::RHF;
            else if (val == "UHF")   ElectronMethod = eMethod::UHF;
            else if (val == "ROHF")  ElectronMethod = eMethod::ROHF;
            else if (val == "RKS")   ElectronMethod = eMethod::RKS;
            else if (val == "UKS")   ElectronMethod = eMethod::UKS;

            else if (val == "RSP")   ElectronMethod = eMethod::RHFRSP;

            else if (val == "OEHF")  ElectronMethod = eMethod::OEHF;
            else if (val == "RMP2")  ElectronMethod = eMethod::RMP2;

            else if (val == "BSO")   ElectronMethod = eMethod::BSO;

            else if (val == "CMM")   ElectronMethod = eMethod::RCMM;

            else throw Exception("Unrecognized method '"+val+"'");
        }
        else if (key == "SCF:PRECISION") {
            SCFprecision = s2d(val);
        }
        else if (key == "SCF:MAXIT") {
            maxSCFc = s2i(val);
        }
        else if (key == "SCF:GUESS") {
            if      (val == "CORE")  InitialGuess = eGuess::CORE;
            else if (val == "KIN")   InitialGuess = eGuess::KIN;
            else if (val == "SAVE")  InitialGuess = eGuess::SAVE;
            else if (val == "SAP")   InitialGuess = eGuess::SAP;
            else if (val == "SAD")   InitialGuess = eGuess::SAD;
            else if (val == "FIT")   InitialGuess = eGuess::DFIT;
			//else if (val == "ADMA")  InitialGuess = eGuess::ADMA; // not implemented
			//else if (val == "ANO")   InitialGuess = eGuess::ANO;
			else throw Exception("Unrecognized initial guess '"+val+"'");
        }
        else if (key == "SCF:CHOLESKY") {
            CDprecision = s2d(val);
        }
        else if (key == "CASEW2") {
            int err;
            err = LibQuimera::Quimera::SetW2(s2d(val));
            if (err)
                throw Exception("Invalid W^2 parameter for CASE integrals requested: "+val);
        }
        else if (key == "USECDR") {
            LibQuimera::Quimera::SetCDR(s2b(val));
        }
        else if (key == "USECASE") {
            LibQuimera::Quimera::SetCASE(s2b(val));
        }

        else if (key == "XCFUN") {
            int err;
            err = SystemData.DFT.SetFunctional(val);
			if (err) throw Exception("Unrecognized XC functional : '"+val+"'");
        }
        else if (key == "GRID:WTHRESH") {
            int err;
            err = SystemData.DFT.SetWthresh(s2d(val));
            if (err) throw Exception("Bad WTHRESH for DFT grid : '"+val+"'");
        }
        else if (key == "GRID:RTHRESH") {
            int err;
            err = SystemData.DFT.SetRthresh(s2d(val));
            if (err) throw Exception("Bad target precision for DFT grid : '"+val+"'");
        }
        else if (key == "GRID:LEBMIN") {
            int err;
            err = SystemData.DFT.SetMinL(s2i(val));
			if (err) throw Exception("Erroneous number of points for Lebedev quadrature : '"+val+"'");
        }
        else if (key == "GRID:LEBMAX") {
            int err;
            err = SystemData.DFT.SetMaxL(s2i(val));
			if (err) throw Exception("Erroneous number of points for Lebedev quadrature : '"+val+"'");
        }

        else if (key == "RSP:MODE") {
            if      (val == "REAL")   SystemData.RSP.SetReal();
            else if (val == "IMAG")   SystemData.RSP.SetImag();
            else if (val == "CPP")    SystemData.RSP.SetCPP();
            else throw Exception("Unrecognized response mode : '"+val+"'");

        }
        else if (key == "RSP:FREQS") {

            int pos = val.find(":");
            int err;

            // range specified;  w0 : wf n
            if (pos != string::npos) {

                string w0, wf, nw; {

                    int pos1 = val.find(":");

                    if (pos1!=string::npos) {

                        if ((pos1 == 0) || (pos1 == val.length()-1))
                            throw Exception("Incorrect format: " + val);

                        w0  = cleanstr(val.substr(0,pos1));
                        val = cleanstr(val.substr(pos1+1));
                    }

                    int pos2 = val.find(" ");

                    if (pos2!=string::npos) {

                        if ((pos2 == 0) || (pos2 == val.length()-1))
                            throw Exception("Incorrect format: " + val);

                        wf  = cleanstr(val.substr(0,pos2));
                        nw  = cleanstr(val.substr(pos2+1));
                    }
                }

                err = SystemData.RSP.SetFreqs ( s2d(w0), s2d(wf), s2i(nw) );
            }
            // list of frequencies specified; w0 w1 w2 w3 w4 ...
            else {
                std:list<double> ww;

                std::size_t pos=-1;

                do {
                    val = val.substr(pos+1); // from pos+1 to the end

                    string s;

                    pos = val.find(" ");

                    if (pos!=string::npos) s = cleanstr(val.substr(0,pos));
                    else                   s = val;

                    ww.push_back(s2d(s));

                } while (pos!=string::npos); // npos == -1


                double w[ww.size()];

                auto it = ww.begin();

                for (int i=0; i<ww.size(); ++i, ++it) w[i] = *it;

                err = SystemData.RSP.SetFreqs (w, ww.size());

            }

            if (err) throw Exception("Problem reading input for RSP module");
        }
        else if (key == "RSP:GAMMA") {
            int err;
            err = SystemData.RSP.SetGamma(s2d(val));
        }
        else if (key == "RSP:ACTIVE") {
            int err;
            err = SystemData.RSP.SetActive(s2d(val));
        }

        else if (key == "RSP:FROZEN") {
            int err;
            //err = SystemData.RSP.SetFrozen(s2d(val));
        }

        else if (key == "RSP:SOLVER:CS") {
            int err = SystemData.RSP.SetCSthresh     ( s2d(val) );
        }

        else if (key == "RSP:SOLVER:PRECISION") {
            int err = SystemData.RSP.SetConvergence  ( s2d(val) );
        }

        else if (key == "RSP:SOLVER:SVD") {
            int err = SystemData.RSP.SetSVDthresh    ( s2d(val) );
        }

        else if (key == "RSP:SOLVER:MAXIT") {
            int err = SystemData.RSP.SetMaxRSPcycles ( s2i(val) );
        }

        else if (key == "RSP:SOLVER:MAXSD") {
            int err = SystemData.RSP.SetMaxSubspace  ( s2i(val) );
        }

        else if (key == "RSP:SOLVER:MAXVECS") {
            int err = SystemData.RSP.SetMaxNumVecs   ( s2i(val) );
        }

        else if (key == "RSP:SOLVER:CHOLESKY") {
            int err = SystemData.RSP.SetCDthreshold ( s2d(val) );
        }

        else if (key == "RSP:SAVE") {
            int err = SystemData.RSP.SetSavePath(val);
        }


        // CONVERGENCE ACCELERATION

        else if (key == "SMEAR") {
            double E = s2d(val);
            if (E>0.) SystemData.iEsmear = 1./E;
        }

        // LOAD AND SAVE FOCK MATRICES

		else if (key == "FMLOAD")
            DMload = val;
        else if (key == "FMSAVE")
            DMsave = val;

        // INPUT / OUTPUT

        else if (key == "PRINTLEVEL") {
            int pl = s2i(val);
            if (pl>0) {
                Messenger.SetMaxDepth(pl);
                Benchmarker.SetMaxDepth(pl);
                Debugger.SetMaxDepth(pl);
            }
        }
        else if (key == "BENCHMARK") {
            LibQuimera::Quimera::SetBenchmark(s2b(val));
        }

        else if (key == "OUTPUTDIR") {
            Fiesta::OutputDir = val;
        }

        else {
            throw Exception("Unrecognized keyword '"+key+"'");
        }
    }
}

// read the input file
void Calculation::Load(const string * files, int N) {

    // reads and preprocesses the input file
    FileBuffer InputFile(files, N);

    std::list<Line>::iterator it = InputFile.LineArray.begin();
    std::list<Line>::iterator iend = InputFile.LineArray.end();

    // list keywords and make
    // list of variables
    while (it!=iend) {

        int possh = it->str.find("#"); // search the first keyword
        int poseq = it->str.find("="); // search for the first assignment (keyword or parameter)

        // keywords: MUL, CHAR, BASIS, ...
        if (possh >= 0) {

            string keyword = it->str.substr(possh+1, it->str.length()-1);

            try {
                SetKeyword(keyword);
            }
            catch (exception & e) {
                Messenger << e.what() << endl;
                throw e;
            }
            it = InputFile.LineArray.erase(it);
        }
        // parameter
        else if (poseq >= 0) {

            // no  rhs in assignement ?
            if (poseq ==  it->str.length()-1)
                throw Exception ("Error in input file, line " + int2str( it->nline)+ ": parameter can't be interpreted");

            string key = cleanstr( it->str.substr(0,poseq));
            string val = cleanstr( it->str.substr(poseq+1,  it->str.length()-1));

            if (ElementList.IsDefined(key)) {
                throw Exception("Attempting to overwrite element of symbol '" + key + "'");
            }

            // alternate element symbol
            if (ElementList.IsDefined(val)) {
                ElementList[key] = ElementList(val);
            }
            // some parameter
            else {
                Parameters[key] = val;
            }

            it = InputFile.LineArray.erase(it);
        }
        else
            ++it;
    }


    // make sure a basis set has been specified
    if (gBasis.name == "")
        throw Exception ("Error: no basis set file specified");

    // need an input density matrix for integral test
    if (JobType == "TEST" && InpDens == "")
        throw Exception ("Error: no input density file specified for test job");

    // need fragment info for exciton model
    if (JobType == "EXMOD" && FragInfo == "")
        throw Exception ("Error: no frag info file specified for exciton model job");


    // store atom coordinates
    Queue<Atom> AtomPool;

    // keep track of whether the first atoms have been defined in z-matrix format
    // ck change (initialize)
    bool firstCart = true;
    uint32_t nat = 0;
    uint32_t nmcp = 0;

    // number of n-poles
    int i1pole, i2pole, i4pole;
    i1pole = i2pole = i4pole = 0;

    it = InputFile.LineArray.begin();


    while (it!=iend) {

        string name,p1,p2,p3,p4,p5,p6; {
            // transforms the line to the iss object and reads the values
            istringstream sline;
            sline.str(it->str);
            // passes the values to substrings
            name = p1 = p2 = p3 = p4 = p5 = p6 = "";

            sline >> name >> p1 >> p2 >> p3 >> p4 >> p5 >> p6;
            sline.clear();
        }

        if (name == "MONOPOLE") {
            SystemData.ExternalV.MC[i1pole] = PointConv(p1,p2,p3, InputUnits);
            SystemData.ExternalV.M [i1pole] = FloatConv(p4);
            ++i1pole;
            continue;
        }

        if (name == "DIPOLE") {
            SystemData.ExternalV.DC[i2pole] = PointConv(p1,p2,p3, InputUnits);
            SystemData.ExternalV.D [i2pole] = VectorConv(p4,p5,p6, InputUnits);
            ++i2pole;
            continue;
        }


        ElementID atype = AtomDefs.AtomType(name);

        if (atype == InvalidElementID)
            throw Exception ("Error in line "+int2str(it->nline)+": element '" + name + "' is undefined");


        Atom nAtom;
        nAtom.num = nat;
        nAtom.type = atype; // returns the charge


        // number of strings to the right of the atom name
        int nterms; {
            if      (p6!="") nterms = 6;
            else if (p5!="") nterms = 5;
            else if (p4!="") nterms = 4;
            else if (p3!="") nterms = 3;
            else if (p2!="") nterms = 2;
            else if (p1!="") nterms = 1;
            else             nterms = 0;
        }

        // z-matrix style
        if (nterms%2==0) {
            if  (nat == 0) firstCart = false; // can only use z-matrix from now on
            if (firstCart && nat<4)
                throw Exception ("Error in line "+int2str(it->nline)+": cannot use z-matrix format before having enough reference points!");
            if (((nat<4) && (nterms!=2*nat)) || ((nat>3) && (nterms!=6)))
                throw Exception ("Error in line "+int2str(it->nline)+": attempting to use z-matrix format with wrong number of parameters!");

            // parse input, check correctness
            bool undef = false;
            bool multi = false;

            uint32_t at1,at2,at3;
            double   v1, v2, v3;
            at1=at2=at3=0;
            v1=v2=v3=0;

            if (nat>0) {
                at1 = IntConv(p1);
                v1  = FloatConv(p2);
                undef = (at1 >= nat+1) || (at1 < 1);
            }

            if (nat>1) {
                at2 = IntConv(p3);
                v2  = FloatConv(p4);
                undef = undef || (at2 >= nat+1) || (at2 < 1);
                multi = (at1 == at2);
            }

            if (nat>2) {
                at3 = IntConv(p5);
                v3  = FloatConv(p6);
                undef = undef || (at3 >= nat+1) || (at3 < 1);
                multi = multi || (at1 == at3) || (at2 == at3);
            }

            if (undef) throw Exception ("Error in line "+int2str(it->nline)+": atom references an undefined atom");
            if (multi) throw Exception ("Error in line "+int2str(it->nline)+": multiple references to same atom");

            nAtom.cart = false;
            nAtom.intc.at1 = at1;
            nAtom.intc.at2 = at2;
            nAtom.intc.at3 = at3;
            nAtom.intc.R   = v1 * InputUnits;
            nAtom.intc.a   = (PI/180.) * v2;
            nAtom.intc.b   = (PI/180.) * v3;
        }
        // cartesian
        else if (nterms==3) {
            if      (nat == 0) firstCart = true; // we need at least three of these
            else if (!firstCart)
                throw Exception ("Error in line "+int2str(it->nline)+": cannot use cartesian format after starting with z-matrix");
            nAtom.cart = true;
            nAtom.c    = PointConv(p1,p2,p3, InputUnits);
        }
        // what ?
        else {
            throw Exception ("Error in line "+int2str(it->nline)+": invalid format");
        }

        // save the atom, increment line
        AtomPool.PushBack(nAtom);
        ++it;
        ++nat;
    }

	Parameters.clear();

	mol.From(AtomPool);

	CauchySchwarz = exp(-logCauchySchwarz);
	GaussOverlap  = exp(-logGaussOverlap);

    // destructor automatico de AtomPool.clear();
}

void Calculation::LoadBasis() {
    // load the base file
    Messenger << "Loading basis set file" << endl;
    GTOMCPECPbasis combined_basis(gBasis,McpBasis,EcpBasis);
    combined_basis.Load();
}

// start calculation
void Calculation::Start() {
    std::vector<double> center;
    center.push_back(0.0);
    center.push_back(0.0);
    center.push_back(0.0);
    Start(center);
}

void Calculation::Start(const std::vector<double>& center)  {

    // print some useful hardware info
    // ===============================

    Messenger.precision(16);

    Messenger << "Using " << NodeGroup.GetTotalNodes() << " MPI processes" << endl;
    Messenger << "Using " << omp_get_max_threads() << " execution thread(s)" << endl;
    Messenger << endl;

    Messenger << "Using " << Echidna::MAXMEM / Mword    << " Mb of memory for 2e Fock" << endl;
    #ifdef USE_GPU
    if (Echidna::UseGPU)
        Messenger << "Using " << Echidna::MAXMEMGPU / Mword << " Mb of GPU memory for 2e Fock" << endl;
    #endif
    Messenger << "Using " << Echidna::MEMBUFFER / Mword << " Mb of memory for 2e buffers" << endl;
    Messenger << endl;

    if (LibQuimera::Quimera::GetCDR()) Messenger << "Using CDR/AERR for 2e integrals" << endl;

    // center and orient the molecule
    // according to inertia moments
    // =============================

    mol.mSumElectrons();

    //don't rotate the system if there is external potential

    // this both initializes atominteractions in MolElData and generates a sorting of atoms
    SystemData.SortAtoms(mol.Relabel, mol.mAtoms, gBasis, logGaussOverlap);

    mol.From(gBasis);

    mol.PrintMol();

    mol.CalcNuclear();

    // ==============================================
    // MOVE ELECTRONDATA FROM MOLECULE TO CALCULATION
    // ==============================================

    {
        // set e- in the electrondata
        SystemData.npairs = mol.nElec/2;
        SystemData.nalpha = mol.nEla;
        SystemData.nbeta  = mol.nElb;

        SystemData.ENuc   = mol.ENuc;

        SystemData.SumBasisFunctions(mol.Atoms, mol.nAtoms, gBasis);

        // sanity check
        // *************

        if (mol.nEla > SystemData.nBFs || mol.nElb > SystemData.nBFs)
            throw Exception ("Error: not enough basis functions, "+int2str(SystemData.nBFs)+", for the given number of electrons,"+int2str(mol.nElec));

        if ((mol.nElec%2 == 1) && ((ElectronMethod == eMethod::RHF) || (ElectronMethod == eMethod::RMP2)))
            throw Exception ("Error: can't perform a RHF calculation with an odd number of electrons");

        if ((mol.multiplicity != 0) && ((ElectronMethod == eMethod::RHF) || (ElectronMethod == eMethod::RMP2)))
            throw Exception ("Error: can't perform a RHF calculation with multiplicity other than 1");

        if ( ((mol.nElec + mol.multiplicity)%2 == 1) || (mol.multiplicity > mol.nElec))
            throw Exception ("Error: impossible combination of charge and multiplicity for the given system");


        // MolElData should hold its own copy of Atoms, nAtoms and gBasis,
        // modified for ECPs, etc.
        SystemData.InitPositions(gBasis);
        SystemData.InitTensors (ElectronMethod==eMethod::UHF || ElectronMethod==eMethod::UMP2);

        SystemData.InitMcp(McpBasis);
        SystemData.InitEcp(EcpBasis);

        Messenger << "Starting computation" << endl;
        Messenger.Push(); {
            // they have to be in this order
            SystemData.CalcShellPairs (gBasis, logGaussOverlap);
            SystemData.Start1eGTO     (center);
            SystemData.Start2eGTO     ();
            if (SystemData.DFT.use_dft) {
                SystemData.DFT.Init   (mol.Atoms, mol.nAtoms, gBasis);
            }
        } Messenger.Pop();
        Messenger << endl;
    }

    // ==================================
    // PERFORM A SINGLE POINT CALCULATION
    // ==================================

    {
        Messenger.precision(16);
        // calculate the nuclear contribution
        //Messenger << "Computing nuclear energy";
        //Messenger.Push(); {
            mol.CalcNuclear();
        //} Messenger.Pop();

        Messenger << "Nuclear energy:  " << mol.ENuc << endl;
        Messenger << endl;

        // list of nuclear charges
        // useful for SAD initial guess
        std::vector<int> nuc_chg;
        for (int i = 0; i < mol.nAtoms; i++) {
            nuc_chg.push_back(mol.Nuclei[i].type);
        }

        // solves the electronic structure
        Messenger << "Solving electronic structure" << endl;
        Messenger.Push(); {
            SystemData.CalcElectronic (InitialGuess, ElectronMethod, CauchySchwarz, SCFprecision, CDprecision, maxSCFc,
                                       DMload, DMsave, nuc_chg, JobType, InpDens, DensityPointer); // this should be set in two different places
        } Messenger.Pop();
        Messenger << endl;

        if (JobType == "TEST") {
            Messenger.precision(16);
            Messenger << "===== Overlap matrix ====="           << endl << SystemData.Sp;
            Messenger << "===== Kinetic energy matrix ====="    << endl << SystemData.Tp;
            Messenger << "===== Nuclear potential matrix =====" << endl << SystemData.Vp;
            Messenger << "===== Density matrix ====="           << endl << SystemData.SCF.GetDensity();
            Messenger << "===== Coulomb matrix ====="           << endl << SystemData.SCF.GetCoulomb();
            Messenger << "===== Exchange matrix ====="          << endl << SystemData.SCF.GetExchange();
            Messenger << "===== Fock matrix ====="              << endl << SystemData.SCF.GetFock();
        }
    }

}
