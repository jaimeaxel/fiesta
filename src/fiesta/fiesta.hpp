#ifndef __FIESTA__
#define __FIESTA__

#include <string>
#include <vector>
#include <memory>
#include "interface/interface.hpp"

class MessagePlotter;
class AtomDefList;
struct Calculation;
class Chronometer;

using namespace std;

namespace Fiesta {
    // global variables
    extern MessagePlotter  Messenger;
    extern MessagePlotter  Debugger;
    extern MessagePlotter  Benchmarker;

    extern std::string   * InputFiles;
    extern int             Nfiles;

    extern std::string     OutputDir;
    extern std::string     AtomsList;

    extern AtomDefList     AtomDefs;

    extern Calculation   * Job;
    extern Chronometer     FIESTAchrono;

    extern std::vector<double>  CenterOfMass;

    // fiesta utilities
    std::shared_ptr<DataInfo> Info();
    std::shared_ptr<DataResult> Result();

    void CalcCenterOfMass();

    void Init(int argc, char* argv[], bool print_output=true);
    void Run();
    void Finalize();

    void InitJob(int argc, char* argv[], bool print_output=true);
    void PrintTitle();
    void RunJob();
    void FinalizeJob();

    void RunTask(const std::shared_ptr<DataRequest>& req);
}

#endif
