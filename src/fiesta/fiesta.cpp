/*
     FIESTA: Fast Implementation of Electronic Structure Theorey's Algorithms

     Autors:
     =======

     Radovan Bast       XCINT, numgrid
     Carolin König      ECP & MCP integral code
     Xin Li             Exciton model, unit testing, python interface
     Jaime Axel Rosal   SCF, RSP, 1 & 2 e integrals, optimization

     v 0.8.6
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cassert>
#include <exception>

#include "defs.hpp"
#include "low/io.hpp"
#include "low/chrono.hpp"
#include "low/MPIwrap.hpp"
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "molecule/molecule.hpp"
#include "molecule/atoms.hpp"
#include "basis/atomdef.hpp"
#include "math/angular.hpp"
#include "math/gamma.hpp"
#include "libquimera/libquimera.hpp"

using namespace std;

// stringify
#define macro_str_2(s) #s
#define macro_str(x) macro_str_2(x)

// global variables
MessagePlotter Fiesta::Messenger;
MessagePlotter Fiesta::Debugger;
MessagePlotter Fiesta::Benchmarker;
string         Fiesta::OutputDir = "./";
string       * Fiesta::InputFiles;
int            Fiesta::Nfiles;
Calculation  * Fiesta::Job=NULL;
Chronometer    Fiesta::FIESTAchrono;
std::vector<double>  Fiesta::CenterOfMass;

void Fiesta::CalcCenterOfMass()
{
    Molecule& mol = Job->mol;
    GTObasis& gBasis = Job->gBasis;
    MolElData& SystemData = Job->SystemData;

    bool msg_active = Messenger.IsEnabled();

    if (msg_active) { Messenger.Disable(); }

    Job->LoadBasis();
    mol.mSumElectrons();
    SystemData.SortAtoms(mol.Relabel, mol.mAtoms, gBasis, Job->logGaussOverlap);
    mol.From(gBasis);
    CenterOfMass = mol.GetCenterOfMass();

    if (msg_active) { Messenger.Enable(); }
}

std::shared_ptr<DataInfo> Fiesta::Info()
{
    std::shared_ptr<DataInfo> info = std::shared_ptr<DataInfo>(new DataInfo);
    info->is_master  = NodeGroup.IsMaster();
    info->num_atoms  = Job->mol.mAtoms.n;
    info->input_xyz  = InputFiles[0];
    info->input_frag = Job->FragInfo;
    return info;
}

std::shared_ptr<DataResult> Fiesta::Result()
{
    std::shared_ptr<DataResult> res = std::shared_ptr<DataResult>(new DataResult);

    const SCFsolver& SCF = Job->SystemData.SCF;
    const CISsolver& CIS = Job->SystemData.CIS;

    // process SCF result
    double  E  = SCF.GetTotalEnergy();
    int  nocc  = SCF.GetNumOcc();

    tensor2 Da = SCF.GetDensity();
    tensor2 Db = SCF.GetDensity(true);
    tensor2 C  = SCF.GetMOs();

    tensor2 F_MO = SCF.GetFockMO();

    int nmo = C.n;
    int nao = C.m;

    assert(Da.n == nao && Da.m == nao);
    assert(Db.n == nao && Db.m == nao);

    assert(F_MO.n == nao && F_MO.m == nao);

    // save energy, nocc, densities, MO coef
    res->E = E;
    res->nocc = nocc;

    res->Da.setsize(nao, nao);
    res->Db.setsize(nao, nao);
    res->C.setsize (nmo, nao);

    res->F_MO.setsize (nao, nao);

    memcpy(res->Da.data(), Da.c2, sizeof(double)*res->Da.size());
    memcpy(res->Db.data(), Db.c2, sizeof(double)*res->Db.size());
    memcpy(res->C.data(),  C.c2,  sizeof(double)*res->C.size());

    memcpy(res->F_MO.data(), F_MO.c2, sizeof(double)*res->F_MO.size());

    // process CIS result
    const std::vector<double>&  E_cis   = CIS.get_E_cis();
    const std::vector<tensor2>& CI_vecs = CIS.get_CI_vecs();

    int nstates = CI_vecs.size();

    // save excitation energies and CI vectors
    if (nstates > 0) {
        res->E_cis.setsize(nstates, 1);
        memcpy(res->E_cis.data(), E_cis.data(), sizeof(double)*nstates);

        int ov_dim = CI_vecs[0].n * CI_vecs[0].m;
        res->CI_vecs.setsize(nstates, ov_dim);
        for (int k = 0; k < nstates; k++) {
            memcpy(res->CI_vecs.data() + k*ov_dim, CI_vecs[k].c2, sizeof(double)*ov_dim);
        }
    }

    // save transition dipoles
    const tensor2& e_tr_dip = CIS.get_elec_trans_dipole();
    const tensor2& v_tr_dip = CIS.get_velo_trans_dipole();
    const tensor2& m_tr_dip = CIS.get_magn_trans_dipole();

    res->e_tr_dip.setsize(e_tr_dip.n, e_tr_dip.m);
    res->v_tr_dip.setsize(v_tr_dip.n, v_tr_dip.m);
    res->m_tr_dip.setsize(m_tr_dip.n, m_tr_dip.m);

    memcpy(res->e_tr_dip.data(), e_tr_dip.c2, sizeof(double)*res->e_tr_dip.size());
    memcpy(res->v_tr_dip.data(), v_tr_dip.c2, sizeof(double)*res->v_tr_dip.size());
    memcpy(res->m_tr_dip.data(), m_tr_dip.c2, sizeof(double)*res->m_tr_dip.size());

    // process dimer couplings
    const tensor2& LE_LE = CIS.get_LE_LE();
    const tensor2& LE_CT = CIS.get_LE_CT();
    const tensor2& CT_CT = CIS.get_CT_CT();

    int n_LE_A = LE_LE.n;
    int n_LE_B = LE_LE.m;
    int n_LE = n_LE_A + n_LE_B;

    assert(n_LE == LE_CT.n);
    int n_CT = LE_CT.m;

    assert(n_CT == CT_CT.n);
    assert(n_CT == CT_CT.m);

    res->LE_LE.setsize(n_LE_A, n_LE_B);
    res->LE_CT.setsize(n_LE, n_CT);
    res->CT_CT.setsize(n_CT, n_CT);

    memcpy(res->LE_LE.data(), LE_LE.c2, sizeof(double)*res->LE_LE.size());
    memcpy(res->LE_CT.data(), LE_CT.c2, sizeof(double)*res->LE_CT.size());
    memcpy(res->CT_CT.data(), CT_CT.c2, sizeof(double)*res->CT_CT.size());

    return res;
}

// top-level: fiesta

void Fiesta::Init(int argc, char* argv[], bool print_output)
{
    NodeGroup.Init();
    InitJob(argc, argv, print_output);
}

void Fiesta::Run()
{
    RunJob();
}

void Fiesta::Finalize()
{
    FinalizeJob();
    NodeGroup.End();
}

// medium-level: fiesta job

void Fiesta::InitJob(int argc, char* argv[], bool print_output)
{
    if (Job != NULL) { delete Job; Job = NULL; }
    Job = new Calculation;

    // wall timer
    FIESTAchrono.Start();

    // initialized some key constants
    LibAngular::InitAngular();
    LibIGamma::InitIncompleteGammas(); // this includes constant mem initialization

    // sets output files and print levels
    Debugger.SetFile    ("fiesta.debug.out");
    Benchmarker.SetFile ("fiesta.benchmark.out");

    Messenger.SetMaxDepth(8);
    Benchmarker.SetMaxDepth(8);
    Debugger.SetMaxDepth(8);

    // output setting
    if (! print_output) {
        Messenger.Disable();
        Benchmarker.Disable();
        Debugger.Disable();
    } else {
        Messenger.Enable();
        Benchmarker.Enable();
        Debugger.Enable();
    }

    // set the default directory to look for atoms.lst and .qic files
    string fiesta_dir = ".";
    if (const char * path = std::getenv("FIESTA_DIR")) { fiesta_dir = path; }

    LibQuimera::Quimera::SetQICdir(fiesta_dir + "/qic");

    // load atom definition file
    string AtomsList = "";
    AtomsList = fiesta_dir + "/atoms.lst";
    AtomDefs.Load(AtomsList);

    // go through input files
    Nfiles = argc-1;
    InputFiles = new string[Nfiles];
    for (int n=0; n<Nfiles; ++n) {
        InputFiles[n] = std::string(argv[n+1]);
    }

    // load input files
    Job->Load(InputFiles, Nfiles);

    // center of mass is only calculated at the very beginning
    CalcCenterOfMass();
}

void Fiesta::PrintTitle()
{
    // print version
    Messenger << "Starting FIESTA v 0.8.6     commit: " << macro_str(COMMIT_HASH);
    #ifdef USE_GPU
    Messenger << "     GPU enabled";
    #endif
    Messenger << endl << endl;

    // print input files
    Messenger << "Input files:";
    for (int n=0; n<Nfiles; ++n) {
        Messenger << "  " << InputFiles[n];
    }
    Messenger << endl << endl;
}

void Fiesta::RunJob()
{
    PrintTitle();
    assert(Fiesta::Job->JobType != "EXMOD");

    // run job
    Fiesta::Job->Load(Fiesta::InputFiles, Fiesta::Nfiles);
    Fiesta::Job->LoadBasis();
    Fiesta::Job->Start(CenterOfMass);
}

void Fiesta::FinalizeJob()
{
    delete Job;
    Job = NULL;

    // print wall time
    FIESTAchrono.Stop();
    Messenger << "Total running time: " << FIESTAchrono << endl;

    // print lame joke
    PlotRandomJoke();
    Messenger << endl;
}

// low-level: fiesta job task

void Fiesta::RunTask(const std::shared_ptr<DataRequest>& req)
{
    NodeGroup.Sync();

    delete Job;
    Job = new Calculation;

    InputFiles[0] = req->input_xyz;
    Job->Load (InputFiles, Nfiles);

    tensor2* dens_ptr = new tensor2 [8];

    if (req->job_type == "EXMOD_DIMER") {
        // process Da, Db, C for exciton dimer task
        int nmo = req->C.nrows();
        int nao = req->C.ncols();

        req->Da.check_size(nao, nao);
        req->Db.check_size(nao, nao);

        dens_ptr[0].setsize(nao, nao);
        dens_ptr[1].setsize(nao, nao);
        dens_ptr[2].setsize(nmo, nao);

        memcpy(dens_ptr[0].c2, req->Da.data(), sizeof(double)*req->Da.size());
        memcpy(dens_ptr[1].c2, req->Db.data(), sizeof(double)*req->Db.size());
        memcpy(dens_ptr[2].c2, req->C.data(),  sizeof(double)*req->C.size());

        int nstates_A = req->CI_vecs_A.nrows();
        int ov_dim_A  = req->CI_vecs_A.ncols();
        int nstates_B = req->CI_vecs_B.nrows();
        int ov_dim_B  = req->CI_vecs_B.ncols();

        if (nstates_A > 0 && nstates_B > 0) {
            dens_ptr[3].setsize(nstates_A, ov_dim_A);
            dens_ptr[4].setsize(nstates_B, ov_dim_B);

            memcpy(dens_ptr[3].c2, req->CI_vecs_A.data(), sizeof(double)*req->CI_vecs_A.size());
            memcpy(dens_ptr[4].c2, req->CI_vecs_B.data(), sizeof(double)*req->CI_vecs_B.size());

            // !!! weird way of communicating size
            dens_ptr[5].setsize(req->nocc_A, req->nvir_A);
            dens_ptr[6].setsize(req->nocc_B, req->nvir_B);

            // !!! even more weird way of communicating indices
            dens_ptr[7].setsize(req->index_A, req->index_B);
        }
    }

    Job->JobType = req->job_type;
    Job->SetDensityPointer(dens_ptr);

    Job->LoadBasis();
    Job->Start(CenterOfMass);

    delete[] dens_ptr;
}
