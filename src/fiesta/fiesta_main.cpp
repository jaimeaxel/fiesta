#include <cassert>
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "exciton/exciton_model.hpp"

void RunExcitonModel()
{
    Fiesta::PrintTitle();
    assert (Fiesta::Job->JobType == "EXMOD");

    ExcitonModel Exciton;
    Exciton.init(Fiesta::Info());

    for (int frag = 0; frag < Exciton.get_total_nfrags(); frag++) {
        int task_id = frag;

        Fiesta::RunTask(Exciton.request(task_id, ""));
        Exciton.finalize_task(task_id, "", Fiesta::Result());

        if (! Exciton.has_dft()) {
            Fiesta::RunTask(Exciton.request(task_id, "cation"));
            Exciton.finalize_task(task_id, "cation", Fiesta::Result());

            Fiesta::RunTask(Exciton.request(task_id, "anion"));
            Exciton.finalize_task(task_id, "anion", Fiesta::Result());
        }
    }

    for (int pair = 0; pair < Exciton.get_total_npairs(); pair++) {
        int task_id = pair + Exciton.get_total_nfrags();

        Fiesta::RunTask(Exciton.request(task_id, ""));
        Exciton.finalize_task(task_id, "", Fiesta::Result());

        if (! Exciton.has_dft()) {
            Fiesta::RunTask(Exciton.request(task_id, "ca"));
            Exciton.finalize_task(task_id, "ca", Fiesta::Result());

            Fiesta::RunTask(Exciton.request(task_id, "ac"));
            Exciton.finalize_task(task_id, "ac", Fiesta::Result());
        }
    }

    Exciton.finish();
}

int main(int argc, char* argv[])
{
    Fiesta::Init(argc, argv);

    if (Fiesta::Job->JobType == "EXMOD") {
        RunExcitonModel();
    } else {
        Fiesta::RunJob();
    }

    Fiesta::Finalize();

    return 0;
}
