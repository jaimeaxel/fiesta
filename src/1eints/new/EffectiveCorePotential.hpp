#include "basis/GTO.hpp"
#include "libechidna/libechidna.hpp"

#ifndef __EFFECTIVECOREPOT__
#define __EFFECTIVECOREPOT__


struct EffectiveCorePotential{

    int    nnuclei = 0;
    point * positions = NULL;
    std::vector<GTOforECP> * parameter = NULL;

    bool Empty() {return (nnuclei < 1);}
    void Init(const int& In){
       nnuclei = In;
       positions = new point[nnuclei];
       parameter = new std::vector<GTOforECP>[nnuclei];
    }

    ~EffectiveCorePotential(){
       delete[] positions;
       delete[] parameter;
    }


};

#endif
