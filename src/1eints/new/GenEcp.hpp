#include <functional>
#include "EffectiveCorePotential.hpp"

//typedef double AngularMatrix [LMAX+1][2*LMAX+1][LMAX+1][2*LMAX+1][LMAX+1][LMAX+1][LMAX+1]; //l1,m1,l2,m2,lx,ly,lz

#ifndef __ECPINTS__
#define __ECPINTS__

typedef double CoeffMatrix [LMAX+1][2*LMAX+1][LMAX+1][LMAX+1][LMAX+1]; // l,m,lx,ly,lz

namespace EcpInts{

  void GenEcp (const ShellPair & AB, const rAtom * nuclei, int nnuclei, const EffectiveCorePotential&, symtensor & Vp);

  void GenEcp (const point & A, const point & B, const GTO & GTOA, const GTO & GTOB, const EffectiveCorePotential &, tensor2 & V, int osa, int osb);


  double SpecialQuadrature (const ShellPair& AB, const rAtom * nuclei, const EffectiveCorePotential&, const int& enuc, const int& i, const int& nn, const int& la, const int& lb, const int& ja, const int& jb);

  double SpecialQuadrature (const point & A, const point & B, const GTO & GTOA, const GTO & GTOB,  const EffectiveCorePotential& arEcp, int arEnuc, int arI, int arNn,   int La, int Lb, int Ja, int Jb);


  namespace quadrature{
     void LogMapping(double& arR, double& arW);
     void LinearMapping(double& arR, double& arW, const double& arMin, const double& arMax);
     void GetPointGeneric(const int& arP, const int& arK, double& arR, double& arW, const std::function<void(double&,double&)>&);
     void GetPoint(const int& arP, const int& arK, double& arR, double& arW);
     double DoQuadrature(const std::function<double(const double&)>& arFunc, const int& arNstart , const int& arNmax,
          const std::function<void(double&,double&)>&, const double& arThresh = 1.0e-8);
  }
}

#endif

