#include <math.h>
#include <typeinfo>
#include <iostream>
#include "math/gamma.hpp" 
#include "math/affine.hpp"

template <int L> 
inline void OneEInts::details::CoulombIntegral_1c_MMDX(TripleX& TR, double k, const point & P, const point & N)
{
   vector3 R = N-P;
   double R2 = R*R;
   double Fn[L+1];
   RBoys boys_function;
   GetBoysFunction(boys_function,L);
   boys_function(k, R2, Fn); 
   RtuvX rtuv_functionX;  
   GetRtuvFunctionX(rtuv_functionX,L);
   rtuv_functionX(Fn,R,TR);
}

template <int L> 
inline void OneEInts::details::CoulombIntegral_1c_MMD(Triple& TR, double k, const point & P, const point & N)
{
   vector3 R = N-P;
   double R2 = R*R;
   double Fn[L+1];
   RBoys boys_function;
   GetBoysFunction(boys_function,L);
   boys_function(k, R2, Fn); 
   Rtuv rtuv_function;  
   GetRtuvFunction(rtuv_function,L);
   rtuv_function(Fn,R,TR);
}

template <int L> 
inline void OneEInts::details::CoulombIntegralExp_1c_MMD(Triple& TR, const double& k, const double& c, const point & P, const point & N)
{
   vector3 R = N-P;
   double R2 = R*R;
   double Fn[L+1];
   RExtBoys boys_ext_function;
   GetExtBoysFunction(boys_ext_function,L);
   boys_ext_function(k, R2, c, Fn); 
   Rtuv rtuv_function;  
   GetRtuvFunction(rtuv_function,L);
   rtuv_function(Fn,R,TR);
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Calculating Boys functions, 
// F_n =  int_{-infty}^{+infty} (k/k+u^2)^{3/2} (2* k * u^2/ (k+u^2) )^n du / sqrt{pi} ???
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<int L>
inline void OneEInts::details::CalculateBoys(const double& ark, const double& arR2, double arFn[])
{
   LibIGamma::IncompleteGamma.InitIncompleteGammaTable();
   double ** gamma = LibIGamma::IncompleteGamma.gamma_table;
   double *  gamma2 = LibIGamma::IncompleteGammas[L].gamma_table_vals;

   
   double kR2 = ark*arR2;

   if (kR2>LibIGamma::vg_max-LibIGamma::vg_step) {
       double iR2 = 1/arR2;
       arFn[0] = sqrt(iR2);

       for (int i=1; i<=L; ++i) {
           arFn[i] = arFn[i-1]*iR2*double(2*i-1);
       }
   }
   else {
       double p = LibIGamma::ivg_step*(kR2-LibIGamma::vg_min);
       int pos = int(p+0.5);
       double x0 = LibIGamma::vg_min + LibIGamma::vg_step*double(pos);
       double Ax1 = x0-kR2;
       double Ax2 = 0.5 * Ax1*Ax1;
       double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

       arFn[L] = (2/sqrt(PI))*(gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

       if (L>0) {
           double expx = (2/sqrt(PI))*gamma[pos][0] * (1+Ax1+Ax2+Ax3);

           for (int i=L-1; i>=0; --i)
               arFn[i] = (2*kR2*arFn[i+1] + expx)*(1./double(2*i+1));
       }

       double kn   = sqrt(ark);

       for (int n=0; n<=L; ++n) {
           arFn[n] *= kn;
           kn    *= 2*ark;
       }
   }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Calculating G_n (x) = k12^(3/2)/(k12+c) int_0^1 (t^2 + c/k12)^n e^{-x (t^2 - c/k12)} dt
//                      = k12^(3/2)/(k12+c) e^{-c/k12 x} 
//                            * sum_i^n ( n over i ) c^{i} F_(n-i)(x)
// where F_n (x) are the respective Boys functions. 
// The sum is certainly not desirable, but I didn't find another way to 
// Construct a function for which 
// dG_n(x)/dx = - G_{n+1}, which the McMurchie-Davidson scheme is based on. 
// input: 
// k, R2, c, as described above
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template<int L>
inline void OneEInts::details::CalculateExtendedBoys(const double& k12, const double& R2, 
       const double& c, double  arGn[])
{
   double k2=k12*k12;
   double s=k12+c;
   double is=1/s;
   k2*=is;
   double c2 = c/k12;
   double Fn[L+1];
   CalculateBoys<L>(k2,R2,Fn);
   double k_temp=1.0e0;
   double ik2_2 = 0.5e0*1/k2;
   for (int n = 0; n <= L ; ++n)
   {
      Fn[n] *=k_temp;
      k_temp *= ik2_2; 
   }
   double exponent = - k12*c*is*R2;
   double fac= exp(exponent);
   fac*=  sqrt(k12*is);
   for (int n=0 ; n <=L ; ++n)
   {
      arGn[n]=0.0e0;
      double k=1;
      for (int i = 0; i <= n ; ++i)
      {
         arGn[n] += binomialCoeff(n,i)*k*Fn[n-i];
         k*= c2; 
      }
      arGn[n] *= fac; 
      fac *= 2* k2;
   }   
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Function calculating the Rtuv Coulomb integrals for a given array of 
// Boys functions Fn[L] and vector for difference between two points 
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template <int L, typename T>
inline void OneEInts::details::ConstructRtuv(const double arFn[], const vector3& R, T& arRtuv)
{
    arRtuv[0][0][0] = arFn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) arRtuv[t][u][v] = R.z * arRtuv[t][u][v-1];
                            else           arRtuv[t][u][v] = R.z * arRtuv[t][u][v-1] -(v-1)*arRtuv[t][u][v-2];
                        }
                        else if (u==1) arRtuv[t][u][v] = R.y * arRtuv[t][u-1][v];
                        else           arRtuv[t][u][v] = R.y * arRtuv[t][u-1][v] -(u-1)*arRtuv[t][u-2][v];
                    }
                    else if (t==1) arRtuv[t][u][v] = R.x * arRtuv[t-1][u][v];
                    else           arRtuv[t][u][v] = R.x * arRtuv[t-1][u][v] -(t-1)*arRtuv[t-2][u][v];
                }
            }
        }

        arRtuv[0][0][0] = arFn[n];
    }
}


