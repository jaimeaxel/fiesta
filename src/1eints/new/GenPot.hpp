#include <tuple>
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"
#include "libechidna/libechidna.hpp"
#include "linear/newtensors.hpp"
#include "ExternalMultipolePotential.hpp"
#include "ModelCorePotential.hpp"

typedef double CartPotential [maxJ][maxJ][MMAX][MMAX];

typedef double Triple  [2*LMAX+1][2*LMAX+1][2*LMAX+1];
typedef double TripleX [2*LMAX+3][2*LMAX+3][2*LMAX+3];

namespace NewInts{
  namespace details{
      void GetP(const ShellPair& AB, const point& A, const point& B, const int&, const int&, point& P,  double& k12, double& ik12);
      void AddContrToCartPot(const ShellPair& AB, const vector3& PA, vector3& PB, Triple& R,
             const int& arA, const int& arB, const double& arRecCoeff, const double& wab,
             CartPotential& VV, const int& Ja, const int& Jb, const int& La, const int& Lb);
      void CalcMonopoleContributionToTriple(const int& Ltot, const int& npoints, const point *,
             const point&, const double&, const double* charges, Triple& R);
      void CalcDipoleContributionToTriple(const int& Ltot, const int& npoints, const point * points, const point& P, const double& k12, const vector3* dipoles, Triple& R);
      void Cart2Spher(const ShellPair & AB, const CartPotential& VV, symtensor & Vp);
      void CalcExpPartForMCP(const ShellPair & AB, const point& A, const point& B, const ModelCorePotential& MCP, symtensor & VMCP);
      void CalcCoulombExpContributionToTriple(const int& Ltot, const double& k12,
            const point& P, const ModelCorePotential& arMCP, Triple& R);
      void CalcProjForMCP(const ShellPair & AB, const point& A, const point& B, const ModelCorePotential& MCP, symtensor & VMCP, std::string);
      void CalcECPpart(const ShellPair & AB, const point& A, const point& B,  const ModelCorePotential& MCP, symtensor & VMCP);
      void CalcOverlaps(double (&SS)[2*LMAX+1][2*LMAX+1], const point& A, const point& B, double kA, double kB, int la, int lb);
  }
  void CalcMonoElectronic(const ShellPair & AB, const rAtom * nuclei, symtensor & S, symtensor & T, symtensor & X, symtensor & Y, symtensor & Z);

  void CalcPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, symtensor & Vp);
  void CalcExtMPPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, const ExternalMultipolePotential& ExMP, symtensor & XVp);

  void CalcMCPPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, const ModelCorePotential&, symtensor & Vp);


  void CalcMCPPotentialCart (const point & A, const point & B, const GTO & GTOA, const GTO & GTOB, const ModelCorePotential& MCP, tensor2 & VMCP,  int osa, int osb);
}
