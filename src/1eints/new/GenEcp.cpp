#include <cmath>
#include <iomanip>
#include "GenEcp.hpp"
#include "GenPot.hpp"
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"
#include "low/strings.hpp"   // excèption
#include "math/angular.hpp"
#include "math/math.hpp"
#include "math/modifiedbessel.hpp"
#include "OneEInts.hpp"


double RealSphValue(const int& arL, const int& arM, vector3 aVec)
{
   double N = norm(aVec);
   if (N < 1.0e-12 && (arL>0))
      return 0.0;
   else if (arL > 0)
      normalize(aVec);
   double value = 0.0;
   for (int j=0; j<LibAngular::SHList[arL][arM].nps; ++j) {
       const LibAngular::SHTerm & sh = LibAngular::SHList[arL][arM].T[j];
       double t = 1.0;
       if (sh.nx>0 )
         t *= std::pow(aVec.x,(int)sh.nx);
       if (sh.ny>0 )
         t *= std::pow(aVec.y,(int)sh.ny);
       if (sh.nz>0 )
         t *= std::pow(aVec.z,(int)sh.nz);
       value += sh.cN*t;
   }
   return value;
}

/* *****************************************************************
*  General ECP procedure according to J Comput Chem 27: 1009-1019, 2006.
* *****************************************************************/
void EcpInts::GenEcp(const ShellPair & AB, const rAtom * nuclei, int nnuclei, const EffectiveCorePotential& Ecp, symtensor & Vp)
{

   //Set points - same for all shell pairs, so should be moved out of here
   const int p1 = 24;
   const int p2 = 2*p1+1;
   const int p = 2*p2+1;
   const double eps = 1.0e-8;
   double r[p];
   double w[p];
   for (int k = 0; k < p; ++k)
   {
      quadrature::GetPoint(p,k+1,r[k],w[k]);
   }

   CartPotential VV;

   int La = AB.getLa();
   int Lb = AB.getLb();

   int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

   int Ja = AB.GP->Ja;
   int Jb = AB.GP->Jb;

   // explicitely initializing only those we will need, to save time??
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
               for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                   VV[ja][jb][i][j] = 0;
               }
           }
       }
   }
   point A = nuclei[AB.ata].c;
   point B = nuclei[AB.atb].c;

   //cout << "point A " << A.x << " " << A.y << " " << A.z  << endl;
   //cout << "point B " << B.x << " " << B.y << " " << B.z  << endl;
   for (int ecp_nuc = 0 ; ecp_nuc < Ecp.nnuclei; ++ecp_nuc)
   {
      point C = Ecp.positions[ecp_nuc];
      //cout << "point C " << C.x << " " << C.y << " " << C.z  << endl;
      //cout << " do ECP center " << ecp_nuc << endl;
      int maxl =0;
      for (auto i :Ecp.parameter[ecp_nuc])
         maxl = std::max(maxl,(int)i.l);

      int maxLa = La + maxl;
      int maxLb = Lb + maxl;
      vector3 A_C = A-C;
      vector3 B_C = B-C;
      // pretabulations Fb
      double BC= norm(B-C);
      double Fb[maxLb+1][p][Jb];

      for (int lb = 0; lb <= maxLb; ++lb)
      {
         for (int k = 0; k < p; ++k)
         {
            for (int jb = 0 ; jb < Jb ; ++jb)
               Fb[lb][k][jb] = 0.0;
            for (int b=0; b<AB.GP->Kb; ++b) // sum over all
            {
               double kB  = AB.GP->kb[b];
               double z = 2.0* kB * BC * r[k];
               double e = r[k] - BC;
               e *= e;
               e *= - kB;
               double v = math::bessel::ModifiedBesselEcp(lb,z)* std::exp(e);
               for (int jb = 0 ; jb < Jb ; ++jb)
               {
                  Fb[lb][k][jb] += AB.GP->Nb[jb][b]*v;
                  //Fb[lb][k][jb] += v;
               }
            }
         }
      }

      // pretabulations Fa
      double AC= norm(A-C);
      double Fa[maxLa+1][p][Ja];

      for (int la = 0; la <= maxLa; ++la)
      {
         for (int k = 0; k < p; ++k)
         {
            for (int ja = 0 ; ja < Ja ; ++ja)
               Fa[la][k][ja] = 0.0;
            for (int a=0; a<AB.GP->Ka; ++a) // sum over all
            {
               double kA  = AB.GP->ka[a];
               double z = 2.0* kA * AC * r[k];
               double e = r[k] - AC;
               e *= e;
               e *= - kA;
               double v = math::bessel::ModifiedBesselEcp(la,z)* std::exp(e);
               for (int ja = 0 ; ja < Ja ; ++ja)
               {
                  Fa[la][k][ja] += AB.GP->Na[ja][a]*v;
                  //Fa[la][k][ja] += v;
               }
               //std::cout << " modified bessel " <<  la << " " <<  z <<  " " <<  details::ModifiedBesselKoester(la,z) << endl;
               //std::cout << "r  e e^e " << r[k] <<  e << " " << c* std::exp(e) << endl;

            }
           // std::cout << "Fa[" << la << " , " << k << " , " << ja << "] " <<  Fa[la][k] << endl;
         }
      }

      // pretabulations r^N U_l(r)
      int N = La+Lb;
      double U[Ecp.parameter[ecp_nuc].size()][p][N+1];
      for (int i = 0; i < Ecp.parameter[ecp_nuc].size(); ++i)
      {
         for (int k = 0; k < p; ++k)
         {
            for (int nn = 0 ; nn <= N; ++nn)
               U[i][k][nn] = 0.0;
            for (int e = 0 ; e < Ecp.parameter[ecp_nuc].at(i).K; ++e)
            {
               double c = Ecp.parameter[ecp_nuc].at(i).N[0][e]*std::exp(-Ecp.parameter[ecp_nuc].at(i).k[e]*r[k]*r[k]);
               //double c = std::exp(-Ecp.parameter[ecp_nuc].at(i).k[e]*r[k]*r[k]);
               c *= std::pow(r[k],Ecp.parameter[ecp_nuc].at(i).Nexp-2);
               double rn = 1;
               for (int nn = 0 ; nn <= N; ++nn)
               {
                  U[i][k][nn] +=c*rn;
                  rn *= r[k];
               }
            }
            //for (int nn = 0 ; nn <= N; ++nn)
            //cout << " U[l][k][nn] " << l << " " << k << " " << nn << " " <<  U[l][k][nn] << endl;
         }
      }
      // quadrature sums
      double T1[Ecp.parameter[ecp_nuc].size()][N+1][maxLa+1][maxLb+1][Ja][Jb];
      double T2[Ecp.parameter[ecp_nuc].size()][N+1][maxLa+1][maxLb+1][Ja][Jb];
      double T3[Ecp.parameter[ecp_nuc].size()][N+1][maxLa+1][maxLb+1][Ja][Jb];
      double r1[p1];
      double w1[p1];
      for (int k = 0; k < p1; ++k)
      {
         quadrature::GetPoint(p1,k+1,r1[k],w1[k]);
      }
      double r2[p2];
      double w2[p2];
      for (int k = 0; k < p2; ++k)
      {
         quadrature::GetPoint(p2,k+1,r2[k],w2[k]);
      }

      //std::cout << endl;
      for (int i = 0; i < Ecp.parameter[ecp_nuc].size(); ++i)
      {
         int l =  Ecp.parameter[ecp_nuc].at(i).l;
         for (int la = 0 ; la <= (La+l); ++la)
         {
            for (int lb = 0 ; lb <= (Lb+l); ++lb)
            {
               for (int nn = 0 ; nn <= N; ++nn)
               {
                  for (int ja = 0; ja < Ja ; ++ja)
                  {
                     for (int jb = 0; jb < Jb ; ++jb)
                     {
                        T1[i][nn][la][lb][ja][jb] = 0.0;
                        for (int k = 0; k < p1; ++k)
                        {
                           int s = 4*k+3;
                           T1[i][nn][la][lb][ja][jb] += Fa[la][s][ja]*Fb[lb][s][jb]*U[i][s][nn]*w1[k]*r1[k]*r1[k];
                        }
                        T2[i][nn][la][lb][ja][jb] = 0.5*  T1[i][nn][la][lb][ja][jb];
                        for (int k = 0; 4*k+1 < p; ++k)
                        {
                           int s = 4*k+1;
                           T2[i][nn][la][lb][ja][jb] += Fa[la][s][ja]*Fb[lb][s][jb]*U[i][s][nn]*w2[2*k]*r2[2*k]*r2[2*k];
                        }
                        T3[i][nn][la][lb][ja][jb] = 0.5*  T2[i][nn][la][lb][ja][jb];
                        for (int k = 0; 2*k < p; ++k)
                        {
                           int s = 2*k;
                           T3[i][nn][la][lb][ja][jb] += Fa[la][s][ja]*Fb[lb][s][jb]*U[i][s][nn]*w[s]*r[s]*r[s];
                        }


                        double c = T3[i][nn][la][lb][ja][jb]-T2[i][nn][la][lb][ja][jb];
                        c *=c;
                        if (0) //(c > (fabs(T3[i][nn][la][lb][ja][jb]-T1[i][nn][la][lb][ja][jb])*eps))
                        {
                           throw Exception{" simple quadrature did not converged -- special one still to instable to be used "};
                           //T3[i][nn][la][lb][ja][jb] = SpecialQuadrature(AB,nuclei,Ecp,ecp_nuc,i,nn,la,lb,ja,jb);
                        }
                        //if (T3[i][nn][la][lb][ja][jb] > 0.0001)
                        //   std::cout << "compare: " << std::scientific <<  T3[i][nn][la][lb][ja][jb] << " " << SpecialQuadrature(AB,nuclei,Ecp,ecp_nuc,i,nn,la,lb,ja,jb) << std::endl;
                     }
                  }
               }
            }
         }
      }
      // getting everything together - by far the maximal number of nested loops that I have produced..
      double CAX[LMAX+1][LMAX+1];
      double CAY[LMAX+1][LMAX+1];
      double CAZ[LMAX+1][LMAX+1];
      double CBX[LMAX+1][LMAX+1];
      double CBY[LMAX+1][LMAX+1];
      double CBZ[LMAX+1][LMAX+1];

      for (int la = 0; la <=La; ++la)
      {
         for (int a = 0; a <= la; ++a)
         {
            CAX[la][a] = binomialCoeff(la,a)* math::pow(A_C.x,la-a);
            CAY[la][a] = binomialCoeff(la,a)* math::pow(A_C.y,la-a);
            CAZ[la][a] = binomialCoeff(la,a)* math::pow(A_C.z,la-a);
         }
      }

      for (int lb = 0; lb <=Lb; ++lb)
      {
         for (int b = 0; b <= lb; ++b)
         {
            CBX[lb][b] = binomialCoeff(lb,b)* math::pow(B_C.x,lb-b);
            CBY[lb][b] = binomialCoeff(lb,b)* math::pow(B_C.y,lb-b);
            CBZ[lb][b] = binomialCoeff(lb,b)* math::pow(B_C.z,lb-b);
         }
      }



      for (int ia = 0; ia < LibAngular::nmC[La]; ++ia)
      {
         LibAngular::SHTerm& sha = LibAngular::CartList[La][ia];
         for (int ib = 0; ib < LibAngular::nmC[Lb]; ++ib)
         {
            //cout << " ecp_nuc " << ecp_nuc << endl;
            //cout << "  Ecp.parameter[ecp_nuc].size() " <<  Ecp.parameter[ecp_nuc].size() << endl;
            LibAngular::SHTerm& shb = LibAngular::CartList[Lb][ib];
            for(int i=0 ; i < Ecp.parameter[ecp_nuc].size(); ++i)
            {
               int l= Ecp.parameter[ecp_nuc].at(i).l;
               int fac = 1; //LibAngular::nmS[l];
               //std::cout << " l= Ecp.parameter[ecp_nuc].at(i).l " << l << endl;
               for(int m=0 ; m < 2*l+1 ; ++m)
               {
                  for(int ax=0 ; ax <= sha.nx ; ++ax)
                  {
                     double cax = CAX[sha.nx][ax]; // binomialCoeff(sha.nx,ax)* std::pow(A_C.x,sha.nx-ax);
                     for(int ay=0 ; ay <= (int)sha.ny; ++ay)
                     {
                        double cay = CAY[sha.ny][ay]; // = binomialCoeff(sha.ny,ay)* std::pow(A_C.y,sha.ny-ay);
                        for(int az=0 ; az <= (int)sha.nz; ++az)
                        {
                           double caz = CAZ[sha.nz][az]; // = binomialCoeff(sha.nz,az)* std::pow(A_C.z,sha.nz-az);
                           int a = ax+ay+az;
                           for(int bx=0 ; bx <= (int)shb.nx; ++bx)
                           {
                              double cbx = CBX[shb.nx][bx]; // = binomialCoeff(shb.nx,bx)* std::pow(B_C.x,shb.nx-bx);
                              for(int by=0 ; by <= (int)shb.ny; ++by)
                              {
                                 double cby = CBY[shb.ny][by]; //  = binomialCoeff(shb.ny,by)* std::pow(B_C.y,shb.ny-by);
                                 for(int bz=0 ; bz <= (int)shb.nz; ++bz)
                                 {
                                    double cbz = CBZ[shb.nz][bz]; //  = binomialCoeff(shb.nz,bz)* std::pow(B_C.z,shb.nz-bz);
                                    int b = bx+by+bz;
                                    int ab = a+b;
                                    double sign = (La+Lb-ab)%2?-1.:1.; // std::pow(-1,La+Lb-ab);
                                    for (int la2 = 0 ; la2 <= l+a; ++la2)
                                    {
                                       double ang_a=0.0;
                                       for (int ma2 = 0 ; ma2 < 2*la2+1 ; ++ma2)
                                       {
                                          ang_a +=  LibAngular::Omega[l][m][la2][ma2][ax][ay][az]*RealSphValue(la2,ma2,A_C);
                                          //std::cout << " realsph "  << la2 << " " << ma2 << " " <<   RealSphValue(la2,ma2,A_C) << " " << norm (A_C) << endl;
                                       }
                                       //ang_a *=LibAngular::nmS[la2];
                                       for (int lb2 = 0 ; lb2 <= l+b; ++lb2)
                                       {
                                          double ang_b=0.0;
                                          for (int mb2 = 0 ; mb2 < 2*lb2+1 ; ++mb2)
                                          {
                                             ang_b += LibAngular::Omega[l][m][lb2][mb2][bx][by][bz]*RealSphValue(lb2,mb2,B_C);
                                          }
                                          //ang_b *=LibAngular::nmS[lb2];

                                          for (int ja=0; ja < Ja; ++ja)
                                          {
                                             for (int jb=0; jb < Jb; ++jb)
                                             {
                                                VV[ja][jb][ia][ib]+= fac*sign*cax*cay*caz*cbx*cby*cbz*ang_a*ang_b*T3[i][ab][la2][lb2][ja][jb];
                                                //double add = sign*cax*cay*caz*cbx*cby*cbz*ang_a*ang_b*T3[i][ab][la2][lb2][ja][jb];
                                                //if (fabs(add) < -1e-10) //std::cout << " CHECK HERE : " ;
                                                {
                                                   //std::cout << " found something" << endl;
                                                   //std::cout << la2 << " " << lb2 << " "  << m << " " << a << " " << b << endl;
                                                   //std::cout << sign << " " << cax << " " << cay << " " << caz << " " << cbx << " " << cby << " " << cbz << " " << ang_a << " " << ang_b << " " << T3[i][ab][la2][lb2][ja][jb] <<  " " <<  VV[ja][jb][ia][ib] << endl;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   } // loop over ECP centers

   double fac = 16.0*PI*PI; //* sqrt((LibAngular::nmS[La])*(LibAngular::nmS[Lb]));
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
               for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                   VV[ja][jb][i][j] *= fac;
                   //std::cout << std::scientific <<" final value " << ja << " " << jb << " " << i << " " << j << " " << VV[ja][jb][i][j] << endl;
               }
           }
       }
   }



   int ta = AB.ata;
   int tb = AB.atb;

   for (int ja=0; ja<Ja; ++ja)
   {
       for (int jb=0; jb<Jb; ++jb)
       {
           for (int a=0; a<LibAngular::nmS[La]; ++a)
           {
               for (int b=0; b<LibAngular::nmS[Lb]; ++b)
               {
                   int ffa = AB.getFa() + ja*LibAngular::nmS[La] + a;
                   int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + b;

                   double sumV = 0;
                   for (int i=0; i<LibAngular::SHList[La][a].nps; ++i) {
                       for (int j=0; j<LibAngular::SHList[Lb][b].nps; ++j) {
                           const LibAngular::SHTerm & sha = LibAngular::SHList[La][a].T[i];
                           const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][b].T[j];
                           sumV += sha.cN*shb.cN* VV[ja][jb][sha.nc][shb.nc];
                       }
                   }

                   Vp(ta,tb, ffa,ffb) = sumV;
               }
           }
       }
   }

}

/* *********************************************
*  jars : modified interface for the ECP routine
* **********************************************/
void EcpInts::GenEcp (const point & A, const point & B, const GTO & GTOA, const GTO & GTOB,    const EffectiveCorePotential& Ecp, tensor2 & V, int osa, int osb)
{

   //Set points - same for all shell pairs, so should be moved out of here
   const int p1 = 24;
   const int p2 = 2*p1+1;
   const int p = 2*p2+1;
   const double eps = 1.0e-8;
   double r[p];
   double w[p];

   for (int k = 0; k < p; ++k)
   {
      quadrature::GetPoint(p,k+1,r[k],w[k]);
   }

   CartPotential VV;

   int La = GTOA.l; // AB.getLa();
   int Lb = GTOB.l; // AB.getLb();

   int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

   int Ja = GTOA.J; //AB.GP->Ja;
   int Jb = GTOB.J; //AB.GP->Jb;

   // explicitely initializing only those we will need, to save time??
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
               for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                   VV[ja][jb][i][j] = 0;
               }
           }
       }
   }


   //cout << "point A " << A.x << " " << A.y << " " << A.z  << endl;
   //cout << "point B " << B.x << " " << B.y << " " << B.z  << endl;
   for (int ecp_nuc = 0 ; ecp_nuc < Ecp.nnuclei; ++ecp_nuc)
   {
      point C = Ecp.positions[ecp_nuc];
      //cout << "point C " << C.x << " " << C.y << " " << C.z  << endl;
      //cout << " do ECP center " << ecp_nuc << endl;
      int maxl =0;
      for (auto i :Ecp.parameter[ecp_nuc])
         maxl = std::max(maxl,(int)i.l);

      int maxLa = La + maxl;
      int maxLb = Lb + maxl;
      vector3 A_C = A-C;
      vector3 B_C = B-C;
      // pretabulations Fb
      double BC= norm(B-C);
      double Fb[maxLb+1][p][Jb];

      for (int lb = 0; lb <= maxLb; ++lb)
      {
         for (int k = 0; k < p; ++k)
         {
            for (int jb = 0 ; jb < Jb ; ++jb)
               Fb[lb][k][jb] = 0.0;
            for (int b=0; b<GTOB.K; ++b) // sum over all
            {
               double kB  = GTOB.k[b]; // AB.GP->kb[b];
               double z = 2.0* kB * BC * r[k];
               double e = r[k] - BC;
               e *= e;
               e *= - kB;
               double v = math::bessel::ModifiedBesselEcp(lb,z)* std::exp(e);
               for (int jb = 0 ; jb < Jb ; ++jb)
               {
                  Fb[lb][k][jb] += GTOB.N[jb][b] * v; //  AB.GP->Nb[jb][b]*v;
                  //Fb[lb][k][jb] += v;
               }
            }
         }
      }

      // pretabulations Fa
      double AC= norm(A-C);
      double Fa[maxLa+1][p][Ja];

      for (int la = 0; la <= maxLa; ++la)
      {
         for (int k = 0; k < p; ++k)
         {
            for (int ja = 0 ; ja < Ja ; ++ja)
               Fa[la][k][ja] = 0.0;
            for (int a=0; a<GTOA.K; ++a) // sum over all
            {
               double kA  = GTOA.k[a]; //AB.GP->ka[a];
               double z = 2.0* kA * AC * r[k];
               double e = r[k] - AC;
               e *= e;
               e *= - kA;
               double v = math::bessel::ModifiedBesselEcp(la,z)* std::exp(e);
               for (int ja = 0 ; ja < Ja ; ++ja)
               {
                  Fa[la][k][ja] += GTOA.N[ja][a] * v; // AB.GP->Na[ja][a]*v;
                  //Fa[la][k][ja] += v;
               }
               //std::cout << " modified bessel " <<  la << " " <<  z <<  " " <<  details::ModifiedBesselKoester(la,z) << endl;
               //std::cout << "r  e e^e " << r[k] <<  e << " " << c* std::exp(e) << endl;

            }
           // std::cout << "Fa[" << la << " , " << k << " , " << ja << "] " <<  Fa[la][k] << endl;
         }
      }

      // pretabulations r^N U_l(r)
      int N = La+Lb;
      double U[Ecp.parameter[ecp_nuc].size()][p][N+1];
      for (int i = 0; i < Ecp.parameter[ecp_nuc].size(); ++i)
      {
         for (int k = 0; k < p; ++k)
         {
            for (int nn = 0 ; nn <= N; ++nn)
               U[i][k][nn] = 0.0;
            for (int e = 0 ; e < Ecp.parameter[ecp_nuc].at(i).K; ++e)
            {
               double c = Ecp.parameter[ecp_nuc].at(i).N[0][e]*std::exp(-Ecp.parameter[ecp_nuc].at(i).k[e]*r[k]*r[k]);
               //double c = std::exp(-Ecp.parameter[ecp_nuc].at(i).k[e]*r[k]*r[k]);
               c *= std::pow(r[k],Ecp.parameter[ecp_nuc].at(i).Nexp-2);
               double rn = 1;
               for (int nn = 0 ; nn <= N; ++nn)
               {
                  U[i][k][nn] +=c*rn;
                  rn *= r[k];
               }
            }
            //for (int nn = 0 ; nn <= N; ++nn)
            //cout << " U[l][k][nn] " << l << " " << k << " " << nn << " " <<  U[l][k][nn] << endl;
         }
      }
      // quadrature sums
      double T1[Ecp.parameter[ecp_nuc].size()][N+1][maxLa+1][maxLb+1][Ja][Jb];
      double T2[Ecp.parameter[ecp_nuc].size()][N+1][maxLa+1][maxLb+1][Ja][Jb];
      double T3[Ecp.parameter[ecp_nuc].size()][N+1][maxLa+1][maxLb+1][Ja][Jb];
      double r1[p1];
      double w1[p1];
      for (int k = 0; k < p1; ++k)
      {
         quadrature::GetPoint(p1,k+1,r1[k],w1[k]);
      }
      double r2[p2];
      double w2[p2];
      for (int k = 0; k < p2; ++k)
      {
         quadrature::GetPoint(p2,k+1,r2[k],w2[k]);
      }

      //std::cout << endl;
      for (int i = 0; i < Ecp.parameter[ecp_nuc].size(); ++i)
      {
         int l =  Ecp.parameter[ecp_nuc].at(i).l;
         for (int la = 0 ; la <= (La+l); ++la)
         {
            for (int lb = 0 ; lb <= (Lb+l); ++lb)
            {
               for (int nn = 0 ; nn <= N; ++nn)
               {
                  for (int ja = 0; ja < Ja ; ++ja)
                  {
                     for (int jb = 0; jb < Jb ; ++jb)
                     {
                        T1[i][nn][la][lb][ja][jb] = 0.0;
                        for (int k = 0; k < p1; ++k)
                        {
                           int s = 4*k+3;
                           T1[i][nn][la][lb][ja][jb] += Fa[la][s][ja]*Fb[lb][s][jb]*U[i][s][nn]*w1[k]*r1[k]*r1[k];
                        }
                        T2[i][nn][la][lb][ja][jb] = 0.5 *  T1[i][nn][la][lb][ja][jb];
                        for (int k = 0; 4*k+1 < p; ++k)
                        {
                           int s = 4*k+1;
                           T2[i][nn][la][lb][ja][jb] += Fa[la][s][ja]*Fb[lb][s][jb]*U[i][s][nn]*w2[2*k]*r2[2*k]*r2[2*k];
                        }
                        T3[i][nn][la][lb][ja][jb] = 0.5*  T2[i][nn][la][lb][ja][jb];
                        for (int k = 0; 2*k < p; ++k)
                        {
                           int s = 2*k;
                           T3[i][nn][la][lb][ja][jb] += Fa[la][s][ja]*Fb[lb][s][jb]*U[i][s][nn]*w[s]*r[s]*r[s];
                        }


                        double c = T3[i][nn][la][lb][ja][jb]-T2[i][nn][la][lb][ja][jb];
                        c *=c;
                        if (0) //(c > (fabs(T3[i][nn][la][lb][ja][jb]-T1[i][nn][la][lb][ja][jb])*eps))
                        {
                           throw Exception{" simple quadrature did not converged -- special one still to instable to be used "};
                           //T3[i][nn][la][lb][ja][jb] = SpecialQuadrature(A,B, GTOA, GTOB, Ecp, ecp_nuc, i, nn, la, lb, ja, jb);
                        }
                        //if (T3[i][nn][la][lb][ja][jb] > 0.0001)
                        //   std::cout << "compare: " << std::scientific <<  T3[i][nn][la][lb][ja][jb] << " " << SpecialQuadrature(AB,nuclei,Ecp,ecp_nuc,i,nn,la,lb,ja,jb) << std::endl;
                     }
                  }
               }
            }
         }
      }
      // getting everything together - by far the maximal number of nested loops that I have produced..
      double CAX[LMAX+1][LMAX+1];
      double CAY[LMAX+1][LMAX+1];
      double CAZ[LMAX+1][LMAX+1];
      double CBX[LMAX+1][LMAX+1];
      double CBY[LMAX+1][LMAX+1];
      double CBZ[LMAX+1][LMAX+1];

      for (int la = 0; la <=La; ++la)
      {
         for (int a = 0; a <= la; ++a)
         {
            CAX[la][a] = binomialCoeff(la,a)* math::pow(A_C.x,la-a);
            CAY[la][a] = binomialCoeff(la,a)* math::pow(A_C.y,la-a);
            CAZ[la][a] = binomialCoeff(la,a)* math::pow(A_C.z,la-a);
         }
      }

      for (int lb = 0; lb <=Lb; ++lb)
      {
         for (int b = 0; b <= lb; ++b)
         {
            CBX[lb][b] = binomialCoeff(lb,b)* math::pow(B_C.x,lb-b);
            CBY[lb][b] = binomialCoeff(lb,b)* math::pow(B_C.y,lb-b);
            CBZ[lb][b] = binomialCoeff(lb,b)* math::pow(B_C.z,lb-b);
         }
      }



      for (int ia = 0; ia < LibAngular::nmC[La]; ++ia)
      {
         LibAngular::SHTerm& sha = LibAngular::CartList[La][ia];
         for (int ib = 0; ib < LibAngular::nmC[Lb]; ++ib)
         {
            //cout << " ecp_nuc " << ecp_nuc << endl;
            //cout << "  Ecp.parameter[ecp_nuc].size() " <<  Ecp.parameter[ecp_nuc].size() << endl;
            LibAngular::SHTerm& shb = LibAngular::CartList[Lb][ib];
            for(int i=0 ; i < Ecp.parameter[ecp_nuc].size(); ++i)
            {
               int l= Ecp.parameter[ecp_nuc].at(i).l;
               int fac = 1; //LibAngular::nmS[l];
               //std::cout << " l= Ecp.parameter[ecp_nuc].at(i).l " << l << endl;
               for(int m=0 ; m < 2*l+1 ; ++m)
               {
                  for(int ax=0 ; ax <= sha.nx ; ++ax)
                  {
                     double cax = CAX[sha.nx][ax]; // binomialCoeff(sha.nx,ax)* std::pow(A_C.x,sha.nx-ax);
                     for(int ay=0 ; ay <= (int)sha.ny; ++ay)
                     {
                        double cay = CAY[sha.ny][ay]; // = binomialCoeff(sha.ny,ay)* std::pow(A_C.y,sha.ny-ay);
                        for(int az=0 ; az <= (int)sha.nz; ++az)
                        {
                           double caz = CAZ[sha.nz][az]; // = binomialCoeff(sha.nz,az)* std::pow(A_C.z,sha.nz-az);
                           int a = ax+ay+az;
                           for(int bx=0 ; bx <= (int)shb.nx; ++bx)
                           {
                              double cbx = CBX[shb.nx][bx]; // = binomialCoeff(shb.nx,bx)* std::pow(B_C.x,shb.nx-bx);
                              for(int by=0 ; by <= (int)shb.ny; ++by)
                              {
                                 double cby = CBY[shb.ny][by]; //  = binomialCoeff(shb.ny,by)* std::pow(B_C.y,shb.ny-by);
                                 for(int bz=0 ; bz <= (int)shb.nz; ++bz)
                                 {
                                    double cbz = CBZ[shb.nz][bz]; //  = binomialCoeff(shb.nz,bz)* std::pow(B_C.z,shb.nz-bz);
                                    int b = bx+by+bz;
                                    int ab = a+b;
                                    double sign = (La+Lb-ab)%2?-1.:1.; // std::pow(-1,La+Lb-ab);
                                    for (int la2 = 0 ; la2 <= l+a; ++la2)
                                    {
                                       double ang_a=0.0;
                                       for (int ma2 = 0 ; ma2 < 2*la2+1 ; ++ma2)
                                       {
                                          ang_a +=  LibAngular::Omega[l][m][la2][ma2][ax][ay][az]*RealSphValue(la2,ma2,A_C);
                                          //std::cout << " realsph "  << la2 << " " << ma2 << " " <<   RealSphValue(la2,ma2,A_C) << " " << norm (A_C) << endl;
                                       }
                                       //ang_a *=LibAngular::nmS[la2];
                                       for (int lb2 = 0 ; lb2 <= l+b; ++lb2)
                                       {
                                          double ang_b=0.0;
                                          for (int mb2 = 0 ; mb2 < 2*lb2+1 ; ++mb2)
                                          {
                                             ang_b += LibAngular::Omega[l][m][lb2][mb2][bx][by][bz]*RealSphValue(lb2,mb2,B_C);
                                          }
                                          //ang_b *=LibAngular::nmS[lb2];

                                          for (int ja=0; ja < Ja; ++ja)
                                          {
                                             for (int jb=0; jb < Jb; ++jb)
                                             {
                                                VV[ja][jb][ia][ib]+= fac*sign*cax*cay*caz*cbx*cby*cbz*ang_a*ang_b*T3[i][ab][la2][lb2][ja][jb];
                                                //double add = sign*cax*cay*caz*cbx*cby*cbz*ang_a*ang_b*T3[i][ab][la2][lb2][ja][jb];
                                                //if (fabs(add) < -1e-10) //std::cout << " CHECK HERE : " ;
                                                {
                                                   //std::cout << " found something" << endl;
                                                   //std::cout << la2 << " " << lb2 << " "  << m << " " << a << " " << b << endl;
                                                   //std::cout << sign << " " << cax << " " << cay << " " << caz << " " << cbx << " " << cby << " " << cbz << " " << ang_a << " " << ang_b << " " << T3[i][ab][la2][lb2][ja][jb] <<  " " <<  VV[ja][jb][ia][ib] << endl;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   } // loop over ECP centers

   double fac = 16*PI*PI; //* sqrt((LibAngular::nmS[La])*(LibAngular::nmS[Lb]));
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
               for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                   VV[ja][jb][i][j] *= fac;
                   //std::cout << std::scientific <<" final value " << ja << " " << jb << " " << i << " " << j << " " << VV[ja][jb][i][j] << endl;
               }
           }
       }
   }




   for (int ja=0; ja<Ja; ++ja)
   {
       for (int jb=0; jb<Jb; ++jb)
       {
           for (int a=0; a<LibAngular::nmS[La]; ++a)
           {
               for (int b=0; b<LibAngular::nmS[Lb]; ++b)
               {
                   int ffa = osa + ja*LibAngular::nmS[La] + a;
                   int ffb = osb + jb*LibAngular::nmS[Lb] + b;

                   double sumV = 0;
                   for (int i=0; i<LibAngular::SHList[La][a].nps; ++i) {
                       for (int j=0; j<LibAngular::SHList[Lb][b].nps; ++j) {
                           const LibAngular::SHTerm & sha = LibAngular::SHList[La][a].T[i];
                           const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][b].T[j];
                           sumV += sha.cN *shb.cN * VV[ja][jb][sha.nc][shb.nc];
                       }
                   }

                   V(ffa,ffb) = sumV;
               }
           }
       }
   }

}



/* *****************************************************************
*  Returns r  and weight for quadrature according to
*          Eq. (25), Eq. (26), and Eq. (27)  in J Comput Chem 27: 1009-1019, 2006.
* *****************************************************************/
void EcpInts::quadrature::GetPointGeneric(const int& arP, const int& arK, double& arR, double& arW, const std::function<void(double&,double&)>& arMappingFunction)
{
  int p1 = arP+1;
  arR = p1-2*arK;
  arR /= p1;
  double arg = arK*PI/p1;
  double s = std::sin(arg);
  double c = std::cos(arg);
  arR += 2.0/PI*(1.0+2.0/3.0*s*s)*s*c;
  arW = 16.0*s*s*s*s;
  arW /= 3*p1;

  arMappingFunction(arR,arW);
  return;
}

void EcpInts::quadrature::LogMapping(double& arR, double& arW)
{
   arW /= std::log(2.0)*(1-arR);
   arR = std::log(2.0/(1.0-arR))/std::log(2.0);
   return;
}


void EcpInts::quadrature::GetPoint(const int& arP, const int& arK, double& arR, double& arW)
{
   int p1 = arP+1;
   arR = p1-2*arK;
   arR /= p1;
   double arg = arK*PI/p1;
   double s = std::sin(arg);
   double c = std::cos(arg);
   arR += 2.0/PI*(1.0+2.0/3.0*s*s)*s*c;
   arW = 16.0*s*s*s*s;
   arW /= 3*p1*std::log(2.0)*(1-arR);
   arR = std::log(2.0/(1.0-arR))/std::log(2.0);
   //GetPointGeneric(arP,arK,arR,arW,EcpInts::quadrature::LogMapping);
}

void EcpInts::quadrature::LinearMapping(double& arR, double& arW, const double& arMin, const double& arMax)
{

  double diff = arMax-arMin;
  arR  = diff*arR;
  arR += arMax+arMin;
  arR *= 0.5;
  arW *= diff/2.0;
  return;
}

double SpFunction(const double& arR, const int& arL1, const int& arL2, const int& arN, const double& arS1, const double& arS2, const double& arEta)
{
   {
     throw Exception("Do not use SpFunction in its current from! The Bessel functions are too instable");
   }
   double value = math::pow(arR,arN);
   //value *=math::bessel::Menz(arL1,arS1*arR,0.5*arEta*arR*arR);
   //value *=math::bessel::Menz(arL2,arS2*arR,0.5*arEta*arR*arR);
   //std::cout << std::scientific << " integrand " << arR << " : " << math::pow(arR,arN) << " * " << math::bessel::Menz(arL1,arS1*arR,0.5*arEta*arR*arR) << " * " << math::bessel::Menz(arL2,arS2*arR,0.5*arEta*arR*arR) << " = " << value << std::endl;
   return value;
}


double EcpInts::SpecialQuadrature(const ShellPair& AB, const rAtom * nuclei, const EffectiveCorePotential& arEcp, const int& arEnuc, const int& arI, const int& arNn, const int& arLa, const int& arLb, const int& arJa, const int& arJb)
{
   const double eps = 1.0e-8;
   point A = nuclei[AB.ata].c;
   point B = nuclei[AB.atb].c;
   point C = arEcp.positions[arEnuc];
   double AC= norm(A-C);
   double BC= norm(B-C);
   //double Q[AB.GP->Ka][AB.GP->Kb][arEcp.parameter[arEnuc].at(arI).K];
   int n = arNn + arEcp.parameter[arEnuc].at(arI).Nexp;// + 2;
   //int n = rNn + 2;
   double value = 0.0;
   for (int a=0; a<AB.GP->Ka; ++a)
   {
      //double limit_a = sqrt(1.0/AB.GP->ka[a]*std::log(fabs(AB.GP->Na[arJa][a])/eps));
      //std::cout << " limit_a " << limit_a << endl;
      //std::cout << " ka " << AB.GP->ka[a] << endl;
      //std::cout << " AC "  << AC << endl;
      //std::cout << " Na " << AB.GP->Na[arJa][a] << endl;
      double s1=2.0*AB.GP->ka[a]*AC;
      for (int b=0; b<AB.GP->Kb; ++b)
      {
         //std::cout << " kb " << AB.GP->kb[b] << endl;
         //std::cout << " BC "  << BC << endl;
         //std::cout << " Nb " << AB.GP->Na[arJb][b] << endl;
         //double limit_b = sqrt(1.0/AB.GP->kb[b]*std::log(fabs(AB.GP->Nb[arJb][b])/eps));
         //std::cout << " limit_b " << limit_b << endl;
         double s2=2.0*AB.GP->kb[b]*BC;
         for (int e = 0 ; e < arEcp.parameter[arEnuc].at(arI).K; ++e)
         {
            //std::cout << " Nl " << arEcp.parameter[arEnuc].at(arI).N[0][e] << endl;
            double eta=AB.GP->ka[a]+AB.GP->kb[b]+arEcp.parameter[arEnuc].at(arI).k[e];
            double C = AB.GP->ka[a]*AC + AB.GP->kb[b]*BC;
            C /= eta;
            double limit1 = std::max(0.0,C-0.5/sqrt(eta));
            limit1 = std::max(limit1,0.0);
            double limit2 = C+0.5/sqrt(eta);
            std::function<double(const double&)> func = std::bind(SpFunction,std::placeholders::_1,arLa,arLb,n,s1,s2,eta);
            std::function<void(double&,double&)> mapfunc = std::bind(quadrature::LinearMapping,std::placeholders::_1,std::placeholders::_2,limit1,limit2);
            std::cout << " function parameter " << arLa << " " << arLb<< " " << n << " " << s1  << " " << s2 << " " << eta << std::endl;
            double tmp = EcpInts::quadrature::DoQuadrature(func,24,2000,mapfunc);
            tmp *= arEcp.parameter[arEnuc].at(arI).N[0][e];
            tmp *= AB.GP->Na[arJa][a];
            tmp *= AB.GP->Nb[arJb][b];
            double ex = AB.GP->ka[a]*AC*AC + AB.GP->kb[b]*BC*BC;
            value = tmp * std::exp(-ex);
         }
      }
   }
   return value;
}

double EcpInts::SpecialQuadrature (const point & A, const point & B, const GTO & GTOA, const GTO & GTOB,  const EffectiveCorePotential& arEcp, int arEnuc, int arI, int arNn,   int La, int Lb, int Ja, int Jb)
{
   const double eps = 1e-8;

   point C = arEcp.positions[arEnuc];
   double AC = norm(A-C);
   double BC = norm(B-C);

   //double Q[AB.GP->Ka][AB.GP->Kb][arEcp.parameter[arEnuc].at(arI).K];
   int n = arNn + arEcp.parameter[arEnuc].at(arI).Nexp;// + 2;

   //int n = rNn + 2;
   double value = 0;

   for (int a=0; a<GTOA.K; ++a)
   {
      double s1 = 2 * GTOA.k[a] * AC;

      for (int b=0; b<GTOB.K; ++b)
      {
         double s2 = 2 * GTOB.k[b] * BC;

         for (int e = 0 ; e < arEcp.parameter[arEnuc].at(arI).K; ++e)
         {
            //std::cout << " Nl " << arEcp.parameter[arEnuc].at(arI).N[0][e] << endl;
            double eta = GTOA.k[a] + GTOB.k[b] + arEcp.parameter[arEnuc].at(arI).k[e];
            double C   = GTOA.k[a] * AC + GTOB.k[b] * BC;
            C /= eta;
            double limit1 = std::max(0., C - 0.5/sqrt(eta));
            limit1 = std::max(limit1,0.);

            double limit2 = C + 0.5 / sqrt(eta);

            std::function<double(const double&)> func = std::bind(SpFunction,std::placeholders::_1, La,Lb,  n,s1,s2,eta);

            std::function<void(double&,double&)> mapfunc = std::bind(quadrature::LinearMapping,std::placeholders::_1,std::placeholders::_2,limit1,limit2);

            std::cout << " function parameter " << La << " " << Lb<< " " << n << " " << s1  << " " << s2 << " " << eta << std::endl;
            double tmp = EcpInts::quadrature::DoQuadrature (func, 24, 2000, mapfunc);
            tmp *= arEcp.parameter[arEnuc].at(arI).N[0][e];
            tmp *= GTOA.N[Ja][a];
            tmp *= GTOB.N[Jb][b];
            double ex = GTOA.k[a]*AC*AC + GTOB.k[b]*BC*BC;
            value = tmp * std::exp(-ex);
         }
      }
   }
   return value;
}


double EcpInts::quadrature::DoQuadrature(
      const std::function<double(const double&)>& arFunc,
      const int& arNstart,
      const int& arNmax,
      const std::function<void(double&,double&)>& arMapFunc,
      const double& arThresh)
{
   int p[3];
   p[0] = arNstart;
   p[1] = 2*p[0]+1;
   p[2] = 2*p[1]+1;
   double Q[3];
   Q[0]=0.0;
   Q[1]=0.0;
   Q[2]=0.0;

   if (p[1] > arNmax)
   {
     throw Exception("start and end number of points do not fit in quadrature");
   }


   // Do the intial ones
   {
      double r[p[0]];
      double w[p[0]];
      for (int k = 0; k < p[0]; ++k)
      {
         quadrature::GetPointGeneric(p[0],k+1,r[k],w[k],arMapFunc);
      }
      for (int k = 0; k < p[0]; ++k)
      {
         Q[0]+= w[k]*arFunc(r[k]);
      }
   }
   for (int i = 1; i < 3 ; ++i)
   {
      Q[i] = 0.5* Q[i-1];
      double r[p[i]];
      double w[p[i]];
      for (int k = 0; k < p[i]; ++k)
      {
         quadrature::GetPointGeneric(p[i],k+1,r[k],w[k],arMapFunc);
      }
      for (int k = 0; 2*k < p[i]; ++k)
      {
         int s = 2*k;
         Q[i]+= w[s]*arFunc(r[s]);
      }
   }

   double c = (Q[2]-Q[1]);
   c *=c;
   bool converged = (c < fabs(Q[2]-Q[0])*arThresh);


   while (!converged && p[2] < arNmax)
   {
      std::swap(p[0],p[1]);
      std::swap(p[1],p[2]);
      std::swap(Q[0],Q[1]);
      std::swap(Q[1],Q[2]);

      p[2] = 2*p[1]+1;
      Q[2] = 0.5* Q[1];
      double r[p[2]];
      double w[p[2]];
      for (int k = 0; k < p[2]; ++k)
      {
         quadrature::GetPointGeneric(p[2],k+1,r[k],w[k],arMapFunc);
      }
      for (int k = 0; 2*k < p[2]; ++k)
      {
         int s = 2*k;
         Q[2]+= w[s]*arFunc(r[s]);
      }
      c = (Q[2]-Q[1]);
      c *=c;
      converged = (c < 1.0e-24 || c < fabs(Q[2]-Q[0])*arThresh);
   }

   std::cout << std::scientific << " final set " << Q[0] << " " << Q[1] << " " << Q[2] << endl;
   if (!converged)
   {
     //std::cout << " convergence : " << c << " " << fabs(Q[2]-Q[0])*arThresh << " " << Q[2] << std::endl;
     std::cout <<  "did NOT converge in  "+std::to_string(p[2])+" points" << endl;;
     throw Exception("General quadrature did not converge in "+std::to_string(p[2])+" points");
   }
   else
      std::cout << "Special quadrature converged in "+std::to_string(p[2])+" points to " << Q[2] << std::endl;


   return Q[2];
}
