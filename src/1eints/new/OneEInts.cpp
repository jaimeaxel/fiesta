#include <math.h>
#include <typeinfo>
#include <iostream>
#include "math/gamma.hpp"
#include "math/affine.hpp"
#include "OneEInts.hpp"


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// One-dimensional overlap integral
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void OneEInts::OverlapIntegral_1D(Overlap& S, const double& arA, const double& arB, const double& arK, const int& arLa, const int& arLb)
{
    for (int ab=0; ab<(LMAX+2)*(LMAX+2); ++ab)  (&(S[0][0]))[ab] = 0;

    double ik2 = 1/(2*arK);

    //(0,0)
    S[0][0] = sqrt(PI/arK);

    //(0,lb)
    S[0][1] = arB * S[0][0];
    for (int b=1; b<=arLb; ++b)
        S[0][b+1] = arB * S[0][b] + b * ik2 * S[0][b-1];

    //(la,0)
    S[1][0] = arA * S[0][0];
    for (int a=1; a<=arLa; ++a)
        S[a+1][0] = arA * S[a][0] + a * ik2 * S[a-1][0];

    //(la,lb)
    for (int b=1; b<=arLb+1; ++b) {
        S[1][b] = arA * S[0][b] + b * ik2 * S[0][b-1];
        for (int a=1; a<=arLa; ++a)
            S[a+1][b] = arA * S[a][b] + a * ik2 * S[a-1][b]  + b * ik2 * S[a][b-1];
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// One-dimensional kinetic integral
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void OneEInts::KineticIntegral_1D(Kinetic& K, const Overlap& S, const double& arK1, const double& arK2, const int& arLa, const int& arLb)
{

    for (int ab=0; ab<(LMAX+1)*(LMAX+1); ++ab)  (&(K[0][0]))[ab] = 0;

    for (int a=0; a<=arLa; ++a)
        for (int b=0; b<=arLb; ++b)
            K[a][b] = 4*arK1*arK2*S[a+1][b+1];

    for (int a=0; a<=arLa; ++a)
        for (int b=1; b<=arLb; ++b)
            K[a][b] -= 2*arK1*b*S[a+1][b-1];

    for (int a=1; a<=arLa; ++a)
        for (int b=0; b<=arLb; ++b)
            K[a][b] -= 2*arK2*a*S[a-1][b+1];

    for (int a=1; a<=arLa; ++a)
        for (int b=1; b<=arLb; ++b)
            K[a][b] +=    a*b*S[a-1][b-1];
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// recursive function for "transfer" of the angular momenta according to the
// Hermite recursion scheme.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

double OneEInts::R2E(int ax, int ay, int az, const vector3 & PA,
                     int bx, int by, int bz, const vector3 & PB,
                      double iz, int px, int py, int pz, Triple & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px-1,py,pz, R) +
               PA.x * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py-1,pz, R) +
               PA.y * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz-1, R) +
               PA.z * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz+1, R);
    }

    if (bx>0) {
        return px   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px-1,py,pz, R) +
               PB.x * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (by>0) {
        return py   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py-1,pz, R) +
               PB.y * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py+1,pz, R);
    }
    if (bz>0) {
        return pz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz-1, R) +
               PB.z * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz+1, R);
    }


    return R[px][py][pz];
}

void OneEInts::GetIntegralFunctionX(RintX& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::CoulombIntegral_1c_MMDX< 0>;
         break;
      case  1:
         arR=details::CoulombIntegral_1c_MMDX< 1>;
         break;
      case  2:
         arR=details::CoulombIntegral_1c_MMDX< 2>;
         break;
      case  3:
         arR=details::CoulombIntegral_1c_MMDX< 3>;
         break;
      case  4:
         arR=details::CoulombIntegral_1c_MMDX< 4>;
         break;
      case  5:
         arR=details::CoulombIntegral_1c_MMDX< 5>;
         break;
      case  6:
         arR=details::CoulombIntegral_1c_MMDX< 6>;
         break;
      case  7:
         arR=details::CoulombIntegral_1c_MMDX< 7>;
         break;
      case  8:
         arR=details::CoulombIntegral_1c_MMDX< 8>;
         break;
      case  9:
         arR=details::CoulombIntegral_1c_MMDX< 9>;
         break;
      case  10:
         arR=details::CoulombIntegral_1c_MMDX<10>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }
}


void OneEInts::GetIntegralCoulombExp(RintExp& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::CoulombIntegralExp_1c_MMD< 0>;
         break;
      case  1:
         arR=details::CoulombIntegralExp_1c_MMD< 1>;
         break;
      case  2:
         arR=details::CoulombIntegralExp_1c_MMD< 2>;
         break;
      case  3:
         arR=details::CoulombIntegralExp_1c_MMD< 3>;
         break;
      case  4:
         arR=details::CoulombIntegralExp_1c_MMD< 4>;
         break;
      case  5:
         arR=details::CoulombIntegralExp_1c_MMD< 5>;
         break;
      case  6:
         arR=details::CoulombIntegralExp_1c_MMD< 6>;
         break;
      case  7:
         arR=details::CoulombIntegralExp_1c_MMD< 7>;
         break;
      case  8:
         arR=details::CoulombIntegralExp_1c_MMD< 8>;
         break;
      case  9:
         arR=details::CoulombIntegralExp_1c_MMD< 9>;
         break;
      case  10:
         arR=details::CoulombIntegralExp_1c_MMD<10>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }
}

void OneEInts::details::GetRtuvFunction(Rtuv& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::ConstructRtuv<0,Triple>;
         break;
      case  1:
         arR=details::ConstructRtuv<1,Triple>;
         break;
      case  2:
         arR=details::ConstructRtuv<2,Triple>;
         break;
      case  3:
         arR=details::ConstructRtuv<3,Triple>;
         break;
      case  4:
         arR=details::ConstructRtuv<4,Triple>;
         break;
      case  5:
         arR=details::ConstructRtuv<5,Triple>;
         break;
      case  6:
         arR=details::ConstructRtuv<6,Triple>;
         break;
      case  7:
         arR=details::ConstructRtuv<7,Triple>;
         break;
      case  8:
         arR=details::ConstructRtuv<8,Triple>;
         break;
      case  9:
         arR=details::ConstructRtuv<9,Triple>;
         break;
      case  10:
         arR=details::ConstructRtuv<10,Triple>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }


}

void OneEInts::details::GetRtuvFunctionX(RtuvX& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::ConstructRtuv<0,TripleX>;
         break;
      case  1:
         arR=details::ConstructRtuv<1,TripleX>;
         break;
      case  2:
         arR=details::ConstructRtuv<2,TripleX>;
         break;
      case  3:
         arR=details::ConstructRtuv<3,TripleX>;
         break;
      case  4:
         arR=details::ConstructRtuv<4,TripleX>;
         break;
      case  5:
         arR=details::ConstructRtuv<5,TripleX>;
         break;
      case  6:
         arR=details::ConstructRtuv<6,TripleX>;
         break;
      case  7:
         arR=details::ConstructRtuv<7,TripleX>;
         break;
      case  8:
         arR=details::ConstructRtuv<8,TripleX>;
         break;
      case  9:
         arR=details::ConstructRtuv<9,TripleX>;
         break;
      case  10:
         arR=details::ConstructRtuv<10,TripleX>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }
}

void OneEInts::GetIntegralFunction(Rint& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::CoulombIntegral_1c_MMD<0>;
         break;
      case  1:
         arR=details::CoulombIntegral_1c_MMD<1>;
         break;
      case  2:
         arR=details::CoulombIntegral_1c_MMD<2>;
         break;
      case  3:
         arR=details::CoulombIntegral_1c_MMD<3>;
         break;
      case  4:
         arR=details::CoulombIntegral_1c_MMD<4>;
         break;
      case  5:
         arR=details::CoulombIntegral_1c_MMD<5>;
         break;
      case  6:
         arR=details::CoulombIntegral_1c_MMD<6>;
         break;
      case  7:
         arR=details::CoulombIntegral_1c_MMD<7>;
         break;
      case  8:
         arR=details::CoulombIntegral_1c_MMD<8>;
         break;
      case  9:
         arR=details::CoulombIntegral_1c_MMD<9>;
         break;
      case  10:
         arR=details::CoulombIntegral_1c_MMD<10>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }


}

void OneEInts::details::GetBoysFunction(RBoys& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::CalculateBoys<0>;
         break;
      case  1:
         arR=details::CalculateBoys<1>;
         break;
      case  2:
         arR=details::CalculateBoys<2>;
         break;
      case  3:
         arR=details::CalculateBoys<3>;
         break;
      case  4:
         arR=details::CalculateBoys<4>;
         break;
      case  5:
         arR=details::CalculateBoys<5>;
         break;
      case  6:
         arR=details::CalculateBoys<6>;
         break;
      case  7:
         arR=details::CalculateBoys<7>;
         break;
      case  8:
         arR=details::CalculateBoys<8>;
         break;
      case  9:
         arR=details::CalculateBoys<9>;
         break;
      case  10:
         arR=details::CalculateBoys<10>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }


}

void OneEInts::details::GetExtBoysFunction(RExtBoys& arR,const int& arL)
{
  switch (arL) {
      case  0:
         arR=details::CalculateExtendedBoys<0>;
         break;
      case  1:
         arR=details::CalculateExtendedBoys<1>;
         break;
      case  2:
         arR=details::CalculateExtendedBoys<2>;
         break;
      case  3:
         arR=details::CalculateExtendedBoys<3>;
         break;
      case  4:
         arR=details::CalculateExtendedBoys<4>;
         break;
      case  5:
         arR=details::CalculateExtendedBoys<5>;
         break;
      case  6:
         arR=details::CalculateExtendedBoys<6>;
         break;
      case  7:
         arR=details::CalculateExtendedBoys<7>;
         break;
      case  8:
         arR=details::CalculateExtendedBoys<8>;
         break;
      case  9:
         arR=details::CalculateExtendedBoys<9>;
         break;
      case  10:
         arR=details::CalculateExtendedBoys<10>;
         break;
      default:
         std::cout<< " not implemented for L>10 " << std::endl;
         break;
    }


}
