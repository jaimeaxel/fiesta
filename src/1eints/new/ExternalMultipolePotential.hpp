#include "1eints/1electron.hpp"

#ifndef __EXMULTIPOT__
#define __EXMULTIPOT__

/*
Needs to be switched on when disentangled from the old structure
struct Qtensor3 {
    double xx;
    double xy;
    double xz;
    double yy;
    double yz;
    double zz;
};
*/

struct ExternalMultipolePotential {
    int Ms; // monopoles
    int Ds; // dipoles
    int Qs; // quadrupoles

    point * MC; // center of the charges
    point * DC; // center of the dipoles
    point * QC; // center of the quadrupoles

    double  * M; // charge of the monopole
    vector3  * D; // dipole components
    Qtensor3 * Q; // quadrupole components

    ExternalMultipolePotential() {
        Ms = Ds = Qs = 0;

        MC = NULL;
        DC = NULL;
        QC = NULL;

        M = NULL;
        D = NULL;
        Q = NULL;
    }

    // CK TEST >>>
    ExternalMultipolePotential(const ExternalPotential& old_pot) {
        std::cout << " A " << std::endl;
        std::cout << "old_pot.Ms " << old_pot.Ms << std::endl;
        InitMonopoles(old_pot.Ms);
        std::cout << " B " << std::endl;
        InitDipoles(old_pot.Ds);
        std::cout << " C " << std::endl;
        InitQuadrupoles(old_pot.Qs);
        std::cout << " D " << std::endl;
        for (int i=0; i < Ms; ++i)
        {
           MC[i] = old_pot.MC[i];
           M[i] = old_pot.M[i];
        }
        std::cout << " E " << std::endl;
        for (int i=0; i < Ds; ++i)
        {
           DC[i] = old_pot.DC[i];
           D[i] = old_pot.D[i];
        }
        std::cout << " F " << std::endl;
        for (int i=0; i < Qs; ++i)
        {
           QC[i] = old_pot.QC[i];
           Q[i] = old_pot.Q[i];
        }
        std::cout << " G " << std::endl;
    }
    // CK TEST <<<

    void InitMonopoles(int n) {
        Ms = n;
        delete[] MC;
        delete[] M;
        MC = new point[Ms];
        M  = new double[Ms];
    }

    void InitDipoles(int n) {
        Ds = n;
        delete[] DC;
        delete[] D;
        DC = new point[Ds];
        D  = new vector3[Ds];
    }

    void InitQuadrupoles(int n) {
        Qs = n;
        delete[] QC;
        delete[] Q;
        QC = new point[Qs];
        Q  = new Qtensor3[Qs];
    }


    ~ExternalMultipolePotential() {
        delete[] MC;
        delete[] DC;
        delete[] QC;

        delete[] M;
        delete[] D;
        delete[] Q;
    }
};

#endif
