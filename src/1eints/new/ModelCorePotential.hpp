#include "basis/GTO.hpp"

#ifndef __MODELCOREPOT__
#define __MODELCOREPOT__


struct ModelCorePotential{

    int    nnuclei = 0;
    point * positions = NULL;
    double * Zeff = NULL;
    std::vector<double> * CoeffExp = NULL;
    std::vector<double> * ExpExp = NULL;
    std::vector<double> * CoeffExp_r = NULL;
    std::vector<double> * ExpExp_r = NULL;
    std::vector<vector<GTOforMCP>> * ProjOrbs = NULL;
    std::vector<vector<GTOforMCP>> * EcpOrbs = NULL;
    std::vector<tensor2> * EcpMats = NULL;

    bool Empty() {return (nnuclei < 1);}
    void Init(const int& In){
       nnuclei = In;
       Zeff = new double[nnuclei];
       positions = new point[nnuclei];
       CoeffExp= new std::vector<double> [nnuclei];
       ExpExp= new std::vector<double>[nnuclei];
       CoeffExp_r= new std::vector<double>[nnuclei];
       ExpExp_r= new std::vector<double>[nnuclei];
       ProjOrbs = new std::vector<vector<GTOforMCP>> [nnuclei];
       EcpOrbs = new std::vector<vector<GTOforMCP>> [nnuclei];
       EcpMats = new std::vector<tensor2> [nnuclei];
    }

    ~ModelCorePotential(){
       delete[] Zeff;
       delete[] positions;
       delete[] CoeffExp ;
       delete[] ExpExp;
       delete[] CoeffExp_r;
       delete[] ExpExp_r;
       delete[] ProjOrbs;
       delete[] EcpOrbs;
       delete[] EcpMats;
    }


};

#endif
