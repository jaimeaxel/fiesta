#include <memory>
#include <iomanip>
#include "defs.hpp"
#include "math/angular.hpp"
#include "OneEInts.hpp"
#include "GenPot.hpp"
#include "GenEcp.hpp"


void NewInts::details::GetP(const ShellPair& AB, const point& A, const point& B, const int& a, const int& b, point& P, double& k12, double& ik12)
{
  double k1, k2;

  k1  = AB.GP->ka[a];
  k2  = AB.GP->kb[b];

  k12 = k1+k2;
  ik12 = 1/k12;

  P.x = (k1*A.x + k2*B.x)*ik12;
  P.y = (k1*A.y + k2*B.y)*ik12;
  P.z = (k1*A.z + k2*B.z)*ik12;
}

void NewInts::details::CalcCoulombExpContributionToTriple(const int& Ltot, const double& k12,
    const point& P, const ModelCorePotential& arMCP, Triple& R)
{
   Triple TR;

   RintExp integral_exp;
   OneEInts::GetIntegralCoulombExp(integral_exp,Ltot);

   for (int i=0; i<arMCP.nnuclei; ++i) {
       for (int r=0; i<arMCP.ExpExp_r[i].size(); ++r) {
          integral_exp(TR, k12, arMCP.ExpExp_r[i].at(r), P, arMCP.positions[i]);
          //std::cout <<  " exp/r component " << std::endl;
          for (int px=0; px<=Ltot; ++px) {
              for (int py=0; py<=Ltot-px; ++py) {
                  for (int pz=0; pz<=Ltot-px-py; ++pz) {
                      //std::cout << px  << " " << py << " " << pz << " " << TR[px][py][pz]  << std::endl;
                      R[px][py][pz] += arMCP.CoeffExp_r[i].at(r) * TR[px][py][pz]; //
                  }
              }
          }
       }
   }
}

void NewInts::details::CalcMonopoleContributionToTriple(const int& Ltot, const int& npoints, const point * points, const point& P, const double& k12, const double* charges, Triple& R)
{
   TripleX TR;

   RintX integral_x;
   OneEInts::GetIntegralFunctionX(integral_x,Ltot);
   for (int i=0; i<npoints; ++i) {
       integral_x(TR, k12, P, points[i]);

       for (int px=0; px<=Ltot; ++px) {
           for (int py=0; py<=Ltot-px; ++py) {
               for (int pz=0; pz<=Ltot-px-py; ++pz) {
                   R[px][py][pz] -= charges[i] * TR[px][py][pz]; //
               }
           }
       }
   }
}

void NewInts::details::CalcDipoleContributionToTriple(const int& Ltot, const int& npoints, const point * points, const point& P, const double& k12, const vector3* dipoles, Triple& R)
{
   TripleX TR;

   RintX integral_x;
   OneEInts::GetIntegralFunctionX(integral_x,Ltot);
   for (int i=0; i<npoints; ++i) {
       integral_x(TR, k12, P, points[i]);
       for (int px=0; px<=Ltot; ++px) {
           for (int py=0; py<=Ltot-px; ++py) {
               for (int pz=0; pz<=Ltot-px-py; ++pz) {
                  R[px][py][pz] += dipoles[i].x * TR[px+1][py][pz];
                  R[px][py][pz] += dipoles[i].y * TR[px][py+1][pz];
                  R[px][py][pz] += dipoles[i].z * TR[px][py][pz+1];

               }
           }
       }
   }
}

void NewInts::details::AddContrToCartPot(const ShellPair& AB, const vector3& PA, vector3& PB, Triple& R,
   const int& arA, const int& arB, const double& arRecCoeff, const double& wab, CartPotential& VV, const int& Ja, const int& Jb, const int& La, const int& Lb)
{

   for (int ma=0; ma<LibAngular::nmC[La]; ++ma) {
       int ax = LibAngular::CartList[La][ma].nx;
       int ay = LibAngular::CartList[La][ma].ny;
       int az = LibAngular::CartList[La][ma].nz;

       for (int mb=0; mb<LibAngular::nmC[Lb]; ++mb) {
           int bx = LibAngular::CartList[Lb][mb].nx;
           int by = LibAngular::CartList[Lb][mb].ny;
           int bz = LibAngular::CartList[Lb][mb].nz;

           //very inefficient, but still a small fraction of the computation O(1)/O(N)
           double r2e = OneEInts::R2E(ax,ay,az,PA, bx,by,bz,PB, arRecCoeff, 0,0,0, R);
           //std::cout << "r2e for " << ax << " "  << ay << " "<<  az
           //     << " " << bx << " " << by << " " << bz << " "   << wab*r2e << std::endl;
           for (int ja=0; ja<Ja; ++ja) {
               for (int jb=0; jb<Jb; ++jb) {
                   double w12 = wab; //AB.W[b][a];

                   if (La==LSP && ma>0) w12 *= AB.GP->Nap[ja][arA];
                   else                 w12 *= AB.GP->Na [ja][arA];

                   if (Lb==LSP && mb>0) w12 *= AB.GP->Nbp[jb][arB];
                   else                 w12 *= AB.GP->Nb [jb][arB];
                   VV[ja][jb][ma][mb] += w12 * r2e;
               }
           }
       }
   }
}
void NewInts::details::Cart2Spher(const ShellPair & AB, const CartPotential& VV, symtensor & Vp)
{
    int La = AB.getLa();
    int Lb = AB.getLb();

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;

    int ta = AB.ata;
    int tb = AB.atb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<LibAngular::nmS[La]; ++a) {
                for (int b=0; b<LibAngular::nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*LibAngular::nmS[La] + a;
                    int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + b;

                    double sumV = 0;
                    for (int i=0; i<LibAngular::SHList[La][a].nps; ++i) {
                        for (int j=0; j<LibAngular::SHList[Lb][b].nps; ++j) {
                            const LibAngular::SHTerm & sha = LibAngular::SHList[La][a].T[i];
                            const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][b].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    Vp(ta,tb, ffa,ffb) = sumV;
                }
            }
        }
    }

}

void NewInts::details::CalcOverlaps(double (&SS)[2*LMAX+1][2*LMAX+1], const point& A, const point& B, double kA, double kB, int La, int Lb)
{
    //cout << " calculating overlap for " <<kA << " " << kB << std::endl;
    double k12 = kA+kB;
    double ik2 = 1/(2*k12);
    int la = (La==LSP)?1:La;
    int lb = (Lb==LSP)?1:Lb;

    point P;
    vector3 Av;
    vector3 Bv;

    P.x = (kA*A.x + kB*B.x)/k12;
    P.y = (kA*A.y + kB*B.y)/k12;
    P.z = (kA*A.z + kB*B.z)/k12;


    Av = P - A;
    Bv = P - B;
    double Sx[LMAX+2][LMAX+2];
    double Sy[LMAX+2][LMAX+2];
    double Sz[LMAX+2][LMAX+2];
    OneEInts::OverlapIntegral_1D(Sx, Av.x, Bv.x, k12, la, lb);
    OneEInts::OverlapIntegral_1D(Sy, Av.y, Bv.y, k12, la, lb);
    OneEInts::OverlapIntegral_1D(Sz, Av.z, Bv.z, k12, la, lb);
    double R2= (A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y) + (A.z - B.z)*(A.z - B.z);
    double nu = kA*kB/k12;
    double prefac = exp(-nu*R2);
    for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
    {      //note: LibAngular::nmS[] contains the possible l values, i.e., 1,3,5,.. / i.e. sum over shells
        for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
        {
            double sumS;
            sumS = 0e0;

            for (int i=0; i<LibAngular::SHList[La][ma].nps; ++i)
            {  //note: LibAngular::SHList[][].nps = number of primitive Cartesian Gaussians // sum over primitives
                for (int j=0; j<LibAngular::SHList[Lb][mb].nps; ++j)
                {
                    const LibAngular::SHTerm & sha = LibAngular::SHList[La][ma].T[i];  //note:  refers to one primitive Cartesian Gaussians  ??
                    const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][mb].T[j];
                    double test = sha.cN *   shb.cN * prefac *
                          (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);  // note :cN=normalization constant?
                    sumS += test;
                }
            }

            SS[ma][mb] =       sumS;
        }
    }
}


void NewInts::details::CalcProjForMCP (const ShellPair & AB, const point& A, const point& B,  const ModelCorePotential& MCP, symtensor & VMCP, std::string aType)
{

    std::vector<std::vector<GTOforMCP>>*  pOrbs = MCP.ProjOrbs;
    if (aType == "MCP")
    {
       pOrbs = MCP.ProjOrbs;
    }
    else if (aType == "ECP")
    {
       pOrbs = MCP.EcpOrbs;
    }
    else
    {
       std::cout << " OrbTyle not known " << std::endl;
    }
    //std::cout << " calc " <<  aType << " proj part for " << pOrbs[0].size() << std::endl;
    int max_terms =1000;
    int La = AB.getLa();
    int Lb = AB.getLb();

    // the following is just dummy stuff for the cartesian coefficients

    int Ja = AB.GP->Ja; // number of primitives in a
    int Jb = AB.GP->Jb; // number of primitives in b

    int ta = AB.ata;
    int tb = AB.atb;

    bool samef = ( (ta == tb) && (AB.getFa() == AB.getFb()) );

    for (int b=0; b<AB.getKb(); ++b) // sum over all
    {
        for (int a=0; a<AB.getKa(b); ++a)
        {
            double kA, kB;

            kA  = AB.GP->ka[a];
            kB  = AB.GP->kb[b];
            double PP[2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix
            for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
            {
                for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                {
                   PP[ma][mb]=0.0e0;
                }
            }
            for (int n = 0; n < MCP.nnuclei; ++n)
            {
               point C=MCP.positions[n];
               //std::cout << " MCP position " << C.x << " " <<  C.y << " " << C.z << std::endl;
               for (int o = 0; o < pOrbs[n].size(); ++o)
               {
                  double SSA[max_terms][2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix
                  double SSB[max_terms][2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix
                  int n_terms=0;
                  for (int o2 = 0; o2 < pOrbs[n].at(o).size(); ++o2)
                  {
                     for (int k = 0; k < (int)pOrbs[n].at(o).at(o2).K; ++k)
                     {
                        CalcOverlaps(SSA[n_terms], A, C, kA, pOrbs[n].at(o).at(o2).k[k], La,
                               (int)pOrbs[n].at(o).at(o2).l);
                        CalcOverlaps(SSB[n_terms], B, C, kB, pOrbs[n].at(o).at(o2).k[k], Lb,
                               (int)pOrbs[n].at(o).at(o2).l);
                        for (int s1 = 0 ; s1 < 2*LMAX+1; ++s1)
                        {
                           for (int s2 = 0 ; s2 < 2*LMAX+1; ++s2)
                           {
                              SSA[n_terms][s1][s2] *= pOrbs[n].at(o).at(o2).N[0][k];
                           }
                        }

                        for (int s1 = 0 ; s1 < 2*LMAX+1; ++s1)
                        {
                           for (int s2 = 0 ; s2 <  2*LMAX+1; ++s2)
                           {
                              SSB[n_terms][s1][s2] *= pOrbs[n].at(o).at(o2).N[0][k];
                           }

                        }
                        ++n_terms;
                     }
                  }

                  double fac= -2* pOrbs[n].at(o).at(0).energy;
                  for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
                  {
                      for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                      {
                         for (int t1=0; t1 < n_terms; ++t1)
                         {
                            for (int t2=0; t2 < n_terms; ++t2)
                            {
                               double fac_pair= fac;
                               if (aType == "ECP") {fac_pair*=(double)MCP.EcpMats[n].at(o).operator()(t1,t2);}
                               for (int poa=0; poa<LibAngular::nmS[(int)pOrbs[n].at(o).at(0).l]; ++poa)
                               {
                                     PP[ma][mb] += fac_pair*SSA[t1][ma][poa]*SSB[t2][mb][poa];
                                }
                            }
                         }
                      }
                  }

               }
            }
            double wab = 1 ; // AB.getW(b,a); // this is the overall factor of exp(-nu x_p), I think
            for (int ja=0; ja<Ja; ++ja)
            {
                for (int jb=0; jb<Jb; ++jb)
                {

                    double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b];

                    for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
                    {
                        for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                        {
                            int ffa = AB.getFa() + ja*LibAngular::nmS[La] + ma;  //mapping between the different formats
                            int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + mb;

                            if (samef && (ffb>ffa)) continue;

                            VMCP(ta,tb, ffa,ffb) += w12 * PP[ma][mb];
                        }
                    }
                }
            }
        }
    }


}

void NewInts::details::CalcExpPartForMCP (const ShellPair & AB, const point& A, const point& B, const ModelCorePotential& MCP, symtensor & VMCP)
{
    int La = AB.getLa();
    int Lb = AB.getLb();
    int la = (AB.getLa()==LSP)?1:AB.getLa();
    int lb = (AB.getLb()==LSP)?1:AB.getLb();
    //std::cout << " la= " << la << " ";
    //std::cout << " lb= " << lb << std::endl;

    int Ja = AB.GP->Ja; // number of primitives in a
    int Jb = AB.GP->Jb; // number of primitives in b

    int ta = AB.ata;
    int tb = AB.atb;

    bool samef = ( (ta == tb) && (AB.getFa() == AB.getFb()) );

    for (int b=0; b<AB.getKb(); ++b) {               // sum over all
        for (int a=0; a<AB.getKa(b); ++a) {

            double k12, ik12;
            point P;
            details::GetP(AB, A, B, a, b, P,k12,ik12);
            vector3 PA = P-A;
            vector3 PB = P-B;

            for (int n = 0; n < MCP.nnuclei; ++n)
            {
               // setting up all the parameters for the integrals
               point C=MCP.positions[n];
               vector3 PC = P-C;
               double PC2 = PC*PC;
               for (int r = 0; r < MCP.ExpExp[n].size(); ++r)
               {
                  double k_exp=k12+ MCP.ExpExp[n].at(r);
                  double ikexp = 1.0e0/k_exp;
                  double nu=k12*MCP.ExpExp[n].at(r)*ikexp;

                  point Q;

                  Q.x = MCP.ExpExp[n].at(r)*C.x + k12*P.x;
                  Q.y = MCP.ExpExp[n].at(r)*C.y + k12*P.y;
                  Q.z = MCP.ExpExp[n].at(r)*C.z + k12*P.z;
                  Q.x *= ikexp;
                  Q.y *= ikexp;
                  Q.z *= ikexp;


                  vector3 QA = Q-A;
                  vector3 QB = Q-B;


                  double wab=  AB.getW(b,a)*MCP.Zeff[n]; // factor for the overlap integral
                  double Ex[LMAX+2][LMAX+2];
                  double Ey[LMAX+2][LMAX+2];
                  double Ez[LMAX+2][LMAX+2];

                  // here we exploit that the integrals resemble overlap integrals to a large extend, only with other coefficients and factors
                  //std::cout << QA.x << " " << QB.x << " " << k_exp << std::endl;
                  OneEInts::OverlapIntegral_1D(Ex, QA.x, QB.x, k_exp, la, lb);

                  double prefac = exp(-nu*(P.x-C.x)*(P.x-C.x));
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                        //std::cout <<  Ex[i][j] << std::endl;
                        Ex[i][j]*=prefac;
                     }
                  prefac = exp(-nu*(P.y-C.y)*(P.y-C.y));
                  OneEInts::OverlapIntegral_1D(Ey, QA.y, QB.y, k_exp, la, lb);
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                        //std::cout <<  Ey[i][j] << std::endl;
                        Ey[i][j]*=prefac;
                     }

                  prefac = exp(-nu*(P.z-C.z)*(P.z-C.z));
                  OneEInts::OverlapIntegral_1D(Ez, QA.z, QB.z, k_exp, la, lb);
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                        //std::cout <<  Ez[i][j] << std::endl;
                        Ez[i][j]*=prefac;
                     }

                  //std::cout << " overlap - exp function " << std::endl;
                  /*
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                       std::cout << " x " << i << " " << j << " " << wab<< " " <<Ex[i][j]<< " " << wab*Ex[i][j]  << std::endl;
                       std::cout << " y " << i << " " << j << " " << wab<< " " <<Ey[i][j]<< " " << wab*Ey[i][j]  << std::endl;
                       std::cout << " z " << i << " " << j << " " << wab<< " " <<Ez[i][j]<< " " << wab*Ez[i][j]  << std::endl;
                     }
                  */

                  double EE[2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix

                  for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {      //note: LibAngular::nmS[] contains the possible l values, i.e., 1,3,5,.. / i.e. sum over shells
                      for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                          double sum = 0;

                          for (int i=0; i<LibAngular::SHList[La][ma].nps; ++i) {  //note: LibAngular::SHList[][].nps = number of primitive Cartesian Gaussians // sum over primitives
                              for (int j=0; j<LibAngular::SHList[Lb][mb].nps; ++j) {
                                  const LibAngular::SHTerm & sha = LibAngular::SHList[La][ma].T[i];  //note:  refers to one primitive Cartesian Gaussians  ??
                                  const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][mb].T[j];

                                  sum+= sha.cN * shb.cN * (Ex[sha.nx][shb.nx] * Ey[sha.ny][shb.ny] * Ez[sha.nz][shb.nz]);
                              }
                          }

                          EE[ma][mb] = sum;
                          //std::cout << "exp contr " << ma << " " << mb << " " << EE[ma][mb] << std::endl;
                      }
                  }

                  // S | X,Y,Z storing into the symtensors that are given as input/output variables mainly mapping and  weighing?!
                  for (int ja=0; ja<Ja; ++ja)
                  {
                      for (int jb=0; jb<Jb; ++jb)
                      {

                          double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]*MCP.CoeffExp[n].at(r); // weight ??

                          for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
                          {
                              for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                              {
                                  int ffa = AB.getFa() + ja*LibAngular::nmS[La] + ma;  //mappint between the different formats
                                  int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + mb;

                                  if (samef && (ffb>ffa)) continue;

                                  VMCP(ta,tb, ffa,ffb) += w12 * EE[ma][mb];
                              }
                          }
                      }
                  }
               }
            }
        }
    }

}

/*
void NewInts::CalcMonoElectronic(const ShellPair & AB, const rAtom * nuclei, symtensor & S, symtensor & T, symtensor & X, symtensor & Y, symtensor & Z) {


    int La = AB.getLa();
    int Lb = AB.getLb();
    int la = (AB.getLa()==LSP)?1:AB.getLa();
    int lb = (AB.getLb()==LSP)?1:AB.getLb();

    int Ja = AB.GP->Ja; // number of primitives in a
    int Jb = AB.GP->Jb; // number of primitives in b


    point A = nuclei[AB.ata].c;
    point B = nuclei[AB.atb].c;

    int ta = AB.ata;
    int tb = AB.atb;

    bool samef = ( (ta == tb) && (AB.getFa() == AB.getFb()) );


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int a=0; a<LibAngular::nmS[La]; ++a) {
                for (int b=0; b<LibAngular::nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*LibAngular::nmS[La] + a;
                    int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + b;
                    S(ta,tb, ffa,ffb) = 0;
                    T(ta,tb, ffa,ffb) = 0;
                    X(ta,tb, ffa,ffb) = 0; //A.x;
                    Y(ta,tb, ffa,ffb) = 0; //A.y;
                    Z(ta,tb, ffa,ffb) = 0; //A.z;
                }
            }
        }
    }


    for (int b=0; b<AB.getKb(); ++b) {               // sum over all
        for (int a=0; a<AB.getKa(b); ++a) {

            double k1, k2, k12, ik2;

            k1  = AB.GP->ka[a];
            k2  = AB.GP->kb[b];
            k12 = k1+k2;
            ik2 = 1/(2*k12);

            point P;
            vector3 Av;
            vector3 Bv;

            P.x = (k1*A.x + k2*B.x)/k12;
            P.y = (k1*A.y + k2*B.y)/k12;
            P.z = (k1*A.z + k2*B.z)/k12;

            Av = P - A;
            Bv = P - B;

            // calculate one-dimensional cartesian overlap integrals
            double Sx[LMAX+2][LMAX+2];
            double Sy[LMAX+2][LMAX+2];
            double Sz[LMAX+2][LMAX+2];

            OneEInts::OverlapIntegral_1D(Sx, Av.x, Bv.x, k12, la, lb);
            OneEInts::OverlapIntegral_1D(Sy, Av.y, Bv.y, k12, la, lb);
            OneEInts::OverlapIntegral_1D(Sz, Av.z, Bv.z, k12, la, lb);

            // calculate one-dimensional cartesian kinetic integrals
            double Tx[LMAX+1][LMAX+1];
            double Ty[LMAX+1][LMAX+1];
            double Tz[LMAX+1][LMAX+1];

            OneEInts::KineticIntegral_1D(Tx, Sx, k1, k2, la, lb);
            OneEInts::KineticIntegral_1D(Ty, Sy, k1, k2, la, lb);
            OneEInts::KineticIntegral_1D(Tz, Sz, k1, k2, la, lb);

            double wab = AB.getW(b,a); // this is the overall factor of exp(-nu x_p), I think

            double SS[2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix
            double TT[2*LMAX+1][2*LMAX+1]; // overall kinetic integral matrix
            double XX[2*LMAX+1][2*LMAX+1]; // overall <x> integral matrix
            double YY[2*LMAX+1][2*LMAX+1]; // overall <y> integral matrix
            double ZZ[2*LMAX+1][2*LMAX+1]; // overall <z> integral matrix

            for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {      //note: LibAngular::nmS[] contains the possible l values, i.e., 1,3,5,.. / i.e. sum over shells
                for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                    double sumS, sumT, sumX, sumY, sumZ;
                    sumS = sumT = sumX = sumY = sumZ = 0;

                    for (int i=0; i<LibAngular::SHList[La][ma].nps; ++i) {  //note: LibAngular::SHList[][].nps = number of primitive Cartesian Gaussians // sum over primitives
                        for (int j=0; j<LibAngular::SHList[Lb][mb].nps; ++j) {
                            const LibAngular::SHTerm & sha = LibAngular::SHList[La][ma].T[i];  //note:  refers to one primitive Cartesian Gaussians  ??
                            const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][mb].T[j];

                            sumS += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);  // note :cN=normalization constant?
                            sumT += sha.cN * shb.cN * (Tx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                       Sx[sha.nx][shb.nx] * Ty[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                       Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Tz[sha.nz][shb.nz]);

                            sumX += sha.cN * shb.cN * (Sx[sha.nx+1][shb.nx] * Sy[sha.ny  ][shb.ny] * Sz[sha.nz  ][shb.nz]);
                            sumY += sha.cN * shb.cN * (Sx[sha.nx  ][shb.nx] * Sy[sha.ny+1][shb.ny] * Sz[sha.nz  ][shb.nz]);
                            sumZ += sha.cN * shb.cN * (Sx[sha.nx  ][shb.nx] * Sy[sha.ny  ][shb.ny] * Sz[sha.nz+1][shb.nz]);
                        }
                    }

                    SS[ma][mb] =       sumS;
                    TT[ma][mb] = 0.5 * sumT;

                    XX[ma][mb] =       sumX;
                    YY[ma][mb] =       sumY;
                    ZZ[ma][mb] =       sumZ;
                }
            }


            // S | X,Y,Z storing into the symtensors that are given as input/output variables mainly mapping and  weighing?!
            for (int ja=0; ja<Ja; ++ja) {
                for (int jb=0; jb<Jb; ++jb) {

                    double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]; // weight ??

                    for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {
                        for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                            int ffa = AB.getFa() + ja*LibAngular::nmS[La] + ma;  //mapping between the different formats
                            int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + mb;

                            if (samef && (ffb>ffa)) continue;

                            S(ta,tb, ffa,ffb) += w12 * SS[ma][mb];
                            T(ta,tb, ffa,ffb) += w12 * TT[ma][mb];

                            X(ta,tb, ffa,ffb) += w12 * XX[ma][mb];
                            Y(ta,tb, ffa,ffb) += w12 * YY[ma][mb];
                            Z(ta,tb, ffa,ffb) += w12 * ZZ[ma][mb];
                        }
                    }
                }
            }

        }
    }


    // add the origin vector // WHY??

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int a=0; a<LibAngular::nmS[La]; ++a) {
                for (int b=0; b<LibAngular::nmS[Lb]; ++b) {

                    int ffa = AB.getFa() + ja*LibAngular::nmS[La] + a;
                    int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + b;

                    if (samef && (ffb>ffa)) continue;

                    X(ta,tb, ffa,ffb) += A.x * S(ta,tb, ffa,ffb);
                    Y(ta,tb, ffa,ffb) += A.y * S(ta,tb, ffa,ffb);
                    Z(ta,tb, ffa,ffb) += A.z * S(ta,tb, ffa,ffb);
                }
            }
        }
    }

}
*/

void NewInts::CalcPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, symtensor & Vp)
{
   CartPotential VV;

   int La = AB.getLa();
   int Lb = AB.getLb();

   int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

   int Ja = AB.GP->Ja;
   int Jb = AB.GP->Jb;

   // explicitely initializing only those we will need, to save time??
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
               for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                   VV[ja][jb][i][j] = 0;
               }
           }
       }
   }


   point A = nuclei[AB.ata].c;
   point B = nuclei[AB.atb].c;

   //set the actual point charges
   double  charges[nnuclei];
   point  points[nnuclei];
   int npoints=0;
   for (int i=0; i< nnuclei; ++i)
   {
       const std::string aname = nuclei[i].rAP->name;
       if (aname[0] == '*') continue; // skip atoms with pseudopotential
       charges[npoints]= nuclei[i].rAP->Z;
       points[npoints++]= nuclei[i].c;
   }

   //loop over primitives

   for (int b=0; b<AB.getKb(); ++b) {
       for (int a=0; a<AB.getKa(b); ++a) {

           Triple R;
           // explicitely initializing only those we will need, to save time??
           for (int px=0; px<=Ltot; ++px)
              for (int py=0; py<=Ltot-px; ++py)
                 for (int pz=0; pz<=Ltot-px-py; ++pz)
                    R[px][py][pz] = 0;


           double k12, ik12;
           point P;
           details::GetP(AB, A, B, a, b, P,k12,ik12);

           details::CalcMonopoleContributionToTriple(Ltot, npoints, points, P, k12 , charges, R);

           vector3 PA = P - A;
           vector3 PB = P - B;

           //double wab = exp(-(AB.GP->kr[a][b]) * AB.R2);
           double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // compensate the lack of normalization of the gaussiandensity

           details::AddContrToCartPot(AB,PA,PB,R, a, b, 0.5*ik12,wab, VV,Ja, Jb, La,Lb);
       }
   }


   details::Cart2Spher(AB,  VV, Vp);

}

void NewInts::CalcExtMPPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, const ExternalMultipolePotential& ExMP, symtensor & XVp)
{
   CartPotential VV;

   int La = AB.getLa();
   int Lb = AB.getLb();

   int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

   int Ja = AB.GP->Ja;
   int Jb = AB.GP->Jb;

   // explicitely initializing only those we will need, to save time??
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
               for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                   VV[ja][jb][i][j] = 0;
               }
           }
       }
   }


   point A = nuclei[AB.ata].c;
   point B = nuclei[AB.atb].c;

   //loop over primitives

   for (int b=0; b<AB.getKb(); ++b) {
       for (int a=0; a<AB.getKa(b); ++a) {

           Triple R;
           // explicitely initializing only those we will need, to save time??
           {
               for (int px=0; px<=Ltot; ++px)
                   for (int py=0; py<=Ltot-px; ++py)
                       for (int pz=0; pz<=Ltot-px-py; ++pz)
                           R[px][py][pz] = 0;
           }


           double k12, ik12;
           point P;
           details::GetP(AB, A, B, a, b, P,k12,ik12);

           details::CalcMonopoleContributionToTriple(Ltot, ExMP.Ms, ExMP.MC, P, k12 , ExMP.M, R);
           details::CalcDipoleContributionToTriple(Ltot, ExMP.Ds, ExMP.DC, P, k12 , ExMP.D, R);

           vector3 PA = P - A;
           vector3 PB = P - B;

           //double wab = exp(-(AB.GP->kr[a][b]) * AB.R2);
           double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // compensate the lack of normalization of the gaussiandensity

           details::AddContrToCartPot(AB,PA,PB,R, a, b, 0.5*ik12,wab, VV,Ja, Jb, La,Lb);
       }
   }

   details::Cart2Spher(AB,  VV, XVp);
}

void NewInts::CalcMCPPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, const ModelCorePotential& MCP, symtensor & VMCP)
{
    int La = AB.getLa();
    int Lb = AB.getLb();
    int la = (AB.getLa()==LSP)?1:AB.getLa();
    int lb = (AB.getLb()==LSP)?1:AB.getLb();

    int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

    int Ja = AB.GP->Ja; // number of primitives in a
    int Jb = AB.GP->Jb; // number of primitives in b

    int ta = AB.ata;
    int tb = AB.atb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int a=0; a<LibAngular::nmS[La]; ++a) {
                for (int b=0; b<LibAngular::nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*LibAngular::nmS[La] + a;
                    int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + b;
                    VMCP(ta,tb, ffa,ffb) = 0;
                }
            }
        }
    }

    // calc 1/r and e^{ar^2}/r contributions with McMurchie-Davidcon
    CartPotential VV;

    // explicitely initializing only those we will need, to save time??
    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<LibAngular::nmC[La]; ++i) {   // LibAngular::nmC contains 1,3,6,10,etc.
                for (int j=0; j<LibAngular::nmC[Lb]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }
        }
    }


    point A = nuclei[AB.ata].c;
    point B = nuclei[AB.atb].c;

    double  dummycharges[MCP.nnuclei];
    for (int i=0; i< nnuclei; ++i)
    {
        dummycharges[i]= MCP.Zeff[i];
    }


    //loop over primitives

    //std::cout << std::endl;
    for (int b=0; b<AB.getKb(); ++b) {
        for (int a=0; a<AB.getKa(b); ++a) {

            Triple R;
            // explicitely initializing only those we will need, to save time??

            for (int px=0; px<=Ltot; ++px)
                for (int py=0; py<=Ltot-px; ++py)
                    for (int pz=0; pz<=Ltot-px-py; ++pz)
                        R[px][py][pz] = 0.0e0;



            double k12, ik12;
            point P;
            details::GetP(AB, A, B, a, b, P,k12,ik12);

            vector3 PA = P - A;
            vector3 PB = P - B;

            double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // weight * (O||O)

            // monopole component
            /* //not needed, this is taken care of by the normal 1/r and the effective
               //nuclear charge for the respective atom type
            std::cout << std::endl << " monopole contributions " << std::endl;
            {
               // first do the common 1/r contribution via the monopole function (not weighted)
               //details::CalcMonopoleContributionToTriple(Ltot, MCP.nnuclei, MCP.positions,
               //   P, k12 , dummycharges, R);
               //details::AddContrToCartPot(AB,PA,PB,R, a, b, 0.5*ik12,wab, VV,Ja, Jb, La,Lb);
               //

               //only for debugging like , i.e., adding part by part
               {
                   TripleX TR;

                   RintX integral_x;
                   OneEInts::GetIntegralFunctionX(integral_x,Ltot);
                   for (int i=0; i<MCP.nnuclei; ++i) {

                       for (int px=0; px<=Ltot; ++px)
                           for (int py=0; py<=Ltot-px; ++py)
                               for (int pz=0; pz<=Ltot-px-py; ++pz)
                                   R[px][py][pz] = 0.0e0;

                       integral_x(TR, k12, P, MCP.positions[i]);

                       for (int px=0; px<=Ltot; ++px) {
                           for (int py=0; py<=Ltot-px; ++py) {
                               for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                   R[px][py][pz] -= dummycharges[i] * TR[px][py][pz]; //
                               }
                           }
                       }
                       details::AddContrToCartPot(AB,PA,PB,R, a, b, 0.5*ik12,wab, VV,Ja, Jb, La,Lb);
                   }
               }

               //for (int px=0; px<=Ltot; ++px)
               //    for (int py=0; py<=Ltot-px; ++py)
               //        for (int pz=0; pz<=Ltot-px-py; ++pz)
               //            std::cout << px << " " << py << " " << pz << " " << R[px][py][pz] << std::endl;

            }
            */
            // exp/r component
            //std::cout << std::endl << " calculate exp/r contributions " << std::endl;
            {


               RintExp integral_exp;
               OneEInts::GetIntegralCoulombExp(integral_exp,Ltot);

               for (int i=0; i< MCP.nnuclei; ++i) {

                   //std::cout << " Calculating exp_r for nucleus" << i << std::endl;
                   for (int r=0; r <  MCP.ExpExp_r[i].size(); ++r)
                   {
                      // reinitialization
                      //std::cout << " Calculating exp_r for " << r << std::endl;
                      {
                         for (int px=0; px<=Ltot; ++px)
                            for (int py=0; py<=Ltot-px; ++py)
                                for (int pz=0; pz<=Ltot-px-py; ++pz)
                                     R[px][py][pz] =0.0e0;
                      }
                      integral_exp(R, k12, MCP.ExpExp_r[i].at(r), P, MCP.positions[i]);
                      /*
                      std::cout <<  " Triple " << std::endl;
                      for (int px=0; px<=Ltot; ++px) {
                          for (int py=0; py<=Ltot-px; ++py) {
                              for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                  std::cout << px  << " " << py << " " << pz << " " << R[px][py][pz]  << std::endl;
                                  R[px][py][pz] *= MCP.CoeffExp_r[i] ; //
                              }
                          }
                      }

                      point Q;
                      */

                      details::AddContrToCartPot(AB,PA,PB, R, a, b, 0.5*ik12, wab*MCP.Zeff[i]*MCP.CoeffExp_r[i].at(r), VV,Ja, Jb, La,Lb);

                  }
               }
            }
               /*
               // and then the A*exp^{-ar^2}/r
               details::CalcCoulombExpContributionToTriple(Ltot, k12, P, MCP, R);
               std::cout << " exp/r contributions  " << std::endl;
               for (int px=0; px<=Ltot; ++px)
                   for (int py=0; py<=Ltot-px; ++py)
                       for (int pz=0; pz<=Ltot-px-py; ++pz)
                           std::cout << px << " " << py << " " << pz << " " << R[px][py][pz] << std::endl;
               */
               /*
               need to be switched on again later
               vector3 PA = P - A;
               vector3 PB = P - B;
               double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // weight * (O||O)
               details::AddContrToCartPot(AB,PA,PB,R, a, b, 0.5*ik12,wab, VV,Ja, Jb, La,Lb);
               */
         }
    }


   details::Cart2Spher(AB,  VV, VMCP);


   // add the exponential part
   details::CalcExpPartForMCP(AB, A, B, MCP, VMCP);

   // add projection part
   //std::cout << " Enter the unknown.." << std::endl;
   details::CalcProjForMCP(AB, A, B, MCP, VMCP, "MCP");
   //std::cout << " bla " << std::endl;
   //details::CalcProjForMCP(AB, A, B, MCP, VMCP, "ECP");
   //EcpInts::GenEcp();
   //std::cout << " blubb " << std::endl;

   //scale not neccessary anymore as done above for each term individually
   /*
   std::cout << " scaled MCP " << endl;
   for (int ja=0; ja<Ja; ++ja) {
       for (int jb=0; jb<Jb; ++jb) {
           for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {
               for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                   int ffa = AB.getFa() + ja*LibAngular::nmS[La] + ma;
                   int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + mb;
                   VMCP(ta,tb, ffa,ffb) *= MCP.Zeff;
                   //std::cout << ta << " " << tb << " " <<  ffa << " " << ffb<< " " <<   VMCP(ta,tb, ffa,ffb) << std::endl;
               }
           }
       }
   }
   */

}


//typedef double Double[2*LMAX+1][2*LMAX+1];
struct Angular2 {
    double v[2*LMAX+1][2*LMAX+1];

    double & operator() (int i, int j) {
        return v[i][j];
    }

    const double & operator() (int i, int j) const {
        return v[i][j];
    }
};

void NewInts::CalcMCPPotentialCart (const point & A, const point & B, const GTO & GTOA, const GTO & GTOB, const ModelCorePotential& MCP, tensor2 & VMCP,  int osa, int osb)
{
    int La = GTOA.l;
    int Lb = GTOB.l;
    int la = (La==LSP)?1:La;
    int lb = (Lb==LSP)?1:Lb;

    int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

    int Ja = GTOA.J; // number of primitives in a
    int Jb = GTOB.J; // number of primitives in b


    CartPotential Vmcp;

    for (int ja=0; ja<Ja; ++ja)
        for (int jb=0; jb<Jb; ++jb)
            for (int i=0; i<LibAngular::nmC[La]; ++i)   // LibAngular::nmC contains 1,3,6,10,etc.
                for (int j=0; j<LibAngular::nmC[Lb]; ++j)
                    Vmcp[ja][jb][i][j] = 0;


    // calc 1/r and e^{ar^2}/r contributions with McMurchie-Davidcon
    CartPotential VV;

    // explicitely initializing only those we will need, to save time??
    for (int ja=0; ja<Ja; ++ja)
        for (int jb=0; jb<Jb; ++jb)
            for (int i=0; i<LibAngular::nmC[La]; ++i)   // LibAngular::nmC contains 1,3,6,10,etc.
                for (int j=0; j<LibAngular::nmC[Lb]; ++j)
                    VV[ja][jb][i][j] = 0;


    double  dummycharges[MCP.nnuclei];
    for (int i=0; i< MCP.nnuclei; ++i)
    {
        dummycharges[i]= MCP.Zeff[i];
    }


    double R2; {
        vector3 AB = A - B;
        R2 = AB*AB;
    }


    //loop over primitives
    for (int b=0; b<GTOB.K; ++b) {
        for (int a=0; a<GTOA.K; ++a) {

            Triple R;
            // explicitely initializing only those we will need, to save time??

            for (int px=0; px<=Ltot; ++px)
                for (int py=0; py<=Ltot-px; ++py)
                    for (int pz=0; pz<=Ltot-px-py; ++pz)
                        R[px][py][pz] = 0.;

            const double k1  = GTOA.k[a];
            const double k2  = GTOB.k[b];

            const double k12 = k1+k2;
            const double ik12 = 1/k12;

            point P;
            P.x = (k1*A.x + k2*B.x)*ik12;
            P.y = (k1*A.y + k2*B.y)*ik12;
            P.z = (k1*A.z + k2*B.z)*ik12;


            vector3 PA = P - A;
            vector3 PB = P - B;

            //double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // weight * (O||O)
            const double wab = exp(-(k1*k2*ik12) * R2)  * (PI*ik12)*sqrt(PI*ik12);

            // exp/r component
            {
                RintExp integral_exp;
                OneEInts::GetIntegralCoulombExp(integral_exp,Ltot);

                for (int i=0; i< MCP.nnuclei; ++i) {

                    //std::cout << " Calculating exp_r for nucleus" << i << std::endl;
                    for (int r=0; r <  MCP.ExpExp_r[i].size(); ++r) {

                        // reinitialization
                        {
                         for (int px=0; px<=Ltot; ++px)
                            for (int py=0; py<=Ltot-px; ++py)
                                for (int pz=0; pz<=Ltot-px-py; ++pz)
                                     R[px][py][pz] = 0;
                        }

                        integral_exp  (R, k12, MCP.ExpExp_r[i].at(r), P, MCP.positions[i]);

                        //details::AddContrToCartPot  (AB,PA,PB, R, a, b, 0.5*ik12, wab*MCP.Zeff[i]*MCP.CoeffExp_r[i].at(r), VV, Ja, Jb, La,Lb);

                        for (int ma=0; ma<LibAngular::nmC[La]; ++ma) {
                            int ax = LibAngular::CartList[La][ma].nx;
                            int ay = LibAngular::CartList[La][ma].ny;
                            int az = LibAngular::CartList[La][ma].nz;

                            for (int mb=0; mb<LibAngular::nmC[Lb]; ++mb) {
                                int bx = LibAngular::CartList[Lb][mb].nx;
                                int by = LibAngular::CartList[Lb][mb].ny;
                                int bz = LibAngular::CartList[Lb][mb].nz;

                                //very inefficient, but still a small fraction of the computation O(1)/O(N)
                                double r2e = OneEInts::R2E(ax,ay,az,PA, bx,by,bz,PB,  0.5*ik12 , 0,0,0, R);

                                for (int ja=0; ja<Ja; ++ja) {
                                    for (int jb=0; jb<Jb; ++jb) {
                                        double w12 =  wab * MCP.Zeff[i] * MCP.CoeffExp_r[i].at(r); //AB.W[b][a];

                                        if (La==LSP && ma>0) w12 *= GTOA.Np [ja][a];
                                        else                 w12 *= GTOA.N  [ja][a];

                                        if (Lb==LSP && mb>0) w12 *= GTOB.Np [jb][b];
                                        else                 w12 *= GTOB.N  [jb][b];

                                        VV[ja][jb][ma][mb] += w12 * r2e;
                                    }
                                }
                            }
                        }

                    }
                }
            }

        }
    }





    // add the exponential part
    // ========================
    //details::CalcExpPartForMCP(AB, A, B, MCP, VMCP);
    //loop over primitives
    for (int b=0; b<GTOB.K; ++b) {
        for (int a=0; a<GTOA.K; ++a) {

            const double k1  = GTOA.k[a];
            const double k2  = GTOB.k[b];

            const double k12 = k1+k2;
            const double ik12 = 1/k12;

            point P;
            P.x = (k1*A.x + k2*B.x)*ik12;
            P.y = (k1*A.y + k2*B.y)*ik12;
            P.z = (k1*A.z + k2*B.z)*ik12;


            vector3 PA = P - A;
            vector3 PB = P - B;

            //double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // weight * (O||O)
            const double WW = exp(-(k1*k2*ik12) * R2);
            const double wab = exp(-(k1*k2*ik12) * R2)  * (PI*ik12)*sqrt(PI*ik12);


            for (int n = 0; n < MCP.nnuclei; ++n)
            {
               // setting up all the parameters for the integrals
               point C = MCP.positions[n];
               vector3 PC = P-C;
               double PC2 = PC * PC;

               for (int r = 0; r < MCP.ExpExp[n].size(); ++r)
               {
                  double k_exp = k12 + MCP.ExpExp[n].at(r);
                  double ikexp = 1. / k_exp;
                  double nu=k12*MCP.ExpExp[n].at(r)*ikexp;

                  point Q;

                  Q.x = MCP.ExpExp[n].at(r)*C.x + k12*P.x;
                  Q.y = MCP.ExpExp[n].at(r)*C.y + k12*P.y;
                  Q.z = MCP.ExpExp[n].at(r)*C.z + k12*P.z;
                  Q.x *= ikexp;
                  Q.y *= ikexp;
                  Q.z *= ikexp;


                  vector3 QA = Q-A;
                  vector3 QB = Q-B;


                  //double wab=  AB.getW(b,a) * MCP.Zeff[n]; // factor for the overlap integral
                  double wab =  WW * MCP.Zeff[n]; // factor for the overlap integral
                  double Ex[LMAX+2][LMAX+2];
                  double Ey[LMAX+2][LMAX+2];
                  double Ez[LMAX+2][LMAX+2];

                  // here we exploit that the integrals resemble overlap integrals to a large extend, only with other coefficients and factors
                  //std::cout << QA.x << " " << QB.x << " " << k_exp << std::endl;
                  OneEInts::OverlapIntegral_1D(Ex, QA.x, QB.x, k_exp, la, lb);

                  double prefac = exp(-nu*(P.x-C.x)*(P.x-C.x));
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                        //std::cout <<  Ex[i][j] << std::endl;
                        Ex[i][j]*=prefac;
                     }
                  prefac = exp(-nu*(P.y-C.y)*(P.y-C.y));
                  OneEInts::OverlapIntegral_1D(Ey, QA.y, QB.y, k_exp, la, lb);
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                        //std::cout <<  Ey[i][j] << std::endl;
                        Ey[i][j]*=prefac;
                     }

                  prefac = exp(-nu*(P.z-C.z)*(P.z-C.z));
                  OneEInts::OverlapIntegral_1D(Ez, QA.z, QB.z, k_exp, la, lb);
                  for (int i = 0 ; i <= la ; ++i)
                     for (int j = 0 ; j <= lb ; ++j)
                     {
                        //std::cout <<  Ez[i][j] << std::endl;
                        Ez[i][j]*=prefac;
                     }

                  double EE[2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix

                  for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {      //note: LibAngular::nmS[] contains the possible l values, i.e., 1,3,5,.. / i.e. sum over shells
                      for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                          double sum = 0;

                          for (int i=0; i<LibAngular::SHList[La][ma].nps; ++i) {  //note: LibAngular::SHList[][].nps = number of primitive Cartesian Gaussians // sum over primitives
                              for (int j=0; j<LibAngular::SHList[Lb][mb].nps; ++j) {
                                  const LibAngular::SHTerm & sha = LibAngular::SHList[La][ma].T[i];  //note:  refers to one primitive Cartesian Gaussians  ??
                                  const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][mb].T[j];

                                  sum += sha.cN * shb.cN * (Ex[sha.nx][shb.nx] * Ey[sha.ny][shb.ny] * Ez[sha.nz][shb.nz]);
                              }
                          }

                          EE[ma][mb] = sum;
                          //std::cout << "exp contr " << ma << " " << mb << " " << EE[ma][mb] << std::endl;
                      }
                  }

                  // S | X,Y,Z storing into the symtensors that are given as input/output variables mainly mapping and  weighing?!
                  /*
                  for (int ja=0; ja<Ja; ++ja)
                  {
                      for (int jb=0; jb<Jb; ++jb)
                      {

                          double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]*MCP.CoeffExp[n].at(r); // weight ??

                          for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
                          {
                              for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                              {
                                  int ffa = AB.getFa() + ja*LibAngular::nmS[La] + ma;  //mappint between the different formats
                                  int ffb = AB.getFb() + jb*LibAngular::nmS[Lb] + mb;

                                  if (samef && (ffb>ffa)) continue;

                                  VMCP(ta,tb, ffa,ffb) += w12 * EE[ma][mb];
                              }
                          }
                      }
                  }
                  */

                  for (int ja=0; ja<Ja; ++ja) {
                      for (int jb=0; jb<Jb; ++jb) {

                          //double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]*MCP.CoeffExp[n].at(r); // weight ??
                          const double w12 = wab * GTOA.N[ja][a] * GTOB.N[jb][b] * MCP.CoeffExp[n].at(r); // weight ??

                          for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {
                              for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                                  Vmcp [ja][jb][ma][mb] += w12 * EE[ma][mb];
                              }
                          }
                      }
                  }


               }
            }
        }
    }






    // add projection part
    // ===================
    //details::CalcProjForMCP(AB, A, B, MCP, VMCP, "MCP");


    std::vector<std::vector<GTOforMCP>>*  pOrbs = MCP.ProjOrbs;


    //if      (aType == "MCP") pOrbs = MCP.ProjOrbs;
    //else if (aType == "ECP") pOrbs = MCP.EcpOrbs;
    //else    std::cout << " OrbTyle not known " << std::endl;


    //std::cout << " calc " <<  aType << " proj part for " << pOrbs[0].size() << std::endl;


    Angular2 * SSA, * SSB;

    int NTERMS; {
        NTERMS = maxJ*maxK; //0;
        //for (int o2 = 0; o2 < pOrbs[n][o].size(); ++o2)
        //    NTERMS += pOrbs[n][o][o2].K;
    }

    SSA = new Angular2[NTERMS];
    SSB = new Angular2[NTERMS];




    for (int b=0; b<GTOB.K; ++b) {
        for (int a=0; a<GTOA.K; ++a) {

            const double kA  = GTOA.k[a];
            const double kB  = GTOB.k[b];

            const double k12 = kA + kB;
            const double ik12 = 1/k12;


            double PP[2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix

            for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
                for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                   PP[ma][mb] = 0;


            for (int n = 0; n < MCP.nnuclei; ++n)
            {
               point C = MCP.positions[n];
               //std::cout << " MCP position " << C.x << " " <<  C.y << " " << C.z << std::endl;
               for (int o = 0; o < pOrbs[n].size(); ++o)
               {
/*
                  Angular2 * SSA, * SSB;

                  int NTERMS; {
                      NTERMS = 0;
                      for (int o2 = 0; o2 < pOrbs[n][o].size(); ++o2)
                          NTERMS += pOrbs[n][o][o2].K;
                  }

                  SSA = new Angular2[NTERMS];
                  SSB = new Angular2[NTERMS];
*/

                  //double SSA[max_terms][2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix
                  //double SSB[max_terms][2*LMAX+1][2*LMAX+1]; // overall overlap integral matrix

                  int n_terms=0;
                  for (int o2 = 0; o2 < pOrbs[n][o].size(); ++o2)
                  {
                     for (int k = 0; k < (int)pOrbs[n][o][o2].K; ++k)
                     {
                        NewInts::details::CalcOverlaps(SSA[n_terms].v, A, C, kA, pOrbs[n][o][o2].k[k], La,
                               (int)pOrbs[n][o][o2].l);
                        NewInts::details::CalcOverlaps(SSB[n_terms].v, B, C, kB, pOrbs[n][o][o2].k[k], Lb,
                               (int)pOrbs[n][o][o2].l);

                        for (int s1 = 0 ; s1 < 2*LMAX+1; ++s1)
                            for (int s2 = 0 ; s2 < 2*LMAX+1; ++s2)
                                SSA[n_terms](s1,s2) *= pOrbs[n][o][o2].N[0][k];

                        for (int s1 = 0 ; s1 < 2*LMAX+1; ++s1)
                            for (int s2 = 0 ; s2 <  2*LMAX+1; ++s2)
                                SSB[n_terms](s1,s2) *= pOrbs[n][o][o2].N[0][k];

                        ++n_terms;
                     }
                  }

                  double fac= -2* pOrbs[n][o][0].energy;
                  for (int ma=0; ma<LibAngular::nmS[La]; ++ma)
                      for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb)
                         for (int t1=0; t1 < n_terms; ++t1)
                            for (int t2=0; t2 < n_terms; ++t2)
                               for (int poa=0; poa<LibAngular::nmS[(int)pOrbs[n][o][0].l]; ++poa)
                                     PP[ma][mb] += fac * SSA[t1](ma,poa) * SSB[t2](mb,poa);

//                  delete[] SSA, SSB;
               }
            }


            for (int ja=0; ja<Ja; ++ja) {
                for (int jb=0; jb<Jb; ++jb) {
                    //double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]*MCP.CoeffExp[n].at(r); // weight ??
                    const double w12 = GTOA.N[ja][a] * GTOB.N[jb][b]; // weight ??

                    for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {
                        for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                            int ffa = osa + ja*LibAngular::nmS[La] + ma;  //mappint between the different formats
                            int ffb = osb + jb*LibAngular::nmS[Lb] + mb;

                            Vmcp [ja][jb][ma][mb] += w12 * PP[ma][mb];
                        }
                    }
                }
            }


        }
    }

    delete[] SSA, SSB;


    // details::Cart2Spher(AB,  VV, VMCP);

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int ma=0; ma<LibAngular::nmS[La]; ++ma) {
                for (int mb=0; mb<LibAngular::nmS[Lb]; ++mb) {
                    int ffa = osa + ja*LibAngular::nmS[La] + ma;
                    int ffb = osb + jb*LibAngular::nmS[Lb] + mb;

                    double sumV = 0;
                    for (int i=0; i<LibAngular::SHList[La][ma].nps; ++i) {
                        for (int j=0; j<LibAngular::SHList[Lb][mb].nps; ++j) {
                            const LibAngular::SHTerm & sha = LibAngular::SHList[La][ma].T[i];
                            const LibAngular::SHTerm & shb = LibAngular::SHList[Lb][mb].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    VMCP (ffa,ffb) = sumV  +  Vmcp [ja][jb][ma][mb];
                }
            }
        }
    }

}


