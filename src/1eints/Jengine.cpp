#include <cmath>
#include "defs.hpp"
#include "Jengine.hpp"

#include "molecule/atoms.hpp"
#include "molecule/edata.hpp"

#include "linear/tensors.hpp"
#include "linear/sparse.hpp"
#include "linear/newtensors.hpp"
#include "linear/eigen.hpp"

#include "math/gamma.hpp" //rutinas de integracion de bajo nivel, 'inlined'
#include "math/angular.hpp"
#include "math/polynomial.hpp"

#include "low/io.hpp"

#include "basis/atomprod.hpp"
#include "basis/SPprototype.hpp"
#include "basis/shellpair.hpp"

#include "libechidna/libechidna.hpp"

#include "fiesta/fiesta.hpp"

using namespace std;
using namespace LibAngular;
using namespace LibIGamma;

using namespace Fiesta;


void SystemDensity::AddShellPair (const ShellPair & SP, const rAtom * nuclei, const symtensor & Dp) {

    const ShellPairPrototype & SPP = *SP.GP;

    const int Ja = SPP.Ja;
    const int Jb = SPP.Jb;

    const int La = SPP.l1;
    const int Lb = SPP.l2;
    const int Lt = La+Lb;

    int ta = SP.ata;
    int tb = SP.atb;


	point A = nuclei[SP.ata].c;
	point B = nuclei[SP.atb].c;

	vector3 BA = A - B;


	//generate HRRs
	pol pHRRx[LMAX+1];
	pol pHRRy[LMAX+1];
	pol pHRRz[LMAX+1];

	for (int lb=0; lb<=Lb; ++lb) {
        pHRRx[lb] = Pascal<2*LMAX>(lb, BA.x);
        pHRRy[lb] = Pascal<2*LMAX>(lb, BA.y);
        pHRRz[lb] = Pascal<2*LMAX>(lb, BA.z);
	}

    //expand the density in cartesian coordinates
    //*******************************************
    double DD [maxJ][maxJ] [2*LMAX+1][2*LMAX+1][2*LMAX+1];

    for (int ja=0; ja<Ja; ++ja)
        for (int jb=0; jb<Jb; ++jb)
            for (int i=0; i<=Lt; ++i)
                for (int j=0; j<=Lt-i; ++j)
                    for (int k=0; k<=Lt-i-j; ++k)
                       DD[ja][jb]  [i][j][k] = 0;



    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            double DD3[MMAX+1][MMAX+1];

            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    DD3[i][j] = 0;
                }
            }

            // contract the density with cartesian the gaussian
            for (int ma=0; ma<nmS[La]; ++ma) {
                for (int mb=0; mb<nmS[Lb]; ++mb) {
                    int ffa = SP.getFa() + ja*nmS[La] + ma;
                    int ffb = SP.getFb() + jb*nmS[Lb] + mb;

                    double dd = Dp(ta,tb, ffa,ffb);

                    if (!SP.sameF()) dd *= 4.;
                    else             dd *= 2.;

                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                            const SHTerm & sha = SHList[La][ma].T[i];
                            const SHTerm & shb = SHList[Lb][mb].T[j];

                            DD3[sha.nc][shb.nc] += sha.cN * shb.cN * dd; //amplitude
                        }
                    }

                }
            }

            //now expand the polynomials of center B in terms of polynomials around A
            for (int mb=0; mb<nmC[Lb]; ++mb) {
                int bx = CartList[Lb][mb].nx;
                int by = CartList[Lb][mb].ny;
                int bz = CartList[Lb][mb].nz;

                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    for (int fx=0; fx<=bx; ++fx) {
                        for (int fy=0; fy<=by; ++fy) {
                            for (int fz=0; fz<=bz; ++fz) {
                                DD[ja][jb] [ax+fx][ay+fy][az+fz] += DD3[ma][mb] * pHRRx[bx].w[fx] * pHRRy[by].w[fy] * pHRRz[bz].w[fz];
                            }
                        }
                    }
                }
            }

        }
    }



    // transform the product of sums of cartesian gaussians into an expansion of Hermite functions
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    const PrimitiveSet & Pset = SPP.Psets[SP.nK2-1];


    // loop over significant densities
    //*********************************
    for (int b=0; b<Pset.nKb; ++b) {
        for (int a=0; a<Pset.nKa[b]; ++a) {

            double k12, ik12;
            point P; // central point

            // contract the densities with the primitive coefficients
            // uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu
            double DA[2*LMAX+1][2*LMAX+1][2*LMAX+1]; //2*LMAX+1][2*LMAX+1][2*LMAX+1];
            {
                for (int i=0; i<=Lt; ++i)
                    for (int j=0; j<=Lt-i; ++j)
                        for (int k=0; k<=Lt-i-j; ++k)
                           DA [i][j][k] = 0;


                double wab = SP.getW(b,a);

                for (int ja=0; ja<Ja; ++ja) {
                    for (int jb=0; jb<Jb; ++jb) {

                        double w12 = wab * SPP.Na [ja][a] * SPP.Nb [jb][b];

                        for (int i=0; i<=Lt; ++i)
                            for (int j=0; j<=Lt-i; ++j)
                                for (int k=0; k<=Lt-i-j; ++k)
                                    DA[i][j][k] += w12 * DD[ja][jb] [i][j][k];
                    }
                }
            }

            // translate the expansion to the center P
            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double DP[2*LMAX+1][2*LMAX+1][2*LMAX+1];
            {
                for (int i=0; i<=Lt; ++i)
                    for (int j=0; j<=Lt-i; ++j)
                        for (int k=0; k<=Lt-i-j; ++k)
                           DP[i][j][k] = 0;


                double k1 = SPP.ka[a];
                double k2 = SPP.kb[b];
                k12 = k1 + k2;
                ik12 = 1./k12;

                P.x = (k1*A.x + k2*B.x) * ik12;
                P.y = (k1*A.y + k2*B.y) * ik12;
                P.z = (k1*A.z + k2*B.z) * ik12;

                vector3 PA = P-A;

                //generate HRRs
                pol tHRRx[LMAX+1];
                pol tHRRy[LMAX+1];
                pol tHRRz[LMAX+1];

                for (int lt=0; lt<=Lt; ++lt) {
                    tHRRx[lt] = Pascal<2*LMAX>(lt, PA.x);
                    tHRRy[lt] = Pascal<2*LMAX>(lt, PA.y);
                    tHRRz[lt] = Pascal<2*LMAX>(lt, PA.z);
                }

                //move the expansions to P
                for (int ex=0; ex<=Lt; ++ex) {
                    for (int ey=0; ey<=Lt-ex; ++ey) {
                        for (int ez=0; ez<=Lt-ex-ey; ++ez) {

                            for (int px=0; px<=ex; ++px) {
                                for (int py=0; py<=ey; ++py) {
                                    for (int pz=0; pz<=ez; ++pz) {
                                        DP [px][py][pz] += DA[ex][ey][ez] * tHRRx[ex].w[px] * tHRRy[ey].w[py] * tHRRz[ez].w[pz];
                                    }
                                }
                            }

                        }
                    }
                }
            }

            // compute new Hermite Density
            // aaaaaaaaaaaaaaaaaaaaaaaaaaa
            double DH[2*LMAX+1][2*LMAX+1][2*LMAX+1];
            {
                // decompose the product in a sum of hermite functions
                pol herix[2*LMAX+1];

                for (int lt=0; lt<=Lt; ++lt) Hexp(herix[lt], lt, .5*ik12);

                for (int i=0; i<=Lt; ++i)
                    for (int j=0; j<=Lt-i; ++j)
                        for (int k=0; k<=Lt-i-j; ++k)
                           DH[i][j][k] = 0;


                for (int px=0; px<=Lt; ++px)
                    for (int py=0; py<=Lt-px; ++py)
                        for (int pz=0; pz<=Lt-px-py; ++pz)

                            for (int i=0; i<=px; ++i)
                                for (int j=0; j<=py; ++j)
                                    for (int k=0; k<=pz; ++k)
                                       DH[i][j][k] += DP [px][py][pz] * herix[px].w[i] * herix[py].w[j] * herix[pz].w[k];
            }

            // copy to the density center
            // ++++++++++++++++++++++++++
            HermiteCenter * HCn = new HermiteCenter;

            HermiteCenter & HCC = *HCn;

            HCC.C  = P;
            HCC.L  = Lt;
            HCC.nu = k12;

            for (int i=0; i<=Lt; ++i)
                for (int j=0; j<=Lt-i; ++j)
                    for (int k=0; k<=Lt-i-j; ++k)
                        HCC.w[i][j][k] = DH[i][j][k];

            HClist.push_back(HCn);
        }
    }

}



typedef double TripleX  [2*LMAX+1][2*LMAX+1][2*LMAX+1];
typedef double TripleXX [4*LMAX+1][4*LMAX+1][4*LMAX+1];

typedef void (*RintXX) (TripleXX & , double, double, const point &, const point &);

template <int L> void RIntegralXX(TripleXX & Rtuv, double k1, double k2, const point & P, const point & Q) {

    double Fn[L+1];

    vector3 R;
    R = Q - P;

    double R2 = R*R;

    double ** gamma = IncompleteGamma.gamma_table;
    double *  gamma2 = IncompleteGammas[L].gamma_table_vals;

    double k = (k1*k2)/(k1+k2);

    //calcula las 'F's
    {
        double Sn[L+1];

        double kR2 = k*R2;

        if (kR2>LibIGamma::vg_max-LibIGamma::vg_step) {
            double iR2 = 1/R2;
            Sn[0] = 0.5*sqrt(PI*iR2);

            for (int i=1; i<=L; ++i) {
                Sn[i] = Sn[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = LibIGamma::ivg_step*(kR2-LibIGamma::vg_min);
            int pos = int(p+0.5);
            double x0 = LibIGamma::vg_min + LibIGamma::vg_step*double(pos);
            double Ax1 = x0-kR2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Sn[L] = (gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Sn[i] = (2*kR2*Sn[i+1] + expx)*(1./double(2*i+1));
            }

            double kn   = sqrt(k);

            for (int n=0; n<=L; ++n) {
                Sn[n] *= kn;
                kn    *= 2*k;
            }
        }

        for (int n=0; n<=L; ++n) Fn[n] = Sn[n];
    }


    //calcula R^{n}_{000}
    //double ac = (2*PI)/(k*sqrt(k));
    double ac = 2*PI*PI*sqrt(PI) / (k1*k2*sqrt(k1*k2));

    for (int n=0; n<=L; ++n) Fn[n] *= ac;

    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }

}

typedef void (*RintXX) (TripleXX & , double, double, const point &, const point &);

template <int L> void RIntegralSS(TripleXX & Rtuv, double k1, double k2, const point & P, const point & Q, double w2) {

    double Fn[L+1];

    vector3 R;
    R = Q - P;

    double R2 = R*R;

    double ** gamma = IncompleteGamma.gamma_table;
    double *  gamma2 = IncompleteGammas[L].gamma_table_vals;

    double k = (k1*k2)/(k1+k2);

    //calcula las 'F's
    {
        double Sn[L+1];

        double kR2 = k*R2;

        if (kR2>LibIGamma::vg_max-LibIGamma::vg_step) {
            double iR2 = 1/R2;
            Sn[0] = 0.5*sqrt(PI*iR2);

            for (int i=1; i<=L; ++i) {
                Sn[i] = Sn[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = LibIGamma::ivg_step*(kR2-LibIGamma::vg_min);
            int pos = int(p+0.5);
            double x0 = LibIGamma::vg_min + LibIGamma::vg_step*double(pos);
            double Ax1 = x0-kR2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Sn[L] = (gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Sn[i] = (2*kR2*Sn[i+1] + expx)*(1./double(2*i+1));
            }

            double kn   = sqrt(k);

            for (int n=0; n<=L; ++n) {
                Sn[n] *= kn;
                kn    *= 2*k;
            }
        }

        for (int n=0; n<=L; ++n) Fn[n] = Sn[n];
    }


    //calcula R^{n}_{000}
    //double ac = (2*PI)/(k*sqrt(k));
    double ac = 2*PI*PI*sqrt(PI) / (k1*k2*sqrt(k1*k2));

    for (int n=0; n<=L; ++n) Fn[n] *= ac;

    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }

}



static double R2E(int ax, int ay, int az, const vector3 & PA,           int bx, int by, int bz, const vector3 & PB,           double iz, int px, int py, int pz, TripleX & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px-1,py,pz, R) +
               PA.x * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py-1,pz, R) +
               PA.y * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz-1, R) +
               PA.z * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz+1, R);
    }

    if (bx>0) {
        return px  * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px-1,py,pz, R) +
               PB.x * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (by>0) {
        return py  * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py-1,pz, R) +
               PB.y * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py+1,pz, R);
    }
    if (bz>0) {
        return pz  * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz-1, R) +
               PB.z * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz+1, R);
    }


    return R[px][py][pz];
}


static double CTE(int ax, int ay, int az, const vector3 & PA,           int bx, int by, int bz, const vector3 & PB,           double iz, int px, int py, int pz, TripleX & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px-1,py,pz, R) +
               PA.x * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py-1,pz, R) +
               PA.y * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz-1, R) +
               PA.z * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz+1, R);
    }

    if (bx>0) {
        return px   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px-1,py,pz, R) +
               PB.x * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (by>0) {
        return py   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py-1,pz, R) +
               PB.y * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py+1,pz, R);
    }
    if (bz>0) {
        return pz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz-1, R) +
               PB.z * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz+1, R);
    }


    return R[px][py][pz];
}


void SystemDensity::ComputeCoulombPotential (const ShellPair & AB, const rAtom * nuclei, symtensor & Jp) const {

    double JJ[maxJ][maxJ][2*LMAX+1][2*LMAX+1][2*LMAX+1];
    double VV[maxJ][maxJ][MMAX+1][MMAX+1];

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;
    int La = AB.getLa();
    int Lb = AB.getLb();
	int L12 = La+Lb;



    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<=L12; ++i) {
                for (int j=0; j<=L12-i; ++j) {
                    for (int k=0; k<=L12-i-j; ++k) {
                        JJ[ja][jb][i][j][k] = 0;
                    }
                }
            }
        }
    }


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }

        }
    }



    RintXX Rints[4*LMAX+1]; {
        Rints [0] = RIntegralXX< 0>;
        Rints [1] = RIntegralXX< 1>;
        Rints [2] = RIntegralXX< 2>;
        Rints [3] = RIntegralXX< 3>;
        Rints [4] = RIntegralXX< 4>;
        Rints [5] = RIntegralXX< 5>;
        Rints [6] = RIntegralXX< 6>;
        Rints [7] = RIntegralXX< 7>;
        Rints [8] = RIntegralXX< 8>;
        Rints [9] = RIntegralXX< 9>;
        Rints[10] = RIntegralXX<10>;
        Rints[11] = RIntegralXX<11>;
        Rints[12] = RIntegralXX<12>;
        /*
        Rints[13] = RIntegralXX<13>;
        Rints[14] = RIntegralXX<14>;
        Rints[15] = RIntegralXX<15>;
        Rints[16] = RIntegralXX<16>;
        Rints[17] = RIntegralXX<17>;
        Rints[18] = RIntegralXX<18>;
        Rints[19] = RIntegralXX<19>;
        Rints[20] = RIntegralXX<20>;
        */
    }

	point A = nuclei[AB.ata].c;
	point B = nuclei[AB.atb].c;


    //loop over primitives
    for (int b=0; b<AB.getKb(); ++b) {
        for (int a=0; a<AB.getKa(b); ++a) {

            TripleX R;

            for (int px=0; px<=L12; ++px)
                for (int py=0; py<=L12-px; ++py)
                    for (int pz=0; pz<=L12-px-py; ++pz)
                        R[px][py][pz] = 0;

            double k1, k2, k12, ik12;

            k1  = AB.GP->ka[a];
            k2  = AB.GP->kb[b];

            k12 = k1+k2;
            ik12 = 1/k12;

            point P;

            P.x = (k1*A.x + k2*B.x)*ik12;
            P.y = (k1*A.y + k2*B.y)*ik12;
            P.z = (k1*A.z + k2*B.z)*ik12;

            //accumulate
            //**********
            TripleXX TR;

            list <HermiteCenter*>::const_iterator it;

            for (it=HClist.begin(); it!=HClist.end(); ++it) {

                const HermiteCenter & HC = *(*it);

                int    L34 = HC.L;
                double k34 = HC.nu;
                point   Q   = HC.C;

                //compute Coulomb interaction
                Rints[L12+L34] (TR, k12, k34, P, Q);

                //contract with Hermite densities
                for (int qx=0; qx<=L34; ++qx) {
                    for (int qy=0; qy<=L34-qx; ++qy) {
                        for (int qz=0; qz<=L34-qx-qy; ++qz) {

                            for (int px=0; px<=L12; ++px) {
                                for (int py=0; py<=L12-px; ++py) {
                                    for (int pz=0; pz<=L12-px-py; ++pz) {

                                        double ss = HC.w[qx][qy][qz] * TR[px+qx][py+qy][pz+qz];

                                        if ((qx+qy+qz)%2==0)  R[px][py][pz] += ss;
                                        else                  R[px][py][pz] -= ss;
                                    }
                                }
                            }

                        }
                    }
                }

            }



            // ????????????????


                vector3 PA;
                PA.x = P.x - A.x;
                PA.y = P.y - A.y;
                PA.z = P.z - A.z;

                vector3 PB;
                PB.x = P.x - B.x;
                PB.y = P.y - B.y;
                PB.z = P.z - B.z;

                //double kR2 = (k1*k2)*ik12*(ABv.x*ABv.x + ABv.y*ABv.y + ABv.z*ABv.z);
                double wab = AB.getW(b,a);

                // V
                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    for (int mb=0; mb<nmC[Lb]; ++mb) {
                        int bx = CartList[Lb][mb].nx;
                        int by = CartList[Lb][mb].ny;
                        int bz = CartList[Lb][mb].nz;

                        //very inefficient, but still a small fraction of the computation O(1)/O(N)
                        double r2e = R2E(ax,ay,az,PA, bx,by,bz,PB,  0.5*ik12, 0,0,0, R);

                        for (int ja=0; ja<Ja; ++ja) {
                            for (int jb=0; jb<Jb; ++jb) {
                                double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]; // wa [ja][a] * wb [jb][b];

                                VV[ja][jb][ma][mb] += w12 * r2e;
                            }
                        }
                    }
                }






            /*

            // transform Hermite to Cartesian
            // kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk

            double VP[2*LMAX+1][2*LMAX+1][2*LMAX+1];
            {
                for (int i=0; i<=L12; ++i)
                    for (int j=0; j<=L12-i; ++j)
                        for (int k=0; k<=L12-i-j; ++k)
                           VP[i][j][k] = 0;

                pol cartix[2*LMAX+1];

                for (int lt=0; lt<=L12; ++lt) cartix[lt] = Cexp(lt, .5*ik12);

                for (int px=0; px<=L12; ++px)
                    for (int py=0; py<=L12-px; ++py)
                        for (int pz=0; pz<=L12-px-py; ++pz)

                            for (int i=0; i<=px; ++i)
                                for (int j=0; j<=py; ++j)
                                    for (int k=0; k<=pz; ++k)
                                        VP[i][j][k] += R[px][py][pz] * cartix[px].w[i] * cartix[py].w[j] * cartix[pz].w[k];
            }


            // smaller HRR
            // kkkkkkkkkkk

            double VA[2*LMAX+1][2*LMAX+1][2*LMAX+1];
            {
                for (int i=0; i<=L12; ++i)
                    for (int j=0; j<=L12-i; ++j)
                        for (int k=0; k<=L12-i-j; ++k)
                           VA[i][j][k] = 0;

                vector3 AP = A-P;

                //generate HRRs
                pol tHRRx[LMAX+1];
                pol tHRRy[LMAX+1];
                pol tHRRz[LMAX+1];

                for (int lt=0; lt<=L12; ++lt) {
                    tHRRx[lt] = Pascal(lt, AP.x);
                    tHRRy[lt] = Pascal(lt, AP.y);
                    tHRRz[lt] = Pascal(lt, AP.z);
                }

                //move the expansions from P to A
                for (int px=0; px<=L12; ++px) {
                    for (int py=0; py<=L12-px; ++py) {
                        for (int pz=0; pz<=L12-px-py; ++pz) {

                            for (int ex=0; ex<=px; ++ex) {
                                for (int ey=0; ey<=py; ++ey) {
                                    for (int ez=0; ez<=pz; ++ez) {
                                        VA[ex][ey][ez] += VP [px][py][pz] * tHRRx[px].w[ex] * tHRRy[py].w[ey] * tHRRz[pz].w[ez];
                                    }
                                }
                            }

                        }
                    }
                }
            }

            // contract with coefficients
            // hhhhhhhhhhhhhhhhhhhhhhhhhh

            double wab = AB.getW(b,a);

            for (int ja=0; ja<Ja; ++ja) {
                for (int jb=0; jb<Jb; ++jb) {
                    double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b];

                    for (int ex=0; ex<=L12; ++ex) {
                        for (int ey=0; ey<=L12-ex; ++ey) {
                            for (int ez=0; ez<=L12-ex-ey; ++ez) {
                                JJ[ja][jb] [ex][ey][ez] += w12 * VA[ex][ey][ez];
                            }
                        }
                    }
                }
            }

            */

        }
    }




    int ta = AB.ata;
    int tb = AB.atb;
    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    double sumJ = 0;
                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumJ += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    Jp(ta,tb, ffa,ffb) = sumJ;
                }
            }
        }
    }



/*
    vector3 BA;
    BA =  A - B;

    //cout << "JJs  " << BA.x << " " << BA.y << " " << BA.z << endl;

	//generate HRRs
	pol pHRRx[LMAX+1];
	pol pHRRy[LMAX+1];
	pol pHRRz[LMAX+1];

	for (int lb=0; lb<=Lb; ++lb) {
        pHRRx[lb] = Pascal(lb, BA.x);
        pHRRy[lb] = Pascal(lb, BA.y);
        pHRRz[lb] = Pascal(lb, BA.z);
	}


    int ta = AB.ata;
    int tb = AB.atb;


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            double JJ3[MMAX+1][MMAX+1];

            for (int mb=0; mb<nmC[Lb]; ++mb) {
                int bx = CartList[Lb][mb].nx;
                int by = CartList[Lb][mb].ny;
                int bz = CartList[Lb][mb].nz;

                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    double sum = 0;

                    for (int fx=0; fx<=bx; ++fx) {
                        for (int fy=0; fy<=by; ++fy) {
                            for (int fz=0; fz<=bz; ++fz) {
                                sum += JJ[ja][jb] [ax+fx][ay+fy][az+fz]  * pHRRx[bx].w[fx] * pHRRy[by].w[fy] * pHRRz[bz].w[fz];
                            }
                        }
                    }

                    JJ3[ma][mb] = sum;
                }
            }


            for (int ma=0; ma<nmS[La]; ++ma) {
                for (int mb=0; mb<nmS[Lb]; ++mb) {
                    int ffa = AB.getFa() + ja*nmS[La] + ma;
                    int ffb = AB.getFb() + jb*nmS[Lb] + mb;

                    double sumJ = 0;
                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                            const SHTerm & sha = SHList[La][ma].T[i];
                            const SHTerm & shb = SHList[Lb][mb].T[j];
                            sumJ += sha.cN * shb.cN * JJ3[sha.nc][shb.nc];
                        }
                    }

                    Jp(ta,tb, ffa,ffb) = sumJ;
                }
            }

        }
    }
    */
}

/*
void SystemDensity::Fit() {

    //load the basis set
    DFbasis.name = "basis/DF.bs";
    DFbasis.Load();
}
*/



void SystemDensity::Clear() {

    list <HermiteCenter*>::iterator it;

    for (it=HClist.begin(); it!=HClist.end(); ++it) {
        HermiteCenter * HC = *it;
        delete HC;
    }

    HClist.clear();
}

void SystemDensity::Output() const {

    list <HermiteCenter*>::const_iterator it;

    int n;

    for (it=HClist.begin(); it!=HClist.end(); ++it) {
        const HermiteCenter * HC = *it;
        ++n;

        cout << n << "    " << HC->L << " " << HC->nu << "       " << HC->C.x << ", " << HC->C.y << "," << HC->C.z << endl;

    }


}




/*
    =============
    OLD SOLVER(S)
    =============
*/



void MolElData::ComputeJ() {

    const ShellPair * SPs = EchidnaSolver.GetShellPairs();
    const rAtom     * ALs = EchidnaSolver.GetAtoms();

    symtensor2 J; // no longer in MolElData

    Messenger << "Computing Coulomb tensor";
    Messenger.Push(); {


        int nSPs = EchidnaSolver.nShellPairs();

        // generating density:
        for (int i=0; i<nSPs; ++i) Density.AddShellPair            (SPs[i], ALs, Dp);

        //Density.Output();

        {
            symtensor Jp;
            Jp.init  (TensorPattern);
            Jp.touch (AtomInteractions);

            #pragma omp parallel for
            for (int i=0; i<nSPs; ++i)
                Density.ComputeCoulombPotential (SPs[i], ALs, Jp);

            Density.Clear();

            J = Jp;
        }

        WriteTensor("Jmat.1.txt", J);

        /*

        int nSPs = EchidnaSolver.nShellPairs();


        for (int i=0; i<nSPs; ++i) {
            Density.AddShellPair            (SPs[i], ALs, Dp);

            for (int j=0; j<nSPs; ++j) Density.ComputeCoulombPotential (SPs[j], ALs, Jp);

            Density.Clear();
            J = Jp;

            char n[] = "0";
            n[0] += i;

            string file = "Jmat.new." + string(n) + ".txt";

            cout << file << endl;

            WriteTensor(file, J);
        }
        */



    } Messenger.Pop();
}

// density multiresolution analysis
void DMRA (tensor2 & D2, int nel) {

    cout << "Value at the diagonal" << endl;
    for (int i=0; i<D2.n; ++i) cout << i << " " << D2(i,i) << endl;

    // 1 pick the indices with the largest value at the diagonal
    // 2 find the index most strongly correlated to it, add it to the 'tentative' list
    // 3 check no other index is attempting to use it; if so, add to definitive list
    // 4 repeat unless subspaces collide or value gets too low
    // 5 find principal components; add them to the list of wavelets
    // 6 update matrix (D'= P D P  &&  P = I - v*v+)

    D2 *= -1; // sort the evs correctly

    double occ[D2.n];
    DiagonalizeV(D2, occ);

    cout << "Occupations in AO" << endl;
    double nn= 0.;
    for (int i=0; i<nel+1; ++i) {nn += occ[i]; cout << i << " " << -occ[i] << endl;}
    cout << "electrons: " << -2*nn << endl;

    D2.n = nel;
    WriteTensor( "MOs.txt",  D2);
}


#include <set>

const int NS=8;

struct sPC {
    double c[NS][NS]; // coefficient
    double v[NS];     // eigenvalue
    int    i[NS];     // indices
    int    n;         // number of elements
};

// nonortogonal sparse PCA
// matrix is SDP
void NOSPCA (tensor2 & D2) {

    cout << "Executing NO SPCA" << endl;

    const int n = D2.n;

    // sparse vectors
    sPC * sV = new sPC[n*n];
    int nsv = 0;


    // indices' labels
    int idx0[n];
    for (int i=0; i<n; ++i) idx0[i]=i;


    while (true) {
        // compute the trace
        double tr = 0;
        for (int i=0; i<n; ++i) tr += D2(i,i);

        cout << "trace: " << tr << " vectors: " << nsv << endl;
        //if (tr<0.01) break;
        if (nsv==n*n) break;

        // 0 sort in descending order
        for (int i=0; i<n; ++i) {
            // find largest diagonal element
            int k=i; for (int j=i+1; j<n; ++j) if (D2(j,j)>D2(k,k)) k=j;
            // swap rows
            for (int j=0; j<n; ++j) swap(D2(i,j),D2(k,j));
            // swap columns
            for (int j=0; j<n; ++j) swap(D2(j,i),D2(j,k));
            // swap indices
            swap(idx0[i],idx0[k]);

            cout << D2(i,i) << " ";
        }

        cout << endl;

        // check convergence
        {
            double F2=0;
            for (int i=0; i<n; ++i) {
                for (int j=0; j<i; ++j) {
                    F2 += D2(i,j)*D2(i,j);
                }
            }
            F2 /= double(n*n-n);
            F2 = sqrt(F2);

            if (F2 < 1E-6) {
                cout << "converged!" << endl;
                break;
            }
            else {
                cout << "residual " << F2 << endl;
                char a; cin >> a;
            }
        }

        // 1 find a partition of the indices
        set<int> idxs; for (int i=0; i<n; ++i) idxs.insert(i);
        set<int> ::iterator it1,it2;
        int nps = 0; // number of partitions

        for (it1=idxs.begin(); it1!=idxs.end(); ) {

            const int i=*it1;

            // find maximally overlapping vectors
            // using Cauchy-Schwarz

            int    sidx[NS];
            double fidx[NS];
            int    nns = 1;
            sidx[0] = i;
            fidx[0] = 1.;

            // loop over remaining indices
            it2=it1; ++it2;
            for (; it2!=idxs.end(); ++it2) {

                const int j=*it2;
                double f = (D2(i,j)*D2(i,j)) / (D2(i,i)*D2(j,j));

                //if (f<0.01) continue; // small overlap

                if ((nns<NS) || (f>fidx[NS-1])) {

                    if (nns<NS) ++nns; // list not full yet

                    // add to the last
                    sidx[nns-1] = j;
                    fidx[nns-1] = f;

                    // insertion sort: always skip first element
                    for (int p=nns-1; p>1; --p) {
                        if (fidx[p-1]<fidx[p]) {
                            swap(sidx[p-1],sidx[p]);
                            swap(fidx[p-1],fidx[p]);
                        }
                    }
                }
            }


            if (nns>1) {
                // 2 find largest PC of subblock
                tensor2 SB(nns);

                for (int p=0; p<nns; ++p) {
                    for (int q=0; q<nns; ++q) {
                        int ii=sidx[p];
                        int jj=sidx[q];
                        SB(p,q) = -D2(ii,jj);
                    }
                }

                double dd[NS];
                DiagonalizeV(SB, dd);

                // store PCA and eigenvalue
                for (int p=0; p<nns; ++p) {
                    sV[nsv].i[p] = sidx[p]; // idx0[sidx[p]];
                    sV[nsv].v[p] = -dd[p];
                    for (int q=0; q<nns; ++q) {
                        sV[nsv].c[p][q] = SB(p,q);
                    }
                }
                sV[nsv].n = nns;

                for (int p=0; p<nns; ++p) cout << -dd[p] << " "; cout << endl;

                ++nsv; // total number of sparse vectors
                ++nps; // number of partitions
            }

            for (int p=0; p<nns; ++p) cout << sidx[p] << " "; cout << endl;

            // delete indices from set (skip i)
            for (int p=1; p<nns; ++p) idxs.erase(sidx[p]);
            ++it1; // find next valid element
            idxs.erase(sidx[0]); // now delete i

            if (nsv==n*n) break;
        }

        cout << endl;

        cout << "eee" << endl;

        // outproject all gathered sparse vectors
        for (int t=0; t<nps; ++t) {
            sPC & sv = sV[nsv-1-t];

            /*
            double ev = 0;

            for (int p=0; p<sv.n; ++p) {
                for (int q=0; q<sv.n; ++q) {
                    int ii=sv.i[p];
                    int jj=sv.i[q];
                    ev += D2(ii,jj) * sv.c[p] * sv.c[q];
                }
            }
            cout << ev << " ";
            */

            /*
            for (int p=0; p<sv.n; ++p) {
                for (int q=0; q<sv.n; ++q) {
                    int ii=sv.i[p];
                    int jj=sv.i[q];
                    D2(ii,jj) -= sv.v * sv.c[p] * sv.c[q];
                }
            }
            */

            // linearly combine rows
            for (int jj=0; jj<n; ++jj) {
                double vv[NS];
                // rotation
                for (int p=0; p<sv.n; ++p) {
                    int ii=sv.i[p];
                    vv[p]=0;
                    for (int r=0; r<sv.n; ++r) {
                        int kk=sv.i[r];
                        vv[p] += sv.c[p][r] * D2(kk,jj);
                    }
                }
                // còpy
                for (int p=0; p<sv.n; ++p) {
                    int ii=sv.i[p];
                    D2(ii,jj) = vv[p];
                }
            }

            // linearly combine cols
            for (int jj=0; jj<n; ++jj) {
                double vv[NS];
                // rotation
                for (int p=0; p<sv.n; ++p) {
                    int ii=sv.i[p];
                    vv[p]=0;
                    for (int r=0; r<sv.n; ++r) {
                        int kk=sv.i[r];
                        vv[p] += sv.c[p][r] * D2(jj,kk);
                    }
                }
                // còpy
                for (int p=0; p<sv.n; ++p) {
                    int ii=sv.i[p];
                    D2(jj,ii) = vv[p];
                }
            }

        }

        cout << endl;

        // correct the indices
        for (int t=0; t<nps; ++t) {
            sPC & sv = sV[nsv-1-t];

            for (int p=0; p<sv.n; ++p)
                sv.i[p] = idx0[sv.i[p]];
        }

    }



}


