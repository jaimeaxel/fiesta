/*
    1electron.cpp

    calcula las integrales monoelectronicas a alto nivel;
    suma de las contribuciones de cada primitiva, etc.
*/


#include <cmath>

#include "defs.hpp"

#include "molecule/atoms.hpp"

#include "basis/atomprod.hpp"
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"

#include "math/angular.hpp"

#include "linear/tensors.hpp"
#include "linear/sparse.hpp"
#include "linear/newtensors.hpp"

#include "math/gamma.hpp" //rutinas de integracion de bajo nivel, 'inlined'

#include "1eints/1electron.hpp"

#include "libechidna/libechidna.hpp"

using namespace LibAngular;
using namespace LibIGamma;


typedef double Triple  [2*LMAX+1][2*LMAX+1][2*LMAX+1];
typedef double TripleX [2*LMAX+3][2*LMAX+3][2*LMAX+3];

typedef void (*Rint)  (Triple &  , double, double, const point &, const point &);
typedef void (*RintX) (TripleX & , double, double, const point &, const point &);


typedef double CTriple [4*LMAX+1][4*LMAX+1][2*LMAX+1][2*LMAX+1][2*LMAX+1];



#include "molecule/prescreen.hpp"
#include <list>

struct Nuke {
    point c;
    double Z;

    bool operator<(const Nuke & rhs) const {
        if (c.x != rhs.c.x) return (c.x<rhs.c.x);
        if (c.y != rhs.c.y) return (c.y<rhs.c.y);
        if (c.z != rhs.c.z) return (c.z<rhs.c.z);
        if (Z   != rhs.Z  ) return (Z  <rhs.Z  );
    }
};




/* ************************************************************************************************** */


// assume normalization of the charge : (PI/k)*sqrt(PI/k);

template <int L> void RIntegral(Triple & TR, double k, double Z, const point & P, const point & N) {
    double Fn[L+1];

    vector3 R = N-P;
    double R2 = R*R;


    double ** gamma = IncompleteGamma.gamma_table;
    double *  gamma2 = IncompleteGammas[L].gamma_table_vals;

    //calcula las 'F's

    {
        double Sn[L+1];

        double kR2 = k*R2;

        if (kR2>vg_max-vg_step) {
            double iR2 = 1/R2;
            Sn[0] = sqrt(iR2);

            for (int i=1; i<=L; ++i) {
                Sn[i] = Sn[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = ivg_step*(kR2-vg_min);
            int pos = int(p+0.5);
            double x0 = vg_min + vg_step*double(pos);
            double Ax1 = x0-kR2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Sn[L] = (2/sqrt(PI))*(gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = (2/sqrt(PI))*gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Sn[i] = (2*kR2*Sn[i+1] + expx)*(1./double(2*i+1));
            }

            double kn   = sqrt(k);

            for (int n=0; n<=L; ++n) {
                Sn[n] *= kn;
                kn    *= 2*k;
            }
        }


        double Ln[L+1];

        /*

        if (UseCASE) {
            double kw2 = (k*case_w2)/(k+case_w2);

            double kw2R2 = kw2*R2;

            if (kw2R2>GammaFunction::vg_max-GammaFunction::vg_step) {
                double iR2 = 1/R2;

                Ln[0] = 0.5*sqrt(PI*iR2);

                for (int i=1; i<=L; ++i) {
                    Ln[i] = Ln[i-1]*iR2*double(2*i-1);
                }
            }
            else {
                double p = GammaFunction::ivg_step*(kw2R2-GammaFunction::vg_min);
                int pos = int(p+0.5);
                double x0 = GammaFunction::vg_min + GammaFunction::vg_step*double(pos);
                double Ax1 = x0-kw2R2;
                double Ax2 = 0.5 * Ax1*Ax1;
                double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

                Ln[L] = (gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

                if (L>0) {
                    double expx = gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                    for (int i=L-1; i>=0; --i)
                        Ln[i] = (2*kw2R2*Ln[i+1] + expx)*(1./double(2*i+1));
                }

                double kw2n = sqrt(kw2);

                for (int n=0; n<=L; ++n) {
                    Ln[n] *= kw2n;
                    kw2n  *= 2*kw2;
                }
            }

        }
        */


        //if (UseCASE)
        //    for (int n=0; n<=L; ++n) Fn[n] = Sn[n] - Ln[n];
        //
        //else
            for (int n=0; n<=L; ++n) Fn[n] = Sn[n];
        //

    }


    //calcula R^{n}_{000}
    Triple Rtuv;


    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }


    for (int px=0; px<=L; ++px)
        for (int py=0; py<=L-px; ++py)
            for (int pz=0; pz<=L-px-py; ++pz)
                TR[px][py][pz] -= Z * Rtuv[px][py][pz];
}

template <int L> void RIntegralX(TripleX & Rtuv, double k, double RQ, const point & P, const point & N) {

    double Fn[L+1];

    vector3 R;
    R.x = N.x-P.x;
    R.y = N.y-P.y;
    R.z = N.z-P.z;

    double R2 = R.x*R.x + R.y*R.y + R.z*R.z;

    double ** gamma = IncompleteGamma.gamma_table;
    double *  gamma2 = IncompleteGammas[L].gamma_table_vals;

    //calcula las 'F's
    {
        double Sn[L+1];

        double kR2 = k*R2;

        if (kR2>LibIGamma::vg_max-LibIGamma::vg_step) {
            double iR2 = 1/R2;
            Sn[0] = sqrt(iR2);

            for (int i=1; i<=L; ++i) {
                Sn[i] = Sn[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = LibIGamma::ivg_step*(kR2-LibIGamma::vg_min);
            int pos = int(p+0.5);
            double x0 = LibIGamma::vg_min + LibIGamma::vg_step*double(pos);
            double Ax1 = x0-kR2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Sn[L] = (2/sqrt(PI))*(gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = (2/sqrt(PI)) * gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Sn[i] = (2*kR2*Sn[i+1] + expx)*(1./double(2*i+1));
            }

            double kn   = sqrt(k);

            for (int n=0; n<=L; ++n) {
                Sn[n] *= kn;
                kn    *= 2*k;
            }
        }


        /*
        double Ln[L+1];

        double w = 1/RQ;
        double w2 = w*w;
        double kw2 = (k*w2)/(k+w2);

        double kw2R2 = kw2*R2;

        if (kw2R2>LibIGamma::vg_max-LibIGamma::vg_step) {
            double iR2 = 1/R2;

            Ln[0] = sqrt(iR2);

            for (int i=1; i<=L; ++i) {
                Ln[i] = Ln[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = LibIGamma::ivg_step*(kw2R2-LibIGamma::vg_min);
            int pos = int(p+0.5);
            double x0 = LibIGamma::vg_min + LibIGamma::vg_step*double(pos);
            double Ax1 = x0-kw2R2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Ln[L] = (2/sqrt(PI))*(gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = (2/sqrt(PI))*gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Ln[i] = (2*kw2R2*Ln[i+1] + expx)*(1./double(2*i+1));
            }

            double kw2n = sqrt(kw2);

            for (int n=0; n<=L; ++n) {
                Ln[n] *= kw2n;
                kw2n  *= 2*kw2;
            }
        }
        */

        for (int n=0; n<=L; ++n) Fn[n] = Sn[n]; // - Ln[n];
    }


    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }

}

// since the MMD-like derivation is almost exactly the same except for the evaluation of the kernel, we could optimize ONE implementation and switch kernels

template <int L> void PPIntegral(Triple & TR, double k, double c, const point & P, const point & N) {
    double Fn[L+1];

    vector3 R = N-P;
    double R2 = R*R;


    //calcula las 'F's

    {
        double Fn[L+1];

        double kR2 = k*R2;

        double f0 = exp(-kR2);

        double kn   = sqrt(k)*k; // this i

        for (int i=0; i<=L; ++i) {
            Fn[L] = kn * (kR2 - i + c)*f0;
            kn    *= 2*k; // this corresponds to the derivatives of z
        }

    }


    //calcula R^{n}_{000}
    Triple Rtuv;


    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }


    for (int px=0; px<=L; ++px)
        for (int py=0; py<=L-px; ++py)
            for (int pz=0; pz<=L-px-py; ++pz)
                TR[px][py][pz] += Rtuv[px][py][pz];
}







double R2E(int ax, int ay, int az, double iz, const vector3 & PA,           int px, int py, int pz, Triple & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * R2E(ax-1,ay,az, iz, PA, px-1,py,pz, R) +
               PA.x * R2E(ax-1,ay,az, iz, PA, px  ,py,pz, R) +
               iz   * R2E(ax-1,ay,az, iz, PA, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * R2E(ax,ay-1,az, iz, PA, px,py-1,pz, R) +
               PA.y * R2E(ax,ay-1,az, iz, PA, px,py  ,pz, R) +
               iz   * R2E(ax,ay-1,az, iz, PA, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * R2E(ax,ay,az-1, iz, PA, px,py,pz-1, R) +
               PA.z * R2E(ax,ay,az-1, iz, PA, px,py,pz  , R) +
               iz   * R2E(ax,ay,az-1, iz, PA, px,py,pz+1, R);
    }
    return R[px][py][pz];
}

double HRR(int ax, int ay, int az, int bx, int by, int bz, const vector3 & AB, double (R) [2*LMAX+1][2*LMAX+1][2*LMAX+1]) {

    if (bx<0) return 0;
    if (by<0) return 0;
    if (bz<0) return 0;

    if (bx>0) {
        return          HRR(ax+1,ay,  az,   bx-1,by,  bz,   AB, R) -
                 AB.x * HRR(ax  ,ay,  az,   bx-1,by,  bz,   AB, R);
    }
    if (by>0) {
        return          HRR(ax,  ay+1,az,   bx,  by-1,bz,   AB, R) -
                 AB.y * HRR(ax,  ay,  az,   bx,  by-1,bz,   AB, R);
    }
    if (bz>0) {
        return          HRR(ax,  ay,  az+1, bx,  by,  bz-1, AB, R) -
                 AB.z * HRR(ax,  ay,  az,   bx,  by,  bz-1, AB, R);
    }
    return R[ax][ay][az];
}

double R2E(int ax, int ay, int az, const vector3 & PA,           int bx, int by, int bz, const vector3 & PB,           double iz, int px, int py, int pz, Triple & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px-1,py,pz, R) +
               PA.x * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax-1,ay,az, PA, bx,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py-1,pz, R) +
               PA.y * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay-1,az, PA, bx,by,bz, PB, iz, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz-1, R) +
               PA.z * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az-1, PA, bx,by,bz, PB, iz, px,py,pz+1, R);
    }

    if (bx>0) {
        return px   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px-1,py,pz, R) +
               PB.x * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px  ,py,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx-1,by,bz, PB, iz, px+1,py,pz, R);
    }
    if (by>0) {
        return py   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py-1,pz, R) +
               PB.y * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py  ,pz, R) +
               iz   * R2E(ax,ay,az, PA, bx,by-1,bz, PB, iz, px,py+1,pz, R);
    }
    if (bz>0) {
        return pz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz-1, R) +
               PB.z * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz  , R) +
               iz   * R2E(ax,ay,az, PA, bx,by,bz-1, PB, iz, px,py,pz+1, R);
    }


    return R[px][py][pz];
}

double CTE(int b , int p ,           int ax, int ay, int az, const vector3 & AB,           int px, int py, int pz, CTriple & R) {

    if (px<0) return 0;
    if (py<0) return 0;
    if (pz<0) return 0;

    if (ax>0) {
        return px   * CTE(b,   p,   ax-1,ay,az, AB, px-1,py,pz, R) +
               AB.x * CTE(b+1, p+1, ax-1,ay,az, AB, px  ,py,pz, R) +
                      CTE(b,   p+1, ax-1,ay,az, AB, px+1,py,pz, R);
    }
    if (ay>0) {
        return py   * CTE(b,   p,   ax,ay-1,az, AB, px,py-1,pz, R) +
               AB.y * CTE(b+1, p+1, ax,ay-1,az, AB, px,py  ,pz, R) +
                      CTE(b,   p+1, ax,ay-1,az, AB, px,py+1,pz, R);
    }
    if (az>0) {
        return pz   * CTE(b,   p,   ax,ay,az-1, AB, px,py,pz-1, R) +
               AB.z * CTE(b+1, p+1, ax,ay,az-1, AB, px,py,pz  , R) +
                      CTE(b,   p+1, ax,ay,az-1, AB, px,py,pz+1, R);
    }

    return R[b][p][px][py][pz];
}

//


void CalcPotentialCart(const ShellPair & AB, const rAtom * nuclei, int nnuclei, symtensor & Vp) {


    int La = AB.getLa();
    int Lb = AB.getLb();

	int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);

    double VV[maxJ][maxJ][MMAX][MMAX];

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<nmC[AB.getLa()]; ++i) {
                for (int j=0; j<nmC[AB.getLb()]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }
        }
    }

    Rint VRint;

    switch (Ltot) {
        case  0: VRint = RIntegral< 0>; break;
        case  1: VRint = RIntegral< 1>; break;
        case  2: VRint = RIntegral< 2>; break;
        case  3: VRint = RIntegral< 3>; break;
        case  4: VRint = RIntegral< 4>; break;
        case  5: VRint = RIntegral< 5>; break;
        case  6: VRint = RIntegral< 6>; break;
        case  7: VRint = RIntegral< 7>; break;
        case  8: VRint = RIntegral< 8>; break;
        case  9: VRint = RIntegral< 9>; break;
        case 10: VRint = RIntegral<10>; break;
    }

    RintX MRint;

    switch (Ltot) {
        case  0: MRint = RIntegralX< 0>; break;
        case  1: MRint = RIntegralX< 1>; break;
        case  2: MRint = RIntegralX< 2>; break;
        case  3: MRint = RIntegralX< 3>; break;
        case  4: MRint = RIntegralX< 4>; break;
        case  5: MRint = RIntegralX< 5>; break;
        case  6: MRint = RIntegralX< 6>; break;
        case  7: MRint = RIntegralX< 7>; break;
        case  8: MRint = RIntegralX< 8>; break;
        case  9: MRint = RIntegralX< 9>; break;
        case 10: MRint = RIntegralX<10>; break;
    }



	point A = nuclei[AB.ata].c;
	point B = nuclei[AB.atb].c;

    vector3 ABv = A - B;

    //loop over primitives

        for (int b=0; b<AB.getKb(); ++b) {
            for (int a=0; a<AB.getKa(b); ++a) {

                Triple R;
                {
                    for (int px=0; px<=Ltot; ++px)
                        for (int py=0; py<=Ltot-px; ++py)
                            for (int pz=0; pz<=Ltot-px-py; ++pz)
                                R[px][py][pz] = 0;
                }


                double k1, k2, k12, ik12;

                k1  = AB.GP->ka[a];
                k2  = AB.GP->kb[b];

                k12 = k1+k2;
                ik12 = 1/k12;

                point P;

                P.x = (k1*A.x + k2*B.x)*ik12;
                P.y = (k1*A.y + k2*B.y)*ik12;
                P.z = (k1*A.z + k2*B.z)*ik12;





                TripleX TR;

                double RRR = 0;

                // monopole contribution
                for (int i=0; i<nnuclei; ++i) {
                    const std::string aname = nuclei[i].rAP->name;

                    if (aname[0] == '*') continue; // skip atoms with pseudopotential

                    //add the damped point charge contribution
                    MRint(TR, k12, RRR, P, nuclei[i].c);

                    for (int px=0; px<=Ltot; ++px) {
                        for (int py=0; py<=Ltot-px; ++py) {
                            for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                R[px][py][pz] -= nuclei[i].rAP->Z * TR[px][py][pz]; //
                            }
                        }
                    }
                }

                vector3 PA;
                PA = P - A;

                vector3 PB;
                PB = P - B;

                //double wab = exp(-(AB.GP->kr[a][b]) * AB.R2);
                double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // compensate the lack of normalization of the gaussiandensity

                // V
                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    for (int mb=0; mb<nmC[Lb]; ++mb) {
                        int bx = CartList[Lb][mb].nx;
                        int by = CartList[Lb][mb].ny;
                        int bz = CartList[Lb][mb].nz;

                        //very inefficient, but still a small fraction of the computation O(1)/O(N)
                        double r2e = R2E(ax,ay,az,PA, bx,by,bz,PB,  0.5*ik12, 0,0,0, R);

                        for (int ja=0; ja<Ja; ++ja) {
                            for (int jb=0; jb<Jb; ++jb) {
                                double w12 = wab; //AB.W[b][a];

                                if (La==LSP && ma>0) w12 *= AB.GP->Nap[ja][a];
                                else                 w12 *= AB.GP->Na [ja][a];

                                if (Lb==LSP && mb>0) w12 *= AB.GP->Nbp[jb][b];
                                else                 w12 *= AB.GP->Nb [jb][b];

                                VV[ja][jb][ma][mb] += w12 * r2e;
                            }
                        }
                    }
                }

            }
        }



    int ta = AB.ata;
    int tb = AB.atb;
    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    double sumV = 0;
                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    Vp(ta,tb, ffa,ffb) = sumV;
                }
            }
        }
    }

}


void CalcPotentialCart1(const ShellPair & AB, const rAtom * nuclei, const Boxing<Nucleus> & TBS, symtensor & Vp) {

    double VV[maxJ][maxJ][MMAX][MMAX];

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<nmC[AB.getLa()]; ++i) {
                for (int j=0; j<nmC[AB.getLb()]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }
        }
    }



    int La = AB.getLa();
    int Lb = AB.getLb();

	int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);


    Rint VRint;

    switch (Ltot) {
        case  0: VRint = RIntegral< 0>; break;
        case  1: VRint = RIntegral< 1>; break;
        case  2: VRint = RIntegral< 2>; break;
        case  3: VRint = RIntegral< 3>; break;
        case  4: VRint = RIntegral< 4>; break;
        case  5: VRint = RIntegral< 5>; break;
        case  6: VRint = RIntegral< 6>; break;
        case  7: VRint = RIntegral< 7>; break;
        case  8: VRint = RIntegral< 8>; break;
        case  9: VRint = RIntegral< 9>; break;
        case 10: VRint = RIntegral<10>; break;
    }





	point A = nuclei[AB.ata].c;
	point B = nuclei[AB.atb].c;

    vector3 ABv = A - B;

    //retrieve list of interacting nuclei
    std::set<Nuke> InteractingNuclei;

    {
        const Box<Nucleus> * BoxList[8];

        double k1, k2, k12, iz;

        k1  = AB.GP->ka[0];
        k2  = AB.GP->kb[0];
        k12 = k1+k2;
        iz = 1/(2*k12);

        point P;

        P.x = (k1*A.x + k2*B.x)/k12;
        P.y = (k1*A.y + k2*B.y)/k12;
        P.z = (k1*A.z + k2*B.z)/k12;

        TBS.RetrieveBoxes(P, BoxList);

        for (int i=0; i<8; ++i) {
            if (BoxList[i]!=NULL) {
                std::list<Nucleus>::const_iterator it;

                int len = BoxList[i]->elements.size();

                for (it=BoxList[i]->elements.begin(); it!=BoxList[i]->elements.end(); ++it) {
                    Nuke N;
                    N.c = it->c;
                    N.Z = it->Z;
                    InteractingNuclei.insert(N);
                }

            }
        }


    }



    //loop over primitives
    {
        for (int b=0; b<AB.getKb(); ++b) {
            for (int a=0; a<AB.getKa(b); ++a) {

                Triple R;
                {
                    for (int px=0; px<=Ltot; ++px)
                        for (int py=0; py<=Ltot-px; ++py)
                            for (int pz=0; pz<=Ltot-px-py; ++pz)
                                R[px][py][pz] = 0;
                }


                double k1, k2, k12, iz;

                k1  = AB.GP->ka[a];
                k2  = AB.GP->kb[b];
                k12 = k1+k2;
                iz = 1/(2*k12);

                point P;

                P.x = (k1*A.x + k2*B.x)/k12;
                P.y = (k1*A.y + k2*B.y)/k12;
                P.z = (k1*A.z + k2*B.z)/k12;


                //accumulate
                //**********
                std::set<Nuke>::const_iterator it;

                for (it=InteractingNuclei.begin(); it!=InteractingNuclei.end(); ++it) {
                    VRint(R, k12, it->Z, P, it->c);
                }


                vector3 PA;
                PA = P - A;

                vector3 PB;
                PB = P - B;


                double wab = AB.getW(b,a) * (PI/k12)*sqrt(PI/k12); // compensate the normalization


                // V
                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    for (int mb=0; mb<nmC[Lb]; ++mb) {
                        int bx = CartList[Lb][mb].nx;
                        int by = CartList[Lb][mb].ny;
                        int bz = CartList[Lb][mb].nz;

                        //very inefficient, but still a small fraction of the computation O(1)/O(N)
                        double r2e = R2E(ax,ay,az,PA, bx,by,bz,PB,  iz, 0,0,0, R);

                        for (int ja=0; ja<Ja; ++ja) {
                            for (int jb=0; jb<Jb; ++jb) {
                                double w12 = wab; //AB.W[b][a];

                                if (La==LSP && ma>0) w12 *= AB.GP->Nap[ja][a];
                                else                 w12 *= AB.GP->Na [ja][a];

                                if (Lb==LSP && mb>0) w12 *= AB.GP->Nbp[jb][b];
                                else                 w12 *= AB.GP->Nb [jb][b];

                                VV[ja][jb][ma][mb] += w12 * r2e;
                            }
                        }
                    }
                }

            }
        }
    }


    int ta = AB.ata;
    int tb = AB.atb;
    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    double sumV = 0;
                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    Vp(ta,tb, ffa,ffb) = sumV;
                }
            }
        }
    }

}


void CalcExternalPotential (const ShellPair & AB, const rAtom * nuclei, const ExternalPotential & XV, symtensor & XVp) {

    double VV[maxJ][maxJ][MMAX][MMAX];

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;
    int La = AB.getLa();
    int Lb = AB.getLb();
	int Ltot = ((La==LSP)?1:La) + ((Lb==LSP)?1:Lb);
	//int Ltot = La + Lb;


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }
        }
    }


    RintX MRint, DRint, QRint;

    switch (Ltot) {
        case  0: MRint = RIntegralX< 0>; break;
        case  1: MRint = RIntegralX< 1>; break;
        case  2: MRint = RIntegralX< 2>; break;
        case  3: MRint = RIntegralX< 3>; break;
        case  4: MRint = RIntegralX< 4>; break;
        case  5: MRint = RIntegralX< 5>; break;
        case  6: MRint = RIntegralX< 6>; break;
        case  7: MRint = RIntegralX< 7>; break;
        case  8: MRint = RIntegralX< 8>; break;
        case  9: MRint = RIntegralX< 9>; break;
        case 10: MRint = RIntegralX<10>; break;
    }

    switch (Ltot) {
        case  0: DRint = RIntegralX< 1>; break;
        case  1: DRint = RIntegralX< 2>; break;
        case  2: DRint = RIntegralX< 3>; break;
        case  3: DRint = RIntegralX< 4>; break;
        case  4: DRint = RIntegralX< 5>; break;
        case  5: DRint = RIntegralX< 6>; break;
        case  6: DRint = RIntegralX< 7>; break;
        case  7: DRint = RIntegralX< 8>; break;
        case  8: DRint = RIntegralX< 9>; break;
        case  9: DRint = RIntegralX<10>; break;
        case 10: DRint = RIntegralX<11>; break;
    }

    switch (Ltot) {
        case  0: QRint = RIntegralX< 2>; break;
        case  1: QRint = RIntegralX< 3>; break;
        case  2: QRint = RIntegralX< 4>; break;
        case  3: QRint = RIntegralX< 5>; break;
        case  4: QRint = RIntegralX< 6>; break;
        case  5: QRint = RIntegralX< 7>; break;
        case  6: QRint = RIntegralX< 8>; break;
        case  7: QRint = RIntegralX< 9>; break;
        case  8: QRint = RIntegralX<10>; break;
        case  9: QRint = RIntegralX<11>; break;
        case 10: QRint = RIntegralX<12>; break;
    }


	point A = nuclei[AB.ata].c;
	point B = nuclei[AB.atb].c;


    vector3 ABv;
    ABv.x = A.x - B.x;
    ABv.y = A.y - B.y;
    ABv.z = A.z - B.z;

    //loop over primitives
    {
        //for (int b=0; b<nKb; ++b) {
        //    for (int a=0; a<nKa; ++a) {

        for (int b=0; b<AB.getKb(); ++b) {
            for (int a=0; a<AB.getKa(b); ++a) {

                Triple R;

                for (int px=0; px<=Ltot; ++px)
                    for (int py=0; py<=Ltot-px; ++py)
                        for (int pz=0; pz<=Ltot-px-py; ++pz)
                            R[px][py][pz] = 0;

                double k1, k2, k12, ik12;

                //k1  = ka[a];
                //k2  = kb[b];
                k1  = AB.GP->ka[a];
                k2  = AB.GP->kb[b];

                k12 = k1+k2;
                ik12 = 1/k12;

                point P;

                P.x = (k1*A.x + k2*B.x)*ik12;
                P.y = (k1*A.y + k2*B.y)*ik12;
                P.z = (k1*A.z + k2*B.z)*ik12;

                //accumulate
                //**********
                TripleX TR;

                double RRR = 0;

                // monopole contribution
                for (int i=0; i<XV.Ms; ++i) {

                    //add the damped point charge contribution
                    MRint(TR, k12, RRR, P, XV.MC[i]);

                    for (int px=0; px<=Ltot; ++px) {
                        for (int py=0; py<=Ltot-px; ++py) {
                            for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                R[px][py][pz] -= XV.M[i] * TR[px][py][pz]; // + (Z charge) x - (e charge) = -
                            }
                        }
                    }
                }

                // dipole contribution
                for (int i=0; i<XV.Ds; ++i) {
                    //add the dipole moment contributions
                    DRint(TR, k12, RRR, P, XV.DC[i]);

                    for (int px=0; px<=Ltot; ++px) {
                        for (int py=0; py<=Ltot-px; ++py) {
                            for (int pz=0; pz<=Ltot-px-py; ++pz) {
                                // - (e charge) x - (r to p|q hermite decoupling) = +
                                R[px][py][pz] += XV.D[i].x * TR[px+1][py][pz];
                                R[px][py][pz] += XV.D[i].y * TR[px][py+1][pz];
                                R[px][py][pz] += XV.D[i].z * TR[px][py][pz+1];
                            }
                        }
                    }
                }

                //quadrupole contibution
                //not implemented

                vector3 PA;
                PA.x = P.x - A.x;
                PA.y = P.y - A.y;
                PA.z = P.z - A.z;

                vector3 PB;
                PB.x = P.x - B.x;
                PB.y = P.y - B.y;
                PB.z = P.z - B.z;

                double kR2 = (k1*k2)*ik12*(ABv.x*ABv.x + ABv.y*ABv.y + ABv.z*ABv.z);
                double wab = exp(-kR2) * (PI*ik12)*sqrt(PI*ik12); // compensate also for lack of normalization
                //double wab = AB.getW(b,a);

                // V
                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    for (int mb=0; mb<nmC[Lb]; ++mb) {
                        int bx = CartList[Lb][mb].nx;
                        int by = CartList[Lb][mb].ny;
                        int bz = CartList[Lb][mb].nz;

                        //very inefficient, but still a small fraction of the computation O(1)/O(N)
                        double r2e = R2E(ax,ay,az,PA, bx,by,bz,PB,  0.5*ik12, 0,0,0, R);

                        for (int ja=0; ja<Ja; ++ja) {
                            for (int jb=0; jb<Jb; ++jb) {
                                double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b]; // wa [ja][a] * wb [jb][b];

                                VV[ja][jb][ma][mb] += w12 * r2e;
                            }
                        }
                    }
                }

            }
        }
    }

    /*

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = ja*nmS[La] + a;
                    int ffb = jb*nmS[Lb] + b;



                    double sumV = 0;
                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    V[ja][jb][a][b] = sumV;
                }
            }
        }
    }
    */

    int ta = AB.ata;
    int tb = AB.atb;
    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    double sumXV = 0;
                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumXV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    XVp(ta,tb, ffa,ffb) = sumXV;
                }
            }
        }
    }

}





/* ************************************************************************************************** */


inline void SRR(double (&S)[LMAX+2][LMAX+2], double AP, double BP, double k12, int la, int lb) {

    for (int ab=0; ab<(LMAX+2)*(LMAX+2); ++ab)  (&(S[0][0]))[ab] = 0;

    double ik2 = 1/(2*k12);

    //(0,0)
    S[0][0] = sqrt(PI/k12);

    //(0,lb)
    S[0][1] = BP * S[0][0];
    for (int b=1; b<=lb; ++b)
        S[0][b+1] = BP * S[0][b] + b * ik2 * S[0][b-1];

    //(la,lb)
    {
        S[1][0] = AP * S[0][0];
        for (int a=1; a<=la; ++a)
            S[a+1][0] = AP * S[a][0] + a * ik2 * S[a-1][0];
    }

    for (int b=1; b<=lb+1; ++b) {
        S[1][b] = AP * S[0][b] + b * ik2 * S[0][b-1];
        for (int a=1; a<=la; ++a)
            S[a+1][b] = AP * S[a][b] + a * ik2 * S[a-1][b]  + b * ik2 * S[a][b-1];
    }
}

inline void dipole_RR(double (&D)[LMAX+2][LMAX+2], const double (&S)[LMAX+2][LMAX+2], double AP, double BP, double k12, int la, int lb, double c) {

    for (int ab=0; ab<(LMAX+2)*(LMAX+2); ++ab)  (&(D[0][0]))[ab] = 0;

    double ik2 = 1/(2*k12);

    //(0,0)
    D[0][0] = c * S[0][0];

    //(0,lb)
    D[0][1] = BP * D[0][0] + ik2 * S[0][0];
    for (int b=1; b<=lb; ++b) {
        D[0][b+1] = BP * D[0][b] + b * ik2 * D[0][b-1] + ik2 * S[0][b];
    }

    //(la,lb)
    D[1][0] = AP * D[0][0] + ik2 * S[0][0];
    for (int a=1; a<=la; ++a) {
        D[a+1][0] = AP * D[a][0] + a * ik2 * D[a-1][0] + ik2 * S[a][0];
    }

    for (int b=1; b<=lb+1; ++b) {
        D[1][b] = AP * D[0][b] + b * ik2 * D[0][b-1] + ik2 * S[0][b];
        for (int a=1; a<=la; ++a) {
            D[a+1][b] = AP * D[a][b] + a * ik2 * D[a-1][b]  + b * ik2 * D[a][b-1] + ik2 * S[a][b];
        }
    }
}

inline void nabla_RR(double (&D)[LMAX+2][LMAX+2], const double (&S)[LMAX+2][LMAX+2], double AP, double BP, double k12, int la, int lb, double ea, double eb) {

    for (int ab=0; ab<(LMAX+2)*(LMAX+2); ++ab)  (&(D[0][0]))[ab] = 0;

    double ik2 = 1/(2*k12);

    //(0,0)
    D[0][0] = (-2.0 * eb * S[0][1]);

    //(0,lb)
    D[0][1] = BP * D[0][0] + ik2 * 2.0 * ea * S[0][0];
    for (int b=1; b<=lb; ++b) {
        D[0][b+1] = BP * D[0][b] + b * ik2 * D[0][b-1] + ik2 * 2.0 * ea * S[0][b];
    }

    //(la,lb)
    D[1][0] = AP * D[0][0] - ik2 * 2.0 * eb * S[0][0];
    for (int a=1; a<=la; ++a) {
        D[a+1][0] = AP * D[a][0] + a * ik2 * D[a-1][0] - ik2 * 2.0 * eb * S[a][0];
    }

    for (int b=1; b<=lb+1; ++b) {
        D[1][b] = AP * D[0][b] + b * ik2 * D[0][b-1] - ik2 * 2.0 * eb * S[0][b];
        for (int a=1; a<=la; ++a) {
            D[a+1][b] = AP * D[a][b] + a * ik2 * D[a-1][b]  + b * ik2 * D[a][b-1] - ik2 * 2.0 * eb * S[a][b];
        }
    }
}

//not really a RR, just named TRR because its formation is parallel to that of SRR
inline void TRR(double (&T)[LMAX+1][LMAX+1], const double (&S)[LMAX+2][LMAX+2], double k1, double k2, int la, int lb) {

    for (int ab=0; ab<(LMAX+1)*(LMAX+1); ++ab)  (&(T[0][0]))[ab] = 0;

    for (int a=0; a<=la; ++a)
        for (int b=0; b<=lb; ++b)
            T[a][b] = 4*k1*k2*S[a+1][b+1];

    for (int a=0; a<=la; ++a)
        for (int b=1; b<=lb; ++b)
            T[a][b] -= 2*k1*b*S[a+1][b-1];

    for (int a=1; a<=la; ++a)
        for (int b=0; b<=lb; ++b)
            T[a][b] -= 2*k2*a*S[a-1][b+1];

    for (int a=1; a<=la; ++a)
        for (int b=1; b<=lb; ++b)
            T[a][b] +=    a*b*S[a-1][b-1];
}

// basic 1e integrals over the product of two GTO shells
// FIXME: tensors X,Y,Z are ANTIsymmetric, although this is not an issue given that we are just filling the upper right half of the matrix entries
void CalcMonoElectronic(const ShellPair & AB, const rAtom * nuclei, symtensor & S, symtensor & T,
                        symtensor & X, symtensor & Y, symtensor & Z,
                        symtensor & DX, symtensor & DY, symtensor & DZ,
                        tensor & PX, tensor & PY, tensor & PZ,
                        tensor & LX, tensor & LY, tensor & LZ,
                        const std::vector<double>& center)
{
    int La = AB.getLa();
    int Lb = AB.getLb();
    int la = (AB.getLa()==LSP)?1:AB.getLa();
    int lb = (AB.getLb()==LSP)?1:AB.getLb();

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;

    point A = nuclei[AB.ata].c;
    point B = nuclei[AB.atb].c;

    int ta = AB.ata;
    int tb = AB.atb;

    bool samef = ( (ta == tb) && (AB.getFa() == AB.getFb()) );


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;
                    S(ta,tb, ffa,ffb) = 0;
                    T(ta,tb, ffa,ffb) = 0;
                    X(ta,tb, ffa,ffb) = 0; //A.x;
                    Y(ta,tb, ffa,ffb) = 0; //A.y;
                    Z(ta,tb, ffa,ffb) = 0; //A.z;
                }
            }
        }
    }

    // loop from smaller to larger exponential prefactors
    // to avoid unnecessary loss of precision
    for (int b=AB.getKb()-1; b>=0; --b) {
        for (int a=AB.getKa(b)-1; a>=0; --a) {

            double k1, k2, k12, ik2;

            k1  = AB.GP->ka[a];
            k2  = AB.GP->kb[b];
            k12 = k1+k2;
            ik2 = 1/(2*k12);

            point P;
            vector3 Av;
            vector3 Bv;

            P.x = (k1*A.x + k2*B.x)/k12;
            P.y = (k1*A.y + k2*B.y)/k12;
            P.z = (k1*A.z + k2*B.z)/k12;

            // center of origin for nabla operator
            point C;
            C.x = center[0];
            C.y = center[1];
            C.z = center[2];

            Av = P - A;
            Bv = P - B;

            // calcula los tensores de propiedades monoelectronicas
            double Sx[LMAX+2][LMAX+2];
            double Sy[LMAX+2][LMAX+2];
            double Sz[LMAX+2][LMAX+2];

            SRR(Sx, Av.x, Bv.x, k12, la, lb);
            SRR(Sy, Av.y, Bv.y, k12, la, lb);
            SRR(Sz, Av.z, Bv.z, k12, la, lb);

            // dipole 1e integrals
            double Dx[LMAX+2][LMAX+2];
            double Dy[LMAX+2][LMAX+2];
            double Dz[LMAX+2][LMAX+2];
            dipole_RR(Dx, Sx, Av.x, Bv.x, k12, la, lb, P.x - C.x);
            dipole_RR(Dy, Sy, Av.y, Bv.y, k12, la, lb, P.y - C.y);
            dipole_RR(Dz, Sz, Av.z, Bv.z, k12, la, lb, P.z - C.z);

            // nabla 1e integrals
            double Px[LMAX+2][LMAX+2];
            double Py[LMAX+2][LMAX+2];
            double Pz[LMAX+2][LMAX+2];
            nabla_RR(Px, Sx, Av.x, Bv.x, k12, la, lb, k1, k2);
            nabla_RR(Py, Sy, Av.y, Bv.y, k12, la, lb, k1, k2);
            nabla_RR(Pz, Sz, Av.z, Bv.z, k12, la, lb, k1, k2);

            // T
            double Tx[LMAX+1][LMAX+1];
            double Ty[LMAX+1][LMAX+1];
            double Tz[LMAX+1][LMAX+1];

            TRR(Tx, Sx, k1, k2, la, lb);
            TRR(Ty, Sy, k1, k2, la, lb);
            TRR(Tz, Sz, k1, k2, la, lb);



            //double wab = exp(-(AB.GP->kr[a][b]) * AB.R2);
            double wab = AB.getW(b,a);

            double SS[2*LMAX+1][2*LMAX+1];
            double TT[2*LMAX+1][2*LMAX+1];
            double XX[2*LMAX+1][2*LMAX+1];
            double YY[2*LMAX+1][2*LMAX+1];
            double ZZ[2*LMAX+1][2*LMAX+1];

            double DDX[2*LMAX+1][2*LMAX+1];
            double DDY[2*LMAX+1][2*LMAX+1];
            double DDZ[2*LMAX+1][2*LMAX+1];

            double PPX[2*LMAX+1][2*LMAX+1];
            double PPY[2*LMAX+1][2*LMAX+1];
            double PPZ[2*LMAX+1][2*LMAX+1];

            double LLX[2*LMAX+1][2*LMAX+1];
            double LLY[2*LMAX+1][2*LMAX+1];
            double LLZ[2*LMAX+1][2*LMAX+1];

            for (int ma=0; ma<nmS[La]; ++ma) {
                for (int mb=0; mb<nmS[Lb]; ++mb) {
                    double sumS, sumT, sumX, sumY, sumZ;
                    sumS = sumT = sumX = sumY = sumZ = 0;

                    double sumDX, sumDY, sumDZ;
                    sumDX = sumDY = sumDZ = 0;

                    double sumPX, sumPY, sumPZ;
                    sumPX = sumPY = sumPZ = 0;

                    double sumLX, sumLY, sumLZ;
                    sumLX = sumLY = sumLZ = 0;

                    for (int i=0; i<SHList[La][ma].nps; ++i) {
                        for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                            const SHTerm & sha = SHList[La][ma].T[i];
                            const SHTerm & shb = SHList[Lb][mb].T[j];

                            sumS += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                            sumT += sha.cN * shb.cN * (Tx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                       Sx[sha.nx][shb.nx] * Ty[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]  +
                                                       Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Tz[sha.nz][shb.nz]);

                            sumX += sha.cN * shb.cN * (Sx[sha.nx+1][shb.nx] * Sy[sha.ny  ][shb.ny] * Sz[sha.nz  ][shb.nz]);
                            sumY += sha.cN * shb.cN * (Sx[sha.nx  ][shb.nx] * Sy[sha.ny+1][shb.ny] * Sz[sha.nz  ][shb.nz]);
                            sumZ += sha.cN * shb.cN * (Sx[sha.nx  ][shb.nx] * Sy[sha.ny  ][shb.ny] * Sz[sha.nz+1][shb.nz]);

                            sumDX += sha.cN * shb.cN * (Dx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                            sumDY += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Dy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                            sumDZ += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Dz[sha.nz][shb.nz]);

                            sumPX += sha.cN * shb.cN * (Px[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                            sumPY += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Py[sha.ny][shb.ny] * Sz[sha.nz][shb.nz]);
                            sumPZ += sha.cN * shb.cN * (Sx[sha.nx][shb.nx] * Sy[sha.ny][shb.ny] * Pz[sha.nz][shb.nz]);

                            sumLX += sha.cN * shb.cN * Sx[sha.nx][shb.nx] * (Dy[sha.ny][shb.ny] * Pz[sha.nz][shb.nz] - Py[sha.ny][shb.ny] * Dz[sha.nz][shb.nz]);
                            sumLY += sha.cN * shb.cN * Sy[sha.ny][shb.ny] * (Dz[sha.nz][shb.nz] * Px[sha.nx][shb.nx] - Pz[sha.nz][shb.nz] * Dx[sha.nx][shb.nx]);
                            sumLZ += sha.cN * shb.cN * Sz[sha.nz][shb.nz] * (Dx[sha.nx][shb.nx] * Py[sha.ny][shb.ny] - Px[sha.nx][shb.nx] * Dy[sha.ny][shb.ny]);
                        }
                    }

                    SS[ma][mb] =       sumS;
                    TT[ma][mb] = 0.5 * sumT;

                    XX[ma][mb] =       sumX;
                    YY[ma][mb] =       sumY;
                    ZZ[ma][mb] =       sumZ;

                    DDX[ma][mb] =      sumDX;
                    DDY[ma][mb] =      sumDY;
                    DDZ[ma][mb] =      sumDZ;

                    PPX[ma][mb] =      sumPX;
                    PPY[ma][mb] =      sumPY;
                    PPZ[ma][mb] =      sumPZ;

                    LLX[ma][mb] =      sumLX;
                    LLY[ma][mb] =      sumLY;
                    LLZ[ma][mb] =      sumLZ;
                }
            }


            // S | X,Y,Z
            for (int ja=0; ja<Ja; ++ja) {
                for (int jb=0; jb<Jb; ++jb) {

                    double w12 = wab * AB.GP->Na [ja][a] * AB.GP->Nb [jb][b];

                    for (int ma=0; ma<nmS[La]; ++ma) {
                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                            int ffa = AB.getFa() + ja*nmS[La] + ma;
                            int ffb = AB.getFb() + jb*nmS[Lb] + mb;

                            if (samef && (ffb>ffa)) continue;

                            S(ta,tb, ffa,ffb) += w12 * SS[ma][mb];
                            T(ta,tb, ffa,ffb) += w12 * TT[ma][mb];

                            X(ta,tb, ffa,ffb) += w12 * XX[ma][mb];
                            Y(ta,tb, ffa,ffb) += w12 * YY[ma][mb];
                            Z(ta,tb, ffa,ffb) += w12 * ZZ[ma][mb];

                            DX(ta,tb, ffa,ffb) += w12 * DDX[ma][mb];
                            DY(ta,tb, ffa,ffb) += w12 * DDY[ma][mb];
                            DZ(ta,tb, ffa,ffb) += w12 * DDZ[ma][mb];

                            // nabla and angular momentum 1e integrals are anti-symmetric

                            PX(ta,tb, ffa,ffb) += w12 * PPX[ma][mb];
                            PY(ta,tb, ffa,ffb) += w12 * PPY[ma][mb];
                            PZ(ta,tb, ffa,ffb) += w12 * PPZ[ma][mb];

                            PX(tb,ta, ffb,ffa) -= w12 * PPX[ma][mb];
                            PY(tb,ta, ffb,ffa) -= w12 * PPY[ma][mb];
                            PZ(tb,ta, ffb,ffa) -= w12 * PPZ[ma][mb];

                            LX(ta,tb, ffa,ffb) += w12 * LLX[ma][mb];
                            LY(ta,tb, ffa,ffb) += w12 * LLY[ma][mb];
                            LZ(ta,tb, ffa,ffb) += w12 * LLZ[ma][mb];

                            LX(tb,ta, ffb,ffa) -= w12 * LLX[ma][mb];
                            LY(tb,ta, ffb,ffa) -= w12 * LLY[ma][mb];
                            LZ(tb,ta, ffb,ffa) -= w12 * LLZ[ma][mb];
                        }
                    }
                }
            }

        }
    }


    // add the origin vector

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {

                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    if (samef && (ffb>ffa)) continue;

                    X(ta,tb, ffa,ffb) += A.x * S(ta,tb, ffa,ffb);
                    Y(ta,tb, ffa,ffb) += A.y * S(ta,tb, ffa,ffb);
                    Z(ta,tb, ffa,ffb) += A.z * S(ta,tb, ffa,ffb);
                }
            }
        }
    }

/*
    //monoelectronic tensor final adjusts & normalization
    int ta = AB.ata;
    int tb = AB.atb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    double sumS = 0;
                    double sumT = 0;

                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumS += sha.cN * shb.cN * SS[ja][jb][sha.nc][shb.nc];
                            sumT += sha.cN * shb.cN * TT[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    S(ta,tb, ffa,ffb) = sumS;
                    T(ta,tb, ffa,ffb) = 0.5*sumT;
                }
            }

        }
    }
*/
}

// copied from GTO
inline double RadialIntegral(double k, int L) {
    double kn = 1;
    for (int i=0; i<2*L+3; ++i)
        kn *= k;
    return hGamma[L+1] / (2*sqrt(kn));
}

// compute pseudopotential

void CalcAuPP(const ShellPair & AB, const rAtom * nuclei, int nnuclei, symtensor & LPP, symtensor & NLPP) {

    // hardcoded pseudopotential parameters
    // ====================================

    // local part
    static const double rloc =  0.62;
    static const double c1   = 62.08;
    static const double c2   = -1.70;
    static const double nu =  1/(2*rloc*rloc);

    // nonlocal part
    static const int nlc = 2; // number of distinct angular momentum in the NLPP projectors
    static const int Jc  = 3; // number of functions in each projector

    static const double rpd[nlc]         =  {0.89, 0.52};
    static const double hpd[nlc][Jc][Jc] = {{{-6.32,  0.48,  2.69}, { 0.48, -3.58, 0.49}, { 2.69, 0.49, -0.79}},
                                            {{66.54, 32.64, 19.45}, {32.64, 77.16, 0.15}, {19.45, 0.15, -0.21}}};

    static const double kpd[nlc] = {1/(2*rpd[0]*rpd[0]), 1/(2*rpd[1]*rpd[1])}; // gaussian exponents
    static const int    lpd[nlc] = {1,2};                                      // angular momenta of the shells



    // shell pair info and such

    const int La = AB.getLa();
    const int Lb = AB.getLb();

    const int Ltot = La+Lb;

    const int la = (AB.getLa()==LSP)?1:AB.getLa();
    const int lb = (AB.getLb()==LSP)?1:AB.getLb();

    const int Ja = AB.GP->Ja;
    const int Jb = AB.GP->Jb;

	point A = nuclei[AB.ata].c;
	point B = nuclei[AB.atb].c;

    vector3 ABv = A - B;



    // accumulate the local integrals somewhere

    double VL[maxJ][maxJ][MMAX][MMAX];


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    VL[ja][jb][i][j] = 0;
                }
            }
        }
    }


    // COMPUTE 1/R COMPONENT OF LOCAL POTENTIAL
    // ========================================


    RintX VRint;

    switch (Ltot) {
        case  0: VRint = RIntegralX< 0>; break;
        case  1: VRint = RIntegralX< 1>; break;
        case  2: VRint = RIntegralX< 2>; break;
        case  3: VRint = RIntegralX< 3>; break;
        case  4: VRint = RIntegralX< 4>; break;
        case  5: VRint = RIntegralX< 5>; break;
        case  6: VRint = RIntegralX< 6>; break;
        case  7: VRint = RIntegralX< 7>; break;
        case  8: VRint = RIntegralX< 8>; break;
        case  9: VRint = RIntegralX< 9>; break;
        case 10: VRint = RIntegralX<10>; break;
    }


    // loop from smaller to larger exponential prefactors
    // to avoid unnecessary loss of precision
    for (int b=AB.getKb()-1; b>=0; --b) {
        for (int a=AB.getKa(b)-1; a>=0; --a) {

            Triple R;
            {
                for (int px=0; px<=Ltot; ++px)
                    for (int py=0; py<=Ltot-px; ++py)
                        for (int pz=0; pz<=Ltot-px-py; ++pz)
                            R[px][py][pz] = 0;
            }


            double k1, k2, k12, ik12;

            k1  = AB.GP->ka[a];
            k2  = AB.GP->kb[b];

            k12 = k1+k2;
            ik12 = 1/k12;


            point P;

            P.x = (k1*A.x + k2*B.x)*ik12;
            P.y = (k1*A.y + k2*B.y)*ik12;
            P.z = (k1*A.z + k2*B.z)*ik12;


            double Kw = (k12*nu)/(k12+nu);



            TripleX TR;

            double ww = 0; // no damping

            // add all contributions
            for (int i=0; i<nnuclei; ++i) {
                const std::string aname = nuclei[i].rAP->name;

                if (aname[0] != '*') continue; // skip atoms without pseudopotential


                //add the damped point charge contribution
                VRint(TR, Kw, ww, P, nuclei[i].c);

                for (int px=0; px<=Ltot; ++px) {
                    for (int py=0; py<=Ltot-px; ++py) {
                        for (int pz=0; pz<=Ltot-px-py; ++pz) {
                            R[px][py][pz] -= TR[px][py][pz]; // has unit charge
                        }
                    }
                }
            }

            vector3 PA;
            PA = P - A;

            vector3 PB;
            PB = P - B;


            double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12);


            // V
            for (int ma=0; ma<nmC[La]; ++ma) {
                int ax = CartList[La][ma].nx;
                int ay = CartList[La][ma].ny;
                int az = CartList[La][ma].nz;

                for (int mb=0; mb<nmC[Lb]; ++mb) {
                    int bx = CartList[Lb][mb].nx;
                    int by = CartList[Lb][mb].ny;
                    int bz = CartList[Lb][mb].nz;

                    //very inefficient, but still a small fraction of the computation O(1)/O(N)
                    double r2e = R2E(ax,ay,az,PA, bx,by,bz,PB,  0.5*ik12, 0,0,0, R);

                    for (int ja=0; ja<Ja; ++ja) {
                        for (int jb=0; jb<Jb; ++jb) {
                            double w12 = wab; //AB.W[b][a];

                            if (La==LSP && ma>0) w12 *= AB.GP->Nap[ja][a];
                            else                 w12 *= AB.GP->Na [ja][a];

                            if (Lb==LSP && mb>0) w12 *= AB.GP->Nbp[jb][b];
                            else                 w12 *= AB.GP->Nb [jb][b];

                            VL[ja][jb][ma][mb] += w12 * r2e;
                        }
                    }
                }
            }

        }
    }



    // COMPUTE EXP COMPONENT OF LOCAL POTENTIAL
    // ========================================

    const double inu = 1./nu;
    const double inu32 = sqrt(inu)*inu;

    const double cc1 = c1 * inu32;
    const double cc2 = c2 * inu32 * inu * 0.5;


    Rint PPint;

    switch (Ltot) {
        case  0: PPint = PPIntegral< 0>; break;
        case  1: PPint = PPIntegral< 1>; break;
        case  2: PPint = PPIntegral< 2>; break;
        case  3: PPint = PPIntegral< 3>; break;
        case  4: PPint = PPIntegral< 4>; break;
        case  5: PPint = PPIntegral< 5>; break;
        case  6: PPint = PPIntegral< 6>; break;
        case  7: PPint = PPIntegral< 7>; break;
        case  8: PPint = PPIntegral< 8>; break;
        case  9: PPint = PPIntegral< 9>; break;
        case 10: PPint = PPIntegral<10>; break;
    }

    // loop from smaller to larger exponential prefactors
    // to avoid unnecessary loss of precision
    for (int b=AB.getKb()-1; b>=0; --b) {
        for (int a=AB.getKa(b)-1; a>=0; --a) {


            double k1, k2, k12, ik12;

            k1  = AB.GP->ka[a];
            k2  = AB.GP->kb[b];

            k12 = k1+k2;
            ik12 = 1/k12;


            point P;

            P.x = (k1*A.x + k2*B.x)*ik12;
            P.y = (k1*A.y + k2*B.y)*ik12;
            P.z = (k1*A.z + k2*B.z)*ik12;


            // exponential constants and modified weights
            const double ikw = 1./(k12+nu);
            const double Kw = k12*nu*ikw;

            // transformed weights
            const double w0 = cc1 + 3*nu*ikw * cc2;
            const double w2 = k12*ikw * cc2;
            const double wc = w0/w2;


            Triple TR;
            {
                for (int px=0; px<=Ltot; ++px)
                    for (int py=0; py<=Ltot-px; ++py)
                        for (int pz=0; pz<=Ltot-px-py; ++pz)
                            TR[px][py][pz] = 0;
            }


            // compute Au local pseudopotential
            for (int i=0; i<nnuclei; ++i) {
                const std::string aname = nuclei[i].rAP->name;

                if (aname[0] != '*') continue; // skip atoms without pseudopotential

                //add the damped point charge contribution
                PPint(TR, Kw, wc,  P, nuclei[i].c);
            }

            // recover the weights
            {
                for (int px=0; px<=Ltot; ++px)
                    for (int py=0; py<=Ltot-px; ++py)
                        for (int pz=0; pz<=Ltot-px-py; ++pz)
                            TR[px][py][pz] *= w2;
            }



            vector3 PA;
            PA = P - A;

            vector3 PB;
            PB = P - B;


            double wab = AB.getW(b,a) * (PI*ik12)*sqrt(PI*ik12); // accounts for the lack of gaussian charge normalization


            // V
            for (int ma=0; ma<nmC[La]; ++ma) {
                int ax = CartList[La][ma].nx;
                int ay = CartList[La][ma].ny;
                int az = CartList[La][ma].nz;

                for (int mb=0; mb<nmC[Lb]; ++mb) {
                    int bx = CartList[Lb][mb].nx;
                    int by = CartList[Lb][mb].ny;
                    int bz = CartList[Lb][mb].nz;

                    //very inefficient, but still a small fraction of the computation O(1)/O(N)
                    double r2e = R2E(ax,ay,az,PA, bx,by,bz,PB,  0.5*ik12, 0,0,0, TR);

                    for (int ja=0; ja<Ja; ++ja) {
                        for (int jb=0; jb<Jb; ++jb) {
                            double w12 = wab; //AB.W[b][a];

                            if (La==LSP && ma>0) w12 *= AB.GP->Nap[ja][a];
                            else                 w12 *= AB.GP->Na [ja][a];

                            if (Lb==LSP && mb>0) w12 *= AB.GP->Nbp[jb][b];
                            else                 w12 *= AB.GP->Nb [jb][b];

                            VL[ja][jb][ma][mb] += w12 * r2e;
                        }
                    }
                }
            }

        }
    }




    // contract to spherical

    int ta = AB.ata;
    int tb = AB.atb;
    bool samef = ( (ta == tb) && (AB.getFa() == AB.getFb()) );

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = AB.getFa() + ja*nmS[La] + a;
                    int ffb = AB.getFb() + jb*nmS[Lb] + b;

                    double sumP = 0;
                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumP += sha.cN * shb.cN * VL[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    LPP(ta,tb, ffa,ffb) = sumP;
                }
            }
        }
    }



    // COMPUTE NONLOCAL POTENTIAL
    // ==========================


    // normalization constants for the NLPP shells

    double Nc[nlc][Jc]; // normalization coefficients

    {
        for (int ll=0; ll<nlc; ++ll) {
            for (int jc=0; jc<Jc; ++jc) {
                int Lc = lpd[ll] + 2*jc; // total radial power

                Nc[ll][jc] = 1./sqrt(RadialIntegral(2*kpd[ll],Lc));
            }
        }
    }


    double VNL[Ja][Jb][MMAX][MMAX];


    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {
            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    VNL[ja][jb][i][j] = 0;
                }
            }
        }
    }


    // loop over all centers with pseudopotentials
    for (int i=0; i<nnuclei; ++i) {
        const std::string aname = nuclei[i].rAP->name;

        if (aname[0] != '*') continue; // skip atoms without pseudopotential

        double SA[nlc][Jc][5][Ja][2*La+1];
        double SB[nlc][Jc][5][Jb][2*Lb+1];

        for (int l=0; l<nlc; ++l) {
            for (int jp=0; jp<Jc; ++jp) {
                for (int mp=0; mp<5; ++mp) {

                    for (int ja=0; ja<Ja; ++ja) {
                        for (int i=0; i<nmS[La]; ++i) {
                            SA[l][jp][mp][ja][i] = 0;
                            SB[l][jp][mp][ja][i] = 0;
                        }
                    }
                }
            }
        }

        const point C = nuclei[i].c;

        vector3 AC = A - C;
        vector3 BC = B - C;

        const int Ka = AB.GP->Ka;
        const int Kb = AB.GP->Kb;


        for (int a=0; a<Ka; ++a) {

            // PP radial projectors of different L have different exponentials, so must be contracted separately

            for (int ll=0; ll<nlc; ++ll) {

                const int Lc = lpd[ll];

                double k1, kc, k1c, ik1c, ikc;

                k1  = AB.GP->ka[a];
                kc  = kpd[ll];

                k1c = k1+kc;
                ik1c = 1/k1c;
                ikc = 0.5*ik1c; // for later


                point P;

                P.x = (k1*A.x + kc*C.x)*ik1c;
                P.y = (k1*A.y + kc*C.y)*ik1c;
                P.z = (k1*A.z + kc*C.z)*ik1c;

                vector3 Av;
                vector3 Cv;

                Av = P - A;
                Cv = P - C;

                // compute overlap
                const int mlc = 2*(Jc-1) + Lc; // the different jc's correspond to the same angular momentum, multiplied times r^(2*Jc)

                double Sx[LMAX+2][LMAX+2];
                double Sy[LMAX+2][LMAX+2];
                double Sz[LMAX+2][LMAX+2];

                SRR(Sx, Av.x, Cv.x, k1c, la, mlc);
                SRR(Sy, Av.y, Cv.y, k1c, la, mlc);
                SRR(Sz, Av.z, Cv.z, k1c, la, mlc);


                double SS[2*La+1][Jc][2*Lc+1]; // Jc==3


                for (int ma=0; ma<nmS[La]; ++ma) {
                    for (int mc=0; mc<nmS[Lc]; ++mc) {

                        // for each Jc
                        double sumS0, sumS2, sumS4;
                        sumS0 = sumS2 = sumS4 = 0;

                        for (int i=0; i<SHList[La][ma].nps; ++i) {
                            for (int j=0; j<SHList[Lc][mc].nps; ++j) {
                                const SHTerm & sha = SHList[La][ma].T[i];
                                const SHTerm & shc = SHList[Lc][mc].T[j];



                                sumS0 += sha.cN * shc.cN * (Sx[sha.nx][shc.nx] * Sy[sha.ny][shc.ny] * Sz[sha.nz][shc.nz]);

                                sumS2 += sha.cN * shc.cN * (Sx[sha.nx][shc.nx+2] * Sy[sha.ny][shc.ny] * Sz[sha.nz][shc.nz] +
                                                            Sx[sha.nx][shc.nx] * Sy[sha.ny][shc.ny+2] * Sz[sha.nz][shc.nz] +
                                                            Sx[sha.nx][shc.nx] * Sy[sha.ny][shc.ny] * Sz[sha.nz][shc.nz+2] );

                                sumS4 += sha.cN * shc.cN * (Sx[sha.nx][shc.nx+4] * Sy[sha.ny][shc.ny] * Sz[sha.nz][shc.nz] +
                                                            Sx[sha.nx][shc.nx] * Sy[sha.ny][shc.ny+4] * Sz[sha.nz][shc.nz] +
                                                            Sx[sha.nx][shc.nx] * Sy[sha.ny][shc.ny] * Sz[sha.nz][shc.nz+4] +
                                                        2 * Sx[sha.nx][shc.nx+2] * Sy[sha.ny][shc.ny+2] * Sz[sha.nz][shc.nz] +
                                                        2 * Sx[sha.nx][shc.nx+2] * Sy[sha.ny][shc.ny] * Sz[sha.nz][shc.nz+2] +
                                                        2 * Sx[sha.nx][shc.nx] * Sy[sha.ny][shc.ny+2] * Sz[sha.nz][shc.nz+2]
                                                            );

                            }
                        }

                        // sum
                        SS[ma][0][mc] = sumS0;
                        SS[ma][1][mc] = sumS2;
                        SS[ma][2][mc] = sumS4;
                    }
                }


                // store the bra shell contraction with all PP projectors

                double wac = exp(-k1*kc*(AC*AC)*ik1c); // AB.getW(b,a);

                for (int ja=0; ja<Ja; ++ja) {
                    for (int jc=0; jc<Jc; ++jc) {

                        double w12 = wac * AB.GP->Na [ja][a] * Nc[ll][jc]; // here goes the normalization coefficient for the PP

                        for (int ma=0; ma<nmS[La]; ++ma) {
                            for (int mc=0; mc<nmS[Lc]; ++mc) {

                                SA[ll][jc][mc] [ja][ma] += w12 * SS[ma][jc][mc];
                            }
                        }
                    }
                }

            }

        }

        // now the same for the other index

        for (int b=0; b<Kb; ++b) {

            for (int ll=0; ll<nlc; ++ll) {

                const int Lc = lpd[ll];

                double k1, kc, k1c, ik1c, ikc;

                k1  = AB.GP->kb[b];
                kc  = kpd[ll];

                k1c = k1+kc;
                ik1c = 1/k1c;
                ikc = 0.5*ik1c; // for later


                point P;

                P.x = (k1*B.x + kc*C.x)*ik1c;
                P.y = (k1*B.y + kc*C.y)*ik1c;
                P.z = (k1*B.z + kc*C.z)*ik1c;

                vector3 Bv;
                vector3 Cv;

                Bv = P - B;
                Cv = P - C;


                // compute overlap
                const int mlc = 2*(Jc-1) + Lc; // the different jc's correspond to the same angular momentum, multiplied times r^(2*jc)

                double Sx[LMAX+2][LMAX+2];
                double Sy[LMAX+2][LMAX+2];
                double Sz[LMAX+2][LMAX+2];

                SRR(Sx, Bv.x, Cv.x, k1c, lb, mlc);
                SRR(Sy, Bv.y, Cv.y, k1c, lb, mlc);
                SRR(Sz, Bv.z, Cv.z, k1c, lb, mlc);


                double SS[2*Lb+1][Jc][2*Lc+1]; // Jc==3


                for (int mb=0; mb<nmS[Lb]; ++mb) {
                    for (int mc=0; mc<nmS[Lc]; ++mc) {

                        // for each Jc
                        double sumS0, sumS2, sumS4;
                        sumS0 = sumS2 = sumS4 = 0;

                        for (int i=0; i<SHList[Lb][mb].nps; ++i) {
                            for (int j=0; j<SHList[Lc][mc].nps; ++j) {
                                const SHTerm & shb = SHList[Lb][mb].T[i];
                                const SHTerm & shc = SHList[Lc][mc].T[j];



                                sumS0 += shb.cN * shc.cN * (Sx[shb.nx][shc.nx] * Sy[shb.ny][shc.ny] * Sz[shb.nz][shc.nz]);

                                sumS2 += shb.cN * shc.cN * (Sx[shb.nx][shc.nx+2] * Sy[shb.ny][shc.ny] * Sz[shb.nz][shc.nz] +
                                                            Sx[shb.nx][shc.nx] * Sy[shb.ny][shc.ny+2] * Sz[shb.nz][shc.nz] +
                                                            Sx[shb.nx][shc.nx] * Sy[shb.ny][shc.ny] * Sz[shb.nz][shc.nz+2] );

                                sumS4 += shb.cN * shc.cN * (Sx[shb.nx][shc.nx+4] * Sy[shb.ny][shc.ny] * Sz[shb.nz][shc.nz] +
                                                            Sx[shb.nx][shc.nx] * Sy[shb.ny][shc.ny+4] * Sz[shb.nz][shc.nz] +
                                                            Sx[shb.nx][shc.nx] * Sy[shb.ny][shc.ny] * Sz[shb.nz][shc.nz+4] +
                                                        2 * Sx[shb.nx][shc.nx+2] * Sy[shb.ny][shc.ny+2] * Sz[shb.nz][shc.nz] +
                                                        2 * Sx[shb.nx][shc.nx+2] * Sy[shb.ny][shc.ny] * Sz[shb.nz][shc.nz+2] +
                                                        2 * Sx[shb.nx][shc.nx] * Sy[shb.ny][shc.ny+2] * Sz[shb.nz][shc.nz+2]
                                                            );

                            }
                        }

                        // sum
                        SS[mb][0][mc] = sumS0;
                        SS[mb][1][mc] = sumS2;
                        SS[mb][2][mc] = sumS4;
                    }
                }


                // store the bra shell contraction with all PP projectors

                //double wab = exp(-(AB.GP->kr[a][b]) * AB.R2);
                double wbc = exp(-k1*kc*(BC*BC)*ik1c); // AB.getW(b,a);

                for (int jb=0; jb<Jb; ++jb) {
                    for (int jc=0; jc<Jc; ++jc) {

                        double w12 = wbc * AB.GP->Nb [jb][b] * Nc[ll][jc]; // here goes the normalization coefficient for the PP

                        for (int mb=0; mb<nmS[Lb]; ++mb) {
                            for (int mc=0; mc<nmS[Lc]; ++mc) {

                                SB[ll][jc][mc] [jb][mb] += w12 * SS[mb][jc][mc];
                            }
                        }
                    }
                }

            }

        }




        // make the inner product

        for (int ja=0; ja<Ja; ++ja) {
            for (int jb=0; jb<Jb; ++jb) {

                for (int ma=0; ma<nmS[La]; ++ma) {
                    for (int mb=0; mb<nmS[Lb]; ++mb) {

                        // compute the aggregate nonlocal PP integral

                        double nlpp = 0;

                        for (int ll=0; ll<nlc; ++ll) {
                            const int Lc = lpd[ll];

                            for (int jp=0; jp<Jc; ++jp) {
                                for (int jq=0; jq<Jc; ++jq) {

                                    // contract all elements of the L shell
                                    double sab = 0;

                                    for (int mc=0; mc<2*Lc+1; ++mc) {
                                        sab += SA[ll][jp][mc][ja][ma] * SB[ll][jq][mc][jb][mb];
                                    }

                                    // weight the two functions
                                    nlpp += hpd[ll][jp][jq] * sab;
                                }
                            }
                        }

                        // add the component

                        VNL[ja][jb][ma][mb] += nlpp;
                    }
                }
            }
        }

    }

    // store the damn thing in the tensor

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int ma=0; ma<nmS[La]; ++ma) {
                for (int mb=0; mb<nmS[Lb]; ++mb) {

                    int ffa = AB.getFa() + ja*nmS[La] + ma;
                    int ffb = AB.getFb() + jb*nmS[Lb] + mb;

                    NLPP(ta,tb, ffa,ffb) = VNL[ja][jb][ma][mb];
                }
            }
        }
    }


}





/* ************************************************************************************************** */



// =================================
// BED integrals for Xray absorption
// =================================


#include <complex>

void CalcXRayProperty(const ShellPair & AB, const rAtom * nuclei, symtensor * RR, symtensor * II, vector3 * kk, vector3 * nn, int N) {


    int La = AB.getLa();
    int Lb = AB.getLb();

    int Ja = AB.GP->Ja;
    int Jb = AB.GP->Jb;


    const point A = nuclei[AB.ata].c;
    const point B = nuclei[AB.atb].c;
    const vector3 ABv = B-A;

    const int ta = AB.ata;
    const int tb = AB.atb;

    const bool samef = ( (ta == tb) && (AB.getFa() == AB.getFb()) );

    // zero all contributions
    for (int n=0; n<N; ++n) {
        for (int ja=0; ja<Ja; ++ja) {
            for (int jb=0; jb<Jb; ++jb) {
                for (int a=0; a<nmS[La]; ++a) {
                    for (int b=0; b<nmS[Lb]; ++b) {
                        int ffa = AB.getFa() + ja*nmS[La] + a;
                        int ffb = AB.getFb() + jb*nmS[Lb] + b;
                        RR[n](ta,tb, ffa,ffb) = 0;
                        II[n](ta,tb, ffa,ffb) = 0;
                    }
                }
            }
        }
    }


    const std::complex<double> iunit(0,1);

    for (int b=AB.getKb()-1; b>=0; --b) {
        for (int a=AB.getKa(b)-1; a>=0; --a) {

            double k1, k2, k12, ik2;

            k1  = AB.GP->ka[a];
            k2  = AB.GP->kb[b];
            k12 = k1+k2;
            ik2 = 1/(2*k12);

            point P;
            P.x = (k1*A.x + k2*B.x)/k12;
            P.y = (k1*A.y + k2*B.y)/k12;
            P.z = (k1*A.z + k2*B.z)/k12;


            //double wab = exp(-(AB.GP->kr[a][b]) * AB.R2);
            const double KP1 = sqrt(PI/k12); // 1D integral
            const double wab = (KP1*KP1*KP1) * AB.getW(b,a)  * pow(2*k1,-La) * pow(2*k2,-Lb); // base integral * exponential decay * spherical harmonic normalization

            const double K12 = (k1*k2)/(k1+k2);

            // loop over all points
            for (int n=0; n<N; ++n) {

                std::complex<double> Hxx[LMAX+2][LMAX+2];
                std::complex<double> Hyy[LMAX+2][LMAX+2];
                std::complex<double> Hzz[LMAX+2][LMAX+2];

                Hxx[0][0] = exp(kk[n].x*P.x*iunit);
                Hyy[0][0] = exp(kk[n].y*P.y*iunit);
                Hzz[0][0] = exp(kk[n].z*P.z*iunit);


                for (int lt=0; lt<=La+Lb; ++lt) {
                    for (int b=0; b<=lt; ++b) {
                        int a = lt-b;
                        // complex multiplication (there's probably a way of factoring the complex exponential out and simplifying the damn thing)
                        // do it twice; why not; use simplified RR in the future
                        Hxx[a+1][b] = (2*K12*ABv.x + iunit*(kk[n].x*k1/k12))* Hxx[a][b]
                                    - 2*K12*double(a)*Hxx[a-1][b] + 2*K12*double(b)*Hxx[a][b-1];
                        Hxx[a][b+1] = (-2*K12*ABv.x + iunit*(kk[n].x*k2/k12))* Hxx[a][b]
                                    + 2*K12*double(a)*Hxx[a-1][b] - 2*K12*double(b)*Hxx[a][b-1];

                        // YYY
                        Hyy[a+1][b] = (2*K12*ABv.y + iunit*(kk[n].y*k1/k12))* Hyy[a][b]
                                    - 2*K12*double(a)*Hyy[a-1][b] + 2*K12*double(b)*Hyy[a][b-1];
                        Hyy[a][b+1] = (-2*K12*ABv.y + iunit*(kk[n].y*k2/k12))* Hyy[a][b]
                                    + 2*K12*double(a)*Hyy[a-1][b] - 2*K12*double(b)*Hyy[a][b-1];

                        // ZZZ
                        Hzz[a+1][b] = (2*K12*ABv.z + iunit*(kk[n].z*k1/k12))* Hzz[a][b]
                                    - 2*K12*double(a)*Hzz[a-1][b] + 2*K12*double(b)*Hzz[a][b-1];
                        Hzz[a][b+1] = (-2*K12*ABv.z + iunit*(kk[n].z*k2/k12))* Hzz[a][b]
                                    + 2*K12*double(a)*Hzz[a-1][b] - 2*K12*double(b)*Hzz[a][b-1];
                    }
                }


                // generate integral over primitives

                double RRR[2*LMAX+1][2*LMAX+1];
                double III[2*LMAX+1][2*LMAX+1];

                const double www = exp( - (kk[n]*kk[n]) / (4*k12)) * wab;


                for (int ma=0; ma<nmS[La]; ++ma) {
                    for (int mb=0; mb<nmS[Lb]; ++mb) {

                        std::complex<double> sumN = 0;

                        for (int i=0; i<SHList[La][ma].nps; ++i) {
                            for (int j=0; j<SHList[Lb][mb].nps; ++j) {
                                const SHTerm & sha = SHList[La][ma].T[i];
                                const SHTerm & shb = SHList[Lb][mb].T[j];

                                // the derivatives are multiplied times the polarization vector;
                                // the minus sign comes from the fact that Dx = -Dbx in the formula
                                sumN -= sha.cN * shb.cN * ( nn[n].x * (Hxx[sha.nx][shb.nx+1] * Hyy[sha.ny][shb.ny  ] * Hzz[sha.nz][shb.nz  ]) +
                                                            nn[n].y * (Hxx[sha.nx][shb.nx  ] * Hyy[sha.ny][shb.ny+1] * Hzz[sha.nz][shb.nz  ]) +
                                                            nn[n].z * (Hxx[sha.nx][shb.nx  ] * Hyy[sha.ny][shb.ny  ] * Hzz[sha.nz][shb.nz+1]) );
                            }
                        }

                        //
                        RRR[ma][mb] = www*sumN.real();
                        III[ma][mb] = www*sumN.imag();
                    }
                }

                // just add the contribution to the final vector
                for (int ja=0; ja<Ja; ++ja) {
                    for (int jb=0; jb<Jb; ++jb) {

                        double w12 = AB.GP->Na [ja][a] * AB.GP->Nb [jb][b];

                        for (int ma=0; ma<nmS[La]; ++ma) {
                            for (int mb=0; mb<nmS[Lb]; ++mb) {
                                int ffa = AB.getFa() + ja*nmS[La] + ma;
                                int ffb = AB.getFb() + jb*nmS[Lb] + mb;

                                if (samef && (ffb>ffa)) continue;

                                RR[n](ta,tb, ffa,ffb) += w12 * RRR[ma][mb];
                                II[n](ta,tb, ffa,ffb) += w12 * III[ma][mb];
                            }
                        }
                    }
                }

            } // loop over operators

        }
    }


}





