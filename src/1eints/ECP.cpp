#include <iostream>
#include <cmath>
#include <set>

#include "ECP.hpp"
#include "math/angular.hpp"
#include "math/polynomial.hpp"
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"

using namespace std;
using namespace LibAngular;

class Monomial {

};

class Polynomial {

};


double Ylm[7][7][7];
double Yprod[7][7][7][13]; //m l1 l2

void InitY() {

    {
        for (int i=0; i<6*6*6;++i) ((double*)Ylm)[i] = 0;

        Ylm[0][0][0] =  0.28209479177387814;

        Ylm[1][0][1] =  0.48860251190291992;
        Ylm[1][1][0] = -0.34549414947133548;

        Ylm[2][0][0] = -0.31539156525252001;
        Ylm[2][0][2] =  0.94617469575756002;
        Ylm[2][1][1] = -0.77254840404637916;
        Ylm[2][2][0] =  0.38627420202318958;

        Ylm[3][0][1] = -1.1195289977703462;
        Ylm[3][0][3] =  1.8658816629505770;
        Ylm[3][1][0] =  0.32318018411415065;
        Ylm[3][1][2] = -1.6159009205707533;
        Ylm[3][2][1] =  1.0219854764332824;
        Ylm[3][3][0] = -0.41722382363278409;


        Ylm[4][0][0] =  0.31735664074561291;
        Ylm[4][0][2] = -3.1735664074561291 ;
        Ylm[4][0][4] =  3.7024941420321506;
        Ylm[4][1][1] =  1.4192620436363400;
        Ylm[4][1][3] = -3.3116114351514601;
        Ylm[4][2][0] = -0.33452327177864458 ;
        Ylm[4][2][2] =  2.3416629024505121;
        Ylm[4][3][1] = -1.2516714708983523;
        Ylm[4][4][0] =  0.44253269244498263;


        Ylm[5][0][1] =  1.7542548368013539;
        Ylm[5][0][3] = -8.1865225717396518;
        Ylm[5][0][5] =  7.3678703145656866;
        Ylm[5][1][0] = -0.32028164857621513;
        Ylm[5][1][2] =  4.4839430800670118;
        Ylm[5][1][4] = -6.7259146201005177;
        Ylm[5][2][1] = -1.6947711832608993;
        Ylm[5][2][3] =  5.0843135497826978;
        Ylm[5][3][0] =  0.34594371914684021;
        Ylm[5][3][2] = -3.1134934723215619;
        Ylm[5][4][1] =  1.4677148983057512;
        Ylm[5][4][0] = -0.46413220344085816;

        Ylm[6][0][0] =  -0.31784601133814213;
        Ylm[6][0][2] =   6.6747662381009847;
        Ylm[6][0][4] = -20.024298714302954;
        Ylm[6][0][6] =  14.684485723822166;
        Ylm[6][1][1] =  -2.0598775815057039;
        Ylm[6][1][3] =  12.359265489034223;
        Ylm[6][1][5] = -13.595192037937646;
        Ylm[6][2][0] =   0.32569524293385787;
        Ylm[6][2][2] =  -5.8625143728094416;
        Ylm[6][2][4] =  10.747943016817310;
        Ylm[6][3][1] =   1.9541714576031472;
        Ylm[6][3][3] =  -7.1652953445448731;
        Ylm[6][4][0] =  -0.35678126285399803;
        Ylm[6][4][2] =   3.9245938913939783;
        Ylm[6][5][1] =  -1.6734524581000979;
        Ylm[6][6][0] =   0.48308411358006623;
    }

    // expand a product of spherical harmonics in powers of t=Cos[phi]

    for (int m=0; m<=6; ++m)  {
        for (int l1=m; l1<=6; ++l1) {
            for (int l2=m; l2<=6; ++l2) {

                for (int k=0; k<=12;++k) Yprod[m][l1][l2][k] = 0;

                // (1-t^2)^m Yl1m(t) Yl2m(t)
                for(int i=l1-m; i>=0; i-=2) {
                    for(int j=l2-m; j>=0; j-=2) {
                        Yprod[m][l1][l2][i+j] += Ylm[l1][m][i] * Ylm[l2][m][j];
                    }
                }

                int tm = l1+l2-2*m;

                for (int k=0; k<m; ++k) {
                    for(int i=tm+2*k; i>=0; i-=2) {
                        Yprod[m][l1][l2][i+2] -=    Yprod[m][l1][l2][i];
                    }
                }

            }
        }
    }


}





//const double PI = 3.14159265358979323846264338327950;

// exp(-z) z 2F2(1,1; 3/2,2; z)
// pre: z>0
double F22(double z) {

    //expansion at infinity
    //formally divergent !!!
    if (z>32.) {
        const double SQPI = 0.5*sqrt(PI);
        double iz = 1./z;

        double acc = 1;
        double sum = 0;

        for (int i=0;i<32; ++i) {
            sum += acc;
            acc *= (i+0.5) * iz;
        }

        return SQPI*sum*sqrt(iz); //13~14 digits of
    }
    //tabulate function
    //but first, just use Taylor!!!
    else {
        double sum = 1;
        double acc = 1;

        double sum2 = 0;

        for (int i=1; sum2!=sum; ++i) {
            sum2 = sum;
            acc *= 2*z/double(2*i+1);
            sum += acc/double(i+1);
        }

        return exp(-z)*z*sum;
    }
}


double DawsonF(double z) {
    //expansion at infinity
    if (z*z>32.) { //~14 significant figures
        double iz = 1./z;
        double iz2 = iz*iz;

        double acc = iz;
        double sum = 0;

        for (int i=0;i<32; ++i) {
            sum += acc;
            //cout << i << " " << acc << endl;
            acc *= (i+0.5) * iz2;
        }

        return 0.5*sum; //almost machine precision
    }
    //Taylor!!!
    else {
        double sum = z;
        double acc = z;

        double sum2 = 0;

        for (int i=1; sum2!=sum; ++i) {
            sum2 = sum;
            acc *= (z*z)/double(i);
            sum += acc/double(2*i+1);
        }

        return exp(-z*z)*sum;
    }
}

double erfi (double z) {
    static const double ISQRTPI = 2/sqrt(PI);

    return ISQRTPI * exp(z*z) * DawsonF(z);
}


double IGamma(double k, double P, int n) {
    if (n==0) return sqrt(PI/k);
    if (n==1) return P*sqrt(PI/k);

    return 0.5*(n-1)*IGamma(k,P,n-2)/k + P*IGamma(k,P,n-1);
}

double F0p(double k, double P) {
    return sqrt(PI/k);
}

double F1p(double k, double P, double Q) {

    double kP2 = k*P*P;
    double kQ2 = k*Q*Q;

    //double ret = 2* (kP2 * F22(kP2) - exp(-kP2+kQ2) * kQ2 * F22(kQ2) );
    double ret = 2* (F22(kP2) - exp(-kP2+kQ2) * F22(kQ2) );

    return ret;
}

double F0m(double k, double P) {
    return sqrt(PI/k)* erf(sqrt(k)*P);
}

//sqrt(PI)/2 DawsonF[sqrt(k)*P]
double F1m(double k, double P) {
    return 2*sqrt(PI)* DawsonF(sqrt(k)*P);
}


double L1m(double k, double P) {
    return 4*k*P;
}


// pre: A>0 && B>0
double RadialIntegral (int ns, int nt,  int nr, double a, double b, double g, double A, double B) {

    //init
    double ppp[32];
    double ppq[32];
    double pmp[32];
    double pmq[32];

    double Ps[8];
    double Pt[8];


    double * PpP = ppp + 16; //[16];
    double * PpQ = ppq + 16; //[16];

    double * PmP = pmp + 16; //[16];
    double * PmQ = pmq + 16; //[16];


    for (int i=0; i<32; ++i) ppp[i] = 0;
    for (int i=0; i<32; ++i) ppq[i] = 0;
    for (int i=0; i<32; ++i) pmp[i] = 0;
    for (int i=0; i<32; ++i) pmq[i] = 0;

    double LpP[16];
    double LpQ[16];

    double LmP[16];
    double LmQ[16];

    for (int i=0; i<=15; ++i) LpP[i] = 0;
    for (int i=0; i<=15; ++i) LpQ[i] = 0;
    for (int i=0; i<=15; ++i) LmP[i] = 0;
    for (int i=0; i<=15; ++i) LmQ[i] = 0;


    {
        double iAa = 1/(2*a*A);
        Ps[0] = 0;
        Ps[1] = iAa;

        double iAn = iAa;

        for (int i=1; i<=ns; ++i) {
            iAn *= -iAa * (1+ns-i);
            Ps[i+1] = iAn;
        }
    }

    {
        double iBb = 1/(2*b*B);
        Pt[0] = 0;
        Pt[1] = iBb;

        double iBn = iBb;

        for (int i=1; i<=nt; ++i) {
            iBn *= -iBb * (1+nt-i);
            Pt[i+1] = iBn;
        }
    }


    double theta = a+b+g;

    double P = (a*A+b*B)/theta;
    double Q = (a*A-b*B)/theta;

    double Kp = exp( -(a*g*A*A + b*g*B*B + a*b*(A-B)*(A-B))/theta  );
    double Kq = exp( -(a*g*A*A + b*g*B*B + a*b*(A+B)*(A+B))/theta  );

    //make the final polynomials

    for (int i=ns+1; i>=0; i-=2) { // -
        for (int j=nt+1; j>=0; j-=2) { // -
            PpP[-i-j+nr] += Ps[i]*Pt[j];
            PpQ[-i-j+nr] -= Ps[i]*Pt[j];
        }
    }

    for (int i=ns; i>=0; i-=2) { // +
        for (int j=nt; j>=0; j-=2) { // +
            PpP[-i-j+nr] += Ps[i]*Pt[j];
            PpQ[-i-j+nr] += Ps[i]*Pt[j];
        }
    }


    for (int i=ns+1; i>=0; i-=2) { // -
        for (int j=nt; j>=0; j-=2) { // +
            PmP[-i-j+nr] += Ps[i]*Pt[j];
            PmQ[-i-j+nr] += Ps[i]*Pt[j];
        }
    }

    for (int i=ns; i>=0; i-=2) { // +
        for (int j=nt+1; j>=0; j-=2) { // -
            PmP[-i-j+nr] += Ps[i]*Pt[j];
            PmQ[-i-j+nr] -= Ps[i]*Pt[j];
        }
    }


    int nst=ns+nt+2-nr;

    for (int i=0; i<32; ++i) ppp[i] *= Kp;
    for (int i=0; i<32; ++i) pmp[i] *= Kp;
    for (int i=0; i<32; ++i) ppq[i] *= Kq;
    for (int i=0; i<32; ++i) pmq[i] *= Kq;


    for (int n=nst; n>1; --n) {
        double in1 = 1./double(n-1);
        {
            LpP[n-1] +=  in1             * PpP[-n];

            PmP[-n+1] +=  in1*(2*theta)*P * PpP[-n];
            PpP[-n+2] -=  in1*(2*theta)   * PpP[-n];
            PpP[-n]    =   0;

            LpQ[n-1] +=  in1             * PpQ[-n];

            PmQ[-n+1] +=  in1*(2*theta)*Q * PpQ[-n];
            PpQ[-n+2] -=  in1*(2*theta)   * PpQ[-n];
            PpQ[-n]    =   0;

            LmP[n-1] +=  in1             * PmP[n];

            PpP[-n+1] +=  in1*(2*theta)*P * PmP[-n];
            PmP[-n+2] -=  in1*(2*theta)   * PmP[-n];
            PmP[-n]    =   0;

            LmQ[n-1] +=  in1             * PmQ[n];

            PpQ[-n+1] +=  in1*(2*theta)*Q * PmQ[-n];
            PmQ[-n+2] -=  in1*(2*theta)   * PmQ[-n];
            PmQ[-n]    =   0;
        }

    }




   {

        double RI = PmP[-1]*F1m(theta, P) +  PmQ[-1]*F1m(theta, Q);

        for (int n=0; n<=nr-2; n+=2) {
            RI += PpP[n]   * IGamma(theta,P,n);
            RI += PpQ[n]   * IGamma(theta,Q,n);
            if (n+1>nr-2) break;

            RI += PmP[n+1] * IGamma(theta,P,n+1);
            RI += PmQ[n+1] * IGamma(theta,Q,n+1);
        }

        return RI;
    }

}


void Matrices(double phi) {

    double M[2*LMAX+1][2*LMAX+1];
    double R[2*LMAX+1][2*LMAX+1];

    for (int l=0; l<LMAX; ++l) {

        for (int j=0; j<=2*l; ++j) {
            M[l][j] = SHrot[l][l][j];

            for (int i=1; i<=l; ++i) {
                double c = cos(i*phi);
                double s = sin(i*phi);

                M[l-i][j] =  c*SHrot[l] [l-i][j] + s*SHrot[l] [l+i][j];
                M[l+i][j] = -s*SHrot[l] [l-i][j] + c*SHrot[l] [l+i][j];
            }
        }

        for (int i=0; i<=2*l; ++i) {
            for (int j=0; j<=2*l; ++j) {
                double sum = 0;

                for (int k=0; k<=2*l; ++k) {
                    sum += SHrotI[l] [i][k]*M[k][j];
                }
                R[i][j] = sum;
            }
        }

        for (int i=0; i<=2*l; ++i) {
            for (int j=0; j<=2*l; ++j) {
                cout << R[i][j] << " ";
            }
            cout << endl;
        }

        cout << endl;
    }

}

void Integral(int La, int Lb, int Ly, double A, double B, double phi) {

    for (int la=0; la<=La; ++la) {
        for (int lb=0; lb<=Lb; ++lb) {

        }
    }



    int Ls, Lt;

    //GTO A
    for (int ma=-La; ma<=La; ++ma) {
        // ECP |l> projector
        for (int ly=abs(ma); ly<=Ly; ++ly) {
            for (int t=La+ly; t>=0; t-=2) {
                Yprod[ma][La][ly][t];
            }
        }

        for (int mb=-Lb; mb<=Lb; ++mb) {}
    }

}

//May 15th 2013: Correctly integrates all even (n+m=2k) orders with precision of about 13 digits;
//evaluation of residuals at even orders apparently not required




int ECPmain() {
    InitCartList();
    InitSHList();
    InitY();

    //char a; cin >> a;

    //cout.precision(17);
    //cout.setf(ios::fixed);

    //Matrices(0.123);


    //cout << RadialIntegral(1,1, 0, 1.,2.,3., 0.5,1.) << endl<< endl; // 0.0028999204521420936`
    //cout << RadialIntegral(2,0, 0, 1.,2.,3., 0.5,1.) << endl<< endl; //0.009178150824661457`
    //cout << RadialIntegral(4,0, 0, 1.,2.,3., 0.5,1.) << endl<< endl; // 0.00561468420440534`
    //cout << RadialIntegral(2,2, 0, 1.,2.,3., 0.5,1.) << endl<< endl; // 0.0044750729620093264`

    cout << RadialIntegral(3,3, 4, 1.,2.,3., 0.5,1.) << endl<< endl; // 0.001172540710697253`

    char a; cin >> a;


    //int la=2, lb=2;

    bool C[9][9] ={{true, false,true, false,false,true, false,true, false},
                    {false,true, false,true, true, false,true, false, true},
                    {true, false,true, false,false,true, false,true, false},
                    {false,true, false,true, true, false,true, false, true},
                    {false,true, false,true, true, false,true, false, true},
                    {true, false,true, false,false,true, false,true, false},
                    {false,true, false,true, true, false,true, false, true},
                    {true, false,true, false,false,true, false,true, false},
                    {false,true, false,true, true, false,true, false, true}};

    set<ECP> ecps;

    for (int la=0; la<=6; ++la) {
        for (int lb=0; lb<=6; ++lb) {

            ECP w;

            w.la = la;
            w.lb = lb;

            for (int ma=-la; ma<=la; ++ma) {
                for (int mb=-lb; mb<=lb; ++mb) {
                    w.ma = ma;
                    w.mb = mb;

                    ecps.insert(w);
                }
            }

        }
    }


    set<ECP> rms;

    for (int l=0; l<=4; ++l) {

        for (int ma=-l; ma<=l; ++ma) {
            for (int mb=-l; mb<=l; ++mb) {

                if (C[4+ma][4+mb]) {
                    ECP w;
                    w.la = w.lb = l;
                    w.ma = ma; w.mb = mb;
                    rms.insert(w);
                }

            }
        }

    }

    set<ECP2> ecps2;

    set<ECP>::iterator it1,it2;


    for (it1=ecps.begin(); it1!=ecps.end(); ++it1) {
        for (it2=rms.begin(); it2!=rms.end(); ++it2) {

            ECP ecp = *it1;
            ECP rm  = *it2;

            if ((ecp.ma==rm.ma) && (ecp.mb==rm.mb)) {
                ECP2 w;
                w.la = ecp.la;
                w.lb = ecp.lb;
                w.ma = ecp.ma;
                w.mb = ecp.mb;
                w.ls = rm.la;
                w.lt = rm.lb;

                ecps2.insert(w);
            }

        }
    }

    cout << ecps2.size() << endl;


    set<ECP3> ecps3;

    set<ECP2>::iterator it3;

    for (it3=ecps2.begin(); it3!=ecps2.end(); ++it3) {
        ECP2 ecp = *it3;

        ECP3 w;
        w.nr = 2+ecp.la + ecp.lb;

        for (int l1=ecp.la+ecp.ls ; l1>=0; l1-=2) {
            for (int l2=ecp.lb+ecp.lt ; l2>=0; l2-=2) {
                w.ns = l1;
                w.nt = l2;

                ecps3.insert(w);
            }
        }

    }

    cout << ecps3.size() << endl;

    set<ECP3>::iterator it4;

    for (it4=ecps3.begin(); it4!=ecps3.end(); ++it4) {
        cout << int(it4->nr) << "  " << int(it4->ns) << " " << int(it4->nt) << endl;
    }







    return 0;
}




#include "libechidna/libechidna.hpp"
#include "linear/newtensors.hpp"
#include "math/gamma.hpp"
using namespace LibIGamma;


typedef double Triple  [3*LMAX+1][3*LMAX+1][3*LMAX+1];
typedef void (*R1int)  (Triple &  , double, const vector3 &);

template <int L> void R1Integral(Triple & Rtuv, double k, const vector3 & R) {
    double Fn[L+1];

    //vector3 R = N-P;
    double R2 = R*R;

    double ** gamma = IncompleteGamma.gamma_table;
    double *  gamma2 = IncompleteGammas[L].gamma_table_vals;

    //calcula las 'F's
    //IncompleteGamma.calcF2(Fn, L, k*(R*R));
    {
        double kR2 = k*R2;

        if (kR2>vg_max-vg_step) {
            double iR2 = 1/R2;
            Fn[0] = 0.5*sqrt(PI*iR2);

            for (int i=1; i<=L; ++i) {
                Fn[i] = Fn[i-1]*iR2*double(2*i-1);
            }
        }
        else {
            double p = ivg_step*(kR2-vg_min);
            int pos = int(p+0.5);
            double x0 = vg_min + vg_step*double(pos);
            double Ax1 = x0-kR2;
            double Ax2 = 0.5 * Ax1*Ax1;
            double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

            Fn[L] = (gamma[pos][L+1] + gamma[pos][L+2] * Ax1 + gamma[pos][L+3] * Ax2 + gamma[pos][L+4] * Ax3);

            if (L>0) {
                double expx = gamma[pos][0] * (1+Ax1+Ax2+Ax3);

                for (int i=L-1; i>=0; --i)
                    Fn[i] = (2*kR2*Fn[i+1] + expx)*(1./double(2*i+1));
            }

            double kn   = sqrt(k);

            for (int n=0; n<=L; ++n) {
                Fn[n] *= kn;
                kn    *= 2*k;
            }
        }

    }


    //calcula R^{n}_{000}
    double ac = (2*PI)/(k*sqrt(k));
    for (int n=0; n<=L; ++n) {
        Fn[n] *= ac;
    }

    //calcula R_{tuv} reaprovechando la memoria intermedia
    //(podria tener un impacto negativo debido a la dependencia de variables o positivo por usar menos registros/cache)
    Rtuv[0][0][0] = Fn[L];
    for (int n=L-1; n>=0; --n) {
        for (int tuv=L-n; tuv>=1; --tuv) {
            for (int uv=0; uv<=tuv; ++uv) {
                for (int v=0; v<=uv; ++v) {
                    int t = tuv - uv;
                    int u =  uv -  v;

                    if(t==0) {
                        if(u==0) {
                            if(v==0) {
                                //(CANNOT HAPPEN)
                            }
                            else if (v==1) Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1];
                            else           Rtuv[t][u][v] = R.z * Rtuv[t][u][v-1] -(v-1)*Rtuv[t][u][v-2];
                        }
                        else if (u==1) Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v];
                        else           Rtuv[t][u][v] = R.y * Rtuv[t][u-1][v] -(u-1)*Rtuv[t][u-2][v];
                    }
                    else if (t==1) Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v];
                    else           Rtuv[t][u][v] = R.x * Rtuv[t-1][u][v] -(t-1)*Rtuv[t-2][u][v];
                }
            }
        }

        //tuv = 0
        Rtuv[0][0][0] = Fn[n];
    }

}


// integrate the triple product of gaussians
void H3 (double ik,
          double A, double B, double C,
          int na, int nb, int nc,
          double HH[LMAX+1][LMAX+1][LMAX+1]) {

    //compute the exp integrals
    double Iexp[2*LMAX+1];
    Iexp[0] = 1;
    for (int i=0; i<=(na+nb+nc)/2; ++i) Iexp[i+1] = double(2*i+1) * ik * Iexp[i];


    pol polA[LMAX+1];
    pol polB[LMAX+1];
    pol polC[LMAX+1];

    for (int i=0; i<=na; ++i) polA[i] = Pascal<2*LMAX>(i,A);
    for (int i=0; i<=nb; ++i) polB[i] = Pascal<2*LMAX>(i,B);
    for (int i=0; i<=nc; ++i) polC[i] = Pascal<2*LMAX>(i,C);

    for (int i=0; i<=na; ++i) {
        for (int j=0; j<=nb; ++j) {
            pol polAB;
            polAB = polA[i] * polB[j];

            for (int k=0; k<=nc; ++k) {
                pol polABC;
                polABC = polAB * polC[k];

                double sum = 0;
                for (int nn=0; nn<=i+j+k; nn+=2) sum += polABC.w[nn] * Iexp[nn/2];

                HH[i][j][k] = sum;
            }
        }
    }

}


// decompose a triple product in Hermite functions
void H3 (double ik,
          double A, double B, double C,
          int na, int nb, int nc,
          polinom<3*LMAX> HH[LMAX+1][LMAX+1][LMAX+1]) {


    polinom<3*LMAX> polA[LMAX+1];
    polinom<3*LMAX> polB[LMAX+1];
    polinom<3*LMAX> polC[LMAX+1];

    for (int i=0; i<=na; ++i) polA[i] = Pascal<3*LMAX>(i,A);
    for (int i=0; i<=nb; ++i) polB[i] = Pascal<3*LMAX>(i,B);
    for (int i=0; i<=nc; ++i) polC[i] = Pascal<3*LMAX>(i,C);


    for (int i=0; i<=na; ++i) {
        for (int j=0; j<=nb; ++j) {
            polinom<3*LMAX> polAB;
            polAB = polA[i] * polB[j];

            for (int k=0; k<=nc; ++k) {
                polinom<3*LMAX> polABC;
                polABC = polAB * polC[k];

                HH[i][j][k] = Hexp(polABC, ik);
            }
        }
    }

}



void ECPlocal(const ShellPair & SP, const rAtom * nuclei, const ECPotential & Vecp, const point & C, symtensor & V) {

    double VV[maxJ][maxJ][MMAX][MMAX];


    int La = SP.getLa();
    int Lb = SP.getLb();

    int Ja = SP.GP->Ja;
    int Jb = SP.GP->Jb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }

        }
    }

    R1int R1ints[3*LMAX+1]; {
        R1ints [0] = R1Integral< 0>;
        R1ints [1] = R1Integral< 1>;
        R1ints [2] = R1Integral< 2>;
        R1ints [3] = R1Integral< 3>;
        R1ints [4] = R1Integral< 4>;
        R1ints [5] = R1Integral< 5>;
        R1ints [6] = R1Integral< 6>;
        R1ints [7] = R1Integral< 7>;
        R1ints [8] = R1Integral< 8>;
        R1ints [9] = R1Integral< 9>;
        R1ints[10] = R1Integral<10>;
        R1ints[11] = R1Integral<11>;
        R1ints[12] = R1Integral<12>;
        /*
        Rints[13] = RIntegralXX<13>;
        Rints[14] = RIntegralXX<14>;
        Rints[15] = RIntegralXX<15>;
        Rints[16] = RIntegralXX<16>;
        Rints[17] = RIntegralXX<17>;
        Rints[18] = RIntegralXX<18>;
        Rints[19] = RIntegralXX<19>;
        Rints[20] = RIntegralXX<20>;
        */
    }



	point A = nuclei[SP.ata].c;
	point B = nuclei[SP.atb].c;

	//loop over local potential primitives
	for (int c=0; c<Vecp.Ulc.nprim; ++ c) {
	    int    lc = Vecp.Ulc.n[c];
	    double kc = Vecp.Ulc.k[c];

        for (int b=0; b<SP.getKb(); ++b) {
            for (int a=0; a<SP.getKa(b); ++a) {
                double ka  = SP.GP->ka[a];
                double kb  = SP.GP->kb[b];

                double kabc = ka+kb+kc;

                double ikabc = 1./(kabc);


                point Q;
                Q.x = (ka*A.x + kb*B.x + kc*C.x) * ikabc;
                Q.y = (ka*A.x + kb*B.x + kc*C.x) * ikabc;
                Q.z = (ka*A.x + kb*B.x + kc*C.x) * ikabc;

                vector3 AQ, BQ, CQ;
                AQ = Q-A;
                BQ = Q-B;
                CQ = Q-C;


                double w3 = sqrt(PI*ikabc);
                double kR2; {
                    vector3 AB = B-A;
                    vector3 CA = A-C;
                    vector3 BC = C-B;
                    kR2 = (ka*kb*(AB*AB) + ka*kc*(CA*CA) + kb*kc*(BC*BC)) * ikabc;
                }

                double wab = w3*w3*w3* exp(-kR2);

                static const int Fact[] = {1,1,2,6,24,120,720,5040};


                // for local potentials with even power of the radius
                if (lc%2==0) {

                    double TPx[LMAX+1][LMAX+1][LMAX+1];
                    double TPy[LMAX+1][LMAX+1][LMAX+1];
                    double TPz[LMAX+1][LMAX+1][LMAX+1];

                    H3 (0.5*ikabc,   AQ.x, BQ.x, CQ.x, La, Lb, lc,   TPx);
                    H3 (0.5*ikabc,   AQ.y, BQ.y, CQ.y, La, Lb, lc,   TPy);
                    H3 (0.5*ikabc,   AQ.z, BQ.z, CQ.z, La, Lb, lc,   TPz);


                    for (int ma=0; ma<nmC[La]; ++ma) {
                        int ax = CartList[La][ma].nx;
                        int ay = CartList[La][ma].ny;
                        int az = CartList[La][ma].nz;

                        for (int mb=0; mb<nmC[Lb]; ++mb) {
                            int bx = CartList[Lb][mb].nx;
                            int by = CartList[Lb][mb].ny;
                            int bz = CartList[Lb][mb].nz;


                            //loop over r^2n indices
                            double sumr2n = 0;

                            for (int cx=0; cx<=lc; cx+=2) {
                                for (int cy=0; cy<=lc-cx; cy+=2) {
                                    int cz=lc-cx-cy;

                                    int nf = Fact[lc/2] / (Fact[cx/2]*Fact[cy/2]*Fact[cz/2]);

                                    sumr2n += nf * (TPx[ax][bx][cx] * TPy[ay][by][cx] * TPz[az][bz][cz]);
                                }
                            }

                            sumr2n *= wab * Vecp.Ulc.w[c];

                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {
                                    double w12 = SP.GP->Na [ja][a] * SP.GP->Nb [jb][b];

                                    VV[ja][jb][ma][mb] += w12 * sumr2n;
                                }
                            }

                        }
                    }

                }

                // odd power; this one is trickier
                else {

                    int Lt = La+Lb+lc+1;

                    Triple R1;
                    R1ints[Lt](R1, kabc, CQ);



                    polinom<3*LMAX> H3x[LMAX+1][LMAX+1][LMAX+1];
                    polinom<3*LMAX> H3y[LMAX+1][LMAX+1][LMAX+1];
                    polinom<3*LMAX> H3z[LMAX+1][LMAX+1][LMAX+1];

                    H3 (0.5*ikabc,   AQ.x, BQ.x, CQ.x, La, Lb, lc+1,   H3x);
                    H3 (0.5*ikabc,   AQ.y, BQ.y, CQ.y, La, Lb, lc+1,   H3y);
                    H3 (0.5*ikabc,   AQ.z, BQ.z, CQ.z, La, Lb, lc+1,   H3z);



                    for (int ma=0; ma<nmC[La]; ++ma) {
                        int ax = CartList[La][ma].nx;
                        int ay = CartList[La][ma].ny;
                        int az = CartList[La][ma].nz;

                        for (int mb=0; mb<nmC[Lb]; ++mb) {
                            int bx = CartList[Lb][mb].nx;
                            int by = CartList[Lb][mb].ny;
                            int bz = CartList[Lb][mb].nz;



                            //loop over r^2n indices
                            double sumr2n = 0;

                            for (int cx=0; cx<=lc+1; cx+=2) {
                                for (int cy=0; cy<=lc+1-cx; cy+=2) {
                                    int cz=lc+1-cx-cy;

                                    int nf = Fact[(lc+1)/2] / (Fact[cx/2]*Fact[cy/2]*Fact[cz/2]);

                                    double sum = 0;
                                    for (int i=0; i<=ax+bx+cx; ++i) {
                                        for (int j=0; j<=ay+by+cy; ++j) {
                                            for (int k=0; k<=az+bz+cz; ++k) {
                                                sum += (H3x[ax][bx][cx].w[i] * H3y[ay][by][cy].w[j] * H3z[az][bz][cz].w[k]) * R1[i][j][k];
                                            }
                                        }
                                   }

                                    sumr2n += nf * sum;
                                }
                            }

                            sumr2n *= wab * Vecp.Ulc.w[c];

                            for (int ja=0; ja<Ja; ++ja) {
                                for (int jb=0; jb<Jb; ++jb) {
                                    double w12 = SP.GP->Na [ja][a] * SP.GP->Nb [jb][b];

                                    VV[ja][jb][ma][mb] += w12 * sumr2n;
                                }
                            }

                        }
                    }

                }

            }
        }
	}





    //monoelectronic tensor final adjusts & normalization
    int ta = SP.ata;
    int tb = SP.atb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = SP.getFa() + ja*nmS[La] + a;
                    int ffb = SP.getFb() + jb*nmS[Lb] + b;

                    double sumV = 0;

                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    V(ta,tb, ffa,ffb) = sumV;
                }
            }

        }
    }


}



/*
    New analytic derivation!
*/

//Integrate [u^(2n+1) Exp[u^2] Erf[u], u]
void intU0 (double u0, double uf, int n, double Us[2*LMAX+1]) {

    static const double ISQRTPI = 1./sqrt(PI);

    //double Pn[2*LMAX+1];

    double expf = exp(uf*uf);
    double exp0 = exp(u0*u0);

    double eef = erf(uf) * expf;
    double ee0 = erf(u0) * exp0;

    double ufn = uf;
    double u0n = u0;

    for (int i=0; i<=n/2; ++i) {
        //Pn[i] = (ufn-u0n)/double(2*i+1);
        double Pi = (ufn-u0n)/double(2*i+1);

        ufn *= uf;
        u0n *= u0;

        Us[i] = 0.5*(ufn*eef - u0n*ee0) - double(i)*Us[i-1] - ISQRTPI * Pi;

        ufn *= uf;
        u0n *= u0;
    }

}

//Integrate [u^(2n) Exp[u^2] , u]
void intU1 (double u0, double uf, int n, double Us[2*LMAX+1]) {

    static const double SQRTPI = sqrt(PI);

    double expf = exp(uf*uf);
    double exp0 = exp(u0*u0);

    double erfif = expf * DawsonF(uf);
    double erfi0 = exp0 * DawsonF(u0);

    Us[0] = erfif - erfi0;

    double ufn = uf*expf;
    double u0n = u0*exp0;

    for (int i=1; i<=n/2; ++i) {
        Us[i] = 0.5*(ufn - u0n) - 0.5*double(2*i+1)*Us[i-1];

        ufn *= uf*uf;
        u0n *= u0*u0;
    }

}


// Integrate [ s^np t^nq Exp[k (eta s + zeta t)^2] Erf[k^(1/2) (eta s + zeta t)], {s,-1,1}, {t,-1,1} ]
void ECP2Dint0 (double eta, double zeta,   int np, int nq,    double ECP2D0 [2*LMAX+1][2*LMAX+1]) {

    int nn = np+nq;

    //eta  *= sqrt(k);
    //zeta *= sqrt(k);

    //find the rotation that "simplifies" the integral
    double mu2 = (eta*eta + zeta*zeta);
    double mu  = sqrt(mu2);
    double imu = 1./mu;

    /*
        u          c      s        s
       (  )  =   (           ) .  (  )
        v         -s      c        t
    */

    double c   =  eta*imu;
    double s   = zeta*imu;

    //find the integration limits in the rotated coordinates
    //******************************************************

    struct point2 {
        double u;
        double v;

        void operator()(double a, double b) { u=a; v=b;};
    };

    point2 C00,C01,C10,C11;

    C00 ( c+s, -s+c); // east  (max U)
    C01 ( c-s, -s-c); // south (min V)
    C10 (-c+s,  s+c); // north (max V)
    C11 (-c-s,  s-c); // west  (min U)

    // positive area: C11 - C10 - C00
    {
        double u0, u1, u2;
        double v0, v1, v2;
        u0 = C11.u;
        u1 = C10.u;
        u2 = C00.u;
        v0 = C11.v;
        v1 = C10.v;
        v2 = C00.v;

        double k01 = (v1-v0)/(u1-u0);
        double k12 = (v2-v1)/(u2-u1);

        //integrate v^n

        //v0 + k01*(u-u0)  -> (v0-k01*u0) + k01*u
        v0 -= k01*u0;

        pol Vu01[2*LMAX+1];
        for (int i=0; i<=nn; ++i) {
            pol veu = Pascal<2*LMAX>(i+1, k01, v0);
            veu.w[0] = 0;  // -=v0^(i+1)
            Vu01[i] =  veu * (1./double(i+1));
        }


        //v1 + k12*(u-u1)  -> (v1-k12*u1) + k12*u
        v1 -= k12*u1;
        pol Vu12[2*LMAX+1];
        for (int i=0; i<=nn; ++i) {
            pol veu = Pascal<2*LMAX>(i+1, k12, v1);
            veu.w[0] = 0;  // -=v0^(i+1)
            Vu12[i] =  veu * (1./double(i+1));
        }


        //compute and accumulate the 2D integrals

        double U01[2*LMAX+1];
        intU0 (mu*u0, mu*u1, nn, U01);

        double U12[2*LMAX+1];
        intU0 (mu*u1, mu*u2, nn, U12);


        double ECP2Drot[2*LMAX+1][2*LMAX+1];

        int nu, nv;

        for (int p=0; p<=nu; ++p) {
            for (int q=0; q<=nv; ++q) {
                double sum = 0;

                for (int kk=1; kk<=q+1; ++kk) {
                    if ( (p+kk)%2 == 1) {
                        sum += Vu01[q].w[kk] * U01[(p+kk)/2];
                        sum += Vu12[q].w[kk] * U12[(p+kk)/2];
                    }
                }

                // perform the mu-scaling!
                ECP2Drot[p][q] = sum * pow(imu,p+q+2);
            }
        }


        //rotate the polynomials:
        polinom<4*LMAX> rotS[2*LMAX+1];
        polinom<4*LMAX> rotT[2*LMAX+1];

        for (int p=0; p<=np; ++p) rotS[p] = Pascal<4*LMAX>(p,  c,-s);
        for (int q=0; q<=nq; ++q) rotT[q] = Pascal<4*LMAX>(q,  s, c);

        //generate the s,t integrals

        for (int p=0; p<=np; ++p) {
             for (int q=0; q<=nq; ++q) {
                 if ((p+q)%2 == 0)
                     ECP2D0[p][q] = 0;
                 else {
                     polinom<4*LMAX> prod;
                     prod = rotS[p]*rotT[q];

                     double sum = 0;
                     int maxu = p+q;

                     for (int i=0; i<=maxu; ++i) sum += prod.w[i] * ECP2Drot[i][maxu-i];

                     ECP2D0[p][q] = sum;
                 }
             }
        }


    }

    // negative area: C11 - C01 - C00

}

// Integrate [ s^np t^nq Exp[k (eta s + zeta t)^2] r Exp[-k r^2] ], {s,-1,1}, {t,-1,1}, {r, 0, (eta s + zeta t)} ]
void ECP2Dint1 (double eta, double zeta,   int np, int nq,    double ECP2D1 [2*LMAX+1][2*LMAX+1]) {

    int nn = np+nq;

    //eta  *= sqrt(k);
    //zeta *= sqrt(k);

    //find the rotation that "simplifies" the integral
    double mu2 = (eta*eta + zeta*zeta);
    double mu  = sqrt(mu2);
    double imu = 1./mu;

    /*
        u          c      s        s
       (  )  =   (           ) .  (  )
        v         -s      c        t
    */

    double c   =  eta*imu;
    double s   = zeta*imu;

    //find the integration limits in the rotated coordinates
    //******************************************************

    struct point2 {
        double u;
        double v;

        void operator()(double a, double b) { u=a; v=b;};
    };

    point2 C00,C01,C10,C11;

    C00 ( c+s, -s+c); // east  (max U)
    C01 ( c-s, -s-c); // south (min V)
    C10 (-c+s,  s+c); // north (max V)
    C11 (-c-s,  s-c); // west  (min U)

    // positive area: C11 - C10 - C00
    {
        double u0, u1, u2;
        double v0, v1, v2;
        u0 = C11.u;
        u1 = C10.u;
        u2 = C00.u;
        v0 = C11.v;
        v1 = C10.v;
        v2 = C00.v;

        double k01 = (v1-v0)/(u1-u0);
        double k12 = (v2-v1)/(u2-u1);

        //integrate v^n

        //v0 + k01*(u-u0)  -> (v0-k01*u0) + k01*u
        v0 -= k01*u0;

        pol Vu01[2*LMAX+1];
        for (int i=0; i<=nn; ++i) {
            pol veu = Pascal<2*LMAX>(i+1, k01, v0);
            veu.w[0] = 0;  // -=v0^(i+1)
            Vu01[i] =  veu * (1./double(i+1));
        }


        //v1 + k12*(u-u1)  -> (v1-k12*u1) + k12*u
        v1 -= k12*u1;
        pol Vu12[2*LMAX+1];
        for (int i=0; i<=nn; ++i) {
            pol veu = Pascal<2*LMAX>(i+1, k12, v1);
            veu.w[0] = 0;  // -=v0^(i+1)
            Vu12[i] =  veu * (1./double(i+1));
        }


        //compute and accumulate the 2D integrals
        double U01[2*LMAX+1];
        intU1 (mu*u0, mu*u1, nn, U01);

        double U12[2*LMAX+1];
        intU1 (mu*u1, mu*u2, nn, U12);


        double ECP2Drot[2*LMAX+1][2*LMAX+1];

        int nu, nv;

        for (int p=0; p<=nu; ++p) {
            for (int q=0; q<=nv; ++q) {
                double sum = 0;

                for (int kk=1; kk<=q+1; ++kk) {
                    if ( (p+kk)%2 == 0) {
                        sum += Vu01[q].w[kk] * U01[(p+kk)/2];
                        sum += Vu12[q].w[kk] * U12[(p+kk)/2];
                    }
                }

                // perform the mu-scaling!
                ECP2Drot[p][q] = sum * pow(imu,p+q+2);
            }
        }


        //rotate the polynomials:
        polinom<4*LMAX> rotS[2*LMAX+1];
        polinom<4*LMAX> rotT[2*LMAX+1];

        for (int p=0; p<=np; ++p) rotS[p] = Pascal<4*LMAX>(p,  c,-s);
        for (int q=0; q<=nq; ++q) rotT[q] = Pascal<4*LMAX>(q,  s, c);

        //generate the s,t integrals


        for (int p=0; p<=np; ++p) {
             for (int q=0; q<=nq; ++q) {
                 if ((p+q)%2 == 1)
                     ECP2D1[p][q] = 0;
                 else {
                     polinom<4*LMAX> prod;
                     prod = rotS[p]*rotT[q];

                     double sum = 0;
                     int maxu = p+q;

                     for (int i=0; i<=maxu; ++i) sum += prod.w[i] * ECP2Drot[i][maxu-i];

                     ECP2D1[p][q] = 0.5*sum;
                 }

                 if (p%2==0 && q%2==0) {
                     ECP2D1[p][q] -= 0.5*2*2* (1./double(p+1)) * (1./double(q+1));
                 }
             }
        }





    }

    // negative area: C11 - C01 - C00




}

// Integrate [ s^np t^nq Exp[k (eta s + zeta t)^2] r Exp[-k r^2] ], {s,-1,1}, {t,-1,1}, {r, 0, (eta s + zeta t)} ]
void ECP2Dint (double eta, double zeta, double k,  int nr, int np, int nq,   double ECP3D [4*LMAX+1][2*LMAX+1][2*LMAX+1]) {


    //eta  *= sqrt(k);
    //zeta *= sqrt(k);

    //fill this with polynomial st integrals
    double pol3Dint[4*LMAX+1][2*LMAX+1][2*LMAX+1];

    for (int r=0; r<=nr; ++r) {
        polinom<4*LMAX> pst;
        pst = Pascal<4*LMAX>(r, eta, zeta);

        for (int p=0; p<=np; ++p) {
            for (int q=0; q<=nq; ++q) {

                double sum = 0;

                for (int i=0; i<=r; ++i) {
                    int pp = p+i;
                    int qq = q+r-i;

                    if ((pp%2==0) && (qq%2==0))
                        sum += pst.w[i] * 2/double(pp+1) * 2/double(qq+1);
                }

                pol3Dint[r][p][q] = sum;
            }
        }
    }


    //apply RR
    for (int r=2; r<=nr; ++r) {
        for (int p=0; p<=np; ++p) {
            for (int q=0; q<=nq; ++q) {
                ECP3D[r][p][q] = -0.5*pol3Dint[r-1][p][q] + 0.5*double(r-1) * ECP3D[r-2][p][q];
            }
        }
    }



}



void CoreECP (double a, double b, double c,   double A, double B  ) {

    double k = a+b+c;
    double ik = 1./k;

    double eta  = a*A*sqrt(ik);
    double zeta = b*B*sqrt(ik);

    //double eta  = a*A*ik;
    //double zeta = b*B*ik;

    double eaA2 = exp (-a*A*A);
    double ebB2 = exp (-b*B*B);


    //compute the most complex integrals
    //==================================
    double ecp3D[4*LMAX+1][2*LMAX+1][2*LMAX+1];

    int np, nq, nr;


    ECP2Dint0 (eta, zeta,       np, nq, ecp3D[0]);
    ECP2Dint1 (eta, zeta,       np, nq, ecp3D[1]);
    //ECP2Dint0 (eta, zeta, k,      np, nq, ecp3D[0]);
    //ECP2Dint1 (eta, zeta, k,      np, nq, ecp3D[1]);
    ECP2Dint  (eta, zeta, k,  nr, np, nq, ecp3D   );




    //after everything else
    //*********************

    //double kn = pow(k, -.5*double(n+1));


}



// evaluates the integral:
// Integrate [ r^n Exp[-c r^2] Exp[-a (r^2 - 2 A r s + A^2)] Exp[-b (r^2 - 2 B r t + B^2)] s^p t^q, {r,0,Infinity}, {s,-1,1}, {t,-1,1}]
// which is rather tricky

void MasterECP (double a, double b, double c,   double A, double B,    int n, int p, int q) {
    double k = a+b+c;
    double ik = 1./k;

    //double eta  = a*A*sqrt(ik);
    //double zeta = b*B*sqrt(ik);

    double eta  = a*A*ik;
    double zeta = b*B*ik;


    double eaA2 = exp (-a*A*A);
    double ebB2 = exp (-b*B*B);




    // coordinate change u'^2 = k u^2

    // divide the area into triangles in orfr to integrate

    // rotate the integration coordinates s and t (expand the polynomials)


    // obtain incomplete gaussian integrals by recursion from erf and exp

    // sum the gaussian integral [0, oo] with the incomplete integral

    // expand the radial polynomial

}



void ECPsemilocal(const ShellPair & SP, const rAtom * nuclei, const ECPotential & Vecp, const point & C, symtensor & V) {

    double VV[maxJ][maxJ][MMAX][MMAX];


    int La = SP.getLa();
    int Lb = SP.getLb();

    int Ja = SP.GP->Ja;
    int Jb = SP.GP->Jb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int i=0; i<nmC[La]; ++i) {
                for (int j=0; j<nmC[Lb]; ++j) {
                    VV[ja][jb][i][j] = 0;
                }
            }

        }
    }



	point A = nuclei[SP.ata].c;
	point B = nuclei[SP.atb].c;

	//positions relative to the ECP
	vector3 CA, CB;
	CA = A-C;
	CB = B-C;


	//find an appropiate  coordinate system
	//+++++++++++++++++++++++++++++++++++++
	vector3 vx,vy,vz; {
        vector3 vx = CA;          // normalize to get x
        vector3 vy = CB;          // y-ish
        vector3 vz = Xprod(vx,vy); // this is now the new z

        normalize(vx);
        normalize(vz);

        vy = Xprod(vz,vx);
        normalize(vy);
	}


	double dCA = norm(CA);
	double dCB = norm(CB);
	double cosphi = (CA*CB)/(dCA*dCB);
	double sinphi = sqrt(1-cosphi*cosphi); // first two quadrants


	// translate the solid harmonics in A and B int the z direction around C
	// half of them is enough
	//*********************************************************************
	double TrAC[LMAX+1][LMAX+1];
	double TrBC[LMAX+1][LMAX+1];

	int la, lb;

	//for (int ma=0; ma<=la; ++ma) SHtranslateZ (la, ma, dCA, TrAC[ma]);
	//for (int mb=0; mb<=lb; ++mb) SHtranslateZ (lb, mb, dCB, TrBC[mb]);




	//loop over angular momentum projectors
	for (int lc=0; lc<=Vecp.lmax; ++lc) {
	    const ECPbatch & ECPb = Vecp.Usl[lc];

	    //create the m,m' coupling matrix for the angular momentum and atom positions
        double R[2*LMAX+1][2*LMAX+1]; {

            double M[2*LMAX+1][2*LMAX+1];


            for (int j=0; j<=2*lc; ++j) {
                M[lc][j] = SHrot[lc][lc][j];

                for (int i=1; i<=lc; ++i) {
                    double c = 1;
                    double s = 0;

                    for (int t=0; t<i; ++t){
                        double c2 = c * cosphi - s*sinphi;
                        double s2 = c * sinphi + s*cosphi;

                        c = c2;
                        s = s2;
                    }

                    //double c = cos(i*phi);
                    //double s = sin(i*phi);

                    M[lc-i][j] =  c*SHrot[lc] [lc-i][j] + s*SHrot[lc] [lc+i][j];
                    M[lc+i][j] = -s*SHrot[lc] [lc-i][j] + c*SHrot[lc] [lc+i][j];
                }
            }

            for (int i=0; i<=2*lc; ++i) {
                for (int j=0; j<=2*lc; ++j) {
                    double sum = 0;

                    int l; // ???????

                    for (int k=0; k<=2*l; ++k) {
                        sum += SHrotI[lc] [i][k]*M[k][j];
                    }
                    R[i][j] = sum;
                }
            }

            /*
            for (int i=0; i<=2*lc; ++i) {
                for (int j=0; j<=2*lc; ++j) {
                    cout << R[i][j] << " ";
                }
                cout << endl;
            }
            */
        }



	    //loop over primitives in the projector
	    for (int c=0; c<ECPb.nprim; ++ c) {

	    }
	}


	for (int c=0; c<Vecp.Ulc.nprim; ++ c) {
	    int    lc = Vecp.Ulc.n[c];
	    double kc = Vecp.Ulc.k[c];

        for (int b=0; b<SP.getKb(); ++b) {
            for (int a=0; a<SP.getKa(b); ++a) {
                double ka  = SP.GP->ka[a];
                double kb  = SP.GP->kb[b];

                double ikabc = 1./(ka+kb+kc);


                point Q;
                Q.x = (ka*A.x + kb*B.x + kc*C.x) * ikabc;
                Q.y = (ka*A.x + kb*B.x + kc*C.x) * ikabc;
                Q.z = (ka*A.x + kb*B.x + kc*C.x) * ikabc;

                vector3 AQ, BQ, CQ;
                AQ = Q-A;
                BQ = Q-B;
                CQ = Q-C;

                double TPx[LMAX+1][LMAX+1][LMAX+1];
                double TPy[LMAX+1][LMAX+1][LMAX+1];
                double TPz[LMAX+1][LMAX+1][LMAX+1];

                H3 (0.5*ikabc,   AQ.x, BQ.x, CQ.x, La, Lb, lc,   TPx);
                H3 (0.5*ikabc,   AQ.y, BQ.y, CQ.y, La, Lb, lc,   TPy);
                H3 (0.5*ikabc,   AQ.z, BQ.z, CQ.z, La, Lb, lc,   TPz);



                double w3 = sqrt(PI*ikabc);
                double kR2; {
                    vector3 AB = B-A;
                    vector3 CA = A-C;
                    vector3 BC = C-B;
                    kR2 = (ka*kb*(AB*AB) + ka*kc*(CA*CA) + kb*kc*(BC*BC)) * ikabc;
                }

                double wab = w3*w3*w3* exp(-kR2);


                static const int Fact[] = {1,1,2,6,24,120,720,5040};


                for (int ma=0; ma<nmC[La]; ++ma) {
                    int ax = CartList[La][ma].nx;
                    int ay = CartList[La][ma].ny;
                    int az = CartList[La][ma].nz;

                    for (int mb=0; mb<nmC[Lb]; ++mb) {
                        int bx = CartList[Lb][mb].nx;
                        int by = CartList[Lb][mb].ny;
                        int bz = CartList[Lb][mb].nz;


                        //loop over r^2n indices
                        for (int cx=0; cx<=lc; cx+=2) {
                            for (int cy=0; cy<=lc-cx; cy+=2) {
                                int cz=lc-cx-cy;

                                int nf = Fact[lc/2] / (Fact[cx/2]*Fact[cy/2]*Fact[cz/2]);

                                for (int ja=0; ja<Ja; ++ja) {
                                    for (int jb=0; jb<Jb; ++jb) {
                                        double w12 = nf * wab * SP.GP->Na [ja][a] * SP.GP->Nb [jb][b] * Vecp.Ulc.w[c];

                                        VV[ja][jb][ma][mb] += w12 * (TPx[ax][bx][cx] * TPy[ay][by][cx] * TPz[az][bz][cz]);
                                    }
                                }

                            }
                        }

                    }
                }

            }
        }
	}





    //monoelectronic tensor final adjusts & normalization
    int ta = SP.ata;
    int tb = SP.atb;

    for (int ja=0; ja<Ja; ++ja) {
        for (int jb=0; jb<Jb; ++jb) {

            for (int a=0; a<nmS[La]; ++a) {
                for (int b=0; b<nmS[Lb]; ++b) {
                    int ffa = SP.getFa() + ja*nmS[La] + a;
                    int ffb = SP.getFb() + jb*nmS[Lb] + b;

                    double sumV = 0;

                    for (int i=0; i<SHList[La][a].nps; ++i) {
                        for (int j=0; j<SHList[Lb][b].nps; ++j) {
                            const SHTerm & sha = SHList[La][a].T[i];
                            const SHTerm & shb = SHList[Lb][b].T[j];
                            sumV += sha.cN * shb.cN * VV[ja][jb][sha.nc][shb.nc];
                        }
                    }

                    V(ta,tb, ffa,ffb) = sumV;
                }
            }

        }
    }


}


