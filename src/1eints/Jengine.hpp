#ifndef __J_ENGINE__
#define __J_ENGINE__

#include <list>
#include "defs.hpp"
#include "math/affine.hpp"


class symtensor;
class ShellPair;
class rAtom;

struct HermiteCenter {
    point     C;  // center of the density expansion
    double  nu;  // Gaussian exponent
    int      L;  // maximum angular momentum of the expansion

    double  w[2*LMAX+1][2*LMAX+1][2*LMAX+1]; // weights of the expansion

    HermiteCenter() {

        L  = 0;
        nu = 0;

        // set to 0
        for (int i=0; i<=2*LMAX; ++i)
            for (int j=0; j<=2*LMAX; ++j)
                for (int k=0; k<=2*LMAX; ++k)
                    w[i][j][k] = 0;
    }
};

#include "basis/GTO.hpp"

struct SystemDensity {

    GTObasis DFbasis;

    //system limits
    point Pmin;
    point Pmax;

    std::list <HermiteCenter*> HClist;

    void AddShellPair            (const ShellPair & SP, const rAtom * nuclei, const symtensor & Dp);
    void ComputeCoulombPotential (const ShellPair & AB, const rAtom * nuclei,        symtensor & Jp) const;

    void FitDensity();

    void Clear();
    void Output() const; //
};


#endif
