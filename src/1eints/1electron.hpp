#ifndef __1ELEC__
#define __1ELEC__

class shellpair;
class ShellPair;
class Nucleus;
class rAtom;
class symtensor;
class tensor;
template <class T> class Boxing;


struct Qtensor3 {
    double xx;
    double xy;
    double xz;
    double yy;
    double yz;
    double zz;
};

struct ExternalPotential {
    int Ms; // monopoles
    int Ds; // dipoles
    int Qs; // quadrupoles

    point * MC; // center of the charges
    point * DC; // center of the dipoles
    point * QC; // center of the quadrupoles

    double  * M; // charge of the monopole
    vector3  * D; // dipole components
    Qtensor3 * Q; // quadrupole components

    ExternalPotential() {
        Ms = Ds = Qs = 0;

        MC = NULL;
        DC = NULL;
        QC = NULL;

        M = NULL;
        D = NULL;
        Q = NULL;
    }


    void InitMonopoles(int n) {
        Ms = n;
        delete[] MC;
        delete[] M;
        MC = new point[Ms];
        M  = new double[Ms];
    }

    void InitDipoles(int n) {
        Ds = n;
        delete[] DC;
        delete[] D;
        DC = new point[Ds];
        D  = new vector3[Ds];
    }

    void InitQuadrupoles(int n) {
        Qs = n;
        delete[] QC;
        delete[] Q;
        QC = new point[Qs];
        Q  = new Qtensor3[Qs];
    }


    ~ExternalPotential() {
        delete[] MC;
        delete[] DC;
        delete[] QC;

        delete[] M;
        delete[] D;
        delete[] Q;
    }
};


void CalcPotentialCart     (const ShellPair & AB, const rAtom * nuclei, int nnuclei, symtensor & Vp);

void CalcPotentialCart     (const ShellPair & AB, const rAtom * nuclei, const Boxing<Nucleus> & TBS, symtensor & Vp);

void CalcMonoElectronic    (const ShellPair & AB, const rAtom * nuclei, symtensor & S, symtensor & T,
                            symtensor & X, symtensor & Y, symtensor & Z,
                            symtensor & DX, symtensor & DY, symtensor & DZ,
                            tensor & PX, tensor & PY, tensor & PZ,
                            tensor & LX, tensor & LY, tensor & LZ,
                            const std::vector<double>& center);

void CalcExternalPotential (const ShellPair & AB, const rAtom * nuclei, const ExternalPotential & XV, symtensor & XVp);

void CalcAuPP              (const ShellPair & AB, const rAtom * nuclei, int nnuclei, symtensor & LPP, symtensor & NLPP);

void CalcXRayProperty      (const ShellPair & AB, const rAtom * nuclei, symtensor * RR, symtensor * II, vector3 * kk, vector3 * nn, int N);

#endif
