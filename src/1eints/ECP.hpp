#ifndef __ECP__
#define __ECP__
#include "defs.hpp"

struct ECPbatch {
    double k[maxK]; // gaussian exponents
    double w[maxK]; // primitive weights;
    uint8_t    n[maxK]; // radial power
    uint8_t    nprim;   // number of primitives (0-maxK)
};

struct ECPotential {
    ECPbatch Ulc;         // strictly local potential
    ECPbatch Usl[LMAX+1]; // semilocal potential (involving projection operators)
    int nelec;            // numkber of closed shell electrons for this ECP
    int lmax;             // maximum projector of angular momentum
};


class ECP {
  public:
    char la;
    char ma;
    char lb;
    char mb;

    bool operator<(const ECP & rhs) const {
        if(la!=rhs.la) return (la<rhs.la);
        if(lb!=rhs.lb) return (lb<rhs.lb);
        if(ma!=rhs.ma) return (ma<rhs.ma);
        if(mb!=rhs.mb) return (mb<rhs.mb);
        return false;
    }
};

class ECP2 {
  public:
    char la;
    char ma;
    char lb;
    char mb;
    char ls;
    char lt;

    bool operator<(const ECP2 & rhs) const {
        if(la!=rhs.la) return (la<rhs.la);
        if(lb!=rhs.lb) return (lb<rhs.lb);
        if(ma!=rhs.ma) return (ma<rhs.ma);
        if(mb!=rhs.mb) return (mb<rhs.mb);
        if(ls!=rhs.ls) return (ls<rhs.ls);
        if(lt!=rhs.lt) return (lt<rhs.lt);
        return false;
    }

};

class ECP3 {
  public:
    char nr;
    char ns;
    char nt;

    bool operator<(const ECP3 & rhs) const {
        if(nr!=rhs.nr) return (nr<rhs.nr);
        if(ns!=rhs.ns) return (ns<rhs.ns);
        if(nt!=rhs.nt) return (nt<rhs.nt);
        return false;
    }

};




#endif
