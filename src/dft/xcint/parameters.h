const int AO_BLOCK_LENGTH = 128;
const int AO_CHUNK_LENGTH = 32;
const int MAX_GEO_DIFF_ORDER = 2;
const int MAX_L_VALUE = 4;
