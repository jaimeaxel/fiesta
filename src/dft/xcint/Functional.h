#ifndef Functional_h_
#define Functional_h_

#include <string>
#include <vector>

#include "xcfun.h"

class Functional {

    public:

        Functional();
       ~Functional();

        int set_functional(const char *line);
        int set_order(const int order, xc_functional_obj * fun) const;


        double hfx, cam_alpha, cam_beta, rangesep_mu; // values for HFX

        bool is_gga;      // FIXME make private
        bool is_tau_mgga; // FIXME make private

        std::vector<std::string> keys; // FIXME make private
        std::vector<double>      weights; // FIXME make private

    private:

        Functional(const Functional &rhs);            // not implemented
        Functional &operator=(const Functional &rhs); // not implemented

        char * functional_line;

        int parse(const char *line);
        void nullify();

        bool is_synced;
};

#endif // Functional_h_
