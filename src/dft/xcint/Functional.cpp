#include <math.h>

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <algorithm>
#include <stdio.h>
#include <string.h>

#include "Functional.h"
#include "xcfun.h"


Functional::Functional() {
    nullify();
}


Functional::~Functional() {
    delete [] functional_line;
    nullify();
}


void Functional::nullify() {
    is_gga = false;
    is_tau_mgga = false;
    is_synced = false;
    hfx = 1.;
    keys.clear();
    weights.clear();
    functional_line = NULL;
}


int Functional::set_functional(const char *line) {

    nullify();

    // parse the line
    {
        int ierr = parse(line);

        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: no valid functional given. \n");
            return -1;
        }

        delete [] functional_line;
        functional_line = NULL;
        functional_line = new char[strlen(line)+1];
        for (int i = 0; i < strlen(line); i++) functional_line[i] = line[i];
        functional_line[strlen(line)] = '\0';
    }

    // check that xcfun admits all the keys
    {
        xc_functional fun;
        fun = xc_new_functional();

        for (int i = 0; i < keys.size(); i++) {
            int ierr = xc_set(fun, keys[i].c_str(), weights[i]);

            if (ierr != 0) {
                fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", keys[i].c_str());
                return -1;
            }
        }

        if (xc_is_gga(fun))     is_gga      = true;
        if (xc_is_metagga(fun)) is_tau_mgga = true;

        const char exx[]   = "exx";
        const char alpha[] = "cam_alpha";
        const char beta[]  = "cam_beta";
        const char mu[]    = "rangesep_mu";

        xc_get(fun, exx,   &hfx);
        xc_get(fun, alpha, &cam_alpha);
        xc_get(fun, beta,  &cam_beta);
        xc_get(fun, mu,    &rangesep_mu);

        xc_free_functional(fun);
    }


    return 0;
}


int Functional::parse(const char *line) {
    int pos;
    double w;
    std::string key;

    keys.clear();
    weights.clear();

    std::istringstream iss(line);

    std::vector<std::string> tokens;
    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              std::back_inserter<std::vector<std::string> >(tokens));

    for (int i = 0; i < tokens.size(); i++) {
        pos = tokens[i].find("=");
        if (pos != std::string::npos)
        {
            key = tokens[i].substr(0, pos);
            w = atof(tokens[i].substr(pos+1).c_str());
        }
        else
        {
            key = tokens[i];
            w = 1.0;
        }

        // convert to lowercase
        std::transform(key.begin(), key.end(), key.begin(), ::tolower);

        /*
        if      (key  == "lda")
        {
            keys.push_back("slaterx");
            weights.push_back(w);
            keys.push_back("vwn5c");
            weights.push_back(w);
        }
        else if (key == "blyp")
        {
            keys.push_back("beckex");
            weights.push_back(w);
            keys.push_back("lypc");
            weights.push_back(w);
        }
        else if (key == "camb3lyp")
        {
            keys.push_back("camb3lyp");
            weights.push_back(w);
        }
        else if (key == "b3lyp")
        {
            keys.push_back("slaterx");
            weights.push_back(w*0.8);
            keys.push_back("beckecorrx");
            weights.push_back(w*0.72);
            keys.push_back("vwn5c");
            weights.push_back(w*0.19);
            keys.push_back("lypc");
            weights.push_back(w*0.81);
        }
        else if (key == "pbe")
        {
            keys.push_back("pbex");
            weights.push_back(w);
            keys.push_back("pbec");
            weights.push_back(w);
        }
        else if (key == "pbe0")
        {
            keys.push_back("pbex");
            weights.push_back(w*0.75);
            keys.push_back("pbec");
            weights.push_back(w);
        }
        else if (key == "m06")
        {
            keys.push_back("m06x");
            weights.push_back(w);
            keys.push_back("m06c");
            weights.push_back(w);
        }
        else if (key == "slaterx")
        {
            keys.push_back("slaterx");
            weights.push_back(w);
        }
        else if (key == "pw86x")
        {
            keys.push_back("pw86x");
            weights.push_back(w);
        }
        else if (key == "vwn5c")
        {
            keys.push_back("vwn5c");
            weights.push_back(w);
        }
        else if (key == "pbex")
        {
            keys.push_back("pbex");
            weights.push_back(w);
        }
        else if (key == "lypc")
        {
            keys.push_back("lypc");
            weights.push_back(w);
        }
        else if (key == "beckecorrx")
        {
            keys.push_back("beckecorrx");
            weights.push_back(w);
        }

        // default: this is either a functional we do not
        //          recognize or some parameter; let XCFUN sort the meaning
        else {
            keys.push_back(key);
            weights.push_back(w);
        }
        */

        keys.push_back(key);
        weights.push_back(w);
    }

    if (keys.size() == 0) return -1;

    return 0;

    /*
    if (keys.size() == 0) {
        fprintf(stderr, "ERROR: functional '%s' not recognized\n", line);
        fprintf(stderr, "       list of implemented functionals:\n");
        fprintf(stderr, "       b3lyp     \n");
        fprintf(stderr, "       beckecorrx\n");
        fprintf(stderr, "       blyp      \n");
        fprintf(stderr, "       camb3lyp  \n");
        fprintf(stderr, "       lda       \n");
        fprintf(stderr, "       lypc      \n");
        fprintf(stderr, "       m06       \n");
        fprintf(stderr, "       pbe       \n");
        fprintf(stderr, "       pbe0      \n");
        fprintf(stderr, "       pbex      \n");
        fprintf(stderr, "       pw86x     \n");
        fprintf(stderr, "       slaterx   \n");
        fprintf(stderr, "       vwn5c     \n");
        exit(-1);
    }
    */

}


int Functional::set_order(const int order, xc_functional_obj * fun) const {
    int ierr = -1;

    /*
       depending on the kind of functional, whether there is spin polarization in our system & how we want to express the variables,
       we can use a number of modes as defined in xcfun:

      XC_VARS_UNSET=-1,
      // LDA
      XC_A,
      XC_N,
      XC_A_B,
      XC_N_S,
      // GGA
      XC_A_GAA,
      XC_N_GNN,
      XC_A_B_GAA_GAB_GBB,
      XC_N_S_GNN_GNS_GSS,
      // MetaGGA
      XC_A_GAA_LAPA,
      XC_A_GAA_TAUA,
      XC_N_GNN_LAPN, // 10
      XC_N_GNN_TAUN,
      XC_A_B_GAA_GAB_GBB_LAPA_LAPB,
      XC_A_B_GAA_GAB_GBB_TAUA_TAUB,
      XC_N_S_GNN_GNS_GSS_LAPN_LAPS,
      XC_N_S_GNN_GNS_GSS_TAUN_TAUS,
      XC_A_B_GAA_GAB_GBB_LAPA_LAPB_TAUA_TAUB,
      XC_A_B_GAA_GAB_GBB_LAPA_LAPB_TAUA_TAUB_JPAA_JPBB,
      XC_N_S_GNN_GNS_GSS_LAPN_LAPS_TAUN_TAUS,
      XC_A_AX_AY_AZ,
      XC_A_B_AX_AY_AZ_BX_BY_BZ,
      XC_N_NX_NY_NZ, // 20
      XC_N_S_NX_NY_NZ_SX_SY_SZ,
      XC_A_AX_AY_AZ_TAUA,
      XC_A_B_AX_AY_AZ_BX_BY_BZ_TAUA_TAUB,
      XC_N_NX_NY_NZ_TAUN,
      XC_N_S_NX_NY_NZ_SX_SY_SZ_TAUN_TAUS,
      // 2:nd order Taylor coefficients of alpha density, 1+3+6=10 	 numbers, rev gradlex order
      XC_A_2ND_TAYLOR,
      // 2:nd order Taylor expansion of alpha and beta densities (first alpha, then beta) 20 numbers
      XC_A_B_2ND_TAYLOR,
      XC_N_2ND_TAYLOR,
      XC_N_S_2ND_TAYLOR,
      XC_NR_VARS
    */

    //int xc_input_length(xc_functional fun)
    //int xc_output_length(xc_functional fun)

    // number of indices for all the possible combinations of functional derivatives contracted with perturbations
    // i.e. 0000 0001 0010 0011 0100 0101 0110 0111 etc.
    int dens_offset = (int)pow(2, order);

    if      (is_tau_mgga) ierr = xc_eval_setup(fun, XC_N_NX_NY_NZ_TAUN, XC_CONTRACTED, order);
    else if (is_gga)      ierr = xc_eval_setup(fun, XC_N_NX_NY_NZ, XC_CONTRACTED, order);
    else                  ierr = xc_eval_setup(fun, XC_N, XC_CONTRACTED, order);

    if (ierr != 0) {
        fprintf(stderr, "ERROR in set_order : %i.\n", ierr);
        if (ierr & XC_EORDER) std::cout << "Invalid order for given mode and vars" << std::endl;
        if (ierr & XC_EVARS)  std::cout << "Invalid vars for functional type (ie. lda vars for gga)" << std::endl;
        if (ierr & XC_EMODE)  std::cout << "Invalid mode for functional type (ie. potential for mgga)" << std::endl;
        exit(-1);
    }

    return dens_offset;
}
