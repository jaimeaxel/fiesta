#ifndef XCINT_H_INCLUDED
#define XCINT_H_INCLUDED

typedef enum {
    XCINT_BASIS_SPHERICAL,
    XCINT_BASIS_CARTESIAN
    } xcint_basis_t;

typedef enum {
    XCINT_MODE_RKS,
    XCINT_MODE_UKS
    } xcint_mode_t;

typedef enum {
    XCINT_PERT_EL,
    XCINT_PERT_GEO,
    XCINT_PERT_MAG_CGO,
    XCINT_PERT_MAG_LAO
    } xcint_perturbation_t;

#endif /* XCINT_H_INCLUDED */
