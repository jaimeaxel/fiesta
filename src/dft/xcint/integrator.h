#ifndef INTEGRATOR_H_INCLUDED
#define INTEGRATOR_H_INCLUDED

#include "Basis.h"
#include "AOBatch.h"
#include "Functional.h"
#include "balboa.h"

#include "xcint.h"
#include "xcfun.h"

// this is for AO_BLOCK_LENGTH
#include "parameters.h"


#include "linear/tensors.hpp"

// move this to xcgrid
struct gpoint {
    double x,y,z, w;
};

struct Block {

    // this is 8 x 128 = 1024 bytes
    double v[AO_BLOCK_LENGTH];

    inline       double & operator[] (int i)       {
        return v[i];
    }

    inline const double & operator[] (int i) const {
        return v[i];
    }

} __attribute__((aligned(64))); // this will fit in be 16 cache lines


#include <string.h>

// a density, perturbation, potential, etc.,
// evaluated on a grid
struct GridFunction {

    Block * f = NULL;

    //uint64_t Npoints = 0;
    uint64_t Nblocks = 0;
    uint64_t Nvars   = 0;


    GridFunction() {
        f = NULL;
        Nblocks = Nvars = 0;
    }

    void Set (int nvars, int npoints) {
        Nblocks = 1 + (npoints-1) / AO_BLOCK_LENGTH;
        Nvars = nvars;

        delete[] f;

        f = new Block[Nvars*Nblocks];

        memset ((void*)f, 0, Nvars*Nblocks*sizeof(Block));
    }

   ~GridFunction() {
        delete[] f;
    }
};


typedef int (*print_function)(const char* line);

class XCint {

    static constexpr double DENSMIN = 1e-14;

    public:

        XCint();
       ~XCint();

        int set_basis(const int    basis_type,
                      const int    num_centers,
                      const double center_coordinates[],
                      const int    num_shells,
                      const int    shell_centers[],
                      const int    shell_l_quantum_numbers[],
                      const int    shell_num_primitives[],
                      const double primitive_exponents[],
                      const double contraction_coefficients[]);

        int set_functional(const char *line);
        int get_hf_parms (double & hfx, double & alpha, double & beta, double & mu) const;

        int integrate(const xcint_mode_t         mode,
                      const int                  num_points,
                      const double             * grid,
                      const int                  num_perturbations,
                      const xcint_perturbation_t perturbations[],
                      const int                  components[],
                      const int                  num_dmat,
                      const int                  perturbation_indices[],
                      const double               dmat[],
                      const bool                 get_exc,
                            double               *exc,
                      const bool                 get_vxc,
                            double               vxc[],
                            double               *num_electrons) const;

        // simplified interfaces for RKS & TDDFT

        int integrate2 (
                       const gpoint             * grid,
                       const int                  num_points,
                       const tensor2 &            dmat,
                             tensor2 &            vxc,
                             double             & exc,
                             double             & num_electrons) const;
        int integrate2 (
                       const gpoint             * grid,
                       const int                  num_points,
                       const tensor2 &            dmat,
                       const tensor2 &            pmat,
                             tensor2 &            vxc) const;


        // inefficient at the moment, but working
        int get_density (
                       const gpoint             * grid,
                       const int                  num_points,
                       const tensor2 &            dmat,
                             GridFunction       * dens,
                             double             & num_electrons) const;

        int xc_potential (
                       const GridFunction       * dens,
                             GridFunction       * pot,
                             int                  order) const;

        int scf_integrate (
                       const gpoint             * grid,
                       const int                  num_points,
                       const GridFunction       * pot,
                             tensor2 &            vxc) const;

        int scf_integrate (
                       const gpoint             * grid,
                       const int                  num_points,
                       const GridFunction       * pot,
                             double  &            exc) const;



        // only for linear RSP
        int xc_potential (
                       const gpoint             * grid,
                       const int                  num_points,
                       const tensor2 &            dmat,
                             GridFunction       * ground) const;


        int rsp_integrate (
                       const gpoint             * grid,
                       const int                  num_points,
                       const GridFunction       * ground,
                       const tensor2 *            pmat,
                             int                  Nmat,
                             tensor2 *            vxc) const;


    private:

        XCint(const XCint &rhs);            // not implemented
        XCint &operator=(const XCint &rhs); // not implemented

        Functional fun;
        Basis basis;
        balboa_context_t *balboa_context;

        xc_functional_obj * xcfun;

        void nullify();

        void distribute_matrix(const int              block_length,
                               const int              num_variables,
                               const int              num_perturbations,
                               const int              mat_dim,
                               const double           prefactors[],
                               const bool             n_is_used[],
                               const double           n[],
                                     double           u[],
                                     double           vxc[],
                                     double           &exc,
                               const std::vector<int> coor,
                                     AOBatch     &batch,
                               const double         * grid_pw,
                               const xc_functional_obj * xcfun,
                               double * ee
                               ) const;

        void integrate_batch(const double dmat[],
                             const bool   get_exc,
                                   double &exc,
                             const bool   get_vxc,
                                   double vxc[],
                                   double &num_electrons,
                             const int    geo_coor[],
                             const bool   use_dmat[],
                             const int    geo_derv_order,
                             const int    max_ao_order_g,
                             const int    block_length,
                             const int    num_variables,
                             const int    num_perturbations,
                             const int    num_fields,
                             const int    mat_dim,
                             const bool   get_gradient,
                             const bool   get_tau,
                             const int    dmat_index[],
                             const double * grid_pw,
                             const xc_functional_obj * xcfun,
                            double * nn, double * ee // debug integration
                             ) const;

//      double time_total;
//      double time_ao;
//      double time_fun_derv;
//      double time_densities;
//      double time_matrix_distribution;

        void reset_time();
};

#endif // INTEGRATOR_H_INCLUDED
