#include "xcint.h"
#include "xcfun.h"

#include <math.h>
#include <time.h>
#include <assert.h>

#include <cstdlib>
#include <fstream>
#include <algorithm>

#include "rolex.h"
#include "integrator.h"
#include "AOBatch.h"
#include "MemAllocator.h"

#include "xcint_parameters.h"

//#ifdef ENABLE_OMP
//#include "omp.h"
//#endif

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
    #define omp_get_num_threads() 1
#endif


XCint::XCint() {
    nullify();
    balboa_context = balboa_new_context();
}

XCint::~XCint() {
    nullify();
    balboa_free_context(balboa_context);
}

int XCint::set_functional(const char * line) {

    int ierr = fun.set_functional(line);

    //xc_functional_obj * xcfun = xc_new_functional();

    return ierr;
}

int XCint::get_hf_parms (double & hfx, double & alpha, double & beta, double & mu) const {

    hfx = fun.hfx;
    alpha = fun.cam_alpha;
    beta = fun.cam_beta;
    mu = fun.rangesep_mu;

    return 0;
}


int XCint::set_basis(const int    basis_type,
                     const int    num_centers,
                     const double center_coordinates[],
                     const int    num_shells,
                     const int    shell_centers[],
                     const int    shell_l_quantum_numbers[],
                     const int    shell_num_primitives[],
                     const double primitive_exponents[],
                     const double contraction_coefficients[])
{
    basis.init(basis_type,
               num_centers,
               center_coordinates,
               num_shells,
               shell_centers,
               shell_l_quantum_numbers,
               shell_num_primitives,
               primitive_exponents,
               contraction_coefficients);

    return 0;
}

void XCint::nullify() {
    reset_time();
}

void XCint::reset_time() {
//  time_total = 0.0;
//  time_ao = 0.0;
//  time_fun_derv = 0.0;
//  time_densities = 0.0;
//  time_matrix_distribution = 0.0;
}


/*
    future improvements on XCINT and DFT:

    1) evaluate the perturbed densities from the Ds first; there is no reason to compute everything on the same subset of points;
       the heterogeneity of the calculations being performed makes it difficult to parallelize or vectorize, requires constant allocation and
       deallocation of memory and prevents using a differemt algorithm/parallelization strategy elsewhere
    2) THEN iterate over all grid points computing n,e,u,v,v2 or whatever.
    3) after we have the potentials, densities, etc., integrate over AO products, auxiliary fitting densities or whatever.

    * the main computational task is the evaluation one DM and 0 or more perturbations on the grid; this is naively a O(N^2 K) task. however
    there are a few interesting alternatives:
    1) use fast Gaussian transforms, which reduce the cost to O(N^2 + N*K)
    2) use a fitting basis. this wont fundamentally change the asymptotic behaviour (unless some higher trickery is imlemented), but will
       considerably cut down the evaluation costs, to O(M K) or O(M + K). of course we need the read or implement a fitting basis and then
       fit the density matrix to it, which is roughly O(N^2 M)
    3) using rank-1 perturbations, u x v, which trivially reduces the grid evaluation to O(N*K) or O(N+K), but more importantly, it would be a
       sensible way to (re)implement Joanna's algotithm as a rank-3 tensor decomposition. this would reduce the memory required for storing trial
       vectors (since they wouldn't be dense) and the computational cost of the linear algebra. naturally, this can only be done when we have a large
       number of simultaneous equations. fitting the TD is a NLSP

*/


void XCint::distribute_matrix(const int block_length,
                              const int num_variables,
                              const int num_perturbations,
                              const int mat_dim,
                              const double prefactors[],
                              const bool n_is_used[],
                              const double n[],
                              double u[],
                              double vxc[],
                              double &exc,
                              const std::vector<int> coor,
                              AOBatch     &batch,
                              const double         * grid,
                              const xc_functional_obj * xcfun,
                              double * ee
                              ) const
{
    // this again
    bool distribute_gradient;
    bool distribute_tau;

    if (fun.is_tau_mgga)
    {
        distribute_gradient = true;
        distribute_tau = true;
    }
    else if (fun.is_gga)
    {
        distribute_gradient = true;
        distribute_tau = false;
    }
    else
    {
        distribute_gradient = false;
        distribute_tau = false;
    }


    // all the 1+2+4+...+2^P possible ways of deriving the fumctopmañ
    // ??? the first half are perturbations to E and the second half are perturbations to Vxc
    const int dens_offset = (int)pow(2, num_perturbations + 1); //  fun.set_order(num_perturbations + 1, xcfun);

    double *xcin = new double[num_variables * dens_offset * block_length]; // each point can contain multiple derivatives and ffuncs can have up to 5 vars
    std::fill(&xcin[0], &xcin[num_variables * dens_offset * block_length], 0.0);

    double *xcout = new double[dens_offset * block_length]; //

    // has to be AO_BLOCK_LENGTH otherwise u can be too short
    std::fill(&u[0], &u[AO_BLOCK_LENGTH * num_variables], 0.0);


    // gather the perturbations on the same point to local format for XCFUN
    for (int k = 0; k < MAX_NUM_DENSITIES; k++)
    {
        if (!n_is_used[k]) continue;

        for (int ivar = 0; ivar < num_variables; ivar++)
            for (int ib = 0; ib < block_length; ib++)
                xcin[ ib * num_variables * dens_offset + ivar * dens_offset + k]
                  = n[k * num_variables * block_length + ivar * block_length + ib];

    }

    // this loops over the density variables of the AO product densities
    // with which the potential will be contracted
    for (int ivar = 0; ivar < num_variables; ivar++)
    {
        // sets the second half of the perturbations to 0,
        // except for the variable we are adding the contribution of

        /*
        for (int jvar = 0; jvar < num_variables; jvar++)
        {
            if (ivar == jvar)
                for (int ib = 0; ib < block_length; ib++)
                    xcin[ ib * num_variables * dens_offset  +  jvar * dens_offset  +  dens_offset/2 ] = 1.0;
            else
                for (int ib = 0; ib < block_length; ib++)
                    xcin[ ib * num_variables * dens_offset  +  jvar * dens_offset  +  dens_offset/2 ] = 0.0;
        }*/

        // set all to 0 ...
        for (int ib = 0; ib < block_length; ib++)
            for (int jvar = 0; jvar < num_variables; jvar++)
                xcin[ ib * num_variables * dens_offset  +  jvar * dens_offset  +  dens_offset/2 ] = 0.0;

        // except for ivar==jvar
        for (int ib = 0; ib < block_length; ib++)
            xcin[ ib * num_variables * dens_offset  +  ivar * dens_offset  +  dens_offset/2 ] = 1.0;


        std::fill(&xcout[0], &xcout[dens_offset * block_length], 0.0);

        for (int ib = 0; ib < block_length; ib++)
            if (n[ib] > DENSMIN)
            {
                // xceval uses NVAR * 2^(P+1) inputs,
                // depending
                xc_eval(xcfun,
                    &xcin[ib * num_variables * dens_offset],
                    &xcout[ib * dens_offset]);

                // update Vxc contribution; it is stored in the last element of the 2 * 2^P
                u[ivar * block_length + ib] = grid[4*ib + 3] * xcout[ib*dens_offset + dens_offset-1];
            }

    }

    //  time_fun_derv += rolex::stop_partial();
    //rolex::start_partial();

    // evaluate the KS matrix elements by integrating the Vxc with the AO products
    batch.distribute_matrix_undiff (
            mat_dim, distribute_gradient, distribute_tau, prefactors, u, vxc);

    // integrate Exc directly from the energy density, which is the first (0th) term of the batch
    for (int ib = 0; ib < block_length; ib++) if (n[ib] > DENSMIN) exc += grid[4*ib + 3] * xcout[ib * dens_offset];

    for (int ib = 0; ib < block_length; ib++) ee[ib] = xcout[dens_offset*ib];


    delete[] xcin;
    delete[] xcout;

    //  time_matrix_distribution += rolex::stop_partial();
}


void XCint::integrate_batch(const double dmat[],
                            const bool   get_exc,
                                  double &exc,
                            const bool   get_vxc,
                                  double vxc[],
                                  double &num_electrons,
                            const int    geo_coor[],
                            const bool   use_dmat[],
                            const int    geo_derv_order,
                            const int    max_ao_order_g,
                            const int    block_length,
                            const int    num_variables,
                            const int    num_perturbations,
                            const int    num_fields,
                            const int    mat_dim,
                            const bool   get_gradient,
                            const bool   get_tau,
                            const int    dmat_index[],
                            const double * grid,
                            const xc_functional_obj * xcfun,

                            double * nn, double * ee // debug integration
                            ) const
{
    assert(geo_derv_order < 5);


    AOBatch batch;

    double *n = new double[AO_BLOCK_LENGTH*num_variables*MAX_NUM_DENSITIES]; // 64 = 2^6
    double *u = new double[AO_BLOCK_LENGTH*num_variables];



    double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    rolex::start_partial();

    batch.get_ao(basis,
                 get_gradient,
                 max_ao_order_g,
                 block_length,
                 grid);

//  time_ao += rolex::stop_partial();

    rolex::start_partial();


    bool n_is_used[MAX_NUM_DENSITIES];
    {
        n_is_used[0] = true;
        for (int n=1; n<MAX_NUM_DENSITIES; ++n)  n_is_used[n] = false;

        for (int i=0; i<block_length*num_variables; ++i) n[i] = 0;
    }

    // obtain the density, gradients and tau of the D matrix
    batch.get_density_undiff(mat_dim,
                             get_gradient,
                             get_tau,
                             prefactors,
                             n,
                             dmat,
                             true,
                             true);


    // integrate number of electrons
    for (int ib = 0; ib < block_length; ib++) if (n[ib] > DENSMIN) num_electrons += grid[4*ib+3] * n[ib];

    for (int ib = 0; ib < block_length; ib++) nn[ib] = n[ib]; // copy density out


//  time_densities += rolex::stop_partial();

    std::vector<int> coor;

    // expectation value contribution
    if (get_exc) {
    //if (0) {

        double *xcin = NULL;
        double *xcout = NULL;




        //                       *
        //                       *  *
        //                       *  *  *  *
        //                       *  *  *  *  *  *  *  *
        //                       *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
        //           i j k l     0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
        //   ------------------------------------------------------------------
        //    0 0                0
        //    1 i    *           1  0
        //    2 j      *         2     0
        //    3 ij   * *         3  2  1  0
        //    4 k        *       4           0
        //    5 ik   *   *       5  4        1  0
        //    6 jk     * *       6     4     2     0
        //    7 ijk  * * *       7  6  5  4  3  2  1  0
        //    8 l          *     8                       0
        //    9 il   *     *     9  8                    1  0
        //   10 jl     *   *    10     8                 2     0
        //   11 ijl  * *   *    11 10  9  8              3  2  1  0
        //   12 kl       * *    12           8           4           0
        //   13 ikl  *   * *    13 12        9  8        5  4        1  0
        //   14 jkl    * * *    14    12    10     8     6     4     2     0
        //   15 ijkl * * * *    15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0

        // this takes care of the first column
        for (int k = 1; k < MAX_NUM_DENSITIES; k++)
        {
            if (use_dmat[k])
            {
                if (!n_is_used[k])
                {
                    std::fill(&n[k*block_length*num_variables], &n[(k+1)*block_length*num_variables], 0.0);
                    n_is_used[k] = true;
                }

                // evaluate the density and derivatives
                // of every matrix tha has been passed

                batch.get_density_undiff(mat_dim,
                                         get_gradient,
                                         get_tau,
                                         prefactors,
                                         &n[k*block_length*num_variables],
                                         &dmat[dmat_index[k]],
                                         false,
                                         false); // FIXME can be true based on dmat, saving possible
            }
        }

        rolex::start_partial();

#include "ave_contributions.h"

//      time_densities += rolex::stop_partial();

        const int dens_offset = (int)pow(2, num_perturbations);

        {
            size_t block_size = num_variables*dens_offset*block_length*sizeof(double);
            xcin = (double*) MemAllocator::allocate(block_size);
            std::fill(&xcin[0], &xcin[num_variables*dens_offset*block_length], 0.0);
        }
        {
            size_t block_size = dens_offset*block_length*sizeof(double);
            xcout = (double*) MemAllocator::allocate(block_size);
        }

        for (int k = 0; k < MAX_NUM_DENSITIES; k++) {
            if (!n_is_used[k]) continue;

            for (int ivar = 0; ivar < num_variables; ivar++)
                for (int ib = 0; ib < block_length; ib++)
                    xcin [ib*num_variables*dens_offset + ivar*dens_offset + k] = n[k*block_length*num_variables + ivar*block_length + ib];
        }

        rolex::start_partial();

        double sum = 0.0;
        for (int ib = 0; ib < block_length; ib++)
        {
            //if (n[ib] > 1.0e-14 and fabs(grid[(ib)*4 + 3]) > 1.0e-30)
            if (n[ib] > DENSMIN)
            {
                xc_eval(xcfun, &xcin[ib*num_variables*dens_offset], &xcout[ib*dens_offset]);
                sum += xcout[ib*dens_offset + dens_offset - 1] * grid[4*ib + 3];
            }
        }

        for (int ib = 0; ib < block_length; ib++) ee[ib] = xcout[ib]; // copy out

        exc += sum;

//      time_fun_derv += rolex::stop_partial();

        MemAllocator::deallocate(xcin);
        MemAllocator::deallocate(xcout);
    }


    // matrix contribution
    if (get_vxc) {

        if      (geo_derv_order == 0) {

            rolex::start_partial();

            // evaluate the densities (and variables) of the perturbations
            for (int ifield = 0; ifield < num_fields; ifield++)
            {
                // store the density of each perturbation on the 2^i-th density entry
                int k = (int)pow(2, ifield); // FIXME: double check this
                if (!n_is_used[k])
                {
                    std::fill(&n[k*block_length*num_variables], &n[(k+1)*block_length*num_variables], 0.0);
                    n_is_used[k] = true;
                }

                batch.get_density_undiff(mat_dim,
                                         get_gradient,
                                         get_tau,
                                         prefactors,
                                         &n[k*block_length*num_variables],
                                         &dmat[(ifield+1)*mat_dim*mat_dim],
                                         false,
                                         false); // FIXME can be true depending on perturbation (savings possible)
            }

            distribute_matrix(block_length,
                              num_variables,
                              num_perturbations,
                              mat_dim,
                              prefactors,
                              n_is_used,
                              n,
                              u,
                              vxc,
                              exc,
                              coor,
                              batch,
                              grid,
                              xcfun,
                              ee);
        }

        else {
            fprintf(stderr, "ERROR: XCint matrix contribution for geo_derv_order=%i and num_fields=%i not implemented\n", geo_derv_order, num_fields);
            exit(-1);
        }
    }


    delete[] n;
    delete[] u;
}

#include <iostream>
using namespace std;

int XCint::integrate(const xcint_mode_t         mode,
                     const int                  num_points,
                     const double             * grid,
                     const int                  num_perturbations,
                     const xcint_perturbation_t perturbations[],
                     const int                  components[],
                     const int                  num_dmat,
                     const int                  perturbation_indices[],
                     const double               dmat[],
                     const bool                 get_exc,
                           double               *exc,
                     const bool                 get_vxc,
                           double               vxc[],
                           double               *num_electrons) const
{

    assert(mode == XCINT_MODE_RKS);
    assert(num_dmat <= MAX_NUM_DENSITIES);
    assert(num_perturbations <= MAX_NUM_PERTURBATIONS);

    //int xc_input_length(xc_functional fun)
    //int xc_output_length(xc_functional fun)


    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++)
    {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0)
        {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // int dens_offset = fun.set_order(num_perturbations,   xcfun); //
    //fun.set_order(num_perturbations+1, xcfun);

    if      (get_vxc) fun.set_order(num_perturbations+1, xcfun); // enough for matrix elements derivatives
    else if (get_exc) fun.set_order(num_perturbations  , xcfun); // enough for computing the scalar value




    rolex::start_global();

    const int mat_dim = basis.get_num_ao();


    const int geo_derv_order = 0; // no geometric derivatives
    int num_fields = 0;
    for (int i=0; i<num_perturbations; i++)
    {
        if (perturbations[i] == XCINT_PERT_EL)  num_fields++;
        assert(perturbations[i] != XCINT_PERT_GEO); // should not happen
    }

    int geo_coor[MAX_NUM_PERTURBATIONS];


    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga)
        {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
        }
        // GGA
        else if (fun.is_gga)
        {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
        }
        //LDA
        else
        {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
        }
    }

//  FIXME
//  this causes the geo_off array to become
//  too small and leads to out of bounds access
//  basis.set_geo_off(max_ao_order_g);

    rolex::start_partial();

    bool use_dmat   [MAX_NUM_DENSITIES];
    int  dmat_index [MAX_NUM_DENSITIES];

    {
        for (int k=0; k<MAX_NUM_DENSITIES; k++) use_dmat[k]   = false;
        for (int k=0; k<MAX_NUM_DENSITIES; k++) dmat_index[k] = 0;

        use_dmat[0] = true;
        dmat_index[0] = 0;

        for (int k=1; k<num_dmat; k++) use_dmat[perturbation_indices[k]]   = true;
        for (int k=1; k<num_dmat; k++) dmat_index[perturbation_indices[k]] = k*mat_dim*mat_dim;
    }

    // debug
    double * nn = new double[num_points];
    double * ee = new double[num_points];
    for (int i=0; i<num_points; ++i) nn[i] = ee[i] = 0;
    // /debug

    const int num_batches = (num_points-1)/AO_BLOCK_LENGTH + 1;


    //if (get_vxc) std::fill(&vxc[0], &vxc[mat_dim*mat_dim], 0.0);
    *exc = 0;
    *num_electrons = 0;

    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    double * vxc_local[Nthreads];

    #pragma omp parallel
    {

        //for (int ithread=0; ithread<Nthreads; ++ithread) {
            const int ithread  = omp_get_thread_num();

            vxc_local[ithread] = NULL;

            if (get_vxc) {
                if (ithread==0) vxc_local[ithread] = vxc;
                else            vxc_local[ithread] = new double[mat_dim*mat_dim]; // &vxc[0];

                for (int i=0; i<mat_dim*mat_dim; ++i) vxc_local[ithread][i] = 0;
            }

            double exc_local = 0;
            double num_electrons_local = 0;


            for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {
                //if (ibatch%Nthreads!=ithread) continue;

                const int ipoint  = ibatch*AO_BLOCK_LENGTH;
                const int block_length = std::min(AO_BLOCK_LENGTH, num_points-ipoint);

                integrate_batch(dmat,
                                get_exc, //true, //
                                exc_local,
                                get_vxc,
                                vxc_local[ithread],
                                num_electrons_local,
                                geo_coor,
                                use_dmat,
                                geo_derv_order,
                                max_ao_order_g,
                                block_length,
                                num_variables,
                                num_perturbations,
                                num_fields,
                                mat_dim,
                                get_gradient,
                                get_tau,
                                dmat_index,
                                grid + 4*ipoint,
                                xcfun,
                                nn + ipoint,
                                ee + ipoint
                                );
            }

            //std::cout << ithread << " : " << exc_local << "  " <<get_exc << std::endl;

            #pragma omp atomic
            *exc           += exc_local;

            #pragma omp atomic
            *num_electrons += num_electrons_local;

        //}



        // tree-reduction;  can be done in O(N^2) instead of O(N^2 log T)
        /*
        if (get_vxc) {
            for (int ss=1; ss<Nthreads; ss*=2) {

                #pragma omp barrier

                //for (int ithread=0; ithread<Nthreads; ++ithread) {

                    if (ithread%(2*ss) == 0) {
                        if (ithread+ss < Nthreads) {
                            double * p = vxc_local[ithread];
                            double * q = vxc_local[ithread+ss];
                            for (int i=0; i<mat_dim*mat_dim; ++i) p[i] += q[i];
                        }
                    }
                //}

                #pragma omp barrier


            }
            //for (int ithread=0; ithread<Nthreads; ++ithread)
                if (ithread!=0) delete[] vxc_local[ithread];
        }
        */

    }

    if (get_vxc) {
        // don't double count the first thread !
        for (int ithread=1; ithread<Nthreads; ++ithread) {
            for (int i=0; i<mat_dim*mat_dim; ++i) vxc[i] += vxc_local[ithread][i];
        }

        // delete the matrices
        for (int ithread=1; ithread<Nthreads; ++ithread) delete[] vxc_local[ithread];
    }


    delete[] nn, ee;

    // /debug


    // compute all the density
    /*
    {
        AOBatch batch;

        double *n = new double[AO_BLOCK_LENGTH*num_variables*];
        double *u = new double[AO_BLOCK_LENGTH*num_variables];



        double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};



        batch.get_ao(basis,
                     get_gradient,
                     max_ao_order_g,
                     block_length,
                     grid);

    //  time_ao += rolex::stop_partial();

        rolex::start_partial();


        bool n_is_used[MAX_NUM_DENSITIES];
        {
            n_is_used[0] = true;
            for (int n=1; n<MAX_NUM_DENSITIES; ++n)  n_is_used[n] = false;

            for (int i=0; i<block_length*num_variables; ++i) n[i] = 0;
        }

        batch.get_density_undiff(mat_dim,
                                 get_gradient,
                                 get_tau,
                                 prefactors,
                                 n,
                                 dmat,
                                 true,
                                 true);

        // integrate number of electrons
        for (int ib = 0; ib < block_length; ib++) num_electrons += grid[(ib)*4 + 3]*n[ib];
    }
    */

    // symmetrize result matrix
    // ========================
    if (get_vxc) {
        for (int k = 0; k < mat_dim; k++) {
            for (int l = 0; l < k; l++) {
                double a = vxc[k*mat_dim + l] + vxc[l*mat_dim + k];
                vxc[k*mat_dim + l] = 0.5*a;
                vxc[l*mat_dim + k] = 0.5*a;
            }
        }
    }


//  time_total += rolex::stop_global();
    xc_free_functional(xcfun);
    return 0;
}


int XCint::integrate2 (
                       const gpoint             * grid,
                       const int                  num_points,
                       const tensor2 &            dmat,
                             tensor2 &            vxc,
                             double             & exc,
                             double             & num_electrons) const
{

    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
    }

    //const int xc_in_len = xc_input_length(xcfun);
    //const int xc_out_len = xc_output_length(xcfun);
    //cout << "xc_in_len = " << xc_in_len << "  xc_out_len = " << xc_out_len << endl;


    const int num_batches = (num_points-1)/AO_BLOCK_LENGTH + 1;


    //if (get_vxc) std::fill(&vxc[0], &vxc[mat_dim*mat_dim], 0.0);
    exc = 0;
    num_electrons = 0;

    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    tensor2 * vxc_local[Nthreads];

    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        vxc_local[ithread] = NULL;

        if (ithread==0) vxc_local[ithread] = &vxc;
        else            vxc_local[ithread] = new tensor2(vxc.n);

        vxc_local[ithread]->zeroize();

        double exc_local = 0;
        double num_electrons_local = 0;

        double *n = new double [ num_variables * AO_BLOCK_LENGTH];
        double *u = new double [ num_variables * AO_BLOCK_LENGTH];

        AOBatch batch;

        for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const int block_length = std::min(AO_BLOCK_LENGTH, num_points-ipoint);
            const gpoint * sgrid  = (grid + ipoint);


            std::fill(&u[0], &u[AO_BLOCK_LENGTH * num_variables], 0.0);
            std::fill(&n[0], &n[AO_BLOCK_LENGTH * num_variables], 0.0);



            // evaluate AOs (and their gradients if needed) on a chunk of the grid points
            batch.get_ao(basis,
                         get_gradient,
                         max_ao_order_g,
                         AO_BLOCK_LENGTH,
                         (const double*) sgrid);


            // obtain the density, gradients and tau of the D matrix
            batch.get_density_undiff(dmat.n,
                                     get_gradient,
                                     get_tau,
                                     prefactors,
                                     n,
                                     dmat[0],
                                     true,
                                     true);

            // integrate number of electrons
            for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
                //if (n[ib] > DENSMIN)
                num_electrons_local += sgrid[ib].w * n[ib];

            for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
              //if (n[ib] > DENSMIN)
              {

                double xcin[num_variables], xcout[1+num_variables];

                 // has to be AO_BLOCK_LENGTH otherwise u can be too short
                for (int ivar=0; ivar< num_variables; ++ivar) xcin [ivar] = n[ivar*AO_BLOCK_LENGTH+ib]; // AO_BLOCK_LENGTH ?

                xc_eval(xcfun, xcin, xcout);

                // xcout[0] == Exc[p(r)]
                exc_local += sgrid[ib].w * xcout[0];

                // update Vxc contribution; it is stored in the last element of the 2 * 2^PS
                for (int ivar=0; ivar<num_variables; ++ivar)
                    u[ivar*AO_BLOCK_LENGTH + ib] = sgrid[ib].w * xcout[1+ivar];
              }

            batch.distribute_matrix_undiff (dmat.n, get_gradient, get_tau, prefactors, u, vxc_local[ithread]->c[0]);
        }

        delete[] n;
        delete[] u;


        #pragma omp atomic
        exc           += exc_local;

        #pragma omp atomic
        num_electrons += num_electrons_local;
    }

    // don't double count the first thread !
    for (int ithread=1; ithread<Nthreads; ++ithread) vxc += *vxc_local[ithread];
    // delete the matrices
    for (int ithread=1; ithread<Nthreads; ++ithread) delete vxc_local[ithread];

    vxc.Symmetrize(); vxc *= 0.5;


    xc_free_functional(xcfun);

    return 0;
}

int XCint::integrate2 (
                       const gpoint             * grid,
                       const int                  num_points,
                       const tensor2 &            dmat,
                       const tensor2 &            pmat,
                             tensor2 &            vxc) const
{

    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
    }

    //const int xc_in_len = xc_input_length(xcfun);
    //const int xc_out_len = xc_output_length(xcfun);
    //cout << "xc_in_len = " << xc_in_len << "  xc_out_len = " << xc_out_len << endl;



    const int num_batches = (num_points-1)/AO_BLOCK_LENGTH + 1;


    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    tensor2 * vxc_local[Nthreads];

    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        vxc_local[ithread] = NULL;

        if (ithread==0) vxc_local[ithread] = &vxc;
        else            vxc_local[ithread] = new tensor2(vxc.n);

        vxc_local[ithread]->zeroize();


        double *n = new double [ num_variables * AO_BLOCK_LENGTH];
        double *p = new double [ num_variables * AO_BLOCK_LENGTH];
        double *u = new double [ num_variables * AO_BLOCK_LENGTH];

        AOBatch batch;

        for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const int block_length = std::min(AO_BLOCK_LENGTH, num_points-ipoint);
            const gpoint * sgrid  = (grid + ipoint);


            std::fill(&u[0], &u[AO_BLOCK_LENGTH * num_variables], 0.0);
            std::fill(&p[0], &p[AO_BLOCK_LENGTH * num_variables], 0.0);
            std::fill(&n[0], &n[AO_BLOCK_LENGTH * num_variables], 0.0);



            // evaluate AOs (and their gradients if needed) on a chunk of the grid points
            batch.get_ao(basis,
                         get_gradient,
                         max_ao_order_g,
                         block_length,
                         (const double*) sgrid);


            // obtain the density, gradients and tau of the D matrix
            batch.get_density_undiff(dmat.n,
                                     get_gradient,
                                     get_tau,
                                     prefactors,
                                     n,
                                     dmat[0],
                                     true,
                                     true);

            // obtain the perturbed density, gradients and tau of the D matrix
            batch.get_density_undiff(pmat.n,
                                     get_gradient,
                                     get_tau,
                                     prefactors,
                                     p,
                                     pmat[0],
                                     true,
                                     true);

            // integrate number of electrons
            //for (int ib = 0; ib < block_length; ib++) num_electrons_local += sgrid[ib].w * n[ib];
            for (int ib = 0; ib < block_length; ib++)
              if (n[ib] > DENSMIN)
              {

                double xcin[num_variables], xcout[1+num_variables+(num_variables*num_variables+num_variables)/2];

                 // has to be AO_BLOCK_LENGTH otherwise u can be too short
                for (int ivar=0; ivar< num_variables; ++ivar) xcin [ivar] = n[ivar*block_length+ib]; // AO_BLOCK_LENGTH ?

                xc_eval(xcfun, xcin, xcout);

                double D2[num_variables][num_variables]; {
                    int k = 1+num_variables; // index; skip energy and first derivatives

                    // unpack second derivatives:
                    for (int n=0; n<num_variables; ++n)
                        for (int m=n; m<num_variables; ++m) {
                            D2[m][n] = D2[n][m] = xcout[k];
                            ++k;
                        }
                }

                // integrate perturbation
                for (int ivar=0; ivar<num_variables; ++ivar) {

                    double p1 = 0;

                    for (int jvar=0; jvar<num_variables; ++jvar)
                        p1 += D2[ivar][jvar] * p[jvar*block_length+ib];

                    u[ivar*AO_BLOCK_LENGTH + ib] = sgrid[ib].w * p1;
                }

              }

            batch.distribute_matrix_undiff (dmat.n, get_gradient, get_tau, prefactors, u, vxc_local[ithread]->c[0]);
        }

        delete[] n;
        delete[] p;
        delete[] u;
    }

    // don't double count the first thread !
    for (int ithread=1; ithread<Nthreads; ++ithread) vxc += *vxc_local[ithread];
    // delete the matrices
    for (int ithread=1; ithread<Nthreads; ++ithread) delete vxc_local[ithread];

    vxc.Symmetrize(); vxc *= 0.5;


    xc_free_functional(xcfun);

    return 0;
}



/*
    evaluate on the grid some density matrix or perturbation
*/

int XCint::get_density (
                       const gpoint             * grid,
                       const int                  num_points,

                       const tensor2 &            dmat,
                             GridFunction       * dens,
                             double             & num_electrons) const
{

    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
        }
    }

    dens->Set(num_variables, num_points);


    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    num_electrons = 0;

    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        AOBatch batch;

        double ne_local = 0;

        for (int ibatch = ithread; ibatch < dens->Nblocks; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const gpoint * sgrid  = (grid + ipoint);

            //double * dd = d + ipoint*num_variables;

            // evaluate AOs (and their gradients if needed)
            // on a chunk of the grid points;
            //
            // since the array is padded to the next 4*128 doubles, and their values set to 0,
            // we can use AO_BLOCK_LENGTH rather than the number of points in the remainder (Npoints - ibatch*AO_BLOCK_LENGTH) % AO...
            batch.get_ao(basis,
                         get_gradient,
                         max_ao_order_g,
                         AO_BLOCK_LENGTH,
                         (const double*) sgrid);

            double * n = dens->f[ibatch*num_variables].v;

            // obtain the density, gradients and tau of the D matrix
            batch.get_density_undiff(dmat.n,
                                     get_gradient,
                                     get_tau,
                                     prefactors,
                                     n,
                                     dmat[0],
                                     true,
                                     true);

            // integrate the density
            for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
                ne_local += sgrid[ib].w * n[ib];
        }

        #pragma omp atomic
        num_electrons += ne_local;
    }

    return 0;
}

// triangular, pyramidal, etc. numbers for unpacking arrays
// Tn(i)
// i.e. 1, i, (i+1)*i/2, (i+2)*(i+1)*i/6, etc.
inline int Tnum(int n, int i) {
    if (n<0) return 0;
    if (n==0) return 1;
    else return (Tnum(n-1,i+1)*i)/n;
}

int XCint::xc_potential (
                       const GridFunction       * dens,
                             GridFunction       * pot,
                             int order) const
{

    if (order<0 || order>3) return -1;

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, order);
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, order);
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, order);
        }
    }

    // number of vars in, number of vars out, & shift used in XCFUN to get to the relevant derivative block
    // these are triangular numbers
    int nvin, nvout, nvlow; {
        nvin   = num_variables;
        nvout  = Tnum(order,num_variables); // number of order-n packed derivatives
        nvlow  = 0;                         // lower-order derivatives

        // 1 + nv + (nv+1)*nv/2 + ...
        for (int i=0; i<order; ++i)
            nvlow += Tnum(i, num_variables);
    }

    const int num_batches = dens->Nblocks;

    pot->Set(nvout, num_batches*AO_BLOCK_LENGTH);


    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        for (int ibatch = ithread; ibatch < num_batches; ibatch += Nthreads) {

            for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
              if (dens->f[ibatch*nvin] [ib] > DENSMIN) {

                double xcin  [nvin];
                double xcout [nvlow+nvout];

                for (int i = 0; i < nvin; ++i)
                    xcin[i] = dens->f[ibatch*nvin + i] [ib];

                xc_eval (xcfun, xcin, xcout);

                for (int i = 0; i < nvout; ++i)
                    pot->f[ibatch*nvout + i] [ib] = xcout[nvlow + i];
            }
        }
    }

    xc_free_functional(xcfun);

    return 0;
}


int XCint::scf_integrate (
                       const gpoint             * grid,
                       const int                  num_points,
                       const GridFunction       * pot,
                             tensor2 &            vxc) const {

    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
    }

    const int num_batches = pot->Nblocks;

    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    tensor2 * vxc_local[Nthreads];

    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        vxc_local[ithread] = NULL;

        if (ithread==0) vxc_local[ithread] = &vxc;
        else            vxc_local[ithread] = new tensor2(vxc.n);

        vxc_local[ithread]->zeroize();


        double *u = new double [ num_variables * AO_BLOCK_LENGTH];

        AOBatch batch;

        for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const gpoint * sgrid  = (grid + ipoint);

            std::fill(&u[0], &u[AO_BLOCK_LENGTH * num_variables], 0.0);

            // evaluate AOs (and their gradients if needed) on a chunk of the grid points
            batch.get_ao(basis,
                         get_gradient,
                         max_ao_order_g,
                         AO_BLOCK_LENGTH,
                         (const double*) sgrid);

            // copy the potential and its derivatives, weighted with the grid
            for (int ivar=0; ivar<num_variables; ++ivar)
                for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
                    u[ivar*AO_BLOCK_LENGTH + ib] = sgrid[ib].w * pot->f[ibatch*num_variables + ivar] [ib];

            batch.distribute_matrix_undiff (vxc.n, get_gradient, get_tau, prefactors, u, vxc_local[ithread]->c[0]);
        }

        delete[] u;
    }

    // don't double count the first thread !
    for (int ithread=1; ithread<Nthreads; ++ithread) vxc += *vxc_local[ithread];
    // delete the matrices
    for (int ithread=1; ithread<Nthreads; ++ithread) delete vxc_local[ithread];

    vxc.Symmetrize(); vxc *= 0.5;

    xc_free_functional(xcfun);

    return 0;
}

int XCint::scf_integrate (
                       const gpoint             * grid,
                       const int                  num_points,
                       const GridFunction       * pot,
                             double             & exc) const {


    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, 1); // compute first functional derivative, for Vxc
        }
    }

    const int num_batches = pot->Nblocks;

    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }

    exc = 0;

    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        double exc_local = 0;

        for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const gpoint * sgrid  = (grid + ipoint);

            double exc_batch = 0;

            for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
                exc_batch += sgrid[ib].w * pot->f[ibatch] [ib];
            exc_local += exc_batch;
        }

        #pragma omp atomic
        exc += exc_local;
    }

    xc_free_functional(xcfun);

    return 0;
}


// =================================
// OPTIMIZED ROUTINES FOR LINEAR RSP
// =================================


// integrate a bunch of perturbation matrices
int XCint::rsp_integrate (
                       const gpoint             * grid,
                       const int                  num_points,

                       const GridFunction       * xc,
                       const tensor2 *            pmat,

                             int                  Nmat,
                             tensor2 *            vxc
                                                ) const
{

    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
    }

    const int num_batches = (num_points-1)/AO_BLOCK_LENGTH + 1;

    //const int n2var = num_variables*num_variables;
    const int n2var = (num_variables*num_variables+num_variables)/2;


    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }


    // this is a disaster of a parallelization strategy, and needs to be re-thought to some degree


    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        tensor2 vxc_local(pmat[0].n);


        //double *p = new double [ num_variables * AO_BLOCK_LENGTH];
        //double *u = new double [ num_variables * AO_BLOCK_LENGTH];

        Block u[num_variables], p[num_variables];


        AOBatch batch;

        for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const gpoint * sgrid  = (grid + ipoint);

            // evaluate AOs (and their gradients if needed) on a chunk of the grid points
            batch.get_ao(basis,
                         get_gradient,
                         max_ao_order_g,
                         AO_BLOCK_LENGTH,
                         (const double*) sgrid);

            for (int n=0; n<Nmat; ++n) {

                //std::fill(&u[0], &u[AO_BLOCK_LENGTH * num_variables], 0.0);
                //std::fill(&p[0], &p[AO_BLOCK_LENGTH * num_variables], 0.0);
                memset ((void*)u[0].v, 0, num_variables*sizeof(Block));
                memset ((void*)p[0].v, 0, num_variables*sizeof(Block));


                // obtain the perturbed density, gradients and tau of the D matrix
                batch.get_density_undiff(pmat[n].n,
                                         get_gradient,
                                         get_tau,
                                         prefactors,
                                         &p[0][0],
                                         pmat[n] [0],  // n-th perturbation
                                         true,
                                         true);


                // integrate perturbation
                for (int ivar=0; ivar<num_variables; ++ivar) {
                    for (int jvar=0; jvar<num_variables; ++jvar) {
                        //int ij = ivar * num_variables + jvar;

                        const int n = std::min(ivar,jvar);
                        const int m = std::max(ivar,jvar);

                        const int nm = (n * num_variables + m) - n*(n+1)/2; // unpacks the triangular layout:    00 01 02 03 11 12 13 22 23 33

                        for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
                            //u[ivar][ib] += p[jvar][ib] * xc->f[ibatch*n2var + ij] [ib];
                            u[ivar][ib] += p[jvar][ib] * xc->f[ibatch*n2var + nm] [ib];
                    }
                }


                // this is performed when evaluating the potential
                /*
                for (int ivar=0; ivar<num_variables; ++ivar)
                    for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
                        u[ivar][ib] *= sgrid[ib].w;
                */

                //
                // vxcneeds to be at least partially local if we want to parametrize it
                //

                vxc_local.zeroize();

                batch.distribute_matrix_undiff (pmat[0].n, get_gradient, get_tau, prefactors, &u[0][0], vxc_local[0] );

                // maybe we should randomize the order of n for each thread
                // and use Nmat locks to speed things up

                #pragma omp critical
                {
                    vxc[n] += vxc_local;
                }
            }

        }

        //delete[] p;
        //delete[] u;
    }


    for (int n=0; n<Nmat; ++n) {vxc[n].Symmetrize(); vxc[n] *= 0.5;}



    xc_free_functional(xcfun);

    return 0;
}



// compute the full exchange vector for RSP (once)
int XCint::xc_potential (
                       const gpoint             * grid,
                       const int                  num_points,

                       const tensor2 &            dmat,
                             GridFunction       * xc) const
{

    const double prefactors[5] = {1.0, 2.0, 2.0, 2.0, 0.5};

    xc_functional_obj * xcfun = xc_new_functional();

    for (int i = 0; i < fun.keys.size(); i++) {
        int ierr = xc_set(xcfun, fun.keys[i].c_str(), fun.weights[i]);
        if (ierr != 0) {
            fprintf(stderr, "ERROR in fun init: \"%s\" not recognized, quitting.\n", fun.keys[i].c_str());
            exit(-1);
        }
    }

    // find which contributions are needed
    bool get_gradient,   get_tau;
    int max_ao_order_g, num_variables;

    {
        // m-GGA
        if (fun.is_tau_mgga) {
            num_variables = 5; // density, density gradient, tau
            get_gradient = true;
            get_tau = true;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ_TAUN, XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
        // GGA
        else if (fun.is_gga) {
            num_variables = 4; // density, density gradient
            get_gradient = true;
            get_tau = false;
            max_ao_order_g = 1;
            xc_eval_setup(xcfun, XC_N_NX_NY_NZ,      XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
        //LDA
        else {
            num_variables = 1; // just the density
            get_gradient = false;
            get_tau = false;
            max_ao_order_g = 0;
            xc_eval_setup(xcfun, XC_N,               XC_PARTIAL_DERIVATIVES, 2); // second functional derivatives for TDDFT
        }
    }

    const int num_batches = (num_points-1)/AO_BLOCK_LENGTH + 1;

    // initialize to 0
    //Cconst int num_variables2 = num_variables*num_variables;
    const int num_variables2 = (num_variables*num_variables+num_variables)/2;


    // initialize the memory
    xc->Set(num_variables2, num_points); // this pads to the next AO_LENGTH * num_variables2


    int Nthreads; {
        #pragma omp parallel
        if (omp_get_thread_num()==0) Nthreads = omp_get_num_threads();
    }


    #pragma omp parallel
    {
        const int ithread  = omp_get_thread_num();

        double *n = new double [ num_variables * AO_BLOCK_LENGTH];


        AOBatch batch;

        for (int ibatch = ithread; ibatch < num_batches; ibatch+=Nthreads) {

            const int ipoint  = ibatch*AO_BLOCK_LENGTH;
            const gpoint * sgrid  = (grid + ipoint);

            std::fill(&n[0], &n[AO_BLOCK_LENGTH * num_variables], 0.0);



            // evaluate AOs (and their gradients if needed) on a chunk of the grid points;
            //
            batch.get_ao(basis,
                         get_gradient,
                         max_ao_order_g,
                         AO_BLOCK_LENGTH,
                         (const double*) sgrid);


            // obtain the density, gradients and tau of the D matrix
            // =====================================================
            batch.get_density_undiff(dmat.n,
                                     get_gradient,
                                     get_tau,
                                     prefactors,
                                     n,
                                     dmat[0],
                                     true,
                                     true);

            // evaluate the XC derivatives of the density
            // ==========================================
            for (int ib = 0; ib < AO_BLOCK_LENGTH; ib++)
              if (n[ib] > DENSMIN)
              {

                double xcin[num_variables], xcout[1+num_variables+(num_variables*num_variables+num_variables)/2];

                 // has to be AO_BLOCK_LENGTH otherwise u can be too short
                for (int ivar=0; ivar< num_variables; ++ivar)
                    xcin [ivar] = n[ivar*AO_BLOCK_LENGTH + ib]; // AO_BLOCK_LENGTH ?


                xc_eval(xcfun, xcin, xcout);

                // store second order derivatives
                // isn't this a bit wasteful?
                /*
                for (int ivar=0; ivar<num_variables; ++ivar) {
                    for (int jvar=0; jvar<num_variables; ++jvar) {

                        int ij  = ivar * num_variables + jvar; // redundant for easy access

                        int p = std::min(ivar,jvar);
                        int q = std::max(ivar,jvar);

                        int pq = (p * num_variables + q) - p*(p+1)/2; // unpacks the triangular layout:    00 01 02 03 11 12 13 22 23 33

                        xc->f[ibatch*num_variables2 + ij] [ib] = sgrid[ib].w * xcout[1+num_variables + pq]; // scale the potential with the grid weights
                    }
                }
                */

                for (int ijvar=0; ijvar<num_variables2; ++ijvar) {
                    xc->f[ibatch*num_variables2 + ijvar] [ib] = sgrid[ib].w * xcout[1+num_variables + ijvar]; // scale the potential with the grid weights
                }
              }
        }

        delete[] n;
    }

    xc_free_functional(xcfun);

    return 0;
}





// ======================================================
// CODE FOR A FUTURE IMPLEMENTATION OF AO GRID EVALUATION
// ======================================================

/*

static inline double ipow(double v, int i) {

    if (i<0) {cout << "error in ipow; called with i="<<i << endl; return 1;}
    if (i==0) return 1;

    // skip all zeros
    while (i%2==0) {
        v*=v;
        i/=2;
    }

    // first bit
    double r=v;
    i/=2;

    // do rest
    while (i) {
        v*=v;
        if (i%2) r*=v;
        i/=2;
    }
    return r;
}


void evaluateGTO (double * fr, const GTO & gto, const double * r, int N) {

    for (int n=0; n<N; ++n) {
        double r2 = r[n]*r[n];

        double gauss[maxK];

        for (int k=0; k<gto.K; ++k) gauss[k] = exp(-gto.k[k] * r2);

        double radial[maxJ];

        for (int j=0; j<gto.J; ++j) {
            radial[j] = 0;
            for (int k=0; k<gto.K; ++k) radial[j] += gto.N[j][k] * gauss[k];
            radial[j] *= ipow(r, gto.l);
        }

        for (int j=0; j<gto.J; ++j) fr[n*gto.J + j] = radial[j];
    }

}

// this is equal for all the functions of the same angular momentum centered on the same atom
void evaluateL (double * fm, const int L, const vector3 * v, int N) {

    //for (int n=0; n<N; ++n)
    {

        double pown[3][L+1];

        pown[0][0] = pown[1][0] = pown[2][0] = 1.;
        pown[0][1] = v.x;
        pown[1][1] = v.y;
        pown[2][1] = v.z;

        for (int l=2; l<=L; ++l) {
            pown[0][l] = pown[0][l-1] * v.x;
            pown[1][l] = pown[1][l-1] * v.y;
            pown[2][l] = pown[2][l-1] * v.z;
        }

        // add all the terms of the solid spherical harmonic
        double sph[2*L+1];

        for (int M=0; M<2*L+1; ++M) {
            sph[M] = 0;
            for (int n=0; n<SHList[L][M].nps; ++n) {
                const SHTerm & T = SHList[L][M].T[n];
                sph[M] += T.cN  * pown[0][T.nx] * pown[1][T.ny] * pown[2][T.nz];
            }
        }

        for (int M=0; M<2*L+1; ++M) fm[M] = sph[M];
    }

}

void evaluateAll() {

    // loop over all elements
        // loop over all atoms of the same element
            // evaluateL for all the relevant Ls  & grid points
            // loop over all relevant Ls
                // loop over all elemental GTO shells of  L= L
                    // evaluateR for all relevant grid points


    // it's easy to precompute all atom cemtre - grid point distances and Ylm's
    // then compute the spherical harmonics and the radial parts independently
    // now we can evaluate product densities on the grid easier, since (La Lb | Ck) are independent of the radial functions, and (fa fb | Ck) can be used for the entire La Lb shell pair


}

*/



