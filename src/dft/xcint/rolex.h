#ifndef rolex_h_
#define rolex_h_

namespace rolex
{
    void start_global();

    double stop_global();

    void start_partial();

    double stop_partial();
}

#endif // rolex_h_
