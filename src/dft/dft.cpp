
#include "linear/tensors.hpp"

#include "low/MPIwrap.hpp"
#include "libechidna/libechidna.hpp"
#include "basis/GTO.hpp"

#include "xcgrid/xcgrid.hpp"
#include "xcint/integrator.h"
#include "dft.hpp"

#include "fiesta/fiesta.hpp"
using namespace Fiesta;


/*

    DFTgrid: this should be renamed and probably merged somehow with XCint, etc.

*/



DFTgrid::DFTgrid() {

    //dft_grid       = NULL;
    //dft_integrator = NULL;

    dft_grid       = new XCgrid; // numgrid_new_context();
    dft_integrator = new XCint;  // xcint_new_context();

    // set some default parameters
    // taken from test; i assume this to be reasonable Lebedeev quadratures
    radial_precision = 1.0e-15;
    max_num_angular_points = 590;
    min_num_angular_points = 146;
    funcname = "";
    // 6 14 26 38 50 74 86 110 146 170 194 230 266 302 350 434 590 770 974 1202 1454 1730 2030 2354

    xs_w = 1; // not initialized by default, so full HF exchange
    mu_w = alpha_w = beta_w = 0;

    use_dft         = false; // don't do anything unless it0s initialized
    init_grid       = false;
    global_hybrid   = false;
    range_separated = false;


    gs_dens      = NULL;
}

DFTgrid::~DFTgrid() {

    delete dft_grid;       //numgrid_free_context (dft_grid);
    delete dft_integrator; //xcint_free_context   (dft_integrator);

    if (init_grid) {

        delete[] center_coordinates;
        delete[] center_elements;

        delete[] outer_center_coordinates;
        delete[] outer_center_elements;

        delete[] shell_centers;
        delete[] shell_l_quantum_numbers;
        delete[] shell_num_primitives;

        delete[] primitive_exponents;
        delete[] contraction_coefficients;
    }

    delete gs_dens;
}

// the complete list of functionals and their parameters are set
// in Functional::parse() in /external/xcint/src/Functional.cpp

int DFTgrid::SetFunctional (const std::string & name) {

    funcname = name + "\0";

    // reset everything
    xs_w = mu_w = alpha_w = beta_w = 0;
    global_hybrid = range_separated = false;

    // specially-defined functional
    if (name=="CAMB3LYP2") funcname = "CAMB3LYP cam_beta=0.810\0"; // otherwise XCINT will not recognize it


    // set the parameters in XCINT / XCFUN
    int err = dft_integrator->set_functional (funcname.c_str());
    if (err) {
        Messenger << "DFT : XCINT : xcint_set_functional error; errcode = "  << err << endl;
        return -1;
    }

    // default values regardless of functional : cam_alpha=0.190 cam_beta=0.460 rangesep_mu=0.330
    err = dft_integrator->get_hf_parms (xs_w, alpha_w, beta_w, mu_w);

    if (xs_w!=0.) global_hybrid = true;
    if (name.substr(0,8)=="CAMB3LYP") range_separated = true;

    use_dft = true;

    return 0;
}

int DFTgrid::SetWthresh (double precision) {
    if (precision<0. || precision>1.) return -1;
    wthresh = precision;
    return 0;
}

int DFTgrid::SetRthresh (double precision) {
    if (precision<0. || precision>1.) return -1;
    radial_precision = precision;
    return 0;
}

int DFTgrid::SetMinL (int npoints) {
    if (npoints<6) return -1;
    min_num_angular_points = npoints;
    return 0;
}

int DFTgrid::SetMaxL (int npoints) {
    if (npoints<6) return -1;
    max_num_angular_points = npoints;
    return 0;
}


void DFTgrid::Init (const rAtom * Atoms, int nnuclei, const GTObasis & BasisSet) {

    Messenger << "DFT grid: initialization" << endl;

    // taken from the molecule
    num_centers = nnuclei;
    center_coordinates = new double[3*nnuclei];
    center_elements    = new int     [nnuclei];

    for (int n=0; n<nnuclei; ++n) {
        center_coordinates[3*n]   = Atoms[n].c.x;
        center_coordinates[3*n+1] = Atoms[n].c.y;
        center_coordinates[3*n+2] = Atoms[n].c.z;
        center_elements   [n]     = Atoms[n].rAP->Z;
    }

    // these are empty
    num_outer_centers = 0;
    outer_center_coordinates = NULL;
    outer_center_elements    = NULL;

    // count number of shells
    {
        num_shells=0;
        for (int n=0; n<nnuclei; ++n) {
            // num_shells += Atoms[n].rAP->basis.N(); // add the number of shells for every atom

            // count every generally contracted shell independently
            const int NS = Atoms[n].rAP->basis.N();
            for (int s=0; s<NS; ++s) {
                num_shells += Atoms[n].rAP->basis[s].J;
            }
        }
    }

    shell_centers           = new int[num_shells];
    shell_l_quantum_numbers = new int[num_shells];
    shell_num_primitives    = new int[num_shells];

    {
        int c=0;
        for (int n=0; n<nnuclei; ++n) {
            /*
            int nshells = Atoms[n].rAP->basis.N();
            for (int s=0; s<nshells; ++s) shell_centers[c+s] = n+1; // all shells belong to the same atom
            for (int s=0; s<nshells; ++s) shell_l_quantum_numbers[c+s] = Atoms[n].rAP->basis[s].l; // extract angular momentum
            for (int s=0; s<nshells; ++s) shell_num_primitives[c+s]    = Atoms[n].rAP->basis[s].K; // extract contraction degree
            c += nshells; // add the number of shells for every atom
            */

            // we must duplicate some of the data in order to use
            // general contraction basis sets
            int sj=0;

            const int NS = Atoms[n].rAP->basis.N();
            for (int s=0; s<NS; ++s) {

                const int NJ = Atoms[n].rAP->basis[s].J;
                for (int j=0; j<NJ; ++j) {
                    shell_centers[c+sj] = n+1; // all shells belong to the same atom
                    shell_l_quantum_numbers[c+sj] = Atoms[n].rAP->basis[s].l; // extract angular momentum
                    shell_num_primitives[c+sj]    = Atoms[n].rAP->basis[s].K; // extract contraction degree
                    ++sj;
                }
            }

            c+= sj;
        }
    }

    // now count primitives and copy exponents
    num_primitives;
    {
        num_primitives=0;
        for (int s=0; s<num_shells; ++s) num_primitives += shell_num_primitives[s];
    }

    // angular normalization (tested!)
    const double NN[] = { sqrt(1./(4*PI)), sqrt(3./(4*PI)), sqrt(15./(4*PI)), sqrt(105./(4*PI)), sqrt(945./(4*PI))};

    primitive_exponents      = new double[num_primitives];
    contraction_coefficients = new double[num_primitives];

    {
        int t=0; // total counter
        for (int n=0; n<nnuclei; ++n) {
            int nshells = Atoms[n].rAP->basis.N();

            for (int s=0; s<nshells; ++s) {
                const int K = Atoms[n].rAP->basis[s].K;
                const int L = Atoms[n].rAP->basis[s].l;
                const int J = Atoms[n].rAP->basis[s].J;

                // loop over generally contracted functions
                for (int j=0; j<J; ++j) {
                    // loop over contraction degree
                    for (int k=0; k<K; ++k) {
                        primitive_exponents[t]      = Atoms[n].rAP->basis[s].k[k];
                        contraction_coefficients[t] = Atoms[n].rAP->basis[s].N[j][k] * NN[L];
                        ++t;
                    }
                }

            }
        }
    }


    // GENERATE GRID
    // =============


    int err;

    Messenger.Push();

    Messenger << "DFT grid : generating grid" << endl;


    // call the grid generation routine
    // ================================
    //err = numgrid_generate_grid  (dft_grid,

    err = dft_grid->generate(
                              radial_precision,
                              min_num_angular_points,
                              max_num_angular_points,
                              num_centers,
                              center_coordinates,
                              center_elements,
                              num_outer_centers,
                              outer_center_coordinates,
                              outer_center_elements,
                              num_shells,
                              shell_centers,
                              shell_l_quantum_numbers,
                              shell_num_primitives,
                              primitive_exponents,
                              wthresh
                              );

    if (err) Messenger << "Error in numgrid; errcode = "  << err << endl;



    Npoints         = dft_grid->numgrid_get_num_points(); // numgrid_get_num_points (dft_grid);
    grid            = dft_grid->numgrid_get_grid();       // (dft_grid);
    Messenger << "DFT : grid points = " << Npoints << endl;


    if (0) {
        string filepath = "grid.xyz";
        fstream ffile(filepath.c_str(), ios::out);

        ffile.precision(16);
        ffile.setf(ios::fixed);

        for (int n=0; n<Npoints; ++n) {
            ffile << grid[4*n+3] << "     " << grid[4*n+0] << "   " << grid[4*n+1] << "   " << grid[4*n+2] <<  endl;
        }

        ffile.close();
    }


    err = dft_integrator->set_basis(
                              XCINT_BASIS_SPHERICAL,
                              num_centers,
                              center_coordinates,
                              num_shells,
                              shell_centers,
                              shell_l_quantum_numbers,
                              shell_num_primitives,
                              primitive_exponents,
                              contraction_coefficients);

    if (err) Messenger << "DFT grid : xcint set basis error; errcode = "  << err << endl;

    //
    init_grid = true;

    Messenger.Pop();
    Messenger << endl;
}

double  DFTgrid::CalcXCscf (tensor2 & KS, const tensor2 & D2) const {

    if (!use_dft) {
        Messenger << "DFT grid : call to integrator but no functional has been set" << endl;
        return 0;
    }

    if (!init_grid) {
        Messenger << "DFT grid : call to integrator but grid was not initialized!" << endl;
        return 0;
    }


    tensor2 D = D2;
    tensor2 V (KS.n);

    D.Symmetrize(); D*= 0.5; // just in case


    // input for XCint
    const double * dmat = D.c2;
          double * vxc  = V.c2;

    // output
    double nelec=0, xcenergy=0;

    int err = dft_integrator->set_functional (funcname.c_str());
    if (err) Messenger << "DFT : XCINT : xcint_set_functional error; errcode = "  << err << endl;

    // compute XC energy and KS matrix elements
    if (1) {
        err = dft_integrator->integrate2 (  (gpoint*)grid, Npoints,   D, V,   xcenergy, nelec  );
    }
    else {
        GridFunction dens, pot0, pot1;

        dft_integrator->get_density ( (gpoint*)grid, Npoints, D, &dens, nelec);

        dft_integrator->xc_potential ( &dens, &pot0, 0);

        dft_integrator->scf_integrate( (gpoint*)grid, Npoints, &pot0, xcenergy);

        dft_integrator->xc_potential ( &dens, &pot1, 1);

        dft_integrator->scf_integrate( (gpoint*)grid, Npoints, &pot1, V);
    }


    if (NodeGroup.GetTotalNodes()>1) {

        //Messenger << "DFT : collecting node contributions " << endl;

        //Messenger.Push(); {
            NodeGroup.AllReduce (&xcenergy, 1);
            NodeGroup.AllReduce (&nelec  ,  1);

            Tensor2Reduce(V);
        //} Messenger.Pop();
    }

    KS += V;


    if (err) Messenger << "DFT : XCINT : xcint_integrate_scf error; errcode = "  << err << endl;

    Messenger << "xcint N_electrons : " << nelec << endl;
    Messenger << "xcint XC energy   : " << xcenergy << endl;

    return xcenergy;
}

void  DFTgrid::CalcXCrsp (tensor2 * KS, const tensor2 * D2, int N, const tensor2 & D0) {

    if (!use_dft) {
        Messenger << "DFT grid : call to integrator but no functional has been set" << endl;
        //return 0;
    }

    if (!init_grid) {
        Messenger << "DFT grid : call to integrator but grid was not initialized!" << endl;
        //return 0;
    }

    // we must symmetrize the input and outpùt
    // we must reduce not on top of the previous results


    int err = dft_integrator->set_functional (funcname.c_str());
    if (err) Messenger << "DFT : XCINT : xcint_set_functional error; errcode = "  << err << endl;


    if (gs_dens==NULL) {
        gs_dens = new GridFunction;
        dft_integrator->xc_potential  ( (gpoint*)grid, Npoints, D0, gs_dens);
    }


    tensor2 Vxc(D0.n), AD(D0.n);
    for (int n=0; n<N; ++n) {
        AD=D2[n];
        AD.Symmetrize();
        AD *= 0.5; // just in case
        Vxc.zeroize();

        dft_integrator->rsp_integrate ( (gpoint*)grid, Npoints, gs_dens,  &AD, 1,   &Vxc);
        if (NodeGroup.GetTotalNodes()>1) Tensor2Reduce(Vxc);
        KS[n] += Vxc;
    }


}

