/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cmath>

#include "error.hpp"
#include "becke.hpp"
#include "xcparam.hpp"

// we use the same values as the DIRAC15 code
double get_bragg_au(const int charge) {
    std::ostringstream err_msg;
    err_msg << "Bragg-Angstrom radius not found for element ";

    if (charge<1 || charge>86) {
        err_msg << charge;
	    NUMGRID_ERROR(err_msg);
    }

    const double radii[] = {0.35,                                                                                                                                                                                     0.35,
                            1.45, 1.05,                                                                                                                                                 0.85, 0.70, 0.65, 0.60, 0.50, 0.45,
                            1.80, 1.50,                                                                                                                                                 1.25, 1.10, 1.00, 1.00, 1.00, 1.00,
                            2.20, 1.80,                                                                                     1.60, 1.40, 1.35, 1.40, 1.40, 1.40, 1.35, 1.35, 1.35, 1.35, 1.30, 1.25, 1.15, 1.15, 1.15, 1.10,
                            2.35, 2.00,                                                                                     1.80, 1.55, 1.45, 1.45, 1.35, 1.30, 1.35, 1.40, 1.60, 1.55, 1.55, 1.45, 1.45, 1.40, 1.40, 1.40,
                            2.60, 2.15, 1.95, 1.85, 1.85, 1.85, 1.85, 1.85, 1.85, 1.80, 1.75, 1.75, 1.75, 1.75, 1.75, 1.75, 1.75, 1.55, 1.45, 1.35, 1.30, 1.30, 1.35, 1.35, 1.35, 1.50, 1.90, 1.75, 1.60, 1.90, 1.50, 1.50};



    return radii[charge-1]*(1./0.529177249);
}

// JCP 88, 2547 (1988), eq. 20
inline double f3(double f) {

    for (int i = 0; i < BECKE_HARDNESS; i++)
        f *= (1.5 - 0.5 * f * f);

    return f;
}

static const int BLOCK = 8;

// JCP 88, 2547 (1988)
double get_becke_w(const double center_coordinates[],
                   const int icent,
                   const int num_centers,
                   const double x,
                   const double y,
                   const double z,
                         double * const * idist,
                         double * const * a_ab
                         )
{
    const int ncols = ((num_centers+BLOCK-1)/BLOCK) * BLOCK; // pad if necessary

    double pa[ncols]   __attribute__ ((aligned (64)));
    double dist[ncols] __attribute__ ((aligned (64)));

    for (int a = 0; a < ncols; a++) pa[a] = 1.0;


    for (int a = 0; a < num_centers; a++) {
        double vx = center_coordinates[a * 3] - x;
        double vy = center_coordinates[a * 3 + 1] - y;
        double vz = center_coordinates[a * 3 + 2] - z;
        dist[a] = sqrt(vx * vx + vy * vy + vz * vz);
    }
    for (int a = num_centers; a < ncols; a++) dist[a] = 0;

    // blocking this loop with the correct stride allows for two different compiler optimizations;
    // 1) vectorization over aa
    // 2) false dependency breaking over bb

    for (int aa = 0; aa < num_centers; aa+=BLOCK) {

        double ppa[BLOCK][BLOCK] __attribute__ ((aligned (64))); // align to cache size; 8*8 bytes
        for (int i=0; i<BLOCK; ++i) for (int j=0; j<BLOCK; ++j) ppa[i][j] = 1.;

        for (int bb = 0; bb < num_centers; bb+=BLOCK) {
            const int amax = std::min(BLOCK, num_centers-aa);
            const int bmax = std::min(BLOCK, num_centers-bb);



            for (int i=0; i<BLOCK; ++i) {
                for (int j=0; j<BLOCK; ++j) {
                    const int a = aa+i;
                    //const int b = bb+j;

                    // if (a==b) continue;

                    // JCP 88, 2547 (1988), eq. 11
                    double mu_ab = (dist[a] - (&dist[bb])[j]) * (&idist[a][bb])[j];
                    double nu_ab = mu_ab + (&a_ab[a][bb])[j] * (1.0 - mu_ab * mu_ab);

                    double f = f3(nu_ab); // eq. 20

                    //pa[a] *= 0.5*(1.0 - f);
                    ppa[i][j] *= 0.5*(1.0 - f);
                }
            }

        }

        {
            const int amax = std::min(BLOCK, num_centers-aa);
            for (int i=0; i<amax; ++i)
                for (int j=0; j<BLOCK; ++j)
                    pa[aa+i] *= ppa[i][j];
        }

    }

    /*
    for (int a = 0; a < num_centers; a+=8) {

        // first blocking avoid chain dependencies
        double ppa[8][4] = {1,1,1,1};
        //double ppa = 1.;

        // if nu_ab == -1 then f==-1, so 0.5*(1--1) ==1
        // the easiest way to do this is with a_ab[i][i]= -1 (since mu_ab[i][i]==0)

        int b = 0;

        // most elements
        for (b = 0; b+4 < num_centers; b+=4) {
            for (int c = 0; c<4; ++c) {
                if (a==b+c) continue;

                // JCP 88, 2547 (1988), eq. 11
                double mu_ab = (dist[a] - dist[b+c]) * idist[a][b+c];
                double nu_ab = mu_ab + a_ab[a][b+c] * (1.0 - mu_ab * mu_ab);

                double f = f3(nu_ab);

                ppa[c] *= 0.5*(1.0 - f);
            }
        }
        // the last few
        {
            for (int c = 0; b+c<num_centers; ++c) {
                if (a==b+c) continue;

                // JCP 88, 2547 (1988), eq. 11
                double mu_ab = (dist[a] - dist[b+c]) * idist[a][b+c];
                double nu_ab = mu_ab + a_ab[a][b+c] * (1.0 - mu_ab * mu_ab);

                double f = f3(nu_ab);

                ppa[c] *= 0.5*(1.0 - f);
            }
        }

        pa[a] = ppa[0]*ppa[1]*ppa[2]*ppa[3];
    }
    */

    double w = 0.0;
    for (int a = 0; a < num_centers; a++) w += pa[a];

    return pa[icent] / w;

    //if (std::fabs(w) > SMALL) return pa[icent] / w;
    //else                      return 0.0;

}
