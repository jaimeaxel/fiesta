/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */


#include "xcgrid.hpp"

#include <algorithm>
#include <cmath>

#include "error.hpp"
#include "becke.hpp"
#include "radial.hpp"
#include "xcparam.hpp"
#include "lebedev.hpp"


#include "low/MPIwrap.hpp"


XCgrid::XCgrid() {
    nullify();
}

XCgrid::~XCgrid() {
    delete[] xyzw;
    nullify();
}

int64_t XCgrid::numgrid_get_num_points() const {
    return num_points;
}

const double * const XCgrid::numgrid_get_grid() const {
    return xyzw;
}

void XCgrid::nullify() {
    xyzw = NULL;
    num_points = -1;
}

int XCgrid::get_closest_num_angular(int n) const {
    int m;

    for (int i = 0; i < MAX_ANGULAR_ORDER; i++) {
        m = lebedev_table[i];
        if (m >= n)
            return m;
    }

    NUMGRID_ERROR("Input n too high in get_closest_num_angular");
}

int XCgrid::get_angular_order(int n) const {

    for (int i = 0; i < MAX_ANGULAR_ORDER; i++) {
        if (lebedev_table[i] == n)
            return i;
    }

    NUMGRID_ERROR("No match found in get_angular_offset");
}


int XCgrid::generate(const double radial_precision,
                   const int min_num_angular_points,
                   const int max_num_angular_points,
                   const int num_centers,
                   const double center_coordinates[],
                   const int center_elements[],
                   const int num_outer_centers,
                   const double outer_center_coordinates[],
                   const int outer_center_elements[],
                   const int num_shells,
                   const int shell_centers[],
                   const int shell_l_quantum_numbers[],
                   const int shell_num_primitives[],
                   const double primitive_exponents[],
                   const double wthresh)
{

    int num_min_num_angular_points =
        get_closest_num_angular(min_num_angular_points);
    int num_max_num_angular_points =
        get_closest_num_angular(max_num_angular_points);

    double *angular_x = new double[MAX_ANGULAR_ORDER * MAX_ANGULAR_GRID];
    double *angular_y = new double[MAX_ANGULAR_ORDER * MAX_ANGULAR_GRID];
    double *angular_z = new double[MAX_ANGULAR_ORDER * MAX_ANGULAR_GRID];
    double *angular_w = new double[MAX_ANGULAR_ORDER * MAX_ANGULAR_GRID];

    int num_centers_total = num_centers + num_outer_centers;
    double *center_coordinates_total = new double[3 * num_centers_total];

    int *center_elements_total = new int[num_centers_total];

    int i = 0;
    for (int icent = 0; icent < num_centers; icent++)
    {
        center_coordinates_total[i * 3 + 0] = center_coordinates[icent * 3 + 0];
        center_coordinates_total[i * 3 + 1] = center_coordinates[icent * 3 + 1];
        center_coordinates_total[i * 3 + 2] = center_coordinates[icent * 3 + 2];
        center_elements_total[i] = center_elements[icent];
        i++;
    }
    for (int icent = 0; icent < num_outer_centers; icent++)
    {
        center_coordinates_total[i * 3 + 0] = outer_center_coordinates[icent * 3 + 0];
        center_coordinates_total[i * 3 + 1] = outer_center_coordinates[icent * 3 + 1];
        center_coordinates_total[i * 3 + 2] = outer_center_coordinates[icent * 3 + 2];
        center_elements_total[i] = outer_center_elements[icent];
        i++;
    }

    for (int i = get_angular_order(num_min_num_angular_points); i <= get_angular_order(num_max_num_angular_points); i++)
    {
        int angular_off = i * MAX_ANGULAR_GRID;
        ld_by_order(lebedev_table[i],
                    &angular_x[angular_off],
                    &angular_y[angular_off],
                    &angular_z[angular_off],
                    &angular_w[angular_off]);
    }

    int *num_points_on_atom = new int[num_centers];
    int64_t * off           = new int64_t[num_centers+1];


    struct rshell {
        double r;
        double w;
        int n_angular;
    };

    int      n_radial[num_centers];
    rshell *   radial[num_centers];

    struct grid_point {
        double x,y,z, w;
    };

    grid_point * gp;


    // 1st generate the grid for each kind of atom
    // 2nd còpy the grid shitfted to each center
    // 3rd weight every point with the becke w partition


    // first round figures out dimensions
    {
        for (int icent = 0; icent < num_centers; icent++) {

            int num_shells_on_this_center = 0;
            int l_max = 0;
            for (int ishell = 0; ishell < num_shells; ishell++) {
                if ((shell_centers[ishell] - 1) == icent) {
                    l_max = std::max(l_max, shell_l_quantum_numbers[ishell]);
                    num_shells_on_this_center++;
                }
            }

            // skip this center if there are no shells
            if (num_shells_on_this_center == 0) {
                num_points_on_atom[icent] = 0;
                continue;
            }

            // get extreme alpha values
            double alpha_max = 0.0;
            double alpha_min[l_max + 1];
            bool   alpha_min_set[l_max + 1];

            for (int l=0; l<=l_max; ++l) alpha_min[l] = 0;
            for (int l=0; l<=l_max; ++l) alpha_min_set[l] = false;


            int n = 0;
            for (int ishell = 0; ishell < num_shells; ishell++)
            {
                if ((shell_centers[ishell] - 1) == icent)
                {
                    int l = shell_l_quantum_numbers[ishell];

                    if (!alpha_min_set[l])
                    {
                        alpha_min[l] = 1.0e50;
                        alpha_min_set[l] = true;
                    }

                    for (int p = 0; p < shell_num_primitives[ishell]; p++)
                    {
                        double e = primitive_exponents[n+p];
                        alpha_max    = std::max(alpha_max, 2.0 * e); // factor 2.0 to match DIRAC
                        alpha_min[l] = std::min(alpha_min[l], e);
                    }
                }
                n += shell_num_primitives[ishell];
            }

            // obtain radial parameters
            double r_inner = get_r_inner(radial_precision, alpha_max);
            double h = 1.0e50;
            double r_outer = 0.0;

            for (int l = 0; l <= l_max; l++)
            {
                if (alpha_min[l] > 0.0)
                {
                    r_outer = std::max(
                        r_outer,
                        get_r_outer(
                            radial_precision,
                            alpha_min[l],
                            l,
                            4.0 * get_bragg_au(center_elements[icent]) ));
                    NUMGRID_ASSERT(r_outer > r_inner);
                    h = std::min (h, get_h(radial_precision, l, 0.1 * (r_outer - r_inner)));
                }
            }
            NUMGRID_ASSERT(r_outer > h);


            num_points_on_atom[icent] = 0;


            double rb = get_bragg_au(center_elements[icent]) / (5.0); // factors match DIRAC code

            double c = r_inner / (exp(h) - 1.0);
            int num_radial = int(log(1.0 + (r_outer / c)) / h);

            n_radial[icent] = num_radial;
            radial[icent] = new rshell[num_radial];

            //std::cout << "center: " << icent << std::endl;

            for (int irad = 0; irad < num_radial; irad++)
            {
                double radial_r = c * (exp((irad + 1) * h) - 1.0);
                double radial_w = (radial_r + c) * radial_r * radial_r * h;

                //std::cout << "r: " << radial_r << "  w: " << radial_w << std::endl;

                int num_angular = num_max_num_angular_points;
                if (radial_r < rb)
                {
                    num_angular = static_cast<int>(num_max_num_angular_points * (radial_r / rb));
                    num_angular = get_closest_num_angular(num_angular);
                    if (num_angular < num_min_num_angular_points)
                        num_angular = num_min_num_angular_points;
                }

                radial[icent][irad].r         = radial_r;
                radial[icent][irad].w         = radial_w;
                radial[icent][irad].n_angular = num_angular;

                num_points_on_atom[icent] += num_angular;
            }
        }
    }

    {
        num_points = 0;
        for (int icent = 0; icent < num_centers; icent++) {
            off[icent] = num_points;
            num_points += num_points_on_atom[icent];
        }
        off[num_centers] = num_points;

        //xyzw = new double[4 * num_points];
        gp = new grid_point[num_points];
    }

    // second round allocates and does the real work
    {
        for (int icent = 0; icent < num_centers; icent++) {

            //int64_t ioff = 0; for (int jcent = 0; jcent < icent; jcent++) ioff += num_points_on_atom[jcent];
            //double * p = xyzw + 4*off[icent];

            grid_point * g = gp + off[icent];

            const int num_radial = n_radial[icent];

            for (int irad = 0; irad < num_radial; irad++) {

                const double radial_r = radial[icent][irad].r;
                const double radial_w = radial[icent][irad].w;
                const int num_angular = radial[icent][irad].n_angular;

                const int angular_off = get_angular_order(num_angular) * MAX_ANGULAR_GRID;

                for (int iang = 0; iang < num_angular; iang++) {

                    //p[4 * (iang)    ] = center_coordinates[icent * 3    ] + angular_x[angular_off + iang] * radial_r;
                    //p[4 * (iang) + 1] = center_coordinates[icent * 3 + 1] + angular_y[angular_off + iang] * radial_r;
                    //p[4 * (iang) + 2] = center_coordinates[icent * 3 + 2] + angular_z[angular_off + iang] * radial_r;
                    //p[4 * (iang) + 3] = 4.0 * M_PI * angular_w[angular_off + iang] * radial_w;

                    g[iang].x = center_coordinates[3*icent   ] + angular_x[angular_off + iang] * radial_r;
                    g[iang].y = center_coordinates[3*icent +1] + angular_y[angular_off + iang] * radial_r;
                    g[iang].z = center_coordinates[3*icent +2] + angular_z[angular_off + iang] * radial_r;
                    g[iang].w = 4.0 * M_PI * angular_w[angular_off + iang] * radial_w;

                }
                //p += 4*num_angular;

                g += num_angular;
            }
        }
    }

    // weight with Becke
    if (num_centers_total > 1) {

        const int ncols = ((num_centers_total+7)/8) * 8; // pad if necessary


        double * idist2; // = new double[num_centers_total*ncols];
        double * idist[ncols];

        {
            posix_memalign((void**)(&idist2), 64, ncols*ncols*sizeof(double));

            for (int i=0; i<ncols; ++i) idist[i] = idist2 + i*ncols;

            for (int i=0; i<num_centers_total; ++i) {
                for (int j=0; j<num_centers_total; ++j) {
                    if (i==j) idist[i][i] = 0;
                    else {
                        double vx = center_coordinates_total[i * 3]     - center_coordinates_total[j * 3];
                        double vy = center_coordinates_total[i * 3 + 1] - center_coordinates_total[j * 3 + 1];
                        double vz = center_coordinates_total[i * 3 + 2] - center_coordinates_total[j * 3 + 2];

                        idist[i][j] = 1./ sqrt(vx * vx + vy * vy + vz * vz);
                    }
                }
                for (int j=num_centers_total; j<ncols; ++j) idist[i][j] = 0;
            }
            for (int i=num_centers_total; i<ncols; ++i) for (int j=0; j<ncols; ++j) idist[i][j] = 0;
        }



        double * ab2; // = new double[num_centers_total*ncols];
        double * ab[ncols];

        {
            posix_memalign((void**)(&ab2), 64, ncols*ncols*sizeof(double));

            for (int i=0; i<ncols; ++i) ab[i] = ab2 + i*ncols;

            for (int i=0; i<num_centers_total; ++i) {
                for (int j=0; j<num_centers_total; ++j) {
                        const double R_a = get_bragg_au(center_elements_total[i]);
                        const double R_b = get_bragg_au(center_elements_total[j]);

                        // JCP 88, 2547 (1988), eq. 11
                        double u_ab = (R_a - R_b) / (R_b + R_a); // original had signs reversed wrt. eq 11, although the result is identical
                        double a_ab = u_ab / (u_ab * u_ab - 1.0);

                        // JCP 88, 2547 (1988), eq. A3
                        if (a_ab > 0.5)  a_ab = 0.5;
                        if (a_ab < -0.5) a_ab = -0.5;

                        ab[i][j] = a_ab;
                }
                for (int j=num_centers_total; j<ncols; ++j) ab[i][j] = -1; // this makes the formula work even for a==b
                ab[i][i] = -1; // this makes the formula work even for a==b
            }
            for (int i=num_centers_total; i<ncols; ++i) for (int j=0; j<ncols; ++j) ab[i][j] = -1;
        }


        const int nnode = NodeGroup.GetTotalNodes();
        const int nrank = NodeGroup.GetRank();

        #pragma omp parallel for schedule(dynamic)
        for (int64_t nn=nrank; nn<num_points; nn+=nnode) {
        //for (int64_t nn = 0; nn < num_points; ++nn) {

            // this can be done in O(1)
            int icent; {
                icent = 0;
                while (off[icent+1]<nn) {
                    ++icent;
                }            }

            double becke_w = get_becke_w(center_coordinates_total,
                                          icent,
                                          num_centers_total,
                                          gp[nn].x,
                                          gp[nn].y,
                                          gp[nn].z,
                                          idist,
                                          ab
                                          );

            if (becke_w > wthresh)  gp[nn].w *= becke_w;
            else                    gp[nn].w  = 0;
        }

        free(idist2);
        free(ab2);
    }

    // copy the points
    {
        const int nnode = NodeGroup.GetTotalNodes();
        const int nrank = NodeGroup.GetRank();

        uint64_t node_points = num_points / nnode + 1;
        xyzw = new double[4*node_points+4*128];

        for (int64_t k=0; k<4*(node_points+128); ++k) {
            xyzw[k] = 0.;
        }

        uint64_t k = 0;
        for (int64_t nn=nrank; nn<num_points; nn+=nnode) {
            // this prunes
            if (fabs(gp[nn].w) > 0) {
                xyzw[4*k+0] = gp[nn].x;
                xyzw[4*k+1] = gp[nn].y;
                xyzw[4*k+2] = gp[nn].z;
                xyzw[4*k+3] = gp[nn].w;
                ++k;
            }
        }

        delete[] gp;
        num_points = k;
    }

    // pruning : to be implemented



    // free memory

    for (int icent = 0; icent < num_centers; icent++) delete[] radial[icent];

    delete[] off;

    delete[] num_points_on_atom;

    delete[] angular_x;
    delete[] angular_y;
    delete[] angular_z;
    delete[] angular_w;

    delete[] center_coordinates_total;
    delete[] center_elements_total;

    return 0;
}
