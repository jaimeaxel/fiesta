/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PARAMETERS_H_INCLUDED
#define PARAMETERS_H_INCLUDED

const double SMALL = 1.0e-14;
const double BECKE_CUTOFF = 64.0; // unused
const int BECKE_HARDNESS = 3;

const int MAX_ANGULAR_ORDER = 32;
const int MAX_ANGULAR_GRID = 5810;

const int lebedev_table[32] = {6,    14,   26,   38,   50,   74,   86,   110,  146,
                             170,  194,  230,  266,  302,  350,  434,  590,  770,
                             974,  1202, 1454, 1730, 2030, 2354, 2702, 3074, 3470,
                             3890, 4334, 4802, 5294, 5810};

#endif /* PARAMETERS_H_INCLUDED */
