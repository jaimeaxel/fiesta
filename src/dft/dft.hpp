#ifndef __dft__
#define __dft__

#include <string>

class XCgrid;
class XCint;

class GridFunction;

class DFTgrid {

    XCgrid * dft_grid;
    XCint  * dft_integrator;

    double wthresh = 1.0e-12;

    double radial_precision;
    int    min_num_angular_points;
    int    max_num_angular_points;

    int    num_centers;
    double * center_coordinates;
    int    * center_elements;

    int    num_outer_centers;
    double * outer_center_coordinates;
    int    * outer_center_elements;

    int    num_shells;

    int    * shell_centers;
    int    * shell_l_quantum_numbers;
    int    * shell_num_primitives;

    int num_primitives;

    double * primitive_exponents;
    double * contraction_coefficients;


    int64_t Npoints;
    const double * grid;

    // ground state density & second derivatives (for response)
    GridFunction * gs_dens;

  public:
    double xs_w,  alpha_w, beta_w, mu_w;

    bool init_grid;
    bool use_dft;
    bool global_hybrid;
    bool range_separated;

    // name of the functional
    std::string funcname;

  public:

    DFTgrid();
   ~DFTgrid();

    void   Init   (const rAtom * Atoms, int nnuclei, const GTObasis & BasisSet);
    double CalcXCscf (tensor2 & KS, const tensor2 & D)  const;
    void   CalcXCrsp (tensor2 * KS, const tensor2 * D2, int N, const tensor2 & D0);

    int    SetWthresh (double thre);

    int    SetFunctional (const std::string & name);
    int    SetRthresh (double precision);
    int    SetMinL (int npoints);
    int    SetMaxL (int npoints);

    //int    Set     (const std::string & key, int    val);
    //int    Set     (const std::string & key, double val);

    bool        IsInit      () const {return init_grid;};
    double      GetHFK      () const {return xs_w;};
    std::string GetFuncName () const {return funcname;};
    bool        IsHybrid    () const {return global_hybrid;};

};

#endif
