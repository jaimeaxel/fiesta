#include <iostream>
using namespace std;

#include "ERIbatch.hpp"
#include "low/chrono.hpp"
#include "basis/SPprototype.hpp"


/*
                D (DIGESTION) STEPS
                ===================
*/

int __forceinline__ __device__ Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

int __forceinline__ __device__ Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}

/*
template <int maxL1, int maxL2> __device__ void Mprod  (double * MR, const double * RM) {

    int t = threadIdx.x;

    double MM[maxL1*maxL2];

    if      (maxL1==3) {
        const double * PP = RM;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 3*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += PP[ki*NTX+t] * MR[kj*NTX+t];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        const double * DD = RM + 9*NTX;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = 5*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += DD[ki*NTX+t] * MR[kj*NTX+t];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij*NTX+t];
            }
        }
    }

    if      (maxL2==3) {
        const double * PP = RM;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 3*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += PP[kj*NTX+t] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = sum;
            }
        }
    }
    else if (maxL2==5) {
        const double * DD = RM + 9*NTX;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = 5*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += DD[kj*NTX+t] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = sum;
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = MM[ij];
            }
        }
    }
}

template <int maxL1, int maxL2> __device__ void MprodT (double * MR, const double * RM) {

    int t = threadIdx.x;

    double MM[maxL1*maxL2];

    if      (maxL1==3) {
        const double * PP = RM;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 3*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += PP[ik*NTX+t] * MR[kj*NTX+t];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else if (maxL1==5) {
        const double * DD = RM + 9*NTX;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = 5*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += DD[ik*NTX+t] * MR[kj*NTX+t];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij*NTX+t];
            }
        }
    }

    if      (maxL2==3) {
        const double * PP = RM;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 3*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += PP[jk*NTX+t] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = sum;
            }
        }
    }
    else if (maxL2==5) {
        const double * DD = RM + 9*NTX;

        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = 5*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += DD[jk*NTX+t] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = sum;
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = MM[ij];
            }
        }
    }
}


template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ jABCD (double *  FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mb=0; mb<maxLb; ++mb) {
            double sum = 0;
            for (int mc=0; mc<maxLc; ++mc) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int cd = Tpos(maxLc,maxLd,mc,md);
                    sum += ERI[pos*NTX+i] * DD[cd*NTX+i];
                }
            }
            int ab = Tpos(maxLa,maxLb,ma,mb);
            FF[ab*NTX+i]+=sum*2;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ jCDAB (double *  FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    for (int mc=0; mc<maxLc; ++mc) {
        for (int md=0; md<maxLd; ++md) {
            double sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLb; ++mb) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLb,ma,mb);
                    sum += ERI[pos*NTX+i] * DD[ab*NTX + i];
                }
            }
            int cd = Tpos(maxLc,maxLd,mc,md);
            FF[cd*NTX+i]+=sum*2;
        }
    }

}


template<int la, int lb, int lc, int ld>
__global__ void J_CDAB(double * d_D, const double * d_ERI, const double * d_RM, double * d_F,  int JA, int JB, int JC, int JD) {

    int i = threadIdx.x;
    int k = blockIdx.y * NTY + threadIdx.y;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    double * Dab = d_D + sAB*k*NTX;
    double * Fcd = d_F + sCD*k*NTX;
    const double * RMV = d_RM + (9+25+49+81)*k*NTX;
    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;


    // rotate density matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            double * dab = Dab + (ja*JB + jb)*(MA*MB) *NTX;

            Mprod <MA,MB> (dab, RMV);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * dab = Dab + (ja*JB + jb)*(MA*MB) *NTX;
                    double * fcd = Fcd + (jc*JD + jd)*(MC*MD) *NTX;

                    jCDAB <MA,MB,MC,MD> (fcd, ERI, dab);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    for (int jc=0; jc<JC; ++jc) {
        for (int jd=0; jd<JD; ++jd) {
            double * fcd = Fcd + (jc*JD + jd)*(MC*MD) *NTX;

            MprodT <MC,MD> (fcd, RMV);
        }
    }

}

template<int la, int lb, int lc, int ld>
__global__ void J_ABCD(double * d_D, const double * d_ERI, const double * d_RM, double * d_F,  int JA, int JB, int JC, int JD) {

    int i = threadIdx.x;
    int k = blockIdx.y * NTY + threadIdx.y;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    double * Dcd = d_D + sCD*k*NTX;
    double * Fab = d_F + sAB*k*NTX;
    const double * RMV = d_RM + (9+25+49+81)*k*NTX;
    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int jc=0; jc<JC; ++jc) {
        for (int jd=0; jd<JD; ++jd) {
            double * dcd = Dcd + (jc*JD + jd)*(MC*MD) *NTX;

            Mprod <MC,MD> (dcd, RMV);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fab = Fab + (ja*JB + jb)*(MA*MB) *NTX;
                    double * dcd = Dcd + (jc*JD + jd)*(MC*MD) *NTX;

                    jABCD <MA,MB,MC,MD> (fab, ERI, dcd);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            double * fab = Fab + (ja*JB + jb)*(MA*MB) *NTX;

            MprodT <MA,MB> (fab, RMV);
        }
    }

}



template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xACBD(double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    for (int ma=0; ma<maxLa; ++ma) {
        for (int mc=0; mc<maxLc; ++mc) {
            double sum = 0;
            for (int mb=0; mb<maxLb; ++mb) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int bd = Tpos(maxLb,maxLd,mb,md);
                    sum += ERI[pos*NTX+i] * DD[bd*NTX+i];
                }
            }
            int ac = Tpos(maxLa,maxLc,ma,mc);
            FF[ac*NTX+i]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xADBC(double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    for (int ma=0; ma<maxLa; ++ma) {
        for (int md=0; md<maxLd; ++md) {
            double sum = 0;
            for (int mb=0; mb<maxLb; ++mb) {
                for (int mc=0; mc<maxLc; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int bc = Tpos(maxLb,maxLc,mb,mc);
                    sum += ERI[pos*NTX+i] * DD[bc*NTX+i];
                }
            }
            int ad = Tpos(maxLa,maxLd,ma,md);
            FF[ad*NTX+i]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xBDAC(double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    for (int mb=0; mb<maxLb; ++mb) {
        for (int md=0; md<maxLd; ++md) {
            double sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int mc=0; mc<maxLc; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ac = Tpos(maxLa,maxLc,ma,mc);
                    sum += ERI[pos*NTX+i] * DD[ac*NTX+i];
                }
            }
            int bd = Tpos(maxLb,maxLd,mb,md);
            FF[bd*NTX+i]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xBCAD(double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    for (int mb=0; mb<maxLb; ++mb) {
        for (int mc=0; mc<maxLc; ++mc) {
            double sum = 0;
            for (int ma=0; ma<maxLa; ++ma) {
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ad = Tpos(maxLa,maxLd,ma,md);
                    sum += ERI[pos*NTX+i] * DD[ad*NTX+i];
                }
            }
            int bc = Tpos(maxLb,maxLc,mb,mc);
            FF[bc*NTX+i]+=sum;
        }
    }

}


template<int la, int lb, int lc, int ld>
__global__ void X_ADBC(double * d_D, const double * d_ERI, const double * d_RM, double * d_F,  int JA, int JB, int JC, int JD) {

    int i = threadIdx.x;
    int k = blockIdx.y * NTY + threadIdx.y;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;

    double * Dbc = d_D + sBC*k*NTX;
    double * Fad = d_F + sAD*k*NTX;
    const double * RMV = d_RM + (9+25+49+81)*k*NTX;
    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int jb=0; jb<JB; ++jb) {
        for (int jc=0; jc<JC; ++jc) {
            double * dbc = Dbc + (jb*JC + jc)*(MB*MC) *NTX;

            Mprod <MB,MC> (dbc, RMV);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fad = Fad + (ja*JD + jd)*(MA*MD) *NTX;
                    double * dbc = Dbc + (jb*JC + jc)*(MB*MC) *NTX;

                    xADBC <MA,MB,MC,MD> (fad, ERI, dbc);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jd=0; jd<JD; ++jd) {
            double * fad = Fad + (ja*JD + jd)*(MA*MD) *NTX;

            MprodT <MA,MD> (fad, RMV);
        }
    }

}

template<int la, int lb, int lc, int ld>
__global__ void X_BCAD(double * d_D, const double * d_ERI, const double * d_RM, double * d_F,  int JA, int JB, int JC, int JD) {

    int i = threadIdx.x;
    int k = blockIdx.y * NTY + threadIdx.y;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;

    double * Dad = d_D + sAD*k*NTX;
    double * Fbc = d_F + sBC*k*NTX;
    const double * RMV = d_RM + (9+25+49+81)*k*NTX;
    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int ja=0; ja<JA; ++ja) {
        for (int jd=0; jd<JD; ++jd) {
            double * dad = Dad + (ja*JD + jd)*(MA*MD) *NTX;

            Mprod <MA,MD> (dad, RMV);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fbc = Fbc + (jb*JC + jc)*(MB*MC) *NTX;
                    double * dad = Dad + (ja*JD + jd)*(MA*MD) *NTX;

                    xBCAD <MA,MB,MC,MD> (fbc, ERI, dad);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int jb=0; jb<JB; ++jb) {
        for (int jc=0; jc<JC; ++jc) {
            double * fbc = Fbc + (jb*JC + jc)*(MB*MC) *NTX;

            MprodT <MB,MC> (fbc, RMV);
        }
    }


}

template<int la, int lb, int lc, int ld>
__global__ void X_ACBD(double * d_D, const double * d_ERI, const double * d_RM, double * d_F,  int JA, int JB, int JC, int JD) {

    int i = threadIdx.x;
    int k = blockIdx.y * NTY + threadIdx.y;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = JA*JC*MA*MC;
    const int sBD = JB*JD*MB*MD;

    double * Dbd = d_D + sBD*k*NTX;
    double * Fac = d_F + sAC*k*NTX;
    const double * RMV = d_RM + (9+25+49+81)*k*NTX;
    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int jb=0; jb<JB; ++jb) {
        for (int jd=0; jd<JD; ++jd) {
            double * dbd = Dbd + (jb*JD + jd)*(MB*MD) *NTX;

            Mprod <MB,MD> (dbd, RMV);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fac = Fac + (ja*JC + jc)*(MA*MC) *NTX;
                    double * dbd = Dbd + (jb*JD + jd)*(MB*MD) *NTX;

                    xACBD <MA,MB,MC,MD> (fac, ERI, dbd);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jc=0; jc<JC; ++jc) {
            double * fac = Fac + (ja*JC + jc)*(MA*MC) *NTX;

            MprodT <MA,MC> (fac, RMV);
        }
    }

}

template<int la, int lb, int lc, int ld>
__global__ void X_BDAC(double * d_D, const double * d_ERI, const double * d_RM, double * d_F,  int JA, int JB, int JC, int JD) {

    int i = threadIdx.x;
    int k = blockIdx.y * NTY + threadIdx.y;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = JA*JC*MA*MC;
    const int sBD = JB*JD*MB*MD;

    double * Dac = d_D + sAC*k*NTX;
    double * Fbd = d_F + sBD*k*NTX;
    const double * RMV = d_RM + (9+25+49+81)*k*NTX;
    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int ja=0; ja<JA; ++ja) {
        for (int jc=0; jc<JC; ++jc) {
            double * dac = Dac + (ja*JC + jc)*(MA*MC) *NTX;

            Mprod <MA,MC> (dac, RMV);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * dac = Dac + (ja*JC + jc)*(MA*MC) *NTX;
                    double * fbd = Fbd + (jb*JD + jd)*(MB*MD) *NTX;

                    xBDAC <MA,MB,MC,MD> (fbd, ERI, dac);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int jb=0; jb<JB; ++jb) {
        for (int jd=0; jd<JD; ++jd) {
            double * fbd = Fbd + (jb*JD + jd)*(MB*MD) *NTX;

            MprodT <MB,MD> (fbd, RMV);
        }
    }


}



void BatchEvaluator::ContractDJs  (const ERIBatch & TheBatch, cudaStream_t * stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    //CONTRACT WITH DENSITY MATRIX
    chronoC.Start();
    {
        dim3 Nbl (  1, TheBatch.NtilesN/NTY);
        dim3 Nth (NTX,                  NTY);

        if      (la==0 && lb==0 && lc==0 && ld==0) {
            J_CDAB <0,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==0 && lc==0 && ld==0) {
            J_CDAB <1,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==0 && lc==1 && ld==0) {
            J_CDAB <1,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==1 && lc==0 && ld==0) {
            J_CDAB <1,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==1 && lc==1 && ld==0) {
            J_CDAB <1,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==1 && lc==1 && ld==1) {
            J_CDAB <1,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==0 && ld==0) {
            J_CDAB <2,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==1 && ld==0) {
            J_CDAB <2,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==1 && ld==1) {
            J_CDAB <2,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==2 && ld==0) {
            J_CDAB <2,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==0 && ld==0) {
            J_CDAB <2,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==1 && ld==0) {
            J_CDAB <2,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==1 && ld==1) {
            J_CDAB <2,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==2 && ld==0) {
            J_CDAB <2,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==2 && ld==1) {
            J_CDAB <2,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==0 && ld==0) {
            J_CDAB <2,2,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==1 && ld==0) {
            J_CDAB <2,2,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==1 && ld==1) {
            J_CDAB <2,2,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==2 && ld==0) {
            J_CDAB <2,2,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==2 && ld==1) {
            J_CDAB <2,2,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==2 && ld==2) {
            J_CDAB <2,2,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
    chronoC.Stop();
}

void BatchEvaluator::ContractDXs  (const ERIBatch & TheBatch, cudaStream_t * stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;


    //CONTRACT WITH DENSITY MATRIX
    chronoC.Start();
    {
        dim3 Nbl (  1, TheBatch.NtilesN/NTY);
        dim3 Nth (NTX,                  NTY);

        if      (la==0 && lb==0 && lc==0 && ld==0) {
            X_ADBC <0,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==0 && lc==0 && ld==0) {
            X_ADBC <1,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==0 && lc==1 && ld==0) {
            X_ADBC <1,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==1 && lc==0 && ld==0) {
            X_ADBC <1,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==1 && lc==1 && ld==0) {
            X_ADBC <1,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==1 && lb==1 && lc==1 && ld==1) {
            X_ADBC <1,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==0 && ld==0) {
            X_ADBC <2,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==1 && ld==0) {
            X_ADBC <2,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==1 && ld==1) {
            X_ADBC <2,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==0 && lc==2 && ld==0) {
            X_ADBC <2,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==0 && ld==0) {
            X_ADBC <2,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==1 && ld==0) {
            X_ADBC <2,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==1 && ld==1) {
            X_ADBC <2,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==2 && ld==0) {
            X_ADBC <2,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==1 && lc==2 && ld==1) {
            X_ADBC <2,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==0 && ld==0) {
            X_ADBC <2,2,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==1 && ld==0) {
            X_ADBC <2,2,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==1 && ld==1) {
            X_ADBC <2,2,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==2 && ld==0) {
            X_ADBC <2,2,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==2 && ld==1) {
            X_ADBC <2,2,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if (la==2 && lb==2 && lc==2 && ld==2) {
            X_ADBC <2,2,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
    chronoC.Stop();
}

void BatchEvaluator::ContractDJXs (const ERIBatch & TheBatch, cudaStream_t * stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    //CONTRACT WITH DENSITY MATRIX
    chronoC.Start();
    {
        dim3 Nbl (  1, TheBatch.NtilesN/NTY);
        dim3 Nth (NTX,                  NTY);

        if           (la==0 && lb==0 && lc==0 && ld==0) {
            J_CDAB <0,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==0) {
            J_CDAB <0,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==1) {
            J_CDAB <0,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==0) {
            J_CDAB <0,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==1) {
            J_CDAB <0,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==2) {
            J_CDAB <0,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==0 && ld==0) {
            J_CDAB <1,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==0) {
            J_CDAB <1,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==1) {
            J_CDAB <1,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==0) {
            J_CDAB <1,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==1) {
            J_CDAB <1,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==2) {
            J_CDAB <1,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==0 && ld==0) {
            J_CDAB <1,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==0) {
            J_CDAB <1,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==1) {
            J_CDAB <1,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==0) {
            J_CDAB <1,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==1) {
            J_CDAB <1,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==2) {
            J_CDAB <1,1,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==0 && ld==0) {
            J_CDAB <2,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==0) {
            J_CDAB <2,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==1) {
            J_CDAB <2,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==0) {
            J_CDAB <2,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==1) {
            J_CDAB <2,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==2) {
            J_CDAB <2,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==0 && ld==0) {
            J_CDAB <2,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==0) {
            J_CDAB <2,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==1) {
            J_CDAB <2,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==0) {
            J_CDAB <2,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==1) {
            J_CDAB <2,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==2) {
            J_CDAB <2,1,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==0 && ld==0) {
            J_CDAB <2,2,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==0) {
            J_CDAB <2,2,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==1) {
            J_CDAB <2,2,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==0) {
            J_CDAB <2,2,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==1) {
            J_CDAB <2,2,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==2) {
            J_CDAB <2,2,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRM8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }
    }
    chronoC.Stop();
}
*/



template <int maxL1, int maxL2> __device__ void Mprod  (double * MR, const double * RM1, const double * RM2) {

    int t = threadIdx.x;

    double MM[maxL1*maxL2];

    if (maxL1==1) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij*NTX+t];
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ki = maxL1*k+i;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM1[ki*NTX+t] * MR[kj*NTX+t];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }

    if (maxL2==1) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = MM[ij];
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int kj = maxL2*k+j;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM2[kj*NTX+t] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = sum;
            }
        }
    }
}

template <int maxL1, int maxL2> __device__ void MprodT (double * MR, const double * RM1, const double * RM2) {

    int t = threadIdx.x;

    double MM[maxL1*maxL2];

    if (maxL1==1) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = MR[ij*NTX+t];
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL1; ++k) {
                    int ik = maxL1*i+k;
                    int kj = Tpos(maxL1,maxL2,k,j);
                    sum  += RM1[ik*NTX+t] * MR[kj*NTX+t];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MM[ij] = sum;
            }
        }
    }

    if (maxL2==1) {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = MM[ij];
            }
        }
    }
    else {
        for (int i=0; i<maxL1; ++i) {
            for (int j=0; j<maxL2; ++j) {
                double sum = 0;
                for (int k=0; k<maxL2; ++k) {
                    int jk = maxL2*j+k;
                    int ik = Tpos(maxL1,maxL2,i,k);
                    sum  += RM2[jk*NTX+t] * MM[ik];
                }
                int ij = Tpos(maxL1,maxL2,i,j);
                MR[ij*NTX+t] = sum;
            }
        }
    }
}


template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ jABCD (double *  FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    #pragma unroll
    for (int ma=0; ma<maxLa; ++ma) {
        #pragma unroll
        for (int mb=0; mb<maxLb; ++mb) {
            double sum = 0;
            #pragma unroll
            for (int mc=0; mc<maxLc; ++mc) {
                #pragma unroll
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int cd  = Tpos(maxLc,maxLd,mc,md);
                    sum += ERI[pos*NTX+i] * DD[cd*NTX+i];
                }
            }
            int ab = Tpos(maxLa,maxLb,ma,mb);
            FF[ab*NTX+i]+=sum*2;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ jCDAB (double *  FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    #pragma unroll
    for (int mc=0; mc<maxLc; ++mc) {
        #pragma unroll
        for (int md=0; md<maxLd; ++md) {
            double sum = 0;
            #pragma unroll
            for (int ma=0; ma<maxLa; ++ma) {
                #pragma unroll
                for (int mb=0; mb<maxLb; ++mb) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ab = Tpos(maxLa,maxLb,ma,mb);
                    sum += ERI[pos*NTX+i] * DD[ab*NTX + i];
                }
            }
            int cd = Tpos(maxLc,maxLd,mc,md);
            FF[cd*NTX+i]+=sum*2;
        }
    }

}


template<int la, int lb, int lc, int ld>
__global__ void J_CDAB (double * d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double * d_F,  int JA, int JB, int JC, int JD, int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    double * Dab = d_D + sAB*k*NTX;
    double * Fcd = d_F + sCD*k*NTX;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;


    // rotate density matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            double * dab = Dab + (ja*JB + jb)*(MA*MB) *NTX;

            Mprod <MA,MB> (dab, RMA, RMB);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * dab = Dab + (ja*JB + jb)*(MA*MB) *NTX;
                    double * fcd = Fcd + (jc*JD + jd)*(MC*MD) *NTX;

                    jCDAB <MA,MB,MC,MD> (fcd, ERI, dab);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    for (int jc=0; jc<JC; ++jc) {
        for (int jd=0; jd<JD; ++jd) {
            double * fcd = Fcd + (jc*JD + jd)*(MC*MD) *NTX;

            MprodT <MC,MD> (fcd, RMC, RMD);
        }
    }

}

template<int la, int lb, int lc, int ld>
__global__ void J_ABCD (double * d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double * d_F,  int JA, int JB, int JC, int JD, int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    double * Dcd = d_D + sCD*k*NTX;
    double * Fab = d_F + sAB*k*NTX;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int jc=0; jc<JC; ++jc) {
        for (int jd=0; jd<JD; ++jd) {
            double * dcd = Dcd + (jc*JD + jd)*(MC*MD) *NTX;

            Mprod <MC,MD> (dcd, RMC, RMD);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fab = Fab + (ja*JB + jb)*(MA*MB) *NTX;
                    double * dcd = Dcd + (jc*JD + jd)*(MC*MD) *NTX;

                    jABCD <MA,MB,MC,MD> (fab, ERI, dcd);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            double * fab = Fab + (ja*JB + jb)*(MA*MB) *NTX;

            MprodT <MA,MB> (fab, RMA, RMB);
        }
    }

}


template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xACBD (double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    #pragma unroll
    for (int ma=0; ma<maxLa; ++ma) {
        #pragma unroll
        for (int mc=0; mc<maxLc; ++mc) {
            double sum = 0;
            #pragma unroll
            for (int mb=0; mb<maxLb; ++mb) {
                #pragma unroll
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int bd = Tpos(maxLb,maxLd,mb,md);
                    sum += ERI[pos*NTX+i] * DD[bd*NTX+i];
                }
            }
            int ac = Tpos(maxLa,maxLc,ma,mc);
            FF[ac*NTX+i]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xADBC (double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    #pragma unroll
    for (int ma=0; ma<maxLa; ++ma) {
        #pragma unroll
        for (int md=0; md<maxLd; ++md) {
            double sum = 0;
            #pragma unroll
            for (int mb=0; mb<maxLb; ++mb) {
                #pragma unroll
                for (int mc=0; mc<maxLc; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int bc = Tpos(maxLb,maxLc,mb,mc);
                    sum += ERI[pos*NTX+i] * DD[bc*NTX+i];
                }
            }
            int ad = Tpos(maxLa,maxLd,ma,md);
            FF[ad*NTX+i]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xBDAC (double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    #pragma unroll
    for (int mb=0; mb<maxLb; ++mb) {
        #pragma unroll
        for (int md=0; md<maxLd; ++md) {
            double sum = 0;
            #pragma unroll
            for (int ma=0; ma<maxLa; ++ma) {
                #pragma unroll
                for (int mc=0; mc<maxLc; ++mc) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ac = Tpos(maxLa,maxLc,ma,mc);
                    sum += ERI[pos*NTX+i] * DD[ac*NTX+i];
                }
            }
            int bd = Tpos(maxLb,maxLd,mb,md);
            FF[bd*NTX+i]+=sum;
        }
    }

}

template <int maxLa, int maxLb, int maxLc, int maxLd> void __device__ xBCAD (double * FF, const double * ERI, const double * DD) {

    int i = threadIdx.x;

    #pragma unroll
    for (int mb=0; mb<maxLb; ++mb) {
        #pragma unroll
        for (int mc=0; mc<maxLc; ++mc) {
            double sum = 0;
            #pragma unroll
            for (int ma=0; ma<maxLa; ++ma) {
                #pragma unroll
                for (int md=0; md<maxLd; ++md) {
                    int pos = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);
                    int ad = Tpos(maxLa,maxLd,ma,md);
                    sum += ERI[pos*NTX+i] * DD[ad*NTX+i];
                }
            }
            int bc = Tpos(maxLb,maxLc,mb,mc);
            FF[bc*NTX+i]+=sum;
        }
    }

}


template<int la, int lb, int lc, int ld>
__global__ void X_ADBC (double * d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double * d_F,  int JA, int JB, int JC, int JD, int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;

    double * Dbc = d_D + sBC*k*NTX;
    double * Fad = d_F + sAD*k*NTX;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int jb=0; jb<JB; ++jb) {
        for (int jc=0; jc<JC; ++jc) {
            double * dbc = Dbc + (jb*JC + jc)*(MB*MC) *NTX;

            Mprod <MB,MC> (dbc, RMB, RMC);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fad = Fad + (ja*JD + jd)*(MA*MD) *NTX;
                    double * dbc = Dbc + (jb*JC + jc)*(MB*MC) *NTX;

                    xADBC <MA,MB,MC,MD> (fad, ERI, dbc);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jd=0; jd<JD; ++jd) {
            double * fad = Fad + (ja*JD + jd)*(MA*MD) *NTX;

            MprodT <MA,MD> (fad, RMA, RMD);
        }
    }

}

template<int la, int lb, int lc, int ld>
__global__ void X_BCAD (double * d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double * d_F,  int JA, int JB, int JC, int JD, int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;

    double * Dad = d_D + sAD*k*NTX;
    double * Fbc = d_F + sBC*k*NTX;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int ja=0; ja<JA; ++ja) {
        for (int jd=0; jd<JD; ++jd) {
            double * dad = Dad + (ja*JD + jd)*(MA*MD) *NTX;

            Mprod <MA,MD> (dad, RMA, RMD);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fbc = Fbc + (jb*JC + jc)*(MB*MC) *NTX;
                    double * dad = Dad + (ja*JD + jd)*(MA*MD) *NTX;

                    xBCAD <MA,MB,MC,MD> (fbc, ERI, dad);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int jb=0; jb<JB; ++jb) {
        for (int jc=0; jc<JC; ++jc) {
            double * fbc = Fbc + (jb*JC + jc)*(MB*MC) *NTX;

            MprodT <MB,MC> (fbc, RMB, RMC);
        }
    }


}

template<int la, int lb, int lc, int ld>
__global__ void X_ACBD (double * d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double * d_F,  int JA, int JB, int JC, int JD, int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = JA*JC*MA*MC;
    const int sBD = JB*JD*MB*MD;

    double * Dbd = d_D + sBD*k*NTX;
    double * Fac = d_F + sAC*k*NTX;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int jb=0; jb<JB; ++jb) {
        for (int jd=0; jd<JD; ++jd) {
            double * dbd = Dbd + (jb*JD + jd)*(MB*MD) *NTX;

            Mprod <MB,MD> (dbd, RMB, RMD);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * fac = Fac + (ja*JC + jc)*(MA*MC) *NTX;
                    double * dbd = Dbd + (jb*JD + jd)*(MB*MD) *NTX;

                    xACBD <MA,MB,MC,MD> (fac, ERI, dbd);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int ja=0; ja<JA; ++ja) {
        for (int jc=0; jc<JC; ++jc) {
            double * fac = Fac + (ja*JC + jc)*(MA*MC) *NTX;

            MprodT <MA,MC> (fac, RMA, RMC);
        }
    }

}

template<int la, int lb, int lc, int ld>
__global__ void X_BDAC (double * d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double * d_F,  int JA, int JB, int JC, int JD, int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = JA*JC*MA*MC;
    const int sBD = JB*JD*MB*MD;

    double * Dac = d_D + sAC*k*NTX;
    double * Fbd = d_F + sBD*k*NTX;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (JA*JB*JC*JD)*(MA*MB*MC*MD)*k*NTX;



    // rotate density matrix
    for (int ja=0; ja<JA; ++ja) {
        for (int jc=0; jc<JC; ++jc) {
            double * dac = Dac + (ja*JC + jc)*(MA*MC) *NTX;

            Mprod <MA,MC> (dac, RMA, RMC);
        }
    }


    // digestion (contraction)
    // ***********************

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {

                    double * dac = Dac + (ja*JC + jc)*(MA*MC) *NTX;
                    double * fbd = Fbd + (jb*JD + jd)*(MB*MD) *NTX;

                    xBDAC <MA,MB,MC,MD> (fbd, ERI, dac);

                    ERI += (MA*MB*MC*MD) * NTX;
                }
            }
        }
    }

    // rotate fock matrices
    for (int jb=0; jb<JB; ++jb) {
        for (int jd=0; jd<JD; ++jd) {
            double * fbd = Fbd + (jb*JD + jd)*(MB*MD) *NTX;

            MprodT <MB,MD> (fbd, RMB, RMD);
        }
    }


}



void BatchEvaluator::ContractDJs  (const ERIBatch & TheBatch, cudaStream_t * stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    //CONTRACT WITH DENSITY MATRIX
    {
        int Ntiles = TheBatch.NtilesN;
        const int NNY = 4;

        int NYtiles = (Ntiles+NNY-1)/NNY;

        dim3 Nbl (  1, NYtiles);
        dim3 Nth (NTX, NNY);

        if           (la==0 && lb==0 && lc==0 && ld==0) {
            J_CDAB <0,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==0) {
            J_CDAB <0,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==1) {
            J_CDAB <0,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==0) {
            J_CDAB <0,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==1) {
            J_CDAB <0,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==2) {
            J_CDAB <0,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==0 && ld==0) {
            J_CDAB <1,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==0) {
            J_CDAB <1,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==1) {
            J_CDAB <1,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==0) {
            J_CDAB <1,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==1) {
            J_CDAB <1,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==2) {
            J_CDAB <1,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==0 && ld==0) {
            J_CDAB <1,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==0) {
            J_CDAB <1,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==1) {
            J_CDAB <1,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==0) {
            J_CDAB <1,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==1) {
            J_CDAB <1,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==2) {
            J_CDAB <1,1,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==0 && ld==0) {
            J_CDAB <2,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==0) {
            J_CDAB <2,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==1) {
            J_CDAB <2,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==0) {
            J_CDAB <2,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==1) {
            J_CDAB <2,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==2) {
            J_CDAB <2,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==0 && ld==0) {
            J_CDAB <2,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==0) {
            J_CDAB <2,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==1) {
            J_CDAB <2,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==0) {
            J_CDAB <2,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==1) {
            J_CDAB <2,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==2) {
            J_CDAB <2,1,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==0 && ld==0) {
            J_CDAB <2,2,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==0) {
            J_CDAB <2,2,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==1) {
            J_CDAB <2,2,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==0) {
            J_CDAB <2,2,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==1) {
            J_CDAB <2,2,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==2) {
            J_CDAB <2,2,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
}

void BatchEvaluator::ContractDXs  (const ERIBatch & TheBatch, cudaStream_t * stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;


    //CONTRACT WITH DENSITY MATRIX
    {
        int Ntiles = TheBatch.NtilesN;
        const int NNY = 4;

        int NYtiles = (Ntiles+NNY-1)/NNY;

        dim3 Nbl (  1, NYtiles);
        dim3 Nth (NTX, NNY);

        if           (la==0 && lb==0 && lc==0 && ld==0) {
            X_ADBC <0,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==0) {
            X_ADBC <0,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==1) {
            X_ADBC <0,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==0) {
            X_ADBC <0,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==1) {
            X_ADBC <0,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==2) {
            X_ADBC <0,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==0 && ld==0) {
            X_ADBC <1,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==0) {
            X_ADBC <1,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==1) {
            X_ADBC <1,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==0) {
            X_ADBC <1,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==1) {
            X_ADBC <1,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==2) {
            X_ADBC <1,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==0 && ld==0) {
            X_ADBC <1,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==0) {
            X_ADBC <1,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==1) {
            X_ADBC <1,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==0) {
            X_ADBC <1,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==1) {
            X_ADBC <1,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==2) {
            X_ADBC <1,1,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==0 && ld==0) {
            X_ADBC <2,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==0) {
            X_ADBC <2,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==1) {
            X_ADBC <2,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==0) {
            X_ADBC <2,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==1) {
            X_ADBC <2,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==2) {
            X_ADBC <2,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==0 && ld==0) {
            X_ADBC <2,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==0) {
            X_ADBC <2,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==1) {
            X_ADBC <2,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==0) {
            X_ADBC <2,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==1) {
            X_ADBC <2,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==2) {
            X_ADBC <2,1,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==0 && ld==0) {
            X_ADBC <2,2,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==0) {
            X_ADBC <2,2,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==1) {
            X_ADBC <2,2,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==0) {
            X_ADBC <2,2,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==1) {
            X_ADBC <2,2,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==2) {
            X_ADBC <2,2,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
}

void BatchEvaluator::ContractDJXs (const ERIBatch & TheBatch, cudaStream_t * stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    //CONTRACT WITH DENSITY MATRIX
    {
        int Ntiles = TheBatch.NtilesN;
        const int NNY = 4;

        int NYtiles = (Ntiles+NNY-1)/NNY;

        dim3 Nbl (  1, NYtiles);
        dim3 Nth (NTX, NNY);

        if           (la==0 && lb==0 && lc==0 && ld==0) {
            J_CDAB <0,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==0) {
            J_CDAB <0,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==1) {
            J_CDAB <0,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==0) {
            J_CDAB <0,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==1) {
            J_CDAB <0,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==2) {
            J_CDAB <0,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==0 && ld==0) {
            J_CDAB <1,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==0) {
            J_CDAB <1,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==1) {
            J_CDAB <1,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==0) {
            J_CDAB <1,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==1) {
            J_CDAB <1,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==2) {
            J_CDAB <1,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==0 && ld==0) {
            J_CDAB <1,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==0) {
            J_CDAB <1,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==1) {
            J_CDAB <1,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==0) {
            J_CDAB <1,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==1) {
            J_CDAB <1,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==2) {
            J_CDAB <1,1,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==0 && ld==0) {
            J_CDAB <2,0,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==0) {
            J_CDAB <2,0,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==1) {
            J_CDAB <2,0,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==0) {
            J_CDAB <2,0,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==1) {
            J_CDAB <2,0,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==2) {
            J_CDAB <2,0,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==0 && ld==0) {
            J_CDAB <2,1,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==0) {
            J_CDAB <2,1,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==1) {
            J_CDAB <2,1,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==0) {
            J_CDAB <2,1,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==1) {
            J_CDAB <2,1,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==2) {
            J_CDAB <2,1,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==0 && ld==0) {
            J_CDAB <2,2,0,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,0,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,0,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,0,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,0,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,0,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==0) {
            J_CDAB <2,2,1,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,1,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==1) {
            J_CDAB <2,2,1,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,1,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==0) {
            J_CDAB <2,2,2,0> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,0> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,0> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,0> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,0> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,0> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==1) {
            J_CDAB <2,2,2,1> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,1> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,1> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,1> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,1> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,1> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==2) {
            J_CDAB <2,2,2,2> <<<Nbl,Nth,0,stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,2> <<<Nbl,Nth,0,stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,2> <<<Nbl,Nth,0,stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,2> <<<Nbl,Nth,0,stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,2> <<<Nbl,Nth,0,stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,2> <<<Nbl,Nth,0,stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
}

void BatchEvaluator::ContractDJXs (const ERIBatch & TheBatch, cudaStream_t ** stream) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    //CONTRACT WITH DENSITY MATRIX
    {
        int Ntiles = TheBatch.NtilesN;
        const int NNY = 4;

        int NYtiles = (Ntiles+NNY-1)/NNY;

        dim3 Nbl (  1, NYtiles);
        dim3 Nth (NTX, NNY);

        if           (la==0 && lb==0 && lc==0 && ld==0) {
            J_CDAB <0,0,0,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,0,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,0,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,0,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,0,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,0,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==0) {
            J_CDAB <0,0,1,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,1,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==1) {
            J_CDAB <0,0,1,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,1,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,1,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,1,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,1,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,1,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==0) {
            J_CDAB <0,0,2,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==1) {
            J_CDAB <0,0,2,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==2) {
            J_CDAB <0,0,2,2> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <0,0,2,2> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <0,0,2,2> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <0,0,2,2> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <0,0,2,2> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <0,0,2,2> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==0 && ld==0) {
            J_CDAB <1,0,0,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,0,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,0,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,0,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,0,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,0,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==0) {
            J_CDAB <1,0,1,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,1,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==1) {
            J_CDAB <1,0,1,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,1,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,1,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,1,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,1,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,1,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==0) {
            J_CDAB <1,0,2,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==1) {
            J_CDAB <1,0,2,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==2) {
            J_CDAB <1,0,2,2> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,0,2,2> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,0,2,2> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,0,2,2> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,0,2,2> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,0,2,2> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==0 && ld==0) {
            J_CDAB <1,1,0,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,0,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,0,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,0,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,0,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,0,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==0) {
            J_CDAB <1,1,1,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,1,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==1) {
            J_CDAB <1,1,1,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,1,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,1,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,1,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,1,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,1,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==0) {
            J_CDAB <1,1,2,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==1) {
            J_CDAB <1,1,2,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==2) {
            J_CDAB <1,1,2,2> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <1,1,2,2> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <1,1,2,2> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <1,1,2,2> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <1,1,2,2> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <1,1,2,2> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==0 && ld==0) {
            J_CDAB <2,0,0,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,0,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,0,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,0,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,0,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,0,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==0) {
            J_CDAB <2,0,1,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,1,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==1) {
            J_CDAB <2,0,1,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,1,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,1,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,1,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,1,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,1,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==0) {
            J_CDAB <2,0,2,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==1) {
            J_CDAB <2,0,2,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==2) {
            J_CDAB <2,0,2,2> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,0,2,2> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,0,2,2> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,0,2,2> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,0,2,2> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,0,2,2> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==0 && ld==0) {
            J_CDAB <2,1,0,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,0,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,0,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,0,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,0,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,0,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==0) {
            J_CDAB <2,1,1,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,1,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==1) {
            J_CDAB <2,1,1,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,1,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,1,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,1,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,1,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,1,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==0) {
            J_CDAB <2,1,2,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==1) {
            J_CDAB <2,1,2,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==2) {
            J_CDAB <2,1,2,2> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,1,2,2> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,1,2,2> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,1,2,2> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,1,2,2> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,1,2,2> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==0 && ld==0) {
            J_CDAB <2,2,0,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,0,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,0,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,0,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,0,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,0,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==0) {
            J_CDAB <2,2,1,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,1,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==1) {
            J_CDAB <2,2,1,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,1,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,1,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,1,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,1,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,1,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==0) {
            J_CDAB <2,2,2,0> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,0> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,0> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,0> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,0> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,0> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==1) {
            J_CDAB <2,2,2,1> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,1> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,1> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,1> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,1> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,1> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==2) {
            J_CDAB <2,2,2,2> <<<Nbl,Nth,0,*stream[1]>>> (b4blockGPU.dDab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFcd, JA,JB,JC,JD,  Ntiles);
            J_ABCD <2,2,2,2> <<<Nbl,Nth,0,*stream[0]>>> (b4blockGPU.dDcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFab, JA,JB,JC,JD,  Ntiles);
            X_ADBC <2,2,2,2> <<<Nbl,Nth,0,*stream[3]>>> (b4blockGPU.dDbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFad, JA,JB,JC,JD,  Ntiles);
            X_ACBD <2,2,2,2> <<<Nbl,Nth,0,*stream[2]>>> (b4blockGPU.dDbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFac, JA,JB,JC,JD,  Ntiles);
            X_BCAD <2,2,2,2> <<<Nbl,Nth,0,*stream[4]>>> (b4blockGPU.dDad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbc, JA,JB,JC,JD,  Ntiles);
            X_BDAC <2,2,2,2> <<<Nbl,Nth,0,*stream[5]>>> (b4blockGPU.dDac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dFbd, JA,JB,JC,JD,  Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
}




template<int la, int lb, int lc, int ld>
__global__ void J_CDAB (double ** d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double ** d_F,  int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = MA*MB;
    const int sCD = MC*MD;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (MA*MB*MC*MD)*k*NTX;

    __shared__ double dab[MA*MB*NTX];
    __shared__ double fcd[MC*MD*NTX];

    //load density
    for (int mab=0; mab<MA*MB; ++mab)
        dab[mab*NTX+i] = d_D[k*NTX+i][mab];
    __syncthreads();

    // rotate density matrices
    Mprod <MA,MB> (dab, RMA, RMB);

    // digestion (contraction)
    // ***********************
    jCDAB <MA,MB,MC,MD> (fcd, ERI, dab);

    MprodT <MC,MD> (fcd, RMC, RMD);

    for (int mcd=0; mcd<MC*MD; ++mcd)
        d_F[k*NTX+i][mcd] = fcd[mcd*NTX+i];
}

template<int la, int lb, int lc, int ld>
__global__ void J_ABCD (double ** d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double ** d_F,  int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = MA*MB;
    const int sCD = MC*MD;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (MA*MB*MC*MD)*k*NTX;

    __shared__ double dcd[MC*MD*NTX];
    __shared__ double fab[MA*MB*NTX];

    //load density
    for (int mcd=0; mcd<MC*MD; ++mcd)
        dcd[mcd*NTX+i] = d_D[k*NTX+i][mcd];
    __syncthreads();

    // rotate density matrix
    Mprod <MC,MD> (dcd, RMC, RMD);

    // digestion (contraction)
    // ***********************
    jABCD <MA,MB,MC,MD> (fab, ERI, dcd);

    // rotate fock matrices
    MprodT <MA,MB> (fab, RMA, RMB);

    for (int mab=0; mab<MA*MB; ++mab)
        d_F[k*NTX+i][mab] = fab[mab*NTX+i];
}



template<int la, int lb, int lc, int ld>
__global__ void X_ADBC (double ** d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double ** d_F,  int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAD = MA*MD;
    const int sBC = MB*MC;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (MA*MB*MC*MD)*k*NTX;

    __shared__ double dbc[MB*MC*NTX];
    __shared__ double fad[MA*MD*NTX];

    //load density
    for (int mbc=0; mbc<MB*MC; ++mbc)
        dbc[mbc*NTX+i] = d_D[k*NTX+i][mbc];
    __syncthreads();

    // rotate density matrix
    Mprod <MB,MC> (dbc, RMB, RMC);

    // digestion (contraction)
    // ***********************
    xADBC <MA,MB,MC,MD> (fad, ERI, dbc);

    // rotate fock matrices
    MprodT <MA,MD> (fad, RMA, RMD);

    for (int mad=0; mad<MA*MD; ++mad)
        d_F[k*NTX+i][mad] = fad[mad*NTX+i];
}

template<int la, int lb, int lc, int ld>
__global__ void X_BCAD (double ** d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double ** d_F,  int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAD = MA*MD;
    const int sBC = MB*MC;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (MA*MB*MC*MD)*k*NTX;

    __shared__ double dad[MA*MD*NTX];
    __shared__ double fbc[MB*MC*NTX];

    //load density
    for (int mad=0; mad<MA*MD; ++mad)
        dad[mad*NTX+i] = d_D[k*NTX+i][mad];
    __syncthreads();


    // rotate density matrix
    Mprod <MA,MD> (dad, RMA, RMD);

    // digestion (contraction)
    // ***********************
    xBCAD <MA,MB,MC,MD> (fbc, ERI, dad);

    // rotate fock matrices
    MprodT <MB,MC> (fbc, RMB, RMC);

    for (int mbc=0; mbc<MB*MC; ++mbc)
        d_F[k*NTX+i][mbc] = fbc[mbc*NTX+i];
}

template<int la, int lb, int lc, int ld>
__global__ void X_ACBD (double ** d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double ** d_F,  int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = MA*MC;
    const int sBD = MB*MD;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (MA*MB*MC*MD)*k*NTX;

    __shared__ double dbd[MB*MD*NTX];
    __shared__ double fac[MA*MC*NTX];

    //load density
    for (int mbd=0; mbd<MB*MD; ++mbd)
        dbd[mbd*NTX+i] = d_D[k*NTX+i][mbd];
    __syncthreads();

    // rotate density matrix
    Mprod <MB,MD> (dbd, RMB, RMD);

    // digestion (contraction)
    // ***********************
    xACBD <MA,MB,MC,MD> (fac, ERI, dbd);

    // rotate fock matrices
    MprodT <MA,MC> (fac, RMA, RMC);

    for (int mac=0; mac<MA*MC; ++mac)
        d_F[k*NTX+i][mac] = fac[mac*NTX+i];
}

template<int la, int lb, int lc, int ld>
__global__ void X_BDAC (double ** d_D, const double * d_ERI, const double * d_RMA, const double * d_RMB, const double * d_RMC, const double * d_RMD, double ** d_F,  int Ntiles) {

    int i = threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = MA*MC;
    const int sBD = MB*MD;

    const double * RMA = d_RMA + (MA*MA)*k*NTX;
    const double * RMB = d_RMB + (MB*MB)*k*NTX;
    const double * RMC = d_RMC + (MC*MC)*k*NTX;
    const double * RMD = d_RMD + (MD*MD)*k*NTX;

    const double * ERI = d_ERI + (MA*MB*MC*MD)*k*NTX;

    __shared__ double dac[MA*MC*NTX];
    __shared__ double fbd[MB*MD*NTX];

    //load density
    for (int mac=0; mac<MA*MC; ++mac)
        dac[mac*NTX+i] = d_D[k*NTX+i][mac];
    __syncthreads();

    // rotate density matrix
    Mprod <MA,MC> (dac, RMA, RMC);

    // digestion (contraction)
    // ***********************
    xBDAC <MA,MB,MC,MD> (fbd, ERI, dac);

    // rotate fock matrices
    MprodT <MB,MD> (fbd, RMB, RMD);

    for (int mbd=0; mbd<MB*MD; ++mbd)
        d_F[k*NTX+i][mbd] = fbd[mbd*NTX+i];
}



void BatchEvaluator::ContractDJXs (const ERIBatch & TheBatch, cudaStream_t ** stream,
					double ** d_Dab,  double ** d_Dcd,   double ** d_Dac, double ** d_Dad, double ** d_Dbc, double ** d_Dbd,
					double ** d_Fab,  double ** d_Fcd,   double ** d_Fac, double ** d_Fad, double ** d_Fbc, double ** d_Fbd) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    //CONTRACT WITH DENSITY MATRIX
    {
        int Ntiles = TheBatch.NtilesN;
        const int NNY = 1;

        int NYtiles = (Ntiles+NNY-1)/NNY;

        dim3 Nbl (  1, NYtiles);
        dim3 Nth (NTX, NNY);

        if           (la==0 && lb==0 && lc==0 && ld==0) {
            J_CDAB <0,0,0,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <0,0,0,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <0,0,0,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <0,0,0,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <0,0,0,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <0,0,0,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==0) {
            J_CDAB <0,0,1,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <0,0,1,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <0,0,1,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <0,0,1,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <0,0,1,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <0,0,1,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==0 && lb==0 && lc==1 && ld==1) {
            J_CDAB <0,0,1,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <0,0,1,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <0,0,1,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <0,0,1,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <0,0,1,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <0,0,1,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==0) {
            J_CDAB <0,0,2,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <0,0,2,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <0,0,2,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <0,0,2,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <0,0,2,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <0,0,2,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==1) {
            J_CDAB <0,0,2,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <0,0,2,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <0,0,2,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <0,0,2,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <0,0,2,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <0,0,2,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==0 && lb==0 && lc==2 && ld==2) {
            J_CDAB <0,0,2,2> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fcd, Ntiles);
            J_ABCD <0,0,2,2> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fab, Ntiles);
            X_ADBC <0,0,2,2> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fad, Ntiles);
            X_ACBD <0,0,2,2> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fac, Ntiles);
            X_BCAD <0,0,2,2> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbc, Ntiles);
            X_BDAC <0,0,2,2> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==0 && lc==0 && ld==0) {
            J_CDAB <1,0,0,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <1,0,0,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <1,0,0,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <1,0,0,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <1,0,0,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <1,0,0,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==0) {
            J_CDAB <1,0,1,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <1,0,1,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <1,0,1,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <1,0,1,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <1,0,1,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <1,0,1,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==0 && lc==1 && ld==1) {
            J_CDAB <1,0,1,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <1,0,1,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <1,0,1,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <1,0,1,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <1,0,1,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <1,0,1,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==0) {
            J_CDAB <1,0,2,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <1,0,2,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <1,0,2,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <1,0,2,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <1,0,2,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <1,0,2,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==1) {
            J_CDAB <1,0,2,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <1,0,2,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <1,0,2,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <1,0,2,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <1,0,2,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <1,0,2,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==0 && lc==2 && ld==2) {
            J_CDAB <1,0,2,2> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fcd, Ntiles);
            J_ABCD <1,0,2,2> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fab, Ntiles);
            X_ADBC <1,0,2,2> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fad, Ntiles);
            X_ACBD <1,0,2,2> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fac, Ntiles);
            X_BCAD <1,0,2,2> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbc, Ntiles);
            X_BDAC <1,0,2,2> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==1 && lc==0 && ld==0) {
            J_CDAB <1,1,0,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <1,1,0,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <1,1,0,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <1,1,0,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <1,1,0,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <1,1,0,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==0) {
            J_CDAB <1,1,1,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <1,1,1,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <1,1,1,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <1,1,1,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <1,1,1,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <1,1,1,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==1 && lc==1 && ld==1) {
            J_CDAB <1,1,1,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <1,1,1,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <1,1,1,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <1,1,1,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <1,1,1,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <1,1,1,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==0) {
            J_CDAB <1,1,2,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <1,1,2,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <1,1,2,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <1,1,2,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <1,1,2,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <1,1,2,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==1) {
            J_CDAB <1,1,2,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <1,1,2,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <1,1,2,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <1,1,2,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <1,1,2,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <1,1,2,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==1 && lb==1 && lc==2 && ld==2) {
            J_CDAB <1,1,2,2> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fcd, Ntiles);
            J_ABCD <1,1,2,2> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fab, Ntiles);
            X_ADBC <1,1,2,2> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fad, Ntiles);
            X_ACBD <1,1,2,2> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fac, Ntiles);
            X_BCAD <1,1,2,2> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbc, Ntiles);
            X_BDAC <1,1,2,2> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==0 && lc==0 && ld==0) {
            J_CDAB <2,0,0,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,0,0,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,0,0,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,0,0,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,0,0,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,0,0,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==0) {
            J_CDAB <2,0,1,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,0,1,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,0,1,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,0,1,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,0,1,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,0,1,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==0 && lc==1 && ld==1) {
            J_CDAB <2,0,1,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <2,0,1,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <2,0,1,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <2,0,1,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <2,0,1,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <2,0,1,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==0) {
            J_CDAB <2,0,2,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,0,2,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,0,2,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,0,2,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,0,2,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,0,2,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==1) {
            J_CDAB <2,0,2,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <2,0,2,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <2,0,2,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <2,0,2,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <2,0,2,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <2,0,2,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==0 && lc==2 && ld==2) {
            J_CDAB <2,0,2,2> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fcd, Ntiles);
            J_ABCD <2,0,2,2> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fab, Ntiles);
            X_ADBC <2,0,2,2> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fad, Ntiles);
            X_ACBD <2,0,2,2> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fac, Ntiles);
            X_BCAD <2,0,2,2> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbc, Ntiles);
            X_BDAC <2,0,2,2> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==1 && lc==0 && ld==0) {
            J_CDAB <2,1,0,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,1,0,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,1,0,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,1,0,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,1,0,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,1,0,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==0) {
            J_CDAB <2,1,1,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,1,1,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,1,1,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,1,1,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,1,1,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,1,1,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==1 && lc==1 && ld==1) {
            J_CDAB <2,1,1,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <2,1,1,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <2,1,1,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <2,1,1,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <2,1,1,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <2,1,1,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==0) {
            J_CDAB <2,1,2,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,1,2,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,1,2,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,1,2,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,1,2,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,1,2,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==1) {
            J_CDAB <2,1,2,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <2,1,2,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <2,1,2,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <2,1,2,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <2,1,2,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <2,1,2,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==1 && lc==2 && ld==2) {
            J_CDAB <2,1,2,2> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fcd, Ntiles);
            J_ABCD <2,1,2,2> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fab, Ntiles);
            X_ADBC <2,1,2,2> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fad, Ntiles);
            X_ACBD <2,1,2,2> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fac, Ntiles);
            X_BCAD <2,1,2,2> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbc, Ntiles);
            X_BDAC <2,1,2,2> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==2 && lc==0 && ld==0) {
            J_CDAB <2,2,0,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,2,0,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,2,0,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,2,0,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,2,0,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,2,0,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==0) {
            J_CDAB <2,2,1,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,2,1,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,2,1,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,2,1,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,2,1,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,2,1,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==2 && lc==1 && ld==1) {
            J_CDAB <2,2,1,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <2,2,1,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <2,2,1,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <2,2,1,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <2,2,1,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <2,2,1,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==0) {
            J_CDAB <2,2,2,0> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fcd, Ntiles);
            J_ABCD <2,2,2,0> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fab, Ntiles);
            X_ADBC <2,2,2,0> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fad, Ntiles);
            X_ACBD <2,2,2,0> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fac, Ntiles);
            X_BCAD <2,2,2,0> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbc, Ntiles);
            X_BDAC <2,2,2,0> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMS8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==1) {
            J_CDAB <2,2,2,1> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fcd, Ntiles);
            J_ABCD <2,2,2,1> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fab, Ntiles);
            X_ADBC <2,2,2,1> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fad, Ntiles);
            X_ACBD <2,2,2,1> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fac, Ntiles);
            X_BCAD <2,2,2,1> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbc, Ntiles);
            X_BDAC <2,2,2,1> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMP8, d_Fbd, Ntiles);
        }
        else if      (la==2 && lb==2 && lc==2 && ld==2) {
            J_CDAB <2,2,2,2> <<<Nbl,Nth,0,*stream[1]>>> (d_Dab, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fcd, Ntiles);
            J_ABCD <2,2,2,2> <<<Nbl,Nth,0,*stream[0]>>> (d_Dcd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fab, Ntiles);
            X_ADBC <2,2,2,2> <<<Nbl,Nth,0,*stream[3]>>> (d_Dbc, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fad, Ntiles);
            X_ACBD <2,2,2,2> <<<Nbl,Nth,0,*stream[2]>>> (d_Dbd, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fac, Ntiles);
            X_BCAD <2,2,2,2> <<<Nbl,Nth,0,*stream[4]>>> (d_Dad, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbc, Ntiles);
            X_BDAC <2,2,2,2> <<<Nbl,Nth,0,*stream[5]>>> (d_Dac, b4blockGPU.dT8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, b4blockGPU.dRMD8, d_Fbd, Ntiles);
        }
        else {
            cout << "couldn't find contraction kernel!!!"  << endl;
        }

    }
}
