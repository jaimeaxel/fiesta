



#ifndef __Fock2eS__
#define __Fock2eS__

#include <set>

#include "defs.hpp"
#include "low/rtensors.hpp"

#include "EFS/BatchInfo.hpp"

#include "linear/tensors.hpp"

const bool PrintStatistics = true;

class ShellPairPrototype;
class APlists;

class BatchEvaluator;
class ERIblock;
class ERIbatchBuffer;

class Sparse;
class SparseGPU;
class sparsetensorpattern;

template <class T> class r2tensor;
template <class T> class r1tensor;

class rAtom;

class Npattern; // internal format for matrices
class NTconst;
class NTmulti;

//classes for managing concurrent algorithms

class FockJobQueue {
    friend class Fock2e;

  private:
    bool Initialized;
    std::set<BatchInfo> JobQueue;

  public:

    FockJobQueue() {
        Initialized = false;
    }

    void push(BatchInfo & Info) {
        JobQueue.insert(Info);
    }

    void Done() {
        Initialized = true;
    }

    void Clear() {
        if (Initialized) {
            Initialized = false;
            JobQueue.clear();
        }
    }

};

template<class T> class TaskQueue;

template<class T> class ConstTaskQueue;


//this class handles the 3electron contribution to the Fock matrix
//****************************************************************
class Fock2e {

    friend class ERIblock;
    friend class BatchEvaluator;

  private:

    uint64_t max_at_int;  //maximum number of atom-atom interactions to be considered
    uint16_t OGMT;        //number of threads

    // this was const; we had to change it in order to be able to initialize CD matrix positions in SPpool
    APlists            * APL; //pointers to required data

    int nDMs;            // number of density matrices of input

    //(anti)symmetrized sparse matrices
    SparseGPU * SDsparseGPU;
    SparseGPU * ADsparseGPU;

    //input and output
    r2tensor<r2tensor<float>> logDs;
    r2tensor<r2tensor<float>> logAD;

    // substitutes
    b2screener lnDs;
    b2screener lnAD;

    // maybe it would be a better idea to be able to control things
    // like XS, prescreening, CASE and CAM parameters, symmetry, etc.
    // collectively or independently for each input matrix, if needed

    double XS;                  // fraction of exchange to consider
    float  logmover;            //-log of the prescreen threshold
    bool   GeneralContraction;
    bool   firstcycle;          //some lazy initializers required in the first call to the method
    bool   UseGPU;

    // CAM DFT parameters
    bool   CAMDFT;              // is it CAM-DFT?
    double alpha;
    double beta;
    double mu;


    //multithraded classes for handling each of the steps of the generation of the Fock matrix
    FockJobQueue TwoElectronJobs;
    ERIbatchBuffer * EbatchBuffer;

    ERIblock       * Blocks;
    BatchEvaluator * BatchEval;


    // these two have a great degree of overlap,
    // suggesting that the classes be merged
    const sparsetensorpattern * STP;
    Npattern * NTP;                  // template for tensors


    // for Cholesky
    struct  {
        tensor2 vecs;
        double * sigma = NULL;
        int NVECS = 0;
        int NDIM  = 0;

        // determine how many basis elements are to be used for a given target precision
        int Nvectors (double precision) {

            if (precision<=0.) return NVECS;

            // compute the trace of the ERI tensor
            double tr2; {
                tr2 = 0;
                for (int m=0; m<vecs.n; ++m)
                    tr2 += sigma[m] * sigma[m];
            }


            // find the M such that the factorization
            // will not incur in a relative error larger
            // than tr2
            const double thresh = precision * tr2;

            double s2=0;

            int MM;

            for (MM = vecs.n; MM>0; --MM) {
                s2 += sigma[MM-1] * sigma[MM-1];
                if (s2>=thresh) break;
            }

            /*
            for (MM = vecs.n; MM>0; --MM) {
                double tr2 = sigma[MM-1] * sigma[MM-1];
                if (tr2>=precision) break;
            }
            */

            return MM;

        }

    } CDdata;




    // initializers, etc

    void InitERIroutines();
    void InitJobQueue(); //just once per molecule should suffice


    // different forms of screening; would be a good idea to control them from the function call

    void CalcDensityPrescreeners   (const NTconst & DS, const NTconst & DA);
    void DensityFittingPrescreener (const NTconst & DS);
    void SetZeroDensityPrescreener ();
    void CheapDensityPrescreener   ();  // for initial guess, mostly. only uses up to 2-center integrals
    void MediumDensityPrescreener  ();

    void CholeskyPrescreener       (const float logCS);


    // there is a great degree of functional overlap across the schedulers; ideally we could merge them

    template <bool J, bool X>  void ParallelScheduler       (float logD, float logS);
    template <bool J, bool X>  void HybridParallelScheduler (float logD, float logS, const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant);
    template <bool J, bool X>  void HybridParallelScheduler (float logD, float logS, const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant,
                                                                                     const Sparse * Ds, const Sparse * Da, SparseGPU * gDs, SparseGPU * gDa, bool * Antisymm, int Nmat);


    void TensorParallelScheduler      (tensor4 & W, float T); // to compute full ERI tensor

    // for Cholesky decomposition
    void DiagonalParallelScheduler    (tensor2           & W,  float T);
    void ABABParallelScheduler        (r2tensor<tensor4> & W2, float T);
    void TwoCenterParallelScheduler   (r2tensor<tensor4> & W2, float T);
    void ThreeCenterParallelScheduler (r1tensor<tensor4> & W3, float T);
    void CholeskyParallelScheduler    (tensor2           & W2, float T);


    //
    void AddCoulomb(Sparse & F, int at, int wa, const double * const T, const Sparse & D);
    void AddXchange(Sparse & F, int at, int wa, const double * const T, const Sparse & D);

    void Trim(Sparse & F);


  public:

    // initializer functions:

    Fock2e();

    void Init (const rAtom * atoms, int natoms); // a different initialized

    void Init (uint32_t nInteractingAtoms, APlists & pAPL, const sparsetensorpattern & SparseTensorPattern, rAtom * const * AtomListGpus); //iniciador
    void SetXS  (double Xscaling);
    void SetCAM (double ialpha, double ibeta, double imu);
    void CalcCauchySwarz();


    // ERI contraction and other ERI-related functions:

    void FockUpdate        (tensor2 * F, const tensor2 * D2, int Nmat, double Dthresh, double Fthresh, bool SplitJX = false);
    void ContractDensities (tensor2 * J, tensor2 * K, tensor2 * A, const tensor2 * D, int Nmat,    double Dthresh, double Fthresh, bool split=true);


    void FactorCholesky  (double thresh=0.);

    void Cholesky        (double thresh=0.);

    void CDtransform   (const tensor2 & CMO); // transform CD/SVD/ev to MO


    // all of the following should be const, except we need a const_tensor class to recast the arrays
    void ContractCholesky (tensor2 * J, const tensor2 * D, int Nmat, double precision=1.e-10);
    void ComputeDiagonal         (tensor2 & D, const tensor2 & C, double precision=1.e-10);
    void ComputeDiagonalExchange (tensor2 & D, const tensor2 & C, double precision=1.e-10);

    void GetIIJJ (tensor2 & D,            double precision = 1.e-10);
    void GetIJIJ (tensor2 & D,            double precision = 1.e-10);
    void GetIJNN (tensor2 & D, int64_t n, double precision = 1.e-10);

    void CoulombCholesky  (tensor2 & W, const tensor2 & U, const tensor2 & V, double precision = 1.e-10);
    void ExchangeCholesky (tensor3 & W, const tensor2 & U, const tensor2 & V, double precision = 1.e-10);
    void ExchangeCholesky (tensor3 & W, const tensor2 & U, double precision = 1.e-10);


    void GetCholesky (tensor2 * V, int nO, int nV, int & M, double precision=1.e-10);

    // for MP2 & higher order methods
    void CalcW2basis (tensor2 * Ws);
    void Get2eTensor (tensor4 & W2, double thresh=0.);

    // deleters & destroyers:
    void Clear();
    ~Fock2e();
};

void ConstructBigW(const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, int wa, const double * w, double * W);

#endif
