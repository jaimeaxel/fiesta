#ifndef __BatchInfo__
#define __BatchInfo__

#include "defs.hpp"

class BlockBuffer;
class ERIblock;
class Fock2e;
class BatchEvaluator;

class ShellPairPrototype;
class AtomProd;
template <class T> class Array;
namespace LibQuimera {class Qalgorithm;}
class ShellPair;


//pointers to functions and t_sizes uniquely dependant on geometry and shell pair 'prototypes'
class BatchInfo {

    friend class BlockBuffer;
    friend class ERIblock;
    friend class Fock2e;

  protected:

    const Fock2e * pFock;

  public:

    const LibQuimera::Qalgorithm * ERIalgorithm; // pointer to function implementing the ERI

    const ShellPairPrototype * ABp;
    const ShellPairPrototype * CDp;

    //window arguments
    //****************

    //atom lists
    const Array<AtomProd> * AP12list;
    const Array<AtomProd> * AP34list;

    const ShellPair * SP12;
    const ShellPair * SP34;

    uint32_t tmax12;
    uint32_t tmax34;


    //size of the ERI batch
    uint32_t NFLOPS; //approximate number of FLOPs

    uint32_t fsize;
    uint32_t msize4; //size of the kernel list (times number of ERI batches)
    uint32_t wsize4;

    uint16_t J4;
    uint16_t msize; //size of the kernel list
    uint16_t wsize;


    // geometry, angular momenta, equality checks
    uint8_t la;
    uint8_t lb;
    uint8_t lc;
    uint8_t ld;
    uint8_t Lt;
    uint8_t Lmax;

    GEOM geometry;

    bool SkipSame;
    bool SameShell;
    bool SameList;

    //to resume
    //*********
    struct {
        uint32_t storeAB;
        uint32_t storeCD;
        uint16_t storeABb;
        uint16_t storeCDb;
        bool storeBlock;
        bool store3center;
    } State;


//functions to set geometry, etc.
  public:
    BatchInfo() {}
    BatchInfo(const Fock2e * fock) {pFock = fock;}

    void Set(GEOM geom, const ShellPairPrototype & AB, const ShellPairPrototype & CD, bool samef);
    void SetLists(const ShellPair * ABs, const ShellPair * CDs, const Array<AtomProd> * ABap, const Array<AtomProd> * CDap, uint32_t tmax12, uint32_t tmax34, bool skipsame);
    void SetLists(const ShellPair * ABs,                        const Array<AtomProd> * ABap                              , uint32_t tmax12,                  bool skipsame);

    uint64_t MaxNTiles(size_t MEM) const;

    bool operator<(const BatchInfo & rhs) const;
};


#include "low/rtensors.hpp"

struct a2screen {
    r2tensor<float> logs;

    inline const float & operator()(int i, int j) const {
        return logs(i,j);
    }

    inline float & operator()(int i, int j) {
        return logs(i,j);
    }

    inline const float * operator[](int i) const {
        return logs[i];
    }

    inline float * operator[](int i) {
        return logs[i];
    }
};

typedef r2tensor< r2tensor<a2screen> > b2screener;
//typedef r2tensor<r2tensor<r2tensor<float>>> densityscreener;


#endif
