
// include this before anything else or else Intel MPI complains
#include "low/MPIwrap.hpp"

#include <cstdlib>
#include <fstream>
#include <cmath>
//#include <malloc/malloc.h>
#include <stdlib.h>
using namespace std;

#include "defs.hpp"

#include "ERIblocks.hpp"

#include "low/chrono.hpp"
#include "low/cache64.hpp"

#include "math/angular.hpp"
using namespace LibAngular;

#include "math/gamma.hpp"
using namespace LibIGamma;

#include "basis/atomprod.hpp"
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"

#include "EFS/fock2e.hpp"
#include "EFS/rotations.hpp"

#include "libquimera/libquimera.hpp"
using namespace LibQuimera;

#include "libquimera/ERIgeom.hpp"
#include "libechidna/libechidna.hpp"


#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
#endif

#define __KC__
#define __TR__
#define __JXC__





const uint16_t DPC = DOUBLES_PER_CACHE_LINE;
const uint16_t FPC = FLOATS_PER_CACHE_LINE;

const uint16_t MAXPC = DOUBLES_PER_BLOCK; //max(DOUBLES_PER_BLOCK, DPC, FPC);
const uint16_t DPB   = DOUBLES_PER_BLOCK;

//this is insane; it's declaring 32 times more memory than it's ever going to use
const uint64_t MAX_INTS_PER_BLOCK  = MAX_TILES_PER_BLOCK*MAXPC;


static const int MINR2 = 1e-20;


//find number of interactions in O(ln(N))
static uint32_t maxSP(float lmo, const ShellPair * CDs, uint32_t max) {
    uint32_t min = 0;

    while (max!=min) {
        uint32_t med = (min+max)/2;

        if (CDs[med].logCS > lmo) max = med;
        else                      min = med+1;
    }

    return max;
}


static inline uint8_t * align2cacheline64(uint8_t * p) {
    uintptr_t addr = (uintptr_t)(p); //convert to integer
    if (addr % CACHE_LINE_SIZE != 0) addr += CACHE_LINE_SIZE - addr % CACHE_LINE_SIZE; //align to next address multiple of 64
    return (uint8_t*)addr; //cast back to pointer
}

static inline size_t align2cacheline64(size_t offset) {
    if (offset % CACHE_LINE_SIZE != 0)
        offset += CACHE_LINE_SIZE - offset % CACHE_LINE_SIZE; //align to next address multiple of 64
    return offset;
}





//sets the general properties of the ERI block from the geometry and shell pair prototypes
void BatchInfo::Set(GEOM geom, const ShellPairPrototype & AB, const ShellPairPrototype & CD, bool samef) {
    //set prototypes and such
    ABp = &AB;
    CDp = &CD;

    la = ABp->l1;
    lb = ABp->l2;
    lc = CDp->l1;
    ld = CDp->l2;

    Lt = la + lb + lc + ld;
    uint8_t maxab = la>lb?la:lb;
    uint8_t maxcd = lc>ld?lc:ld;
    Lmax = maxab>maxcd?maxab:maxcd;

    SameShell = samef;


    //set geometry
    geometry = geom;

    if      (geometry==ABCD) ERIalgorithm = Q.SelectAlgorithm(ABCD,la,lb,lc,ld); // || geometry==ABAB
    else if (geometry==AACD) ERIalgorithm = Q.SelectAlgorithm(AACD,la,lb,lc,ld);
    else if (geometry==AACC) ERIalgorithm = Q.SelectAlgorithm(AACC,la,lb,lc,ld);
    else if (geometry==ABAB) ERIalgorithm = Q.SelectAlgorithm(ABAB,la,lb,lc,ld);
    else if (geometry==AAAA) ERIalgorithm = Q.SelectAlgorithm(AAAA,la,lb,lc,ld);


    //set values required for computing memory requirements
    J4     = ABp->Ja * ABp->Jb * CDp->Ja * CDp->Jb;
    Lt     = ABp->l1 + ABp->l2 + CDp->l1 + CDp->l2;
    fsize  = (ABp->Ka * ABp->Kb *CDp->Ka * CDp->Kb)*(Lt+1);
    msize  = ERIalgorithm->GetNKernels();
    wsize  = nmS[la]*nmS[lb]*nmS[lc]*nmS[ld];
    msize4 = J4*msize;
    wsize4 = J4*wsize;

    // these are not real, but are correct in the order;
    // it's just so we can establish a priority
    if      (geometry==ABCD || geometry==ABAD || geometry==ABAB) NFLOPS = fsize * (Lt+1);
    else if (geometry==AACD)                                     NFLOPS = fsize;
    else if (geometry==AACC)                                     NFLOPS = 1+fsize/2;
    else                                                         NFLOPS = Lt+1;        // AAAA and such

    //NFLOPS *= J4; // WAAAAY too much


    //SetJXC();

    State.storeABb  = 0;
    State.storeCDb  = 0;
    State.storeAB   = 0;
    State.storeCD   = 0;
    State.storeBlock   = false;
    State.store3center = false;
}

void BatchInfo::SetLists(const ShellPair * ABs, const ShellPair * CDs, const Array<AtomProd> * ABap, const Array<AtomProd> * CDap, uint32_t max12, uint32_t max34, bool skipsame) {
    AP12list = ABap;
    AP34list = CDap;
    SP12 = ABs;
    SP34 = CDs;
    SkipSame = skipsame;
    SameList = false;

    tmax12 = max12;
    tmax34 = max34;
}

void BatchInfo::SetLists(const ShellPair * ABs, const Array<AtomProd> * ABap, uint32_t max12, bool skipsame) {
    AP12list = ABap;
    AP34list = ABap;
    SP12 = ABs;
    SP34 = ABs;
    SkipSame = skipsame;
    SameList = true;

    tmax12 = max12;
    tmax34 = max12;
}

bool BatchInfo::operator<(const BatchInfo & rhs) const {

    // guesstimate the amount of work
    uint64_t P1 =     NFLOPS;
    uint64_t P2 = rhs.NFLOPS;

    P1 *= (    tmax12);
    P2 *= (rhs.tmax12);
    if (!    SameList) P1*=    tmax34;
    if (!rhs.SameList) P2*=rhs.tmax34;

    if (P1!=P2) return (P1>P2);

    // if equal, any way of breaking the tie is admissible
    if (geometry!=rhs.geometry)
        return (geometry<rhs.geometry);
    if (ABp!=rhs.ABp) return (ABp<rhs.ABp);
    if (CDp!=rhs.CDp) return (CDp<rhs.CDp);

    if (SameShell!=rhs.SameShell)
        return (SameShell);

    return false; // equal


    /*
    uint8_t gl, gr;
    if      (geometry==ABCD) gl = 1;
    else if (geometry==AACD) gl = 2;
    else if (geometry==ABAD) gl = 3;
    else if (geometry==AACC) gl = 4;
    else if (geometry==ABAB) gl = 5;
    else if (geometry==AAAA) gl = 6;
    else  gl = 0;

    if      (rhs.geometry==ABCD) gr = 1;
    else if (rhs.geometry==AACD) gr = 2;
    else if (rhs.geometry==ABAD) gl = 3;
    else if (rhs.geometry==AACC) gr = 4;
    else if (rhs.geometry==ABAB) gr = 5;
    else if (rhs.geometry==AAAA) gr = 6;
    else  gr = 0;


    if (gl!=gr) return (gl<gr);

    if (ABp->l1!=rhs.ABp->l1) return (ABp->l1<rhs.ABp->l1);
    if (ABp->l2!=rhs.ABp->l2) return (ABp->l2<rhs.ABp->l2);
    if (CDp->l1!=rhs.CDp->l1) return (CDp->l1<rhs.CDp->l1);
    if (CDp->l2!=rhs.CDp->l2) return (CDp->l2<rhs.CDp->l2);

    if (ABp->ka!=rhs.ABp->ka) return (ABp->ka<rhs.ABp->ka);
    if (ABp->kb!=rhs.ABp->kb) return (ABp->kb<rhs.ABp->kb);
    if (CDp->ka!=rhs.CDp->ka) return (CDp->ka<rhs.CDp->ka);
    if (CDp->kb!=rhs.CDp->kb) return (CDp->kb<rhs.CDp->kb);

    return (ABp<CDp);
    */
}



ERIblock::ERIblock() {
    min12l = NULL;
    max12l = NULL;
    min34l = NULL;
    max34l = NULL;

    SPPlist = NULL;
    R2 = NULL;

    ABlogs = NULL;
    CDlogs = NULL;

    nJs = 0;
    nXs = 0;
}

ERIblock::~ERIblock() {
    delete[] min12l;
    delete[] max12l;
    delete[] min34l;
    delete[] max34l;

    delete[] SPPlist;
    delete[] R2;

    delete[] ABlogs;
    delete[] CDlogs;
}

void ERIblock::Init(size_t max_at_int, ERIbatchBuffer * ERIBB, const sparsetensorpattern * P) {

    //in theory only a maximum of O(maxK2) extra lists should be needed
    //one per row/column + position id for the list

    //another possible scheme is to use log(2^N) intermediate tile arrays,
    //where the tiles' number of elements doubles at each step

    //anyway, this struct cannot be

    TS.Set(MAX_TILES_PER_BLOCK*MAXPC);


    min34l  = new uint32_t[max_at_int];
    max34l  = new uint32_t[max_at_int];

    min12l  = new uint32_t[max_at_int];
    max12l  = new uint32_t[max_at_int];

    ABlogs  = new SPJ[max_at_int];
    CDlogs  = new SPJ[max_at_int];

    EBB = ERIBB;

    STP = P;


    //these two can be ignored for GPU code; later on they'll be eliminated
    SPPlist = new ShellPairPair[MAX_INTS_PER_BLOCK*MAXPC]; // if every integral is put in a different box, DPC * NINT space is needed
    R2      = new double[MAX_TILES_PER_BLOCK*MAXPC]; // maximum possible number of SPP in the same block
}

void ERIblock::Reset() {
    nJs = 0;
    nXs = 0;
    mJs = 0;
    mXs = 0;
    tJs = 0;
    tXs = 0;
    pJs = 0;
    pXs = 0;
}

//default copy constructor plus initialization of some stuff
const ERIblock & ERIblock::operator=(const BatchInfo & rhs) {

    pFock = rhs.pFock;

    ABp = rhs.ABp;
    CDp = rhs.CDp;

    SkipSame  = rhs.SkipSame;
    SameShell = rhs.SameShell;
    SameList  = rhs.SameList;

    geometry  = rhs.geometry;

    la   = rhs.la;
    lb   = rhs.lb;
    lc   = rhs.lc;
    ld   = rhs.ld;
    Lt   = rhs.Lt;
    Lmax = rhs.Lmax;

    //size of the ERI batch
    J4     = rhs.J4;
    fsize  = rhs.fsize;

    msize  = rhs.msize;
    msize4 = rhs.msize4;

    wsize  = rhs.wsize;
    wsize4 = rhs.wsize4;

    //funciones necesarias
    //********************
    ERIalgorithm = rhs.ERIalgorithm;


    //window arguments
    //****************

    AP12list = rhs.AP12list;
    AP34list = rhs.AP34list;

    SP12 = rhs.SP12;
    SP34 = rhs.SP34;

    tmax12 = rhs.tmax12;
    tmax34 = rhs.tmax34;

    State = rhs.State;

    return *this;
}



//finds minimum distance between the segments AB and CD
double R2distance(const AtomProd & ABp, const AtomProd & CDp) {
    const point & A = ABp.A;
    const point & B = ABp.B;
    const point & C = CDp.A;
    const point & D = CDp.B;

    vector3 AB = B-A;
    vector3 AC = C-A;
    vector3 CD = D-C;

    double ab2  = AB*AB;
    double abcd = AB*CD;
    double cd2  = CD*CD;

    double acab = AC*AB;
    double accd = AC*CD;

    double dis = ab2*cd2 - abcd*abcd;

    double v = cd2*acab + abcd*accd;
    double w = ab2*accd + abcd*acab;

    point ABmin, CDmin;

    if      (v<=0)   ABmin = A;
    else if (v>=dis) ABmin = B;
    else             ABmin = A + (v/dis) * AB;

    if      (w<=0)   CDmin = C;
    else if (w>=dis) CDmin = D;
    else             CDmin = C + (w/dis) * CD;

    vector3 vmin = CDmin - ABmin;
    double R2 = vmin*vmin;
    if (R2<MINR2) R2=0;
    return (R2);
}


double CasePrescreener::LogShortCoulomb (double R2) {

    const double case_w2 = LibQuimera::Quimera::GetW2();

    double maxR2 = (vg_max-vg_step)/case_w2; //absolute maximum (any more gives an infinite logarithm)

    double logSC;

    double ** gamma = ::IncompleteGamma.gamma_table;

    //close enough to interact
    if (R2<maxR2) {
        double w2R2 = case_w2 * R2;

        double p = ivg_step*(w2R2-vg_min);
        int pos = int(p+0.5);
        double x0 = vg_min + vg_step*double(pos);
        double Ax1 = x0-w2R2;
        double Ax2 = 0.5 * Ax1*Ax1;
        double Ax3 = 0.33333333333333333333 * Ax1*Ax2;

        double G = (gamma[pos][1] + gamma[pos][2] * Ax1 + gamma[pos][3] * Ax2 + gamma[pos][4] * Ax3);

        G *= 2*sqrt(case_w2/PI);

        double S = 1/sqrt(R2) - G;

        logSC = -log(S);
    }
    else
        logSC = 100;

    return logSC;
}

void   CasePrescreener::InitLogsShort () {
     //absolute maximum (any more gives an infinite logarithm)

    double AR2 = maxR2 / double(NLogsShort);

    for (int i=0; i<NLogsShort; ++i) {
        double R2 = AR2 * NLogsShort;

        LogShort[i] = LogShortCoulomb(R2);
    }
}

double CasePrescreener::LogsShort (double R2) {
    if (R2>=maxR2) return 100;

    int p = (R2/maxR2)*NLogsShort;

    return LogShort[p]; // fast and cheap
}

//improve coulomb CASE prescreening
//use alternative CASE prescreening


void ERIblock::GenerateCasePrescreenedList(float logD, float logS,  const b2screener & logDs) {
/*
    double iwmin = 1./case_w2 + 1./ABp->kmin + 1./CDp->kmin;
    double Dmax2 = ShortCoulomb.CalcR2(1./iwmin, case_err);
    double Dmax = 2* sqrt(Dmax2);

    PWBoxing TheBoxingScheme;

    //compute position
    PW * SPD = new PW [tmax12];

    {
        double k1    = ABp->ka[0];
        double k2    = ABp->kb[0];
        double k12   = k1+k2;
        double ik12  = 1/k12;

        for (uint32_t ab=0; ab<tmax12; ++ab) {
            const AtomProd & AP12 = (*AP12list)[ab];

            SPD[ab].P.x = (k1*AP12.A.x + k2*AP12.B.x)*ik12;
            SPD[ab].P.y = (k1*AP12.A.y + k2*AP12.B.y)*ik12;
            SPD[ab].P.z = (k1*AP12.A.z + k2*AP12.B.z)*ik12;

            SPD[ab].w.n[0] = ab;
        }
    }

    //register everything
    TheBoxingScheme.InitFrom(SPD, tmax12, Dmax);

    delete[] SPD;



    //now compute interactions
    uint64_t Nprescreened = 0;

    {
        double k3    = CDp->ka[0];
        double k4    = CDp->kb[0];
        double k34   = k3+k4;
        double ik34  = 1/k34;

        const PWBox * IntBoxes[8];

        for (uint32_t cd=0; cd<tmax34; ++cd) {
            const AtomProd & AP34 = (*AP34list)[cd];

            point Q;

            Q.x = (k3*AP34.A.x + k4*AP34.B.x)*ik34;
            Q.y = (k3*AP34.A.y + k4*AP34.B.y)*ik34;
            Q.z = (k3*AP34.A.z + k4*AP34.B.z)*ik34;

            //retrieve the (maximally) 8 interacting boxes
            TheBoxingScheme.RetrieveBoxes(Q, IntBoxes);

            for (int nb=0; nb<8; ++nb) {
                if (IntBoxes[nb] == NULL) continue;

                //loop over the box' elements
                list<PW>::const_iterator it;

                for (it=IntBoxes[nb]->elements.begin(); it!=IntBoxes[nb]->elements.end(); ++it) {
                    PW P = *it;

                    //check whether it's necessary to compute
                    int ab = P.w.n[0];

                    if (cd>=max34l[ab])     continue;
                    if (SkipSame && ab==cd) continue;



                    uint32_t ata = SP12[ab].ata;
                    uint32_t atb = SP12[ab].atb;
                    uint32_t atc = SP34[cd].ata;
                    uint32_t atd = SP34[cd].atb;

                    uint16_t  fa = ABp->nb1;
                    uint16_t  fb = ABp->nb2;
                    uint16_t  fc = CDp->nb1;
                    uint16_t  fd = CDp->nb2;

                    //simple check (could be done better taking distance into account)
                    double wabcd = SP12[ab].logCS + SP34[cd].logCS;

                    double logJ;
                    logJ  = logDs[ata][atb][fa][fb] + logDs[atc][atd][fc][fd] + wabcd;

                    if (logJ<lmo)  {
                        ++Nprescreened;

                        uint16_t i = K2max12-SP12[ab].nK2; //>0
                        uint16_t j = K2max34-SP34[cd].nK2; //>0

                        ++SortedERIs(i,j).Nints;
                    }

                }
            }
        }

        InitBlocks();

        for (uint32_t cd=0; cd<tmax34; ++cd) {
            const AtomProd & AP34 = (*AP34list)[cd];

            point Q;

            Q.x = (k3*AP34.A.x + k4*AP34.B.x)*ik34;
            Q.y = (k3*AP34.A.y + k4*AP34.B.y)*ik34;
            Q.z = (k3*AP34.A.z + k4*AP34.B.z)*ik34;

            //retrieve the (maximally) 8 interacting boxes
            TheBoxingScheme.RetrieveBoxes(Q, IntBoxes);

            for (int nb=0; nb<8; ++nb) {
                if (IntBoxes[nb] == NULL) continue;

                //loop over the box' elements
                list<PW>::const_iterator it;

                for (it=IntBoxes[nb]->elements.begin(); it!=IntBoxes[nb]->elements.end(); ++it) {
                    PW P = *it;

                    //check whether it's necessary to compute
                    int ab = P.w.n[0];

                    if (cd>=max34l[ab])     continue;
                    if (SkipSame && ab==cd) continue;



                    uint32_t ata = SP12[ab].ata;
                    uint32_t atb = SP12[ab].atb;
                    uint32_t atc = SP34[cd].ata;
                    uint32_t atd = SP34[cd].atb;

                    uint16_t  fa = ABp->nb1;
                    uint16_t  fb = ABp->nb2;
                    uint16_t  fc = CDp->nb1;
                    uint16_t  fd = CDp->nb2;

                    //simple check (could be done better taking distance into account)
                    double wabcd = SP12[ab].logCS + SP34[cd].logCS;

                    double logJ;
                    logJ  = logDs[ata][atb][fa][fb] + logDs[atc][atd][fc][fd] + wabcd;

                    if ( logJ<lmo )  {
                        uint16_t i = K2max12-SP12[ab].nK2; //>0
                        uint16_t j = K2max34-SP34[cd].nK2; //>0

                        uint64_t t = SortedERIs(i,j).Iints;

                        SortedERIs(i,j).vList[t].n12 = ab;
                        SortedERIs(i,j).vList[t].n34 = cd;

                        ++SortedERIs(i,j).Iints;
                    }

                }
            }
        }
    }
*/
}

inline bool ERIblock::include(bool J, bool X, bool R, bool useJD, bool useJS, bool useXD, bool useXS) {
    bool useD = (J && useJD || X && useXD);
    bool useS = (J && useJS || X && useXS);


    return ( !R && useD ) || (R && useS && !useD); // || (R && (J && useJS || X && useXS) && !(J && useJD || X && useXD) ) );
    //return !R;
}


// G: generate integral list
// J: include Coulomb
// X: include Exchange
// R: single/double precision
// mode: 1:     only count ABAD integrals in the ABCD block
//       2:     skip ABAD-type integrals in ABCD block
//       other: count all integrals

template<bool G, bool J, bool X, bool R>           uint64_t ERIblock::PrescreenedListS(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS) {


    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & fab = logFs[ea][eb][fa][fb];
    const a2screen & fcd = logFs[ec][ed][fc][fd];
    const a2screen & fac = logFs[ea][ec][fa][fc];
    const a2screen & fbd = logFs[eb][ed][fb][fd];
    const a2screen & fad = logFs[ea][ed][fa][fd];
    const a2screen & fbc = logFs[eb][ec][fb][fc];

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];


    uint64_t LPL = 0;

    for (uint32_t ab=State.storeAB; ab<tmax12; ++ab) {
        if (!NodeGroup.CheckMod(ab)) continue;

        uint32_t ata = SP12[ab].ata;
        uint32_t atb = SP12[ab].atb;
        uint32_t atc = SP34[ab].ata;
        uint32_t atd = SP34[ab].atb;

        float wabcd = SP12[ab].logCS + SP34[ab].logCS;

        bool useJD, useJS, useXD, useXS;
        useJD = useJS = useXD = useXS = false;


        if (J) {
            //float logJa  = logFs[ata][atb][fa][fb] + logDs[atc][atd][fc][fd]; // + wabcd;
            //float logJb  = logDs[ata][atb][fa][fb] + logFs[atc][atd][fc][fd]; // + wabcd;
            //useJD = (logJa<logD || logJb<logD);
            //useJS = (logJa<logS || logJb<logS);

            float logJa  = fab[ata][atb] + dcd[atc][atd];
            float logJb  = dab[ata][atb] + fcd[atc][atd];

            float logJ = min(logJa, logJb) + wabcd;

            useJD = (logJ<logD);
            useJS = (logJ<logS);
        }
        if (X) {
            //float logX1a = logFs[ata][atc][fa][fc] + logDs[atb][atd][fb][fd]; // + wabcd;
            //float logX2a = logFs[ata][atd][fa][fd] + logDs[atb][atc][fb][fc]; // + wabcd;
            //float logX1b = logDs[ata][atc][fa][fc] + logFs[atb][atd][fb][fd]; // + wabcd;
            //float logX2b = logDs[ata][atd][fa][fd] + logFs[atb][atc][fb][fc]; // + wabcd;
            //useXD = (logX1a<logD || logX2a<logD || logX1b<logD || logX2b<logD);
            //useXS = (logX1a<logS || logX2a<logS || logX1b<logS || logX2b<logS);

            float logX1a = fac[ata][atc] + dbd[atb][atd];
            float logX2a = fad[ata][atd] + dbc[atb][atc];
            float logX1b = dac[ata][atc] + fbd[atb][atd];
            float logX2b = dad[ata][atd] + fbc[atb][atc];

            float logX = min (min(logX1a, logX2a), min(logX1b, logX2b)) + wabcd;

            useXD = (logX<logD);
            useXS = (logX<logS);
        }

        //generate the list
        if (G) {
            if (include(J,X,R,useJD,useJS,useXD,useXS)) {
                uint16_t i = K2max12-SP12[ab].nK2; //>0
                uint16_t j = K2max34-SP34[ab].nK2; //>0

                ShellPairPair SPP(ab,ab);
                SortedERIs(i,j).push(SPP);

                ++LPL;

                if (LPL==NINTS) {
                    State.storeAB = ab+1;
                    return LPL; //interrupt the thread
                }
            }
        }
        //only count integrals
        else {
            if (include(J,X,R,useJD,useJS,useXD,useXS)) {
                ++LPL;

                uint16_t i = K2max12-SP12[ab].nK2; //>0
                uint16_t j = K2max34-SP34[ab].nK2; //>0

                SortedERIs(i,j).reserve();

                if (LPL==NINTS) {
                    return LPL;
                }
            }

        }


    }

    if(G) State.storeAB = tmax12;

    return LPL;
}

template<bool G, bool J, bool X, bool R, int mode> uint64_t ERIblock::PrescreenedList(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS) {

    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & fab = logFs[ea][eb][fa][fb];
    const a2screen & fcd = logFs[ec][ed][fc][fd];
    const a2screen & fac = logFs[ea][ec][fa][fc];
    const a2screen & fbd = logFs[eb][ed][fb][fd];
    const a2screen & fad = logFs[ea][ed][fa][fd];
    const a2screen & fbc = logFs[eb][ec][fb][fc];

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];


    uint64_t LPL = 0;

    uint16_t minnab = State.storeABb;
    uint16_t maxnab = K2max12;

    for (uint16_t nab=minnab; nab<maxnab; ++nab) {
        uint16_t minncd = (nab==State.storeABb)?State.storeCDb:0;
        uint16_t maxncd = K2max34;

        for (uint16_t ncd=minncd; ncd<maxncd; ++ncd) {

            uint32_t Aab = SortedABs[nab].vList;
            uint32_t Acd = SortedCDs[ncd].vList;

            uint32_t minab = (nab==State.storeABb && ncd==State.storeCDb)?State.storeAB:0;
            uint32_t maxab = SortedABs[nab].Nsps;

            uint16_t i = nab; //K2max12-SP12[ab].nK2; //>0
            uint16_t j = ncd; //K2max34-SP34[cd].nK2; //>0

            //skips in none of the shellpair pairs survives CS prescreening
            //if (Acd >= max34l[Aab+maxab-1]) continue;

            for (uint32_t abp=minab; abp<maxab; ++abp) {
                uint32_t ab = Aab + abp;

                //if (!NodeGroup.CheckMod(ab)) continue;

                uint32_t mincd = (nab==State.storeABb && ncd==State.storeCDb && abp==State.storeAB)?State.storeCD:0;
                uint32_t maxcd = SortedCDs[ncd].Nsps;

                if (Acd >= max34l[ab]) continue;


                for (uint32_t cdp=mincd; cdp<maxcd; ++cdp) {
                    uint32_t cd = Acd + cdp;

                    if (cd >= max34l[ab]) continue;
                    if (SkipSame && ab==cd) continue;

                    if (!NodeGroup.CheckMod(ab+cd)) continue;

                    uint32_t ata = SP12[ab].ata;
                    uint32_t atb = SP12[ab].atb;
                    uint32_t atc = SP34[cd].ata;
                    uint32_t atd = SP34[cd].atb;

                    if (ata==atc || ata==atd || atb==atc || atb==atd) {
                        // skip it if computing strictly 4-center integrals
                        if (mode==2) continue;
                    }
                    else {
                        // skip it if computing strictly 3-center integrals
                        if (mode==1) continue;
                    }

                    float wabcd = SP12[ab].logCS + SP34[cd].logCS;

                    bool useJD, useJS, useXD, useXS;
                    useJD = useJS = useXD = useXS = false;


                    if (J) {
                        float logJa  = fab[ata][atb] + dcd[atc][atd];
                        float logJb  = dab[ata][atb] + fcd[atc][atd];

                        float logJ = min(logJa, logJb) + wabcd;

                        useJD = (logJ<logD);
                        useJS = (logJ<logS);
                    }
                    if (X) {
                        float logX1a = fac[ata][atc] + dbd[atb][atd];
                        float logX2a = fad[ata][atd] + dbc[atb][atc];
                        float logX1b = dac[ata][atc] + fbd[atb][atd];
                        float logX2b = dad[ata][atd] + fbc[atb][atc];

                        float logX = min (min(logX1a, logX2a), min(logX1b, logX2b)) + wabcd;

                        useXD = (logX<logD);
                        useXS = (logX<logS);
                    }

                    //if generate
                    if (G) {
                        if (include(J,X,R,useJD,useJS,useXD,useXS)) {
                            ShellPairPair SPP(ab,cd);
                            SortedERIs(i,j).push(SPP);


                            ++LPL;

                            if (LPL==NINTS) {
                                State.storeABb = nab;
                                State.storeCDb = ncd;
                                State.storeAB = abp; //save the state for the next iteration
                                State.storeCD = cdp+1;
                                return LPL;
                            }
                        }
                    }
                    // else only count
                    else {
                        if (include(J,X,R,useJD,useJS,useXD,useXS)) {
                            ++LPL;
                            SortedERIs(i,j).reserve();

                            if (LPL==NINTS) {
                                return LPL;
                            }
                        }
                    }
                }
            }
        }
    }

    if (G) {
        State.storeABb = K2max12;
        State.storeCDb = K2max34;
        State.storeAB = tmax12;
        State.storeCD = tmax34;
    }

    return LPL;
}



void ERIblock::InitONX(const b2screener & logFs, const b2screener & logDs) {

    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];


    int natoms = ONXtensors.natoms = logDs.N();

    //make a list of ata, atb, atc, atd
    for (uint32_t ab=0; ab<tmax12; ++ab) {
       if (!NodeGroup.CheckMod(ab)) continue;

        uint32_t ata = SP12[ab].ata;
        uint32_t atb = SP12[ab].atb;

        ONXtensors.AtomsA.insert(ata);
        ONXtensors.AtomsB.insert(atb);
    }

    for (uint32_t cd=0; cd<tmax34; ++cd) {
        uint32_t atc = SP34[cd].ata;
        uint32_t atd = SP34[cd].atb;

        ONXtensors.AtomsC.insert(atc);
        ONXtensors.AtomsD.insert(atd);
    }


    //AtomsAB.set(natoms);
    ONXtensors.AtomsCD.set(natoms);
    ONXtensors.AtomsAC.set(natoms);
    ONXtensors.AtomsAD.set(natoms);
    ONXtensors.AtomsBC.set(natoms);
    ONXtensors.AtomsBD.set(natoms);

    set<uint32_t>::iterator it1, it2;



    float maxx = log(1e99)/2;

/*
    for (uint32_t ab=0; ab<tmax12; ++ab) {
        uint32_t ata = SP12[ab].ata;
        uint32_t atb = SP12[ab].atb;

        AtomsAB(ata,atb) = ab; //SP12[ab].logCS;
    }
*/
    for (uint32_t cd=0; cd<tmax34; ++cd) {
        uint32_t atc = SP34[cd].ata;
        uint32_t atd = SP34[cd].atb;

        ONXtensors.AtomsCD(atc,atd) = cd; // SP34[cd].logCS;
    }

    for (it1=ONXtensors.AtomsA.begin(); it1!=ONXtensors.AtomsA.end(); ++it1) {
        for (it2=ONXtensors.AtomsC.begin(); it2!=ONXtensors.AtomsC.end(); ++it2) {
            uint32_t ata = *it1;
            uint32_t atc = *it2;
            if (dac[ata][atc] < maxx)
                ONXtensors.AtomsAC(ata,atc) = dac[ata][atc];
        }
    }

    for (it1=ONXtensors.AtomsB.begin(); it1!=ONXtensors.AtomsB.end(); ++it1) {
        for (it2=ONXtensors.AtomsD.begin(); it2!=ONXtensors.AtomsD.end(); ++it2) {
            uint32_t atb = *it1;
            uint32_t atd = *it2;
            if (dbd[atb][atd] < maxx)
                ONXtensors.AtomsBD(atb,atd) = dbd[atb][atd];
        }
    }

    for (it1=ONXtensors.AtomsA.begin(); it1!=ONXtensors.AtomsA.end(); ++it1) {
        for (it2=ONXtensors.AtomsD.begin(); it2!=ONXtensors.AtomsD.end(); ++it2) {
            uint32_t ata = *it1;
            uint32_t atd = *it2;
            if (dad[ata][atd] < maxx)
                ONXtensors.AtomsAD(ata,atd) = dad[ata][atd];
        }
    }

    for (it1=ONXtensors.AtomsB.begin(); it1!=ONXtensors.AtomsB.end(); ++it1) {
        for (it2=ONXtensors.AtomsC.begin(); it2!=ONXtensors.AtomsC.end(); ++it2) {
            uint32_t atb = *it1;
            uint32_t atc = *it2;
            if (dbc[atb][atc] < maxx)
                ONXtensors.AtomsBC(atb,atc) = dbc[atb][atc];
        }
    }


    float minBC =  ONXtensors.AtomsBC.GetMin();
    float minAD =  ONXtensors.AtomsAD.GetMin();
    float minAC =  ONXtensors.AtomsAC.GetMin();
    float minBD =  ONXtensors.AtomsBD.GetMin();

    //cout << "1" << endl; cout.flush();
}

void ERIblock::CleanONX() {
    ONXtensors.AtomsA.clear();
    ONXtensors.AtomsB.clear();
    ONXtensors.AtomsC.clear();
    ONXtensors.AtomsD.clear();
    ONXtensors.AtomsAB.clean();
    ONXtensors.AtomsCD.clean();
    ONXtensors.AtomsAC.clean();
    ONXtensors.AtomsBD.clean();
    ONXtensors.AtomsAD.clean();
    ONXtensors.AtomsBC.clean();
}

template<bool G, int mode> uint64_t ERIblock::ONXPrescreenedList(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS) {

    //cout << "2" << endl; cout.flush();

    map<uint32_t,uint32_t>::iterator itab, itcd;
    map<uint32_t,float>::iterator ita, itb, itc, itd;


    float minAB =  SP12[0].logCS;
    float minCD =  SP34[0].logCS;

    uint64_t LPL = 0;


    for (uint32_t ab=State.storeAB; ab<tmax12; ++ab) {
        if (!NodeGroup.CheckMod(ab)) continue;

        uint32_t ata = SP12[ab].ata;
        uint32_t atb = SP12[ab].atb;

        float lAB = SP12[ab].logCS;

        float minBC = ONXtensors.AtomsBC[atb].minf;
        float minAD = ONXtensors.AtomsAD[ata].minf;

        float minAC = ONXtensors.AtomsAC[ata].minf;
        float minBD = ONXtensors.AtomsBD[atb].minf;

        if (lAB+minBC+minCD+minAD>=logD) break;

        //eliminate duplicates in SortedERIs and keeps it sorted
        set<uint32_t> cds;

        //make a list

        for (itc=ONXtensors.AtomsBC[atb].r.begin(); itc!=ONXtensors.AtomsBC[atb].r.end(); ++itc) {
            uint32_t atc = itc->first;

            if (mode==2) if (ata==atc || atb==atc) continue; // skip it if computing strictly 4-center integrals

            float lBC = itc->second;

            if (lAB+lBC+minCD+minAD>=logD) continue;

            for (itcd=ONXtensors.AtomsCD[atc].r.begin(); itcd!=ONXtensors.AtomsCD[atc].r.end(); ++itcd) {
                uint32_t atd = itcd->first;

                if (mode==2) if   (ata==atd || atb==atd) continue; // skip it if computing strictly 4-center integrals
                if (mode==1) if (!(ata==atc || ata==atd || atb==atc || atb==atd)) continue; // skip it if computing strictly 3-center integrals

                uint32_t cd = itcd->second;

                if (!NodeGroup.CheckMod(ab+cd)) continue;
                if (cd >= max34l[ab]) continue;
                if (SkipSame && ab==cd) continue;

                float lCD = SP34[cd].logCS;
                float lAD = ONXtensors.AtomsAD[ata][atd];

                if (lAB+lBC+lCD+lAD < logD) {
                    cds.insert(cd);
                }
            }
        }


        for (itc=ONXtensors.AtomsAC[ata].r.begin(); itc!=ONXtensors.AtomsAC[ata].r.end(); ++itc) {
            uint32_t atc = itc->first;

            if (mode==2) if (ata==atc || atb==atc) continue; // skip it if computing strictly 4-center integrals

            float lAC = itc->second;

            if (lAB+lAC+minCD+minBD>=logD) continue;

            for (itcd=ONXtensors.AtomsCD[atc].r.begin(); itcd!=ONXtensors.AtomsCD[atc].r.end(); ++itcd) {
                uint32_t atd = itcd->first;

                if (mode==2) if   (ata==atd || atb==atd) continue; // skip it if computing strictly 4-center integrals
                if (mode==1) if (!(ata==atc || ata==atd || atb==atc || atb==atd)) continue; // skip it if computing strictly 3-center integrals

                uint32_t cd = itcd->second;

                if (!NodeGroup.CheckMod(ab+cd)) continue;
                if (cd >= max34l[ab]) continue;
                if (SkipSame && ab==cd) continue;

                float lCD = SP34[cd].logCS;
                float lBD = ONXtensors.AtomsBD[atb][atd];

                if (lAB+lAC+lCD+lBD < logD) {
                    cds.insert(cd);
                }
            }
        }


        set<uint32_t>::iterator it = cds.begin();

        uint32_t sCD = (ab==State.storeAB)?State.storeCD:0;

        // skip the elements that have been computed
        for (int ii=0; ii<sCD; ++ii) ++it;


        for (; it!=cds.end(); ++it, ++sCD) {

            uint32_t cd = *it;

            uint16_t i = K2max12 - SP12[ab].nK2;
            uint16_t j = K2max34 - SP34[cd].nK2;


            //if generate
            if (G) {
                //if (include(J,X,R,useJD,useJS,useXD,useXS))
                {
                    ShellPairPair SPP(ab,cd);
                    SortedERIs(i,j).push(SPP);
                    ++LPL;

                    if (LPL==NINTS) {
                        State.storeAB = ab;
                        State.storeCD = sCD +1;
                        return LPL;
                    }
                }
            }

            // else only count
            else {
                //if (include(J,X,R,useJD,useJS,useXD,useXS))
                {
                    ++LPL;
                    SortedERIs(i,j).reserve();

                    if (LPL==NINTS) {
                        return LPL;
                    }
                }
            }
        }
    }

    if (G) {
        State.storeABb = K2max12;
        State.storeCDb = K2max34;
        State.storeAB  = tmax12;
        State.storeCD  = tmax34;
    }

    return LPL;
}

template<bool G, int mode> uint64_t ERIblock::JPrescreenedList(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS) {


    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & fab = logFs[ea][eb][fa][fb];
    const a2screen & fcd = logFs[ec][ed][fc][fd];

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];


    uint64_t LPL = 0;

    uint16_t minnab = State.storeABb;
    uint16_t maxnab = K2max12;

    for (uint16_t nab=minnab; nab<maxnab; ++nab) {
        uint16_t minncd = (nab==State.storeABb)?State.storeCDb:0;
        uint16_t maxncd = K2max34;

        for (uint16_t ncd=minncd; ncd<maxncd; ++ncd) {

            uint32_t Aab = SortedABs[nab].vList;
            uint32_t Acd = SortedCDs[ncd].vList;

            uint32_t minab = (nab==State.storeABb && ncd==State.storeCDb)?State.storeAB:0;
            uint32_t maxab = SortedABs[nab].Nsps;

            uint16_t i = nab; //K2max12-SP12[ab].nK2; //>0
            uint16_t j = ncd; //K2max34-SP34[cd].nK2; //>0

            //skips in none of the shellpair pairs survives CS prescreening
            //if (Acd >= max34l[Aab+maxab-1]) continue;


            for (uint32_t abp=minab; abp<maxab; ++abp) {
                uint32_t ab = Aab + abp;

                if (!NodeGroup.CheckMod(ab)) continue;

                uint32_t mincd = (nab==State.storeABb && ncd==State.storeCDb && abp==State.storeAB)?State.storeCD:0;
                uint32_t maxcd = SortedCDs[ncd].Nsps;

                if (Acd >= max34l[ab]) continue;

                uint32_t ata = SP12[ab].ata;
                uint32_t atb = SP12[ab].atb;

                float logAB  = SP12[ab].logCS + dab[ata][atb];
                float logAB2 = SP12[ab].logCS + fab[ata][atb];


                for (uint32_t cdp=mincd; cdp<maxcd; ++cdp) {
                    uint32_t cd = Acd + cdp;

                    if (cd >= max34l[ab]) continue;
                    if (SkipSame && ab==cd) continue;

                    uint32_t atc = SP34[cd].ata;
                    uint32_t atd = SP34[cd].atb;

                    if (ata==atc || ata==atd || atb==atc || atb==atd) {
                        // skip it if computing strictly 4-center integrals
                        if (mode==2) continue;
                    }
                    else {
                        // skip it if computing strictly 3-center integrals
                        if (mode==1) continue;
                    }


                    float logJ1 = logAB  + fcd[atc][atd] + SP34[cd].logCS;
                    float logJ2 = logAB2 + dcd[atc][atd] + SP34[cd].logCS;

                    float logJ = min(logJ1, logJ2);

                    //if generate
                    if (G) {
                        if (logJ<logD) {
                            ShellPairPair SPP(ab,cd);
                            SortedERIs(i,j).push(SPP);


                            ++LPL;

                            if (LPL==NINTS) {
                                State.storeABb = nab;
                                State.storeCDb = ncd;
                                State.storeAB = abp; //save the state for the next iteration
                                State.storeCD = cdp+1;
                                return LPL;
                            }
                        }
                    }
                    // else only count
                    else {
                        if (logJ<logD) {
                            ++LPL;
                            SortedERIs(i,j).reserve();

                            if (LPL==NINTS) {
                                return LPL;
                            }
                        }
                    }
                }
            }
        }
    }

    if (G) {
        State.storeABb = K2max12;
        State.storeCDb = K2max34;
        State.storeAB = tmax12;
        State.storeCD = tmax34;
    }

    return LPL;
}


#include "low/sort.hpp"

void ERIblock::SortPrescreenedLists() {

    // sort the shell quadruplets according to distance
    // ************************************************

    for (uint32_t i=0; i<K2max12; ++i) {
        for (uint32_t j=0; j<K2max34; ++j) {

            int N = SortedERIs(i,j).size();

            if (N<2) continue;

            ShellPairPair * SPPblock = SortedERIs(i,j).vList;


            //compute the (squared) distances
            for (int n=0; n<N; ++n) {
                uint32_t ab = SPPblock[n].n12;
                uint32_t cd = SPPblock[n].n34;

                const AtomProd & AP12 = (*AP12list)[ ab ];
                const AtomProd & AP34 = (*AP34list)[ cd ];

                R2[n] = R2distance(AP12, AP34);
            }

            //FlashSort4(R2, SPPblock, N);
            QuickSort3(R2, SPPblock, N);
        }
    }

}

void ERIblock::CaseSortPrescreenedLists() {
    // sort the shell quadruplets according to distance; discard elements far apart
    // ****************************************************************************

    const int NVIG = 1024;
    const double vg_min  = 0;
    const double vg_max  = 32;
    const double vg_step  = (vg_max-vg_min)/double(NVIG);

    const double case_w2 = LibQuimera::Quimera::GetW2();


    //correct, but extremely conservative
    double maxR2 = (vg_max-vg_step)*(1./ABp->kmin + 1./CDp->kmin + 1./case_w2);

    uint64_t N0 = 0;
    uint64_t NF = 0;


    for (uint32_t i=0; i<K2max12; ++i) {
        for (uint32_t j=0; j<K2max34; ++j) {

            uint64_t N = SortedERIs(i,j).Nints;

            N0 += N;
            //if (N<2) continue;

            ShellPairPair * SPPblock = SortedERIs(i,j).vList;

            double * R2 = new double[N];

            //compute the (squared) distances
            for (int n=0; n<N; ++n) {
                uint32_t ab = SPPblock[n].n12;
                uint32_t cd = SPPblock[n].n34;

                const AtomProd & AP12 = (*AP12list)[ ab ];
                const AtomProd & AP34 = (*AP34list)[ cd ];

                R2[n] = R2distance(AP12, AP34);
            }

            FlashSort(R2, SPPblock, N);

            //find the last allowed element
            int last=0;
            for (; last<N; ++last) if (R2[last]>maxR2) break;

            //set the list shorter
            SortedERIs(i,j).Nints = last;
            NF += last;


            delete[] R2;
        }
    }

    //Benchmarker << "Initial CASE Coulomb list contained " << N0 << " elements" << endl;
    //Benchmarker << " prescreened list contains "  << NF << " elements" << endl;
    //Benchmarker << " cutoff distance for CASE with present pair of prototypes is " << sqrt(maxR2)*BOHR << " Angstroms" << endl << endl;
}

template <int LEN> void ERIblock::InitBlocks() {

    //set list pointers
    uint64_t Icount = 0;

    for (uint32_t i=0; i<K2max12; ++i) {
        for (uint32_t j=0; j<K2max34; ++j) {
            SortedERIs(i,j).vList = SPPlist + LEN*Icount;
            int nblocks = (SortedERIs(i,j).Nints + (LEN-1)) / LEN;
            SortedERIs(i,j).Nints = nblocks*LEN;
            Icount += nblocks;
        }
    }
}

void ERIblock::InitLists() {

    for (uint32_t ab=0; ab<tmax12; ++ab) {
        uint16_t i = K2max12-SP12[ab].nK2; //>0
        ++SortedABs[i].Nsps;
    }

    for (uint32_t cd=0; cd<tmax34; ++cd) {
        uint16_t j = K2max34-SP34[cd].nK2; //>0
        ++SortedCDs[j].Nsps;
    }

    uint32_t Icount12 = 0;
    uint32_t Icount34 = 0;

    for (uint16_t p=0; p<K2max12; ++p) {
        SortedABs[p].vList = Icount12;
        Icount12 += SortedABs[p].Nsps;
    }

    for (uint16_t p=0; p<K2max34; ++p) {
        SortedCDs[p].vList = Icount34;
        Icount34 += SortedCDs[p].Nsps;
    }
}


void ERIblock::InitListsJ(const b2screener & logDs) {

    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];


    for (uint32_t ab=0; ab<tmax12; ++ab) {
        uint32_t ata = SP12[ab].ata;
        uint32_t atb = SP12[ab].atb;
        ABlogs[ab].cs = SP12[ab].logCS + dab[ata][atb];
        ABlogs[ab].n  = ab;
    }

    for (uint32_t cd=0; cd<tmax34; ++cd) {
        uint32_t atc = SP34[cd].ata;
        uint32_t atd = SP34[cd].atb;
        CDlogs[cd].cs = SP34[cd].logCS + dcd[atc][atd];
        CDlogs[cd].n  = cd;
    }

    //sort both lists
    FlashSort(ABlogs, tmax12);
    FlashSort(CDlogs, tmax34);

    for (uint32_t ab=0; ab<tmax12; ++ab) {
        uint16_t i = K2max12-SP12[ab].nK2; //>0
        ++SortedABs[i].Nsps;
    }

    for (uint32_t cd=0; cd<tmax34; ++cd) {
        uint16_t j = K2max34-SP34[cd].nK2; //>0
        ++SortedCDs[j].Nsps;
    }

    uint32_t Icount12 = 0;
    uint32_t Icount34 = 0;

    // these will now be offset, but no shellpair will demote

    for (uint16_t p=0; p<K2max12; ++p) {
        SortedABs[p].vList = Icount12;
        Icount12 += SortedABs[p].Nsps;
    }

    for (uint16_t p=0; p<K2max34; ++p) {
        SortedCDs[p].vList = Icount34;
        Icount34 += SortedCDs[p].Nsps;
    }
}


//efficient integral vector packing algorithm
template <int LEN> void ERIblock::FuseIncompleteTiles() {
    //fuse incomplete tiles
    //*********************

    for (int e=1; e<LEN; e*=2) {

        //a better algorithm should eliminate with the closest element in the table class, not just the next in the row/column
        //also: just check active rows and columns

        //eliminate by rows
        for (int p=0; p<SortedERIs.N(); ++p) {
            int qq;
            bool first=true;

            for (int q=0; q<SortedERIs.M(); ++q) {

                if (SortedERIs(p,q).size()&e) {
                    if (first) {
                        if (SortedERIs(p,q).size() + e <= SortedERIs(p,q).capacity()) {
                            qq = q;
                            first = false;
                        }
                    }
                    else {
                        //upgrade 'e' elements to a higher class and update
                        for (int ee=0; ee<e; ++ee) {
                            ShellPairPair SP = SortedERIs(p,q).pop();
                            SortedERIs(p,qq).push(SP);
                        }
                        first = true;
                    }
                }
            }
        }

        //eliminate by columns
        for (int q=0; q<SortedERIs.M(); ++q) {
            int pp;
            bool first=true;

            for (int p=0; p<SortedERIs.N(); ++p) {
                if (SortedERIs(p,q).size()&e) {
                    if (first) {
                        //adding 'e' integrals to this class should not overflow it
                        if (SortedERIs(p,q).size() + e <= SortedERIs(p,q).capacity()) {
                            pp = p;
                            first = false;
                        }
                    }
                    else {
                        //upgrade 'e' elements to a higher class and update
                        for (int ee=0; ee<e; ++ee) {
                            ShellPairPair SP = SortedERIs(p,q).pop();
                            SortedERIs(pp,q).push(SP);
                        }
                        first = true;
                    }
                }
            }
        }



        //eliminate permitted diagonals (without generating new tiles)
        uint32_t LastInRow[maxK2];
        uint32_t LastAccepting[maxK2];

        for (int p=0; p<SortedERIs.N(); ++p) {
            int last = -1;
            for (int q=0; q<SortedERIs.M(); ++q)
                if (SortedERIs(p,q).size()&e) last = q;

            LastInRow[p] = last;

            if ( last>-1 && SortedERIs(p,last).size()+e<=SortedERIs(p,last).capacity() )
                LastAccepting[p] = last;
            else
                LastAccepting[p] = -1;
        }

        for (int p=SortedERIs.N()-1; p>=0; --p) {
            if (LastInRow[p]==-1) continue;

            int pp;
            for (pp=p-1; pp>=0; --pp) {
                if (LastAccepting[pp]>-1 && LastAccepting[pp]<LastInRow[p]) break;
            }

            if (pp==-1) continue;

            //merge
            int q  = LastInRow[p];
            int qq = LastAccepting[pp];

            for (int ee=0; ee<e; ++ee) {
                ShellPairPair SP = SortedERIs(p,q).pop();
                SortedERIs(pp,qq).push(SP);
            }

            LastInRow[p] = LastInRow[pp] = LastAccepting[p] = LastAccepting[pp] = -1;
        }


        //do everything else here

    }


    // more sophisticated alternatives are possible, but they come at a cost
    // simply move all orphan integrals together to the bucket which minimally
    // satisfies the required conditions

    int cup  = 0;
    int minp = SortedERIs.N();
    int minq = SortedERIs.M();

    for (int p=0; p<SortedERIs.N(); ++p) {
        for (int q=0; q<SortedERIs.M(); ++q) {

            if (SortedERIs(p,q).size()%LEN != 0) {
                cup += SortedERIs(p,q).size()%LEN;
                minp = min(minp, p);
                minq = min(minq, q);
            }
        }
    }

    if (cup==0) return; // no leftovers; return with glory

    cup -= SortedERIs(minp,minq).size()%LEN; //don't count it twice!!

    //merge the list and the leftovers in a unique array
    int totmin = cup + SortedERIs(minp,minq).size();

    ShellPairPair leftovers[totmin];
    int ncup  = 0;

    while (SortedERIs(minp,minq).size() > 0) {
        leftovers[ncup] = SortedERIs(minp,minq).pop();
        ++ncup;
    }

    for (int p=0; p<SortedERIs.N(); ++p) {
        for (int q=0; q<SortedERIs.M(); ++q) {
            //if (p==minp && q==minq) continue; //doesn't matter

            while (SortedERIs(p,q).size()%LEN != 0) {
                leftovers[ncup] =  SortedERIs(p,q).pop();
                ++ncup;
            }
        }
    }



    // compact everything else!!!
    ShellPairPair * sppl = SPPlist;

    for (int p=0; p<SortedERIs.N(); ++p) {
        for (int q=0; q<SortedERIs.M(); ++q) {
            if (p==minp && q==minq) continue; //leave for last

            for (int n=0; n<SortedERIs(p,q).size(); ++n)
                sppl[n] = SortedERIs(p,q).vList[n];

            SortedERIs(p,q).vList = sppl;

            sppl += SortedERIs(p,q).size();
        }
    }

    // append the merged list at the end of the array
    for (int n=0; n<totmin; ++n) sppl[n] = leftovers[n];

    SortedERIs(minp,minq).Iints = totmin;
    SortedERIs(minp,minq).Nints = totmin;
    SortedERIs(minp,minq).vList = sppl;

}

// make the tiles
// **************

template <int LEN> uint64_t ERIblock::GetNTiles() const {

    uint64_t NNtiles = 0;

    for (uint16_t i=0; i<SortedERIs.N(); ++i) {
        for (uint16_t j=0; j<SortedERIs.M(); ++j) {

            uint64_t Nints = SortedERIs(i,j).size();

            //set same contraction header for all tiles
            NNtiles += Nints/LEN;

            //fill remnant
            if (Nints%LEN) ++NNtiles;
        }
    }

    return NNtiles;
}


template <bool J, bool X>
void ERIblock::SetTilesUse (LibQuimera::ERITile64 * pTile, uint64_t Ntiles,  float logT, const b2screener & logDs) {

/*
    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];
    */


    //loop over all tiles
    for (uint64_t n=0; n<Ntiles; ++n) {

        //no use at the moment
        pTile[n].useJ = 0;
        pTile[n].useX = 0;

        //loop over all batches
        for (int k=0; k<DPC; ++k) {

            //if it contains valid info, check if it is needed by J, X or both
            if (pTile[n].inuse & (1<<k)) {

                uint32_t ap12 = pTile[n].ap12[k];
                uint32_t ap34 = pTile[n].ap34[k];

                uint32_t ata = SP12[ap12].ata;
                uint32_t atb = SP12[ap12].atb;
                uint32_t atc = SP34[ap34].ata;
                uint32_t atd = SP34[ap34].atb;

                float wabcd = SP12[ap12].logCS + SP34[ap34].logCS;


                if (J) {
                    //float logJa  = fab[ata][atb] + dcd[atc][atd];
                    //float logJb  = dab[ata][atb] + fcd[atc][atd];
                    //float logJ = min(logJa, logJb) + wabcd;

                    pTile[n].useJ |= (1<<k);

                    /*
                    if (logJ<logT) {
                        pTile[n].useJ |= (1<<k);
                        if (X) pTile[n].useX |= (1<<k);
                    }
                    */
                }
                if (X) {
                    //float logX1a = fac[ata][atc] + dbd[atb][atd];
                    //float logX2a = fad[ata][atd] + dbc[atb][atc];
                    //float logX1b = dac[ata][atc] + fbd[atb][atd];
                    //float logX2b = dad[ata][atd] + fbc[atb][atc];
                    //float logX = min (min(logX1a, logX2a), min(logX1b, logX2b)) + wabcd;

                    //if (logX1<logT || logX2<logT)
                    pTile[n].useX |= (1<<k);
                }

            }
        }
    }


}

template <bool J, bool X,  int N>
void ERIblock::SetTilesUse (LibQuimera::ERITile<N> * pTile, uint64_t Ntiles,  float logT, const b2screener & logDs) {

/*
    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];
    */


    //loop over all tiles
    for (uint64_t n=0; n<Ntiles; ++n) {

        //no use at the moment
        pTile[n].useJ = 0;
        pTile[n].useX = 0;

        //loop over all batches
        for (int k=0; k<N; ++k) {

            //if it contains valid info, check if it is needed by J, X or both
            if (pTile[n].inuse & (1UL<<k)) {

                uint32_t ap12 = pTile[n].ap12[k];
                uint32_t ap34 = pTile[n].ap34[k];

                uint32_t ata = SP12[ap12].ata;
                uint32_t atb = SP12[ap12].atb;
                uint32_t atc = SP34[ap34].ata;
                uint32_t atd = SP34[ap34].atb;

                float wabcd = SP12[ap12].logCS + SP34[ap34].logCS;

                if (J) {
                    //float logJa  = fab[ata][atb] + dcd[atc][atd];
                    //float logJb  = dab[ata][atb] + fcd[atc][atd];
                    //float logJ = min(logJa, logJb) + wabcd;

                    pTile[n].useJ |= (1<<k);

                    /*
                    if (logJ<logT) {
                        pTile[n].useJ |= (1<<k);
                        if (X) pTile[n].useX |= (1<<k);
                    }
                    */
                }
                if (X) {
                    //float logX1a = fac[ata][atc] + dbd[atb][atd];
                    //float logX2a = fad[ata][atd] + dbc[atb][atc];
                    //float logX1b = dac[ata][atc] + fbd[atb][atd];
                    //float logX2b = dad[ata][atd] + fbc[atb][atc];
                    //float logX = min (min(logX1a, logX2a), min(logX1b, logX2b)) + wabcd;

                    //if (logX1<logT || logX2<logT)
                    pTile[n].useX |= (1<<k);
                }
            }
        }
    }


}



uint64_t ERIblock::MakeAllTiles(LibQuimera::ERITile64 * pTile) {

    //compute the offsets for later digestion step
    uint32_t ta = SP12->ata;
    uint32_t tb = SP12->atb;
    uint32_t tc = SP34->ata;
    uint32_t td = SP34->atb;


    uint32_t offAB = STP->GetOffset(ta, tb, ABp->nb1, ABp->nb2);
    uint32_t offCD = STP->GetOffset(tc, td, CDp->nb1, CDp->nb2);

    uint32_t offAC = STP->GetOffset(ta, tc, ABp->nb1, CDp->nb1);
    uint32_t offAD = STP->GetOffset(ta, td, ABp->nb1, CDp->nb2);
    uint32_t offBC = STP->GetOffset(tb, tc, ABp->nb2, CDp->nb1);
    uint32_t offBD = STP->GetOffset(tb, td, ABp->nb2, CDp->nb2);

    uint32_t offBA = STP->GetOffset(tb, ta, ABp->nb2, ABp->nb1);
    uint32_t offDC = STP->GetOffset(td, tc, CDp->nb2, CDp->nb1);

    uint64_t Ntiles = 0;

    for (uint16_t i=0; i<SortedERIs.N(); ++i) {
        for (uint16_t j=0; j<SortedERIs.M(); ++j) {

            uint64_t Nints = SortedERIs(i,j).size();

            //set same contraction header for all tiles
            uint64_t t;
            for (t=0; t<Nints/DPC; ++t) {

                pTile[Ntiles].nKab = SortedERIs.N()-i-1;
                pTile[Ntiles].nKcd = SortedERIs.M()-j-1;
                pTile[Ntiles].used = DPC;
                pTile[Ntiles].inuse = 255; //not the most portable solution...

                for (int k=0; k<DPC; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[DPC*t+k].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[DPC*t+k].n34;
                }

                ++Ntiles;
            }

            //fill remnant
            if (Nints%DPC) {

                pTile[Ntiles].nKab = SortedERIs.N()-i-1;
                pTile[Ntiles].nKcd = SortedERIs.M()-j-1;
                pTile[Ntiles].used = Nints%DPC;
                pTile[Ntiles].inuse = (1<<(Nints%DPC)) - 1;

                for (int k=0; k<Nints%DPC; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[DPC*t+k].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[DPC*t+k].n34;
                }

                for (int k=Nints%DPC; k<DPC; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[DPC*t].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[DPC*t].n34;
                }

                ++Ntiles;
            }
        }
    }

    for (uint64_t n=0; n<Ntiles; ++n) {
        pTile[n].wSP12 = SP12->WW;
        pTile[n].wSP34 = SP34->WW;
        pTile[n].nK2ab = ABp->Ka * ABp->Kb;
        pTile[n].nK2cd = CDp->Ka * CDp->Kb;

        pTile[n].offAB = offAB;
        pTile[n].offCD = offCD;
        pTile[n].offAC = offAC;
        pTile[n].offAD = offAD;
        pTile[n].offBC = offBC;
        pTile[n].offBD = offBD;
        //pTile[n].offBA = offBA;
        //pTile[n].offDC = offDC;

        pTile[n].lowgeom = geometry;
    }

    return Ntiles;
}

uint64_t ERIblock::MakeAllTiles(LibQuimera::ERITile32 * pTile) {

    //compute the offsets for later digestion step
    uint32_t ta = SP12->ata;
    uint32_t tb = SP12->atb;
    uint32_t tc = SP34->ata;
    uint32_t td = SP34->atb;


    uint32_t offAB = STP->GetOffset(ta, tb, ABp->nb1, ABp->nb2);
    uint32_t offCD = STP->GetOffset(tc, td, CDp->nb1, CDp->nb2);

    uint32_t offAC = STP->GetOffset(ta, tc, ABp->nb1, CDp->nb1);
    uint32_t offAD = STP->GetOffset(ta, td, ABp->nb1, CDp->nb2);
    uint32_t offBC = STP->GetOffset(tb, tc, ABp->nb2, CDp->nb1);
    uint32_t offBD = STP->GetOffset(tb, td, ABp->nb2, CDp->nb2);

    uint32_t offBA = STP->GetOffset(tb, ta, ABp->nb2, ABp->nb1);
    uint32_t offDC = STP->GetOffset(td, tc, CDp->nb2, CDp->nb1);


    uint64_t Ntiles = 0;

    for (uint16_t i=0; i<SortedERIs.N(); ++i) {
        for (uint16_t j=0; j<SortedERIs.M(); ++j) {

            uint64_t Nints = SortedERIs(i,j).size();

            //set same contraction header for all tiles
            uint64_t t;
            for (t=0; t<Nints/FPC; ++t) {

                pTile[Ntiles].nKab = SortedERIs.N()-i-1;
                pTile[Ntiles].nKcd = SortedERIs.M()-j-1;
                pTile[Ntiles].used = FPC;

                for (int k=0; k<FPC; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[FPC*t+k].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[FPC*t+k].n34;
                    pTile[Ntiles].use[k] = 1;
                }

                ++Ntiles;
            }

            //fill remnant
            if (Nints%FPC) {

                pTile[Ntiles].nKab = SortedERIs.N()-i-1;
                pTile[Ntiles].nKcd = SortedERIs.M()-j-1;
                pTile[Ntiles].used = Nints%FPC;

                for (int k=0; k<Nints%FPC; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[FPC*t+k].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[FPC*t+k].n34;

                    pTile[Ntiles].use[k] = 1;
                }

                for (int k=Nints%FPC; k<FPC; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[FPC*t].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[FPC*t].n34;

                    pTile[Ntiles].use[k] = 0;
                }

                ++Ntiles;
            }
        }
    }

    for (uint64_t n=0; n<Ntiles; ++n) {
        pTile[n].wSP12 = SP12->WW;
        pTile[n].wSP34 = SP34->WW;
        pTile[n].nK2ab = ABp->Ka * ABp->Kb;
        pTile[n].nK2cd = CDp->Ka * CDp->Kb;
        pTile[n].offAB = offAB;
        pTile[n].offCD = offCD;
        pTile[n].offAC = offAC;
        pTile[n].offAD = offAD;
        pTile[n].offBC = offBC;
        pTile[n].offBD = offBD;
        pTile[n].offBA = offBA;
        pTile[n].offDC = offDC;
    }

    return Ntiles;
}

template<int NNN>
uint64_t ERIblock::MakeAllTiles(LibQuimera::ERITile<NNN> * pTile) {

    //compute the offsets for later digestion step
    uint32_t ta = SP12->ata;
    uint32_t tb = SP12->atb;
    uint32_t tc = SP34->ata;
    uint32_t td = SP34->atb;


    uint32_t offAB = STP->GetOffset(ta, tb, ABp->nb1, ABp->nb2);
    uint32_t offCD = STP->GetOffset(tc, td, CDp->nb1, CDp->nb2);

    uint32_t offAC = STP->GetOffset(ta, tc, ABp->nb1, CDp->nb1);
    uint32_t offAD = STP->GetOffset(ta, td, ABp->nb1, CDp->nb2);
    uint32_t offBC = STP->GetOffset(tb, tc, ABp->nb2, CDp->nb1);
    uint32_t offBD = STP->GetOffset(tb, td, ABp->nb2, CDp->nb2);

    uint32_t offBA = STP->GetOffset(tb, ta, ABp->nb2, ABp->nb1);
    uint32_t offDC = STP->GetOffset(td, tc, CDp->nb2, CDp->nb1);


    uint64_t Ntiles = 0;

    for (uint16_t i=0; i<SortedERIs.N(); ++i) {
        for (uint16_t j=0; j<SortedERIs.M(); ++j) {

            uint64_t Nints = SortedERIs(i,j).size();

            //set same contraction header for all tiles
            uint64_t t;
            for (t=0; t<Nints/NNN; ++t) {

                pTile[Ntiles].nKab = SortedERIs.N()-i-1;
                pTile[Ntiles].nKcd = SortedERIs.M()-j-1;
                pTile[Ntiles].used = NNN;
                pTile[Ntiles].inuse = (1UL<<NNN) -1; //not the most portable solution...


                for (int k=0; k<NNN; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[NNN*t+k].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[NNN*t+k].n34;
                }

                ++Ntiles;
            }

            //fill remnant
            if (Nints%NNN) {

                pTile[Ntiles].nKab = SortedERIs.N()-i-1;
                pTile[Ntiles].nKcd = SortedERIs.M()-j-1;
                pTile[Ntiles].used = Nints%NNN;
                pTile[Ntiles].inuse = (1UL<<(Nints%NNN)) - 1; //not the most portable solution...


                for (int k=0; k<Nints%NNN; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[NNN*t+k].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[NNN*t+k].n34;
                }

                for (int k=Nints%NNN; k<NNN; ++k) {
                    pTile[Ntiles].ap12[k] = SortedERIs(i,j).vList[NNN*t].n12;
                    pTile[Ntiles].ap34[k] = SortedERIs(i,j).vList[NNN*t].n34;
                }

                ++Ntiles;
            }
        }
    }

    for (uint64_t n=0; n<Ntiles; ++n) {
        pTile[n].wSP12 = SP12->WW;
        pTile[n].wSP34 = SP34->WW;
        pTile[n].nK2ab = ABp->Ka * ABp->Kb;
        pTile[n].nK2cd = CDp->Ka * CDp->Kb;
        pTile[n].offAB = offAB;
        pTile[n].offCD = offCD;
        pTile[n].offAC = offAC;
        pTile[n].offAD = offAD;
        pTile[n].offBC = offBC;
        pTile[n].offBD = offBD;
        pTile[n].offBA = offBA;
        pTile[n].offDC = offDC;
    }

    return Ntiles;
}



//evaluate blocks of integrals
//****************************

void ERIblock::SetInitialList_ABCD (float lmo) {

    uint64_t NElements = 0;

    for (max12=0; max12<tmax12; ++max12) {
        float maxCS = lmo - SP12[max12].logCS;
        uint32_t max34 = maxSP( maxCS, SP34, tmax34);
        max34l[max12] = max34;

        NElements += max34;
    }

}

void ERIblock::SetInitialList_ABCDs(float lmo) {
    uint64_t NElements = 0;

    for (max12=0; max12<tmax12; ++max12) {
        float maxCS = lmo - SP12[max12].logCS;
        uint32_t max34 = maxSP( maxCS, SP34, tmax34);
        max34 = min(max12, max34);
        max34l[max12] = max34;

        NElements += max34;
    }
}

void ERIblock::SetInitialList_ABAB (float lmo) {
    //new method
    uint64_t NElements = 0;

    for (uint32_t ap12=0; ap12<tmax12; ++ap12) {
        const ShellPair & AB = SP12[ap12];
        const ShellPair & CD = SP34[ap12];
        if ((AB.logCS + CD.logCS > lmo)) {tmax12 = ap12; break;}
    }
    NElements = tmax12;

}

void ERIblock::SetInitialList_AACD (float lmo) {

    uint64_t NElements = 0;

    for (max12=0; max12<tmax12; ++max12) {
        float maxCS = lmo - SP12[max12].logCS;
        uint32_t max34 = maxSP( maxCS, SP34, tmax34);
        max34l[max12] = max34;

        NElements += max34;
    }

}

void ERIblock::SetInitialList_AACC (float lmo) {
    uint64_t NElements = 0;

    for (max12=0; max12<tmax12; ++max12) {
        min34l[max12] = 0;
        max34l[max12] = tmax34;

        NElements += tmax34;
    }

}

void ERIblock::SetInitialList_AACCs(float lmo) {
    uint64_t NElements = 0;

    for (max12=0; max12<tmax12; ++max12) {
        min34l[max12] = 0;
        max34l[max12] = max12;

        NElements += max12;
    }

}


void ERIblock::SetInitialList      (float lmo, bool noK4) {

    if       (geometry==ABCD) {
        if (!SameList) SetInitialList_ABCD  (lmo);
        else           SetInitialList_ABCDs (lmo);
    }
    else if (geometry==AACD) {
                        SetInitialList_AACD  (lmo);
    }
    else if (geometry==AACC) {
        if (!SameList) SetInitialList_AACC  (lmo);
        else           SetInitialList_AACCs (lmo);
    }
    else if (geometry==ABAB) {
                       SetInitialList_ABAB  (lmo);
    }

    K2max12 = 0;
    K2max34 = 0;

    for (uint32_t ab=0; ab<tmax12; ++ab) K2max12 = max (K2max12, SP12[ab].nK2);
    for (uint32_t cd=0; cd<tmax34; ++cd) K2max34 = max (K2max34, SP34[cd].nK2);

    SortedABs.set(K2max12);
    SortedCDs.set(K2max34);

    if (!noK4) SortedERIs.set(K2max12, K2max34);
}

//empty everything
void ERIblock::ClearBlocks() {

    for (uint16_t i=0; i<K2max12; ++i) {
        for (uint16_t j=0; j<K2max34; ++j) {
            SortedERIs(i,j).reset();
        }
    }
}

#include "ERIbatch.hpp"

enum accuracy {LOW, MEDIUM, HIGH};

// S = 3 ABAB two-center integrals
// S = 2 ABCD integrals with some repeated element
// S = 1 ABCD integrals with some repeated element which become 3-center ABAD integrals
// S = 0 rest of integrals
template<bool J, bool X, int S> bool ERIblock::MakeBlocks      (float logD, float logS, const b2screener & logAD,  const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    // switch between 3-center and 4-center algorithm for CDR
    // necessary to use CDR on the regular ABCD integrals
    if (S==1) ERIalgorithm = Q.SelectAlgorithm(ABAD,la,lb,lc,ld);
    if (S==2) ERIalgorithm = Q.SelectAlgorithm(ABCD,la,lb,lc,ld);


    accuracy acc = HIGH;

    if      (acc==HIGH) {
        //always use double precision
        logD = logS;
    }
    else if (acc==MEDIUM) {
        //always use double precision for 1,2 and 3 center integrals
        if (geometry!=ABCD || (S==1) ) logD = logS;
    }
    else if (acc==LOW) {
        //always use double precision for 1,2 and 3 center integrals
        if (geometry!=ABCD || (S==1) ) logD = logS;
        else {
            logD = -100; //never use double for ABCD integrals
        }
    }


    //do double packed
    if (!State.storeBlock) {

        uint64_t MaxTiles = BlockBufferCPU::MaxTiles64(*this, pFock->nDMs);

        if (MaxTiles==0) {
            Echidna::EMessenger << "Error: not enough ERI buffer memory; increase program memory or reduce number of threads" << endl;
            throw (1420);
        }

        SetInitialList(logD);
        InitLists();

        size_t size8  = (wsize4 + msize4) * sizeof(cacheline64) + DPC * sizeof(ERIgeometry) + sizeof(ERIgeometries64) + sizeof(RotationMatrices64); //maximum size (assuming a float tile)

        uint64_t max_tiles = min ( MaxTiles, MAX_TILES_PER_BLOCK);

        // if only exchange is being computed, initialize tensors for ONX
        //if (!J && X) InitONX(logAD, logDs);

        //generate batches of ERIs until the buffer mnemory is exhausted or the whole class is finished
        while (1) {

            uint16_t sABb = State.storeABb;
            uint16_t sCDb = State.storeCDb;
            uint32_t sAB  = State.storeAB;
            uint32_t sCD  = State.storeCD;

            uint64_t Nprescreened;

            //counts the number of shell quartets in each GDO prescreening bucket
            ClearBlocks();

            /*
            if (!J && X) {
                if (S==3) Nprescreened = PrescreenedListS    <false, J,X, false  > (logD, logS, logAD, logDs, max_tiles*DPC);
                else      Nprescreened = ONXPrescreenedList  <false, S>            (logD, logS, logAD, logDs, max_tiles*DPC);
            }
            else
            */
            {
                if (S==3) Nprescreened = PrescreenedListS <false, J,X, false  > (logD, logS, logAD, logDs, max_tiles*DPC);
                else      Nprescreened = PrescreenedList  <false, J,X, false,S> (logD, logS, logAD, logDs, max_tiles*DPC);
            }

            if (Nprescreened==0) break; //no quadruplets left

            uint64_t nTiles2Get = (Nprescreened + (DPC-1))/DPC;

            uint64_t size = nTiles2Get * sizeof(ERITile64);
            ERITile64 * tList = (ERITile64*)EBB->Allocate(size);

            // no memory at all!!!
            if (tList==NULL) return false;

            uint64_t nTilesWeGot = size/sizeof(ERITile64);


            ERIBatch * newbatch  = new ERIBatch;
            *newbatch                 = *this;
            newbatch->TileList64      = tList;
            newbatch->memAllocated    = size;
            newbatch->J               = J;
            newbatch->X               = X;

            // must compute again, with fewer tiles
            if (nTiles2Get!=nTilesWeGot) {
                ClearBlocks();

                /*
                if (!J && X) {
                    if (S==3)  Nprescreened = PrescreenedListS <false, J,X, false  > (logD, logS, logAD, logDs, nTilesWeGot*DPC);
                    else       Nprescreened = ONXPrescreenedList  <false, S> (logD, logS, logAD, logDs, nTilesWeGot*DPC);
                }
                else
                */
                {
                    if (S==3)  Nprescreened = PrescreenedListS <false, J,X, false  > (logD, logS, logAD, logDs, nTilesWeGot*DPC);
                    else       Nprescreened = PrescreenedList  <false, J,X, false,S> (logD, logS, logAD, logDs, nTilesWeGot*DPC);
                }
            }

            //fills the GDO pescreening buckets with integrals
            InitBlocks<DPC>();

            /*
            if (!J && X) {
                if (S==3)       PrescreenedListS <true, J,X, false  > (logD, logS, logAD, logDs, Nprescreened);
                else         ONXPrescreenedList  <true, S>            (logD, logS, logAD, logDs, Nprescreened);
            }
            else
            */
            {
                if (S==3)       PrescreenedListS <true, J,X, false  > (logD, logS, logAD, logDs, Nprescreened);
                else            PrescreenedList  <true, J,X, false,S> (logD, logS, logAD, logDs, Nprescreened);
            }


            //merges all incomplete tiles to maximize vectorization and sorts each GDO bucket quadruplet list by increasing distance (to later use long range approximations)
            FuseIncompleteTiles<DPC>();
            SortPrescreenedLists();

            newbatch->Ntiles64        = MakeAllTiles(newbatch->TileList64);

            //set the use of every individual batch (coulomb, exchange, both, skip...)
            SetTilesUse<J,X> (newbatch->TileList64, newbatch->Ntiles64,   logD, logDs);


            if (newbatch->Ntiles64 != nTilesWeGot) {
                cout << "problem!!!    " << newbatch->Ntiles64 << " " << nTiles2Get << " " << nTilesWeGot << " " << GetNTiles<DPC>() << "   " << Nprescreened << endl;
            }

            TaskList.push(newbatch);
        }

        State.storeABb = 0;
        State.storeCDb = 0;
        State.storeAB  = 0;
        State.storeCD  = 0;

        //free the memory
        //if (!J && X) CleanONX();

        if (S==1) {
            State.store3center = true;
            return false;
        }
        else if (geometry==ABCD) {
            State.storeBlock   = true;
            return false;
        }
        else return true;
    }


    static const int NPBLOCK = DOUBLES_PER_BLOCK;

    //copypasta for vector
    //ignore at the moment
    if (false &&!State.storeBlock) {

        uint64_t MaxTiles = BlockBufferCPU::MaxTiles64(*this, pFock->nDMs);

        if (MaxTiles==0) {
            Echidna::EMessenger << "Error: not enough ERI buffer memory; increase program memory or reduce number of threads" << endl;
            throw (1421);
        }

        SetInitialList(logD);
        InitLists();

        uint64_t max_tiles = min ( MaxTiles, MAX_TILES_PER_BLOCK);

        // if only exchange is being computed, initialize tensors for ONX
        //if (!J && X) InitONX(logAD, logDs);

        //generate batches of ERIs until the buffer mnemory is exhausted or the whole class is finished
        while (1) {

            uint16_t sABb = State.storeABb;
            uint16_t sCDb = State.storeCDb;
            uint32_t sAB  = State.storeAB;
            uint32_t sCD  = State.storeCD;

            uint64_t Nprescreened;

            //counts the number of shell quartets in each GDO prescreening bucket
            ClearBlocks();

            /*
            if (!J && X) {
                if (S==3) Nprescreened = PrescreenedListS    <false, J,X, false  > (logD, logS, logAD, logDs, max_tiles*NPBLOCK);
                else      Nprescreened = ONXPrescreenedList  <false, S>            (logD, logS, logAD, logDs, max_tiles*NPBLOCK);
            }
            else
            */
            {
                if (S==3) Nprescreened = PrescreenedListS <false, J,X, false  > (logD, logS, logAD, logDs, max_tiles*NPBLOCK);
                else      Nprescreened = PrescreenedList  <false, J,X, false,S> (logD, logS, logAD, logDs, max_tiles*NPBLOCK);
            }

            if (Nprescreened==0) break; //no quadruplets left

            uint64_t nTiles2Get = (Nprescreened + (NPBLOCK-1))/NPBLOCK;

            uint64_t size = nTiles2Get * sizeof(ERITile<NPBLOCK>);
            ERITile<NPBLOCK> * tList = (ERITile<NPBLOCK>*)EBB->Allocate(size);

            // no memory at all!!!
            if (tList==NULL) return false;

            uint64_t nTilesWeGot = size/sizeof(ERITile<NPBLOCK>);


            ERIBatch * newbatch  = new ERIBatch;
            *newbatch                 = *this;
            newbatch->TileListN       = tList;
            newbatch->memAllocated    = size;
            newbatch->J               = J;
            newbatch->X               = X;

            // must compute again, with fewer tiles
            if (nTiles2Get!=nTilesWeGot) {
                ClearBlocks();

                /*
                if (!J && X) {
                    if (S==3)   Nprescreened = PrescreenedListS   <false, J,X, false  > (logD, logS, logAD, logDs, nTilesWeGot*NPBLOCK);
                    else        Nprescreened = ONXPrescreenedList <false, S>            (logD, logS, logAD, logDs, nTilesWeGot*NPBLOCK);
                }
                else
                */
                {
                    if (S==3)   Nprescreened = PrescreenedListS <false, J,X, false  > (logD, logS, logAD, logDs, nTilesWeGot*NPBLOCK);
                    else        Nprescreened = PrescreenedList  <false, J,X, false,S> (logD, logS, logAD, logDs, nTilesWeGot*NPBLOCK);
                }
            }

            //fills the GDO pescreening buckets with integrals
            InitBlocks<NPBLOCK>();

            /*
            if (!J && X) {
                if (S==3)        PrescreenedListS <true, J,X, false  > (logD, logS, logAD, logDs, Nprescreened);
                else          ONXPrescreenedList  <true, S>             (logD, logS, logAD, logDs, Nprescreened);
            }
            else
            */
            {
                if (S==3)        PrescreenedListS <true, J,X, false  > (logD, logS, logAD, logDs, Nprescreened);
                else             PrescreenedList  <true, J,X, false,S> (logD, logS, logAD, logDs, Nprescreened);
            }


            //merges all incomplete tiles to maximize vectorization and sorts each GDO bucket quadruplet list by increasing distance (to later use long range approximations)
            FuseIncompleteTiles<NPBLOCK>(); //should work as long as it's a power of 2
            SortPrescreenedLists();

            newbatch->NtilesN        = MakeAllTiles(newbatch->TileListN);

            //set the use of every individual batch (coulomb, exchange, both, skip...)
            SetTilesUse<J,X, NPBLOCK> (newbatch->TileListN, newbatch->NtilesN,   logD, logDs);


            if (newbatch->NtilesN != nTilesWeGot) {
                cout << "problem!!!    " << newbatch->NtilesN << " " << nTiles2Get << " " << nTilesWeGot << " " << GetNTiles<DPC>() << "   " << Nprescreened << endl;
            }

            TaskList.push(newbatch);
        }

        State.storeABb = 0;
        State.storeCDb = 0;
        State.storeAB  = 0;
        State.storeCD  = 0;

        //free the memory
        //if (!J && X) CleanONX();

        if (S==1) {
            State.store3center = true;
            return false;
        }
        else if (geometry==ABCD) {
            State.storeBlock   = true;
            return false;
        }
        else return true;
    }






    //do float packed
    if (State.storeBlock) {

        uint64_t MaxTiles = BlockBufferCPU::MaxTiles32(*this, pFock->nDMs);

        if (MaxTiles==0) {
            Echidna::EMessenger << "Error: not enough ERI buffer memory; increase program memory or reduce number of threads" << endl;
            throw (1422);
        }

        SetInitialList(logS);
        InitLists();

        uint64_t max_tiles = min ( MaxTiles, MAX_TILES_PER_BLOCK);

        while (1) {

            uint16_t sABb = State.storeABb;
            uint16_t sCDb = State.storeCDb;
            uint32_t sAB  = State.storeAB;
            uint32_t sCD  = State.storeCD;

            uint64_t Nprescreened;

            //counts the number of shell quartets in each GDO prescreening bucket
            ClearBlocks();

            if (S==3)                    Nprescreened = PrescreenedListS <false, J,X, true  > (logD, logS, logAD, logDs, max_tiles*FPC);
            else                         Nprescreened = PrescreenedList  <false, J,X, true,S> (logD, logS, logAD, logDs, max_tiles*FPC);


            if (Nprescreened==0) break;

            uint64_t nTiles2Get = (Nprescreened + (FPC-1))/FPC; //at least; let's think about it l8r

            uint64_t size = nTiles2Get * sizeof(ERITile32);
            ERITile32 * tList = (ERITile32*)EBB->Allocate(size);

            // no memory at all!!!
            if (tList==NULL) return false;

            uint64_t nTilesWeGot = size/sizeof(ERITile32);

            ERIBatch * newbatch  = new ERIBatch;
            *newbatch                 = *this;
            newbatch->TileList32      = tList;
            newbatch->memAllocated    = size;
            newbatch->J               = J;
            newbatch->X               = X;



            // must compute again, with fewer tiles
            if (nTiles2Get!=nTilesWeGot) {
                ClearBlocks();

                if (S==3)                    Nprescreened = PrescreenedListS <false, J,X, true  > (logD, logS, logAD, logDs, nTilesWeGot*FPC);
                else                         Nprescreened = PrescreenedList  <false, J,X, true,S> (logD, logS, logAD, logDs, nTilesWeGot*FPC);
            }

            //fills the GDO pescreening buckets with integrals
            InitBlocks<FPC>();

            if (S==3)                    PrescreenedListS <true, J,X, true  > (logD, logS, logAD, logDs, Nprescreened);
            else                         PrescreenedList  <true, J,X, true,S> (logD, logS, logAD, logDs, Nprescreened);


            //merges all incomplete tiles to maximize vectorization and sorts each GDO bucket quadruplet list by increasing distance (to later use long range approximations)
            FuseIncompleteTiles<FPC>();
            SortPrescreenedLists();

            if (GetNTiles<FPC>() != nTilesWeGot) {
                cout << "problem!!!    " << nTiles2Get << " " << nTilesWeGot << " " << GetNTiles<FPC>() << "   " << Nprescreened << endl;;
            }

            newbatch->Ntiles32        = MakeAllTiles(newbatch->TileList32);

            TaskList.push(newbatch);
        }

        return true;
    }

    return true;
}




void PrintEvaluationInfo(GEOM geometry, const ShellPairPrototype * ABp, const ShellPairPrototype * CDp) {
    Echidna::EBenchmarker << "Thread number " << omp_get_thread_num() << " beginning ERI block evaluation" << endl;
    Echidna::EBenchmarker << "   Geometry ";

    if      (geometry==ABCD) Echidna::EBenchmarker << "ABCD";
    else if (geometry==ABAB) Echidna::EBenchmarker << "ABAB";
    else if (geometry==AACD) Echidna::EBenchmarker << "AACD";
    else if (geometry==AACC) Echidna::EBenchmarker << "AACC";

    Echidna::EBenchmarker << "   Ls: " << int(ABp->l1) << " " << int(ABp->l2) << " " << int(CDp->l1) << " " << int(CDp->l2) << endl;
    Echidna::EBenchmarker << "   Elements: " << ABp->atype1 << " " << ABp->atype2 << " " << CDp->atype1 << " "  << CDp->atype2 << endl;
    Echidna::EBenchmarker << "   BF: " << int(ABp->nb1) << " " << int(ABp->nb2) << " " << int(CDp->nb1) << " " << int(CDp->nb2) << endl;

}

//check if it is possible to have overlapping centers in ABCD integrals
bool ERIblock::Check3Center() {
    bool check3C = false;
    if (geometry==ABCD) {

        if (ABp->atype1==CDp->atype1 || ABp->atype1==CDp->atype2 || ABp->atype2==CDp->atype1 || ABp->atype2==CDp->atype2)
            check3C = true;
    }

    return check3C;
}



bool ERIblock::MakeCoulomb (float logD, float logS,  const b2screener & logAD, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    //PrintEvaluationInfo(geometry, ABp, CDp);
    //Echidna::EBenchmarker << " Coulomb only" << endl;

    if      (geometry==ABAB)     return MakeBlocks <true, false, 3> (logD, logS, logAD, logDs, TaskList);
    else if (Check3Center()) {
        if (!State.store3center) return MakeBlocks <true, false, 1> (logD, logS, logAD, logDs, TaskList);
        else                     return MakeBlocks <true, false, 2> (logD, logS, logAD, logDs, TaskList);
    }
    else                         return MakeBlocks <true, false, 0> (logD, logS, logAD, logDs, TaskList);
}

bool ERIblock::MakeExchange(float logD, float logS,  const b2screener & logAD, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    //PrintEvaluationInfo(geometry, ABp, CDp);
    //Echidna::EBenchmarker << " Exchange only" << endl;

    if (geometry==ABAB)      return MakeBlocks <false, true, 3> (logD, logS, logAD, logDs, TaskList);
    else if (Check3Center()) {
        if (!State.store3center) return MakeBlocks <false, true, 1> (logD, logS, logAD, logDs, TaskList);
        else                     return MakeBlocks <false, true, 2> (logD, logS, logAD, logDs, TaskList);
    }
    else                     return MakeBlocks <false, true, 0> (logD, logS, logAD, logDs, TaskList);
}

bool ERIblock::MakeAll     (float logD, float logS,  const b2screener & logAD, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    //PrintEvaluationInfo(geometry, ABp, CDp);
    //Echidna::EBenchmarker << " Coulomb and Exchange simultaneously" << endl;

    if (geometry==ABAB)      return MakeBlocks <true, true, 3> (logD, logS, logAD, logDs, TaskList);
    else if (Check3Center()) {
        if (!State.store3center) return MakeBlocks <true, true, 1> (logD, logS, logAD, logDs, TaskList);
        else                     return MakeBlocks <true, true, 2> (logD, logS, logAD, logDs, TaskList);
    }
    else                     return MakeBlocks <true, true, 0> (logD, logS, logAD, logDs, TaskList);
}



/*

        FOR GPU; SIMPLIFIED METHOD

*/


void TileH::set(ShellPairPair * pSP, const TileH & p1, const TileH & p2) {

    SP = pSP;

    ij.i = std::min(p1.ij.i, p2.ij.i);
    ij.j = std::min(p1.ij.j, p2.ij.j);


    for (int l=0; l<p1.used; ++l) SP[        l] = p1.SP[l];
    for (int l=0; l<p2.used; ++l) SP[p1.used+l] = p2.SP[l];

    used = p1.used+p2.used;
}

void TileH::set(ShellPairPair * pSP, const TileH & p1) {

    SP = pSP;

    ij.i = p1.ij.i;
    ij.j = p1.ij.j;

    for (int l=0; l<p1.used; ++l) SP[l] = p1.SP[l];

    used = p1.used;
}

void PairStuff (TileH * P, TileH * Q, ShellPairPair * SP, int L, uint64_t N) {

    uint64_t s = 0;

    map <Tij, TileH > M;

    //match two subtiles of exactly the same contraction degrees
    for (uint64_t  t=0; t<N; ++t) {
        Tij ij = P[t].ij;

        if (M.count(ij)>0) {
            Q[s].set(SP, M[ij],P[t]);
            SP += L;
            ++s;

            M.erase (ij);
        }
        else {
            M[ij] = P[t];
        }
    }

    //trata de unir el resto
    map <Tij, TileH>::iterator it2;
    map <uint32_t , TileH>::iterator it;

    //promote subtiles within same row (some contraction degree for AB pair)
    map <uint32_t, TileH> R;

    for (it2=M.begin(); it2!=M.end(); ++it2) {
        uint32_t i = it2->second.ij.i;

        if (R.count(i)>0) {
            Q[s].set(SP, R[i],it2->second);
            SP += L;
            ++s;

            R.erase (i);
        }
        else {
            R[i] = it2->second;
        }
    }

    //promote subtiles within same column (some contraction degree for CD pair)
    map <uint32_t, TileH> C;

    for (it=R.begin(); it!=R.end(); ++it) {
        uint32_t j = it->second.ij.j;

        if (C.count(j)>0) {
            Q[s].set(SP, C[j],it->second);
            SP += L;
            ++s;

            C.erase (j);
        }
        else {
            C[j] = it->second;
        }
    }

    //make diagonal promotions; either one absolutely dominates the other or
    //promotes two tiles at once to a new void block
    for (it=C.begin(); it!=C.end();) {

        TileH T1, T2;

        T1 = it->second;
        ++it;

        if (it==C.end()) {
            Q[s].set(SP, T1);
        }
        else {
            //get it from list
            T2 = it->second;
            Q[s].set(SP, T1, T2);
            SP += L;
            ++it;
        }

        ++s;
    }

}


TileSet::TileSet() {
    Npairs = 0;

    SP1 = SP2 = NULL;
    T0 = T1 = T2 = T3 = T4 = T5 = NULL;
}

void TileSet::Set(uint64_t MaxERIs) {

    SP1 = new ShellPairPair[2*MaxERIs];
    SP2 = SP1+MaxERIs;

    T0 = new TileH[2*MaxERIs];
    T1 = T0 + MaxERIs;
    T2 = T1 + MaxERIs/2;
    T3 = T2 + MaxERIs/4;
    T4 = T3 + MaxERIs/8;
    T5 = T4 + MaxERIs/16;
}

void TileSet::push(uint32_t i, uint32_t j, const ShellPairPair & SPP) {

    T0[Npairs].ij.i  = i;
    T0[Npairs].ij.j  = j;
    T0[Npairs].SP    = SP1+Npairs;
    T0[Npairs].used  = 1;
    T0[Npairs].SP[0] = SPP;

    ++Npairs;
}

void TileSet::PairAll () {

    PairStuff (T0, T1, SP2,  2,  Npairs);
    PairStuff (T1, T2, SP1,  4, (Npairs+ 1)/ 2);
    PairStuff (T2, T3, SP2,  8, (Npairs+ 3)/ 4);
    PairStuff (T3, T4, SP1, 16, (Npairs+ 7)/ 8);
    PairStuff (T4, T5, SP2, 32, (Npairs+15)/16);

}

TileSet::~TileSet() {
    delete[] SP1;
    delete[] T0;
}



// J: include Coulomb
// X: include Exchange
// mode: 1:     only count ABAD integrals in the ABCD block
//       2:     skip ABAD-type integrals in ABCD block
//       other: count all integrals


template<bool J, bool X>           uint64_t ERIblock::PSCListS(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS) {

    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & fab = logFs[ea][eb][fa][fb];
    const a2screen & fcd = logFs[ec][ed][fc][fd];
    const a2screen & fac = logFs[ea][ec][fa][fc];
    const a2screen & fbd = logFs[eb][ed][fb][fd];
    const a2screen & fad = logFs[ea][ed][fa][fd];
    const a2screen & fbc = logFs[eb][ec][fb][fc];

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];


    uint64_t LPL = 0;

    for (uint32_t ab=State.storeAB; ab<tmax12; ++ab) {
        if (!NodeGroup.CheckMod(ab)) continue;

        uint32_t ata = SP12[ab].ata;
        uint32_t atb = SP12[ab].atb;
        uint32_t atc = SP34[ab].ata;
        uint32_t atd = SP34[ab].atb;


        float wabcd = SP12[ab].logCS + SP34[ab].logCS;

        bool useJD, useJS, useXD, useXS;
        useJD = useJS = useXD = useXS = false;

        if (J) {
            float logJa  = fab[ata][atb] + dcd[atc][atd];
            float logJb  = dab[ata][atb] + fcd[atc][atd];

            float logJ = min(logJa, logJb) + wabcd;

            useJD = (logJ<logD);
            //useJS = (logJ<logS);
        }
        if (X) {
            float logX1a = fac[ata][atc] + dbd[atb][atd];
            float logX2a = fad[ata][atd] + dbc[atb][atc];
            float logX1b = dac[ata][atc] + fbd[atb][atd];
            float logX2b = dad[ata][atd] + fbc[atb][atc];

            float logX = min (min(logX1a, logX2a), min(logX1b, logX2b)) + wabcd;

            useXD = (logX<logD);
            //useXS = (logX<logS);
        }

        //generate the list
        {
            //if (include(J,X,false,useJD,useJS,useXD,useXS)) {
            if (useJD || useXD) {
                uint16_t i = K2max12-SP12[ab].nK2; //>0
                uint16_t j = K2max34-SP34[ab].nK2; //>0

                ShellPairPair SPP(ab,ab);

                TS.push(i,j, SPP);

                ++LPL;

                if (LPL==NINTS) {
                    State.storeAB = ab+1;
                    return LPL; //interrupt the thread
                }
            }
        }

    }

    State.storeAB = tmax12;

    return LPL;
}

template<bool J, bool X, int mode> uint64_t ERIblock::PSCList(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS) {

    uint8_t  ea = ABp->eid1;
    uint8_t  eb = ABp->eid2;
    uint8_t  ec = CDp->eid1;
    uint8_t  ed = CDp->eid2;

    uint16_t  fa = ABp->nb1;
    uint16_t  fb = ABp->nb2;
    uint16_t  fc = CDp->nb1;
    uint16_t  fd = CDp->nb2;

    const a2screen & fab = logFs[ea][eb][fa][fb];
    const a2screen & fcd = logFs[ec][ed][fc][fd];
    const a2screen & fac = logFs[ea][ec][fa][fc];
    const a2screen & fbd = logFs[eb][ed][fb][fd];
    const a2screen & fad = logFs[ea][ed][fa][fd];
    const a2screen & fbc = logFs[eb][ec][fb][fc];

    const a2screen & dab = logDs[ea][eb][fa][fb];
    const a2screen & dcd = logDs[ec][ed][fc][fd];
    const a2screen & dac = logDs[ea][ec][fa][fc];
    const a2screen & dbd = logDs[eb][ed][fb][fd];
    const a2screen & dad = logDs[ea][ed][fa][fd];
    const a2screen & dbc = logDs[eb][ec][fb][fc];


    uint64_t LPL = 0;


    uint16_t minnab = State.storeABb;
    uint16_t maxnab = K2max12;

    for (uint16_t nab=minnab; nab<maxnab; ++nab) {
        uint16_t minncd = (nab==State.storeABb)?State.storeCDb:0;
        uint16_t maxncd = K2max34;

        for (uint16_t ncd=minncd; ncd<maxncd; ++ncd) {

            uint32_t Aab = SortedABs[nab].vList;
            uint32_t Acd = SortedCDs[ncd].vList;

            uint32_t minab = (nab==State.storeABb && ncd==State.storeCDb)?State.storeAB:0;
            uint32_t maxab = SortedABs[nab].Nsps;

            uint16_t i = nab; //K2max12-SP12[ab].nK2; //>0
            uint16_t j = ncd; //K2max34-SP34[cd].nK2; //>0

            //skips in none of the shellpair pairs survives CS prescreening
            //if (Acd >= max34l[Aab+maxab-1]) continue;

            for (uint32_t abp=minab; abp<maxab; ++abp) {
                uint32_t ab = Aab + abp;

                if (!NodeGroup.CheckMod(ab)) continue;

                uint32_t mincd = (nab==State.storeABb && ncd==State.storeCDb && abp==State.storeAB)?State.storeCD:0;
                uint32_t maxcd = SortedCDs[ncd].Nsps;

                if (Acd >= max34l[ab]) continue;


                for (uint32_t cdp=mincd; cdp<maxcd; ++cdp) {
                    uint32_t cd = Acd + cdp;

                    if (cd >= max34l[ab]) continue;
                    if (SkipSame && ab==cd) continue;

                    uint32_t ata = SP12[ab].ata;
                    uint32_t atb = SP12[ab].atb;
                    uint32_t atc = SP34[cd].ata;
                    uint32_t atd = SP34[cd].atb;

                    if (ata==atc || ata==atd || atb==atc || atb==atd) {
                        // skip it if computing strictly 4-center integrals
                        if (mode==2) continue;
                    }
                    else {
                        // skip it if computing strictly 3-center integrals
                        if (mode==1) continue;
                    }

                    float wabcd = SP12[ab].logCS + SP34[cd].logCS;

                    bool useJD, useJS, useXD, useXS;
                    useJD = useJS = useXD = useXS = false;


                    if (J) {
                        float logJa  = fab[ata][atb] + dcd[atc][atd];
                        float logJb  = dab[ata][atb] + fcd[atc][atd];

                        float logJ = min(logJa, logJb) + wabcd;

                        useJD = (logJ<logD);
                        //useJS = (logJ<logS);
                    }
                    if (X) {
                        float logX1a = fac[ata][atc] + dbd[atb][atd];
                        float logX2a = fad[ata][atd] + dbc[atb][atc];
                        float logX1b = dac[ata][atc] + fbd[atb][atd];
                        float logX2b = dad[ata][atd] + fbc[atb][atc];

                        float logX = min (min(logX1a, logX2a), min(logX1b, logX2b)) + wabcd;

                        useXD = (logX<logD);
                        //useXS = (logX<logS);
                    }


                    //generate
                    //if (include(J,X,false,useJD,useJS,useXD,useXS)) {
                    if (useJD || useXD) {
                        ShellPairPair SPP(ab,cd);

                        TS.push(i,j, SPP);

                        ++LPL;

                        if (LPL==NINTS) {
                            State.storeABb = nab;
                            State.storeCDb = ncd;
                            State.storeAB = abp; //save the state for the next iteration
                            State.storeCD = cdp+1;
                            return LPL;
                        }
                    }

                }
            }
        }
    }

    {
        State.storeABb = K2max12;
        State.storeCDb = K2max34;
        State.storeAB = tmax12;
        State.storeCD = tmax34;
    }

    return LPL;
}



uint64_t ERIblock::MakeAllTilesTS(LibQuimera::ERITile<DPB> * pTile) {

    //group all stuff
    TS.PairAll();

    uint64_t Ntiles = (TS.Npairs+DPB-1)/DPB;

    //get some constants
    uint32_t ta = SP12->ata;
    uint32_t tb = SP12->atb;
    uint32_t tc = SP34->ata;
    uint32_t td = SP34->atb;

    uint32_t offAB = STP->GetOffset(ta, tb, ABp->nb1, ABp->nb2);
    uint32_t offCD = STP->GetOffset(tc, td, CDp->nb1, CDp->nb2);

    uint32_t offAC = STP->GetOffset(ta, tc, ABp->nb1, CDp->nb1);
    uint32_t offAD = STP->GetOffset(ta, td, ABp->nb1, CDp->nb2);
    uint32_t offBC = STP->GetOffset(tb, tc, ABp->nb2, CDp->nb1);
    uint32_t offBD = STP->GetOffset(tb, td, ABp->nb2, CDp->nb2);

    uint32_t offBA = STP->GetOffset(tb, ta, ABp->nb2, ABp->nb1);
    uint32_t offDC = STP->GetOffset(td, tc, CDp->nb2, CDp->nb1);



    //make the tiles
    for (int n=0; n<Ntiles; ++n) {

        int used = TS.T5[n].used;

        pTile[n].nKab = K2max12 - TS.T5[n].ij.i - 1;
        pTile[n].nKcd = K2max34 - TS.T5[n].ij.j - 1;

        pTile[n].used = used;
        pTile[n].inuse = (1UL<<used) -1; //not the most portable solution...
        pTile[n].useJ = pTile[n].useX = pTile[n].inuse;

        for (int k=0; k<used; ++k) {
            pTile[n].ap12[k] = TS.T5[n].SP[k].n12;
            pTile[n].ap34[k] = TS.T5[n].SP[k].n34;
        }
        for (int k=used; k<DPB; ++k) {
            pTile[n].ap12[k] = pTile[n].ap12[0];
            pTile[n].ap34[k] = pTile[n].ap34[0];
        }

        pTile[n].wSP12 = SP12->WW;
        pTile[n].wSP34 = SP34->WW;
        pTile[n].nK2ab = ABp->Ka * ABp->Kb;
        pTile[n].nK2cd = CDp->Ka * CDp->Kb;
        pTile[n].offAB = offAB;
        pTile[n].offCD = offCD;
        pTile[n].offAC = offAC;
        pTile[n].offAD = offAD;
        pTile[n].offBC = offBC;
        pTile[n].offBD = offBD;
        pTile[n].offBA = offBA;
        pTile[n].offDC = offDC;
    }

    return Ntiles;
}

template<bool J, bool X, int S> bool ERIblock::MakeBlocksN      (float logD, float logS, const b2screener & logAD,  const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {


    logD = logS; //don't try anything fancy; just prescreen at the same level

    const int NPBLOCK = DOUBLES_PER_BLOCK;

    uint64_t MaxTiles = BlockBufferGPU::MaxTilesN(*this);

    if (MaxTiles==0) {
        Echidna::EMessenger << "Error: not enough ERI buffer memory; increase program memory or reduce number of threads" << endl;
        throw (1423);
    }

    //copypasta for vector
    {

        SetInitialList(logD, true); // don't initialize the maxK4 lists
        InitLists();

        uint64_t max_tiles = min (MaxTiles, MAX_TILES_PER_BLOCK);

        // if only exchange is being computed, initialize tensors for ONX
        //if (!J && X) InitONX(logAD, logDs);

        //generate batches of ERIs until the buffer mnemory is exhausted or the whole class is finished
        while (1) {
            uint64_t size = max_tiles * sizeof(ERITile<DPB>);
            ERITile<DPB> * tList = (ERITile<DPB>*)EBB->Allocate(size);

            // no memory at all!!!
            if (tList==NULL) return false;

            uint64_t nTilesWeGot = size/sizeof(ERITile<DPB>);


            uint16_t sABb = State.storeABb;
            uint16_t sCDb = State.storeCDb;
            uint32_t sAB  = State.storeAB;
            uint32_t sCD  = State.storeCD;

            uint64_t Nprescreened;
            TS.Npairs = 0;

            {
                if (S==3)           Nprescreened = PSCListS <J,X  > (logD, logS, logAD, logDs, nTilesWeGot*DPB);
                else                Nprescreened = PSCList  <J,X,0> (logD, logS, logAD, logDs, nTilesWeGot*DPB);
            }

            uint64_t nFinalTiles = (Nprescreened + DPB-1)/DPB;


            if (Nprescreened==0) {
                EBB->Free(tList, size);
                break; //no quadruplets left
            }
            else {
                //Reallocate mem
                EBB->ReAllocate(tList, size, nFinalTiles*sizeof(ERITile<DPB>));
                size = nFinalTiles*sizeof(ERITile<DPB>);
            }


            ERIBatch * newbatch  = new ERIBatch;
            *newbatch                 = *this;
            newbatch->TileListN       = tList;
            newbatch->memAllocated    = size;
            newbatch->J               = J;
            newbatch->X               = X;

            newbatch->NtilesN         = MakeAllTilesTS(newbatch->TileListN);

            if (nFinalTiles!=newbatch->NtilesN) {
                cout << "Error! Number of final tiles does not coincide!    " << nFinalTiles << " " << newbatch->NtilesN << "      " << TS.Npairs <<  endl;
            }

            TaskList.push(newbatch);
        }

        State.storeABb = 0;
        State.storeCDb = 0;
        State.storeAB  = 0;
        State.storeCD  = 0;

        //free the memory
        //if (!J && X) CleanONX();
    }

    return true;
}




bool ERIblock::MakeCoulombN (float logD, float logS,  const b2screener & logAD, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    //PrintEvaluationInfo(geometry, ABp, CDp);
    //Echidna::EBenchmarker << " Coulomb only" << endl;

    if (geometry==ABAB)          return MakeBlocksN <true, false, 3> (logD, logS, logAD, logDs, TaskList);
    else                         return MakeBlocksN <true, false, 0> (logD, logS, logAD, logDs, TaskList);
}

bool ERIblock::MakeExchangeN(float logD, float logS,  const b2screener & logAD, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    //PrintEvaluationInfo(geometry, ABp, CDp);
    //Echidna::EBenchmarker << " Exchange only" << endl;

    if (geometry==ABAB)          return MakeBlocksN <false, true, 3> (logD, logS, logAD, logDs, TaskList);
    else                         return MakeBlocksN <false, true, 0> (logD, logS, logAD, logDs, TaskList);
}

bool ERIblock::MakeAllN     (float logD, float logS,  const b2screener & logAD, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList) {

    //PrintEvaluationInfo(geometry, ABp, CDp);
    //Echidna::EBenchmarker << " Coulomb and Exchange simultaneously" << endl;

    if (geometry==ABAB)          return MakeBlocksN <true, true, 3> (logD, logS, logAD, logDs, TaskList);
    else                         return MakeBlocksN <true, true, 0> (logD, logS, logAD, logDs, TaskList);
}


