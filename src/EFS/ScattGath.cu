

/*

        LEFTOVER ROUTINES FROM THE VARIOUS ATTEMPTS TO IMPLEMENT THE GATHER TRANSFORMATIOM
            AND SUBSEQUENT DEBUG

*/


__global__ void CopyDMs (const int * pA, const int * pB, const int * pC, const int * pD,
                         const double * vD, const int * pDm, int nAtoms,
                         int JA, int JB, int JC, int JD,
                         int la, int lb, int lc, int ld,
                         uint32_t offAB, uint32_t offCD, uint32_t offAC, uint32_t offAD, uint32_t offBC, uint32_t offBD,
                         double * Dab, double * Dcd, double * Dac, double * Dad, double * Dbc, double * Dbd,
                         bool invAB, bool invCD,
                         int Ntiles) {


    const int k = threadIdx.x;
    const int i = blockIdx.y * blockDim.y + threadIdx.y;

    if (i>=Ntiles) return;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;
    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBD = JB*JD*MB*MD;
    const int sBC = JB*JC*MB*MC;

    const int ik = i*NTX + k;



    int ta,tb, tc,td;

    if (!invAB) {
        ta = pA[ik];
        tb = pB[ik];
    } else {
        ta = pB[ik];
        tb = pA[ik];
    }

    if (!invCD) {
        tc = pC[ik];
        td = pD[ik];
    } else {
        tc = pD[ik];
        td = pC[ik];
    }



    __shared__ const double * src[NTX];
    __shared__ double shared[NTX][NTX];


    // AB

    src[k] = vD + pDm [ ta*nAtoms + tb ] + offAB;
    __syncthreads();

    for (int t=0; t<NTX; ++t)        if (k<sAB) shared[t][k] = src[t][k];  // get a consecutive value of the DM subblock
    __syncthreads();                      // wait until all threads are done

    for (int mm=0; mm<sAB; ++mm) Dab [i*sAB*NTX + mm*NTX + k] = shared[k][mm];



    // CD

    src[k] = vD + pDm [ tc*nAtoms + td ] + offCD;
    __syncthreads();

    //for (int t=0; t<NTX; ++t)        if (k<sCD) shared[t][k] = src[t][k];  // get a consecutive value of the DM subblock
    //__syncthreads();                      // wait until all threads are done

    for (int mm=0; mm<sCD; ++mm) Dcd [i*sCD*NTX + mm*NTX + k] = shared[k][mm];



    // AC

    src[k] = vD + pDm [ ta*nAtoms + tc ] + offAC;
    __syncthreads();

    //for (int t=0; t<NTX; ++t)        if (k<sAC) shared[t][k] = src[t][k];  // get a consecutive value of the DM subblock
    //__syncthreads();                      // wait until all threads are done

    for (int mm=0; mm<sAC; ++mm) Dac [i*sAC*NTX + mm*NTX + k] = shared[k][mm];


    // AD

    src[k] = vD + pDm [ ta*nAtoms + td ] + offAD;
    __syncthreads();

    //for (int t=0; t<NTX; ++t)         if (k<sAD) shared[t][k] = src[t][k];  // get a consecutive value of the DM subblock
    //__syncthreads();                      // wait until all threads are done

    for (int mm=0; mm<sAD; ++mm) Dad [i*sAD*NTX + mm*NTX + k] = shared[k][mm];


    // BC

    src[k] = vD + pDm [ tb*nAtoms + tc ] + offBC;
    __syncthreads();

    //for (int t=0; t<NTX; ++t)         if (k<sBC) shared[t][k] = src[t][k];  // get a consecutive value of the DM subblock
    //__syncthreads();                      // wait until all threads are done

    for (int mm=0; mm<sBC; ++mm) Dbc [i*sBC*NTX + mm*NTX + k] = shared[k][mm];


    // BD

    src[k] = vD + pDm [ tb*nAtoms + td ] + offBD;
    __syncthreads();

    //for (int t=0; t<NTX; ++t)         if (k<sBD) shared[t][k] = src[t][k];  // get a consecutive value of the DM subblock
    //__syncthreads();                      // wait until all threads are done

    for (int mm=0; mm<sBD; ++mm) Dbd [i*sBD*NTX + mm*NTX + k] = shared[k][mm];


    /*
    //copy all
    for (int mm=0; mm<sAB; ++mm) Dab [i*sAB*NTX + mm*NTX + k] = dab[mm];
    for (int mm=0; mm<sCD; ++mm) Dcd [i*sCD*NTX + mm*NTX + k] = dcd[mm];

    for (int mm=0; mm<sAC; ++mm) Dac [i*sAC*NTX + mm*NTX + k] = dac[mm];
    for (int mm=0; mm<sAD; ++mm) Dad [i*sAD*NTX + mm*NTX + k] = dad[mm];
    for (int mm=0; mm<sBD; ++mm) Dbd [i*sBD*NTX + mm*NTX + k] = dbd[mm];
    for (int mm=0; mm<sBC; ++mm) Dbc [i*sBC*NTX + mm*NTX + k] = dbc[mm];
    */
}


template <class T> void QuickSort(T * keys, int N) {

    //for short lists default to selection sort
    if (N<64) {
        for (int i=0; i<N; ++i)  {
            T best = keys[i];
            int   k   = i;

            for (int j=i+1; j<N; ++j)
                if (keys[j]<best) {best = keys[j]; k = j;}

            swap(keys[i]    , keys[k]);
        }
        return;
    }

    T & k1 = keys[0];
    T & k2 = keys[N/2];
    T & k3 = keys[N-1];
    T kpiv;

    if      (k1<=k2 && k2<=k3) kpiv = k2;
    else if (k3<=k2 && k2<=k1) kpiv = k2;
    else if (k1<=k3 && k3<=k2) kpiv = k3;
    else if (k2<=k3 && k3<=k1) kpiv = k3;
    else                       kpiv = k1;


    //count number of elements lower than the pivot
    int nl=0;
    int ng=0;
    for (int i=0; i<N; ++i) {
        if      (keys[i] < kpiv) ++nl;
        else if (keys[i] > kpiv) ++ng;
    }

    int i,j,k;


    i=0;
    j=nl;

    while (1) {
        //find the first useful place in the first half of the list
        while (i<nl && keys[i]<kpiv) ++i;
        if (i>=nl) break;
        while (j<N  && keys[j]>=kpiv) ++j;
        if (j>=N) break;

        swap(keys[i]    , keys[j]);
    }

    j = nl;
    k = N-ng;

    while (1) {
        //find the first useful place in the first half of the list
        while (j<N-ng && keys[j]==kpiv) ++j;
        if (j>=N-ng) break;
        while (k<N  && keys[k]>kpiv) ++k;
        if (k>=N) break;

        swap(keys[j]    , keys[k]);
    }


    //QuickSort both sublists (skipping the pivot)
    QuickSort(keys         ,  nl);
    QuickSort(keys + (N-ng),  ng);
}

//this doesn't work because atomicAdd is not implemented for double
__global__ void GatherFM   (const int * p1, const int * p2,   double * vF, const int * pFm, int nAtoms,
                            int J1, int J2,    int l1, int l2,   uint32_t off12,   const double * F12,  int Ntiles) {


    const int x = threadIdx.x;
    const int y = threadIdx.y;

    const int n = blockIdx.y;

    const int NTY = blockDim.y;

    if (n>=Ntiles) return;

    const int M1 = 2*l1+1;
    const int M2 = 2*l2+1;

    const int s12 = J1*J2*M1*M2;


    //__shared__ int t2[NTX];
    __shared__ double * src[NTX];
    extern __shared__ double shared [];

    if (y==0) {
        const int nx = n*NTX + x;
        int t1 = p1[nx];
        int t2 = p2[nx];
        src[x] = vF + pFm [ t1*nAtoms + t2 ] + off12; // this is totally non-coalesced
    }
    __syncthreads(); // make src visible to all

    int yx = y*NTX + x;

    int s = yx/NTY;
    int d = yx%NTY;
    int ds = d*NTX + s;


    shared[yx] = F12 [n*s12*NTX + yx]; // perfectly coalesced
    __syncthreads();

    //src[s][d] += shared[ds]; // coalesced!!!! (as much as possible)
    //atomicAdd (&(src[s][d]), shared[ds]);
    __syncthreads();

}



__global__ void ReverseMap (const uint32_t * p1, const uint32_t * p2,   uint32_t * o,  uint32_t * h, uint32_t nAtoms, uint32_t nelements) {

    const uint32_t i     = blockIdx.x;
    const uint32_t dimx  = blockDim.x;
    const uint32_t dimy  = blockDim.x;
    const uint32_t thx   = threadIdx.x;
    const uint32_t thy   = threadIdx.y;

    uint32_t indx = i*dimx*dimy + thy*dimx + thx;

    if (indx>=nelements) return;

    uint32_t at1 = p1[indx];
    uint32_t at2 = p2[indx];

    uint32_t at12 = at1*nAtoms + at2;

    //write it somewhere
    o[2*indx  ] = at12;
    o[2*indx+1] = indx;

    //update the histogram
    atomicAdd(&h[at12],1);
}


// gather the sorted submatrices to where they belong before the parallel reduction
__global__ void GatherSorted (double * out, const unsigned int * pout, const unsigned int * nout,    const double * in, const uint64_t * pin) {

    unsigned int bx = blockIdx.x;   // this iterates over the number of tiles
    unsigned int by = blockIdx.y;   // this iterates over the groups

    unsigned int tx = threadIdx.x;  // element of the tile
    unsigned int ty = threadIdx.y;  // element in the submatrix

    unsigned int dimx = blockDim.x; // this is the length of the vector, 32 as usual
    unsigned int dimy = blockDim.y; // this is the number of elements of the matrix sub-block

    if ( bx >= (nout[by]-1+dimx)/dimx ) return;       // check the tile number is within the bounds of the group

    //possibly extremely slow
    if (bx*dimx+tx>=nout[by]) // check the id of the submatrix is within the bounds of the group
        out [ pout[by]*dimy + bx*dimx*dimy + ty*dimx + tx ] = 0;

    else {
        uint32_t indx = (pin[bx*dimx+tx] & 4294967295); // eliminate upper bits (which contain the two parent atoms)

        unsigned int bbx = indx/dimx;  // number of tile 'indx' corresponds to
        unsigned int ttx = indx%dimx;  // number of individual unit in the tile

        out [ pout[by]*dimy + bx*dimx*dimy + ty*dimx + tx ] = in [ bbx*dimx*dimy + ty*dimx + ttx ];
    }
}

// gather the sorted submatrices to where they belong before the parallel reduction
__global__ void ReduceDMs (double * out, const unsigned int * pout, const unsigned int * nout,    const double * in, const unsigned int * pin, const unsigned int * nin) {

    extern __shared__ double sdata[];

    unsigned int bx = blockIdx.x;   // this iterates over the number of tiles
    unsigned int by = blockIdx.y;   // this iterates over the groups

    unsigned int tx = threadIdx.x;  // element of the tile
    unsigned int ty = threadIdx.y;  // element in the submatrix

    unsigned int dimx = blockDim.x; // this is the length of the vector, 32 as usual
    unsigned int dimy = blockDim.y; // this is the number of elements of the matrix sub-block

    unsigned int tyx = ty*dimx + tx;

    sdata[tyx] = 0; // initialize everything to 0 and avoid a future headache


    if (bx*dimx+tx >= nin[by]) return; // exceeds number of elements for given sublist

    unsigned int i = bx * (dimy * dimx) + tyx; //
    sdata[tyx] = in [ pin[by]*dimy + i ];
    __syncthreads();

    // do reduction in shared mem
    for (unsigned int s=1; s<dimx; s*=2) {
        if (tx % (2*s) == 0) {
            sdata[tyx] += sdata[tyx + s];
        }
        __syncthreads();
    }

    // write result for this block to global mem
    unsigned int bbx = bx / dimx; // new 'block' for the output
    unsigned int ttx = bx % dimx; // new 'thread' in the block

    unsigned int j = (bbx*dimy + ty)*dimx + ttx;

    if (tx==0) out[ pout[by]*dimy + j] = sdata[ty*dimx];
}

// final step, where everything is added to the device array
__global__ void FinalReduce ( double * vF, const unsigned int * pFm, const unsigned int * A2, unsigned int offset,    const double * in, const unsigned int * pin) {

    unsigned int by = blockIdx.y;   // this iterates over the groups
    unsigned int ty = threadIdx.y;  // element in the submatrix
    unsigned int dimy = blockDim.y;  // element in the submatrix

    double * F = vF + pFm [ A2[by] ] + offset;

    F[ty] += in [ pin[by]*dimy + ty*NTX ];
}


template <class T> T round32 (T rhs) {
    T ret;
    ret = 32*((rhs+31)/32);
    return ret;
}

void BatchEvaluator::CopyDJXfromDevice  (const ERIBatch & TheBatch, cudaStream_t * stream, SparseGPU & gJs, SparseGPU & gXs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;

    bool SS = ((TheBatch.geometry==ABAB) && TheBatch.SameShell);
    bool SAB = TheBatch.ABp->samef;
    bool SCD = TheBatch.CDp->samef;


    if      (SAB && SCD) {
        int nel = TheBatch.NtilesN * (sAB+sCD+sAD+sAC+sBC+sBD) * DPB;
        Quarter <<<(nel+255)/256, 256, 0,*stream>>> (b4block.dFab, nel);
    }
    else if (SS || SAB || SCD) {
        int nel = TheBatch.NtilesN * (sAB+sCD+sAD+sAC+sBC+sBD) * DPB;
        Half    <<<(nel+255)/256, 256, 0,*stream>>> (b4block.dFab, nel);
    }

    int nAtoms = gJs.len;
    int nElements = DPB * (TheBatch.NtilesN-1) + TheBatch.TileListN[TheBatch.NtilesN-1].used; // true number of elements (excluding padding)


    // two atoms' + a batch Ids packed into 64 bits
    uint64_t * A2E = new uint64_t[nElements];

    for (uint64_t e=0; e<nElements; ++e) {
        uint64_t ata = b4block.AtA[e];
        uint64_t atb = b4block.AtB[e];
        A2E[e] = ( (ata*nAtoms + atb)<<32 ) + e;
    }

    // sort the hashes
    QuickSort (A2E, nElements);

    // count unique atom pairs
    int nGroups = 0; {
        uint64_t A2o = -1;
        for (uint64_t i=0; i<nElements; ++i) {
            uint64_t A2i = A2E[i]>>32;
            if (A2i!=A2o) {++nGroups; A2o = A2i;}
        }
    }

    // count elements within each group; save each atom pair
    unsigned int * nEs  = new unsigned int[nGroups]; // number of elements in the group
    unsigned int * A2   = new unsigned int[nGroups]; // id for atom pair

    {
        for (int i=0; i<nGroups; ++i) nEs[i] = 0;

        int iGroup = -1;
        uint64_t A2o = -1;
        for (uint64_t i=0; i<nElements; ++i) {
            uint64_t A2i = A2E[i]>>32;
            if (A2i!=A2o) {++iGroup; A2o = A2i;}
            ++nEs[iGroup];       // add an element
            A2[iGroup] = A2i; // save the code for the atom pair many times
        }
    }

    // find the longest list amongst all groups
    unsigned int maxList = 0;
    for (int i=0; i<nGroups; ++i)
        maxList = max(maxList, nEs[i]);

    unsigned int maxNtiles = round32(maxList)/32; //maximum number of tiles



    unsigned int * pGroup = new unsigned int[nGroups]; // reference to first element in the group

    unsigned int nElementsPadding = 0;
    for (int i=0; i<nGroups; ++i) {
        pGroup[i] = nElementsPadding;
        nElementsPadding += round32(nEs[i]);
    }

    double   * d_FF;   // allocate, etc.
    cudaMalloc ((void**)&d_FF  , nElementsPadding*sAB*sizeof(double));

    // "gather" (sort) all submatrices
    // ===============================

    dim3 Nbl (maxNtiles, nGroups);
    dim3 Nab (NTX, sAB);
    GatherSorted <<<Nbl,Nab, 0,*stream>>> (d_FF, pGroup, nEs,    b4block.dFab, A2E); // groups DMs which belong to the same atom pair


    // parallel reduce
    // ===============
    const int MAXNLEVEL = 4; // 32^4 > 10^6 elements

    for (int nlevel=0; nlevel<MAXNLEVEL; ++nlevel) {

        unsigned int * nEs2 = new unsigned int[nGroups]; // number of elements after one level of reduction

        for (int i=0; i<nGroups; ++i) nEs2[i] = round32(nEs[i])/32; //don't forget everything must be aligned


        unsigned int * pGroup2 = new unsigned int[nGroups]; // reference to first element in the group

        unsigned int nElementsPadding2 = 0;
        for (int i=0; i<nGroups; ++i) {
            pGroup2[i] = nElementsPadding2;
            nElementsPadding2 += round32(nEs2[i]);
        }

        double   * d_FF2;   // allocate, etc.
        cudaMalloc ((void**)&d_FF2  , nElementsPadding2*sAB*sizeof(double));

        //new level
        dim3 Nbl (maxNtiles, nGroups);
        ReduceDMs <<<Nbl,Nab, sAB*NTX*8,*stream>>> (d_FF2, pGroup2, nEs2,   d_FF, pGroup, nEs); // groups DMs which belong to the same atom pair


        // replace data with current level's
        nEs       = nEs2;
        pGroup    = pGroup2;
        d_FF      = d_FF2;
        maxNtiles = round32(maxNtiles)/32;
    }

    // add to matrix
    // =============

    dim3 Nby (1, nGroups);
    dim3 Nty (1, sAB);

    FinalReduce <<<Nby,Nty, 0, *stream>>> (gJs.v[GPUdevice], gJs.p[GPUdevice], A2, offAB,    d_FF, pGroup);


    // free as the wind
    // ----------------

    delete[] nEs;
    delete[] pGroup;
    delete[] A2E;
    delete[] A2;

}






// first pass to count the number of instances of a2 appears
//template<bool first>
__global__ void ReduceList   (uint16_t * count, uint64_t * a2c, const uint64_t * a2e, int nElements) {

    __shared__ uint32_t a2[1024];
    __shared__ uint16_t  c[1024];

    int tx = threadIdx.x;

    uint64_t n = blockIdx.x*1024 + tx;
    if (n>=nElements) return;

    a2     [tx] = a2e     [n]>>32; // higher bits
    //if (first)
    c [tx] = 1;
    //else       c [tx] = a2e[n]&((1<<31)-1); //lower bits

    __syncthreads();

    __shared__ int ix=0;

    if (tx==0) {
        uint16_t cc  = 0; // total counter
        uint32_t aa2 = a2[0];

        for (int i=0; i<1024; ++i) {
            if (a2[i]==aa2)
                cc += c[i];
            else {
                //save
                a2[ix] = aa2;
                c [ix] = cc;
                //new value
                ++ix;
                cc = 0;
                aa2 = a2[i];
            }
        }
        //save last
        a2[ix] = aa2;
        c [ix] = cc;
        ++ix;
    }
    __syncthreads();

    // write compacted result
    if (tx<ix)
        a2c[tx] = (a2[tx]<<32) + c[tx];
    // number of used elements
    if (tx==0)
        count[blockIdx.x] = ix;
}

//fuse/merge two contiguous tiles, and do so recursively
__global__ void ReduceListR  (uint16_t * count, uint64_t * a2c, const uint64_t * a2e, int nElements) {

}

//very inefficient reduction
__global__ void ReduceF   (double * dF, uint64_t * a2e, int nElements, int len, int N) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;

    const int N = 1<<L; // add every Nth element

    const int el1 = 2*N*n  ;
    if (el1>=nElements) return;
    const int el2 = 2*N*n+N;
    if (el2>=nElements) return;

    uint64_t i1 = a2e[el1];
    uint64_t i2 = a2e[el2];

    if ((i1>>32)!=(i2>>32)) return;

    i1 = i1 & ((1<<31)-1);
    i2 = i2 & ((1<<31)-1);

    const int b1 = i1/DPB;
    const int t1 = i1%DPB;

    const int b2 = i2/DPB;
    const int t2 = i2%DPB;

    for (int s=0; s<len; ++s)
        dF [b1*len*DPB + s*DPB + t1] += dF [b2*len*DPB + s*DPB + t2];

    a2e[el2] = -1; // should fill with FFF...
}


void ReduceAll (double * dF, uint64_t * a2e, int nElements, int len) {

    for (int L=0;;++L) {
        ReduceF   (dF, a2e, nElements, len, L);
    }
}


// parallel prefix sum, stolen from somewhere and modified
__global__ void PrefixSum (uint64_t * out, uint64_t * in, int n) {

    extern __shared__ uint64_t temp[];

    int thid = threadIdx.x;
    int offset = 1;

    temp[2*thid]   = in[2*thid];
    temp[2*thid+1] = in[2*thid+1];

    // build sum in place up the tree
    for (int d = n>>1; d > 0; d >>= 1) {
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            temp[bi] += temp[ai];
        }
        offset *= 2;
    }

    if (thid == 0) temp[n-1] = 0; // clear the last element

    // traverse down tree & build scan
    for (int d = 1; d < n; d*=2) {
        offset >>= 1;
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            float t = temp[ai];
            temp[ai] = temp[bi];
            temp[bi] += t;
        }
    }
    __syncthreads();

    // write results to device memory
    out[2*thid]   = temp[2*thid];
    out[2*thid+1] = temp[2*thid+1];
}


// based on optimized reduction, according to NVIDIA
// reduce elements only within the block
__global__ void ParallelReduce (uint32_t * out, const uint32_t * in, uint32_t n) {
    const uint32_t BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];

    uint32_t tid = threadIdx.x;
    uint32_t base = (2*BSIZE)*blockIdx.x;

    if (base >= n) return; // if i is out of bounds, skip entirely

    sdata[tid] = 0;

    if (base + tid < n)
        sdata[tid]  = in[base + tid];
    if (base + BSIZE + tid < n)
        sdata[tid] += in[base + BSIZE + tid];
    __syncthreads();

    //across warps
    if (tid < 512) sdata[tid] += sdata[tid + 512];
    __syncthreads();
    if (tid < 256) sdata[tid] += sdata[tid + 256];
    __syncthreads();
    if (tid < 128) sdata[tid] += sdata[tid + 128];
    __syncthreads();
    if (tid <  64) sdata[tid] += sdata[tid +  64];
    __syncthreads();

    //within same warp
    if (tid < 32) {
        sdata[tid] += sdata[tid + 32];
        sdata[tid] += sdata[tid + 16];
        sdata[tid] += sdata[tid +  8];
        sdata[tid] += sdata[tid +  4];
        sdata[tid] += sdata[tid +  2];
        sdata[tid] += sdata[tid +  1];
    }

    if (tid == 0) out[blockIdx.x] = sdata[0];
}

// parallel scan for just one block (<=1K elements)
__global__ void ParallelScan1   (uint32_t * p, uint32_t n) {
    const uint32_t BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];

    uint32_t tid = threadIdx.x;
    uint32_t base = (BSIZE)*blockIdx.x;

    if (base >= n) return; // if i is out of bounds, skip entirely

    sdata[tid] = 0;

    // input
    if (base + tid < n)
        sdata[tid]  = p[base + tid];
    __syncthreads();

    #pragma unroll
    for (int i=1; i<BSIZE; i*=2) {
        if (tid+i<BSIZE)
            sdata[tid+i] += sdata[tid];
        __syncthreads();
    }

    // don't forget first element
    //if (tid==0) p[base] = 0;

    // output
    if (base + tid < n)
        p[base + tid ] = sdata[tid];
}

// parallel scan for multiple blocks
__global__ void ParallelScanN   (uint32_t * p, uint32_t n) {
    const uint32_t BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];

    uint32_t tid = threadIdx.x;
    uint32_t base = (BSIZE)*blockIdx.x;

    if (base >= n) return; // if i is out of bounds, skip entirely

    sdata[tid] = 0;

    // input
    if (base + tid < n)
        sdata[tid]  = p[base + tid];
    __syncthreads();

    #pragma unroll
    for (int i=1; i<BSIZE; i*=2) {
        if (tid+i<BSIZE)
            sdata[tid+i] += sdata[tid];
        __syncthreads();
    }

    // don't forget first element
    if (tid==0) p[base] = 0;

    // output
    if (base + tid < n)
        p[base + tid + 1] = sdata[tid];
}






void printAt2 (const uint32_t * a, const uint32_t * p, const uint32_t * c, uint32_t n) {

    uint32_t * h_a = new uint32_t[n];
    uint32_t * h_p = new uint32_t[n];
    uint32_t * h_c = new uint32_t[n];

    cudaMemcpy (h_a, a, n*sizeof(uint32_t), cudaMemcpyDeviceToHost);
    cudaMemcpy (h_p, p, n*sizeof(uint32_t), cudaMemcpyDeviceToHost);
    cudaMemcpy (h_c, c, n*sizeof(uint32_t), cudaMemcpyDeviceToHost);

    for (int i=0; i<n; ++i)
        if (h_p[i]!=MASK32) cout  << i << "  " << h_a[i] << " " << h_p[i] << " " << h_c[i] <<  endl;

    delete[] h_a;
    delete[] h_p;
    delete[] h_c;

    char aa; cin >> aa;
}

void printAt2 (const uint32_t * a, const uint32_t * p, const uint32_t * c, uint32_t n, uint32_t *e, uint32_t m) {

    uint32_t * h_a = new uint32_t[n];
    uint32_t * h_p = new uint32_t[n];
    uint32_t * h_c = new uint32_t[n];
    uint32_t * h_e = new uint32_t[m];

    cudaMemcpy (h_a, a, n*sizeof(uint32_t), cudaMemcpyDeviceToHost);
    cudaMemcpy (h_p, p, n*sizeof(uint32_t), cudaMemcpyDeviceToHost);
    cudaMemcpy (h_c, c, n*sizeof(uint32_t), cudaMemcpyDeviceToHost);
    cudaMemcpy (h_e, e, m*sizeof(uint32_t), cudaMemcpyDeviceToHost);

    for (int i=0; i<n; ++i) {
        cout << h_a[i] << " :: ";
        for (int j=0; j<h_c[i]; ++j)
            cout << " " << h_e[h_p[i]+j];
         cout << endl;
    }

    delete[] h_a;
    delete[] h_p;
    delete[] h_c;
    delete[] h_e;

    char aa; cin >> aa;
}




static const int BTNT = 1024;

void BTsort (uint64_t * list, uint64_t nList, cudaStream_t * stream);


// parallel scan within the block; write the total sum elsewhere
__global__ void ParallelScanL   (uint32_t * s, uint32_t * p, uint32_t n) {
    const uint32_t BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];

    uint32_t tid = threadIdx.x;
    uint32_t base = (BSIZE)*blockIdx.x;

    if (base >= n) return; // if i is out of bounds, skip entirely

    sdata[tid] = 0;

    // input
    if (base + tid < n)
        sdata[tid]  = p[base + tid];
    __syncthreads();

    #pragma unroll
    for (int i=1; i<BSIZE; i*=2) {
        if (tid+i<BSIZE) sdata[tid+i] += sdata[tid];
        __syncthreads();
    }

    // write result
    if (base + tid < n) {
        if (tid==0) p[base]       = 0;
        else        p[base + tid] = sdata[tid-1];
    }

    // write total sum elsewhere
    if (tid == 0) s[blockIdx.x] = sdata[BSIZE-1]; //the first element should be 0, but we skip it since it's never accessed
}

// add the prefixs to the whole block
__global__ void ParallelAddL   (uint32_t * p, const uint32_t * s, uint32_t n) {
    const uint32_t BSIZE = 1024;

    uint32_t tid = threadIdx.x;
    uint32_t bid = blockIdx.x;
    uint32_t base = (BSIZE)*bid;

    /*
    if (bid==0) return; // the first block's prefix is supposed to be 0, if everything is correct
    if (base>n) return;

    __shared__ uint32_t prefix;
    if (tid == 0) prefix = s[bid];
    __syncthreads();

    if (base + tid < n) p[base + tid] += prefix; // add prefix to all elements
    */
    if (base + tid < n) p[base + tid] += s[bid];
}


__global__ void ParallelScanS   (uint32_t * p, const uint32_t * c, uint32_t n) {

    const uint32_t BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];

    uint32_t tid = threadIdx.x;

    //initialize the total sum
    __shared__ uint32_t offset;
    if (tid == 0) offset = 0;
    __syncthreads();

    //execute sequentially
    while (1) {
        // load input
        if (tid < n) sdata[tid]  = c[tid];
        else         sdata[tid]  = 0;
        __syncthreads();

        #pragma unroll
        for (int i=1; i<BSIZE; i*=2) {
            if (tid+i<BSIZE) sdata[tid+i] += sdata[tid];
            __syncthreads();
        }

        // write result
        if (tid < n) {
            if (tid==0) p[tid] = offset;
            else        p[tid] = offset + sdata[tid-1];
            __syncthreads();
        }

        if (tid==0) offset += sdata[BSIZE-1];
        __syncthreads();

        if (n<=BSIZE) break;

        c += BSIZE;
        p += BSIZE;
        n -= BSIZE;
    }

    if (tid == 0) p[n] = offset;
}

// returns on p[n] the sum of the first c[m] m:0,n elements
void Scan (uint32_t * p, const uint32_t * c, uint32_t nEl, cudaStream_t * stream, uint32_t * q) {

    // copy the count list to the prefix list, since it is going to be modified
    //cudaMemcpyAsync (p, c, nEl*sizeof(int32_t), cudaMemcpyDeviceToDevice, *stream);

    //uint32_t * r = p + nEl; // last element of the list

    // two-level parallel scan (prefix sum)
    //const uint32_t nBl = (nEl+1024-1)/1024;
    //ParallelScanL <<<nBl,1024,0,*stream>>> (q, p, nEl);
    //ParallelScanL <<<  1,1024,0,*stream>>> (r, q, nBl); // r contains the total sum now
    //ParallelAddL  <<<nBl,1024,0,*stream>>> (p, q, nEl); // add the reduced block prefixes

    ParallelScanS <<<  1,1024,0,*stream>>> (p, c, nEl); // r contains the total sum now
}

// self-explanatory
__global__ void CountNew (uint32_t * c, const uint32_t * e, uint32_t nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;

    if (n>=nElements) return;

    bool use = false;
    if (n==0) use = true;
    else if (e[n-1]!=e[n]) use = true;

    if (use) {
        //if (e[n]==-1) c[n] = -1; // ignore the tile later on
        //else
        c[n] =  1;
    }
    else     c[n] = 0;
}

// write the element to a compact array, if required
__global__ void CompactArrays (uint32_t * ao, uint32_t * po, const uint32_t * ai, const uint32_t * pi, const uint32_t * c, uint32_t nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;

    if (n>=nElements) return;

    //if it's a new element, write it to the compacted array
    if (c[n]!=0) {
        uint32_t p = pi[n];
        ao [p] = ai[n];
        po [p] = n;
    }
}

// count elements by making the difference between consecutive pointers
__global__ void CountFromPointers (uint32_t * c, const uint32_t * p, uint32_t pLast, uint32_t nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;

    if      (n>=nElements) return;
    else if (n==nElements-1)
        c[n] = pLast - p[n];
    else
        c[n] = p[n+1] - p[n];
}

__global__ void CompactArrays2 (uint32_t * a, uint32_t * p, uint32_t * c, const uint32_t * h, const uint32_t * q, uint32_t nElements, uint32_t nrElements) {

    uint64_t n = threadIdx.x;

    //__shared__ uint32_t pos[1024];


    __shared__ uint32_t nre;

    if (n==0) nre = 0;
    __syncthreads();


    while (n<nElements) {

        int used = ( (n==0) || ( (n>0)&&(h[n]!=h[n-1]) ) )?1:0;

        if (n==0) nre += 1;
        //__syncthreads_count (used);

        //if it's a new element, write it to the compacted array
        if (used) {
            uint32_t qn = q[n];
            a [qn  ] = h[n];
            p [qn  ] = n;
            if (qn>0) c [qn-1] = n;
        }

        n += 1024;
    }

    //will wait until all threads in the block are done
    __syncthreads();

    n = threadIdx.x;

    while (n<nrElements-1) {
        c[n] = p[n+1] - p[n];
        n += 1024;
    }

    if (n==nrElements-1) c[n] = nElements - p[n];
}

// takes a sorted array and gets the unique elements, their position and number of repetitions
void SortedHistogram (uint32_t * a, uint32_t * p, uint32_t * c, uint32_t * nOut,    const uint32_t * h, uint32_t nIn,    cudaStream_t * stream,    uint32_t * q) {

    //CPU CODE
    if (0) {
        uint32_t * hh = new uint32_t[4*nIn+32];
        uint32_t * ha = hh + nIn;
        uint32_t * hp = ha + nIn;
        uint32_t * hc = hp + nIn + 32;

        //cudaMemcpyAsync(hh, h, nIn*sizeof(int32_t), cudaMemcpyDeviceToHost, *stream);
        cudaMemcpy (hh, h, nIn*sizeof(int32_t), cudaMemcpyDeviceToHost);

        //cudaStreamSynchronize(*stream);

        hp[0] = 0;
        ha[0] = hh[0];

        uint32_t n = 1;

        for (int i=1; i<nIn; ++i) {
            if (hh[i]!=hh[i-1]) {
                ha[n] = hh[i];
                hp[n] = i;
                ++n;
            }
        }
        hp[n] = nIn;

        for (int i=0; i<n; ++i)
            hc[i] = hp[i+1]-hp[i];

        //cudaMemcpyAsync(a, ha, n*sizeof(int32_t), cudaMemcpyHostToDevice, *stream);
        //cudaMemcpyAsync(p, hp, n*sizeof(int32_t), cudaMemcpyHostToDevice, *stream);
        //cudaMemcpyAsync(c, hc, n*sizeof(int32_t), cudaMemcpyHostToDevice, *stream);
        cudaMemcpy(a, ha, n*sizeof(int32_t), cudaMemcpyHostToDevice);
        cudaMemcpy(p, hp, n*sizeof(int32_t), cudaMemcpyHostToDevice);
        cudaMemcpy(c, hc, n*sizeof(int32_t), cudaMemcpyHostToDevice);

        *nOut = n;

        delete[] hh;

        //cudaStreamSynchronize(*stream); //before freeing the memory
    }

    const int nBlIn   = (nIn  + BTNT-1)/BTNT;



    CountNew <<<nBlIn,BTNT,0,*stream>>> (c, h, nIn);   // count new to a temporal array


    ParallelScanS <<<  1,1024,0,*stream>>> (q, c, nIn);

    {
        uint32_t * hc = new uint32_t[4*nIn+32];
        uint32_t * hp = hc + nIn;
        uint32_t * hq = hp + nIn;
        uint32_t * hx = hq + nIn;

        cudaMemcpy (hc, c, nIn*sizeof(int32_t), cudaMemcpyDeviceToHost);
        cudaMemcpy (hq, q, nIn*sizeof(int32_t), cudaMemcpyDeviceToHost);
        cudaMemcpy (hx, p, nIn*sizeof(int32_t), cudaMemcpyDeviceToHost);

        uint32_t n = 0;

        for (int i=0; i<nIn; ++i) {
            hp[i] = n;
            if (hc[i]) ++n;
        }

        int pos;
        for (pos=0; pos<nIn; ++pos) if (hp[pos]!=hq [pos]) break;

        if (pos<nIn) {
            /*

            cout << "Not eq  !" << pos << endl;
            for (int i=0; i<nIn; ++i)
                cout << hp[i] << " "; cout << endl;cout << endl;
            for (int i=0; i<nIn; ++i)
                cout << hq[i] << " "; cout << endl;cout << endl;

            for (int i=0; i<nIn; ++i)
                cout << hx[i] << " "; cout << endl;

            char aa;
            cin >> aa;
            */
            cout << "*";

            cudaMemcpy(q, hp, nIn*sizeof(int32_t), cudaMemcpyHostToDevice);
        }

        *nOut = n;

        delete[] hc;
    }

/*
    cudaMemcpyAsync(nOut, &(q[nIn]), sizeof(int32_t), cudaMemcpyDeviceToHost, *stream); // copy last element of q from
*/

    CompactArrays2 <<<1,1024,0,*stream>>> (a,p,c,   h,q, nIn, *nOut); // write the selected (unique) elements to a compact array

    //cudaStreamSynchronize(*stream); // we need nOut for the last kernel

    //const int nBlOut  = (*nOut + BTNT-1)/BTNT;

    //CountFromPointers <<<nBlOut,BTNT,0,*stream>>> (c, p, nIn, *nOut);  // the array q must have a last extra element
}

__global__ void ParallelCount   (uint32_t * a, uint32_t * o,  uint32_t * c,  uint32_t * p, const uint32_t * h, uint32_t N) {

    const uint32_t BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];

    uint32_t tid = threadIdx.x;

    //initialize the total sum
    __shared__ uint32_t offset;
    if (tid == 0) offset = 0;
    __syncthreads();

    //execute sequentially
    int it = 0;

    while (1) {

        sdata[tid]  = 0;
        bool unique = false;

        if (tid < N) {
            int n = tid;
            if   (it==0 && tid==0) unique = true;
            else if (h[n-1]!=h[n]) unique = true;
        }
        if (unique) sdata[tid] = 1;
        __syncthreads();


        #pragma unroll
        for (int i=1; i<BSIZE; i*=2) {
            if (tid+i<BSIZE) sdata[tid+i] += sdata[tid];
            __syncthreads();
        }

        // write result
        if (tid < N) {
            if (tid==0) p[tid] = offset;
            else        p[tid] = offset + sdata[tid-1];
            __syncthreads();

            if (unique) {
                uint32_t pp = p[tid];
                a [pp] = h[tid];
                o [pp] = it*BSIZE + tid;
                //c [pp] = o [pp+1]-o[pp];
            }
        }

        if (tid==0) offset += sdata[BSIZE-1];
        __syncthreads();

        if (N<=BSIZE) break;

        h += BSIZE;
        p += BSIZE;
        N -= BSIZE;

        ++it;
    }

    if (tid == 0) p[N] = offset;
}

void SortedHistogramL (uint32_t * a, uint32_t * p, uint32_t * c, uint32_t * nOut,    const uint32_t * h, uint32_t nIn,    cudaStream_t * stream,    uint32_t * q) {

    ParallelCount <<<  1,1024,0,*stream>>> (a, p, c,   q, h, nIn);

    cudaMemcpyAsync(nOut, &(q[nIn]), sizeof(int32_t), cudaMemcpyDeviceToHost, *stream); // copy last element of q from

    cudaStreamSynchronize(*stream); // we need nOut for the last kernel

    const int nBlOut  = (*nOut + BTNT-1)/BTNT;

    CountFromPointers <<<nBlOut,BTNT,0,*stream>>> (c, p, nIn, *nOut);  // the array q must have a last extra element
}

// save a2 and its position for every first instance of an atom pair
__global__ void A2Plist (uint64_t * a2p, const uint32_t * a2, uint32_t nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;

    if      (n>nElements) return;
    else if (n==nElements) {
        uint64_t a = -1;
        a2p[n] = (a<<32) + n;
    }
    else if (n==0 || (a2[n]!=a2[n-1])) {
        uint64_t a = a2[n];
        a2p[n] = (a<<32) + n;
    }
    else {
        a2p[n] = -1;
    }
}

// split an array of int64 in two arrays of int32 by hi/lo bits of every value
__global__ void SplitUI64   (uint32_t * a, uint32_t * p, uint32_t * c, const uint64_t * ap, uint32_t nElements, uint32_t * nReduced) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;
    if (n>=nElements) return;

    // gather
    uint64_t apn = ap[n];

    // split
    uint32_t an  = apn >> 32;    // hi bits
    uint32_t pn  = apn & MASK32; //((1<<32)-1); // lo bits

    //if it is past the last element
    if (an==-1) {
        if (pn!=-1) nReduced[0] = n; //
    }
    else {
        // write to memory
        a[n] = an;
        p[n] = pn;
        c[n] = (ap[n+1] & MASK32) - pn;
    }
}

// a variant
void SortedHistogram2 (uint32_t * a, uint32_t * p, uint32_t * c, uint32_t * nOut,    uint32_t * h, uint32_t nIn,    cudaStream_t * stream,  uint64_t * a2p) {
    //one extra element for the last pointer
    const int nBlIn   = nIn/BTNT + 1;
    // make an array of unique atom pairs and pointers
    A2Plist   <<<nBlIn,BTNT,0,*stream>>> (a2p, h, nIn); // make a list of packed a2+pos; fill the rest with all bits, except for the last+1 element, which contains the number of elements
    //sort it (at1 > at2 > e)
    BTsort ((uint64_t*)a2p, nIn+1, stream);        // sorting leaves all valid elements in the lower part, then the extra element, then all the -1s
    //split a2p in two uint32 lists
    SplitUI64 <<<nBlIn,BTNT,0,*stream>>> (a, p, c, a2p, nIn, h); // splits the list in a2 and p, but also computes the number of elements for each a2 AND writes the total number of valid (not -1) elements to a pointer, h
    // copy number of elements (a2) in the compressed list(s)
    cudaMemcpyAsync(nOut, h, sizeof(int32_t), cudaMemcpyDeviceToHost, *stream);
}


__device__ uint32_t popcount (uint32_t x) {
    x ^= (-1); // invert bit pattern
    uint32_t pc; for (pc=0; x; pc++) x &= x-1; // fast when most bits are 0
    return 32-pc; //return maxcount - count
}

__global__ void CountUsed (uint32_t * p_total, const uint32_t * used, uint32_t n) {

    const int BSIZE = 1024;

    __shared__ uint32_t sdata[BSIZE];
    const uint32_t tid = threadIdx.x;
    uint32_t i   = tid;

    sdata[tid] = 0;

    while (i<n) {
        uint32_t x = used[i];
        x ^= (-1);
        uint32_t pc; for (pc=0; x; ++pc) x &= x-1;
        sdata[tid] += 32-pc;
        //sdata[tid] += popcount(used[i]);
        i += BSIZE;
    }
    __syncthreads();
    if (tid >= 512) return;

    sdata[tid] += sdata[tid + 512];
    __syncthreads();
    if (tid >= 256) return;

    sdata[tid] += sdata[tid + 256];
    __syncthreads();
    if (tid >= 128) return;

    sdata[tid] += sdata[tid + 128];
    __syncthreads();
    if (tid >=  64) return;

    sdata[tid] += sdata[tid +  64];
    __syncthreads();
    if (tid >=  32) return;

    sdata[tid] += sdata[tid + 32];
    if (tid <  16)
    sdata[tid] += sdata[tid + 16];
    if (tid <   8)
    sdata[tid] += sdata[tid +  8];
    if (tid <   4)
    sdata[tid] += sdata[tid +  4];
    if (tid <   2)
    sdata[tid] += sdata[tid +  2];
    if (tid <   1)
    *p_total = sdata[0]+sdata[1];
}

// split an array of int64 in two arrays of int32 by hi/lo bits of every value
__global__ void SplitUI64   (uint32_t * a, uint32_t * b, const uint64_t * ab, int nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;
    if (n>=nElements) return;

    // gather
    uint64_t abn = ab[n];

    // split
    uint32_t an  = abn >> 32;    // hi bits
    uint32_t bn  = abn & MASK32; //((1<<32)-1); // lo bits

    // write to memory
    a[n] = an;
    b[n] = bn;
}

void Reduce ( double * dF, // device array with all the relevant subblocks
                uint32_t * ip,
                const int * p1, const int * p2,  const uint32_t * use, uint32_t nAtoms, uint32_t nTiles, uint32_t off12, uint32_t S12, cudaStream_t * stream, double * vF, const uint32_t * pF) {

    //device temporal arrays
    uint64_t * a2e   = (uint64_t*)ip;
    uint32_t * e    = ip + 2*DPB*nTiles;
    uint32_t * c    = ip + 3*DPB*nTiles;
    uint32_t * p    = ip + 4*DPB*nTiles;
    uint32_t * a2r  = ip + 5*DPB*nTiles;
    uint32_t * a2   = ip + 6*DPB*nTiles;

    cudaError_t cudaResult;

    uint32_t nElements = nTiles*DPB;  // the transposition kernel makes sure of zeroing the unused matrices

    //create the list
    const int nBlocks = (nElements + BTNT-1)/BTNT;
    dim3 nTh2(32,32);
    A2Elist <<<nBlocks,nTh2,0,*stream>>> (a2e, p1,p2, nAtoms, nElements); // don't set the unused to -1; they will be taken care of by the transposition kernel
    //sort it (at1 > at2 > e)
    BTsort ((uint64_t*)a2e, nElements, stream);
    //split a2e in two uint32 lists
    SplitUI64 <<<nBlocks,BTNT,0,*stream>>> (a2, e, a2e, nElements);
    //count the occurrences of each atom pair int the sorted list, return the total number of atom pairs
    uint32_t nA2r;
    SortedHistogram (a2r, p, c, &nA2r,   a2, nElements,    stream,  (uint32_t*)a2e); //(uint64_t*)

    {
        // threads
        dim3 rgTh (DPB, S12);

        const int MAXB = 32*1024; // actually for <=2.x is almost twice as much

        int bby = (nTiles+MAXB-1) / MAXB;
        int bbx = (nTiles+bby-1)  / bby;
        dim3 tpBl (bbx, bby);

        TransposeFM <<<tpBl,rgTh, S12*DPB*sizeof(double),*stream>>> (dF, use, nTiles);

        cudaStreamSynchronize(*stream); // need nA2r from SortedHistogram2

        // find nbx,nby compatible with CUDA block dimension limitations (for compute capability up to 2.x)
        int nby = (nA2r+MAXB-1) / MAXB;
        int nbx = (nA2r+nby-1)  / nby;
        dim3 rgBl (nbx, nby);

        ReduceGatherFM2T            <<<rgBl,rgTh, S12*DPB*sizeof(double),*stream>>>
                                    ( vF, pF,  //
                                     S12, off12,   dF,
                                     a2r,   //  reduced  a2
                                     p,     // (reduced) pointer to e
                                     c,     // (reduced) number of elements of e
                                     e,     //  raw array of elements
                                     //will crash!!!
                                     &nA2r); //  number of A2
    }
}









// sort the elements within the block
__forceinline__ __device__ void xSort2 (uint32_t & ka, uint32_t & kb, uint16_t & va, uint16_t & vb) {
    if (ka>kb) {
        uint32_t kc = ka;
        ka = kb;
        kb = kc;
        uint16_t vc = va;
        va = vb;
        vb = vc;
    }
}

template <int L, bool I>
__forceinline__ __device__ void xBTStep  (int ni, uint32_t * keys, uint16_t * vals) {

    const int N = 1<<L; // resolved by the compiler

    //int nb = ni>>L; // number of algorithm block
    int nl = ni&(N-1); // number of lane

    //int base = 2*i - 2*nl; // nb<<(L+1);

    int i1 = (ni<<1) - nl; //base + nl;
    int i2;

    if (I) i2 = i1 + (2*N-1) - 2*nl; //base + (2*N-1) - nl;
    else   i2 = i1 + N;

    xSort2 (keys[i1],keys[i2], vals[i1],vals[i2]);

    //__syncthreads();
}

//block minus first step
template <int L> __forceinline__ __device__ void xBTBlock      (int ni, uint32_t * keys, uint16_t * vals) {
    xBTStep  <L,false> (ni, keys, vals);
    __syncthreads();
    xBTBlock <L-1>     (ni, keys, vals);
}
//end recursion
template <>      __forceinline__ __device__ void xBTBlock<0>   (int ni, uint32_t * keys, uint16_t * vals) {
    xBTStep  <0,false> (ni, keys, vals);
    __syncthreads();
}

//whole sorting network
template <int L> __forceinline__ __device__ void xBTNetwork    (int ni, uint32_t * keys, uint16_t * vals) {
    xBTNetwork <L-1>    (ni, keys, vals);
    xBTStep    <L,true> (ni, keys, vals);
    __syncthreads();
    xBTBlock   <L-1>    (ni, keys, vals);
}
//end recursion
template <>      __forceinline__ __device__ void xBTNetwork<0> (int ni, uint32_t * keys, uint16_t * vals) {
    xBTStep <0,false>   (ni, keys, vals);
    __syncthreads();
}


__forceinline__ __device__ void xSort1 (int i1, int i2, uint32_t * keys, uint16_t * vals) {
    xSort2 (keys[i1],keys[i2], vals[i1], vals[i2]);
    __syncthreads();
}


__forceinline__ __device__ void xBTNetwork10 (int ni, uint32_t * keys, uint16_t * vals) {

    // can't use warp-synchronous programming since sort2 is branch-divergent
    // can't use warp-synchronous programming anyway, since the CUDA compiler is a black-box optimizer which doesn't garantee warp-synchronicity

    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <1,true > (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <2,true > (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <3,true > (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <4,true > (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <5,true > (ni, keys, vals);    __syncthreads();
    xBTStep <4,false> (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <6,true > (ni, keys, vals);    __syncthreads();
    xBTStep <5,false> (ni, keys, vals);    __syncthreads();
    xBTStep <4,false> (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <7,true > (ni, keys, vals);    __syncthreads();
    xBTStep <6,false> (ni, keys, vals);    __syncthreads();
    xBTStep <5,false> (ni, keys, vals);    __syncthreads();
    xBTStep <4,false> (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <8,true > (ni, keys, vals);    __syncthreads();
    xBTStep <7,false> (ni, keys, vals);    __syncthreads();
    xBTStep <6,false> (ni, keys, vals);    __syncthreads();
    xBTStep <5,false> (ni, keys, vals);    __syncthreads();
    xBTStep <4,false> (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep <9,true > (ni, keys, vals);    __syncthreads();
    xBTStep <8,false> (ni, keys, vals);    __syncthreads();
    xBTStep <7,false> (ni, keys, vals);    __syncthreads();
    xBTStep <6,false> (ni, keys, vals);    __syncthreads();
    xBTStep <5,false> (ni, keys, vals);    __syncthreads();
    xBTStep <4,false> (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();


    xBTStep<10,true > (ni, keys, vals);    __syncthreads();
    xBTStep <9,false> (ni, keys, vals);    __syncthreads();
    xBTStep <8,false> (ni, keys, vals);    __syncthreads();
    xBTStep <7,false> (ni, keys, vals);    __syncthreads();
    xBTStep <6,false> (ni, keys, vals);    __syncthreads();
    xBTStep <5,false> (ni, keys, vals);    __syncthreads();
    xBTStep <4,false> (ni, keys, vals);    __syncthreads();
    xBTStep <3,false> (ni, keys, vals);    __syncthreads();
    xBTStep <2,false> (ni, keys, vals);    __syncthreads();
    xBTStep <1,false> (ni, keys, vals);    __syncthreads();
    xBTStep <0,false> (ni, keys, vals);    __syncthreads();

    /*
        xSort1 ( (i<<1)       , (i<<1)+1, keys, vals);
        xSort1 ( (i<<1)-(i&1) , (i<<1)-2*(i&1)  , keys, vals);
        xSort2 (keys[2*i-(i&1)],keys[2*i+1], vals[2*i],vals[2*i+1]);
    */
}



template <int B, int N>
__forceinline__ __device__ void xRadixSort1   (int i, uint32_t * keys, uint16_t * vals, uint16_t * scan) {

    __shared__ uint16_t psum;

    //__syncthreads();

    // count the 0s
    scan[  i] = 1 - ( (keys[  i]>>B) & 1 );
    scan[N+i] = 1 - ( (keys[N+i]>>B) & 1 );

    bool use0 = (scan[  i]==1);
    bool useN = (scan[N+i]==1);

    __syncthreads();


    //parallel reduce, divided in two blocks
    const int ii = 2*i+1;

/*
    #pragma unroll
    for (int s=2; s<=2*N; s*=2) {
        if (((ii)&(s-1))==(s-1)) scan[ii] += scan[ii - s/2];
        __syncthreads();
    }
    */
    #pragma unroll
    for (int s=N, t=1; s>=1; s<<=1, t>>=1) {
        if (i<s) scan[2*N-1] += scan[2*N-1 - t];
        //if (((ii)&(s-1))==(s-1)) scan[ii] += scan[ii - s/2];
        __syncthreads();
    }

    /*
    if ((ii&   1)==(   1)) scan[ii] += scan[ii -   1];    __syncthreads();
    if ((ii&   3)==(   3)) scan[ii] += scan[ii -   2];    __syncthreads();
    if ((ii&   7)==(   7)) scan[ii] += scan[ii -   4];    __syncthreads();
    if ((ii&  15)==(  15)) scan[ii] += scan[ii -   8];    __syncthreads();
    if (N>=  16) {if ((ii&  31)==(  31)) scan[ii] += scan[ii -  16];    __syncthreads();}
    if (N>=  32) {if ((ii&  63)==(  63)) scan[ii] += scan[ii -  32];    __syncthreads();}
    if (N>=  64) {if ((ii& 127)==( 127)) scan[ii] += scan[ii -  64];    __syncthreads();}
    if (N>= 128) {if ((ii& 255)==( 255)) scan[ii] += scan[ii - 128];    __syncthreads();}
    if (N>= 256) {if ((ii& 512)==( 512)) scan[ii] += scan[ii - 256];    __syncthreads();}
    if (N>= 512) {if ((ii&1023)==(1023)) scan[ii] += scan[ii - 512];    __syncthreads();}
    if (N>=1024) {if ((ii&2047)==(2047)) scan[ii] += scan[ii -1024];    __syncthreads();}
    */



    if (i==0) psum = scan[2*N-1]; // total number of 0s
    if (i==0) scan[2*N-1] = 0;
    __syncthreads();

    #pragma unroll
    for (int s=2*N; s>=2; s/=2) {
        if (((ii)&(s-1))==(s-1)) {
            uint16_t vi = scan[ii];
            uint16_t vs = scan[ii - s/2];

            scan[ii - s/2]  = vi;
            scan[ii]       += vs;
        }
        __syncthreads();
    }

    // as well as pointers, etc.
    uint16_t  p0;
    uint16_t  pN;

    if (use0) p0   = scan[  i];
    else      p0   = psum - scan[  i] +    i ;

    if (useN) pN   = scan[N+i];
    else      pN   = psum - scan[N+i] + (N+i);

    uint32_t  k0   = keys[  i];
    uint32_t  kN   = keys[N+i];

    uint16_t  v0   = vals[  i];
    uint16_t  vN   = vals[N+i];

    __syncthreads();

    keys[p0] = k0;
    vals[p0] = v0;

    keys[pN] = kN;
    vals[pN] = vN;

    __syncthreads();
}



// optimize !!!!
template <int N>
__forceinline__ __device__ uint16_t xreverse   (uint16_t j) {

    uint16_t r = 0;
    #pragma unroll
    for (uint16_t n=N/2; n>0; n>>=1) {
        r |= (j&1);
        j >>=1;
        r <<=1;
    }
    r |= (j&1);

    return 2*N-1-r;
}

template <int N>
__forceinline__ __device__ void xpermute    (uint16_t i, uint16_t ri, uint32_t * k, uint16_t * v) {

    uint32_t k0 = k[i];
    uint32_t kN = k[i+N];

    uint16_t v0 = v[i];
    uint16_t vN = v[i+N];

    __syncthreads();

    k[ri  ] = k0;
    k[ri-1] = kN;

    v[ri  ] = v0;
    v[ri-1] = vN;

    __syncthreads();
}

template <int N>
__forceinline__ __device__ void xpermuteT   (uint16_t i, uint16_t ri, uint32_t * k, uint16_t * v) {

    uint32_t k0 = k[ri];
    uint32_t kN = k[ri-1];

    uint16_t v0 = v[ri];
    uint16_t vN = v[ri-1];

    __syncthreads();

    k[i  ] = k0;
    k[i+N] = kN;

    v[i  ] = v0;
    v[i+N] = vN;

    __syncthreads();
}



template <int B, int N>
__forceinline__ __device__ void xRadixSort2 (uint16_t i, uint16_t ri, uint32_t * keys, uint16_t * vals, uint16_t * scan) {

    __shared__ uint16_t psum;


    // count the 0s
    scan[  i] = 1 - ( (keys[i  ]>>B) & 1 );
    scan[N+i] = 1 - ( (keys[i+N]>>B) & 1 );

    bool use0 = (scan[  i]==1);
    bool useN = (scan[N+i]==1);

    __syncthreads();

    // algorithm in neg bit rev order
    // ==============================

    #pragma unroll
    for (int s=N; s>=2; s/=2) {
        if (i<s) scan[i] += scan[i+s];
        __syncthreads();
    }

    if (i==0) psum = scan[0] + scan[1]; // total number of 0s
    if (i==0) scan[0] = scan[1];
    if (i==0) scan[1] = 0;
    __syncthreads();

    #pragma unroll
    for (int s=2; s<=N; s*=2) {
        if (i<s) {
            uint16_t vi = scan[i];
            scan[i]    += scan[i+s];
            scan[i+s]   = vi;
        }
        __syncthreads();
    }

    if (!use0) scan[ i  ] = psum - scan[ i]  +   ri;
    if (!useN) scan[ i+N] = psum - scan[i+N] + (ri-1);

    uint16_t  p0   = xreverse<N> (scan[i]);
    uint16_t  pN   = xreverse<N> (scan[i+N]);

    uint32_t  k0   = keys[i  ];
    uint32_t  kN   = keys[i+N];

    uint16_t  v0   = vals[i];
    uint16_t  vN   = vals[i+N];

    __syncthreads();

    keys[p0] = k0;
    keys[pN] = kN;

    vals[p0] = v0;
    vals[pN] = vN;

    __syncthreads();
}


template <int B, int N>
__forceinline__ __device__ void xRadixSort  (uint16_t i, uint32_t * keys, uint16_t * vals, uint16_t * scan) {

    if (B> 0) xRadixSort1 < 0,N> (i, keys, vals, scan);
    if (B> 1) xRadixSort1 < 1,N> (i, keys, vals, scan);
    if (B> 2) xRadixSort1 < 2,N> (i, keys, vals, scan);
    if (B> 3) xRadixSort1 < 3,N> (i, keys, vals, scan);
    if (B> 4) xRadixSort1 < 4,N> (i, keys, vals, scan);
    if (B> 5) xRadixSort1 < 5,N> (i, keys, vals, scan);
    if (B> 6) xRadixSort1 < 6,N> (i, keys, vals, scan);
    if (B> 7) xRadixSort1 < 7,N> (i, keys, vals, scan);
    if (B> 8) xRadixSort1 < 8,N> (i, keys, vals, scan);
    if (B> 9) xRadixSort1 < 9,N> (i, keys, vals, scan);
    if (B>10) xRadixSort1 <10,N> (i, keys, vals, scan);
    if (B>11) xRadixSort1 <11,N> (i, keys, vals, scan);
    if (B>12) xRadixSort1 <12,N> (i, keys, vals, scan);
    if (B>13) xRadixSort1 <13,N> (i, keys, vals, scan);
    if (B>14) xRadixSort1 <14,N> (i, keys, vals, scan);
    if (B>15) xRadixSort1 <15,N> (i, keys, vals, scan);
    if (B>16) xRadixSort1 <16,N> (i, keys, vals, scan);
    if (B>17) xRadixSort1 <17,N> (i, keys, vals, scan);
    if (B>18) xRadixSort1 <18,N> (i, keys, vals, scan);
    if (B>19) xRadixSort1 <19,N> (i, keys, vals, scan);

}

template <int B, int N>
__forceinline__ __device__ void xRadixSortT (uint16_t i, uint32_t * keys, uint16_t * vals, uint16_t * scan) {

    uint16_t ri = xreverse <N> (i);

    xpermute <N> (i, ri, keys, vals);

    if (B> 0) xRadixSort2 < 0,N> (i, ri, keys, vals, scan);
    if (B> 1) xRadixSort2 < 1,N> (i, ri, keys, vals, scan);
    if (B> 2) xRadixSort2 < 2,N> (i, ri, keys, vals, scan);
    if (B> 3) xRadixSort2 < 3,N> (i, ri, keys, vals, scan);
    if (B> 4) xRadixSort2 < 4,N> (i, ri, keys, vals, scan);
    if (B> 5) xRadixSort2 < 5,N> (i, ri, keys, vals, scan);
    if (B> 6) xRadixSort2 < 6,N> (i, ri, keys, vals, scan);
    if (B> 7) xRadixSort2 < 7,N> (i, ri, keys, vals, scan);
    if (B> 8) xRadixSort2 < 8,N> (i, ri, keys, vals, scan);
    if (B> 9) xRadixSort2 < 9,N> (i, ri, keys, vals, scan);
    if (B>10) xRadixSort2 <10,N> (i, ri, keys, vals, scan);
    if (B>11) xRadixSort2 <11,N> (i, ri, keys, vals, scan);
    if (B>12) xRadixSort2 <12,N> (i, ri, keys, vals, scan);
    if (B>13) xRadixSort2 <13,N> (i, ri, keys, vals, scan);
    if (B>14) xRadixSort2 <14,N> (i, ri, keys, vals, scan);
    if (B>15) xRadixSort2 <15,N> (i, ri, keys, vals, scan);
    if (B>16) xRadixSort2 <16,N> (i, ri, keys, vals, scan);
    if (B>17) xRadixSort2 <17,N> (i, ri, keys, vals, scan);
    if (B>18) xRadixSort2 <18,N> (i, ri, keys, vals, scan);
    if (B>19) xRadixSort2 <19,N> (i, ri, keys, vals, scan);

    xpermuteT <N> (i, ri, keys, vals);
}


static const uint64_t MASK64  = (-1);
static const uint64_t MASKU32 = MASK64 - MASK32;


// perform steps in shared memory when possible
template <int L>
__global__ void xReduce (uint32_t * na2, uint32_t * a2, uint32_t * p, uint32_t * c, uint32_t * e, uint64_t * a2p,   const int * p1, const int * p2, uint64_t nAtoms, uint64_t nElements) {

    const int N = 1<<L;

    __shared__ uint32_t keys[2*N]; //  8K; no problem
    __shared__ uint16_t vals[2*N]; //  4K; no problem

    __shared__ uint16_t scan[2*N]; //  4K; no problem


    const int i = threadIdx.x;
    const int o = blockIdx.x *2*N;

    keys [i] = keys [N+i] = -1; // maximum possible value
    vals [i] = vals [N+i] = -1; // maximum possible value


    //copy in
    if (  o+i<nElements) {
        uint32_t at1 = p1[i];
        uint32_t at2 = p2[i];
        keys [  i] = (at1*nAtoms + at2);
        vals [  i] = i;
    }
    if (N+o+i<nElements) {
        uint32_t at1 = p1[N+i];
        uint32_t at2 = p2[N+i];
        keys [N+i] = (at1*nAtoms + at2);
        vals [N+i] = N+i;
    }
    __syncthreads();


    //sort
    //xBTNetwork<L> (i, keys, vals);
    xRadixSortT<20,N> (i, keys, vals, scan);



    uint32_t an0  = keys[i];
    uint32_t am0 = -1;
    if (i>0) am0 = keys[i-1];
    bool use0 = ((an0!=-1) && (an0!=am0));

    uint32_t an1  = keys[N+i];
    uint32_t am1  = keys[N+i-1];
    bool use1 = ((an1!=-1) && (an1!=am1));


    if (  o+i<nElements) e[  o+i] = (uint32_t)vals[i];
    if (N+o+i<nElements) e[N+o+i] = (uint32_t)vals[N+i];

    //parallel prefix sum
    //*******************
    vals[i] = vals[N+i] = 0;

    if (use0) vals[  i] = 1;
    if (use1) vals[N+i] = 1;

    __syncthreads();



    // get the total prefix sum
    __shared__ uint16_t nA2r;

    //parallel reduce, divided in two blocks
    const int ii = 2*i+1;

    #pragma unroll
    for (int s=2; s<=2*N; s*=2) {
        if (((ii)&(s-1))==(s-1)) vals[ii] += vals[ii - s/2];
        __syncthreads();
    }

    if (i==0) nA2r = vals[2*N-1];
    if (i==0) vals[2*N-1] = 0;
    __syncthreads();

    #pragma unroll
    for (int s=2*N; s>=2; s/=2) {
        if (((ii)&(s-1))==(s-1)) {
            uint16_t vi = vals[ii];
            uint16_t vs = vals[ii - s/2];

            vals[ii - s/2]  = vi;
            vals[ii]       += vs;
        }
        __syncthreads();
    }



    // as well as pointers, etc.
    uint16_t  p0;
    uint16_t  pN;

    uint32_t a2r0;
    uint32_t a2rN;

    if (use0) {
        p0   = vals[  i];
        a2r0 = keys[  i];
    }
    if (use1) {
        pN   = vals[N+i];
        a2rN = keys[N+i];
    }

    keys[i] = keys[N+i] = -1;
    vals[i] = vals[N+i] = -1;

    __syncthreads();


    if (use0) {
        keys[p0] = a2r0; // atom2 id
        vals[p0] = i;    // pointer to the e array
    }
    if (use1) {
        keys[pN] = a2rN; // atom2 id
        vals[pN] = i+N;  // pointer to the e array
    }

    __syncthreads();



    // write everything else for the upper and lower halfs
    if (i<nA2r) {
        uint16_t pn  = vals [i];
        uint16_t ppn = vals [i+1];

        // this means the thread is both valid AND the next one isn't, so it's the last element
        if (i+1==nA2r) ppn  = nElements;

        // write element to memory
        a2 [i] = keys[i];
        p  [i] = pn;
        c  [i] = ppn-pn;
    }
    else if (  o+i<nElements) {
        a2 [  i] = -1;
        p  [  i] =  0;
        c  [  i] =  0;
    }

    if (N+i<nA2r) {
        uint16_t pn  = vals [N+i];
        uint16_t ppn;

        // if very last thread coincides with very last element OR next element is not valid
        if (N+i+1==nA2r) ppn  = nElements;
        else             ppn = vals [N+i+1];

        // write element to memory
        a2 [N+i] = keys[N+i];
        p  [N+i] = pn;
        c  [N+i] = ppn-pn;
    }
    else if (N+o+i<nElements){
        a2 [N+i] = -1;
        p  [N+i] =  0;
        c  [N+i] =  0;
    }

    if (i==0) na2[blockIdx.x] = nA2r;

/*
    if (use0) vals[  i] =   i;
    else      keys[  i] =  -1;

    if (use1) vals[N+i] = N+i;
    else      keys[N+i] =  -1;
    __syncthreads();


    xBTNetwork9 (i, keys, vals);
    */

}


// (unoptimized) parallel prefix scan for 2048 elements, 1024 threads
template <int N>
__device__ void PrefixScan (int i, uint32_t * vals, uint32_t & nv) {

    const int ii = 2*i+1;

    #pragma unroll
    for (int s=2; s<=2*N; s*=2) {
        if (((ii)&(s-1))==(s-1)) vals[ii] += vals[ii - s/2];
        __syncthreads();
    }

    if (i==0) nv = vals[2*N-1];
    if (i==0) vals[2*N-1] = 0;
    __syncthreads();

    #pragma unroll
    for (int s=2*N; s>=2; s/=2) {
        if (((ii)&(s-1))==(s-1)) {
            uint16_t vi = vals[ii];
            uint16_t vs = vals[ii - s/2];

            vals[ii - s/2]  = vi;
            vals[ii]       += vs;
        }
        __syncthreads();
    }
}

// fast function for hashing
__forceinline__ __device__ uint16_t xHash (uint32_t at1, uint32_t at2, uint32_t nAtoms) {
    const uint32_t M = 2039;
    //hash
    return ((at1+1)*(nAtoms+at2) ) % M;
}

// another fast function for hashing
__forceinline__ __device__ uint16_t xHash10b (uint32_t at1, uint32_t at2, uint32_t nAtoms) {
    const uint32_t M = 1021;
    //hash
    return ((at1+1)*(nAtoms+at2) ) % M;
}


__global__ void xReduce2 (uint32_t * nA2, uint32_t * a2, uint32_t * p, uint32_t * c, uint32_t * e,    const int * p1, const int * p2, int nAtoms, int nElements) {

    const int N = 1024;

    const int i = threadIdx.x;
    const int o = blockIdx.x * blockDim.x;

    // length of input to consider
    const int NE = min (2*N, nElements-o);

    __shared__ uint32_t nhash   [2*N]; // 8K
    __shared__ uint16_t  hashes [2*N]; // 4K
    __shared__ uint32_t ne, na2;


    nhash[  i] = 0;
    nhash[N+i] = 0;

    __syncthreads();

    // read input and save hashes
    // ==========================
    if (  i<NE) {
        uint32_t at1 = p1[o+i];
        uint32_t at2 = p2[o+i];

        hashes[i]   = xHash (at1,at2, nAtoms);
    }
    if (N+i<NE) {
        uint32_t at1 = p1[N+o+i];
        uint32_t at2 = p2[N+o+i];

        hashes[N+i] = xHash (at1,at2, nAtoms);
    }
    __syncthreads();


    // count occurrences
    // =================

    if (  i<NE)  atomicAdd(&nhash[hashes[  i]], 1);
    if (N+i<NE)  atomicAdd(&nhash[hashes[N+i]], 1);

    __syncthreads();

    // compute indices
    // ===============

    // i and i+N iterate now over the hash id's

    // save number of collisions
    uint32_t c0 = nhash[i];
    uint32_t cN = nhash[N+i];

    // integrate index to element list
    PrefixScan <1024> (i, nhash, ne); // ne == NE

    // save indexes
    uint32_t p0 = nhash[i];
    uint32_t pN = nhash[N+i];

    // count the number of groups and compute indexes
    if (c0>0) nhash[i] = 1;
    else      nhash[i] = 0;
    if (cN>0) nhash[N+i] = 1;
    else      nhash[N+i] = 0;

    PrefixScan <1024> (i, nhash, na2); // na2: number of different groups

    uint32_t pp0 = nhash[i];
    uint32_t ppN = nhash[N+i];

    __syncthreads();

    // write to arrays
    if (c0) {
        a2 [pp0] = i;  // i is the hash value
        c  [pp0] = c0; // number of occurrences
        p  [pp0] = p0; // position to e
    }
    if (cN) {
        a2 [ppN] = N+i; //
        c  [ppN] = cN;  // number of occurrences
        p  [ppN] = pN;  // position to e
    }

    // save the positions
    nhash[  i] = p0;
    nhash[N+i] = pN;

    __syncthreads();

    // zero everything not used
    if ( (na2<=i)&&(i<ne) ) {
        a2 [i] = -1;
        c  [i] =  0;
        p  [i] =  0;
    }
    if ( (na2<=N+i)&&(N+i<ne) ) {
        a2[N+i] = -1;
        c [N+i] =  0;
        p [N+i] =  0;
    }

    // now add the elements where they belong
    // ======================================

    if (  i<NE) {
        uint16_t h = hashes[i];
        e[nhash[h]] = o+i; // write global index to corresponding sublist
        atomicAdd(&nhash[h], 1);
    }
    if (N+i<NE) {
        uint16_t h = hashes[N+i];
        e[nhash[h]] = o+N+i; // write global index to corresponding sublist
        atomicAdd(&nhash[h], 1);
    }

    // save the number of atoms
    nA2[blockIdx.x] = na2;

}


/*


__global__ void xReduceS1 (uint32_t * nA2, uint32_t * a2, uint32_t * p, uint32_t * c, uint32_t * e,    const int * p1, const int * p2, int nAtoms, int nElements) {

    const int N = 1024;

    const int i = threadIdx.x;
    const int o = blockIdx.x * blockDim.x;

    if (o>=nElements) return;

    // length of input to consider
    const int NE = min (N, nElements-o);


    __shared__ uint16_t nhash   [1024]; // 4K

    nhash[  i] = 0;

    __syncthreads();

    // read input and increment counter
    // ================================
    if (i<NE) {
        uint32_t at1 = p1[o+i];
        uint32_t at2 = p2[o+i];

        uint16_t h = xHash10b (at1,at2, nAtoms);
        atomicAdd(&nhash[h], 1); // shouldn't incur in many collisions if the hash is good and values are different
    }
    __syncthreads();


    int NB = gridDim.x; // number of blocks
    int ib = blockIdx.x; // id of this block


    c [i*NB + ib] = nhash[i]; // write the number of instances of each hash in current block
    p [i*NB + ib] = nhash[i]; // write it to the pointer list, too
}

// need synchronization

__global__ void xReduceS2 (uint32_t * s, uint32_t * p, int nP) {

    const int N = 1024;

    const int i = threadIdx.x;
    const int o = blockIdx.x * blockDim.x;

    if (o>=nP) return;

    __shared__ uint32_t list[2048]; // 8K
    __shared__ uint32_t suml;

    list[i] = list[N+i] = 0;

    // input data
    if (o+  i<nP) list[  i] = p[o  +i];
    if (o+N+i<nP) list[N+i] = p[o+N+i];

    __syncthreads();

    // prefix sum the number of elements
    PrefixScan <N> (i, list, suml);

    // output everything
    if (o+  i<nP) p[o  +i] = list[  i];
    if (o+N+i<nP) p[o+N+i] = list[N+i];

    s [blockIdx.x] = suml;
}

// do last routine iteratively; now fix upward

__global__ void xReduceS3 (uint32_t * p, const uint32_t * s, int nP) {

    const int N = 1024;

    const int i = threadIdx.x;
    const int o = blockIdx.x * blockDim.x;

    if (o>=nP) return;

    __shared__ uint32_t suml;

    if (i==0) suml = s[blockIdx.x];
    __syncthreads();

    // update data
    if (o+  i<nP) p[  i] += suml;
    if (o+N+i<nP) p[N+i] += suml;
}

// now every block of data knows where to write the elements

__global__ void xReduceS4 (uint32_t * nA2, uint32_t * a2, uint32_t * p, uint32_t * c, uint32_t * e,    const int * p1, const int * p2, int nAtoms, int nElements) {

    const int N = 1024;

    const int i = threadIdx.x;
    const int o = blockIdx.x * blockDim.x;

    if (o>=nElements) return;

    // length of input to consider
    const int NE = min (N, nElements-o);

    __shared__ uint32_t pos [1024]; // 8K

    pos[i] = p[i*NB + ib]; // get the (summed) positions where to write the elements

    __syncthreads();

    // hash input, write element
    // =========================
    if (i<NE) {
        uint32_t at1 = p1[o+i];
        uint32_t at2 = p2[o+i];

        uint16_t h = xHash10b (at1,at2, nAtoms);
        uint32_t pp = atomicAdd(&pos[h], 1); // shouldn't incur in many collisions if the hash is good and values are different

        // this is a mess of unaligned accesses
        e  [pp] = i;
        a2 [pp] = at1*nAtoms + at2;
    }

}

*/


