
#include "ERIbatch.hpp"
#include "ERIblocks.hpp"

#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"

#include "math/angular.hpp"
using namespace LibAngular;

#include "libechidna/libechidna.hpp"
#include "contraction/contractions.hpp"

#include "libquimera/ERIgeom.hpp"

#ifdef _OPENMP
    #include <omp.h>
#endif


/* BlockBuffer's static variables */
/* ============================== */

size_t    BlockBufferGPU::TotalMaxBlockMem = 0;
size_t    BlockBufferGPU::LocalMaxBlockMem = 0;
uint8_t * BlockBufferGPU::TotalBlockMem = NULL;

size_t    BlockBufferGPU::TotalMaxBlockMemGPU [MAX_GPUS] = {0,0,0,0};
size_t    BlockBufferGPU::LocalMaxBlockMemGPU [MAX_GPUS] = {0,0,0,0};
uint8_t * BlockBufferGPU::TotalBlockMemGPU    [MAX_GPUS] = {NULL,NULL,NULL,NULL};

int       BlockBufferGPU::NumGPUs = 0;

/* ============================== */

size_t    BlockBufferCPU::TotalMaxBlockMem = 0;
size_t    BlockBufferCPU::LocalMaxBlockMem = 0;
uint8_t * BlockBufferCPU::TotalBlockMem = NULL;

/* ============================== */



void BlockBufferCPU::InitMem (size_t MEM, BatchEvaluator * BE, int Nprox) {

    // do not initialize twice
    if (TotalBlockMem==NULL)
    {

        const int PAGE = 4096; //round mem to page boundaries / bank

        // INIT MEM POOL
        // =============

        TotalMaxBlockMem = MEM;

        uint32_t error;

        //aligning to cache line size avoids different cores fighting for the same cache line
        while (
            posix_memalign((void**)(&TotalBlockMem), 4096, TotalMaxBlockMem)
        ) TotalMaxBlockMem/=2;

      // split mem between all processes
        LocalMaxBlockMem    = TotalMaxBlockMem    / Nprox;
        LocalMaxBlockMem    = (LocalMaxBlockMem   /PAGE) * PAGE;
    }

    for (int n=0; n<Nprox; ++n) {
        BE[n].b4blockCPU.MaxBlockMem    = LocalMaxBlockMem; //set the constant mems
        BE[n].b4blockCPU.BlockMem       = TotalBlockMem    + n*LocalMaxBlockMem; //set the pointers
    }


}

void BlockBufferCPU::FreeMem () {
    // do not delete twice
    if (TotalBlockMem!= NULL) free (TotalBlockMem);
    TotalMaxBlockMem = 0;
    LocalMaxBlockMem = 0;
    TotalBlockMem = NULL;
}



void BatchEvaluator::InitMem (size_t MEM, BatchEvaluator * BE, int Nprox) {

    BlockBufferCPU::InitMem (MEM, BE, Nprox);
}

void BatchEvaluator::InitMem (size_t MEM, size_t MEMGPU, BatchEvaluator * BE, int Nprox) {

    BlockBufferCPU::InitMem (MEM, BE, Nprox);
    #ifdef USE_GPU
    BlockBufferGPU::InitMem (MEMGPU, BE, Nprox);
    #endif
}

void BatchEvaluator::FreeMem () {

    BlockBufferCPU::FreeMem ();
    #ifdef USE_GPU
    BlockBufferGPU::FreeMem ();
    #endif
}

#ifndef USE_GPU
BatchEvaluator::BatchEvaluator()  {}
BatchEvaluator::~BatchEvaluator() {}
#endif





const int DPC = DOUBLES_PER_CACHE_LINE;
const int FPC = FLOATS_PER_CACHE_LINE;
const int DPB = DOUBLES_PER_BLOCK;


static inline uint8_t * align2cacheline64(uint8_t * p) {
    uintptr_t addr = (uintptr_t)(p); //convert to integer
    if (addr % CACHE_LINE_SIZE != 0) addr += CACHE_LINE_SIZE - addr % CACHE_LINE_SIZE; //align to next address multiple of 64
    return (uint8_t*)addr; //cast back to pointer
}

static inline size_t align2cacheline64(size_t offset) {
    if (offset % CACHE_LINE_SIZE != 0)
        offset += CACHE_LINE_SIZE - offset % CACHE_LINE_SIZE; //align to next address multiple of 64
    return offset;
}

// TRY TO MAKE ALL THIS LOCK-FREE


ERIbatchBuffer::ERIbatchBuffer() {
    //TheTiles = NULL;
    //MaxTiles = 0;
    mem = NULL;
    count   = 0;
    UsedMem  = 0;
    #ifdef _OPENMP
    omp_init_lock (&Lock);
    #endif
}

void ERIbatchBuffer::Init(size_t MEM) {
    TotalMem = MEM;
    uint32_t error;

    do {
        //aligning to cache line size avoids different cores fighting for the same cache line
        error = posix_memalign((void**)(&mem), 4096, TotalMem);

        if (error) TotalMem/=2;
    } while (error!=0);

    memFree[0] = TotalMem;
}

ERIbatchBuffer::~ERIbatchBuffer() {

    if (mem!=NULL) free(mem);

    #ifdef _OPENMP
    omp_destroy_lock (&Lock);
    #endif
}

void * ERIbatchBuffer::Allocate (uint64_t & Size) {
    std::map<uint64_t, uint64_t>::iterator it;

    uint8_t * ret;

    #ifdef _OPENMP
    omp_set_lock (&Lock);
    #endif
    {
        // best second choice
        std::map<uint64_t, uint64_t>::iterator itc = memFree.begin();

        for (it=memFree.begin(); it!=memFree.end(); ++it) {
            if (it->second>=Size) break;                  //found some memory
            else if (it->second > itc->second) itc = it;  //found a better second choice
        }

        //couldn't find the memory requested
        if (it==memFree.end()) {
            //no memory at all, actually
            if (itc==memFree.end())
                ret = NULL;
            else {
                //return as many as found
                Size = itc->second;

                uint64_t offset = itc->first;
                memFree.erase(itc);

                ret = mem + offset;
                ++count;
                UsedMem += Size;
            }
        }
        //found enough memory
        else {
            uint64_t offset = it->first;
            uint64_t tsize    = it->second;
            uint64_t left = tsize - Size;
            memFree.erase(it);

            if (left>0) memFree[offset+Size] = left; //memory left

            ret = mem + offset;
            ++count;
            UsedMem += Size;
        }
    }
    #ifdef _OPENMP
    omp_unset_lock (&Lock);
    #endif


    return (void*)ret;
}

void ERIbatchBuffer::ReAllocate(void * List, uint64_t OSize, uint64_t NSize) {

    if      (OSize==NSize) return; //do nothing
    else if (OSize<NSize) {
        std::cout << "Error: trying to realloc with a larger amount of memory; " << NSize << " > " << OSize << endl;
        throw(522);
    }

    uint64_t offset0  = (uint8_t*)List - mem;
    uint64_t offsetF  = offset0 + NSize;
    uint64_t FSize    = OSize - NSize;


    #ifdef _OPENMP
    omp_set_lock (&Lock);
    #endif
    {
        std::map<uint64_t, uint64_t>::iterator it, itn;

        //insert the freed memory
        memFree[offsetF] = FSize;

        //merge with next if possible
        {
            it = memFree.find(offsetF);
            itn = it;
            ++itn;
            if ( itn!=memFree.end() &&  offsetF + FSize == itn->first ) {
                //merge the memory
                it->second += itn->second;
                //delete next
                memFree.erase(itn);
            }
        }

        //--count;
        UsedMem -= FSize;
    }
    #ifdef _OPENMP
    omp_unset_lock (&Lock);
    #endif

}

void ERIbatchBuffer::Free(void * List, uint64_t Size) {
    uint64_t offset  = (uint8_t*)List - mem;

    #ifdef _OPENMP
    omp_set_lock (&Lock);
    #endif
    {
        std::map<uint64_t, uint64_t>::iterator it, itn;

        //insert the freed memory
        memFree[offset] = Size;

        //merge with next if possible
        {
            it = memFree.find(offset);
            itn = it;
            ++itn;
            if ( itn!=memFree.end() &&  offset + Size == itn->first ) {
                //merge the memory
                it->second += itn->second;
                //delete next
                memFree.erase(itn);
            }
        }

         //marge with previous, if possible
        {
            it = memFree.find(offset);

            if ( it!=memFree.begin() ) {
                itn = it;
                --it;
                if ( it->first + it->second == offset ) {
                    //merge the memory
                    it->second += itn->second;
                    //delete next
                    memFree.erase(itn);
                }
            }
        }

        --count;
        UsedMem -= Size;
    }
    #ifdef _OPENMP
    omp_unset_lock (&Lock);
    #endif

}

void ERIbatchBuffer::Check()  {
    std::map<uint64_t, uint64_t>::const_iterator it;

    if (count>0 || UsedMem>0) {
        Echidna::EDebugger << "ERI batch buffer was not completely freed after computation: " << std::endl;
        Echidna::EDebugger << "Imbalance: " << count << std::endl;
        Echidna::EDebugger << "Lost mem : " << UsedMem << std::endl;

        for (it=memFree.begin(); it!=memFree.end(); ++it) {
            Echidna::EDebugger << it->first << " " << it->second << std::endl;
        }

        Echidna::EDebugger << "Erasing" << std::endl;
        count = 0;
        UsedMem = 0;
        memFree.clear();
        memFree[0] = TotalMem;
    }
}





void BlockBufferCPU::SetBuffers(const ERIBatch & TheBatch,  LibQuimera::ERIbuffer & ERIbuff, int NMs) {

    //compute and initialize the (fixed) amount of buffer memory required for contraction and evaluation
    size_t ERIoffset = ERIbuff.SetBuffers((void*)BlockMem, sizeof(cacheline), *TheBatch.ABp, *TheBatch.CDp,  *TheBatch.ERIalgorithm);


    uint32_t NTiles64 = TheBatch.Ntiles64;
    uint32_t NTiles32 = TheBatch.Ntiles32;

    uint32_t Ntiles = max(NTiles32, NTiles64);

    //uint32_t NInts  = NTiles64 * DPC + NTiles32 * FPC;
    //if (NTiles32==0) NTiles32 = NTiles64;


    //distribute memory accordingly, align to cache line
    uint8_t * p = BlockMem + ERIoffset;

    E0d = (ERIgeometry*)p; p += NTiles64 * DPC * sizeof(ERIgeometry);
    E0s = (ERIgeometry*)p; p += NTiles32 * FPC * sizeof(ERIgeometry);

    E8d = (ERIgeometries64*)p; p += NTiles64 * sizeof(ERIgeometries64);
    E8s = (ERIgeometries32*)p; p += NTiles32 * sizeof(ERIgeometries32);

    I8d = (cacheline64*)p; p += NTiles64 * TheBatch.msize4 * sizeof(cacheline64);
    I8s = (cacheline32*)p; p += NTiles32 * TheBatch.msize4 * sizeof(cacheline32);

    T8d = (cacheline64*)p; p += NTiles64 * TheBatch.wsize4 * sizeof(cacheline64);
    T8s = (cacheline32*)p; p += NTiles32 * TheBatch.wsize4 * sizeof(cacheline32);


    RM8d = (RotationMatrices64*)p; p += NTiles64 * sizeof(RotationMatrices64);
    RM8s = (RotationMatrices32*)p; p += NTiles32 * sizeof(RotationMatrices32);


    // HAVE TO RETHINK THIS, INCLUDING THE NUMBER OF MATRICES
    // CAN PROBABLY BE SPLIT CONTRACTIONS AND ALL POINT TO THE SAME MEM REFERENCE
    // USE ROUND-ROBIN LOCK ACQUISITION (I.E. ACQUIRE THE FIRST AVAILBALE LOCK AND PERFORM THE UPDATE; REPEAT)

    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);

    int JJLL = max(max(JLA,JLB),max(JLC,JLD));

    Dab = Dcd = Dac = Dad = Dbc = Dbd = (cacheline64*)p; p += JJLL*JJLL * sizeof(cacheline64) * NMs;
    Fab = Fcd = Fac = Fad = Fbc = Fbd = (cacheline64*)p; p += JJLL*JJLL * sizeof(cacheline64) * NMs;

    FFF  = (double*)p;      p += JJLL*JJLL * sizeof(double) * NMs * Ntiles;


    if (p > BlockMem + MaxBlockMem) {
        //Echidna::EMessenger << "Error in ERI block memory allocation: final offset is larger than available memory ";
        //Echidna::EMessenger << (p-BlockMem) << "  " << MaxBlockMem << std::endl;
        //Echidna::EMessenger << ERIoffset << std::endl;

        std::cout << " :: 111111" << endl;
        std::cout << "SetBuffers:: Error in ERI block memory allocation: final offset is larger than available memory ";
        std::cout << (p-BlockMem) << "  " << MaxBlockMem << std::endl;
        std::cout << ERIoffset << std::endl;

        throw(523);
    }
}

void BlockBufferCPU::SetBuffers64(const ERIBatch & TheBatch,  LibQuimera::ERIbuffer & ERIbuff, int NMs) {

    //compute and initialize the (fixed) amount of buffer memory required for contraction and evaluation
    size_t ERIoffset = ERIbuff.SetBuffers((void*)BlockMem, sizeof(cacheline), *TheBatch.ABp, *TheBatch.CDp,  *TheBatch.ERIalgorithm);


    uint32_t NTiles64 = TheBatch.Ntiles64;

    //distribute memory accordingly, align to cache line
    uint8_t * p = BlockMem + ERIoffset;

    E0  = (ERIgeometry*)p; p += NTiles64 * DPC * sizeof(ERIgeometry);
    E8  = (void*)p;        p += NTiles64 * sizeof(ERIgeometries64);
    I8  = (void*)p;        p += NTiles64 * TheBatch.msize4 * sizeof(cacheline64);
    T8  = (void*)p;        p += NTiles64 * TheBatch.wsize4 * sizeof(cacheline64);
    RM8 = (void*)p;        p += NTiles64 * sizeof(RotationMatrices64);


    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);

    int JJLL = max(max(JLA,JLB),max(JLC,JLD));

    dab = dcd = dac = dad = dbc = dbd = (cacheline64*)p; p += JJLL*JJLL * sizeof(cacheline64) * NMs;
    fab = fcd = fac = fad = fbc = fbd = (cacheline64*)p; p += JJLL*JJLL * sizeof(cacheline64) * NMs;

    FFF  = (double*)p;      p += JJLL*JJLL * sizeof(double) * NMs * NTiles64;


    if (p > BlockMem + MaxBlockMem) {
        std::cout << "SetBuffers64:: Error in ERI block memory allocation: final offset is larger than available memory ";
        std::cout << (p-BlockMem) << "  " << MaxBlockMem << std::endl;
        std::cout << ERIoffset << std::endl;

        throw(523);
    }
}

void BlockBufferCPU::SetBuffers32(const ERIBatch & TheBatch,  LibQuimera::ERIbuffer & ERIbuff, int NMs) {

    //compute and initialize the (fixed) amount of buffer memory required for contraction and evaluation
    size_t ERIoffset = ERIbuff.SetBuffers((void*)BlockMem, sizeof(cacheline), *TheBatch.ABp, *TheBatch.CDp,  *TheBatch.ERIalgorithm);


    uint32_t NTiles32 = TheBatch.Ntiles32;

    //distribute memory accordingly, align to cache line
    uint8_t * p = BlockMem + ERIoffset;

    E0  = (ERIgeometry*)p; p += NTiles32 * FPC * sizeof(ERIgeometry);
    E8  = (void*)p;        p += NTiles32 * sizeof(ERIgeometries32);
    I8  = (void*)p;        p += NTiles32 * TheBatch.msize4 * sizeof(cacheline32);
    T8  = (void*)p;        p += NTiles32 * TheBatch.wsize4 * sizeof(cacheline32);
    RM8 = (void*)p;        p += NTiles32 * sizeof(RotationMatrices32);


    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);

    int JJLL = max(max(JLA,JLB),max(JLC,JLD));

    dab = dcd = dac = dad = dbc = dbd = (cacheline32*)p; p += JJLL*JJLL * sizeof(cacheline32) * NMs;
    fab = fcd = fac = fad = fbc = fbd = (cacheline32*)p; p += JJLL*JJLL * sizeof(cacheline32) * NMs;

    FFF  = (double*)p;      p += JJLL*JJLL * sizeof(double) * NMs * NTiles32;


    if (p > BlockMem + MaxBlockMem) {
        std::cout << "SetBuffers32:: Error in ERI block memory allocation: final offset is larger than available memory ";
        std::cout << (p-BlockMem) << "  " << MaxBlockMem << std::endl;
        std::cout << ERIoffset << std::endl;

        throw(523);
    }
}

uint64_t BlockBufferCPU::MaxTiles64(const BatchInfo & TheBatch, int NMs) {

    //compute and initialize the (fixed) amount of buffer memory required for contraction and evaluation
    size_t ConstSize = TheBatch.ERIalgorithm->MemSize(*TheBatch.ABp, *TheBatch.CDp);


    size_t Tsize = 0;

    Tsize += DPC * sizeof(ERIgeometry);
    Tsize += sizeof(ERIgeometries64);
    Tsize += TheBatch.msize4 * sizeof(cacheline64);
    Tsize += TheBatch.wsize4 * sizeof(cacheline64);
    Tsize += sizeof(RotationMatrices64);


    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);

    int JJLL = max(max(JLA,JLB),max(JLC,JLD));

    size_t Msize = JJLL*JJLL*sizeof(cacheline64);

    ConstSize += 2 * NMs * Msize;

    Tsize += NMs * JJLL*JJLL * sizeof(double) * 8;

    //now compute how many tiles can be used
    if (ConstSize>LocalMaxBlockMem)
        return 0;
    else
        return (LocalMaxBlockMem - ConstSize) / Tsize;
}

uint64_t BlockBufferCPU::MaxTiles32(const BatchInfo & TheBatch, int NMs) {

    //compute and initialize the (fixed) amount of buffer memory required for contraction and evaluation
    size_t ConstSize = TheBatch.ERIalgorithm->MemSize(*TheBatch.ABp, *TheBatch.CDp);


    size_t Tsize = 0;

    Tsize += FPC * sizeof(ERIgeometry);
    Tsize += sizeof(ERIgeometries32);
    Tsize += TheBatch.msize4 * sizeof(cacheline32);
    Tsize += TheBatch.wsize4 * sizeof(cacheline32);
    Tsize += sizeof(RotationMatrices32);


    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);

    int JJLL = max(max(JLA,JLB),max(JLC,JLD));

    size_t Msize = JJLL*JJLL*sizeof(cacheline32);

    ConstSize += 2 * NMs * Msize;

    Tsize += NMs * JJLL*JJLL * sizeof(double) * 8;

    //now compute how many tiles can be used
    if (ConstSize>LocalMaxBlockMem)
        return 0;
    else
        return (LocalMaxBlockMem - ConstSize) / Tsize;
}

void BlockBufferGPU::SetBuffersN (const ERIBatch & TheBatch,  LibQuimera::ERIbuffer & ERIbuff) {

    //compute and initialize the (fixed) amount of buffer memory required for contraction and evaluation
    //size_t ERIoffset = ERIbuff.SetBuffers((void*)BlockMem, sizeof(cachelineN<DPB>), *TheBatch.ABp, *TheBatch.CDp,  *TheBatch.ERIalgorithm);


    uint32_t NtilesN = TheBatch.NtilesN;

    int nKab = TheBatch.ABp->Ka*TheBatch.ABp->Kb;
    int nKcd = TheBatch.CDp->Ka*TheBatch.CDp->Kb;

    int nK4 = nKab*nKcd;

    //coinciding centers have all exponentials weights equal to 1
    if (TheBatch.geometry==AACD) nKab = 0;
    if (TheBatch.geometry==AACC) nKab = nKcd = 0;

    bool hasP = ( (TheBatch.la==1)||(TheBatch.lb==1)||(TheBatch.lc==1)||(TheBatch.ld==1) );
    bool hasD = ( (TheBatch.la==2)||(TheBatch.lb==2)||(TheBatch.lc==2)||(TheBatch.ld==2) );
    bool hasF = ( (TheBatch.la==3)||(TheBatch.lb==3)||(TheBatch.lc==3)||(TheBatch.ld==3) );
    bool hasG = ( (TheBatch.la==4)||(TheBatch.lb==4)||(TheBatch.lc==4)||(TheBatch.ld==4) );
    bool hasH = ( (TheBatch.la==5)||(TheBatch.lb==5)||(TheBatch.lc==5)||(TheBatch.ld==5) );

    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);



    //distribute memory accordingly, align to cache line
    uint8_t * p = BlockMem; // + ERIoffset;

    AtA = (int*)p;         p += NtilesN * DPB * sizeof(int);
    AtB = (int*)p;         p += NtilesN * DPB * sizeof(int);
    AtC = (int*)p;         p += NtilesN * DPB * sizeof(int);
    AtD = (int*)p;         p += NtilesN * DPB * sizeof(int);

    Juse = (uint32_t*)p;   p += NtilesN * sizeof(int);
    Xuse = (uint32_t*)p;   p += NtilesN * sizeof(int);

    p += sizeof(cachelineN<DPB>) - (2*NtilesN*sizeof(int)) % sizeof(cachelineN<DPB>); // align to cacheline size

    IntBuff = (int*)p;     p += (NtilesN+1)*48*DPB * sizeof(int);

/*
    E0  = (ERIgeometry*)p; p += NtilesN * DPB * sizeof(ERIgeometry);
    E8  = (void*)p;        p += NtilesN * sizeof(ERIgeometriesN<DPB>);

    //AB8 = (void*)p;        p += NtilesN * nKab            * sizeof(cachelineN<DPB>);
    //CD8 = (void*)p;        p += NtilesN * nKcd            * sizeof(cachelineN<DPB>);

    I8  = (void*)p;        p += NtilesN * TheBatch.msize4 * sizeof(cachelineN<DPB>);
    T8  = (void*)p;        p += NtilesN * TheBatch.wsize4 * sizeof(cachelineN<DPB>);
    //RM8 = (void*)p;        p += NtilesN * sizeof(RotationMatricesN);
    RMP8 = (void*)p; if (hasP) p += NtilesN * sizeof(RotationMatrixN<1>);
    RMD8 = (void*)p; if (hasD) p += NtilesN * sizeof(RotationMatrixN<2>);
    RMF8 = (void*)p; if (hasF) p += NtilesN * sizeof(RotationMatrixN<3>);
    RMG8 = (void*)p; if (hasG) p += NtilesN * sizeof(RotationMatrixN<4>);
    //RMH8 = (void*)p; if (hasH) p += NtilesN * sizeof(RotationMatrixN<5>);

    dab  = (void*)p; p += NtilesN * JLA*JLB * sizeof(cachelineN<DPB>);
    dcd  = (void*)p; p += NtilesN * JLC*JLD * sizeof(cachelineN<DPB>);

    dac  = (void*)p; p += NtilesN * JLA*JLC * sizeof(cachelineN<DPB>);
    dad  = (void*)p; p += NtilesN * JLA*JLD * sizeof(cachelineN<DPB>);
    dbc  = (void*)p; p += NtilesN * JLB*JLC * sizeof(cachelineN<DPB>);
    dbd  = (void*)p; p += NtilesN * JLB*JLD * sizeof(cachelineN<DPB>);


    fab  = (void*)p; p += NtilesN * JLA*JLB * sizeof(cachelineN<DPB>);
    fcd  = (void*)p; p += NtilesN * JLC*JLD * sizeof(cachelineN<DPB>);

    fac  = (void*)p; p += NtilesN * JLA*JLC * sizeof(cachelineN<DPB>);
    fad  = (void*)p; p += NtilesN * JLA*JLD * sizeof(cachelineN<DPB>);
    fbc  = (void*)p; p += NtilesN * JLB*JLC * sizeof(cachelineN<DPB>);
    fbd  = (void*)p; p += NtilesN * JLB*JLD * sizeof(cachelineN<DPB>);
*/

    if (p > BlockMem + MaxBlockMem) {
        std::cout << " :: 2222222" << endl;
        std::cout << "SetBuffersN:: Error in ERI block host memory allocation: final offset is larger than available memory ";
        std::cout << (p-BlockMem) << "  " << MaxBlockMem << std::endl;
        //std::cout << ERIoffset << std::endl;

        throw(523);
    }


    // device memory
    // =============

    p = BlockMemGPU; // + ERIoffset;

    dAtA = (int*)p;        p += NtilesN * DPB * sizeof(int);
    dAtB = (int*)p;        p += NtilesN * DPB * sizeof(int);
    dAtC = (int*)p;        p += NtilesN * DPB * sizeof(int);
    dAtD = (int*)p;        p += NtilesN * DPB * sizeof(int);

    dJuse = (uint32_t*)p;   p += NtilesN * sizeof(int);
    dXuse = (uint32_t*)p;   p += NtilesN * sizeof(int);

    p += sizeof(cachelineN<DPB>) - (2*NtilesN*sizeof(int)) % sizeof(cachelineN<DPB>); // align to cacheline size

    dIntBuff = (int*)p;    p += (NtilesN+1) * 48 * DPB * sizeof(int); // device buffer for multiple int lists



    dE8  = (double*)p;        p += NtilesN * 6               * sizeof(cachelineN<DPB>);
    //dAB8 = (double*)p;        p += NtilesN *   nKab          * sizeof(cachelineN<DPB>);
    //dCD8 = (double*)p;        p += NtilesN *   nKcd          * sizeof(cachelineN<DPB>);
    //dG8  = (double*)p;        p += NtilesN * 2*nK4           * sizeof(cachelineN<DPB>);
    int n4 = ((2*nK4 + DPB-1)/DPB)*DPB;
    dG8  = (double*)p;        p += n4 * sizeof(cachelineN<DPB>); //small buffer for K4 loop constants
    //dG8  = (double*)p;        p += 2*maxK4*sizeof(double); //small buffer for K4 loop constants

    dI8  = (double*)p;        p += NtilesN * TheBatch.msize4 * sizeof(cachelineN<DPB>);
    dT8  = (double*)p;        p += NtilesN * TheBatch.wsize4 * sizeof(cachelineN<DPB>);
    //dRM8 = (double*)p;        p += NtilesN                   * sizeof(RotationMatricesN);
    dRMS8 = (double*)p; //dummy pointer
    dRMP8 = (double*)p; if (hasP) p += NtilesN * sizeof(RotationMatrixN<1>);
    dRMD8 = (double*)p; if (hasD) p += NtilesN * sizeof(RotationMatrixN<2>);
    dRMF8 = (double*)p; if (hasF) p += NtilesN * sizeof(RotationMatrixN<3>);
    dRMG8 = (double*)p; if (hasG) p += NtilesN * sizeof(RotationMatrixN<4>);
    //dRMH8 = (double*)p; if (hasH) p += NtilesN * sizeof(RotationMatrixN<5>);


    dDab  = (double*)p; p += NtilesN * JLA*JLB * sizeof(cachelineN<DPB>);
    dDcd  = (double*)p; p += NtilesN * JLC*JLD * sizeof(cachelineN<DPB>);

    dDac  = (double*)p; p += NtilesN * JLA*JLC * sizeof(cachelineN<DPB>);
    dDad  = (double*)p; p += NtilesN * JLA*JLD * sizeof(cachelineN<DPB>);
    dDbc  = (double*)p; p += NtilesN * JLB*JLC * sizeof(cachelineN<DPB>);
    dDbd  = (double*)p; p += NtilesN * JLB*JLD * sizeof(cachelineN<DPB>);


    dFab  = (double*)p; p += NtilesN * JLA*JLB * sizeof(cachelineN<DPB>);
    dFcd  = (double*)p; p += NtilesN * JLC*JLD * sizeof(cachelineN<DPB>);

    dFac  = (double*)p; p += NtilesN * JLA*JLC * sizeof(cachelineN<DPB>);
    dFad  = (double*)p; p += NtilesN * JLA*JLD * sizeof(cachelineN<DPB>);
    dFbc  = (double*)p; p += NtilesN * JLB*JLC * sizeof(cachelineN<DPB>);
    dFbd  = (double*)p; p += NtilesN * JLB*JLD * sizeof(cachelineN<DPB>);


    if (p > BlockMemGPU + MaxBlockMemGPU) {
        std::cout << "SetBuffersN:: Error in ERI block device memory allocation: final offset is larger than available memory ";
        std::cout << (p-BlockMemGPU) << "  " << MaxBlockMemGPU << std::endl;
        //std::cout << ERIoffset << std::endl;
        throw(524);
    }
    //else        std::cout << (p-BlockMemGPU) << "  " << MaxBlockMemGPU << std::endl;
}

uint64_t BlockBufferGPU::MaxTilesN(const BatchInfo & TheBatch) {

    //size_t eribuff   = (MAXPC/DPC) * TheBatch.ERIalgorithm->MemSize(*TheBatch.ABp, *TheBatch.CDp);
    //if (eribuff>=MEM) return 0;

    size_t Hostconstbuff = 0;
    size_t GPUconstbuff = 0;

    int nKab = TheBatch.ABp->Ka*TheBatch.ABp->Kb;
    int nKcd = TheBatch.CDp->Ka*TheBatch.CDp->Kb;

    int nK4 = nKab * nKcd;

    //coinciding centers have all exponentials weights equal to 1
    if (TheBatch.geometry==AACD) nKab = 0;
    if (TheBatch.geometry==AACC) nKab = nKcd = 0;


    bool hasP = ( (TheBatch.la==1)||(TheBatch.lb==1)||(TheBatch.lc==1)||(TheBatch.ld==1) );
    bool hasD = ( (TheBatch.la==2)||(TheBatch.lb==2)||(TheBatch.lc==2)||(TheBatch.ld==2) );
    bool hasF = ( (TheBatch.la==3)||(TheBatch.lb==3)||(TheBatch.lc==3)||(TheBatch.ld==3) );
    bool hasG = ( (TheBatch.la==4)||(TheBatch.lb==4)||(TheBatch.lc==4)||(TheBatch.ld==4) );
    bool hasH = ( (TheBatch.la==5)||(TheBatch.lb==5)||(TheBatch.lc==5)||(TheBatch.ld==5) );


    int JLA = TheBatch.ABp->Ja*(2*TheBatch.la+1);
    int JLB = TheBatch.ABp->Jb*(2*TheBatch.lb+1);
    int JLC = TheBatch.CDp->Ja*(2*TheBatch.lc+1);
    int JLD = TheBatch.CDp->Jb*(2*TheBatch.ld+1);

    //amount of buffer needed in main mem, per tile
    size_t Tsize; {
        size_t p = 0;

        p += 4 * DPB * sizeof(int);
        p += 2       * sizeof(int);

        p += 48 * DPB * sizeof(int);

        p += DPB * sizeof(ERIgeometry);
        p += sizeof(ERIgeometriesN<DPB>);
        //p += nKab * sizeof(cachelineN<DPB>);
        //p += nKcd * sizeof(cachelineN<DPB>);
        p += TheBatch.msize4 * sizeof(cachelineN<DPB>);
        p += TheBatch.wsize4 * sizeof(cachelineN<DPB>);
        //p += sizeof(RotationMatricesN);

        if (hasP) p += sizeof(RotationMatrixN<1>);
        if (hasD) p += sizeof(RotationMatrixN<2>);
        if (hasF) p += sizeof(RotationMatrixN<3>);
        if (hasG) p += sizeof(RotationMatrixN<4>);
        if (hasH) p += sizeof(RotationMatrixN<5>);

        p += JLA*JLB * sizeof(cachelineN<DPB>);
        p += JLC*JLD * sizeof(cachelineN<DPB>);

        p += JLA*JLC * sizeof(cachelineN<DPB>);
        p += JLA*JLD * sizeof(cachelineN<DPB>);
        p += JLB*JLC * sizeof(cachelineN<DPB>);
        p += JLB*JLD * sizeof(cachelineN<DPB>);

        p += JLA*JLB * sizeof(cachelineN<DPB>);
        p += JLC*JLD * sizeof(cachelineN<DPB>);

        p += JLA*JLC * sizeof(cachelineN<DPB>);
        p += JLA*JLD * sizeof(cachelineN<DPB>);
        p += JLB*JLC * sizeof(cachelineN<DPB>);
        p += JLB*JLD * sizeof(cachelineN<DPB>);

        Tsize = p;
    }

    //amount of buffer needed in device mem, per tile
    size_t TsizeGPU; {
        size_t p = 0;

        p += 4 * DPB * sizeof(int);
        p += 2 * sizeof(int);
        p += 48 * DPB * sizeof(int);

        p += 6               * sizeof(cachelineN<DPB>);
        //p += nKab            * sizeof(cachelineN<DPB>);
        //p += nKcd            * sizeof(cachelineN<DPB>);
        //p += 2*nK4           * sizeof(cachelineN<DPB>);
        p += TheBatch.msize4 * sizeof(cachelineN<DPB>);
        p += TheBatch.wsize4 * sizeof(cachelineN<DPB>);
        //p += sizeof(RotationMatricesN);

        if (hasP) p += sizeof(RotationMatrixN<1>);
        if (hasD) p += sizeof(RotationMatrixN<2>);
        if (hasF) p += sizeof(RotationMatrixN<3>);
        if (hasG) p += sizeof(RotationMatrixN<4>);
        if (hasH) p += sizeof(RotationMatrixN<5>);

        p += JLA*JLB * sizeof(cachelineN<DPB>);
        p += JLC*JLD * sizeof(cachelineN<DPB>);

        p += JLA*JLC * sizeof(cachelineN<DPB>);
        p += JLA*JLD * sizeof(cachelineN<DPB>);
        p += JLB*JLC * sizeof(cachelineN<DPB>);
        p += JLB*JLD * sizeof(cachelineN<DPB>);


        p += JLA*JLB * sizeof(cachelineN<DPB>);
        p += JLC*JLD * sizeof(cachelineN<DPB>);

        p += JLA*JLC * sizeof(cachelineN<DPB>);
        p += JLA*JLD * sizeof(cachelineN<DPB>);
        p += JLB*JLC * sizeof(cachelineN<DPB>);
        p += JLB*JLD * sizeof(cachelineN<DPB>);

        TsizeGPU = p;
    }


    Hostconstbuff += 2*DPB * sizeof(int); // to take into account the possible remainder of Juse and Xuse
    Hostconstbuff += 48*DPB * sizeof(int);

    int nT4 = DPB*((2*nK4 + DPB-1)/DPB);
    GPUconstbuff  += nT4 * sizeof(cachelineN<DPB>); // required for K4 constants
    GPUconstbuff  += 48*DPB * sizeof(int);         // required for extra ints (uses one extra tile)
    //GPUconstbuff  += 2*maxK4*sizeof(double);
    GPUconstbuff  += 2*DPB * sizeof(int); // to take into account the possible remainder of Juse and Xuse


    size_t LocalGPUMem = LocalMaxBlockMemGPU[0];
    for (int i=1; i<NumGPUs; ++i) LocalGPUMem = std::min(LocalGPUMem,LocalMaxBlockMemGPU[i]);

    if (Hostconstbuff >= LocalMaxBlockMem) return 0;
    if (GPUconstbuff  >= LocalGPUMem)      return 0;

    uint64_t NtilesCPU = (LocalMaxBlockMem - Hostconstbuff) / Tsize;
    uint64_t NtilesGPU = (LocalGPUMem      - GPUconstbuff)  / TsizeGPU;

    //now compute how many tiles can be used
    return std::min(NtilesCPU, NtilesGPU);
}




//copy everything
const ERIBatch & ERIBatch::operator=(const ERIblock & rhs) {
    ABp = rhs.ABp;
    CDp = rhs.CDp;

    SkipSame  = rhs.SkipSame;
    SameShell = rhs.SameShell;
    SameList  = rhs.SameList;

    geometry  = rhs.geometry;

    la   = rhs.la;
    lb   = rhs.lb;
    lc   = rhs.lc;
    ld   = rhs.ld;
    Lt   = rhs.Lt;
    Lmax = rhs.Lmax;

    //size of the ERI batch
    J4     = rhs.J4;
    fsize  = rhs.fsize;

    msize  = rhs.msize;
    msize4 = rhs.msize4;

    wsize  = rhs.wsize;
    wsize4 = rhs.wsize4;


    //funciones necesarias
    //********************
    ERIalgorithm = rhs.ERIalgorithm;

    //window arguments
    //****************

    AP12list = rhs.AP12list;
    AP34list = rhs.AP34list;

    SP12 = rhs.SP12;
    SP34 = rhs.SP34;

    tmax12 = rhs.tmax12;
    tmax34 = rhs.tmax34;

    State   = rhs.State;

    return *this;
}



void BatchEvaluator::SetBuffers (const ERIBatch & TheBatch) {
    b4blockCPU.SetBuffers(TheBatch, b4ERIs, TheBatch.pFock->nDMs);
}

#include "linear/ntensors.hpp"

void BatchEvaluator::Evaluate    (const ERIBatch & TheBatch, int nmat,   const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant,   double gamma) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    b4blockCPU.SetBuffers(TheBatch, b4ERIs, nmat);

    //geometries
    if      (geometry==ABCD) Evaluate_LP_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_LP_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_LP_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_LP_ABAB(TheBatch);


    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);


    //L step: MIRROR transformations
    if (!S) Evaluate_L      (TheBatch);
    else    Evaluate_L_sameF(TheBatch);


    //generate vector packed rotation matrices for spherical harmonics
    if      (geometry==ABCD) Evaluate_RM_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_RM_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_RM_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_RM_ABAB(TheBatch);


    //D step: ERI Digestion with the DM
    if (!S) {
        if (J)       Evaluate_D_Js(TheBatch, Dsnt, Jnt, nmat);
        if (X)       Evaluate_D_Xs(TheBatch, Dsnt, Xnt, nmat, gamma);
        if (X)       Evaluate_D_Xs(TheBatch, Dant, Ant, nmat, gamma);
    }
    else {
        if (J)       Evaluate_D_Js_sameF(TheBatch, Dsnt, Jnt, nmat);
        if (X)       Evaluate_D_Xs_sameF(TheBatch, Dsnt, Xnt, nmat, gamma);
        if (X)       Evaluate_D_Xs_sameF(TheBatch, Dant, Ant, nmat, gamma);
    }

};

// substract the short range contribution to exchange
void BatchEvaluator::EvaluateCAM (const ERIBatch & TheBatch, int nmat,   const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant,   double alpha, double beta, double mu) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    // evaluate normal Coulomb and (weighted) exchange
    // ===============================================
    Evaluate    (TheBatch, nmat, Dsnt, Dant, Jnt, Xnt, Ant, alpha+beta);

    // evaluate short range exchange and combine
    // =========================================

    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC_SR(TheBatch, mu);

    //L step: MIRROR transformations
    if (!S) Evaluate_L       (TheBatch);
    else    Evaluate_L_sameF (TheBatch);

    // extra D step
    if (!S) {
        if (X)       Evaluate_D_Xs(TheBatch, Dsnt, Xnt, nmat, -beta);
        if (X)       Evaluate_D_Xs(TheBatch, Dant, Ant, nmat, -beta);
    }
    else {
        if (X)       Evaluate_D_Xs_sameF(TheBatch, Dsnt, Xnt, nmat, -beta);
        if (X)       Evaluate_D_Xs_sameF(TheBatch, Dant, Ant, nmat, -beta);
    }

};

// for tensors:
// ============

void BatchEvaluator::EvaluateT  (const ERIBatch & TheBatch, tensor4 & W, const sparsetensorpattern * STP) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    b4blockCPU.SetBuffers(TheBatch, b4ERIs);

    //geometries
    if      (geometry==ABCD) Evaluate_LP_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_LP_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_LP_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_LP_ABAB(TheBatch);


    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);


    //L step: MIRROR transformations
    if (!S) Evaluate_L      (TheBatch);
    else    Evaluate_L_sameF(TheBatch);

    //generate vector packed rotation matrices for spherical harmonics
    if      (geometry==ABCD) Evaluate_RM_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_RM_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_RM_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_RM_ABAB(TheBatch);

    // as last step, instead of ERI digestion, distribute the elements in the general tensor
    DistributeWs (TheBatch, W, STP);
};

void BatchEvaluator::EvaluateDiagonal  (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    b4blockCPU.SetBuffers(TheBatch, b4ERIs);

    //geometries
    if      (geometry==ABCD) Evaluate_LP_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_LP_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_LP_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_LP_ABAB(TheBatch);


    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);


    //L step: MIRROR transformations
    if (!S) Evaluate_L      (TheBatch);
    else    Evaluate_L_sameF(TheBatch);

    //generate vector packed rotation matrices for spherical harmonics
    if      (geometry==ABCD) Evaluate_RM_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_RM_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_RM_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_RM_ABAB(TheBatch);

    // as last step, instead of ERI digestion, distribute the elements in the general tensor
    DistributeDs (TheBatch, W2, STP);
};


void BatchEvaluator::EvaluateDiagonal  (const ERIBatch & TheBatch, tensor2 & D, const sparsetensorpattern * STP) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell; // true

    b4blockCPU.SetBuffers(TheBatch, b4ERIs);

    //geometries
    Evaluate_LP_ABAB(TheBatch);

    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);

    //L step: MIRROR transformations
    Evaluate_L_sameF(TheBatch);

    //generate vector packed rotation matrices for spherical harmonics
    Evaluate_RM_ABAB(TheBatch);

    // as last step, instead of ERI digestion, distribute the elements in the general tensor
    DistributeDs (TheBatch, D, STP);
};

void BatchEvaluator::Evaluate2Center   (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    b4blockCPU.SetBuffers(TheBatch, b4ERIs);

    //geometries
    Evaluate_LP_AACC(TheBatch); // has to be true


    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);


    //L step: MIRROR transformations
    if (!S) Evaluate_L      (TheBatch);
    else    Evaluate_L_sameF(TheBatch);

    //generate vector packed rotation matrices for spherical harmonics
    Evaluate_RM_AACC(TheBatch);

    // as last step, instead of ERI digestion, distribute the elements in the general tensor
    DistributeWs (TheBatch, W2, STP);
}

void BatchEvaluator::Evaluate3Center  (const ERIBatch & TheBatch, r1tensor<tensor4> & W3, const sparsetensorpattern * STP) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    b4blockCPU.SetBuffers(TheBatch, b4ERIs);

    //geometries
    if (geometry==AACD) Evaluate_LP_AACD(TheBatch);

    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);

    //L step: MIRROR transformations
    Evaluate_L      (TheBatch);

    //generate vector packed rotation matrices for spherical harmonics
    if (geometry==AACD) Evaluate_RM_AACD(TheBatch);

    // as last step, instead of ERI digestion, distribute the elements in the general tensor
    DistributeWs (TheBatch, W3, STP);
};

// evaluate relevant entries of the Cholesky tensor
void BatchEvaluator::EvaluateCholesky (const ERIBatch & TheBatch, tensor2           & W2, const sparsetensorpattern * STP) {

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;
    bool S = (TheBatch.geometry==ABAB) && TheBatch.SameShell;

    b4blockCPU.SetBuffers(TheBatch, b4ERIs);

    //geometries
    if      (geometry==ABCD) Evaluate_LP_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_LP_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_LP_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_LP_ABAB(TheBatch);


    //OC steps: gamma function generation and K4 kernel contraction
    Evaluate_OC(TheBatch);


    //L step: MIRROR transformations
    if (!S) Evaluate_L      (TheBatch);
    else    Evaluate_L_sameF(TheBatch);

    //generate vector packed rotation matrices for spherical harmonics
    if      (geometry==ABCD) Evaluate_RM_ABCD(TheBatch);
    else if (geometry==AACD) Evaluate_RM_AACD(TheBatch);
    else if (geometry==AACC) Evaluate_RM_AACC(TheBatch);
    else if (geometry==ABAB) Evaluate_RM_ABAB(TheBatch);

    // as last step, instead of ERI digestion, distribute the elements in the general tensor
    DistributeCholesky (TheBatch, W2, STP);
}


void BatchEvaluator::Evaluate_LP_ABCD(const ERIBatch & TheBatch) {

    {
        ERIgeometry     * erig  = b4blockCPU.E0d;
        ERIgeometries64 * erig8 = b4blockCPU.E8d;

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            for (uint32_t j=0; j<DPC; ++j) {
                uint32_t ap12 = TheBatch.TileList64[i].ap12[j]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[j]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];
                erig[j].MakeRotation4  (AP12, AP34);
                erig[j].Adjust(TheBatch.ABp->inverted, TheBatch.CDp->inverted);
            }
            PackGeometries (erig, *erig8);

            erig8++;
            erig += DPC;
        }
    }

    {
        ERIgeometry     * erig  = b4blockCPU.E0s;
        ERIgeometries32 * erig8 = b4blockCPU.E8s;

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            for (uint32_t j=0; j<FPC; ++j) {
                uint32_t ap12 = TheBatch.TileList32[i].ap12[j]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[j]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];
                erig[j].MakeRotation4  (AP12, AP34);
                erig[j].Adjust(TheBatch.ABp->inverted, TheBatch.CDp->inverted);
            }
            PackGeometries (erig, *erig8);

            erig8++;
            erig += FPC;
        }
    }

}

void BatchEvaluator::Evaluate_LP_AACD(const ERIBatch & TheBatch) {


    {
        ERIgeometry     * erig  = b4blockCPU.E0d;
        ERIgeometries64 * erig8 = b4blockCPU.E8d;

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            for (uint32_t j=0; j<DPC; ++j) {
                uint32_t ap12 = TheBatch.TileList64[i].ap12[j]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[j]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];

                erig[j].MakeRotation3  (AP12, AP34);
                erig[j].Adjust(TheBatch.ABp->inverted, TheBatch.CDp->inverted);
            }

            PackGeometries (erig, *erig8);
            erig8++;
            erig += DPC;
        }
    }

    {
        ERIgeometry     * erig  = b4blockCPU.E0s;
        ERIgeometries32 * erig8 = b4blockCPU.E8s;

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            for (uint32_t j=0; j<FPC; ++j) {
                uint32_t ap12 = TheBatch.TileList32[i].ap12[j]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[j]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];

                erig[j].MakeRotation3  (AP12, AP34);
                erig[j].Adjust(TheBatch.ABp->inverted, TheBatch.CDp->inverted);
            }

            PackGeometries (erig, *erig8);
            erig8++;
            erig += FPC;
        }
    }

}

void BatchEvaluator::Evaluate_LP_AACC(const ERIBatch & TheBatch) {

    {
        ERIgeometry     * erig  = b4blockCPU.E0d;
        ERIgeometries64 * erig8 = b4blockCPU.E8d;

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            for (uint32_t k=0; k<DPC; ++k) {
                uint32_t ap12 = TheBatch.TileList64[i].ap12[k]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[k]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];
                erig[k].MakeRotation2  (AP12.A, AP34.A);
            }
            PackGeometries (erig, *erig8);
            erig8++;
            erig += DPC;
        }
    }

    {
        ERIgeometry     * erig  = b4blockCPU.E0s;
        ERIgeometries32 * erig8 = b4blockCPU.E8s;

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            for (uint32_t k=0; k<FPC; ++k) {
                uint32_t ap12 = TheBatch.TileList32[i].ap12[k]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[k]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];
                erig[k].MakeRotation2  (AP12.A, AP34.A);
            }
            PackGeometries (erig, *erig8);
            erig8++;
            erig += FPC;
        }
    }

}

void BatchEvaluator::Evaluate_LP_ABAB(const ERIBatch & TheBatch) {

    {
        ERIgeometry     * erig  = b4blockCPU.E0d;
        ERIgeometries64 * erig8 = b4blockCPU.E8d;

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            for (uint32_t k=0; k<DPC; ++k) {
                uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                erig[k].MakeRotation2S ( AP12 );
                erig[k].Adjust(TheBatch.ABp->inverted, TheBatch.CDp->inverted);
            }
            PackGeometries (erig, *erig8);
            erig8++;
            erig += DPC;
        }
    }

    {
        ERIgeometry     * erig  = b4blockCPU.E0s;
        ERIgeometries32 * erig8 = b4blockCPU.E8s;

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            for (uint32_t k=0; k<FPC; ++k) {
                uint32_t ap12 = TheBatch.TileList32[i].ap12[k];
                const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                erig[k].MakeRotation2S ( AP12 );
                erig[k].Adjust(TheBatch.ABp->inverted, TheBatch.CDp->inverted);
            }
            PackGeometries (erig, *erig8);
            erig8++;
            erig += FPC;
        }
    }


}

//decoupled paths
void BatchEvaluator::Evaluate_OC(const ERIBatch & TheBatch) {

    bool OnlyJ = false; //TheBatch.J && !TheBatch.X;

    {
        cacheline64     * i8   = b4blockCPU.I8d;
        ERIgeometries64 * e8   = b4blockCPU.E8d;

        //regular kernel contraction
        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            TheBatch.ERIalgorithm->K4 (e8[i], TheBatch.TileList64[i], *TheBatch.ABp, *TheBatch.CDp, i8, b4ERIs);

            i8 += TheBatch.msize4;
        }
    }

    {
        cacheline32     * i8   = b4blockCPU.I8s;
        ERIgeometries32 * e8   = b4blockCPU.E8s;

        //regular kernel contraction
        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            TheBatch.ERIalgorithm->K4 (e8[i], TheBatch.TileList32[i], *TheBatch.ABp, *TheBatch.CDp, i8, b4ERIs);

            i8 += TheBatch.msize4;
        }
    }

}

void BatchEvaluator::Evaluate_OC_SR(const ERIBatch & TheBatch, double w2) {

    {
        cacheline64     * i8   = b4blockCPU.I8d;
        ERIgeometries64 * e8   = b4blockCPU.E8d;

        //regular kernel contraction
        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            TheBatch.ERIalgorithm->K4SR (e8[i], TheBatch.TileList64[i], *TheBatch.ABp, *TheBatch.CDp, i8, b4ERIs, w2);

            i8 += TheBatch.msize4;
        }
    }

    // not yet implemented

    /*
    {
        cacheline32     * i8   = b4blockCPU.I8s;
        ERIgeometries32 * e8   = b4blockCPU.E8s;

        //regular kernel contraction
        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            TheBatch.ERIalgorithm->K4 (e8[i], TheBatch.TileList32[i], *TheBatch.ABp, *TheBatch.CDp, i8, b4ERIs);

            i8 += TheBatch.msize4;
        }
    }
    */
}

void BatchEvaluator::Evaluate_OC_LR(const ERIBatch & TheBatch, double w2) {

    {
        cacheline64     * i8   = b4blockCPU.I8d;
        ERIgeometries64 * e8   = b4blockCPU.E8d;

        //regular kernel contraction
        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
            TheBatch.ERIalgorithm->K4LR (e8[i], TheBatch.TileList64[i], *TheBatch.ABp, *TheBatch.CDp, i8, b4ERIs, w2);

            i8 += TheBatch.msize4;
        }
    }

    // not yet implemented

    /*
    {
        cacheline32     * i8   = b4blockCPU.I8s;
        ERIgeometries32 * e8   = b4blockCPU.E8s;

        //regular kernel contraction
        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {
            TheBatch.ERIalgorithm->K4 (e8[i], TheBatch.TileList32[i], *TheBatch.ABp, *TheBatch.CDp, i8, b4ERIs);

            i8 += TheBatch.msize4;
        }
    }
    */
}


void BatchEvaluator::Evaluate_L(const ERIBatch & TheBatch) {
    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    {
        ERIgeometries64 * e8  = b4blockCPU.E8d;
        cacheline64     * i8  = b4blockCPU.I8d;
        cacheline64     * t8  = b4blockCPU.T8d;

        //packed
        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {

            for (int ja=0; ja<TheBatch.ABp->Ja; ++ja) {
                for (int jb=0; jb<TheBatch.ABp->Jb; ++jb) {
                    for (int jc=0; jc<TheBatch.CDp->Ja; ++jc) {
                        for (int jd=0; jd<TheBatch.CDp->Jb; ++jd) {
                            TheBatch.ERIalgorithm->MIRROR(i8, e8[i], t8, b4ERIs);

                            i8 += TheBatch.msize;
                            t8 += TheBatch.wsize;
                        }
                    }
                }
            }
        }
    }

    {
        ERIgeometries32 * e8  = b4blockCPU.E8s;
        cacheline32     * i8  = b4blockCPU.I8s;
        cacheline32     * t8  = b4blockCPU.T8s;

        //packed
        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {

            for (int ja=0; ja<TheBatch.ABp->Ja; ++ja) {
                for (int jb=0; jb<TheBatch.ABp->Jb; ++jb) {
                    for (int jc=0; jc<TheBatch.CDp->Ja; ++jc) {
                        for (int jd=0; jd<TheBatch.CDp->Jb; ++jd) {
                            TheBatch.ERIalgorithm->MIRROR(i8, e8[i], t8, b4ERIs);

                            i8 += TheBatch.msize;
                            t8 += TheBatch.wsize;
                        }
                    }
                }
            }
        }
    }

}

void BatchEvaluator::Evaluate_L_sameF(const ERIBatch & TheBatch) {
    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    //APPLY TRANSFORMATIONS

    {
        ERIgeometries64 * e8 = b4blockCPU.E8d;
        cacheline64     * i8 = b4blockCPU.I8d;
        cacheline64     * t8 = b4blockCPU.T8d;

        //packed
        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {

            for (int ja=0; ja<TheBatch.ABp->Ja; ++ja) {
                for (int jb=0; jb<TheBatch.ABp->Jb; ++jb) {
                    for (int jc=0; jc<TheBatch.CDp->Ja; ++jc) {
                        for (int jd=0; jd<TheBatch.CDp->Jb; ++jd) {
                            //skip unnecessary (repeated) transformations
                            if (ja*JB+jb >= jc*JD+jd) {
                                TheBatch.ERIalgorithm->MIRROR(i8, e8[i], t8, b4ERIs);
                            }
                            i8 += TheBatch.msize;
                            t8 += TheBatch.wsize;
                        }
                    }
                }
            }
        }
    }

    {
        ERIgeometries32 * e8 = b4blockCPU.E8s;
        cacheline32     * i8 = b4blockCPU.I8s;
        cacheline32     * t8 = b4blockCPU.T8s;

        //packed
        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {

            for (int ja=0; ja<TheBatch.ABp->Ja; ++ja) {
                for (int jb=0; jb<TheBatch.ABp->Jb; ++jb) {
                    for (int jc=0; jc<TheBatch.CDp->Ja; ++jc) {
                        for (int jd=0; jd<TheBatch.CDp->Jb; ++jd) {
                            //skip unnecessary (repeated) transformations
                            if (ja*JB+jb >= jc*JD+jd) {
                                TheBatch.ERIalgorithm->MIRROR(i8, e8[i], t8, b4ERIs);
                            }
                            i8 += TheBatch.msize;
                            t8 += TheBatch.wsize;
                        }
                    }
                }
            }
        }
    }

}




void BatchEvaluator::Evaluate_RM_ABCD(const ERIBatch & TheBatch) {

    {
        ERIgeometry        * erig = b4blockCPU.E0d;
        RotationMatrices64 * rm8  = b4blockCPU.RM8d;

        RotationMatrix RM[DPC];

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<DPC; ++k) {
                if ((TheBatch.TileList64[i].inuse & (1<<k)) == 0) continue;
                uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                RM[k].From((*TheBatch.AP12list)[ap12].RM,  erig[k], TheBatch.Lmax);
            }

            erig += DPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }
    }

    {
        ERIgeometry        * erig = b4blockCPU.E0s;
        RotationMatrices32 * rm8  = b4blockCPU.RM8s;

        RotationMatrix RM[FPC];

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {

            //pack the rotation matrices in a cacheline32 array
            for (uint32_t k=0; k<FPC; ++k) {
                if (TheBatch.TileList32[i].use[k] == 0) continue;
                uint32_t ap12 = TheBatch.TileList32[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[k];

                RM[k].From((*TheBatch.AP12list)[ap12].RM,  erig[k], TheBatch.Lmax);
            }

            erig += FPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }
    }


}

void BatchEvaluator::Evaluate_RM_AACD(const ERIBatch & TheBatch) {

    {
        ERIgeometry        * erig = b4blockCPU.E0d;
        RotationMatrices64 * rm8  = b4blockCPU.RM8d;

        RotationMatrix RM[DPC];

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<DPC; ++k) {
                if ((TheBatch.TileList64[i].inuse & (1<<k)) == 0) continue;
                uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                RM[k].From((*TheBatch.AP34list)[ap34].RM,  erig[k], TheBatch.Lmax);
            }

            erig += DPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }
    }

    {
        ERIgeometry        * erig = b4blockCPU.E0s;
        RotationMatrices32 * rm8  = b4blockCPU.RM8s;

        RotationMatrix RM[FPC];

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<FPC; ++k) {
                if (TheBatch.TileList32[i].use[k] == 0) continue;
                uint32_t ap12 = TheBatch.TileList32[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[k];

                RM[k].From((*TheBatch.AP34list)[ap34].RM,  erig[k], TheBatch.Lmax);
            }

            erig += FPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }
    }


}

void BatchEvaluator::Evaluate_RM_AACC(const ERIBatch & TheBatch) {

    {
        ERIgeometry        * erig = b4blockCPU.E0d;
        RotationMatrices64 * rm8  = b4blockCPU.RM8d;

        RotationMatrix RM[DPC];

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<DPC; ++k) {
                if ((TheBatch.TileList64[i].inuse & (1<<k)) == 0) continue;

                uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];

                RM[k].From(AP12.A, AP34.A, TheBatch.Lmax);
            }

            erig += DPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }

    }

    {
        ERIgeometry        * erig = b4blockCPU.E0s;
        RotationMatrices32 * rm8  = b4blockCPU.RM8s;

        RotationMatrix RM[FPC];

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<FPC; ++k) {
                if (TheBatch.TileList32[i].use[k] == 0) continue;

                uint32_t ap12 = TheBatch.TileList32[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[k];

                const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
                const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];

                RM[k].From(AP12.A, AP34.A, TheBatch.Lmax);
            }

            erig += FPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }

    }


}

void BatchEvaluator::Evaluate_RM_ABAB(const ERIBatch & TheBatch) {

    {
        ERIgeometry        * erig = b4blockCPU.E0d;
        RotationMatrices64 * rm8  = b4blockCPU.RM8d;

        RotationMatrix RM[DPC];

        for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<DPC; ++k) {
                if ((TheBatch.TileList64[i].inuse & (1<<k)) == 0) continue;
                uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                RM[k] = (*TheBatch.AP12list)[ap12].RM;
            }

            erig += DPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }
    }

    {
        ERIgeometry        * erig = b4blockCPU.E0s;
        RotationMatrices32 * rm8  = b4blockCPU.RM8s;

        RotationMatrix RM[FPC];

        for (uint32_t i=0; i<TheBatch.Ntiles32; ++i) {

            //pack the rotation matrices in a cacheline64-array
            for (uint32_t k=0; k<FPC; ++k) {
                if (TheBatch.TileList32[i].use[k] == 0) continue;

                uint32_t ap12 = TheBatch.TileList32[i].ap12[k];
                uint32_t ap34 = TheBatch.TileList32[i].ap34[k];

                RM[k] = (*TheBatch.AP12list)[ap12].RM;
            }

            erig += FPC;
            rm8[i].From(RM, TheBatch.Lmax);
        }
    }

}


void jxd64NULL(cacheline64     * F, const cacheline64     * T, const cacheline64     * D) {}
void jxd32NULL(cacheline32     * F, const cacheline32     * T, const cacheline32     * D) {}


#include <string.h>
#include <map>

void BatchEvaluator::Evaluate_D_Js       (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs) {

    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const uint8_t na = TheBatch.ABp->nb1;
    const uint8_t nb = TheBatch.ABp->nb2;
    const uint8_t nc = TheBatch.CDp->nb1;
    const uint8_t nd = TheBatch.CDp->nb2;

    const ConstNpattern CNP = Jnt.GetPattern();

    const uint8_t ea = CNP.GetID(TheBatch.SP12[0].ata);
    const uint8_t eb = CNP.GetID(TheBatch.SP12[0].atb);
    const uint8_t ec = CNP.GetID(TheBatch.SP34[0].ata);
    const uint8_t ed = CNP.GetID(TheBatch.SP34[0].atb);

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const bool AB = !TheBatch.ABp->samef;
    const bool CD = !TheBatch.CDp->samef;

    //CONTRACT WITH DENSITY MATRIX
    {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;
                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tb = TheBatch.SP12[ap12].atb;

                    uint64_t tab = (uint64_t(ta)<<32) + uint64_t(tb);
                    Fmap[tab] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sAB;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sAB*sizeof(double));
        }

        // select functions
        JXD64 Jabcd;

        if      ( AB &&  CD) Jabcd = Echidna::dmd.J64abcd[la][lb][lc][ld];
        else if ( AB && !CD) Jabcd = Echidna::dmd.J64abcc[la][lb] [lc];
        else if (!AB &&  CD) Jabcd = Echidna::dmd.J64aacd[la] [lc][ld];
        else if (!AB && !CD) Jabcd = Echidna::dmd.J64aacc[la] [lc];

        JXrot64 Rcd  = Echidna::dmd.R64[lc][ld];
        JXrot64 RTab = Echidna::dmd.RT64[la][lb];

        //add Coulomb interaction (only symmetric)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dcd = b4blockCPU.Dcd;
            cacheline64 * Fab = b4blockCPU.Fab;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useJ == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                memset(Fab, 0, NMs*sAB*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tc = TheBatch.SP34[ap34].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    const double * dDcd = Dnt(tc, td, nc, nd);
                    for (int mm=0; mm<NMs*sCD; ++mm) Dcd[mm](k) = dDcd[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JC*JD; ++n) Rcd(Dcd + n*(MC*MD), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAB = (ja*JB + jb)*(MA*MB);
                                uint32_t oCD = (jc*JD + jd)*(MC*MD);
                                for (int n=0; n<NMs; ++n) Jabcd(Fab + n*sAB + oAB, t8, Dcd + n*sCD + oCD);
                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JA*JB; ++n) RTab(Fab + n*(MA*MB), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tb = TheBatch.SP12[ap12].atb;

                    uint64_t tab = (uint64_t(ta)<<32) + uint64_t(tb);
                    double * pF = Fmap[tab];

                    for (int mm=0; mm<NMs*sAB; ++mm) pF[mm] += Fab[mm](k);
                }
            }

        }

        // update
        NTsub Jsub_ab = Jnt(ea,eb,na,nb);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t ta = (it->first>>32);
            uint32_t tb = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFab =  Jsub_ab(ta, tb);
            for (int mm=0; mm<NMs*sAB; ++mm) dFab[mm] += pF[mm];
        }

    }

    {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t tc = TheBatch.SP34[ap34].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tcd = (uint64_t(tc)<<32) + uint64_t(td);
                    Fmap[tcd] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sCD;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sCD*sizeof(double));
        }

        // select functions
        JXD64 Jcdab;
        if      ( AB &&  CD) Jcdab = Echidna::dmd.J64cdab[la][lb][lc][ld];
        else if ( AB && !CD) Jcdab = Echidna::dmd.J64ccab[la][lb] [lc];
        else if (!AB &&  CD) Jcdab = Echidna::dmd.J64cdaa[la] [lc][ld];
        else if (!AB && !CD) Jcdab = Echidna::dmd.J64ccaa[la] [lc];

        JXrot64 Rab  = Echidna::dmd.R64[la][lb];
        JXrot64 RTcd = Echidna::dmd.RT64[lc][ld];

        //add Coulomb interaction (only symmetric)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dab = b4blockCPU.Dab;
            cacheline64 * Fcd = b4blockCPU.Fcd;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useJ == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                memset(Fcd, 0, NMs*sCD*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tb = TheBatch.SP12[ap12].atb;

                    const double * dDab = Dnt(ta, tb, na, nb);
                    for (int mm=0; mm<NMs*sAB; ++mm) Dab[mm](k) = dDab[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JA*JB; ++n) Rab(Dab + n*(MA*MB), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAB = (ja*JB + jb)*(MA*MB);
                                uint32_t oCD = (jc*JD + jd)*(MC*MD);
                                for (int n=0; n<NMs; ++n) Jcdab(Fcd + n*sCD + oCD, t8, Dab + n*sAB + oAB);
                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JC*JD; ++n) RTcd(Fcd + n*(MC*MD), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tc = TheBatch.SP34[ap34].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tcd = (uint64_t(tc)<<32) + uint64_t(td);
                    double * pF = Fmap[tcd];

                    for (int mm=0; mm<NMs*sCD; ++mm) pF[mm] += Fcd[mm](k);
                }
            }

        }

        // update
        NTsub Jsub_cd = Jnt(ec,ed,nc,nd);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t tc = (it->first>>32);
            uint32_t td = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFcd =  Jsub_cd(tc, td);
            for (int mm=0; mm<NMs*sCD; ++mm) dFcd[mm] += pF[mm];
        }
    }

}

void BatchEvaluator::Evaluate_D_Xs       (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Xnt, int NMs, double w) {

    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const uint8_t na = TheBatch.ABp->nb1;
    const uint8_t nb = TheBatch.ABp->nb2;
    const uint8_t nc = TheBatch.CDp->nb1;
    const uint8_t nd = TheBatch.CDp->nb2;

    const ConstNpattern CNP = Xnt.GetPattern();

    const uint8_t ea = CNP.GetID(TheBatch.SP12[0].ata);
    const uint8_t eb = CNP.GetID(TheBatch.SP12[0].atb);
    const uint8_t ec = CNP.GetID(TheBatch.SP34[0].ata);
    const uint8_t ed = CNP.GetID(TheBatch.SP34[0].atb);

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    bool AB = !TheBatch.ABp->samef;
    bool CD = !TheBatch.CDp->samef;

    //CONTRACT WITH DENSITY MATRIX
    {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tac = (uint64_t(ta)<<32) + uint64_t(tc);
                    Fmap[tac] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sAC;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sAC*sizeof(double));
        }

        // select functions
        JXD64 Xacbd;
        if      ( AB &&  CD) Xacbd = Echidna::dmd.X64acbd[la][lb][lc][ld];
        else if ( AB && !CD) Xacbd = Echidna::dmd.X64acbc[la][lb] [lc];
        else if (!AB &&  CD) Xacbd = Echidna::dmd.X64acad[la] [lc][ld];
        else if (!AB && !CD) Xacbd = Echidna::dmd.X64acac[la] [lc];

        JXrot64 Rbd  = Echidna::dmd.R64[lb][ld];
        JXrot64 RTac = Echidna::dmd.RT64[la][lc];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dbd = b4blockCPU.Dbd;
            cacheline64 * Fac = b4blockCPU.Fac;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fac, 0, NMs*sAC*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    const double * dDbd = Dnt(tb, td, nb, nd);
                    for (int mm=0; mm<NMs*sBD; ++mm) Dbd[mm](k) = dDbd[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JB*JD; ++n) Rbd(Dbd + n*(MB*MD), &RMV);

                // digestion (contraction)
                // ***********************

                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAC = (ja*JC + jc)*(MA*MC);
                                uint32_t oBD = (jb*JD + jd)*(MB*MD);
                                for (int n=0; n<NMs; ++n) Xacbd(Fac + n*sAC + oAC, t8, Dbd + n*sBD + oBD);
                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JA*JC; ++n) RTac(Fac + n*(MA*MC), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tac = (uint64_t(ta)<<32) + uint64_t(tc);
                    double * pF = Fmap[tac];

                    for (int mm=0; mm<NMs*sAC; ++mm) pF[mm] += Fac[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_ac = Xnt(ea,ec,na,nc);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t ta = (it->first>>32);
            uint32_t tc = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFac =  Xsub_ac(ta, tc);
            for (int mm=0; mm<NMs*sAC; ++mm) dFac[mm] += w*pF[mm];
        }

    }

    if (CD) {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tad = (uint64_t(ta)<<32) + uint64_t(td);
                    Fmap[tad] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sAD;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sAD*sizeof(double));
        }

        // select functions
        JXD64 Xadbc;
        if      ( AB &&  CD) Xadbc = Echidna::dmd.X64adbc[la][lb][lc][ld];
        else if ( AB && !CD) Xadbc = jxd64NULL;
        else if (!AB &&  CD) Xadbc = Echidna::dmd.X64adac[la] [lc][ld];
        else if (!AB && !CD) Xadbc = jxd64NULL;

        JXrot64  Rbc = Echidna::dmd.R64[lb][lc];
        JXrot64 RTad = Echidna::dmd.RT64[la][ld];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dbc = b4blockCPU.Dbc;
            cacheline64 * Fad = b4blockCPU.Fad;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fad, 0, NMs*sAD*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    const double * dDbc = Dnt(tb, tc, nb, nc);
                    for (int mm=0; mm<NMs*sBC; ++mm) Dbc[mm](k) = dDbc[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JB*JC; ++n) Rbc(Dbc + n*(MB*MC), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAD = (ja*JD + jd)*(MA*MD);
                                uint32_t oBC = (jb*JC + jc)*(MB*MC);
                                for (int n=0; n<NMs; ++n) Xadbc(Fad + n*sAD + oAD, t8, Dbc + n*sBC + oBC);
                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JA*JD; ++n) RTad(Fad + n*(MA*MD), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tad = (uint64_t(ta)<<32) + uint64_t(td);
                    double * pF = Fmap[tad];

                    for (int mm=0; mm<NMs*sAD; ++mm) pF[mm] += Fad[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_ad = Xnt(ea,ed,na,nd);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t ta = (it->first>>32);
            uint32_t td = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFad =  Xsub_ad(ta, td);
            for (int mm=0; mm<NMs*sAD; ++mm) dFad[mm] += w*pF[mm];
        }

    }

    if (AB && CD) {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tbc = (uint64_t(tb)<<32) + uint64_t(tc);
                    Fmap[tbc] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sBC;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sBC*sizeof(double));
        }

        // select functions
        JXD64 Xbcad;

        if      ( AB &&  CD) Xbcad = Echidna::dmd.X64bcad[la][lb][lc][ld];
        else if ( AB && !CD) Xbcad = jxd64NULL;
        else if (!AB &&  CD) Xbcad = jxd64NULL;
        else if (!AB && !CD) Xbcad = jxd64NULL;

        JXrot64 Rad  = Echidna::dmd.R64[la][ld];
        JXrot64 RTbc = Echidna::dmd.RT64[lb][lc];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dad = b4blockCPU.Dad;
            cacheline64 * Fbc = b4blockCPU.Fbc;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fbc, 0, NMs*sBC*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    const double * dDad = Dnt(ta, td, na, nd);
                    for (int mm=0; mm<NMs*sAD; ++mm) Dad[mm](k) = dDad[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JA*JD; ++n) Rad(Dad + n*(MA*MD), &RMV);

                // digestion (contraction)
                // ***********************

                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAD = (ja*JD + jd)*(MA*MD);
                                uint32_t oBC = (jb*JC + jc)*(MB*MC);
                                for (int n=0; n<NMs; ++n) Xbcad(Fbc + n*sBC + oBC, t8, Dad + n*sAD + oAD);
                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JB*JC; ++n) RTbc(Fbc + n*(MB*MC), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tbc = (uint64_t(tb)<<32) + uint64_t(tc);
                    double * pF = Fmap[tbc];

                    for (int mm=0; mm<NMs*sBC; ++mm) pF[mm] += Fbc[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_bc = Xnt(eb,ec,nb,nc);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t tb = (it->first>>32);
            uint32_t tc = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFbc =  Xsub_bc(tb, tc);
            for (int mm=0; mm<NMs*sBC; ++mm) dFbc[mm] += w*pF[mm];
        }

    }

    if (AB) {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tbd = (uint64_t(tb)<<32) + uint64_t(td);
                    Fmap[tbd] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sBD;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sBD*sizeof(double));
        }

        // select functions
        JXD64 Xbdac;
        if      ( AB &&  CD) Xbdac = Echidna::dmd.X64bdac[la][lb][lc][ld];
        else if ( AB && !CD) Xbdac = Echidna::dmd.X64bcac[la][lb] [lc];
        else if (!AB &&  CD) Xbdac = jxd64NULL;
        else if (!AB && !CD) Xbdac = jxd64NULL;

        JXrot64 Rac  = Echidna::dmd.R64[la][lc];
        JXrot64 RTbd = Echidna::dmd.RT64[lb][ld];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dac = b4blockCPU.Dac;
            cacheline64 * Fbd = b4blockCPU.Fbd;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fbd, 0, NMs*sBD*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    const double * dDac = Dnt(ta, tc, na, nc);
                    for (int mm=0; mm<NMs*sAC; ++mm) Dac[mm](k) = dDac[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JA*JC; ++n) Rac(Dac + n*(MA*MC), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAC = (ja*JC + jc)*(MA*MC);
                                uint32_t oBD = (jb*JD + jd)*(MB*MD);
                                for (int n=0; n<NMs; ++n) Xbdac(Fbd + n*sBD + oBD, t8, Dac + n*sAC + oAC);
                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JB*JD; ++n) RTbd(Fbd + n*(MB*MD), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tbd = (uint64_t(tb)<<32) + uint64_t(td);
                    double * pF = Fmap[tbd];

                    for (int mm=0; mm<NMs*sBD; ++mm) pF[mm] += Fbd[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_bd = Xnt(eb,ed,nb,nd);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t tb = (it->first>>32);
            uint32_t td = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFbd =  Xsub_bd(tb, td);
            for (int mm=0; mm<NMs*sBD; ++mm) dFbd[mm] += w*pF[mm];
        }

    }

}

void BatchEvaluator::Evaluate_D_Js_sameF (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs) {

    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const uint8_t na = TheBatch.ABp->nb1;
    const uint8_t nb = TheBatch.ABp->nb2;
    const uint8_t nc = TheBatch.CDp->nb1;
    const uint8_t nd = TheBatch.CDp->nb2;

    const ConstNpattern CNP = Jnt.GetPattern();

    const uint8_t ea = CNP.GetID(TheBatch.SP12[0].ata);
    const uint8_t eb = CNP.GetID(TheBatch.SP12[0].atb);
    const uint8_t ec = CNP.GetID(TheBatch.SP34[0].ata);
    const uint8_t ed = CNP.GetID(TheBatch.SP34[0].atb);

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    bool AB = !TheBatch.ABp->samef;
    bool CD = !TheBatch.CDp->samef;

    //CONTRACT WITH DENSITY MATRIX
    {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;
                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tb = TheBatch.SP12[ap12].atb;

                    uint64_t tab = (uint64_t(ta)<<32) + uint64_t(tb);
                    Fmap[tab] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sAB;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sAB*sizeof(double));
        }

        // select functions
        JXD64 Jabcd;

        if      ( AB &&  CD) Jabcd = Echidna::dmd.J64abcd[la][lb][lc][ld];
        else if ( AB && !CD) Jabcd = Echidna::dmd.J64abcc[la][lb] [lc];
        else if (!AB &&  CD) Jabcd = Echidna::dmd.J64aacd[la] [lc][ld];
        else if (!AB && !CD) Jabcd = Echidna::dmd.J64aacc[la] [lc];

        JXrot64 Rcd  = Echidna::dmd.R64[lc][ld];
        JXrot64 RTab = Echidna::dmd.RT64[la][lb];

        //add Coulomb interaction (only symmetric)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dcd = b4blockCPU.Dcd;
            cacheline64 * Fab = b4blockCPU.Fab;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useJ == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                memset(Fab, 0, NMs*sAB*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tc = TheBatch.SP34[ap34].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    const double * dDcd = Dnt(tc, td, nc, nd);
                    for (int mm=0; mm<NMs*sCD; ++mm) Dcd[mm](k) = dDcd[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JC*JD; ++n) Rcd(Dcd + n*(MC*MD), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {

                                uint32_t oAB = (ja*JB + jb)*(MA*MB);
                                uint32_t oCD = (jc*JD + jd)*(MC*MD);

                                if (ja*JB+jb >= jc*JD+jd)
                                    for (int n=0; n<NMs; ++n) Jabcd(Fab + n*sAB + oAB, t8, Dcd + n*sCD + oCD);

                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JA*JB; ++n) RTab(Fab + n*(MA*MB), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tb = TheBatch.SP12[ap12].atb;

                    uint64_t tab = (uint64_t(ta)<<32) + uint64_t(tb);
                    double * pF = Fmap[tab];

                    for (int mm=0; mm<NMs*sAB; ++mm) pF[mm] += Fab[mm](k);
                }
            }

        }

        // update
        NTsub Jsub_ab = Jnt(ea,eb,na,nb);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t ta = (it->first>>32);
            uint32_t tb = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFab =  Jsub_ab(ta, tb);
            for (int mm=0; mm<NMs*sAB; ++mm) dFab[mm] += pF[mm];
        }

    }

    {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t tc = TheBatch.SP34[ap34].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tcd = (uint64_t(tc)<<32) + uint64_t(td);
                    Fmap[tcd] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sCD;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sCD*sizeof(double));
        }

        // select functions
        JXD64 Jcdab;
        if      ( AB &&  CD) Jcdab = Echidna::dmd.J64cdab[la][lb][lc][ld];
        else if ( AB && !CD) Jcdab = Echidna::dmd.J64ccab[la][lb] [lc];
        else if (!AB &&  CD) Jcdab = Echidna::dmd.J64cdaa[la] [lc][ld];
        else if (!AB && !CD) Jcdab = Echidna::dmd.J64ccaa[la] [lc];

        JXrot64 Rab  = Echidna::dmd.R64[la][lb];
        JXrot64 RTcd = Echidna::dmd.RT64[lc][ld];

        //add Coulomb interaction (only symmetric)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dab = b4blockCPU.Dab;
            cacheline64 * Fcd = b4blockCPU.Fcd;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useJ == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                memset(Fcd, 0, NMs*sCD*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tb = TheBatch.SP12[ap12].atb;

                    const double * dDab = Dnt(ta, tb, na, nb);
                    for (int mm=0; mm<NMs*sAB; ++mm) Dab[mm](k) = dDab[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JA*JB; ++n) Rab(Dab + n*(MA*MB), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {

                                uint32_t oAB = (ja*JB + jb)*(MA*MB);
                                uint32_t oCD = (jc*JD + jd)*(MC*MD);

                                if (ja*JB+jb > jc*JD+jd)
                                    for (int n=0; n<NMs; ++n) Jcdab(Fcd + n*sCD + oCD, t8, Dab + n*sAB + oAB);

                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JC*JD; ++n) RTcd(Fcd + n*(MC*MD), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tc = TheBatch.SP34[ap34].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tcd = (uint64_t(tc)<<32) + uint64_t(td);
                    double * pF = Fmap[tcd];

                    for (int mm=0; mm<NMs*sCD; ++mm) pF[mm] += Fcd[mm](k);
                }
            }

        }

        // update
        NTsub Jsub_cd = Jnt(ec,ed,nc,nd);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t tc = (it->first>>32);
            uint32_t td = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFcd =  Jsub_cd(tc, td);
            for (int mm=0; mm<NMs*sCD; ++mm) dFcd[mm] += pF[mm];
        }
    }

}

void BatchEvaluator::Evaluate_D_Xs_sameF (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Xnt, int NMs, double w) {

    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const uint8_t na = TheBatch.ABp->nb1;
    const uint8_t nb = TheBatch.ABp->nb2;
    const uint8_t nc = TheBatch.CDp->nb1;
    const uint8_t nd = TheBatch.CDp->nb2;

    const ConstNpattern CNP = Xnt.GetPattern();

    const uint8_t ea = CNP.GetID(TheBatch.SP12[0].ata);
    const uint8_t eb = CNP.GetID(TheBatch.SP12[0].atb);
    const uint8_t ec = CNP.GetID(TheBatch.SP34[0].ata);
    const uint8_t ed = CNP.GetID(TheBatch.SP34[0].atb);

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    bool AB = !TheBatch.ABp->samef;
    bool CD = !TheBatch.CDp->samef;

    //CONTRACT WITH DENSITY MATRIX
    {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tac = (uint64_t(ta)<<32) + uint64_t(tc);
                    Fmap[tac] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sAC;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sAC*sizeof(double));
        }

        // select functions
        JXD64 Xacbd;
        if      ( AB &&  CD) Xacbd = Echidna::dmd.X64acbd[la][lb][lc][ld];
        else if ( AB && !CD) Xacbd = Echidna::dmd.X64acbc[la][lb] [lc];
        else if (!AB &&  CD) Xacbd = Echidna::dmd.X64acad[la] [lc][ld];
        else if (!AB && !CD) Xacbd = Echidna::dmd.X64acac[la] [lc];

        JXD64 Xaabb  = Echidna::dmd.X64aabb[la][lb];

        JXrot64 Rbd  = Echidna::dmd.R64[lb][ld];
        JXrot64 RTac = Echidna::dmd.RT64[la][lc];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dbd = b4blockCPU.Dbd;
            cacheline64 * Fac = b4blockCPU.Fac;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fac, 0, NMs*sAC*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    const double * dDbd = Dnt(tb, td, nb, nd);
                    for (int mm=0; mm<NMs*sBD; ++mm) Dbd[mm](k) = dDbd[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JB*JD; ++n) Rbd(Dbd + n*(MB*MD), &RMV);

                // digestion (contraction)
                // ***********************

                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAC = (ja*JC + jc)*(MA*MC);
                                uint32_t oBD = (jb*JD + jd)*(MB*MD);

                                //these contract to the (block)diagonal
                                if (ja*JB+jb > jc*JD+jd)
                                    for (int n=0; n<NMs; ++n) Xacbd(Fac + n*sAC + oAC, t8, Dbd + n*sBD + oBD);
                                else if (ja==jc && jb==jd)
                                    for (int n=0; n<NMs; ++n) Xaabb(Fac + n*sAC + oAC, t8, Dbd + n*sBD + oBD);

                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JA*JC; ++n) RTac(Fac + n*(MA*MC), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tac = (uint64_t(ta)<<32) + uint64_t(tc);
                    double * pF = Fmap[tac];

                    for (int mm=0; mm<NMs*sAC; ++mm) pF[mm] += Fac[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_ac = Xnt(ea,ec,na,nc);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t ta = (it->first>>32);
            uint32_t tc = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFac =  Xsub_ac(ta, tc);
            for (int mm=0; mm<NMs*sAC; ++mm) dFac[mm] += w*pF[mm];
        }

    }

    if (CD) {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tad = (uint64_t(ta)<<32) + uint64_t(td);
                    Fmap[tad] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sAD;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sAD*sizeof(double));
        }

        // select functions
        JXD64 Xadbc;
        if      ( AB &&  CD) Xadbc = Echidna::dmd.X64adbc[la][lb][lc][ld];
        else if ( AB && !CD) Xadbc = jxd64NULL;
        else if (!AB &&  CD) Xadbc = Echidna::dmd.X64adac[la] [lc][ld];
        else if (!AB && !CD) Xadbc = jxd64NULL;

        JXD64 Xabba  = Echidna::dmd.X64abba[la][lb];

        JXrot64  Rbc = Echidna::dmd.R64[lb][lc];
        JXrot64 RTad = Echidna::dmd.RT64[la][ld];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dbc = b4blockCPU.Dbc;
            cacheline64 * Fad = b4blockCPU.Fad;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fad, 0, NMs*sAD*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    const double * dDbc = Dnt(tb, tc, nb, nc);
                    for (int mm=0; mm<NMs*sBC; ++mm) Dbc[mm](k) = dDbc[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JB*JC; ++n) Rbc(Dbc + n*(MB*MC), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAD = (ja*JD + jd)*(MA*MD);
                                uint32_t oBC = (jb*JC + jc)*(MB*MC);

                                if (ja*JB+jb > jc*JD+jd)
                                    for (int n=0; n<NMs; ++n) Xadbc(Fad + n*sAD + oAD, t8, Dbc + n*sBC + oBC);
                                else if (ja==jc && jb==jd)
                                    // note that using Dad instead of Dbc works fine for
                                    // symmetric contractions but not for antisymmetric, as it flips the sign!
                                    // actually, Xabab was written taking into account the index permutation,
                                    // but the simplification with Xbaba was exclusively dependent on the symmetry of D
                                    // (best 100 hours ever)
                                    //Xabab(Fad + oAD, t8, Dad + oAD);
                                    for (int n=0; n<NMs; ++n) Xabba(Fad + n*sAD + oAD, t8, Dbc + n*sBC + oBC);

                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JA*JD; ++n) RTad(Fad + n*(MA*MD), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tad = (uint64_t(ta)<<32) + uint64_t(td);
                    double * pF = Fmap[tad];

                    for (int mm=0; mm<NMs*sAD; ++mm) pF[mm] += Fad[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_ad = Xnt(ea,ed,na,nd);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t ta = (it->first>>32);
            uint32_t td = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFad =  Xsub_ad(ta, td);
            for (int mm=0; mm<NMs*sAD; ++mm) dFad[mm] += w*pF[mm];
        }

    }

    if (AB && CD) {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tbc = (uint64_t(tb)<<32) + uint64_t(tc);
                    Fmap[tbc] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sBC;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sBC*sizeof(double));
        }

        // select functions
        JXD64 Xbcad;

        if      ( AB &&  CD) Xbcad = Echidna::dmd.X64bcad[la][lb][lc][ld];
        else if ( AB && !CD) Xbcad = jxd64NULL;
        else if (!AB &&  CD) Xbcad = jxd64NULL;
        else if (!AB && !CD) Xbcad = jxd64NULL;

        JXD64 Xbaab = Echidna::dmd.X64baab[la][lb];

        JXrot64 Rad  = Echidna::dmd.R64[la][ld];
        JXrot64 RTbc = Echidna::dmd.RT64[lb][lc];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dad = b4blockCPU.Dad;
            cacheline64 * Fbc = b4blockCPU.Fbc;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fbc, 0, NMs*sBC*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    const double * dDad = Dnt(ta, td, na, nd);
                    for (int mm=0; mm<NMs*sAD; ++mm) Dad[mm](k) = dDad[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JA*JD; ++n) Rad(Dad + n*(MA*MD), &RMV);

                // digestion (contraction)
                // ***********************

                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAD = (ja*JD + jd)*(MA*MD);
                                uint32_t oBC = (jb*JC + jc)*(MB*MC);

                                if (ja*JB+jb > jc*JD+jd)
                                    for (int n=0; n<NMs; ++n) Xbcad(Fbc + n*sBC + oBC, t8, Dad + n*sAD + oAD);
                                else if (ja==jc && jb==jd)
                                    // note that using Dad instead of Dbc works fine for
                                    // symmetric contractions but not for antisymmetric, as it flips the sign!
                                    // actually, Xabab was written taking into account the index permutation,
                                    // but the simplification with Xbaba was exclusively dependent on the symmetry of D
                                    // (best 100 hours ever)
                                    //Xabab(Fad + oAD, t8, Dad + oAD);
                                    for (int n=0; n<NMs; ++n) Xbaab(Fbc + n*sBC + oBC, t8, Dad + n*sAD + oAD);

                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JB*JC; ++n) RTbc(Fbc + n*(MB*MC), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    uint64_t tbc = (uint64_t(tb)<<32) + uint64_t(tc);
                    double * pF = Fmap[tbc];

                    for (int mm=0; mm<NMs*sBC; ++mm) pF[mm] += Fbc[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_bc = Xnt(eb,ec,nb,nc);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t tb = (it->first>>32);
            uint32_t tc = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFbc =  Xsub_bc(tb, tc);
            for (int mm=0; mm<NMs*sBC; ++mm) dFbc[mm] += w*pF[mm];
        }

    }

    if (AB) {
        // map all possible atom pair combinations into
        // a portion of the write cache
        map <uint64_t, double*> Fmap; {

            int na2 = 0;

            // list all atom pairs
            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];
                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tbd = (uint64_t(tb)<<32) + uint64_t(td);
                    Fmap[tbd] = NULL;
                }
            }

            map <uint64_t, double*>::iterator it;

            for (it=Fmap.begin(); it!=Fmap.end(); ++it) {
                it->second = b4blockCPU.FFF + na2 * NMs * sBD;
                ++na2;
            }

            memset(b4blockCPU.FFF, 0, na2*NMs*sBD*sizeof(double));
        }

        // select functions
        JXD64 Xbdac;
        if      ( AB &&  CD) Xbdac = Echidna::dmd.X64bdac[la][lb][lc][ld];
        else if ( AB && !CD) Xbdac = Echidna::dmd.X64bcac[la][lb] [lc];
        else if (!AB &&  CD) Xbdac = jxd64NULL;
        else if (!AB && !CD) Xbdac = jxd64NULL;

        JXD64 Xbbaa = Echidna::dmd.X64bbaa[la][lb];

        JXrot64 Rac  = Echidna::dmd.R64[la][lc];
        JXrot64 RTbd = Echidna::dmd.RT64[lb][ld];

        //add Exchange interaction (symmetric component)
        {
            cacheline64        * t8  = b4blockCPU.T8d;
            RotationMatrices64 * rm8 = b4blockCPU.RM8d;

            cacheline64 * Dac = b4blockCPU.Dac;
            cacheline64 * Fbd = b4blockCPU.Fbd;

            for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
                if (TheBatch.TileList64[i].useX == 0) continue;

                RotationMatrices64 & RMV = rm8[i];

                //compute offsets, digest
                memset(Fbd, 0, NMs*sBD*sizeof(cacheline64));

                // set density matrix blocks
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t ta = TheBatch.SP12[ap12].ata;
                    uint32_t tc = TheBatch.SP34[ap34].ata;

                    const double * dDac = Dnt(ta, tc, na, nc);
                    for (int mm=0; mm<NMs*sAC; ++mm) Dac[mm](k) = dDac[mm];
                }

                // rotate density matrices
                for (int n=0; n<NMs*JA*JC; ++n) Rac(Dac + n*(MA*MC), &RMV);

                // digestion (contraction)
                // ***********************
                for (int ja=0; ja<JA; ++ja) {
                    for (int jb=0; jb<JB; ++jb) {
                        for (int jc=0; jc<JC; ++jc) {
                            for (int jd=0; jd<JD; ++jd) {
                                uint32_t oAC = (ja*JC + jc)*(MA*MC);
                                uint32_t oBD = (jb*JD + jd)*(MB*MD);

                                //these contract to the (block)diagonal
                                if (ja*JB+jb > jc*JD+jd)
                                    for (int n=0; n<NMs; ++n) Xbdac(Fbd + n*sBD + oBD, t8, Dac + n*sAC + oAC);
                                else if (ja==jc && jb==jd)
                                    for (int n=0; n<NMs; ++n) Xbbaa(Fbd + n*sBD + oBD, t8, Dac + n*sAC + oAC); // bbaa is just half the previous result

                                t8 += TheBatch.wsize;
                            }
                        }
                    }
                }

                // rotate fock matrices
                for (int n=0; n<NMs*JB*JD; ++n) RTbd(Fbd + n*(MB*MD), &RMV);

                // update fock matrix
                for (uint32_t k=0; k<DPC; ++k) {
                    if ((TheBatch.TileList64[i].useX & (1<<k)) == 0) continue;

                    uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                    uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                    uint32_t tb = TheBatch.SP12[ap12].atb;
                    uint32_t td = TheBatch.SP34[ap34].atb;

                    uint64_t tbd = (uint64_t(tb)<<32) + uint64_t(td);
                    double * pF = Fmap[tbd];

                    for (int mm=0; mm<NMs*sBD; ++mm) pF[mm] += Fbd[mm](k);
                }
            }

        }

        // update
        NTsub Xsub_bd = Xnt(eb,ed,nb,nd);

        map <uint64_t, double*>::const_iterator it;

        for (it=Fmap.begin(); it!=Fmap.end(); ++it) {

            uint32_t tb = (it->first>>32);
            uint32_t td = uint32_t(it->first);

            const double * pF = it->second;

            // this locks the handle, which is released when xArray goes out of scope
            xArray dFbd =  Xsub_bd(tb, td);
            for (int mm=0; mm<NMs*sBD; ++mm) dFbd[mm] += w*pF[mm];
        }

    }

}



#include "linear/tensors.hpp"

static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline int Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}

// distributes the integrals but only for 2 center AACC;
void BatchEvaluator::DistributeWs        (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP) {


    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    int sAB = JA*JB*MA*MB;
    int sCD = JC*JD*MC*MD;



    cacheline64        * t8  = b4blockCPU.T8d;
    RotationMatrices64 * rm8 = b4blockCPU.RM8d;


    for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
        if (TheBatch.TileList64[i].useJ == 0) continue;

        RotationMatrices64 & RMV = rm8[i];

        // rotate block
        // ************


        int fa = TheBatch.ABp->f1;
        for (int ja=0; ja<JA; ++ja) {
            int fb = TheBatch.ABp->f2;
            for (int jb=0; jb<JB; ++jb) {
                int fc = TheBatch.CDp->f1;
                for (int jc=0; jc<JC; ++jc) {
                    int fd = TheBatch.CDp->f2;
                    for (int jd=0; jd<JD; ++jd) {

                        Mrot4 (t8, &RMV, la,lb,lc,ld);  // rotate tensor reference frame

                        for (uint32_t k=0; k<DPC; ++k) {
                            if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                            const uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                            const uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                            // ta == tb && tc==td
                            const uint32_t ta = TheBatch.SP12[ap12].ata;
                            const uint32_t tc = TheBatch.SP34[ap34].ata;


                            for (int ma=0; ma<MA; ++ma) {
                                for (int mb=0; mb<MB; ++mb) {
                                    for (int mc=0; mc<MC; ++mc) {
                                        for (int md=0; md<MD; ++md) {

                                            const int a = fa + ma;
                                            const int b = fb + mb;
                                            const int c = fc + mc;
                                            const int d = fd + md;

                                            const int mABCD = Tpos(MA,MB,MC,MD,ma,mb,mc,md);

                                            W2(ta,tc) (a,b, c,d) =
                                            W2(ta,tc) (a,b, d,c) =
                                            W2(ta,tc) (b,a, c,d) =
                                            W2(ta,tc) (b,a, d,c) =
                                            W2(tc,ta) (c,d, a,b) =
                                            W2(tc,ta) (c,d, b,a) =
                                            W2(tc,ta) (d,c, a,b) =
                                            W2(tc,ta) (d,c, b,a) =
                                            t8[mABCD](k);
                                        }
                                    }
                                }
                            }
                        }

                        t8 += TheBatch.wsize;

                        fd += MD;
                    }
                    fc += MC;
                }
                fb += MB;
            }
            fa += MA;
        }

    }

}

// distribute the diagonal ABAB blocks
void BatchEvaluator::DistributeDs        (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP) {


    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    int sAB = JA*JB*MA*MB;
    int sCD = JC*JD*MC*MD;

    cacheline64        * t8  = b4blockCPU.T8d;
    RotationMatrices64 * rm8 = b4blockCPU.RM8d;


    for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
        if (TheBatch.TileList64[i].useJ == 0) continue;

        RotationMatrices64 & RMV = rm8[i];

        // rotate block
        // ************


        int fa = TheBatch.ABp->f1;
        for (int ja=0; ja<JA; ++ja) {
            int fb = TheBatch.ABp->f2;
            for (int jb=0; jb<JB; ++jb) {
                int fc = TheBatch.CDp->f1;
                for (int jc=0; jc<JC; ++jc) {
                    int fd = TheBatch.CDp->f2;
                    for (int jd=0; jd<JD; ++jd) {

                        Mrot4 (t8, &RMV, la,lb,lc,ld);  // rotate tensor reference frame

                        for (uint32_t k=0; k<DPC; ++k) {
                            if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                            const uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                            const uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                            const uint32_t ta = TheBatch.SP12[ap12].ata;
                            const uint32_t tb = TheBatch.SP12[ap12].atb;
                            const uint32_t tc = TheBatch.SP34[ap34].ata;
                            const uint32_t td = TheBatch.SP34[ap34].atb;


                            for (int ma=0; ma<MA; ++ma) {
                                for (int mb=0; mb<MB; ++mb) {
                                    for (int mc=0; mc<MC; ++mc) {
                                        for (int md=0; md<MD; ++md) {

                                            const int a = fa + ma;
                                            const int b = fb + mb;
                                            const int c = fc + mc;
                                            const int d = fd + md;

                                            const int mABCD = Tpos(MA,MB,MC,MD,ma,mb,mc,md);

                                            if      ((ta==tc)&&(tb==td))
                                                W2(ta,tb) (a,b, c,d) = W2(ta,tb) (c,d, a,b) = W2(tb,ta) (b,a, d,c) = W2(tb,ta) (d,c, b,a) = t8[mABCD](k);
                                            else if ((ta==td)&&(tb==tc))
                                                W2(ta,tb) (a,b, d,c) = W2(ta,tb) (d,c, a,b) = W2(tb,ta) (b,a, c,d) = W2(tb,ta) (c,d, b,a) = t8[mABCD](k);
                                        }
                                    }
                                }
                            }
                        }

                        t8 += TheBatch.wsize;

                        fd += MD;
                    }
                    fc += MC;
                }
                fb += MB;
            }
            fa += MA;
        }

    }

}


// distributes AACD
void BatchEvaluator::DistributeWs        (const ERIBatch & TheBatch, r1tensor<tensor4> & W3,           const sparsetensorpattern * STP) {


    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    cacheline64        * t8  = b4blockCPU.T8d;
    RotationMatrices64 * rm8 = b4blockCPU.RM8d;


    for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
        if (TheBatch.TileList64[i].useJ == 0) continue;

        RotationMatrices64 & RMV = rm8[i];

        // rotate block
        // ************


        int fa = TheBatch.ABp->f1;
        for (int ja=0; ja<JA; ++ja) {
            int fb = TheBatch.ABp->f2;
            for (int jb=0; jb<JB; ++jb) {
                int fc = TheBatch.CDp->f1;
                for (int jc=0; jc<JC; ++jc) {
                    int fd = TheBatch.CDp->f2;
                    for (int jd=0; jd<JD; ++jd) {

                        Mrot4 (t8, &RMV, la,lb,lc,ld);  // rotate tensor reference frame

                        for (uint32_t k=0; k<DPC; ++k) {
                            if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                            uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                            uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                            uint32_t ta = TheBatch.SP12[ap12].ata;
                            uint32_t tc = TheBatch.SP34[ap34].ata;
                            uint32_t td = TheBatch.SP34[ap34].atb;


                            for (int ma=0; ma<MA; ++ma) {
                                for (int mb=0; mb<MB; ++mb) {
                                    for (int mc=0; mc<MC; ++mc) {
                                        for (int md=0; md<MD; ++md) {

                                            const int a = fa + ma;
                                            const int b = fb + mb;

                                            const int c = STP->GetOffset1(tc) + fc + mc;
                                            const int d = STP->GetOffset1(td) + fd + md;

                                            const int mABCD = Tpos(MA,MB,MC,MD,ma,mb,mc,md);

                                            W3(ta) (a,b, c,d) = W3(ta)(a,b, d,c) = W3(ta)(b,a, c,d) = W3(ta)(b,a, d,c) = t8[mABCD](k);
                                        }
                                    }
                                }
                            }
                        }

                        t8 += TheBatch.wsize;

                        fd += MD;
                    }
                    fc += MC;
                }
                fb += MB;
            }
            fa += MA;
        }

    }

}

// distributes everything
void BatchEvaluator::DistributeWs        (const ERIBatch & TheBatch, tensor4 & W, const sparsetensorpattern * STP) {


    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    int sAB = JA*JB*MA*MB;
    int sCD = JC*JD*MC*MD;

    /*
    int wA = sparsetensorpattern::alen[sparsetensorpattern::ids[TheBatch.SP12[0].ata]];
    int wB = sparsetensorpattern::alen[sparsetensorpattern::ids[TheBatch.SP12[0].atb]];
    int wC = sparsetensorpattern::alen[sparsetensorpattern::ids[TheBatch.SP34[0].ata]];
    int wD = sparsetensorpattern::alen[sparsetensorpattern::ids[TheBatch.SP34[0].atb]];
    */


    cacheline64        * t8  = b4blockCPU.T8d;
    RotationMatrices64 * rm8 = b4blockCPU.RM8d;


    for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
        if (TheBatch.TileList64[i].useJ == 0) continue;

        RotationMatrices64 & RMV = rm8[i];

        // rotate block
        // ************


        int fa = TheBatch.ABp->f1;
        for (int ja=0; ja<JA; ++ja) {
            int fb = TheBatch.ABp->f2;
            for (int jb=0; jb<JB; ++jb) {
                int fc = TheBatch.CDp->f1;
                for (int jc=0; jc<JC; ++jc) {
                    int fd = TheBatch.CDp->f2;
                    for (int jd=0; jd<JD; ++jd) {

                        Mrot4 (t8, &RMV, la,lb,lc,ld);  // rotate tensor reference frame

                        for (uint32_t k=0; k<DPC; ++k) {
                            if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                            uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                            uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                            uint32_t ta = TheBatch.SP12[ap12].ata;
                            uint32_t tb = TheBatch.SP12[ap12].atb;
                            uint32_t tc = TheBatch.SP34[ap34].ata;
                            uint32_t td = TheBatch.SP34[ap34].atb;

                            /*
                            int sAB = sparsetensorpattern::GetOffset(ta,tb);
                            int sBA = sparsetensorpattern::GetOffset(tb,ta);
                            int sCD = sparsetensorpattern::GetOffset(tc,td);
                            int sDC = sparsetensorpattern::GetOffset(td,tc);
                            */


                            for (int ma=0; ma<MA; ++ma) {
                                for (int mb=0; mb<MB; ++mb) {
                                    for (int mc=0; mc<MC; ++mc) {
                                        for (int md=0; md<MD; ++md) {

                                            int a = STP->GetOffset1(ta) + fa + ma;
                                            int b = STP->GetOffset1(tb) + fb + mb;
                                            int c = STP->GetOffset1(tc) + fc + mc;
                                            int d = STP->GetOffset1(td) + fd + md;

                                            // cout << a << " " << b << " " << c << " " << d << endl;

                                            /*
                                            int pAB = sAB + Tpos (wA, wB, faa, fbb);
                                            int pCD = sCD + Tpos (wC, wD, fcc, fdd);
                                            int pBA = sBA + Tpos (wB, wA, fbb, faa);
                                            int pDC = sDC + Tpos (wD, wC, fdd, fcc);
                                            */

                                            int mABCD = Tpos(MA,MB,MC,MD,ma,mb,mc,md);

                                            // distribute in the tensor
                                            //W (pAB,pCD) = W (pAB,pDC) = W (pBA,pCD) = W (pBA,pDC) =
                                            //W (pCD,pAB) = W (pDC,pAB) = W (pCD,pBA) = W (pDC,pBA) = t8[mABCD](k);
                                            /*
                                            int pab = faa*nbf + fbb;
                                            int pba = fbb*nbf + faa;
                                            int pcd = fcc*nbf + fdd;
                                            int pdc = fdd*nbf + fcc;

                                            W (pab,pcd) = W (pab,pdc) = W (pba,pcd) = W (pba,pdc) =
                                            W (pcd,pab) = W (pdc,pab) = W (pcd,pba) = W (pdc,pba) = t8[mABCD](k);
                                            */

                                            W(a,b,c,d) = W(a,b,d,c) = W(b,a,c,d) = W(b,a,d,c) =
                                            W(c,d,a,b) = W(c,d,b,a) = W(d,c,a,b) = W(d,c,b,a) = t8[mABCD](k);
                                        }
                                    }
                                }
                            }
                        }

                        t8 += TheBatch.wsize;

                        fd += MD;
                    }
                    fc += MC;
                }
                fb += MB;
            }
            fa += MA;
        }

    }



}

void BatchEvaluator::DistributeDs        (const ERIBatch & TheBatch, tensor2 & D, const sparsetensorpattern * STP) {


    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;

    int sAB = JA*JB*MA*MB;


    cacheline64        * t8  = b4blockCPU.T8d;
    RotationMatrices64 * rm8 = b4blockCPU.RM8d;


    for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
        if (TheBatch.TileList64[i].useJ == 0) continue;

        RotationMatrices64 & RMV = rm8[i];

        // rotate block
        // ************

        int fa = TheBatch.ABp->f1;
        for (int ja=0; ja<JA; ++ja) {
            int fb = TheBatch.ABp->f2;
            for (int jb=0; jb<JB; ++jb) {
                int fc = TheBatch.CDp->f1;
                for (int jc=0; jc<JA; ++jc) {
                    int fd = TheBatch.CDp->f2;
                    for (int jd=0; jd<JB; ++jd) {

                        if ((ja==jc)&&(jb==jd)) {
                            Mrot4 (t8, &RMV, la,lb,la,lb);  // rotate tensor reference frame; ignore rest

                            for (uint32_t k=0; k<DPC; ++k) {
                                if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                                uint32_t ap12 = TheBatch.TileList64[i].ap12[k];

                                uint32_t ta = TheBatch.SP12[ap12].ata;
                                uint32_t tb = TheBatch.SP12[ap12].atb;

                                for (int ma=0; ma<MA; ++ma) {
                                    for (int mb=0; mb<MB; ++mb) {

                                        int a = STP->GetOffset1(ta) + fa + ma;
                                        int b = STP->GetOffset1(tb) + fb + mb;

                                        int mABCD = Tpos(MA,MB,MA,MB,ma,mb,ma,mb);

                                        D(a,b) = D(b,a) = t8[mABCD](k);
                                    }
                                }
                            }

                        }

                        t8 += TheBatch.wsize;

                        fd += MB;
                    }
                    fc += MA;
                }
                fb += MB;
            }
            fa += MA;
        }

    }



}


void BatchEvaluator::DistributeCholesky  (const ERIBatch & TheBatch, tensor2 & W2, const sparsetensorpattern * STP) {


    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    cacheline64        * t8  = b4blockCPU.T8d;
    RotationMatrices64 * rm8 = b4blockCPU.RM8d;


    for (uint32_t i=0; i<TheBatch.Ntiles64; ++i) {
        if (TheBatch.TileList64[i].useJ == 0) continue;

        RotationMatrices64 & RMV = rm8[i];

        // rotate block
        // ************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        Mrot4 (t8, &RMV, la,lb,lc,ld);  // rotate tensor reference frame

                        for (uint32_t k=0; k<DPC; ++k) {
                            if ((TheBatch.TileList64[i].useJ & (1<<k)) == 0) continue;

                            const uint32_t ap12 = TheBatch.TileList64[i].ap12[k];
                            const uint32_t ap34 = TheBatch.TileList64[i].ap34[k];

                            const uint32_t osab = TheBatch.SP12[ap12].cdos;
                            const uint32_t oscd = TheBatch.SP34[ap34].cdos;


                            for (int ma=0; ma<MA; ++ma) {
                                for (int mb=0; mb<MB; ++mb) {
                                    for (int mc=0; mc<MC; ++mc) {
                                        for (int md=0; md<MD; ++md) {

                                            const uint32_t a = ja*MA + ma;
                                            const uint32_t b = jb*MB + mb;

                                            const uint32_t c = jc*MC + mc;
                                            const uint32_t d = jd*MD + md;

                                            const uint32_t mABCD = Tpos(MA,MB,MC,MD,ma,mb,mc,md);


                                            const uint32_t ab = osab + a*JB*MB + b;
                                            const uint32_t cd = oscd + c*JD*MD + d;

                                            W2(ab,cd) = W2(cd,ab) = t8[mABCD](k);

                                        }
                                    }
                                }
                            }
                        }

                        t8 += TheBatch.wsize;
                    }
                }
            }
        }



    }

}



#include "math/angular.hpp"
#include "linear/eigen.hpp"
#include "linear/tensors.hpp"
#include "EFS/fock2e.hpp"

//find the spectral norm of the 2e-tensor block
cacheline64 BatchEvaluator::ERItrace(const ERIBatch & TheBatch, const ShellPair & ABs, const ERIgeometries64 & eriG8, const LibQuimera::ERITile64 & ET, uint8_t lenk) {

    TheBatch.ERIalgorithm->K4(eriG8, ET, *TheBatch.ABp, *TheBatch.ABp, b4blockCPU.I8d, b4ERIs);

    cacheline64 * i8 = b4blockCPU.I8d;
    cacheline64 * t8 = b4blockCPU.T8d;

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;

    int nmLa = nmS[ABs.getLa()];
    int nmLb = nmS[ABs.getLb()];

    cacheline64 trace;
    trace.set(0);

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {

            TheBatch.ERIalgorithm->MIRROR(i8, eriG8, t8, b4ERIs);

            for (int i=0; i<nmLa; ++i) {
                for (int j=0; j<nmLb; ++j) {
                    int ijij = (((j*nmLa+i)*nmLb+j)*nmLa+i);
                    trace += t8[ijij];
                }
            }

            // only diagonal blocks needed for the trace
            t8 += TheBatch.wsize * (JA*JB+1);
            i8 += TheBatch.msize * (JA*JB+1);
        }
    }

    return trace;
}

double BatchEvaluator::ERItrace(const ERIBatch & TheBatch, const ShellPair & ABs, const ERIgeometry & eriG) {

    double * i0 = (double*)b4blockCPU.I8d;
    double * t0 = (double*)b4blockCPU.T8d;

    TheBatch.ERIalgorithm->K4(*TheBatch.ABp, *TheBatch.CDp, i0, b4ERIs);

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;

    int nmLa = nmS[ABs.getLa()];
    int nmLb = nmS[ABs.getLb()];

    double trace = 0;

    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {

            TheBatch.ERIalgorithm->MIRROR(i0, t0, b4ERIs);

            for (int i=0; i<nmLa; ++i) {
                for (int j=0; j<nmLb; ++j) {
                    int ijij = (((j*nmLa+i)*nmLb+j)*nmLa+i);
                    trace += t0[ijij];
                }
            }

            // only diagonal blocks needed for the trace
            t0 += TheBatch.wsize * (JA*JB+1);
            i0 += TheBatch.msize * (JA*JB+1);
        }
    }

    return trace;
}





//very inefficient
//the point is that the matrix is constructed only once for each type of atom
void ConstructBigW(const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, int wa, const double * w, double * W) {

    const int maxLa = nmS[ABp.l1];
    const int maxLb = nmS[ABp.l2];
    const int maxLc = nmS[CDp.l1];
    const int maxLd = nmS[CDp.l2];

    int wsize = maxLa * maxLb * maxLc * maxLd;

    const int Ja = ABp.Ja;
    const int Jb = ABp.Jb;
    const int Jc = CDp.Ja;
    const int Jd = CDp.Jb;

    const double * TT = w;

    int fa = ABp.f1;
    for (int ja=0; ja<Ja; ++ja) {
        int fb = ABp.f2;
        for (int jb=0; jb<Jb; ++jb) {
            int fc = CDp.f1;
            for (int jc=0; jc<Jc; ++jc) {
                int fd = CDp.f2;
                for (int jd=0; jd<Jd; ++jd) {

                    for (int ma=0; ma<maxLa; ++ma) {
                        for (int mb=0; mb<maxLb; ++mb) {
                            for (int mc=0; mc<maxLc; ++mc) {
                                for (int md=0; md<maxLd; ++md) {
                                    int posw = Tpos (maxLa, maxLb, maxLc, maxLd, ma, mb, mc, md);

                                    int faa, fbb, fcc, fdd;

                                    if (ABp.inverted) {faa = fa+ma; fbb = fb+mb;}
                                    else              {faa = fb+mb; fbb = fa+ma;}

                                    if (CDp.inverted) {fcc = fd+md; fdd = fc+mc;}
                                    else              {fcc = fc+mc; fdd = fd+md;}

                                    int posW1 = Tpos (wa, wa, wa, wa, faa, fbb, fcc, fdd);
                                    int posW2 = Tpos (wa, wa, wa, wa, fcc, fdd, faa, fbb);

                                    int posW3 = Tpos (wa, wa, wa, wa, fbb, faa, fcc, fdd);
                                    int posW4 = Tpos (wa, wa, wa, wa, fcc, fdd, fbb, faa);

                                    int posW5 = Tpos (wa, wa, wa, wa, faa, fbb, fdd, fcc);
                                    int posW6 = Tpos (wa, wa, wa, wa, fdd, fcc, faa, fbb);

                                    int posW7 = Tpos (wa, wa, wa, wa, fbb, faa, fdd, fcc);
                                    int posW8 = Tpos (wa, wa, wa, wa, fdd, fcc, fbb, faa);

                                    // scaled factor (for some functionals)
                                    double R = TT[posw]; // *s

                                    W[posW1] = R;
                                    W[posW2] = R;
                                    W[posW3] = R;
                                    W[posW4] = R;
                                    W[posW5] = R;
                                    W[posW6] = R;
                                    W[posW7] = R;
                                    W[posW8] = R;
                                }
                            }
                        }
                    }

                    TT += wsize;

                    fd += maxLd;
                }
                fc += maxLc;
            }
            fb += maxLb;
        }
        fa += maxLa;
    }
}

void BatchEvaluator::ConstructW(const ERIBatch & TheBatch, const ERIgeometry & eriG, double * WWW, uint32_t wa) {

    double * I0 = (double*)b4blockCPU.I8d;
    double * T0 = (double*)b4blockCPU.T8d;

    TheBatch.ERIalgorithm->K4   (*TheBatch.ABp, *TheBatch.CDp, I0, b4ERIs);

    double * II = I0;
    double * TT = T0;

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;


    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {
                    TheBatch.ERIalgorithm->MIRROR(II, TT, b4ERIs);

                    II += TheBatch.msize;
                    TT += TheBatch.wsize;
                }
            }
        }
    }
    ConstructBigW(*TheBatch.ABp, *TheBatch.CDp, wa, T0, WWW);
}

void BatchEvaluator::ConstructW(const ERIBatch & TheBatch, const ERIgeometry & eriG, double * WWW, uint32_t wa, double alpha, double beta, double w2) {

    double * I0 = (double*)b4blockCPU.I8d;
    double * T0 = (double*)b4blockCPU.T8d;
    double * I1 = I0 + TheBatch.msize4;

    TheBatch.ERIalgorithm->K4   (*TheBatch.ABp, *TheBatch.CDp, I0, b4ERIs);      // regular exchange integral
    TheBatch.ERIalgorithm->K4SR (*TheBatch.ABp, *TheBatch.CDp, I1, b4ERIs, w2);  // short-range integral

    for (int k=0; k<TheBatch.msize4; ++k) I0[k] *= (alpha+beta); // scale normal contribution
    for (int k=0; k<TheBatch.msize4; ++k) I1[k] *= -beta;        // scale short range contribution
    for (int k=0; k<TheBatch.msize4; ++k) I0[k] += I1[k];        // add the two contributions


    double * II = I0;
    double * TT = T0;

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;


    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {
                    TheBatch.ERIalgorithm->MIRROR(II, TT, b4ERIs);

                    II += TheBatch.msize;
                    TT += TheBatch.wsize;
                }
            }
        }
    }
    ConstructBigW(*TheBatch.ABp, *TheBatch.CDp, wa, T0, WWW);
}


void ConstructBigW(const ShellPairPrototype & ABp, int wa, const double * w, double * W) {

    const int maxLa = nmS[ABp.l1];
    const int maxLb = nmS[ABp.l2];

    int wsize = maxLa * maxLb * maxLa * maxLb;

    const int Ja = ABp.Ja;
    const int Jb = ABp.Jb;

    const double * TT = w;

    int fa = ABp.f1;
    for (int ja=0; ja<Ja; ++ja) {
        int fb = ABp.f2;
        for (int jb=0; jb<Jb; ++jb) {

            for (int ma=0; ma<maxLa; ++ma) {
                for (int mb=0; mb<maxLb; ++mb) {

                    int posw = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, ma, mb);

                    int faa = fa+ma;
                    int fbb = fb+mb;

                    // scaled factor (for some functionals)
                    double R = TT[posw]; // *s

                    W[faa*wa + fbb] = R;
                    W[fbb*wa + faa] = R;
                }
            }


            TT += wsize;

            fb += maxLb;
        }
        fa += maxLa;
    }

}

// construct element diagonal
void BatchEvaluator::ConstructDiagonal(const ERIBatch & TheBatch, const ERIgeometry & eriG, double * WWW, uint32_t wa) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;

    const int maxLa = nmS[TheBatch.ABp->l1];
    const int maxLb = nmS[TheBatch.ABp->l2];

    const int wsize = maxLa * maxLb * maxLa * maxLb;


    // compute the ERI batch
    // =====================

    {
        double * I0 = (double*)b4blockCPU.I8d;
        double * T0 = (double*)b4blockCPU.T8d;

        TheBatch.ERIalgorithm->K4   (*TheBatch.ABp, *TheBatch.ABp, I0, b4ERIs);

        double * II = I0;
        double * TT = T0;


        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JA; ++jc) {
                    for (int jd=0; jd<JB; ++jd) {

                        if ((ja==jc)&&(jb==jd)) {
                            TheBatch.ERIalgorithm->MIRROR(II, TT, b4ERIs);
                            TT += TheBatch.wsize; // concatenate the (ja jb ja jb) and discard the rest
                        }

                        II += TheBatch.msize; // advance the batch of kernels
                    }
                }
            }
        }
    }


    // now fill the (ij|ij) diagonal
    // =============================

    //ConstructBigW(*TheBatch.ABp, wa, T0, WWW);
    {
        const double * TT = (double*)b4blockCPU.T8d;

        int fa = TheBatch.ABp->f1;
        for (int ja=0; ja<JA; ++ja) {
            int fb = TheBatch.ABp->f2;
            for (int jb=0; jb<JB; ++jb) {

                for (int ma=0; ma<maxLa; ++ma) {
                    for (int mb=0; mb<maxLb; ++mb) {

                        // find the element we want
                        const int posw = Tpos (maxLa, maxLb, maxLa, maxLb, ma, mb, ma, mb);

                        const int faa = fa+ma;
                        const int fbb = fb+mb;

                        // the element appears twice due to symmetry
                        WWW[faa*wa + fbb] = WWW[fbb*wa + faa] = TT[posw];
                    }
                }

                TT += wsize;

                fb += maxLb;
            }
            fa += maxLa;
        }
    }

}

/*

    WHY IS THIS PART OF FOCK2E?

*/

void Fock2e::AddCoulomb(Sparse & F, int at, int wa, const double * const T, const Sparse & D) {

    const sparsetensorpattern & P = *D.P;


    uint16_t id = P.ids[at];

    //loop over shells
    for (int b1=0, ma=0; b1<P.nfs[id]; ++b1) {
        uint32_t mj1 = P.js[id][b1];
        uint32_t mm1 = P.ms[id][b1];

        for (uint8_t j1=0; j1<mj1; ++j1) {
            for (uint8_t m1=0; m1<mm1; ++m1, ++ma) {


                for (int b2=0, mb=0; b2<P.nfs[id]; ++b2) {
                    uint32_t mj2 = P.js[id][b2];
                    uint32_t mm2 = P.ms[id][b2];

                    for (uint8_t j2=0; j2<mj2; ++j2) {
                        for (uint8_t m2=0; m2<mm2; ++m2, ++mb) {
                            //if (mb>ma) continue;

                            double sum = 0;
                            int pos = (ma*wa+mb)*wa*wa;


                            for (int b3=0, mc=0; b3<P.nfs[id]; ++b3) {
                                uint32_t mj3 = P.js[id][b3];
                                uint32_t mm3 = P.ms[id][b3];

                                for (uint8_t j3=0; j3<mj3; ++j3) {
                                    for (uint8_t m3=0; m3<mm3; ++m3, ++mc) {


                                        for (int b4=0, md=0; b4<P.nfs[id]; ++b4) {
                                            uint32_t mj4 = P.js[id][b4];
                                            uint32_t mm4 = P.ms[id][b4];

                                            for (uint8_t j4=0; j4<mj4; ++j4) {
                                                for (uint8_t m4=0; m4<mm4; ++m4, ++md) {

                                                    sum += T[pos] * D(at,at, b3,b4, j3,j4, m3,m4);

                                                    ++pos;
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                            //if (ma==mb) sum*= 0.5;

                            F(at,at, b1,b2, j1,j2, m1,m2) += 0.5*sum;
                        }
                    }
                }

            }
        }
    }

}

void Fock2e::AddXchange(Sparse & F, int at, int wa, const double * const T, const Sparse & D) {

    const sparsetensorpattern & P = *D.P;


    uint16_t id = P.ids[at];

    //loop over shells
    int ma = 0;
    for (int b1=0; b1<P.nfs[id]; ++b1) {
        uint32_t mj1 = P.js[id][b1];
        uint32_t mm1 = P.ms[id][b1];

        for (uint8_t j1=0; j1<mj1; ++j1) {
            for (uint8_t m1=0; m1<mm1; ++m1, ++ma) {

                int mc = 0;
                for (int b3=0; b3<P.nfs[id]; ++b3) {
                    uint32_t mj3 = P.js[id][b3];
                    uint32_t mm3 = P.ms[id][b3];

                    for (uint8_t j3=0; j3<mj3; ++j3) {
                        for (uint8_t m3=0; m3<mm3; ++m3, ++mc) {

                            //if (mc>ma) continue;


                            double sum = 0;
                            int pos = (ma*wa*wa + mc)*wa;

                            int mb = 0;

                            for (int b2=0; b2<P.nfs[id]; ++b2) {
                                uint32_t mj2 = P.js[id][b2];
                                uint32_t mm2 = P.ms[id][b2];

                                for (uint8_t j2=0; j2<mj2; ++j2) {
                                    for (uint8_t m2=0; m2<mm2; ++m2, ++mb) {

                                        int pos2 = pos + mb*wa*wa;

                                        int md = 0;

                                        for (int b4=0; b4<P.nfs[id]; ++b4) {
                                            uint32_t mj4 = P.js[id][b4];
                                            uint32_t mm4 = P.ms[id][b4];

                                            for (uint8_t j4=0; j4<mj4; ++j4) {
                                                for (uint8_t m4=0; m4<mm4; ++m4, ++md) {

                                                    sum += T[pos2] * D(at,at, b2,b4, j2,j4, m2,m4);

                                                    ++pos2;
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                            //if (ma==mc) sum*= 0.5;

                            F(at,at, b1,b3, j1,j3, m1,m3) += 0.5*sum;
                        }
                    }
                }

            }
        }
    }

}





void BatchEvaluator::AAAA (const ERIBatch & TheBatch, const ERIgeometry & eriG) {

    double * I0 = (double*)b4blockCPU.I8d;
    double * T0 = (double*)b4blockCPU.T8d;

    TheBatch.ERIalgorithm->K4   (*TheBatch.ABp, *TheBatch.CDp, I0, b4ERIs);

    double * II = I0;
    double * TT = T0;

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;


    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {
                    TheBatch.ERIalgorithm->MIRROR(II, TT, b4ERIs);

                    II += TheBatch.msize;
                    TT += TheBatch.wsize;
                }
            }
        }
    }

}

void BatchEvaluator::AAAA (const ERIBatch & TheBatch, const ERIgeometry & eriG, double alpha, double beta, double w2) {

    double * I0 = (double*)b4blockCPU.I8d;
    double * T0 = (double*)b4blockCPU.T8d;
    double * I1 = I0 + TheBatch.msize4;

    TheBatch.ERIalgorithm->K4   (*TheBatch.ABp, *TheBatch.CDp, I0, b4ERIs);      // regular exchange integral
    TheBatch.ERIalgorithm->K4SR (*TheBatch.ABp, *TheBatch.CDp, I1, b4ERIs, w2);  // short-range integral

    for (int k=0; k<TheBatch.msize4; ++k) I0[k] *= (alpha+beta); // scale normal contribution
    for (int k=0; k<TheBatch.msize4; ++k) I1[k] *= -beta;        // scale short range contribution
    for (int k=0; k<TheBatch.msize4; ++k) I0[k] += I1[k];        // add the two contributions


    double * II = I0;
    double * TT = T0;

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;


    for (int ja=0; ja<JA; ++ja) {
        for (int jb=0; jb<JB; ++jb) {
            for (int jc=0; jc<JC; ++jc) {
                for (int jd=0; jd<JD; ++jd) {
                    TheBatch.ERIalgorithm->MIRROR(II, TT, b4ERIs);

                    II += TheBatch.msize;
                    TT += TheBatch.wsize;
                }
            }
        }
    }

}


void BatchEvaluator::AddCoulomb   (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs, uint32_t at) {

    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const uint8_t na = TheBatch.ABp->nb1;
    const uint8_t nb = TheBatch.ABp->nb2;
    const uint8_t nc = TheBatch.CDp->nb1;
    const uint8_t nd = TheBatch.CDp->nb2;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const ConstNpattern CNP = Jnt.GetPattern();
    const uint8_t eat = CNP.GetID(at);


    //CONTRACT WITH DENSITY MATRIX
    {
        double * TT = (double*)b4blockCPU.T8d;

        double * Fab = (double*)b4blockCPU.Fab;
        memset(Fab, 0, NMs*sAB*sizeof(double));
        const double * dDcd = Dnt(at, at, nc, nd);


        // digestion (contraction)
        // ***********************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        uint32_t oAB = (ja*JB + jb)*(MA*MB);
                        uint32_t oCD = (jc*JD + jd)*(MC*MD);

                        for (int n=0; n<NMs; ++n) {

                            const double * DD = dDcd + n*sCD + oCD;
                            double       * FF =  Fab + n*sAB + oAB;

                            for (int ma=0; ma<MA; ++ma) {
                                for (int mb=0; mb<MB; ++mb) {
                                    double sum = 0;
                                    for (int mc=0; mc<MC; ++mc) {
                                        for (int md=0; md<MD; ++md) {
                                            int pos = Tpos (MA, MB, MC, MD, ma, mb, mc, md);
                                            int cd  = Tpos (MC, MD, mc, md);
                                            sum += TT[pos] * DD[cd];
                                        }
                                    }
                                    int ab = Tpos (MA, MB, ma, mb);
                                    FF[ab]+=sum*2;
                                }
                            }

                        }

                        TT += TheBatch.wsize;
                    }
                }
            }
        }

        // scale the coulomb atomic block updates
        {
            double f = 1.;
            if (TheBatch.ABp->samef) f*=0.5; // A==B
            if (TheBatch.CDp->samef) f*=0.5; // C==D
            if (TheBatch.ABp == TheBatch.CDp) f*=0.5; //AB==CD

            for (int mm=0; mm<NMs*sAB; ++mm) Fab[mm]*=f;
        }

        // update fock matrix
        NTsub Jsub_ab = Jnt(eat,eat,na,nb);

        {
            // this locks the handle, which is released when xArray goes out of scope
            xArray dFab =  Jsub_ab(at, at);
            for (int mm=0; mm<NMs*sAB; ++mm) dFab[mm] += Fab[mm];
        }
    }

    {
        double * TT = (double*)b4blockCPU.T8d;

        double * Fcd = (double*)b4blockCPU.Fcd;
        memset(Fcd, 0, NMs*sCD*sizeof(double));
        const double * dDab = Dnt(at, at, na, nb);


        // digestion (contraction)
        // ***********************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        uint32_t oAB = (ja*JB + jb)*(MA*MB);
                        uint32_t oCD = (jc*JD + jd)*(MC*MD);

                        for (int n=0; n<NMs; ++n) {

                            const double * DD = dDab + n*sAB + oAB;
                            double       * FF =  Fcd + n*sCD + oCD;

                            for (int mc=0; mc<MC; ++mc) {
                                for (int md=0; md<MD; ++md) {
                                    double sum = 0;
                                    for (int ma=0; ma<MA; ++ma) {
                                        for (int mb=0; mb<MB; ++mb) {
                                            int pos = Tpos (MA, MB, MC, MD, ma, mb, mc, md);
                                            int ab = Tpos (MA, MB, ma, mb);
                                            sum += TT[pos] * DD[ab];
                                        }
                                    }
                                    int cd = Tpos(MC, MD, mc, md);
                                    FF[cd]+=sum*2;
                                }
                            }

                        }

                        TT += TheBatch.wsize;
                    }
                }
            }
        }

        // scale the coulomb atomic block updates
        {
            double f = 1.;
            if (TheBatch.ABp->samef) f*=0.5; // A==B
            if (TheBatch.CDp->samef) f*=0.5; // C==D
            if (TheBatch.ABp == TheBatch.CDp) f*=0.5; //AB==CD

            for (int mm=0; mm<NMs*sCD; ++mm) Fcd[mm]*=f;
        }

        // update fock matrices
        const ConstNpattern CNP = Jnt.GetPattern();

        const uint8_t eat = CNP.GetID(at);
        NTsub Jsub_cd = Jnt(eat,eat,nc,nd);

        {
            xArray dFcd =  Jsub_cd(at, at);
            for (int mm=0; mm<NMs*sCD; ++mm) dFcd[mm] += Fcd[mm];
        }
    }

}

void BatchEvaluator::AddExchange  (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Xnt, int NMs, uint32_t at) {

    const uint8_t JA = TheBatch.ABp->Ja;
    const uint8_t JB = TheBatch.ABp->Jb;
    const uint8_t JC = TheBatch.CDp->Ja;
    const uint8_t JD = TheBatch.CDp->Jb;

    const uint8_t la = TheBatch.la;
    const uint8_t lb = TheBatch.lb;
    const uint8_t lc = TheBatch.lc;
    const uint8_t ld = TheBatch.ld;

    const uint8_t MA = 2*TheBatch.la+1;
    const uint8_t MB = 2*TheBatch.lb+1;
    const uint8_t MC = 2*TheBatch.lc+1;
    const uint8_t MD = 2*TheBatch.ld+1;

    const uint8_t na = TheBatch.ABp->nb1;
    const uint8_t nb = TheBatch.ABp->nb2;
    const uint8_t nc = TheBatch.CDp->nb1;
    const uint8_t nd = TheBatch.CDp->nb2;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    const ConstNpattern CNP = Xnt.GetPattern();


    //CONTRACT WITH DENSITY MATRIX
    {
        double * TT = (double*)b4blockCPU.T8d;

        double * Fac = (double*)b4blockCPU.Fac;
        memset(Fac, 0, NMs*sAC*sizeof(double));
        const double * dDbd = Dnt(at, at, nb, nd);

        // digestion (contraction)
        // ***********************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        if ((TheBatch.ABp == TheBatch.CDp) && (ja*JB+jb < jc*JD+jd)) {
                            TT += TheBatch.wsize;
                            continue;
                        }
                        bool half = ((TheBatch.ABp == TheBatch.CDp) && (ja==jc) && (jb==jd));

                        uint32_t oAC = (ja*JC + jc)*(MA*MC);
                        uint32_t oBD = (jb*JD + jd)*(MB*MD);

                        for (int n=0; n<NMs; ++n) {

                            const double * DD = dDbd + n*sBD + oBD;
                            double       * FF =  Fac + n*sAC + oAC;

                            for (int ma=0; ma<MA; ++ma) {
                                for (int mc=0; mc<MC; ++mc) {
                                    double sum = 0;
                                    for (int mb=0; mb<MB; ++mb) {
                                        for (int md=0; md<MD; ++md) {
                                            int pos = Tpos (MA, MB, MC, MD, ma, mb, mc, md);
                                            int bd  = Tpos (MB, MD, mb, md);
                                            sum += TT[pos] * DD[bd];
                                        }
                                    }
                                    int ac = Tpos (MA, MC, ma, mc);
                                    if (half) sum *= 0.5;
                                    FF[ac] += sum;
                                }
                            }
                        }

                        TT += TheBatch.wsize;
                    }
                }
            }
        }

        // scale the atomic block updates
        {
            double f = 1.;
            if (TheBatch.ABp->samef) f*=0.5; // A==B
            if (TheBatch.CDp->samef) f*=0.5; // C==D
            //if (TheBatch.ABp == TheBatch.CDp) f*=0.5; //AB==CD
            for (int mm=0; mm<NMs*sAC; ++mm) Fac[mm]*=f;
        }

        // update fock matrices
        const uint8_t eat = CNP.GetID(at);
        NTsub Xsub_ac = Xnt(eat,eat,na,nc);

        // update fock matrices
        {
            xArray dFac = Xsub_ac(at, at);
            for (int mm=0; mm<NMs*sAC; ++mm) dFac[mm] += Fac[mm];
        }
    }

    {
        double * TT = (double*)b4blockCPU.T8d;

        double * Fad = (double*)b4blockCPU.Fad;
        memset(Fad, 0, NMs*sAD*sizeof(double));
        const double * dDbc = Dnt(at, at, nb, nc);

        // digestion (contraction)
        // ***********************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        if ((TheBatch.ABp == TheBatch.CDp) && (ja*JB+jb < jc*JD+jd)) {
                            TT += TheBatch.wsize;
                            continue;
                        }
                        bool half = ((TheBatch.ABp == TheBatch.CDp) && (ja==jc) && (jb==jd));

                        uint32_t oAD = (ja*JD + jd)*(MA*MD);
                        uint32_t oBC = (jb*JC + jc)*(MB*MC);

                        for (int n=0; n<NMs; ++n) {

                            const double * DD = dDbc + n*sBC + oBC;
                            double       * FF =  Fad + n*sAD + oAD;

                            for (int ma=0; ma<MA; ++ma) {
                                for (int md=0; md<MD; ++md) {
                                    double sum = 0;
                                    for (int mb=0; mb<MB; ++mb) {
                                        for (int mc=0; mc<MC; ++mc) {
                                            int pos = Tpos (MA, MB, MC, MD, ma, mb, mc, md);
                                            int bc  = Tpos (MB, MC, mb, mc);
                                            sum += TT[pos] * DD[bc];
                                        }
                                    }
                                    int ad = Tpos(MA, MD, ma, md);
                                    if (half) sum *= 0.5;
                                    FF[ad] += sum;
                                }
                            }
                        }

                        TT += TheBatch.wsize;
                    }
                }
            }
        }

        // scale the atomic block updates
        {
            double f = 1.;
            if (TheBatch.ABp->samef) f*=0.5; // A==B
            if (TheBatch.CDp->samef) f*=0.5; // C==D
            //if (TheBatch.ABp == TheBatch.CDp) f*=0.5; //AB==CD
            for (int mm=0; mm<NMs*sAD; ++mm) Fad[mm]*=f;
        }

        // update fock matrices
        const uint8_t eat = CNP.GetID(at);
        NTsub Xsub_ad = Xnt(eat,eat,na,nd);

        // update fock matrices
        {
            xArray dFad = Xsub_ad(at, at);
            for (int mm=0; mm<NMs*sAD; ++mm) dFad[mm] += Fad[mm];
        }
    }

    {
        double * TT = (double*)b4blockCPU.T8d;

        double * Fbc = (double*)b4blockCPU.Fbc;
        memset(Fbc, 0, NMs*sBC*sizeof(double));
        const double * dDad = Dnt(at, at, na, nd);

        // digestion (contraction)
        // ***********************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        if ((TheBatch.ABp == TheBatch.CDp) && (ja*JB+jb < jc*JD+jd)) {
                            TT += TheBatch.wsize;
                            continue;
                        }
                        bool half = ((TheBatch.ABp == TheBatch.CDp) && (ja==jc) && (jb==jd));

                        uint32_t oAD = (ja*JD + jd)*(MA*MD);
                        uint32_t oBC = (jb*JC + jc)*(MB*MC);

                        for (int n=0; n<NMs; ++n) {

                            const double * DD = dDad + n*sAD + oAD;
                            double       * FF =  Fbc + n*sBC + oBC;

                            for (int mb=0; mb<MB; ++mb) {
                                for (int mc=0; mc<MC; ++mc) {
                                    double sum = 0;
                                    for (int ma=0; ma<MA; ++ma) {
                                        for (int md=0; md<MD; ++md) {
                                            int pos = Tpos (MA, MB, MC, MD, ma, mb, mc, md);
                                            int ad  = Tpos (MA, MD, ma, md);
                                            sum += TT[pos] * DD[ad];
                                        }
                                    }
                                    int bc = Tpos (MB, MC, mb, mc);
                                    if (half) sum *= 0.5;
                                    FF[bc] += sum;
                                }
                            }
                        }

                        TT += TheBatch.wsize;
                    }
                }
            }
        }

        // scale the atomic block updates
        {
            double f = 1.;
            if (TheBatch.ABp->samef) f*=0.5; // A==B
            if (TheBatch.CDp->samef) f*=0.5; // C==D
            //if (TheBatch.ABp == TheBatch.CDp) f*=0.5; //AB==CD
            for (int mm=0; mm<NMs*sBC; ++mm) Fbc[mm]*=f;
        }

        // update fock matrices
        const uint8_t eat = CNP.GetID(at);
        NTsub Xsub_bc = Xnt(eat,eat,nb,nc);

        // update fock matrices
        {
            xArray dFbc = Xsub_bc(at, at);
            for (int mm=0; mm<NMs*sBC; ++mm) dFbc[mm] += Fbc[mm];
        }
    }

    {
        double * TT = (double*)b4blockCPU.T8d;

        double * Fbd = (double*)b4blockCPU.Fbd;
        memset(Fbd, 0, NMs*sBD*sizeof(double));
        const double * dDac = Dnt(at, at, na, nc);

        // digestion (contraction)
        // ***********************

        for (int ja=0; ja<JA; ++ja) {
            for (int jb=0; jb<JB; ++jb) {
                for (int jc=0; jc<JC; ++jc) {
                    for (int jd=0; jd<JD; ++jd) {

                        if ((TheBatch.ABp == TheBatch.CDp) && (ja*JB+jb < jc*JD+jd)) {
                            TT += TheBatch.wsize;
                            continue;
                        }
                        bool half = ((TheBatch.ABp == TheBatch.CDp) && (ja==jc) && (jb==jd));

                        uint32_t oAC = (ja*JC + jc)*(MA*MC);
                        uint32_t oBD = (jb*JD + jd)*(MB*MD);

                        for (int n=0; n<NMs; ++n) {

                            const double * DD = dDac + n*sAC + oAC;
                            double       * FF =  Fbd + n*sBD + oBD;

                            for (int mb=0; mb<MB; ++mb) {
                                for (int md=0; md<MD; ++md) {
                                    double sum = 0;
                                    for (int ma=0; ma<MA; ++ma) {
                                        for (int mc=0; mc<MC; ++mc) {
                                            int pos = Tpos (MA, MB, MC, MD, ma, mb, mc, md);
                                            int ac  = Tpos (MA, MC, ma, mc);
                                            sum += TT[pos] * DD[ac];
                                        }
                                    }
                                    int bd = Tpos (MB, MD, mb, md);
                                    if (half) sum *= 0.5;
                                    FF[bd] += sum;
                                }
                            }
                        }

                        TT += TheBatch.wsize;
                    }
                }
            }
        }

        // scale the atomic block updates
        {
            double f = 1.;
            if (TheBatch.ABp->samef) f*=0.5; // A==B
            if (TheBatch.CDp->samef) f*=0.5; // C==D
            //if (TheBatch.ABp == TheBatch.CDp) f*=0.5; //AB==CD
            for (int mm=0; mm<NMs*sBD; ++mm) Fbd[mm]*=f;
        }

        // update fock matrices
        const uint8_t eat = CNP.GetID(at);
        NTsub Xsub_bd = Xnt(eat,eat,nb,nd);

        // update fock matrices
        {
            xArray dFbd = Xsub_bd(at, at);
            for (int mm=0; mm<NMs*sBD; ++mm) dFbd[mm] += Fbd[mm];
        }
    }

}
