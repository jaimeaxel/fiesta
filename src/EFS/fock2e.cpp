
// include this before anything else or else Intel MPI complains
#include "low/MPIwrap.hpp"

#include <cstdlib>
#include <fstream>
#include <cmath>
//#include <malloc/malloc.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "defs.hpp"
#include "fock2e.hpp"

#include "low/chrono.hpp"
#include "math/angular.hpp"

#include "linear/ntensors.hpp"
#include "linear/tensors.hpp"

using namespace LibAngular;

#include "basis/atomprod.hpp"
#include "basis/shellpair.hpp"
#include "basis/SPprototype.hpp"
#include "libquimera/ERIgeom.hpp"
#include "libquimera/libquimera.hpp"
using namespace LibQuimera;

#include "EFS/ERIblocks.hpp"
#include "EFS/ERIbatch.hpp"
#include "libechidna/libechidna.hpp"

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
    #define omp_get_num_threads() 1
#endif


static inline uint8_t lmax(uint8_t l1, uint8_t l2) {
    l1 = (l1==LSP)?1:l1;
    l2 = (l2==LSP)?1:l2;
    return max(l1,l2);
}

static inline int min(int a, int b) {
    return (a<b)?a:b;
}

static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline bool InvertPrototypePair(const ShellPairPrototype & ABp, const ShellPairPrototype & CDp) {
    uint8_t ma = ABp.l1==LSP?4:2*ABp.l1+1;
    uint8_t mb = ABp.l2==LSP?4:2*ABp.l2+1;
    uint8_t mc = CDp.l1==LSP?4:2*CDp.l1+1;
    uint8_t md = CDp.l2==LSP?4:2*CDp.l2+1;

    return (ma>mc) || (ma==mc && mb>=md);
}



Fock2e::Fock2e() {

    EbatchBuffer = NULL;

    Blocks       = NULL;
    BatchEval    = NULL;

    SDsparseGPU = NULL;
    ADsparseGPU = NULL;

    APL = NULL;

    NTP = NULL;

    STP = NULL;

    // constants; may be changed before starting calculation
    CAMDFT   = false;
    XS       = 1.;
    alpha    = 0.19;
    beta     = 0.46;
    mu       = 0.33;
}

Fock2e::~Fock2e() {

    delete EbatchBuffer;
    delete[] Blocks;
    delete[] BatchEval; // this is
    //BatchEvaluator::FreeMem ();
    delete NTP;

    // delete cholesky data; the rest will destroy itself when out of scope
    delete[] CDdata.sigma;
}




void Fock2e::Init(uint32_t nInteractingAtoms, APlists & pAPL, const sparsetensorpattern & SparseTensorPattern, rAtom * const * AtomListGpus) {

    // keep a local pointer
    STP = &SparseTensorPattern;

    //add HF exchange (default)
    XS = 1.;

    //number of matrices of last cycle
    nDMs = 0;

    //these pointers hold the information about the shellpairs, etc.
    APL = &pAPL;

    //all the following sizes for buffers and arrays should be calculated properly
    #pragma omp parallel
    if (omp_get_thread_num()==0) OGMT = omp_get_num_threads();

    // whether or not GPU acceleration is to be  used
    UseGPU = Echidna::UseGPU;

    //maximum number of atom interactions
    max_at_int = nInteractingAtoms; //AtomInteractions.GetTotN();

    // one central buffer to store the tiles' lists before processing
    EbatchBuffer = new ERIbatchBuffer;
    EbatchBuffer->Init(Echidna::MEMBUFFER);

    // one ERIblock for every thread running
    Blocks = new ERIblock[OGMT];
    for (int p=0; p<OGMT; ++p) Blocks[p].Init(max_at_int, EbatchBuffer, STP);


    // one BatchEvaluator for every thread running
    BatchEval = new BatchEvaluator[OGMT];

    #ifdef USE_GPU
      if (UseGPU) BatchEvaluator::InitMem (Echidna::MAXMEM, Echidna::MAXMEMGPU, BatchEval, OGMT);
      else        BatchEvaluator::InitMem (Echidna::MAXMEM,                     BatchEval, OGMT);
    #else
      BatchEvaluator::InitMem (Echidna::MAXMEM,                     BatchEval, OGMT);
    #endif

    // set a const pointer to a copy of atom list on same device
    #ifdef USE_GPU
    if (UseGPU) for (int p=0; p<OGMT; ++p) BatchEval[p].AtomListGpus = AtomListGpus[BatchEval[p].GPUdevice];
    #endif

    InitERIroutines(); //initializes the ERI evaluation routines needed for the system
    InitJobQueue();    //initialized the job queue
}

void Fock2e::Clear() {
    // this is static and thus should be a thing of its own
    //BatchEvaluator::FreeMem ();

    delete EbatchBuffer;
    delete[] Blocks;
    delete[] BatchEval;

    EbatchBuffer = NULL;
    Blocks       = NULL;
    BatchEval    = NULL;
}

void Fock2e::SetXS(double Xscaling) {
    CAMDFT = false;
    XS = Xscaling;
}

void Fock2e::SetCAM (double ialpha, double ibeta, double imu) {
    CAMDFT = true;
    alpha  = ialpha;
    beta   = ibeta;
    mu     = imu;
    XS     = 1.;
}

void Fock2e::InitERIroutines() {

    //sets the geometry and angular momenta only for the required routines
    //********************************************************************

    //all four atoms different (4-center ERIs) (k² N²)
    for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomPairBatch[n12];
        if (APB12.APlist.n==0) continue;

        for (int n34=0; n34<=n12; ++n34) {
            const APbatch & APB34 = APL->AtomPairBatch[n34];
            if (APB34.APlist.n==0) continue;

            //loop over shell pair prototypes
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];
                for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                    const ShellPairPrototype & CDp = APB34.APP->Gs[cd];
                    if (APB12.nAPS[ab]==0) continue;
                    if (APB34.nAPS[cd]==0) continue;
                    bool rev = InvertPrototypePair(ABp, CDp);
                    if (rev) Q.ListNeeded(ABCD, ABp.l1, ABp.l2, CDp.l1, CDp.l2);
                    else     Q.ListNeeded(ABCD, CDp.l1, CDp.l2, ABp.l1, ABp.l2);
                }
            }

            // for integrals with at least one atom center shared

            // needed because CDR becomes numerically unstable when trying to apply
            // the general, 4-center integral algorithm to degenerate 3-center and 2-center
            // cases in conjunction with basis sets containing very diffuse primitives
            if ( (APB12.APP->Atype1 == APB34.APP->Atype1) ||
                 (APB12.APP->Atype1 == APB34.APP->Atype2) ||
                 (APB12.APP->Atype2 == APB34.APP->Atype1) ||
                 (APB12.APP->Atype2 == APB34.APP->Atype2) ) {

                //loop over shell pair prototypes
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];
                    for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                        const ShellPairPrototype & CDp = APB34.APP->Gs[cd];
                        if (APB12.nAPS[ab]==0) continue;
                        if (APB34.nAPS[cd]==0) continue;

                         // there is 4 different ways this could happen;
                         // it is necessary to keep track of whether ABp and/or CDp
                         // order needs to be swapped
                        if      (ABp.atype1 == CDp.atype1) {
                        }
                        else if (ABp.atype1 == CDp.atype2) {
                        }
                        else if (ABp.atype2 == CDp.atype1) {
                        }
                        else if (ABp.atype2 == CDp.atype2) {
                        }

                        bool rev = InvertPrototypePair(ABp, CDp);

                        if (rev) Q.ListNeeded(ABAD, ABp.l1, ABp.l2, CDp.l1, CDp.l2);
                        else     Q.ListNeeded(ABAD, CDp.l1, CDp.l2, ABp.l1, ABp.l2);
                    }
                }

            }


        }
    }


    //same atom pair (2-center ABAB-type ERIs) (k N)
    for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomPairBatch[n12];

        if (APB12.APlist.n==0) continue;

        //same as with ABAB integrals, only that there's only two states

        for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
            const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

            for (int cd=0; cd<=ab; ++cd) {
                const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                if (APB12.nAPS[ab]==0) continue;
                if (APB12.nAPS[cd]==0) continue;

                bool rev = InvertPrototypePair(ABp, CDp);

                if (rev) Q.ListNeeded(ABAB, ABp.l1, ABp.l2, CDp.l1, CDp.l2);
                else     Q.ListNeeded(ABAB, CDp.l1, CDp.l2, ABp.l1, ABp.l2);
            }
        }
    }


    //two centers + same atom (3-center ERIs) (k N²)
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];
        if (APB12.APlist.n==0) continue;

        for (int n34=0; n34<APL->AtomPairBatch.n; ++n34) {
            const APbatch & APB34 = APL->AtomPairBatch[n34];
            if (APB34.APlist.n==0) continue;

            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];
                for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                    const ShellPairPrototype & CDp = APB34.APP->Gs[cd];
                    if (APB12.nAPS[ab]==0) continue;
                    if (APB34.nAPS[cd]==0) continue;
                    Q.ListNeeded(AACD, ABp.l1, ABp.l2, CDp.l1, CDp.l2);
                }
            }
        }
    }

    //two different atoms (2-center ERIs) (N²)
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];
        if (APB12.APlist.n==0) continue;

        for (int n34=0; n34<=n12; ++n34) {
            const APbatch & APB34 = APL->AtomSameBatch[n34];
            if (APB34.APlist.n==0) continue;
            if (n12==n34 && APB34.APlist.n==1) continue; //only one atom!

            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                    const ShellPairPrototype & CDp = APB34.APP->Gs[cd];
                    bool rev = InvertPrototypePair(ABp, CDp);
                    if (rev) Q.ListNeeded(AACC, ABp.l1, ABp.l2, CDp.l1, CDp.l2);
                    else     Q.ListNeeded(AACC, CDp.l1, CDp.l2, ABp.l1, ABp.l2);
                }
            }
        }
    }

    //one atom only (1-center)
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];
        const AtomProdPrototype * APP = APB12.APP;

        //calcula las integrales para SOLO un atomo de cada tipo
        for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
            const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

            for (int cd=0; cd<=ab; ++cd) {
                const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                bool rev = InvertPrototypePair(ABp,CDp);

                if (rev) Q.ListNeeded(AAAA, ABp.l1, ABp.l2, CDp.l1, CDp.l2);
                else     Q.ListNeeded(AAAA, CDp.l1, CDp.l2, ABp.l1, ABp.l2);
            }
        }
    }

    //initializes (computes) the required routines
    Q.GenerateNeeded(false);
}

//put in the queue all the calls to different ERI routines, except for the 1-nuclear
void Fock2e::InitJobQueue() {

    TwoElectronJobs.Clear();

    // ERI calc
    // ********

    //4-center integrals
    // ****************

    //all four atoms different (4-center ERIs) (k² N²)
    for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomPairBatch[n12];

        if (APB12.APlist.n==0) continue;

        for (int n34=0; n34<n12; ++n34) {
            const APbatch & APB34 = APL->AtomPairBatch[n34];

            if (APB34.APlist.n==0) continue;

            //loop over shell pair prototypes
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                    const ShellPairPrototype & CDp = APB34.APP->Gs[cd];

                    if (APB12.nAPS[ab]==0) continue;
                    if (APB34.nAPS[cd]==0) continue;

                    bool rev = InvertPrototypePair(ABp, CDp);

                    BatchInfo Info(this);

                    //sets the general properties of the ERI block
                    if (rev) {
                        Info.Set(ABCD, ABp, CDp, false);
                        Info.SetLists(APB12.SP[ab],APB34.SP[cd],&APB12.APlist,&APB34.APlist, APB12.nAPS[ab], APB34.nAPS[cd],false);
                    }
                    else {
                        Info.Set(ABCD, CDp, ABp, false);
                        Info.SetLists(APB34.SP[cd],APB12.SP[ab],&APB34.APlist,&APB12.APlist, APB34.nAPS[cd], APB12.nAPS[ab],false);
                    }

                    TwoElectronJobs.push(Info);
                }
            }
        }
    }

    //n12==n34
    for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomPairBatch[n12];

        if (APB12.APlist.n==0) continue;

        {
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<ab; ++cd) {
                    const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                    if (APB12.nAPS[ab]==0) continue;
                    if (APB12.nAPS[cd]==0) continue;

                    bool rev = InvertPrototypePair(ABp, CDp);

                    BatchInfo Info(this);

                    if (rev) {
                        Info.Set(ABCD, ABp, CDp, false);
                        Info.SetLists(APB12.SP[ab],APB12.SP[cd],&APB12.APlist,&APB12.APlist, APB12.nAPS[ab], APB12.nAPS[cd],true);
                    }
                    else {
                        Info.Set(ABCD, CDp, ABp, false);
                        Info.SetLists(APB12.SP[cd],APB12.SP[ab],&APB12.APlist,&APB12.APlist, APB12.nAPS[cd], APB12.nAPS[ab],true);
                    }

                    TwoElectronJobs.push(Info);
                }

                //ab==cd
                {
                    BatchInfo Info(this);

                    Info.Set(ABCD, ABp, ABp, false);
                    Info.SetLists(APB12.SP[ab],&APB12.APlist, APB12.nAPS[ab],false);

                    TwoElectronJobs.push(Info);
                }
            }
        }
    }

    //same atom pair (2-center ERIs, spread integrals) (k N)
    for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomPairBatch[n12];

        if (APB12.APlist.n==0) continue;


        for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
            const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

            for (int cd=0; cd<ab; ++cd) {
                const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                if (APB12.nAPS[ab]==0) continue;
                if (APB12.nAPS[cd]==0) continue;

                bool rev = InvertPrototypePair(ABp, CDp);
                uint32_t tmax = min(APB12.nAPS[ab], APB12.nAPS[cd]);

                BatchInfo Info(this);

                if (rev) {
                    Info.Set(ABAB, ABp, CDp, false);
                    Info.SetLists(APB12.SP[ab],APB12.SP[cd],&APB12.APlist,&APB12.APlist,tmax,tmax,false);
                }
                else {
                    Info.Set(ABAB, CDp, ABp, false);
                    Info.SetLists(APB12.SP[cd],APB12.SP[ab],&APB12.APlist,&APB12.APlist,tmax,tmax,false);
                }

                TwoElectronJobs.push(Info);
            }

            //ab==cd
            {
                uint32_t tmax = APB12.nAPS[ab];

                BatchInfo Info(this);

                Info.Set(ABAB, ABp, ABp, true);
                Info.SetLists(APB12.SP[ab],&APB12.APlist,tmax,false);

                TwoElectronJobs.push(Info);
            }
        }
    }


    //3-center integrals
    // ****************

    //two centers + same atom (3-center ERIs) (k N²)
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];

        if (APB12.APlist.n==0) continue;

        for (int n34=0; n34<APL->AtomPairBatch.n; ++n34) {
            const APbatch & APB34 = APL->AtomPairBatch[n34];

            if (APB34.APlist.n==0) continue;

            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                    const ShellPairPrototype & CDp = APB34.APP->Gs[cd];

                    if (APB34.nAPS[cd]==0) continue;

                    BatchInfo Info(this);

                    Info.Set(AACD, ABp, CDp, false);
                    Info.SetLists(APB12.SP[ab],APB34.SP[cd],&APB12.APlist,&APB34.APlist, APB12.nAPS[ab], APB34.nAPS[cd],false);

                    TwoElectronJobs.push(Info);
                }
            }
        }
    }

    //2-center integrals
    // ****************

    //two different atoms (2-center ERIs) (N²)
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];

        if (APB12.APlist.n==0) continue;

        for (int n34=0; n34<n12; ++n34) {
            const APbatch & APB34 = APL->AtomSameBatch[n34];

            if (APB34.APlist.n==0) continue;

            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<APB34.APP->nGPt; ++cd) {
                    const ShellPairPrototype & CDp = APB34.APP->Gs[cd];

                    bool rev = InvertPrototypePair(ABp, CDp);

                    BatchInfo Info(this);

                    if (rev) {
                        Info.Set(AACC, ABp, CDp, false);
                        Info.SetLists(APB12.SP[ab],APB34.SP[cd],&APB12.APlist,&APB34.APlist, APB12.APlist.n, APB34.APlist.n,false);
                    }
                    else {
                        Info.Set(AACC, CDp, ABp, false);
                        Info.SetLists(APB34.SP[cd],APB12.SP[ab],&APB34.APlist,&APB12.APlist, APB34.APlist.n, APB12.APlist.n,false);
                    }

                    TwoElectronJobs.push(Info);
                }
            }
        }
    }

    //n12==n34
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];

        if (APB12.APlist.n<2) continue;

        {
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<ab; ++cd) {
                    const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                    bool rev = InvertPrototypePair(ABp, CDp);

                    BatchInfo Info(this);

                    if (rev) {
                        Info.Set(AACC, ABp, CDp, false);
                        Info.SetLists(APB12.SP[ab],APB12.SP[cd],&APB12.APlist,&APB12.APlist, APB12.APlist.n, APB12.APlist.n,true);
                    }
                    else {
                        Info.Set(AACC, CDp, ABp, false);
                        Info.SetLists(APB12.SP[cd],APB12.SP[ab],&APB12.APlist,&APB12.APlist, APB12.APlist.n, APB12.APlist.n,true);
                    }

                    TwoElectronJobs.push(Info);
                }

                {
                    BatchInfo Info(this);

                    Info.Set(AACC, ABp,ABp, false);
                    Info.SetLists(APB12.SP[ab],&APB12.APlist, APB12.APlist.n,false);

                    TwoElectronJobs.push(Info);
                }
            }
        }
    }

    TwoElectronJobs.Done();
}



// compute the frobenius norm of every block and extract the logarithm
void Fock2e::CalcDensityPrescreeners(const NTconst & DS, const NTconst & DA) {

    const double NORM0 = 1.e-100; // avoid taking the logarithm of 0
    const double EPS   = 1.e-16;  // minimum norm required to consider antisymmetric components

    uint32_t natoms = NTP->GetAtoms();

    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1) {
                for (uint16_t bf2=0; bf2<BF2; ++bf2) {

                    uint32_t len = NTP->GetLen(id1,bf1) * NTP->GetLen(id2,bf2);

                    double smax = NORM0;

                    // compute all symmetric blocks
                    {
                        const double * vals = DS(at1, at2, bf1, bf2);
                        for (int n=0; n<nDMs; ++n) {

                            double sum2 = 0;
                            for (uint32_t it=0; it<len; ++it)
                                sum2 += vals[it]*vals[it];

                            smax = max(smax,sum2);

                            // next block
                            vals += len;
                        }
                    }

                    // compute all antysymmetric blocks
                    {
                        const double * vals = DA(at1, at2, bf1, bf2);
                        for (int n=0; n<nDMs; ++n) {

                            double sum2 = 0;
                            for (uint32_t it=0; it<len; ++it)
                                sum2 += vals[it]*vals[it];

                            smax = max(smax,sum2);

                            // next block
                            vals += len;
                        }
                    }

                    // save the log of the largest contribution
                    lnDs[id1][id2][bf1][bf2][at1][at2] = logDs[at1][at2][bf1][bf2] = -log(smax)/2;
                    lnAD[id1][id2][bf1][bf2][at1][at2] = logAD[at1][at2][bf1][bf2] = 0; // this wont add to the prescreen
                }
            }
        }
    }

    // this doesnt make any sense any longer, as nowhere is logAD used for the antisymmetric stuff

    /*
    // ignore the antisymmetric parts of the density matrices unless the contribution is significant

    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1) {
                for (uint16_t bf2=0; bf2<BF2; ++bf2) {

                    uint32_t len = NTP->GetLen(id1,bf1) * NTP->GetLen(id2,bf2);

                    double smax = NORM0;

                    const double * vals = DA(at1, at2, bf1, bf2);

                    for (int n=0; n<nDMs; ++n) {

                        double sum2 = 0;
                        for (uint32_t it=0; it<len; ++it)
                            sum2 += vals[it]*vals[it];

                        smax = max(smax,sum2);

                        // next block
                        vals += len;
                    }

                    logAD[at1][at2][bf1][bf2] = -log(smax)/2;

                    // at the moment logDs is the only one being checked
                    logDs[at1][at2][bf1][bf2] = min(logDs[at1][at2][bf1][bf2], logAD[at1][at2][bf1][bf2]);
                }
            }
        }
    }
    */

/*
    for (uint32_t at1=0; at1<natoms; ++at1) {
        cout << "atom : "  << at1 << "  RID=" << atoms[at1].rAP->RId  << "  DID=" << atoms[at1].rAP->RId  << "  efs ID=" << int(NTP->GetID(at1)) << endl;
    }
    cout << endl;
    */

    /*
    //all four atoms different (4-center ERIs) (k² N²)
    for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomPairBatch[n12];

        if (APB12.APlist.n==0) continue;

        //loop over shell pair prototypes
        for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
            const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

            cout << "n12 " << n12 << "  ab : " << ab << "   " << int(ABp.atype1) << "," << int(ABp.atype2) << "   " << int(ABp.eid1) << "," << int(ABp.eid2) << endl;
        }
    }
    cout << endl;
    */

}

// this is not as good as it can be;
// it still computes the (ON||NN) blocks
void Fock2e::DensityFittingPrescreener(const NTconst & DS) {

    const double NORM0 = 1.e-100; // avoid taking the logarithm of 0

    // initialize to false
    const uint8_t NID = NTP->GetNIDs();
    r1tensor<r1tensor<bool>> IsUsed(NID);

    for (int id1=0; id1<NID; ++id1) {
        const uint8_t BF1 = NTP->GetFuncs(id1);
        IsUsed[id1].set(BF1);
        for (uint16_t bf1=0; bf1<BF1; ++bf1) IsUsed[id1][bf1] = false;
    }

    /*
    {
        for (uint32_t at1=0; at1<natoms; ++at1) {
            uint8_t id1 = NTP->GetID(at1);

            if (IsUsed[id1]==NULL) {
                const uint8_t BF1 = NTP->GetFuncs(id1);
                IsUsed[id1] = new bool[BF1];

                for (uint16_t bf1=0; bf1<BF1; ++bf1) IsUsed[id1][bf1] = false;
            }
        }
    }
    */

    const uint32_t natoms = NTP->GetAtoms();

    // compute the initial density matrix prescreeners
    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1) {
                for (uint16_t bf2=0; bf2<BF2; ++bf2) {

                    uint32_t len = NTP->GetLen(id1,bf1) * NTP->GetLen(id2,bf2);

                    double smax = NORM0;

                    // compute all symmetric blocks
                    {
                        const double * vals = DS(at1, at2, bf1, bf2);
                        for (int n=0; n<nDMs; ++n) {

                            double sum2 = 0;
                            for (uint32_t it=0; it<len; ++it)
                                sum2 += vals[it]*vals[it];

                            smax = max(smax,sum2);

                            // next block
                            vals += len;
                        }
                    }

                    // save the log of the largest contribution
                    lnDs[id1][id2][bf1][bf2][at1][at2] = logDs[at1][at2][bf1][bf2] = -log(smax)/2;

                    if (smax>NORM0) IsUsed[id1][bf1] = true; // the block has been used
                }
            }
        }
    }

    // mask the blocks that are not needed
    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1) {
                for (uint16_t bf2=0; bf2<BF2; ++bf2) {

                    if (!IsUsed[id1][bf1] && !IsUsed[id2][bf2])
                        lnAD[id1][id2][bf1][bf2][at1][at2] = logAD[at1][at2][bf1][bf2] = 0; // only update elements of the new density matrix
                    else
                        lnAD[id1][id2][bf1][bf2][at1][at2] = logAD[at1][at2][bf1][bf2] = -log(NORM0)/2; // otherwise, prescreen
                }
            }
        }
    }

}


// set to zero the prescreeners; used to compute the bulk 2e integrals
void Fock2e::CheapDensityPrescreener() {

    const double NORM0 = 1.e-100;
    const double logN0 = -log(NORM0)/2;


    const uint32_t natoms = NTP->GetAtoms();

    // set everything to zero
    // ======================
    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1)
                for (uint16_t bf2=0; bf2<BF2; ++bf2)
                    lnDs[id1][id2][bf1][bf2][at1][at2] = lnAD[id1][id2][bf1][bf2][at1][at2] =
                    logDs[at1][at2][bf1][bf2] = logAD[at1][at2][bf1][bf2] = logN0;
        }
    }

    // except for same atom densities
    // ==============================
    for (uint32_t at=0; at<natoms; ++at) {

        uint8_t id = NTP->GetID(at);

        const uint8_t BF = NTP->GetFuncs(id);

        for (uint16_t bf1=0; bf1<BF; ++bf1)
            for (uint16_t bf2=0; bf2<BF; ++bf2)
                lnDs[id][id][bf1][bf2][at][at] = lnAD[id][id][bf1][bf2][at][at] =
                logDs[at][at][bf1][bf2] = logAD[at][at][bf1][bf2] = 0;
    }

}

// this will compute all contributions from same-center products to the rest of the matrix
void Fock2e::MediumDensityPrescreener() {

    const double NORM0 = 1.e-100;
    const double logN0 = -log(NORM0)/2;


    const uint32_t natoms = NTP->GetAtoms();

    // set everything to zero
    // ======================
    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1)
                for (uint16_t bf2=0; bf2<BF2; ++bf2) {
                    lnDs[id1][id2][bf1][bf2][at1][at2] = logDs[at1][at2][bf1][bf2] = logN0; // ignore the content of the input density matrix
                    lnAD[id1][id2][bf1][bf2][at1][at2] = logAD[at1][at2][bf1][bf2] = 0;     // consider all pairs in principle
                }
        }
    }

    // except for same atom densities
    // ==============================
    for (uint32_t at=0; at<natoms; ++at) {

        uint8_t id = NTP->GetID(at);

        const uint8_t BF = NTP->GetFuncs(id);

        for (uint16_t bf1=0; bf1<BF; ++bf1)
            for (uint16_t bf2=0; bf2<BF; ++bf2)
                lnDs[id][id][bf1][bf2][at][at] = lnAD[id][id][bf1][bf2][at][at] =
                logDs[at][at][bf1][bf2] = logAD[at][at][bf1][bf2] = 0;
    }

}

// compute all integrals for everything
// ====================================
void Fock2e::SetZeroDensityPrescreener() {

    const uint32_t natoms = NTP->GetAtoms();

    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1)
                for (uint16_t bf2=0; bf2<BF2; ++bf2)
                    lnDs[id1][id2][bf1][bf2][at1][at2] =
                    lnAD[id1][id2][bf1][bf2][at1][at2] =
                    logDs[at1][at2][bf1][bf2] =
                    logAD[at1][at2][bf1][bf2] = 0;
        }
    }

}


void Fock2e::CholeskyPrescreener(const float logCS) {

    const double NORM0 = 1.e-100; // avoid taking the logarithm of 0


    // initially set all to NOT compute

    const uint32_t natoms = NTP->GetAtoms();

    // compute the initial density matrix prescreeners
    for (uint32_t at1=0; at1<natoms; ++at1) {
        for (uint32_t at2=0; at2<natoms; ++at2) {

            // get the "element" to which the atoms correspond
            uint8_t id1 = NTP->GetID(at1);
            uint8_t id2 = NTP->GetID(at2);

            const uint8_t BF1 = NTP->GetFuncs(id1);
            const uint8_t BF2 = NTP->GetFuncs(id2);

            for (uint16_t bf1=0; bf1<BF1; ++bf1) {
                for (uint16_t bf2=0; bf2<BF2; ++bf2) {
                    // save the log of the largest contribution
                    lnDs[id1][id2][bf1][bf2][at1][at2] = logDs[at1][at2][bf1][bf2] = -log(NORM0)/2;
                }
            }
        }
    }


    // add all SPs with CS over a given threshold
    const ShellPair * spp = APL->SPpool;

    for (int n=0; n<APL->nGTOps; ++n) {

        if (spp[n].logCS<logCS) {

            const uint32_t at1 = spp[n].ata;
            const uint32_t at2 = spp[n].atb;

            const uint8_t id1 = NTP->GetID(at1);
            const uint8_t id2 = NTP->GetID(at2);

            const uint8_t bf1 = spp[n].GP->nb1;
            const uint8_t bf2 = spp[n].GP->nb2;

            lnDs[id1][id2][bf1][bf2][at1][at2] = logDs[at1][at2][bf1][bf2] = 0; // compute this one
        }
    }


}



#include "low/sort.hpp"

// evaluates Cauchy-Swarz bounds for ERI
void Fock2e::CalcCauchySwarz() {

    Echidna::EMessenger << "Calculating Cauchy-Swarz bounds for integral screening";
    Echidna::EMessenger.Push(); {

        //two different atoms
        for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
            const APbatch & APB12 = APL->AtomPairBatch[n12];

            if (APB12.APlist.n==0) continue; //no overlaps between these two specific elements

            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                if (APB12.nAPS[ab]==0) continue; //no overlaps between these two shell pair prototypes

                ERIBatch CSbatch(this);
                CSbatch.Set(ABAB, ABp, ABp, true);
                CSbatch.Ntiles64 = 1;
                for (int i=0; i<OGMT; ++i) BatchEval[i].SetBuffers(CSbatch);

                //buffers, sizes and other stuff needed for the calculation

                //ERI
                const AtomProd & AP12 = APB12.APlist[0];
                ShellPair * ABs = AP12.pSPs[ab];

                #pragma omp parallel for schedule(dynamic)
                for (int ap12=0; ap12<APB12.nAPS[ab]; ap12+=DOUBLES_PER_CACHE_LINE) {

                    ERIgeometry   eriG[DOUBLES_PER_CACHE_LINE];
                    ERIgeometries64  eriG8;

                    ERITile64 ET;

                    ET.nKab = ABp.Ka*ABp.Kb -1;
                    ET.nKcd = ABp.Ka*ABp.Kb -1;

                    int lenk = min (DOUBLES_PER_CACHE_LINE, APB12.nAPS[ab]-ap12);

                    for (int k=0; k<lenk; ++k) {
                        ET.ap12[k] = ap12+k;
                        ET.ap34[k] = ap12+k;
                    }
                    for (int k=lenk; k<DOUBLES_PER_CACHE_LINE; ++k) {
                        ET.ap12[k] = ap12; //+lenk-1;
                        ET.ap34[k] = ap12; //+lenk-1;
                    }

                    ET.wSP12 = ABs->WW;
                    ET.wSP34 = ABs->WW;
                    ET.nK2ab = ABp.Ka * ABp.Kb;
                    ET.nK2cd = ABp.Ka * ABp.Kb;

                    ET.lowgeom = ABAB;

                    for (int k=0; k<DOUBLES_PER_CACHE_LINE; ++k) {
                        const AtomProd & AP12 = APB12.APlist[ ET.ap12[k] ];
                        eriG[k].MakeRotation2S (AP12);
                        eriG[k].Adjust         (ABp.inverted, ABp.inverted);
                    }

                    PackGeometries           (eriG, eriG8);

                    int ogtn = omp_get_thread_num();

                    cacheline64 maxv;
                    maxv = BatchEval[ogtn].ERItrace(CSbatch, *ABs, eriG8, ET, lenk);

                    //set the Cauchy-Schwarz parameter
                    for (int k=0; k<lenk; ++k) {
                        double mv = maxv(k);
                        double logCS = (mv>0)?(-log(mv)/2):100;
                        ABs[ap12+k].logCS = logCS;
                    }
                }

            }
        }

        //same atom
        for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
            const APbatch & APB12 = APL->AtomSameBatch[n12];

            ERIgeometry eriG;
            point A(0,0,0);
            eriG.MakeRotation1(A);

            //#pragma omp parallel for schedule(dynamic)
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];
                const ShellPair & ABs = APB12.SP[ab][0];

                ERIBatch CSbatch(this);
                CSbatch.Ntiles64 = 1;
                CSbatch.Set(AAAA, ABp, ABp, true);
                BatchEval[0].SetBuffers(CSbatch);

                double mv = BatchEval[0].ERItrace(CSbatch, ABs,eriG);
                double logCS = (mv>0)?(-log(mv)/2):100;

                //set the cauchy-schwarz parameter to ALL atom pairs
                //**************************************************

                for (int ap12=0; ap12<APB12.APlist.n; ++ap12)  {
                    const AtomProd & AP12 = APB12.APlist[ap12];
                    ShellPair & ABs = *AP12.pSPs[ab];
                    ABs.logCS = logCS;
                }
            }
        }

    } Echidna::EMessenger.Pop();

    Echidna::EMessenger << endl;

    // initialize positions for compact CD supertensor indexing
    {


        ShellPair * sp = APL->SPpool;
        const int NSP = APL->nGTOps;

        ShellPair ** spp = new ShellPair*[NSP];
        float    * logCS = new float[NSP];

        for (int n=0; n<NSP; ++n) spp[n]   = &sp[n];
        for (int n=0; n<NSP; ++n) logCS[n] = sp[n].logCS;

        FlashSort (logCS, spp, NSP);


        uint32_t cd2=0;

        for (int n=0; n<NSP; ++n) {

            spp[n]->cdos = cd2;

            int JA = spp[n]->GP->Ja;
            int JB = spp[n]->GP->Jb;
            int MA = 2*spp[n]->GP->l1+1;
            int MB = 2*spp[n]->GP->l2+1;

            cd2 += JA*JB*MA*MB;
        }

        delete[] spp;
        delete[] logCS;
    }

}

// compute the block diagonal basis
void Fock2e::CalcW2basis(tensor2 * Ws) {

    Echidna::EMessenger << "Compute the 2e basis";
    Echidna::EMessenger.Push(); {

        //two different atoms
        for (int n12=0; n12<APL->AtomPairBatch.n; ++n12) {
            const APbatch & APB12 = APL->AtomPairBatch[n12];

            if (APB12.APlist.n==0) continue; //no overlaps between these two specific elements

            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                if (APB12.nAPS[ab]==0) continue; //no overlaps between these two shell pair prototypes

                ERIBatch CSbatch(this);
                CSbatch.Set(ABAB, ABp, ABp, true);
                CSbatch.Ntiles64 = 1;
                for (int i=0; i<OGMT; ++i) BatchEval[i].SetBuffers(CSbatch);

                //buffers, sizes and other stuff needed for the calculation

                //ERI
                const AtomProd & AP12 = APB12.APlist[0];
                ShellPair * ABs = AP12.pSPs[ab];

                #pragma omp parallel for schedule(dynamic)
                for (int ap12=0; ap12<APB12.nAPS[ab]; ap12+=DOUBLES_PER_CACHE_LINE) {

                    ERIgeometry   eriG[DOUBLES_PER_CACHE_LINE];
                    ERIgeometries64  eriG8;

                    ERITile64 ET;

                    ET.nKab = ABp.Ka*ABp.Kb -1;
                    ET.nKcd = ABp.Ka*ABp.Kb -1;

                    int lenk = min (DOUBLES_PER_CACHE_LINE, APB12.nAPS[ab]-ap12);

                    for (int k=0; k<lenk; ++k) {
                        ET.ap12[k] = ap12+k;
                        ET.ap34[k] = ap12+k;
                    }
                    for (int k=lenk; k<DOUBLES_PER_CACHE_LINE; ++k) {
                        ET.ap12[k] = ap12; //+lenk-1;
                        ET.ap34[k] = ap12; //+lenk-1;
                    }

                    ET.wSP12 = ABs->WW;
                    ET.wSP34 = ABs->WW;
                    ET.nK2ab = ABp.Ka * ABp.Kb;
                    ET.nK2cd = ABp.Ka * ABp.Kb;

                    ET.lowgeom = ABAB;

                    for (int k=0; k<DOUBLES_PER_CACHE_LINE; ++k) {
                        const AtomProd & AP12 = APB12.APlist[ ET.ap12[k] ];
                        eriG[k].MakeRotation2S (AP12);
                        eriG[k].Adjust         (ABp.inverted, ABp.inverted);
                    }

                    PackGeometries           (eriG, eriG8);

                    int ogtn = omp_get_thread_num();

                    cacheline64 maxv;
                    maxv = BatchEval[ogtn].ERItrace(CSbatch, *ABs, eriG8, ET, lenk);

                    //set the Cauchy-Schwarz parameter
                    for (int k=0; k<lenk; ++k) {
                        double mv = maxv(k);
                        double logCS = (mv>0)?(-log(mv)/2):100;
                        ABs[ap12+k].logCS = logCS;
                    }
                }

            }
        }

        //same atom
        for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
            const APbatch & APB12 = APL->AtomSameBatch[n12];

            ERIgeometry eriG;
            point A(0,0,0);
            eriG.MakeRotation1(A);

            //#pragma omp parallel for schedule(dynamic)
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];
                const ShellPair & ABs = APB12.SP[ab][0];

                ERIBatch CSbatch(this);
                CSbatch.Ntiles64 = 1;
                CSbatch.Set(AAAA, ABp, ABp, true);
                BatchEval[0].SetBuffers(CSbatch);

                double mv = BatchEval[0].ERItrace(CSbatch, ABs,eriG);
                double logCS = (mv>0)?(-log(mv)/2):100;

                //set the cauchy-schwarz parameter to ALL atom pairs
                //**************************************************

                for (int ap12=0; ap12<APB12.APlist.n; ++ap12)  {
                    const AtomProd & AP12 = APB12.APlist[ap12];
                    ShellPair & ABs = *AP12.pSPs[ab];
                    ABs.logCS = logCS;
                }
            }
        }

    } Echidna::EMessenger.Pop();
}


void Fock2e::Init (const rAtom * atoms, int natoms) {

    // initialize pattern
    NTP = new Npattern;
    NTP->Set(atoms, natoms);

    //initialize density prescreen matrices
    {
        r1tensor<int> DPlengths(natoms);

        for (uint32_t at1=0; at1<natoms; ++at1) {
            uint8_t id1 = NTP->GetID(at1);
            uint8_t nb1 = NTP->GetFuncs(id1); // number of shells, actually

            DPlengths[at1] = nb1;
        }

        logDs.set(DPlengths, DPlengths);
        logAD.set(DPlengths, DPlengths);
    }

    //initialize density prescreen matrices
    {
        const uint8_t NID = NTP->GetNIDs();

        r1tensor<int> ebslen(NID);
        for (int id=0; id<NID; ++id) ebslen[id] = NTP->GetFuncs(id);

        lnDs.set(ebslen, ebslen);
        lnAD.set(ebslen, ebslen);

        for (int id1=0; id1<NID; ++id1)
            for (int id2=0; id2<NID; ++id2) {
                const int NF1 = NTP->GetFuncs(id1);
                const int NF2 = NTP->GetFuncs(id2);

                for (int nf1=0; nf1<NF1; ++nf1)
                    for (int nf2=0; nf2<NF2; ++nf2) {
                        lnDs[id1][id2][nf1][nf2].logs.set(natoms,natoms); // this is rather wasteful at the moment
                        lnAD[id1][id2][nf1][nf2].logs.set(natoms,natoms); // this is rather wasteful at the moment
                    }
            }

        /*
        const uint8_t NID = NTP->GetNIDs();

        lnDs.set(NID, NID);
        lnAD.set(NID, NID);

        for (int id1=0; id1<NID; ++id1)
            for (int id2=0; id2<NID; ++id2) {
                const int NF1 = NTP->GetFuncs(id1);
                const int NF2 = NTP->GetFuncs(id2);

                lnDs[id1][id2].set(NF1, NF2);
                lnAD[id1][id2].set(NF1, NF2);

                for (int nf1=0; nf1<NF1; ++nf1)
                    for (int nf2=0; nf2<NF2; ++nf2) {
                        lnDs[id1][id2][nf1][nf2].set(natoms,natoms); // this is rather wasteful at the moment
                        lnAD[id1][id2][nf1][nf2].set(natoms,natoms); // this is rather wasteful at the moment
                    }
            }
        */
    }


}

struct Update {

    tensor2 * D;
    tensor2 * F;

    double jw; // weight of J
    double kw; // weight for K

    bool symm; // contract symmetric components
    bool anti; // contract antisymmetric components
};

void Fock2e::FockUpdate(tensor2 * F, const tensor2 * D2, int Nmat, double Dthresh, double Sthresh, bool SplitJX) {

    // declare NTensors for DM and Fock
    NTconst Dsnt(NTP);
    NTconst Dant(NTP);
    NTmulti Jnt(NTP);
    NTmulti Xnt(NTP);
    NTmulti Ant(NTP);

    // just consider symmetric contributions for the moment
    Dsnt.SetS (D2, Nmat);
    Dant.SetA (D2, Nmat);
    Jnt.Set   (    Nmat);
    Xnt.Set   (    Nmat);
    Ant.Set   (    Nmat);


    //make a local copy of the number of matrices
    nDMs = Nmat;


    // skip the initialization of old Sparse matrices at the moment
    // take care of allocation/initialization of all necessary Sparse matrices
    Sparse * SDsparse;
    Sparse * ADsparse;

    if (0) {
        SDsparse = new Sparse[nDMs];
        ADsparse = new Sparse[nDMs];

        for (int p=0; p<nDMs; ++p) {
            SDsparse[p].Set(*STP);
            ADsparse[p].Set(*STP);

            SDsparse[p].zeroize();
            ADsparse[p].zeroize();
        }

        #ifdef USE_GPU
        if (UseGPU) {
            SDsparseGPU = new SparseGPU[nDMs];
            ADsparseGPU = new SparseGPU[nDMs];

            for (int n=0; n<OGMT; ++n) {
                BatchEval[n].Jgpu = new SparseGPU[nDMs];
                BatchEval[n].Xgpu = new SparseGPU[nDMs];
                BatchEval[n].Agpu = new SparseGPU[nDMs];
            }

            for (int p=0; p<nDMs; ++p) {
                SDsparseGPU[p].Set(*STP);
                ADsparseGPU[p].Set(*STP);

                for (int n=0; n<OGMT; ++n) {
                    BatchEval[n].Jgpu[p].Set(*STP);
                    BatchEval[n].Xgpu[p].Set(*STP);
                    BatchEval[n].Agpu[p].Set(*STP);
                }

                SDsparseGPU[p].Zeroize();
                ADsparseGPU[p].Zeroize();

                for (int n=0; n<OGMT; ++n) {
                    BatchEval[n].Jgpu[p].Zeroize();
                    BatchEval[n].Xgpu[p].Zeroize();
                    BatchEval[n].Agpu[p].Zeroize();
                }
            }
        }
        #endif

        //make a copy of the D2 tensor(s) to a format which exploits data locality and symmetry
        for (int n=0; n<nDMs; ++n) {
            SDsparse[n].GetSymmetric     (D2[n]);
            ADsparse[n].GetAntiSymmetric (D2[n]);

            #ifdef USE_GPU
            if (UseGPU) SDsparseGPU[n].Copy(SDsparse[n]);
            if (UseGPU) ADsparseGPU[n].Copy(ADsparse[n]);
            #endif
        }
    }


    //SetZeroDensityPrescreener();

    float logD, logS;
    // reset block counters
    //for (int p=0; p<OGMT; ++p) Blocks[p].Reset();

    if      (Dthresh == -3) {
        MediumDensityPrescreener();
        logD = logS = 10; // enough to compute the 1- and 2- center integrals, but not the rest
    }
    else if (Dthresh == -2) {
        CheapDensityPrescreener();
        logD = logS = 10; // enough to compute the 1- and 2- center integrals, but not the rest
    }
    else if (Dthresh <= 0) {
        SetZeroDensityPrescreener();
        logD = logS = 100;
    }
    else {
        CalcDensityPrescreeners (Dsnt, Dant);
        logD = -log(Dthresh); // -log(Dthresh * Athresh);
        logS = -log(Sthresh); // -log(Sthresh * Athresh);
    }



    //Echidna::EMessenger << "Computing two-electron contributions to the new Fock matrix";
    //Echidna::EMessenger.Push();



    Chronometer ERIchrono;
    ERIchrono.Start(); {

        //evaluate all n-center integrals, n>1
        //************************************
        if    (XS>0.) HybridParallelScheduler<true, true > (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant); // for DFT with no exact exchange contribution
        else          HybridParallelScheduler<true, false> (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant); // for DFT with exact exchange contribution or HF


        //1-center integrals
        // ****************
        //this solution is a bit unfortunate, but the proper solution involves
        //coding the many permutational symmetries and special cases

        if (NodeGroup.IsMaster()) {

            //loop over all elements
            for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {

                const APbatch & APB12 = APL->AtomSameBatch[n12];
                const AtomProdPrototype * APP = APB12.APP;

                ERIgeometry eriG;
                point A(0,0,0);
                eriG.MakeRotation1(A);


                //COULOMB
                //*******

                //calcula las integrales para SOLO un atomo de cada tipo
                #pragma omp parallel for schedule(dynamic)
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    for (int cd=0; cd<=ab; ++cd) {
                        const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                        if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                        if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                        ERIBatch C1batch(this);
                        C1batch.Ntiles64 = 1;

                        bool rev = InvertPrototypePair(ABp,CDp);
                        if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                        else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                        int ogtn = omp_get_thread_num();
                        BatchEval[ogtn].SetBuffers(C1batch);
                        BatchEval[ogtn].AAAA(C1batch, eriG);

                        for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                            const AtomProd & AP12 = APB12.APlist[ap12];
                            int at = AP12.at1;

                            BatchEval[ogtn].AddCoulomb(C1batch, Dsnt, Jnt, Nmat, at);
                        }

                    }
                }

                if (XS == 0.) continue; // skip exchange if not needed

                //EXCHANGE
                //********

                //don't merge with previous block for future fast Coulomb techniques
                #pragma omp parallel for schedule(dynamic)
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    for (int cd=0; cd<=ab; ++cd) {
                        const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                        if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                        if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                        ERIBatch C1batch(this);
                        C1batch.Ntiles64 = 2;

                        bool rev = InvertPrototypePair(ABp,CDp);
                        if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                        else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                        int ogtn = omp_get_thread_num();
                        BatchEval[ogtn].SetBuffers(C1batch);
                        if (CAMDFT) BatchEval[ogtn].AAAA (C1batch, eriG,  alpha, beta, mu*mu);
                        else        BatchEval[ogtn].AAAA (C1batch, eriG);

                        for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                            const AtomProd & AP12 = APB12.APlist[ap12];
                            int at = AP12.at1;

                            BatchEval[ogtn].AddExchange(C1batch, Dsnt, Xnt, Nmat, at);
                            BatchEval[ogtn].AddExchange(C1batch, Dant, Ant, Nmat, at);
                        }

                    }
                }

            }
        }
    }
    ERIchrono.Stop();


    if (PrintStatistics) {

        Q.Statistics();

        uint64_t NJs = 0;
        uint64_t NXs = 0;

        for (int i=0; i<OGMT; ++i) {
            NJs  += Blocks[i].nJs;
            NXs  += Blocks[i].nXs;
        }

        Echidna::EBenchmarker << "          Number of Coulomb block integrals  : " << NJs << endl;
        Echidna::EBenchmarker << "          Number of Exchange block integrals : " << NXs << endl;
    }

    //print more statistics
    if (0) {
        double tJs, pJs, tXs, pXs;
        tJs = pJs = tXs = pXs = 0;

        uint64_t nJs, mJs, nXs, mXs;
        nJs = mJs = nXs = mXs = 0;

        for (int i=0; i<OGMT; ++i) {
            tJs  += Blocks[i].tJs;
            pJs  += Blocks[i].pJs;
            tXs  += Blocks[i].tXs;
            pXs  += Blocks[i].pXs;

            nJs  += Blocks[i].nJs;
            mJs  += Blocks[i].mJs;
            nXs  += Blocks[i].nXs;
            mXs  += Blocks[i].mXs;
        }

        Echidna::EMessenger << "Total       J " << tJs << endl;
        Echidna::EMessenger << "Partial     J " << pJs << endl;
        Echidna::EMessenger << "Fraction of J " << pJs/tJs << endl;

        Echidna::EMessenger << "Total       X " << tXs << endl;
        Echidna::EMessenger << "Partial     X " << pXs << endl;
        Echidna::EMessenger << "Fraction of X " << pXs/tXs << endl;

        Echidna::EMessenger << "Total blocks   J " << mJs << endl;
        Echidna::EMessenger << "Partial blocks J " << nJs << endl;

        Echidna::EMessenger << "Total blocks   X " << mXs << endl;
        Echidna::EMessenger << "Partial blocks X " << nXs << endl;
    }


    // simple benchmark for streams
    #ifdef USE_GPU
    {
        double times[] = {0,0,0,0,0,0,0,0};

        uint64_t NJs = 0;
        uint64_t NXs = 0;


        for (int j=0; j<7; ++j) {
            Echidna::EMessenger << "Step  " << j << " :  ";
            for (int i=0; i<OGMT; ++i) {
                double t = BatchEval[i].GetTime(j);
                times[j] += t;
                Echidna::EMessenger << t << "  ";
            }
            Echidna::EMessenger << "   " << times[j] << endl;
         }

        //for (int j=0; j<7; ++j) Echidna::EMessenger << "Agregated times of all streams for step " << j << ":  " << times[j] << " sec." << endl;

        double Ttotal = 0; for (int j=0; j<7; ++j) Ttotal += times[j];

        Echidna::EMessenger << "Total time  : " << Ttotal << " sec." << endl;
        Echidna::EMessenger << "Time/thread : " << Ttotal/(OGMT) << " sec." << endl;
    }
    #endif


    //Echidna::EMessenger.Pop();


    //add each thread's contributions to the final fock matrix(ces)
    {

        // reduce GPU matrices (asynchronous)
        /*
        #ifdef USE_GPU
        if (UseGPU)
            for (int n=0; n<Nmat; ++n) {
                for (int p=1; p<OGMT; ++p) BatchEval[0].Jgpu[n] += BatchEval[p].Jgpu[n];
                if (XS>0.) for (int p=1; p<OGMT; ++p) BatchEval[0].Xgpu[n] += BatchEval[p].Xgpu[n];
                if (XS>0.) for (int p=1; p<OGMT; ++p) BatchEval[0].Agpu[n] += BatchEval[p].Agpu[n];
            }
        #endif
        */

        /*
        #ifdef USE_GPU
        // gather the GPU partial updates on host memory
        if (UseGPU)
            for (int n=0; n<Nmat; ++n) {
                BatchEval[0].Jresp[n].Reduce(BatchEval[0].Jgpu[n]);
                if (XS>0.) BatchEval[0].Xresp[n].Reduce(BatchEval[0].Xgpu[n]);
                if (XS>0.) BatchEval[0].Aresp[n].Reduce(BatchEval[0].Agpu[n]);
            }
        #endif
        */


        // clean previous results
        for (int n=0; n<Nmat; ++n)
            F[n].zeroize();

        // add Coulomb if interaction not split
        if (!SplitJX) {

            Jnt.AddTo(F); // get Coulomb contributions

            for (int n=0; n<Nmat; ++n)
            {
                F[n].Symmetrize(); //sums F and its transpose
            }
        }


        //add exchange contributions (if necessary)
        if (XS>0.) {

            const int dim = F[0].n;

            tensor2 X(dim), A(dim);

            for (int n=0; n<Nmat; ++n) {

                X.zeroize();
                A.zeroize();

                Xnt.AddTo(X,n);
                Ant.AddTo(A,n);

                X.Symmetrize();       // sums X and its transpose to obtain the symmetric exchange contributions
                A.AntiSymmetrize();   // substract A with its transpose to obtain the matrix of antisymmetric exchange contributions

                X += A;

                // scale the matrix (for DFT)
                // scale by half because of closed shell HF/DFT (i.e. 0.5*(2*J-X)=J-0.5*X)
                X *= 0.5*XS;

                //add it to Coulomb
                F[n] -= X;
            }
        }

        // reduce - broadcast at the MPI level
        for (int n=0; n<Nmat; ++n) {
            Echidna::EMessenger.Push();
            Tensor2Reduce(F[n]);
            Echidna::EMessenger.Pop();
        }
    }

    // clean matrices
    if (0) {
        delete[] SDsparse;
        delete[] ADsparse;
        SDsparse = NULL;
        ADsparse = NULL;


        #ifdef USE_GPU
        if (UseGPU) {
            delete[] SDsparseGPU;
            delete[] ADsparseGPU;
            SDsparseGPU = NULL;
            ADsparseGPU = NULL;

            for (int n=0; n<OGMT; ++n) {
                delete[] BatchEval[n].Jgpu;
                delete[] BatchEval[n].Xgpu;
                delete[] BatchEval[n].Agpu;
                BatchEval[n].Jgpu = NULL;
                BatchEval[n].Xgpu = NULL;
                BatchEval[n].Agpu = NULL;
            }
        }
        #endif
    }

}


// use this routine for everything
// ===============================

void Fock2e::ContractDensities (tensor2 * J, tensor2 * K, tensor2 * A, const tensor2 * D, int Nmat,    double Dthresh, double Sthresh, bool split) {


    // first set all matrices everything to 0

    if (J!=NULL)
        for (int n=0; n<Nmat; ++n)
            J[n].zeroize();

    if (K!=NULL)
        for (int n=0; n<Nmat; ++n)
            K[n].zeroize();

    if (A!=NULL)
        for (int n=0; n<Nmat; ++n)
            A[n].zeroize();


    bool UseJ, UseK, UseA;

    // check various counditions under which one or more blocks may be deactivated
    {
        // default
        UseJ=true;
        UseK=true;
        UseA=true;

        // skip correlation if pure DFT
        if (XS==0.) UseK = UseA = false;

        // skip any of the contractions if there are no matrices to write them to
        UseJ &= (J!=NULL);

        if (split) {
            UseK &= (K!=NULL);
            UseA &= (A!=NULL);
        }
    }


    // declare NTensors for DM and Fock
    NTconst Dsnt(NTP);
    NTconst Dant(NTP);
    NTmulti Jnt(NTP);
    NTmulti Xnt(NTP);
    NTmulti Ant(NTP);

    // jthis is extremely memory wasteful for no reason
    Dsnt.SetS (D,  Nmat);
    Dant.SetA (D,  Nmat);
    Jnt.Set   (    Nmat);
    Xnt.Set   (    Nmat);
    Ant.Set   (    Nmat);


    //make a local copy of the number of matrices
    nDMs = Nmat;


    // skip the initialization of old Sparse matrices at the moment
    // take care of allocation/initialization of all necessary Sparse matrices
    Sparse * SDsparse;
    Sparse * ADsparse;
    bool * asymm;

    if (1) {
        SDsparse = new Sparse[nDMs];
        ADsparse = new Sparse[nDMs];

        for (int p=0; p<nDMs; ++p) {
            SDsparse[p].Set(*STP);
            ADsparse[p].Set(*STP);

            SDsparse[p].zeroize();
            ADsparse[p].zeroize();
        }

        asymm =  new bool[Nmat];
        for (int p=0; p<nDMs; ++p) asymm[p] = false;

        #ifdef USE_GPU
        if (UseGPU) {
            SDsparseGPU = new SparseGPU[nDMs];
            ADsparseGPU = new SparseGPU[nDMs];

            for (int n=0; n<OGMT; ++n) {
                BatchEval[n].Jgpu = new SparseGPU[nDMs];
                BatchEval[n].Xgpu = new SparseGPU[nDMs];
                BatchEval[n].Agpu = new SparseGPU[nDMs];
            }

            for (int p=0; p<nDMs; ++p) {
                SDsparseGPU[p].Set(*STP);
                ADsparseGPU[p].Set(*STP);

                for (int n=0; n<OGMT; ++n) {
                    BatchEval[n].Jgpu[p].Set(*STP);
                    BatchEval[n].Xgpu[p].Set(*STP);
                    BatchEval[n].Agpu[p].Set(*STP);
                }

                SDsparseGPU[p].Zeroize();
                ADsparseGPU[p].Zeroize();

                for (int n=0; n<OGMT; ++n) {
                    BatchEval[n].Jgpu[p].Zeroize();
                    BatchEval[n].Xgpu[p].Zeroize();
                    BatchEval[n].Agpu[p].Zeroize();
                }
            }
        }
        #endif

        //make a copy of the D2 tensor(s) to a format which exploits data locality and symmetry
        for (int n=0; n<nDMs; ++n) {
            SDsparse[n].GetSymmetric     (D[n]);
            ADsparse[n].GetAntiSymmetric (D[n]);

            #ifdef USE_GPU
            if (UseGPU) SDsparseGPU[n].Copy(SDsparse[n]);
            if (UseGPU) ADsparseGPU[n].Copy(ADsparse[n]);
            #endif
        }
    }


    // compute screening parameters


    // adjust threshold to the number of shell pair interactions
    double FS = NTP->GetFuncs();
    // worst case scenario, NK is dense and has O(N^2) contributions
    // however, we expect these to behave like a random walk, so their impact is sqrt(N^2)
    double Athresh = 1./FS;

    // 100./(FS*FS);
    float logD, logS;
    // reset block counters
    //for (int p=0; p<OGMT; ++p) Blocks[p].Reset();

    if      (Dthresh == -3) {
        MediumDensityPrescreener();
        logD = logS = 10; // enough to compute the 1- and 2- center integrals, but not the rest
    }
    else if (Dthresh == -2) {
        CheapDensityPrescreener();
        logD = logS = 10; // enough to compute the 1- and 2- center integrals, but not the rest
    }
    else if (Dthresh == -1) {
        DensityFittingPrescreener (Dsnt);
        logD = logS = -log(Sthresh); // -log(Dthresh * Athresh);
    }
    else if (Dthresh <= 0) {
        SetZeroDensityPrescreener();
        logD = logS = 100;
    }
    else {
        CalcDensityPrescreeners (Dsnt, Dant);
        logD = -log(Dthresh); // -log(Dthresh * Athresh);
        logS = -log(Sthresh); // -log(Sthresh * Athresh);
    }

    // reset block counters
    //for (int p=0; p<OGMT; ++p) Blocks[p].Reset();

    //Echidna::EMessenger << "Computing two-electron contributions to the new Fock matrix";
    //Echidna::EMessenger.Push();



    Chronometer ERIchrono;
    ERIchrono.Start(); {

        #ifdef USE_GPU
        {
            //evaluate all n-center integrals, n>1
            //************************************
            if         (( UseJ) && ( UseK))    HybridParallelScheduler<true,  true > (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant);
            else if    (( UseJ) && (!UseK))    HybridParallelScheduler<true,  false> (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant); // no K required
            else if    ((!UseJ) && ( UseK))    HybridParallelScheduler<false, true > (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant); // no J required
            else       {
                // nothing to compute ??????
            }
        }
        #else
            //evaluate all n-center integrals, n>1
            //************************************
            if         (( UseJ) && ( UseK))    HybridParallelScheduler<true,  true > (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant,   SDsparse, ADsparse, SDsparseGPU, ADsparseGPU, asymm, nDMs);
            else if    (( UseJ) && (!UseK))    HybridParallelScheduler<true,  false> (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant,   SDsparse, ADsparse, SDsparseGPU, ADsparseGPU, asymm, nDMs); // no K required
            else if    ((!UseJ) && ( UseK))    HybridParallelScheduler<false, true > (logD, logS, Dsnt, Dant, Jnt,Xnt, Ant,   SDsparse, ADsparse, SDsparseGPU, ADsparseGPU, asymm, nDMs); // no J required
            else       {
                // nothing to compute ??????
            }
        #endif




        //1-center integrals
        // ****************
        //this solution is a bit unfortunate, but the proper solution involves
        //coding the many permutational symmetries and special cases

        if (NodeGroup.IsMaster()) {

            //loop over all elements
            for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {

                const APbatch & APB12 = APL->AtomSameBatch[n12];
                const AtomProdPrototype * APP = APB12.APP;

                ERIgeometry eriG;
                point A(0,0,0);
                eriG.MakeRotation1(A);


                //COULOMB
                //*******

                //calcula las integrales para SOLO un atomo de cada tipo
                #pragma omp parallel for schedule(dynamic)
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    for (int cd=0; cd<=ab; ++cd) {
                        const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                        if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                        if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                        ERIBatch C1batch(this);
                        C1batch.Ntiles64 = 1;

                        bool rev = InvertPrototypePair(ABp,CDp);
                        if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                        else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                        int ogtn = omp_get_thread_num();
                        BatchEval[ogtn].SetBuffers(C1batch);
                        BatchEval[ogtn].AAAA(C1batch, eriG);

                        for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                            const AtomProd & AP12 = APB12.APlist[ap12];
                            int at = AP12.at1;

                            if (UseJ) BatchEval[ogtn].AddCoulomb(C1batch, Dsnt, Jnt, Nmat, at);
                        }

                    }
                }

                if ((!UseK) && (!UseA)) continue;

                //EXCHANGE
                //********

                //don't merge with previous block for future fast Coulomb techniques
                #pragma omp parallel for schedule(dynamic)
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    for (int cd=0; cd<=ab; ++cd) {
                        const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                        if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                        if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                        ERIBatch C1batch(this);
                        C1batch.Ntiles64 = 2;

                        bool rev = InvertPrototypePair(ABp,CDp);
                        if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                        else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                        int ogtn = omp_get_thread_num();
                        BatchEval[ogtn].SetBuffers(C1batch);
                        if (CAMDFT) BatchEval[ogtn].AAAA (C1batch, eriG,  alpha, beta, mu*mu);
                        else        BatchEval[ogtn].AAAA (C1batch, eriG);

                        for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                            const AtomProd & AP12 = APB12.APlist[ap12];
                            int at = AP12.at1;

                            if (UseK) BatchEval[ogtn].AddExchange(C1batch, Dsnt, Xnt, Nmat, at);
                            if (UseA) BatchEval[ogtn].AddExchange(C1batch, Dant, Ant, Nmat, at);
                        }

                    }
                }

            }
        }
    }
    ERIchrono.Stop();


    if (PrintStatistics) {

        Q.Statistics();

        uint64_t NJs = 0;
        uint64_t NXs = 0;

        for (int i=0; i<OGMT; ++i) {
            NJs  += Blocks[i].nJs;
            NXs  += Blocks[i].nXs;
        }

        Echidna::EBenchmarker << "          Number of Coulomb block integrals  : " << NJs << endl;
        Echidna::EBenchmarker << "          Number of Exchange block integrals : " << NXs << endl;
    }

    //print more statistics
    if (0) {
        double tJs, pJs, tXs, pXs;
        tJs = pJs = tXs = pXs = 0;

        uint64_t nJs, mJs, nXs, mXs;
        nJs = mJs = nXs = mXs = 0;

        for (int i=0; i<OGMT; ++i) {
            tJs  += Blocks[i].tJs;
            pJs  += Blocks[i].pJs;
            tXs  += Blocks[i].tXs;
            pXs  += Blocks[i].pXs;

            nJs  += Blocks[i].nJs;
            mJs  += Blocks[i].mJs;
            nXs  += Blocks[i].nXs;
            mXs  += Blocks[i].mXs;
        }

        Echidna::EMessenger << "Total       J " << tJs << endl;
        Echidna::EMessenger << "Partial     J " << pJs << endl;
        Echidna::EMessenger << "Fraction of J " << pJs/tJs << endl;

        Echidna::EMessenger << "Total       X " << tXs << endl;
        Echidna::EMessenger << "Partial     X " << pXs << endl;
        Echidna::EMessenger << "Fraction of X " << pXs/tXs << endl;

        Echidna::EMessenger << "Total blocks   J " << mJs << endl;
        Echidna::EMessenger << "Partial blocks J " << nJs << endl;

        Echidna::EMessenger << "Total blocks   X " << mXs << endl;
        Echidna::EMessenger << "Partial blocks X " << nXs << endl;
    }


    // simple benchmark for streams
    #ifdef USE_GPU
    {
        double times[] = {0,0,0,0,0,0,0,0};

        uint64_t NJs = 0;
        uint64_t NXs = 0;


        for (int j=0; j<7; ++j) {
            Echidna::EMessenger << "Step  " << j << " :  ";
            for (int i=0; i<OGMT; ++i) {
                double t = BatchEval[i].GetTime(j);
                times[j] += t;
                Echidna::EMessenger << t << "  ";
            }
            Echidna::EMessenger << "   " << times[j] << endl;
         }

        //for (int j=0; j<7; ++j) Echidna::EMessenger << "Agregated times of all streams for step " << j << ":  " << times[j] << " sec." << endl;

        double Ttotal = 0; for (int j=0; j<7; ++j) Ttotal += times[j];

        Echidna::EMessenger << "Total time  : " << Ttotal << " sec." << endl;
        Echidna::EMessenger << "Time/thread : " << Ttotal/(OGMT) << " sec." << endl;
    }
    #endif


    //Echidna::EMessenger.Pop();


    //add each thread's contributions to the final fock matrix(ces)
    if (split) {

        //reduce GPU matrices (asynchronous)
        #ifdef USE_GPU
        if (UseGPU)
            for (int n=0; n<Nmat; ++n) {
                for (int p=1; p<OGMT; ++p) BatchEval[0].Jgpu[n] += BatchEval[p].Jgpu[n];
                if (XS>0.) for (int p=1; p<OGMT; ++p) BatchEval[0].Xgpu[n] += BatchEval[p].Xgpu[n];
                if (XS>0.) for (int p=1; p<OGMT; ++p) BatchEval[0].Agpu[n] += BatchEval[p].Agpu[n];
            }
        #endif

        /*
        #ifdef USE_GPU
        //gather the GPU computations on CPU (blocking)
        if (UseGPU)
            for (int n=0; n<Nmat; ++n) {
                BatchEval[0].Jresp[n].Reduce(BatchEval[0].Jgpu[n]);
                if (XS>0.) BatchEval[0].Xresp[n].Reduce(BatchEval[0].Xgpu[n]);
                if (XS>0.) BatchEval[0].Aresp[n].Reduce(BatchEval[0].Agpu[n]);
            }
        #endif
        */


        // get Coulomb contributions
        if (UseJ) {
            Jnt.AddTo(J);

            for (int n=0; n<Nmat; ++n) J[n].Symmetrize();   // sums J and its transpose

            for (int n=0; n<Nmat; ++n) Tensor2Reduce(J[n]); // collects all results
        }

        // get symmetric Exchange contributions
        if (UseK) {
            Xnt.AddTo(K);

            for (int n=0; n<Nmat; ++n) K[n].Symmetrize();   // sums K and its transpose

            for (int n=0; n<Nmat; ++n) Tensor2Reduce(K[n]); // collects all results
        }

        // get antisymmetric Exchange contributions
        if (UseA) {
            Ant.AddTo(A);

            for (int n=0; n<Nmat; ++n) A[n].AntiSymmetrize();   // sums J and its transpose

            for (int n=0; n<Nmat; ++n) Tensor2Reduce(A[n]); // collects all results
        }

    }
    else {
        Jnt.AddTo(J);

        for (int n=0; n<Nmat; ++n) J[n].Symmetrize();   // sums J and its transpose


        //add exchange contributions (if necessary)
        if (XS!=0.) {

            const int dim = J[0].n;

            tensor2 X(dim), A(dim);

            for (int n=0; n<Nmat; ++n) {

                if (UseK) {
                    Xnt.AddTo(X,n);
                    X.Symmetrize();       // sums X and its transpose to obtain the symmetric exchange contributions
                    X *= 0.5*XS;
                    J[n] -= X;
                }
                if (UseA) {
                    Ant.AddTo(A,n);
                    A.AntiSymmetrize();   // substract A with its transpose to obtain the matrix of antisymmetric exchange contributions
                    A *= 0.5*XS;
                    J[n] -= X;
                }

            }
        }

        for (int n=0; n<Nmat; ++n) Tensor2Reduce(J[n]); // collects all results
    }

    // clean matrices
    if (1) {
        delete[] SDsparse;
        delete[] ADsparse;
        SDsparse = NULL;
        ADsparse = NULL;
        delete[] asymm;


        #ifdef USE_GPU
        if (UseGPU) {
            delete[] SDsparseGPU;
            delete[] ADsparseGPU;
            SDsparseGPU = NULL;
            ADsparseGPU = NULL;

            for (int n=0; n<OGMT; ++n) {
                delete[] BatchEval[n].Jgpu;
                delete[] BatchEval[n].Xgpu;
                delete[] BatchEval[n].Agpu;
                BatchEval[n].Jgpu = NULL;
                BatchEval[n].Xgpu = NULL;
                BatchEval[n].Agpu = NULL;
            }
        }
        #endif
    }


}


// OLD SCHEDULER
template <bool J, bool X> void Fock2e::HybridParallelScheduler (float logD, float logS, const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant,
                                                                                        const Sparse * SDsparse, const Sparse * ADsparse, SparseGPU * SDsparseGPU, SparseGPU * ADsparseGPU,
                                                                                        bool * AntiSymm, int nDMs
                                                                ) {

    if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;


    //if (J && X)
    //    Echidna::EMessenger << "Computing 2 electron Fock contributions";
    //else if (J && !X)
    //    Echidna::EMessenger << "Computing Coulomb interaction";
    //else if (!J && X)
    //    Echidna::EMessenger << "Computing exchange interaction";

    //Echidna::EMessenger.Push();


    // copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it) {
            BatchInfo * BI =  new BatchInfo;
            *BI = *it;

            //Echidna::EMessenger << "geometry: " << int(BI->geometry) << " Nab: " << BI->tmax12 << " Ncd: " << BI->tmax34 << endl;

            BlockList.push(BI);
        }
    }

    // task queues
    TaskQueue<ERIBatch> GPUqueue;
    TaskQueue<ERIBatch> CPUqueue;




    // OLD SCHEDULER; INCLUDES GFU
    {
        volatile int nworkers = OGMT;

        #pragma omp parallel
        {
            Chronometer ThreadTime;
            ThreadTime.Start();

            uint32_t tn = omp_get_thread_num();

            bool worked = true;

            while (worked) {

                worked = false;

                //run GPU jobs
                #ifdef USE_GPU
                bool FreeStream;
                if (UseGPU) FreeStream = BatchEval[tn].IsFree();
                else        FreeStream = false;

                // if stream is idle, launch a new batch
                if (FreeStream) {
                    // pull a block from GPUqueue
                    ERIBatch * GPUbatch = GPUqueue.pop();

                    // evaluate the batch
                    if (GPUbatch!=NULL) {
                        BatchEval[tn].Evaluate (*GPUbatch, SDsparse, ADsparse, SDsparseGPU, ADsparseGPU, AntiSymm, nDMs);
                        GPUbatch->clear(EbatchBuffer);
                        delete GPUbatch;
                        worked = true;
                    }

                    // add one batch to the queue
                    //else
                    {
                        //for (int m=0; m<OGMT; ++m)
                        {
                            BatchInfo * Block = BlockList.pop(); // pop from top

                            if (Block!=NULL) {
                                bool finish;
                                Blocks[tn] = *Block;

                                if ( J &&  X) finish = Blocks[tn].MakeAllN      (logD, logS, lnAD, lnDs, GPUqueue);
                                if ( J && !X) finish = Blocks[tn].MakeCoulombN  (logD, logS, lnAD, lnDs, GPUqueue);
                                if (!J &&  X) finish = Blocks[tn].MakeExchangeN (logD, logS, lnAD, lnDs, GPUqueue);

                                if (finish) delete Block;
                                else        {
                                    Block->State    = Blocks[tn].State;
                                    BlockList.push_top(Block); // push top
                                }

                                worked = true;
                            }
                        }
                    }

                    if (GPUbatch==NULL) continue; // skip CPU task; start GPU stream
                }
                #endif

                //run CPU jobs
                {
                    // pull a block from CPUqueue

                    ERIBatch  * CPUbatch = NULL;
                    BatchInfo * Block    = NULL;

                                        CPUbatch = CPUqueue.pop();
                    if (CPUbatch==NULL) Block    = BlockList.pop_bottom();


                    // evaluate the batch
                    if (CPUbatch!=NULL) {

                        if (CAMDFT) BatchEval[tn].EvaluateCAM (*CPUbatch, nDMs,    Dsnt, Dant, Jnt, Xnt, Ant,     alpha, beta, mu*mu);
                        else        BatchEval[tn].Evaluate    (*CPUbatch, nDMs,    Dsnt, Dant, Jnt, Xnt, Ant);

                        CPUbatch->clear(EbatchBuffer);
                        delete CPUbatch;
                        worked = true;
                    }

                    // populates the queue with more batches
                    else if (Block!=NULL) {
                        bool finish;
                        Blocks[tn] = *Block;

                        if ( J &&  X) finish = Blocks[tn].MakeAll      (logD, logS, lnAD, lnDs, CPUqueue);
                        if ( J && !X) finish = Blocks[tn].MakeCoulomb  (logD, logS, lnAD, lnDs, CPUqueue);
                        if (!J &&  X) finish = Blocks[tn].MakeExchange (logD, logS, lnAD, lnDs, CPUqueue);

                        if (finish) delete Block;
                        else        {
                            Block->State    = Blocks[tn].State;
                            BlockList.push(Block); //push low
                            //BlockList.push_top(Block); //push low
                        }
                        worked = true;
                    }
                    else {
                        #pragma omp atomic
                        --nworkers;
                        worked = false;
                    }

                    // this was broken; check


                    // if the thread could not do anything
                    if (!worked) {
                        // all blocks taken
                        //if (BlockList.len==0)
                    }

                }

                /*
                // did not find a task
                else {
                    if (nblocks==0) // if there are no blocks left (this is only upated when it's destroyed)
                        if (CPUqueue.length()==0) // and the CPU queue is empty
                            worked = false; // exit the loop
                }
                */


            }

            ThreadTime.Stop();

            //Echidna::EMessenger << "thread " << tn << " finished after " << ThreadTime << endl;
        }

    }


    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (CPUqueue.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (GPUqueue.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    //Echidna::EMessenger.Pop();
}




// NEW SCHEDULER
template <bool J, bool X> void Fock2e::HybridParallelScheduler (float logD, float logS, const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant) {

    if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;


    //if (J && X)
    //    Echidna::EMessenger << "Computing 2 electron Fock contributions";
    //else if (J && !X)
    //    Echidna::EMessenger << "Computing Coulomb interaction";
    //else if (!J && X)
    //    Echidna::EMessenger << "Computing exchange interaction";

    //Echidna::EMessenger.Push();


    // copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it) {
            BatchInfo * BI =  new BatchInfo;
            *BI = *it;

            //Echidna::EMessenger << "geometry: " << int(BI->geometry) << " Nab: " << BI->tmax12 << " Ncd: " << BI->tmax34 << endl;

            BlockList.push(BI);
        }
    }

    // task queues
    TaskQueue<ERIBatch> GPUqueue;
    TaskQueue<ERIBatch> CPUqueue;




    {

        volatile int nblocks = BlockList.length();

        #pragma omp parallel
        {
            //Chronometer ThreadTime;
            //ThreadTime.Start();

            uint32_t tn = omp_get_thread_num();

            bool worked = true;


            while (worked) {

                // pull a block from CPUqueue

                ERIBatch  * CPUbatch = NULL;
                BatchInfo * Block    = NULL;

                                    CPUbatch = CPUqueue.pop();
                if (CPUbatch==NULL) Block    = BlockList.pop_bottom();


                // evaluate the batch
                if (CPUbatch!=NULL) {

                    if (CAMDFT) BatchEval[tn].EvaluateCAM (*CPUbatch, nDMs,    Dsnt, Dant, Jnt, Xnt, Ant,     alpha, beta, mu*mu);
                    else        BatchEval[tn].Evaluate    (*CPUbatch, nDMs,    Dsnt, Dant, Jnt, Xnt, Ant);

                    CPUbatch->clear(EbatchBuffer);
                    delete CPUbatch;
                }

                // populates the queue with more batches
                else if (Block!=NULL) {
                    bool finish;
                    Blocks[tn] = *Block;

                    if ( J &&  X) finish = Blocks[tn].MakeAll      (logD, logS, lnAD, lnDs, CPUqueue);
                    if ( J && !X) finish = Blocks[tn].MakeCoulomb  (logD, logS, lnAD, lnDs, CPUqueue);
                    if (!J &&  X) finish = Blocks[tn].MakeExchange (logD, logS, lnAD, lnDs, CPUqueue);

                    if (finish) {
                        delete Block;
                        #pragma omp atomic
                        --nblocks;
                    }
                    else        {
                        Block->State    = Blocks[tn].State;
                        BlockList.push(Block); //push low
                        //BlockList.push_top(Block); //push low
                    }
                }

                // did not find a task
                else {
                    if (nblocks==0) // if there are no blocks left (this is only upated when it's destroyed)
                        if (CPUqueue.length()==0) // and the CPU queue is empty
                            worked = false; // exit the loop
                }
            }

            //ThreadTime.Stop();
            //Echidna::EMessenger << "thread " << tn << " finished after " << ThreadTime << endl;
        }


    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (CPUqueue.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (GPUqueue.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    //Echidna::EMessenger.Pop();
}


// COMPUTE THE FULL 2-E TENSORS
// ============================
void Fock2e::Get2eTensor(tensor4 & W2, double thresh) {

    //compute screening parameters
    SetZeroDensityPrescreener();
    float logT = -log(thresh);

    //reset block counters
    //for (int p=0; p<OGMT; ++p) Blocks[p].Reset();

    Echidna::EMessenger << "Computing two-electron tensor";
    Echidna::EMessenger.Push();

    Chronometer ERIchrono;
    ERIchrono.Start(); {

        //evaluate all n-center integrals, n>1
        //************************************
        TensorParallelScheduler (W2, logT);


        //1-center integrals
        // ****************
        //this solution is a bit unfortunate, but the proper solution involves
        //coding the many permutational symmetries and special cases

        if (NodeGroup.IsMaster()) {

            int nbf = sqrt(W2.n);

            //loop over all elements
            for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
                const APbatch & APB12 = APL->AtomSameBatch[n12];
                const AtomProdPrototype * APP = APB12.APP;

                uint64_t wa = APB12.APP->wa1;
                uint64_t wa4;
                wa4 = wa * wa * wa * wa;


                ERIgeometry eriG;
                point A(0,0,0);
                eriG.MakeRotation1(A);


                //COULOMB
                //*******

                double * WWW = new double[wa4];

                for (int i=0; i<wa4; ++i) WWW[i]=0;

                //calcula las integrales para SOLO un atomo de cada tipo
                #pragma omp parallel for schedule(dynamic)
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    for (int cd=0; cd<=ab; ++cd) {
                        const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                        if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                        if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                        ERIBatch C1batch(this);
                        C1batch.Ntiles64 = 1;

                        bool rev = InvertPrototypePair(ABp,CDp);
                        if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                        else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                        int ogtn = omp_get_thread_num();
                        BatchEval[ogtn].SetBuffers(C1batch);
                        BatchEval[ogtn].ConstructW(C1batch, eriG,WWW,wa);
                    }
                }

                // copy data
                #pragma omp parallel for schedule(dynamic)
                for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                    const AtomProd & AP12 = APB12.APlist[ap12];
                    int at1 = AP12.at1;

                    /*
                    //int os1 = sparsetensorpattern::GetOffset(at1,at1);
                    // copy to every single location
                    for (int ii=0; ii<wa*wa; ++ii)
                        for (int jj=0; jj<wa*wa; ++jj)
                            W2 (os1+ii, os1+jj) = WWW[ii*wa*wa+jj];
                    */

                    int os = STP->a1pos[at1]; //sparsetensorpattern::a1pos[at1];

                    for (int i=0; i<wa; ++i)
                        for (int j=0; j<wa; ++j)
                            for (int k=0; k<wa; ++k)
                                for (int l=0; l<wa; ++l) {
                                    //int ij = (os+i)*nbf + (os+j);
                                    //int kl = (os+k)*nbf + (os+l);
                                    int ijkl = i*wa*wa*wa + j*wa*wa + k*wa + l;
                                    W2(os+i,os+j,os+k,os+l) = WWW[ijkl];
                                    //W2(ij,kl) = WWW[ijkl];
                                }

                }

                delete[] WWW;
            }
        }
    }
    ERIchrono.Stop();

    Echidna::EMessenger.Pop();
}


void Fock2e::TensorParallelScheduler (tensor4 & W, float logT) {

    enum JobDescription{JOB_NONE, JOB_MAKE_BATCH, JOB_COMPUTE_BATCH};

    //if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;

    Echidna::EMessenger << "Computing 2 electron tensor";

    Echidna::EMessenger.Push();


    //copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it) {
            BatchInfo * BI =  new BatchInfo;
            *BI = *it;

            BlockList.push(BI);
        }
    }

    TaskQueue<ERIBatch> OCLDList;

    //begin
    volatile int workingthreads = OGMT;

    #pragma omp parallel shared(workingthreads)
    {
        uint32_t tn = omp_get_thread_num();
        bool lastfailed = false;

        ERIBatch  * OCLD;
        BatchInfo * Block;

        while (workingthreads>0 || !lastfailed ) {

            JobDescription JobType = JOB_NONE;


            //first try to retrieve a batch
            OCLD = OCLDList.pop();

            if (OCLD!=NULL) JobType = JOB_COMPUTE_BATCH;
            //if it doesn't succeed, try to retrieve a block from the queue
            else {
                Block = BlockList.pop();
                if (Block!=NULL) JobType = JOB_MAKE_BATCH;
            }

            //never actually executed
            if (lastfailed && JobType!=JOB_NONE) {
                lastfailed = false;
                #pragma omp atomic
                ++workingthreads;
            }

            //OCLD PATHJOB_MAKE_BATCH
            if      (JobType==JOB_COMPUTE_BATCH) {
                BatchEval[tn].EvaluateT (*OCLD, W, STP);

                OCLD->clear(EbatchBuffer);
                delete OCLD; //recycle ?
            }
            //
            else if (JobType==JOB_MAKE_BATCH) {

                bool finish;
                Blocks[tn] = *Block;

                finish = Blocks[tn].MakeCoulomb (logT, logT, lnDs, lnDs, OCLDList); // only Coulomb required

                if (finish) delete Block;
                else        {
                    Block->State    = Blocks[tn].State;
                    BlockList.push_top(Block);
                }
            }

            else {
                if (!lastfailed) {
                    lastfailed = true;
                    #pragma omp atomic
                    --workingthreads;
                }
            }
        }
    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (OCLDList.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    Echidna::EMessenger.Pop();
}


#include "linear/eigen.hpp"

static inline uint64_t sidx(uint64_t i, uint64_t j) {
    return (i*i+i)/2 + j;
}

// ======================================================
// COMPUTE THE CHOLESKY FACTORIZATION OF THE 2-ERI TENSOR
// ======================================================
//
// 1) think about what to do with the interface: who holds the matrices? how do we call the method?
//
// 2) some basic optimization: compress the blocks from the beginning by diagonalizing and discarding all components already below some threshold
// 3) do not necessarily add all vectors from a block to the basis; rather, wait until other larger components from other blocks have been added
// 4) we can skip computing interactions with |CD) pairs once their residual trace falls below the threshold; in principle this would result in much sparser vectors later on; this can also be done when contracting the ERIs
// 5) actually, instead of picking the largest trace we should pick the block with the largest first eigenvalue; store the remaining computed matrices for later, rather than recomputing
//
// WARNING!
// for low threshold decompositions, there seems to be some accumulation of roundoff towards the end, which can trigger exiting before completing the basis


void Fock2e::FactorCholesky (double thresh) {


    const double CSthresh = 1.e-40;
    const float logT  = -log(thresh);    // CD threshold
    const float logCS = -log(CSthresh);  // CS thresh set to not skip anything

    // compute screening parameters
    SetZeroDensityPrescreener();


    Echidna::EMessenger << "Computing new Cholesky Decomposition of the two-electron tensor";
    Echidna::EMessenger.Push();


    // ==================================
    // the algorithm should run in O(N^3)
    // ==================================
    // use CD products increasingly less overlapping (CS); update the diagonal accordingly

    // algorithm returns or stores the CD vectors in order to contract them with incoming densities

    const int NBF = NTP->GetTotalLen(); // total number of basis functions
    const int N = STP->natoms;
    const int NBF2 = (NBF*NBF+NBF)/2; // symmetrized dimension of AO product space

    // compute the full (AB|AB) block
    // ==============================

    r2tensor<tensor4> W2B(N,N); {

        // initialize each subtensor with the number of basis functions, squared
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);

                W2B(n,m).Set(BN,BM,BN,BM); // in general many will be empty
            }
        }

        // compute (AB|AB)
        ABABParallelScheduler (W2B, logCS);

        // add (AA|AA)
        for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
            const APbatch & APB12 = APL->AtomSameBatch[n12];
            const AtomProdPrototype * APP = APB12.APP;

            const uint64_t wa = APB12.APP->wa1;
            tensor4 WWW(wa);

            ERIgeometry eriG;
            point A(0,0,0);
            eriG.MakeRotation1(A);


            //calcula las integrales para SOLO un atomo de cada tipo
            #pragma omp parallel for schedule(dynamic)
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<=ab; ++cd) {
                    const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                    if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                    if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                    ERIBatch C1batch(this);
                    C1batch.Ntiles64 = 1;

                    bool rev = InvertPrototypePair(ABp,CDp);
                    if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                    else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                    int ogtn = omp_get_thread_num();
                    BatchEval[ogtn].SetBuffers(C1batch);
                    BatchEval[ogtn].ConstructW(C1batch, eriG, WWW.T.c2, wa);
                }
            }

            // copy data
            #pragma omp parallel for schedule(dynamic)
            for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                const AtomProd & AP12 = APB12.APlist[ap12];
                const int at1 = AP12.at1;

                // this does not modify shape or size
                //W2B(at1,at1).T = WWW.T;

                const int wa2 = (wa*wa+wa)/2;

                // skip repeating entries
                tensor2 & W1 = W2B(at1,at1).T;
                W1.setsize(wa2);

                // copy, adapting the format for symmetry

                for (int i=0; i<wa; ++i)
                    for (int j=0; j<=i; ++j)
                        for (int k=0; k<wa; ++k)
                            for (int l=0; l<=k; ++l) {
                                const int ij = (i*i+i)/2 + j;
                                const int kl = (k*k+k)/2 + l;

                                W1(ij,kl) = WWW(i,j,k,l);
                            }

                // half for i==j or k==l

                for (int i=0; i<wa; ++i)
                    for (int kl=0; kl<wa2; ++kl) {
                        const int ii = (i*i+i)/2 + i;
                        W1(ii,kl) *= 0.5;
                    }

                for (int k=0; k<wa; ++k)
                    for (int ij=0; ij<wa2; ++ij) {
                        const int kk = (k*k+k)/2 + k;
                        W1(ij,kk) *= 0.5;
                    }

            }

        }

        // just in case
        for (int n=0; n<N; ++n) {
            for (int m=0; m<=n; ++m) {
                W2B(n,m).T.Symmetrize();
                W2B(n,m).T *= 0.5;
            }
        }

    }

    r2tensor<tensor2> B2(N,N); // save the basis for rotations, just in case

    int KK = 0;
    int KKK = 0;
    for (int n=0; n<N; ++n) {
        for (int m=0; m<=n; ++m) {

            const int DNM = W2B(n,m).T.n;
            tensor2 w2b(1,DNM);

            B2(n,m) = W2B(n,m).T;

            B2(n,m) *= -1;
            DiagonalizeV(B2(n,m), w2b.c2);
            w2b *= -1;

            int K;
            for (K=0; K<DNM; ++K) if (w2b(0,K)<thresh) break;

            B2(n,m).n = K;

            Echidna::EMessenger << "block: " << n << " " << m << " evs:  " << K << " / " << DNM << endl;
            KK+= K;

            if (n==m) KKK += K;
        }
    }
    Echidna::EMessenger << "total vecs in W2 to consider:  " << KK <<  " / " << NBF2 << endl;

    // !!! it is possible to use the previous AA & AB local bases to reduce the N2 dimension to KK
    //     then we can reorder the vectors according to eigenvalue (which will act as pivot);

    //     alternatively, we can try the following: compute all KK vectors, contracting them immediately will all KK vectors;
    //     this will form a matrix of dimension KK x KK, which should be easier to decompose in O(KK^2 M) with a regular ev routine
    //     after that, do the SVD

    //     in any case the cost of storing these projectors is minimal



    // first compute same-atom centers
    // this is unfortunately way too expensive (O(N^4))
    // also, it doesn't seem to do all that much other than make the dimension KK lower (which could be an advantage for larger systems)

    tensor2 vecsAC;
    int KAC = 0;

    if (0) {

        // compute the full (AA|CC) block
        // ==============================

        int NB2;

        r2tensor<tensor4> W2(N,N); {

            NB2 = 0;

            // initialize each subtensor with the number of basis functions, squared
            for (int n=0; n<N; ++n) {
                const int BN = STP->GetLen1(n);

                for (int m=0; m<N; ++m) {
                    const int BM = STP->GetLen1(m);

                    W2(n,m).Set(BN,BM);
                }

                NB2 += (BN*BN+BN)/2;
            }

            // compute AACC
            TwoCenterParallelScheduler   (W2, logT);

            // now add AAAA
            for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
                const APbatch & APB12 = APL->AtomSameBatch[n12];
                const AtomProdPrototype * APP = APB12.APP;

                const uint64_t wa = APB12.APP->wa1;
                tensor4 WWW(wa);

                ERIgeometry eriG;
                point A(0,0,0);
                eriG.MakeRotation1(A);


                //calcula las integrales para SOLO un atomo de cada tipo
                #pragma omp parallel for schedule(dynamic)
                for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                    const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    for (int cd=0; cd<=ab; ++cd) {
                        const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                        if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                        if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                        ERIBatch C1batch(this);
                        C1batch.Ntiles64 = 1;

                        bool rev = InvertPrototypePair(ABp,CDp);
                        if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                        else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                        int ogtn = omp_get_thread_num();
                        BatchEval[ogtn].SetBuffers(C1batch);
                        BatchEval[ogtn].ConstructW(C1batch, eriG, WWW.T.c2, wa);
                    }
                }

                // copy data
                #pragma omp parallel for schedule(dynamic)
                for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                    const AtomProd & AP12 = APB12.APlist[ap12];
                    const int at1 = AP12.at1;

                    // this does not modify shape or size
                    W2(at1,at1).T = WWW.T;
                }

            }

        }

        // copy just the symmetrized stuff
        tensor2 Waacc (NB2,NB2);

        {
            Waacc.zeroize();

            for (int A=0, OSA=0; A<N; ++A) {
                const int BFA = STP->GetLen1(A);

                for (int C=0, OSC=0; C<N; ++C) {
                    const int BFC = STP->GetLen1(C);

                    for (int i=0; i<BFA; ++i)
                        for (int j=0; j<=i; ++j)
                            for (int k=0; k<BFC; ++k)
                                for (int l=0; l<=k; ++l) {
                                    const int ij = OSA+sidx(i,j);
                                    const int kl = OSC+sidx(k,l);
                                    Waacc(ij,kl) = W2(A,C)(i,j,k,l);
                                    if (i==j) Waacc(ij,kl) *= 0.5;
                                    if (k==l) Waacc(ij,kl) *= 0.5;
                                }
                    OSC += (BFC*BFC+BFC)/2;
                }
                OSA += (BFA*BFA+BFA)/2;
            }
        }

        // find evalues

        tensor2 wac(1,NB2);
        Waacc *= -1;
        DiagonalizeV(Waacc, wac[0]);
        wac *= -1;

        Echidna::EMessenger.precision(16);

        Echidna::EMessenger << "eigenvalues of AACC = " << wac << endl;

        for (KAC=0; KAC<NB2; ++KAC)
            if (wac(0,KAC)<thresh) break;

        Echidna::EMessenger << "number of admitted eigenvalues of AACC = " << KAC<< endl;

        for (int k=0; k<KAC; ++k)
            for (int l=0; l<NB2; ++l)
                Waacc(k,l) *= 1./sqrt(wac(0,k));


        // contract (AA|CD)
        {
            vecsAC.setsize(KAC, NBF2);

            vecsAC.zeroize();

            const int KBATCH = 200;

            // set & contract the pseudodensity matrices

            tensor2 * J = new tensor2[KBATCH];
            tensor2 * D = new tensor2[KBATCH];

            for (int k=0; k<KBATCH; ++k) D[k].setsize(NBF);
            for (int k=0; k<KBATCH; ++k) J[k].setsize(NBF);

            for (int kk=0; kk<KAC; kk+=KBATCH) {

                const int KMAX = min(KBATCH, KAC-kk);

                for (int k=0; k<KMAX; ++k) {

                    D[k].zeroize();
                    J[k].zeroize();

                    int OS2=0;

                    for (int n=0; n<N; ++n) {
                        const int BF = STP->GetLen1(n);
                        const int OS = STP->GetOffset1(n);

                        for (int i=0; i<BF; ++i) {
                            for (int j=0; j<BF; ++j) {
                                const int ij = sidx(max(i,j),min(i,j)); //   ( (i>=j) ? (i*i+i)/2+j:(j*j+j)/2+i );

                                D[k](OS+i,OS+j) += Waacc(kk+k,OS2+ij);
                            }
                        }

                        const int BF2 = (BF*BF+BF)/2;
                        OS2 += BF2;
                    }

                    // fix the factor of 2 for closed shell
                    D[k] *= 0.5;
                }

                // form the coulomb matrices
                ContractDensities (J, NULL, NULL, D, KMAX,    CSthresh, CSthresh); // the Coulomb prescreeners will keep the cost of this down to O(K N^2)



                // copies new vectors to the basis, halving the diagonal elements
                for (int k=0; k<KMAX; ++k)
                    for (int i=0; i<NBF; ++i) {
                        for (int j=0; j<i; ++j)
                            vecsAC(kk+k, sidx(i,j) ) = J[k](i,j);
                        vecsAC(kk+k, sidx(i,i) ) = 0.5* J[k](i,i);
                    }

            }

            delete[] D;
            delete[] J;
        }


        // now update
        KK = 0;
        KKK = 0;

        for (int n=0; n<N; ++n) {
            for (int m=0; m<=n; ++m) {
                const int DNM = W2B(n,m).T.n;
                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);
                const int ON = STP->GetOffset1(n);
                const int OM = STP->GetOffset1(m);

                tensor2 & WW = W2B(n,m).T;


                tensor2 TT(KAC, DNM);

                if (n!=m) {
                    for (int p=0; p<KAC; ++p)
                        for (int i=0; i<BN; ++i)
                            for (int j=0; j<BM; ++j) {
                                const int ij = sidx(ON+i,OM+j);
                                TT(p, i*BM+j) = vecsAC(p,ij);
                            }
                }
                else {
                    for (int p=0; p<KAC; ++p)
                        for (int i=0; i<BN; ++i)
                            for (int j=0; j<=i; ++j) {
                                const int ij = sidx(ON+i,OM+j);
                                TT(p, sidx(i,j)) = vecsAC(p,ij);
                            }
                }

                TensorProductNTN (WW, TT, TT, -1.);

                tensor2 w2b(1,DNM);
                WW *= -1;
                DiagonalizeE(WW, w2b.c2);
                WW *= -1;
                w2b *= -1;

                int K;
                for (K=0; K<DNM; ++K) if (w2b(0,K)<thresh) break;

                Echidna::EMessenger << "block: " << n << " " << m << " evs:  " << K << " / " << DNM << endl; // << "   " << w2b << endl;
                KK += K;

                if (n==m) KKK += K;
            }
        }

        Echidna::EMessenger << "vecs in W2 to consider after update:  " << KK <<  " / " << NBF2 << endl;
    }



    // LAZY-EVALUATED BLOCK APPROACH
    // =============================

    KK += KAC;

    if (KK>20*NBF) Echidna::EMessenger << "Warning! number of ERI block eigenvalues " << KK << " is larger than maximum allowed subspace dimension " << 20*NBF << "  (this will not necessarily cause any issues) " << endl;


    const int MAXM = min(KK,20*NBF);

    // dimension of subspace
    int M;
    int it;

    CDdata.vecs.setsize(MAXM, NBF2);
    CDdata.vecs.zeroize();
    CDdata.sigma = new double[MAXM];

    // append the AACC basis
    {
        for (int k=0; k<KAC; ++k) {
            for (int ij=0; ij<NBF2; ++ij) {
                CDdata.vecs(k, ij) = vecsAC(k,ij);
            }
        }

        M=KAC;
    }


    {
        /*
        // traces for each block in order to select pivots and do CS
        symtensor2 traces (N);
        traces.zeroize();

        for (int n=0; n<N; ++n)
            for (int m=0; m<=n; ++m)
                traces(n,m) = W2B(n,m).T.trace();

        // total trace
        double Wtrace = 0;

        for (int n=0; n<N; ++n)
            for (int m=0; m<=n; ++m)
                Wtrace += traces(n,m);
        */

        it = 0;


        while (true) {

            Echidna::EMessenger.precision(16);

            Echidna::EMessenger << "iteration = " << it << "    CD dimension = " << M << endl; //"   global W trace = " << Wtrace << endl;
            //Echidna::EMessenger << "block traces: " << traces << endl << endl;

            if (M==MAXM) break; // no more place for vectors


            // step 1: find the diagonal (AB*|AB*) block with largest trace
            // ============================================================

            // in theory and if everything is sorted, we can skip computing the trace for most blocks

            int A,B; {
                double maxtr=-1;

                for (int n=0; n<N; ++n)
                    for (int m=0; m<=n; ++m) {
                        double tr = W2B(n,m).T.trace();

                        if (tr>maxtr) {
                            maxtr = tr;
                            A = n;
                            B = m;
                        }
                    }

                if (maxtr < thresh) break;

                Echidna::EMessenger << "block selected = " << A << " " << B << "      block trace = " << maxtr << endl;
            }

            // some constants
            const int BFA = STP->GetLen1(A);
            const int BFB = STP->GetLen1(B);
            const int OSA = STP->GetOffset1(A);
            const int OSB = STP->GetOffset1(B);

            const int BF2 = ( (A==B)?(BFA*BFA+BFA)/2:BFA*BFB );


            // step 2: form the residual (AB|AB) & diagonalize
            // ===============================================

            tensor2 Wab; // "eigendensities"
            int K;       // number of new elements

            tensor2 T; // for later use

            {
                // cache a local copy of the necessary entries
                T.setsize(M, BF2);

                if (A!=B) {
                    for (int m=0; m<M; ++m)
                        for (int i=0; i<BFA; ++i)
                            for (int j=0; j<BFB; ++j)
                                T(m, i*BFB+j) = CDdata.vecs (m, sidx(OSA+i,OSB+j) );
                }
                else {
                    for (int m=0; m<M; ++m)
                        for (int i=0; i<BFA; ++i)
                            for (int j=0; j<=i; ++j)
                                T(m, sidx(i,j) ) = CDdata.vecs (m, sidx(OSA+i,OSB+j) );
                }

                // generate the block
                Wab = W2B(A,B).T;

                // update block
                //TensorProductNTN (Wab, T, T, -1.);


                // diagonalize; sort evs in descending order
                tensor2 wab(1, BF2);
                Wab *= -1;
                DiagonalizeV (Wab, wab[0]);
                wab *= -1;

                // number of vecs to use
                for (K=0; K<BF2; ++K)
                    if (wab(0,K) < thresh) break;

                if (M+K>MAXM) K = max(0,MAXM-M); // do not go over dimension

                if (K==0) break;


                // make scaled eigendensities
                wab.m = K;
                for (int k=0; k<K; ++k) {
                    const double s = 1./sqrt(wab(0,k));

                    for (int ij=0; ij<BF2; ++ij)
                        Wab(k,ij) *= s;
                }

                Echidna::EMessenger << "number of new CD vectors from the block = " << K << "    out of = " << Wab.m << endl;

                Echidna::EMessenger << "block eigenvalues : "<< endl << wab << endl;
            }



            // step 3: form the new orthogonal (ab|CD) Cholesky vectors by forming the contraction
            //         between the eigenvectors (treated as pseudodensities) W tensors
            // ===================================================================================


            // set & contract the pseudodensity matrices
            {
                tensor2 * J = new tensor2[K];
                tensor2 * D = new tensor2[K];

                for (int k=0; k<K; ++k) D[k].setsize(NBF);
                for (int k=0; k<K; ++k) J[k].setsize(NBF);

                for (int k=0; k<K; ++k) {

                    D[k].zeroize();
                    J[k].zeroize();

                    if (A!=B) {
                        for (int i=0; i<BFA; ++i) {
                            for (int j=0; j<BFB; ++j) {
                                const int ij = i*BFB+j;
                                D[k](OSA+i,OSB+j) += Wab(k,ij);
                                D[k](OSB+j,OSA+i) += Wab(k,ij);
                            }
                        }
                    }
                    else {
                        for (int i=0; i<BFA; ++i) {
                            for (int j=0; j<BFA; ++j) {
                                const int ij = sidx(max(i,j),min(i,j)); //   ( (i>=j) ? (i*i+i)/2+j:(j*j+j)/2+i );

                                D[k](OSA+i,OSA+j) += Wab(k,ij);
                            }
                        }
                    }

                    // fix the factor of 2 for closed shell
                    D[k] *= 0.5;
                }

                // form the coulomb matrices
                ContractDensities (J, NULL, NULL, D, K,    CSthresh, CSthresh); // the Coulomb prescreeners will keep the cost of this down to O(K N^2)

                delete[] D;

                // copies new vectors to the basis, halving the diagonal elements
                for (int k=0; k<K; ++k)
                    for (int i=0; i<NBF; ++i) {
                        for (int j=0; j<i; ++j)
                            CDdata.vecs(M+k, sidx(i,j) ) = J[k](i,j);
                        CDdata.vecs(M+k, sidx(i,i) ) = 0.5* J[k](i,i);
                    }


                delete[] J;
            }


            // !!! this is actually quite expensive and requires proper BLAS3 primitives

            // remove CD components from previous basis elements & store
            if (M>0) {
                // compute the contribution of each old vector to the new ones

                tensor2 C; // contributions to the CD vectors from previous ones
                {
                    tensor2 W (K, BF2, Wab[0]); // dummy array mocking the shape of Wab down to the actual number of eigenvectors being used
                    TensorProductNNT (C, W, T); // Wab(k,ij) * T(m,ij)
                }

                // substract the nonorthogonal components
                {
                    // mock the vecs array into two different blocks with the old and new vectors
                    tensor2 CDold (M,NBF2, CDdata.vecs[0]);
                    tensor2 CDnew (K,NBF2, CDdata.vecs[M]);

                    TensorProduct (CDnew, C, CDold, -1.);
                }
            }

            // update block & global traces
            {
                /*
                // sum all contributions to the ijij diagonal
                tensor2 CD2(1,NBF2);
                CD2.zeroize();

                for (int k=0; k<K; ++k) {
                    for (int ij=0; ij<NBF2; ++ij)
                        CD2[0][ij] += CDdata.vecs[M+k][ij] * CDdata.vecs[M+k][ij];
                }
                */

                // figure out to which trace these correspond
                for (int n=0; n<N; ++n) {
                    const int BFN = STP->GetLen1(n);
                    const int OSN = STP->GetOffset1(n);

                    for (int m=0; m<n; ++m) {
                        const int BFM = STP->GetLen1(m);
                        const int OSM = STP->GetOffset1(m);

                        tensor2 & WW = W2B(n,m).T;
                        tensor2 TT(K, BFN*BFM);

                        for (int p=0; p<K; ++p)
                            for (int i=0; i<BFN; ++i)
                                for (int j=0; j<BFM; ++j) {
                                    const int ij = sidx(OSN+i,OSM+j);
                                    TT(p, i*BFM+j) = CDdata.vecs(M+p,ij);
                                }
                        TensorProductNTN (WW, TT, TT, -1.);

                        /*
                        double s2=0;

                        for (int i=0; i<BFN; ++i)
                            for (int j=0; j<BFM; ++j)
                                //s2 += CD2(OSN+i, OSM+j);
                                s2 += CD2[0][sidx(OSN+i,OSM+j)];

                        traces(n,m) -= s2;
                        Wtrace      -= s2;
                        */
                    }

                    {
                        tensor2 & WW = W2B(n,n).T;
                        tensor2 TT(K, (BFN*BFN+BFN)/2);

                        for (int p=0; p<K; ++p)
                            for (int i=0; i<BFN; ++i)
                                for (int j=0; j<=i; ++j) {
                                    const int ij = sidx(OSN+i,OSN+j);
                                    TT(p, sidx(i,j)) = CDdata.vecs(M+p,ij);
                                }

                        TensorProductNTN (WW, TT, TT, -1.);

                        /*
                        double s2=0;

                        for (int i=0; i<BFN; ++i)
                            for (int j=0; j<=i; ++j)
                                //s2 += CD2(OSN+i, OSN+j);
                                s2 += CD2[0][sidx(OSN+i,OSN+j)];

                        traces(n,n) -= s2;
                        Wtrace      -= s2;
                        */
                    }
                }

            }

            // increase subspace
            M += K;
            ++it;
        }


        Echidna::EMessenger.precision(6);
    }


    // do the SVD for maximum efficiency
    // of a truntated representation
    {
        CDdata.NDIM = NBF;            // save the logical dimension of the packed symmetric matrix, which may change in the future
        CDdata.NVECS = CDdata.vecs.n; // save the total size of the array just in case
        CDdata.vecs.n = M;


        Echidna::EMessenger << "computing SVD" << endl;

        tensor2 U(M);

        int R = SVD (CDdata.vecs, U, CDdata.sigma);

        Echidna::EMessenger << "new rank R = " << R << "   from M = " << M << "  and KK = " << KK << endl;
        Echidna::EMessenger << "singular values: " << endl;
        for (int m=0; m<M; ++m) Echidna::EMessenger << CDdata.sigma[m] << "  "; Echidna::EMessenger << endl;
    }

    Echidna::EMessenger.Pop();
}

// computes only the Coulomb-like contribution of an array of (pseudo)density matrices
void Fock2e::ContractCholesky (tensor2 * J, const tensor2 * D, int Nmat, double precision) {

    const int NBF  = CDdata.NDIM; // total number of basis functions
    const int NBF2 = (NBF*NBF+NBF)/2;    // symmetrized dimension of AO product space


    // determine the number of basis elements to be used:
    // & mock a smaller basis if necessary
    const int MM = CDdata.Nvectors(precision);

    // create a mock re-shaped (smaller) array in order to use DGEMM
    tensor2 CDvecs (MM, NBF2, CDdata.vecs[0]);

    const int M    = CDvecs.n;           // dimension of the Cholesky/SVD basis


    // copy the matrices to a compact format, symmetrizing them in the process
    tensor2 DD (Nmat, NBF2);

    for (int n=0; n<Nmat; ++n) {
        for (int i=0; i<NBF; ++i) {
            for (int j=0; j<=i; ++j)
                DD(n, sidx(i,j)) = D[n](i,j) + D[n](j,i); // notice the diagonal is counted twice
        }
    }

    // compute the contractions with each element of the SVD basis:
    tensor2 WS; // Nmat x M
    TensorProductNNT (WS, DD, CDvecs);

    //Echidna::EMessenger << "DD projected in CD basis: " << endl << WS << endl << endl;


    // compute the residuals (to estimate the error norm)
    TensorProduct (DD, WS, CDvecs, -1.);


    // now scale with the corresponding weights

    for (int n=0; n<Nmat; ++n) {
        //double E2 = 0;
        for (int m=0; m<M; ++m) {
            //E2 += WS (n,m) * WS (n,m) * CDdata.sigma[m]*CDdata.sigma[m];
            WS (n,m) *= CDdata.sigma[m]*CDdata.sigma[m];
        }
        //Echidna::EMessenger << "Coulomb energy for matrix " << n << " :  " << E2 << endl;
    }

    //Echidna::EMessenger << "JJ projected in CD basis: " << endl << WS << endl << endl;


    // now construct the Js in packed format from the vectors
    tensor2 & JJ = DD;

    TensorProduct (JJ, WS, CDvecs); // N*BF2 = N*M x M*BF2

    // copy the matrices to the regular format, symmetrizing them in the process
    for (int n=0; n<Nmat; ++n) {
        J[n].zeroize();

        for (int i=0; i<NBF; ++i)
            for (int j=0; j<=i; ++j)
                J[n](i,j) = JJ(n, sidx(i,j));

        // notice that since the upper half is 0, the diagonal is counted twice
        J[n].Symmetrize();
    }

}


// computes the diagonal (ij | ij) Coulomb-like contributions to response
void Fock2e::ComputeDiagonal (tensor2 & D, const tensor2 & CMO, double precision) {

    const int NBF  = CDdata.NDIM; // total number of basis functions

    const int NMO = CMO.n;

    // determine the number of basis elements to be used:
    // & mock a smaller basis if necessary
    const int MM = CDdata.Nvectors(precision);



    D.setsize(NMO);
    D.zeroize();

    // D_ij = (ij|ij) = sum_m (s_m)^2 * (M_ij^m)^2
    for (int m=0; m<MM; ++m) {

        //m-th slice in the MO basis
        tensor2 JT;

        {
            // recast the m-th Cholesky vector as a symmetric matrix
            symtensor2 CV (NBF, CDdata.vecs[m]);
            //symtensor2 CV (NBF);
            //for (int ij=0; ij<CV.n2; ++ij) CV.c2[ij] = CDdata.vecs[m][ij];

            // make it into a full square matrix;
            // must scale the diagonal by a factor of 2;
            tensor2 J;
            J = CV;

            for (int i=0; i<NBF; ++i)
                J(i,i) *= 2;

            // change of basis to MO; C+ J C
            tensor2 P;

            TensorProductNNT (P,  J, CMO);
            TensorProduct    (JT, CMO, P);

            JT.Symmetrize();
            JT *= 0.5;
        }

        // now add the contribution to the J matrix
        {
            // squares the slice
            for (int ij=0; ij<NMO*NMO; ++ij) JT[0][ij] *= JT[0][ij];

            // D <- (s_m)^2 * JT2
            D.FMA (JT, CDdata.sigma[m]*CDdata.sigma[m]);
        }
    }

}

// computes the diagonal (ii | aa) Exchange-like contributions to response
void Fock2e::ComputeDiagonalExchange (tensor2 & D, const tensor2 & CMO, double precision) {

    const int NAO = CMO.m; //NTP->GetTotalLen(); // total number of basis functions
    const int NMO = CMO.n;

    // determine the number of basis elements to be used:
    // & mock a smaller basis if necessary
    const int MM = CDdata.Nvectors(precision);


    tensor2 T(MM, NMO);
    T.zeroize();


    // D_ij = (ij|ij) = sum_m (s_m)^2 * (M_ij^m)^2
    for (int m=0; m<MM; ++m) {

        //m-th slice in the MO basis
        tensor2 JT;

        {
            // recast the m-th Cholesky vector as a symmetric matrix
            symtensor2 CV (NAO, CDdata.vecs[m]);

            // make it into a full square matrix;
            // must scale the diagonal by a factor of 2;
            tensor2 J;
            J = CV;

            for (int i=0; i<NAO; ++i)
                J(i,i) *= 2;

            // change of basis to MO; C+ J C
            tensor2 P;

            TensorProductNNT (P,  J, CMO);
            TensorProduct    (JT, CMO, P);
        }

        // store the diagonal, which is the only thing we are after
        for (int i=0; i<NAO; ++i)
            T(m,i) = JT(i,i) * CDdata.sigma[m];
    }

    TensorProductNTN (D, T, T);
}


// transform the CD from AO to MO
void Fock2e::CDtransform (const tensor2 & CMO) {

    const int NAO = CMO.m; // total number of basis functions
    const int NMO = CMO.n;

    // determine the number of basis elements to be used:
    // & mock a smaller basis if necessary
    const int M = CDdata.vecs.n;


    // intermediate tensors for packing & basis set transformation
    tensor2 J(NAO,NAO); // in AO
    tensor2 P(NAO,NMO); // temp
    tensor2 T(NMO,NMO); // transformed to MO


    // D_ij = (ij|ij) = sum_m (s_m)^2 * (M_ij^m)^2
    for (int m=0; m<M; ++m) {

        // recast the m-th Cholesky vector as a symmetric matrix
        symtensor2 CV (NAO, CDdata.vecs[m]);

        // make it into a full square matrix;
        // must scale the diagonal by a factor of 2;
        J = CV;

        for (int i=0; i<NAO; ++i)
            J(i,i) *= 2;

        // change of basis to MO; C+ J C
        TensorProductNNT (P,  J, CMO);
        TensorProduct    (T, CMO, P);

        //
        T.Symmetrize();
        T *= 0.5;

        for (int i=0; i<NMO; ++i)
            T(i,i) *= 0.5;

        // erase previous data & copy
        // since NAO >= NMO, we must leave trailing
        // zeros between the packed slice and the next one
        CV.zeroize();
        symtensor2 CR (NMO, CDdata.vecs[m]);

        CR = T;
        CR *= CDdata.sigma[m]; // scale
    }


    // redo the SVD, now in MOs
    {
        CDdata.NDIM = NMO; // save the logical dimension in order to be able to recast vectors it to the correct physical size

        Echidna::EMessenger << "computing new SVD in MO basis" << endl;

        tensor2 U(M);

        SVD (CDdata.vecs, U, CDdata.sigma);

        Echidna::EMessenger << "singular values: " << endl;
        for (int m=0; m<M; ++m) Echidna::EMessenger << CDdata.sigma[m] << "  "; Echidna::EMessenger << endl;
    }

}


// returns (nn|ab), assuming MO basis
void Fock2e::GetIJNN (tensor2 & D, int64_t n, double precision) {

    const int NMO = CDdata.NDIM;

    const int M = CDdata.vecs.n; // number of SVD/CD/eigenvectors

    // offset of the (n,n) element in the packed vectors
    //const int n2 = (n*n+n)/2;

    // weights
    double * ww = new double[M];

    for (int m=0; m<M; ++m) {
        symtensor2 CV (NMO, CDdata.vecs[m]);
        ww[m] = CV(n,n) * CDdata.sigma[m] * CDdata.sigma[m]; // the diagonal may be negative
    }


    symtensor2 T(NMO);
    T.zeroize();

    for (int m=0; m<M; ++m) {
        if (fabs(ww[m])<precision) continue; // skip small values
        symtensor2 CV (NMO, CDdata.vecs[m]); // recast the vec of dim=T(NAO) into a (maybe) smaller packed matrix of dim NMO
        T.FMA (CV, ww[m]);
    }

    // 2 is for the halved diagonal in ww[]
    T *= 2;

    // unpack unto D
    D = T;

    // double the diagonal of the resulting
    for (int i=0; i<NMO; ++i)
        D(i,i) *= 2;

    delete[] ww;
}

// computes the diagonal (ij | ij) contributions, assuming MO basis
void Fock2e::GetIJIJ (tensor2 & D, double precision) {

    const int NMO = CDdata.NDIM;
    const int NMO2 = (NMO*NMO+NMO)/2;

    // determine the number of basis elements to be used:
    // & mock a smaller basis if necessary
    const int MM = CDdata.Nvectors(precision);

    symtensor2 T(NMO), P(NMO);
    T.zeroize();

    // accumulate everything on T
    for (int m=0; m<MM; ++m) {

        // recast the m-th Cholesky vector as a symmetric matrix
        symtensor2 CV (NMO, CDdata.vecs[m]);

        P = CV; // hard copy

        // squares the slice entries
        for (int ij=0; ij<NMO2; ++ij) P.c2[ij] *= P.c2[ij];

        T.FMA (P, CDdata.sigma[m]*CDdata.sigma[m]);
    }

    // scale the diagonal due to symmetry (twice)
    for (int i=0; i<NMO; ++i)
        T(i,i) *= 4;

    // copy/reshape the result
    D = T;
}

// computes the diagonal (ii | aa) Exchange-like contributions to response
void Fock2e::GetIIJJ (tensor2 & D, double precision) {

    const int NMO = CDdata.NDIM;

    // determine the number of basis elements to be used:
    // & mock a smaller basis if necessary
    const int MM = CDdata.Nvectors(precision);


    tensor2 T(MM, NMO);
    T.zeroize();

    for (int m=0; m<MM; ++m) {

        symtensor2 CV (NMO, CDdata.vecs[m]);

        // store the diagonal, which is the only thing we are after
        for (int i=0; i<NMO; ++i)
            T(m,i) = CV(i,i) * CDdata.sigma[m];
    }

    TensorProductNTN (D, T, T);

    // since the diagonals are halved (twice)
    D *= 4;
}

// returns coefficient expansion for a tensor-decomposed Coulomb
void Fock2e::CoulombCholesky (tensor2 & W, const tensor2 & U, const tensor2 & V, double precision) {
    const int NMO = CDdata.NDIM;

    //const int M = CDdata.vecs.n; // number of SVD/CD/eigenvectors
    const int M = CDdata.Nvectors(precision);
    const int R = U.n;

    W.setsize(R,M);

    tensor2 J(NMO,NMO); // in AO
    tensor2 JJ(R,R); // in AO

    for (int m=0; m<M; ++m) {

        // recast the m-th Cholesky vector as a symmetric matrix
        symtensor2 CV (NMO, CDdata.vecs[m]);

        // make it into a full square matrix;
        // must scale the diagonal by a factor of 2;
        J = CV;

        for (int i=0; i<NMO; ++i)
            J(i,i) *= 2;

        tensor2 T, JJ;

        TensorProduct    (T, U, J);
        TensorProductNNT (JJ,T, V);

        for (int r=0; r<R; ++r) W(r,m) = CDdata.sigma[m] * JJ(r,r);
    }

}

// returns coefficient expansion for a tensor-decomposed Coulomb
void Fock2e::ExchangeCholesky (tensor3 & W, const tensor2 & U, const tensor2 & V, double precision) {

    const int NMO = CDdata.NDIM;

    //const int M = CDdata.vecs.n; // number of SVD/CD/eigenvectors
    const int M = CDdata.Nvectors(precision);
    const int R = U.n;
    const int S = V.n;

    W.Set(M,R,S);

    tensor2 J(NMO,NMO); // in AO

    for (int m=0; m<M; ++m) {

        // recast the m-th Cholesky vector as a symmetric matrix
        symtensor2 CV (NMO, CDdata.vecs[m]);

        // make it into a full square matrix;
        // must scale the diagonal by a factor of 2;
        J = CV;

        for (int i=0; i<NMO; ++i)
            J(i,i) *= 2;

        tensor2 T;

        TensorProduct    (T, U, J);
        TensorProductNNT (W[m],T, V);

        W[m] *= CDdata.sigma[m];
    }
}

// returns coefficient expansion for a tensor-decomposed Coulomb
void Fock2e::ExchangeCholesky (tensor3 & W, const tensor2 & U, double precision) {

}



// retrieve a few CD / SVD vectors for a low-rank expansion
void Fock2e::GetCholesky (tensor2 * V, int nO, int nV, int & M, double precision) {

    const int NMO = CDdata.NDIM;

    // find the number of vectors necessary to reach the given precision,
    // but do not use more than the number provided;
    const int MM = min(CDdata.Nvectors(precision), M);


    for (int m=0; m<MM; ++m) {

        V[m].setsize(nO,nV);

        symtensor2 CV (NMO, CDdata.vecs[m]);

        // copy the necessary subblock
        for (int i=0; i<nO; ++i)
            for (int a=0; a<nV; ++a)
                V[m](i,a) = CV(nO+a,i);

        // scale
        V[m] *= CDdata.sigma[m];
    }


    // return how many vectors are actually used
    M = MM;
}


void Fock2e::Cholesky (double thresh) {


    const float logT = -log(thresh);

    //compute screening parameters
    SetZeroDensityPrescreener();


    //reset block counters
    //for (int p=0; p<OGMT; ++p) Blocks[p].Reset();

    Echidna::EMessenger << "Computing Cholesky Decomposition of the two-electron tensor";
    Echidna::EMessenger.Push();


    // ==================================
    // the algorithm should run in O(N^3)
    // ==================================
    // use CD products increasingly less overlapping (CS); update the diagonal accordingly

    // algorithm returns or stores the CD vectors in order to contract them with incoming densities

    const int NBF = NTP->GetTotalLen(); // total number of basis functions
    const int NEL = APL->AtomSameBatch.n;
    const int N = STP->natoms;

    // first compute the diagonal, i.e. (ij|ij)
    // ========================================

    tensor2 diag (NBF); {

        diag.zeroize();

        // for blocks involving two different atoms
        DiagonalParallelScheduler (diag, logT);

        // for same atom
        for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {

            const APbatch & APB12 = APL->AtomSameBatch[n12];
            const AtomProdPrototype * APP = APB12.APP;

            const uint64_t wa = APB12.APP->wa1;

            tensor2 ediag(wa);
            ediag.zeroize();


            ERIgeometry eriG;
            point A(0,0,0);
            eriG.MakeRotation1(A);

            // compute the necessary ab=cd shell pair integrals for the element
            #pragma omp parallel for schedule(dynamic)
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                    ERIBatch C1batch(this);
                    C1batch.Ntiles64 = 1;

                    C1batch.Set(AAAA, ABp, ABp, true);

                    int ogtn = omp_get_thread_num();
                    BatchEval[ogtn].SetBuffers(C1batch);
                    BatchEval[ogtn].ConstructDiagonal (C1batch, eriG, ediag.c2, wa);
            }

            // now copy the info to all the elements
            #pragma omp parallel for schedule(dynamic)
            for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                const AtomProd & AP12 = APB12.APlist[ap12];
                int at1 = AP12.at1;

                const int os = STP->a1pos[at1];

                for (int i=0; i<wa; ++i)
                    for (int j=0; j<wa; ++j)
                        diag (os+i,os+j) = ediag(i,j);
            }

        }




    }






    // decompose the (AA|AA) product integrals; find also the proper LM subspace for each product
    // ==========================================================================================

    tensor4 * W1 = new tensor4[NEL];
    tensor2 * w1 = new tensor2[NEL];

    {
        //loop over all elements
        for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {

            const APbatch & APB12 = APL->AtomSameBatch[n12];
            const AtomProdPrototype * APP = APB12.APP;

            const uint64_t wa = APB12.APP->wa1;

            ERIgeometry eriG;
            point A(0,0,0);
            eriG.MakeRotation1(A);


            W1[n12].Set(wa);

            // compute same atom integrals
            #pragma omp parallel for schedule(dynamic)
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<=ab; ++cd) {
                    const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                    if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                    if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                    ERIBatch C1batch(this);
                    C1batch.Ntiles64 = 1;

                    bool rev = InvertPrototypePair(ABp,CDp);
                    if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                    else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                    int ogtn = omp_get_thread_num();
                    BatchEval[ogtn].SetBuffers(C1batch);
                    BatchEval[ogtn].ConstructW(C1batch, eriG, W1[n12].T.c2, wa);
                }
            }

            // now diagonalize to find some acceptable basis without
            // repeating products & subspaces with proper L & M
            w1[n12].setsize(wa);
            W1[n12].T *= -1;
            DiagonalizeE (W1[n12].T, w1[n12].c2);
            w1[n12] *= -1;

            // now do something with the basis, like store it
            Echidna::EMessenger.precision(12);
            Echidna::EMessenger << "element " << n12 << endl << endl;
            Echidna::EMessenger << w1[n12] << endl << endl;
        }

    }

    // compute the full (AB|AB) block
    // ==============================

    r2tensor<tensor4> W2B(N,N); {

        // initialize each subtensor with the number of basis functions, squared
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);

                W2B(n,m).Set(BN,BM,BN,BM); // in general many will be empty
            }
        }

        // compute (AB|AB)
        ABABParallelScheduler (W2B, logT);

        // add (AA|AA)
        // now add AAAA
        for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
            const APbatch & APB12 = APL->AtomSameBatch[n12];
            const AtomProdPrototype * APP = APB12.APP;

            const uint64_t wa = APB12.APP->wa1;
            tensor4 WWW(wa);

            ERIgeometry eriG;
            point A(0,0,0);
            eriG.MakeRotation1(A);


            //calcula las integrales para SOLO un atomo de cada tipo
            #pragma omp parallel for schedule(dynamic)
            for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
                const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

                for (int cd=0; cd<=ab; ++cd) {
                    const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                    if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                    if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                    ERIBatch C1batch(this);
                    C1batch.Ntiles64 = 1;

                    bool rev = InvertPrototypePair(ABp,CDp);
                    if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                    else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                    int ogtn = omp_get_thread_num();
                    BatchEval[ogtn].SetBuffers(C1batch);
                    BatchEval[ogtn].ConstructW(C1batch, eriG, WWW.T.c2, wa);
                }
            }

            // copy data
            #pragma omp parallel for schedule(dynamic)
            for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
                const AtomProd & AP12 = APB12.APlist[ap12];
                const int at1 = AP12.at1;

                // this does not modify shape or size
                W2B(at1,at1).T = WWW.T;
            }

        }


        int c3=0, c4=0, c6=0, c8=0, c10=0;

        for (int n=0; n<N; ++n) {
            for (int m=0; m<=n; ++m) {
                Echidna::EMessenger << "atoms : " << n << " " << m << endl;

                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);

                tensor2 w2(BN,BM);
                w2.zeroize();

                W2B(n,m).T *= -1;
                DiagonalizeE(W2B(n,m).T, w2.c2);
                W2B(n,m).T *= -1;
                w2 *= -1;

                for (int i=0; i<BN*BM; ++i) {
                    if (w2.c2[i] > 1.e-3) ++c3;
                    if (w2.c2[i] > 1.e-4) ++c4;
                    if (w2.c2[i] > 1.e-6) ++c6;
                    if (w2.c2[i] > 1.e-8) ++c8;
                    if (w2.c2[i] > 1.e-10) ++c10;
                }

                Echidna::EMessenger << w2 << endl << endl;
            }

        }

        Echidna::EMessenger << "eigenvalues larger that 1e-3 : " << c3 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-4 : " << c4 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-6 : " << c6 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-8 : " << c8 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-10 : " << c10 << endl;


    }

    // compute the full (AA|CC) block
    // ==============================

    r2tensor<tensor4> W2(N,N); {

        // initialize each subtensor with the number of basis functions, squared
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);

                W2(n,m).Set(BN,BM);
            }
        }
    }

    // compute AACC
    TwoCenterParallelScheduler   (W2, logT);

    // now add AAAA
    for (int n12=0; n12<APL->AtomSameBatch.n; ++n12) {
        const APbatch & APB12 = APL->AtomSameBatch[n12];
        const AtomProdPrototype * APP = APB12.APP;

        const uint64_t wa = APB12.APP->wa1;
        tensor4 WWW(wa);

        ERIgeometry eriG;
        point A(0,0,0);
        eriG.MakeRotation1(A);


        //calcula las integrales para SOLO un atomo de cada tipo
        #pragma omp parallel for schedule(dynamic)
        for (int ab=0; ab<APB12.APP->nGPt; ++ab) {
            const ShellPairPrototype & ABp = APB12.APP->Gs[ab];

            for (int cd=0; cd<=ab; ++cd) {
                const ShellPairPrototype & CDp = APB12.APP->Gs[cd];

                if ((ABp.l1+ABp.l2+CDp.l1+CDp.l2)%2) continue;
                if ((ABp.l2==0) && (CDp.l2==0) && (ABp.l1!=CDp.l1)) continue;

                ERIBatch C1batch(this);
                C1batch.Ntiles64 = 1;

                bool rev = InvertPrototypePair(ABp,CDp);
                if (rev) C1batch.Set(AAAA, ABp, CDp, (ab==cd));
                else     C1batch.Set(AAAA, CDp, ABp, (ab==cd));

                int ogtn = omp_get_thread_num();
                BatchEval[ogtn].SetBuffers(C1batch);
                BatchEval[ogtn].ConstructW(C1batch, eriG, WWW.T.c2, wa);
            }
        }

        // copy data
        #pragma omp parallel for schedule(dynamic)
        for (int ap12=0; ap12<APB12.APlist.n; ++ap12) {
            const AtomProd & AP12 = APB12.APlist[ap12];
            const int at1 = AP12.at1;

            // this does not modify shape or size
            W2(at1,at1).T = WWW.T;
        }

    }



    // now incrementally compute (AA|CD)
    // =================================

    // dimension of the AACC block (including repeated densities)

    // N * B * B * NBF * NBF
    r1tensor<tensor4> W3(N); {

        for (int n=0; n<N; ++n) {
            const int BN = NTP->GetLen(n); // total number of basis funcs in atom
            W3(n).Set (BN, NBF);
        }

        // three-center integrals
        ThreeCenterParallelScheduler (W3, logT);


        // copy two- and one- center from previous
        for (int n=0; n<N; ++n) {
            const int BN = NTP->GetLen(n);

            for (int p=0; p<BN; ++p) {
                for (int q=0; q<BN; ++q) {

                    for (int m=0; m<N; ++m) {
                        const int BM = NTP->GetLen(m);
                        const int OS = STP->GetOffset1(m);

                        for (int r=0; r<BM; ++r) {
                            for (int s=0; s<BM; ++s) {
                                W3 (n) (p,q, OS+r, OS+s) = W2(n, m) (p,q, r,s);
                            }
                        }
                    }

                }
            }

        }

    }




    Echidna::EMessenger << "ij|ij diagonal tensor elements: " << endl;
    Echidna::EMessenger << diag << endl << endl;

    {
        double s=0;
        for (int i=0; i<NBF; ++i)
            for (int j=0; j<NBF; ++j)
                s += diag(i,j);

        Echidna::EMessenger << "tensor diagonal sum: " << s << endl;
    }

    // make the real W2 matrix (without repetitions)
    int D;
    tensor2 Waacc; {

        D = 0;

        for (int n=0; n<N; ++n) {
            const int B = NTP->GetLen(n); // total number of basis funcs in atom
            D += (B*B+B)/2;
        }

        Waacc.setsize(D); Waacc.zeroize();


        for (int osn=0,n=0; n<N; ++n) {
            const int BN = NTP->GetLen(n); // total number of basis funcs in atom

            for (int osm=0,m=0; m<N; ++m) {
                const int BM = NTP->GetLen(m); // total number of basis funcs in atom

                for (int p=0; p<BN; ++p)
                    for (int q=0; q<=p; ++q)
                        for (int r=0; r<BM; ++r)
                            for (int s=0; s<=r; ++s)
                                Waacc (osn + (p*p+p)/2 + q, osm + (r*r+r)/2 + s) = W2 (n,m) (p,q, r,s);

                osm += (BM*BM+BM)/2;
            }
            osn += (BN*BN+BN)/2;
        }
    }



    //Echidna::EMessenger << "pseudoinverse of AACC block" << endl;
    //Echidna::EMessenger << Winv << endl << endl;

    // copy the data to a more useful format
    symtensor2 * Waacd = new symtensor2[D]; {

        for (int k=0; k<D; ++k) Waacd[k].setsize(NBF);

        int d=0;

        for (int n=0; n<N; ++n) {
            const int B = NTP->GetLen(n); // total number of basis funcs in atom

            for (int p=0; p<B; ++p)
                for (int q=0; q<=p; ++q) {

                    const int k = (d + (p*p+p)/2 + q);

                    for (int c=0; c<NBF; ++c)
                        for (int d=0; d<=c; ++d)
                            Waacd[k](c,d) = W3(n) (p,q, c,d);
                }

            d += (B*B+B)/2;
        }
    }

    // make the cholesky vecs proper
    symtensor2 * W3CD; {

        int K=0;

        {
            tensor2 wac(1,D);
            wac.zeroize();

            Waacc *= -1;
            DiagonalizeV (Waacc,wac.c2);
            wac *= -1;

            // find reasonable decomposition threshold
            for (K=0; K<D; ++K) {
                if (wac(0,K)<1e-12) break;
                wac(0,K) = 1./sqrt(wac(0,K));
            }
            for (int k=K; k<D; ++k) wac(0,k) = 0;

            // scale the eigenvectors properly

            for (int k=0; k<D; ++k) {
                for (int d=0; d<D; ++d) {
                    Waacc (k,d) *= wac(0,k);
                }
            }


            // rotated, orthogonal vectors
            W3CD = new symtensor2[K];

            for (int k=0; k<K; ++k) {
                W3CD[k].setsize(NBF);
                W3CD[k].zeroize();

                for (int d=0; d<D; ++d)
                    for (int ij=0; ij<(NBF*NBF+NBF)/2; ++ij)
                        W3CD[k].c2[ij] += Waacc(k,d) * Waacd[d].c2[ij];

                // scale
                W3CD[k] *= wac(0,k);
            }
        }

        // now update the block diagonal
        // =============================

        for (int k=0; k<K; ++k) {
            for (int i=0; i<NBF; ++i)
                for (int j=0; j<NBF; ++j)
                    diag(i,j) -= W3CD[k](i,j)*W3CD[k](i,j);
        }


        // update full ABAB blocks
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {

                const int OSN = STP->GetOffset1(n);
                const int OSM = STP->GetOffset1(m);
                const int BN = NTP->GetLen(n);
                const int BM = NTP->GetLen(m);

                // loop over all CD vectors
                for (int p=0; p<K; ++p)

                    for (int i=0; i<BN; ++i)
                        for (int j=0; j<BM; ++j)
                            for (int k=0; k<BN; ++k)
                                for (int l=0; l<BM; ++l)
                                    W2B(n,m)(i,j, k,l) -= W3CD[p](OSN+i,OSM+j)*W3CD[p](OSN+k,OSM+l);
            }
        }

        // print new evs

        int c3=0, c4=0, c6=0, c8=0, c10=0;

        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                //Echidna::EMessenger << "atoms : " << n << " " << m << endl;

                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);

                tensor2 w2(BN,BM);
                w2.zeroize();

                W2B(n,m).T *= -1;
                DiagonalizeE(W2B(n,m).T, w2.c2);
                W2B(n,m).T *= -1;
                w2 *= -1;

                for (int i=0; i<BN*BM; ++i) {
                    if (w2.c2[i] > 1.e-3) ++c3;
                    if (w2.c2[i] > 1.e-4) ++c4;
                    if (w2.c2[i] > 1.e-6) ++c6;
                    if (w2.c2[i] > 1.e-8) ++c8;
                    if (w2.c2[i] > 1.e-10) ++c10;
                }

                //Echidna::EMessenger << w2 << endl << endl;
            }
        }

        Echidna::EMessenger << "eigenvalues larger that 1e-3 : " << c3 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-4 : " << c4 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-6 : " << c6 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-8 : " << c8 << endl;
        Echidna::EMessenger << "eigenvalues larger that 1e-10 : " << c10 << endl;
    }


    Echidna::EMessenger << "ij|ij fitted diagonal tensor elements: " << endl;
    Echidna::EMessenger << diag << endl << endl;

    {
        double s=0;
        for (int i=0; i<NBF; ++i)
            for (int j=0; j<NBF; ++j)
                s += diag(i,j);

        Echidna::EMessenger << "tensor diagonal sum: " << s << endl;
    }


    /*

    // now check the worst ABAB offenders and add them to a list
    // skip same atom, as they should already be perfectly fitted
    tensor2 res(N); res.zeroize();

        for (int n=0; n<N; ++n) {
            for (int m=0; m<n; ++m) {
                //Echidna::EMessenger << "atoms : " << n << " " << m << endl;

                const int BN = STP->GetLen1(n);
                const int BM = STP->GetLen1(m);

                double s=0;

                for (int i=0; i<BN; ++i)
                    for (int j=0; j<BM; ++j)
                        s+=W2B(n,m)(i,j,i,j);

                res(n,m) = s;
            }
        }

    */


    return;


    // NEW APPROACH CIRCUMVENTING MEMORY LIMITATIONS
    // =============================================

    CholeskyPrescreener (logT);

    // compute the dimension
    uint32_t cd2; {
        const ShellPair * sp = APL->SPpool;
        const int NSP = APL->nGTOps;

        cd2=0;

        for (int n=0; n<NSP; ++n) {
            if (sp[n].logCS<logT) {
                const int JA = sp[n].GP->Ja;
                const int JB = sp[n].GP->Jb;
                const int MA = 2*sp[n].GP->l1+1;
                const int MB = 2*sp[n].GP->l2+1;

                cd2 += JA*JB*MA*MB; // this may be different for A=B

                Echidna::EMessenger << n << " "  << sp[n].logCS << " " << logT << " " << cd2 << endl;
            }
        }
    }

    Echidna::EMessenger << "dimension M = " << cd2 << endl;

    {
        tensor2 W2(cd2);
        W2.zeroize();

        // call the scheduler
        CholeskyParallelScheduler (W2, 2*logT); // twice the CS limit to guarantee all pairs that are not explicitly discarded pass

        Echidna::EMessenger << "Cholesky matrix " << cd2 << endl;
        Echidna::EMessenger << W2 << endl;
    }


    // LAZY-EVALUATED BLOCK APPROACH
    // =============================

    while (true) {

        // step 1: find the diagonal (AB*|AB*) block with largest trace

        // step 2: compute (AB|CD) interaction with remaining blocks

        // step 3: update (AB*|CD) with previous Cholesky block-vectors

        // step 4: diagonalize (AB*|AB*); select eigenvalues > threshold; rotate (AB*|CD) vectors

        // step 5: update (CD*|CD*) diagonal

    }



    Echidna::EMessenger.Pop();
}


void Fock2e::CholeskyParallelScheduler (tensor2 & W2, float logT) {

    enum JobDescription{JOB_NONE, JOB_MAKE_BATCH, JOB_COMPUTE_BATCH};

    //if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;

    Echidna::EMessenger << "Computing 2 electron tensor";

    Echidna::EMessenger.Push();


    //copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it) {
            BatchInfo * BI =  new BatchInfo;
            *BI = *it;

            BlockList.push(BI);
        }
    }

    TaskQueue<ERIBatch> OCLDList;

    //begin
    volatile int workingthreads = OGMT;

    #pragma omp parallel shared(workingthreads)
    {
        uint32_t tn = omp_get_thread_num();
        bool lastfailed = false;

        ERIBatch  * OCLD;
        BatchInfo * Block;

        while (workingthreads>0 || !lastfailed ) {

            JobDescription JobType = JOB_NONE;


            //first try to retrieve a batch
            OCLD = OCLDList.pop();

            if (OCLD!=NULL) JobType = JOB_COMPUTE_BATCH;
            //if it doesn't succeed, try to retrieve a block from the queue
            else {
                Block = BlockList.pop();
                if (Block!=NULL) JobType = JOB_MAKE_BATCH;
            }

            //never actually executed
            if (lastfailed && JobType!=JOB_NONE) {
                lastfailed = false;
                #pragma omp atomic
                ++workingthreads;
            }

            //OCLD PATHJOB_MAKE_BATCH
            if      (JobType==JOB_COMPUTE_BATCH) {
                BatchEval[tn].EvaluateCholesky (*OCLD, W2, STP);

                OCLD->clear(EbatchBuffer);
                delete OCLD; //recycle ?
            }
            //
            else if (JobType==JOB_MAKE_BATCH) {

                bool finish;
                Blocks[tn] = *Block;

                finish = Blocks[tn].MakeCoulomb (logT, logT, lnDs, lnDs, OCLDList); // only Coulomb required

                if (finish) delete Block;
                else        {
                    Block->State    = Blocks[tn].State;
                    BlockList.push_top(Block);
                }
            }

            else {
                if (!lastfailed) {
                    lastfailed = true;
                    #pragma omp atomic
                    --workingthreads;
                }
            }
        }
    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (OCLDList.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    Echidna::EMessenger.Pop();
}




void Fock2e::DiagonalParallelScheduler (tensor2 & W, float logT) {

    enum JobDescription{JOB_NONE, JOB_MAKE_BATCH, JOB_COMPUTE_BATCH};

    //if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;

    Echidna::EMessenger << "Computing 2 electron tensor diagonal";

    Echidna::EMessenger.Push();


    //copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it)
            // only compute repeating shell products
            if (it->ABp == it->CDp) {
                BatchInfo * BI =  new BatchInfo;
                *BI = *it;
                BlockList.push(BI);
            }
    }

    TaskQueue<ERIBatch> OCLDList;

    //begin
    volatile int workingthreads = OGMT;

    #pragma omp parallel shared(workingthreads)
    {
        uint32_t tn = omp_get_thread_num();
        bool lastfailed = false;

        ERIBatch  * OCLD;
        BatchInfo * Block;

        while (workingthreads>0 || !lastfailed ) {

            JobDescription JobType = JOB_NONE;


            //first try to retrieve a batch
            OCLD = OCLDList.pop();

            if (OCLD!=NULL) JobType = JOB_COMPUTE_BATCH;
            //if it doesn't succeed, try to retrieve a block from the queue
            else {
                Block = BlockList.pop();
                if (Block!=NULL) JobType = JOB_MAKE_BATCH;
            }

            //never actually executed
            if (lastfailed && JobType!=JOB_NONE) {
                lastfailed = false;
                #pragma omp atomic
                ++workingthreads;
            }

            //OCLD PATHJOB_MAKE_BATCH
            if      (JobType==JOB_COMPUTE_BATCH) {
                BatchEval[tn].EvaluateDiagonal (*OCLD, W, STP);

                OCLD->clear(EbatchBuffer);
                delete OCLD; //recycle ?
            }
            //
            else if (JobType==JOB_MAKE_BATCH) {

                bool finish;
                Blocks[tn] = *Block;

                finish = Blocks[tn].MakeCoulomb (logT, logT, lnDs, lnDs, OCLDList); // only Coulomb required

                if (finish) delete Block;
                else        {
                    Block->State    = Blocks[tn].State;
                    BlockList.push_top(Block);
                }
            }

            else {
                if (!lastfailed) {
                    lastfailed = true;
                    #pragma omp atomic
                    --workingthreads;
                }
            }
        }
    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (OCLDList.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    Echidna::EMessenger.Pop();
}


void Fock2e::ABABParallelScheduler (r2tensor<tensor4> & W2, float logT) {

    enum JobDescription{JOB_NONE, JOB_MAKE_BATCH, JOB_COMPUTE_BATCH};

    //if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;

    Echidna::EMessenger << "Computing 2 electron tensor block-diagonal";

    Echidna::EMessenger.Push();


    //copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it)
            // only compute repeating shell products
            if (it->geometry == ABAB) {
                BatchInfo * BI =  new BatchInfo;
                *BI = *it;
                BlockList.push(BI);
            }
    }

    TaskQueue<ERIBatch> OCLDList;

    //begin
    volatile int workingthreads = OGMT;

    #pragma omp parallel shared(workingthreads)
    {
        uint32_t tn = omp_get_thread_num();
        bool lastfailed = false;

        ERIBatch  * OCLD;
        BatchInfo * Block;

        while (workingthreads>0 || !lastfailed ) {

            JobDescription JobType = JOB_NONE;


            //first try to retrieve a batch
            OCLD = OCLDList.pop();

            if (OCLD!=NULL) JobType = JOB_COMPUTE_BATCH;
            //if it doesn't succeed, try to retrieve a block from the queue
            else {
                Block = BlockList.pop();
                if (Block!=NULL) JobType = JOB_MAKE_BATCH;
            }

            //never actually executed
            if (lastfailed && JobType!=JOB_NONE) {
                lastfailed = false;
                #pragma omp atomic
                ++workingthreads;
            }

            //OCLD PATHJOB_MAKE_BATCH
            if      (JobType==JOB_COMPUTE_BATCH) {
                BatchEval[tn].EvaluateDiagonal (*OCLD, W2, STP);

                OCLD->clear(EbatchBuffer);
                delete OCLD; //recycle ?
            }
            //
            else if (JobType==JOB_MAKE_BATCH) {

                bool finish;
                Blocks[tn] = *Block;

                finish = Blocks[tn].MakeCoulomb (logT, logT, lnDs, lnDs, OCLDList); // only Coulomb required

                if (finish) delete Block;
                else        {
                    Block->State    = Blocks[tn].State;
                    BlockList.push_top(Block);
                }
            }

            else {
                if (!lastfailed) {
                    lastfailed = true;
                    #pragma omp atomic
                    --workingthreads;
                }
            }
        }
    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (OCLDList.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    Echidna::EMessenger.Pop();
}


void Fock2e::TwoCenterParallelScheduler   (r2tensor<tensor4> & W2, float logT) {

    enum JobDescription{JOB_NONE, JOB_MAKE_BATCH, JOB_COMPUTE_BATCH};

    //if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;

    Echidna::EMessenger << "Computing 2-center integrals";

    Echidna::EMessenger.Push();


    //copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it)
            // only compute repeating shell products
            if (it->geometry == AACC) {
                BatchInfo * BI =  new BatchInfo;
                *BI = *it;
                BlockList.push(BI);
            }
    }

    TaskQueue<ERIBatch> OCLDList;

    //begin
    volatile int workingthreads = OGMT;

    #pragma omp parallel shared(workingthreads)
    {
        uint32_t tn = omp_get_thread_num();
        bool lastfailed = false;

        ERIBatch  * OCLD;
        BatchInfo * Block;

        while (workingthreads>0 || !lastfailed ) {

            JobDescription JobType = JOB_NONE;


            //first try to retrieve a batch
            OCLD = OCLDList.pop();

            if (OCLD!=NULL) JobType = JOB_COMPUTE_BATCH;
            //if it doesn't succeed, try to retrieve a block from the queue
            else {
                Block = BlockList.pop();
                if (Block!=NULL) JobType = JOB_MAKE_BATCH;
            }

            //never actually executed
            if (lastfailed && JobType!=JOB_NONE) {
                lastfailed = false;
                #pragma omp atomic
                ++workingthreads;
            }

            //OCLD PATHJOB_MAKE_BATCH
            if      (JobType==JOB_COMPUTE_BATCH) {
                // this is meaningless; write the function
                BatchEval[tn].Evaluate2Center (*OCLD, W2, STP);

                OCLD->clear(EbatchBuffer);
                delete OCLD; //recycle ?
            }
            //
            else if (JobType==JOB_MAKE_BATCH) {

                bool finish;
                Blocks[tn] = *Block;

                finish = Blocks[tn].MakeCoulomb (logT, logT, lnDs, lnDs, OCLDList); // only Coulomb required

                if (finish) delete Block;
                else        {
                    Block->State    = Blocks[tn].State;
                    BlockList.push_top(Block);
                }
            }

            else {
                if (!lastfailed) {
                    lastfailed = true;
                    #pragma omp atomic
                    --workingthreads;
                }
            }
        }
    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (OCLDList.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    Echidna::EMessenger.Pop();
}


void Fock2e::ThreeCenterParallelScheduler (r1tensor<tensor4> & W3, float logT) {

    enum JobDescription{JOB_NONE, JOB_MAKE_BATCH, JOB_COMPUTE_BATCH};

    //if (Quimera::GetCASE()) Echidna::EDebugger << "Warning: using CASE operator without splitting of Coulomb and Exchange contributions!" << endl;

    Echidna::EMessenger << "Computing 3-center integrals";

    Echidna::EMessenger.Push();


    //copy the JobQueue (which is a set) to a FIFO queue
    TaskQueue<BatchInfo> BlockList;
    {
        set<BatchInfo>::const_iterator it;

        for (it=TwoElectronJobs.JobQueue.begin(); it!=TwoElectronJobs.JobQueue.end(); ++it)
            // only compute repeating shell products
            if (it->geometry == AACD) {
                BatchInfo * BI =  new BatchInfo;
                *BI = *it;
                BlockList.push(BI);
            }
    }

    TaskQueue<ERIBatch> OCLDList;

    //begin
    volatile int workingthreads = OGMT;

    #pragma omp parallel shared(workingthreads)
    {
        uint32_t tn = omp_get_thread_num();
        bool lastfailed = false;

        ERIBatch  * OCLD;
        BatchInfo * Block;

        while (workingthreads>0 || !lastfailed ) {

            JobDescription JobType = JOB_NONE;


            //first try to retrieve a batch
            OCLD = OCLDList.pop();

            if (OCLD!=NULL) JobType = JOB_COMPUTE_BATCH;
            //if it doesn't succeed, try to retrieve a block from the queue
            else {
                Block = BlockList.pop();
                if (Block!=NULL) JobType = JOB_MAKE_BATCH;
            }

            //never actually executed
            if (lastfailed && JobType!=JOB_NONE) {
                lastfailed = false;
                #pragma omp atomic
                ++workingthreads;
            }

            //OCLD PATHJOB_MAKE_BATCH
            if      (JobType==JOB_COMPUTE_BATCH) {
                BatchEval[tn].Evaluate3Center (*OCLD, W3, STP);

                OCLD->clear(EbatchBuffer);
                delete OCLD; //recycle ?
            }
            //
            else if (JobType==JOB_MAKE_BATCH) {

                bool finish;
                Blocks[tn] = *Block;

                finish = Blocks[tn].MakeCoulomb (logT, logT, lnDs, lnDs, OCLDList); // only Coulomb required

                if (finish) delete Block;
                else        {
                    Block->State    = Blocks[tn].State;
                    BlockList.push_top(Block);
                }
            }

            else {
                if (!lastfailed) {
                    lastfailed = true;
                    #pragma omp atomic
                    --workingthreads;
                }
            }
        }
    }

    if (BlockList.length() != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;
    if (OCLDList.length()  != 0) Echidna::EDebugger << "Warning! not all ERI blocks were successfully evaluated!" << endl;

    EbatchBuffer->Check();

    Echidna::EMessenger.Pop();
}

