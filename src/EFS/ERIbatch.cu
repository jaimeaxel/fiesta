
#include "low/chrono.hpp"

#include "ERIbatch.hpp"
#include "libechidna/libechidna.hpp"
#include <iostream>
using namespace std;

// choose one
//#define __GATHER_CPU__
#define __GATHER_GPU__

//doubles per bank
const int DPB = 32;

// this routine needs rethinking after some refactoring (don't need much host memory to hold only atoms' lists)

void BlockBufferGPU::InitMem (size_t MEMGPU, BatchEvaluator * BE, int Nprox) {

    const int PAGE = 4096; //round mem to page boundaries / bank
    const int BANK =  256; //round mem to bank in GPU

    int Nstreams;

    cudaError_t cudaResult; // to check for errors


    // do not initialize twice
    if (TotalBlockMem==NULL) {

        //find number of available GPUs
        cudaGetDeviceCount(&NumGPUs);
        std::cout << "Number of available GPUs: " << NumGPUs << endl;

        NumGPUs = min(Echidna::nGPUs, NumGPUs);
        std::cout << "Number of GPUs to be used: " << NumGPUs << endl;

        //find the number of streams runnable on the GPU
        Nstreams = (Nprox+NumGPUs-1)/NumGPUs;
        std::cout << "Number of concurrent streams per GPU: " << Nstreams << endl;

        //assign threads/streams to GPUs in a round-robin fashion
        for (int n=0; n<Nprox; ++n) {
            BE[n].GPUdevice = n / Nstreams;
            cout << "GPU "<<BE[n].GPUdevice<<" will be used by process " << n << endl;
        }

        size_t MEM = NumGPUs * MEMGPU;



        // INIT PINNED MEM
        // ===============

        TotalMaxBlockMem = MEM;

        while (
            cudaMallocHost((void **) &TotalBlockMem,  TotalMaxBlockMem) != cudaSuccess
        ) TotalMaxBlockMem/=2;

        cudaResult = cudaGetLastError();

        if (cudaResult != cudaSuccess) {
            std::cout << std::endl;
            std::cout << "Couldn't allocate " << MEM << " bytes of pinned host memory for ERI buffering; allocating " << TotalMaxBlockMem << " instead" << std::endl;
            std::cout << (void*)TotalBlockMem << endl;
            std::cout << cudaGetErrorString(cudaResult) << endl;
        }

        // split mem between all processes
        LocalMaxBlockMem    = TotalMaxBlockMem    / Nprox;
        LocalMaxBlockMem    = (LocalMaxBlockMem   /PAGE) * PAGE;
    }

    // this may be reset
    for (int n=0; n<Nprox; ++n) {
        BE[n].b4blockGPU.MaxBlockMem    = LocalMaxBlockMem; //set the constant mems
        BE[n].b4blockGPU.BlockMem       = TotalBlockMem    + n*LocalMaxBlockMem; //set the pointers
    }


    // INIT DEVICE(S) MEM
    // ==================

    if (TotalBlockMem==NULL) {

        for (int n=0; n<NumGPUs; ++n) {

            // select the n-th device
            cudaSetDevice(n);

            //set globally 48kb of L1 cache and 16kb of shared memory
            cudaDeviceSetCacheConfig (cudaFuncCachePreferL1);

            TotalMaxBlockMemGPU[n] = MEMGPU;

            while (
                cudaMalloc((void **) &TotalBlockMemGPU[n],  TotalMaxBlockMemGPU[n]) != cudaSuccess
            ) TotalMaxBlockMemGPU[n]/=2;

            cudaResult = cudaGetLastError();

            if (cudaResult != cudaSuccess) {
                std::cout << std::endl;
                std::cout << "Couldn't allocate " << MEMGPU << " bytes of memory on device "<<n<< "for ERI buffering; allocating " << TotalMaxBlockMemGPU[n] << " instead" << std::endl;
                std::cout << (void*)TotalBlockMemGPU[n] << endl;
                std::cout << cudaGetErrorString(cudaResult) << endl;
            }

            // split mem between all streams meant to run on the given GPU
            LocalMaxBlockMemGPU[n] = TotalMaxBlockMemGPU[n] / Nstreams;
            LocalMaxBlockMemGPU[n] = (LocalMaxBlockMemGPU[n]/BANK) * BANK;
        }

    }

    for (int n=0; n<NumGPUs; ++n) {
        for (int s=0; s<Nstreams; ++s) {
            int nth = n*Nstreams + s;
            if (nth >= Nprox) continue; // don't initialize threads that don't exist !!!

            cout << "GPU "<<n<<" is used by process " << nth << endl;

            BE[nth].b4blockGPU.MaxBlockMemGPU = LocalMaxBlockMemGPU[n]; //set the constant mems
            BE[nth].b4blockGPU.BlockMemGPU    = TotalBlockMemGPU[n] + s*LocalMaxBlockMemGPU[n]; //set the pointers
        }
    }

}

void BlockBufferGPU::FreeMem () {

    // do not delete twice
    if (TotalBlockMem!=NULL) {
        cudaFreeHost(TotalBlockMem);

        for (int n=0; n<NumGPUs; ++n) {
            // select the n-th device
            cudaSetDevice(n);
            cudaFree(TotalBlockMemGPU[n]);
            TotalBlockMemGPU[n] = NULL;
            TotalMaxBlockMemGPU[n] = 0;
            LocalMaxBlockMemGPU[n] = 0;
        }

        cudaDeviceReset();
    }
    TotalBlockMem = NULL;
    TotalMaxBlockMem = 0;
    LocalMaxBlockMem = 0;
}


BatchEvaluator::BatchEvaluator() {

    GPUstream = new cudaStream_t [NSTREAMS];
    event_set = new cudaEvent_t  [NEVENTS];

    Chronos   = new StreamChrono [8];

    for (int n=0; n<NSTREAMS; ++n) cudaStreamCreate (&GPUstream[n]);
    for (int n=0; n<NEVENTS;  ++n) cudaEventCreate  (&event_set[n]);

    //Jresp = NULL;
    //Xresp = NULL;
    //Aresp = NULL;
    Jgpu = NULL;
    Xgpu = NULL;
    Agpu = NULL;
}

BatchEvaluator::~BatchEvaluator() {

    for (int n=0; n<NSTREAMS; ++n) cudaStreamDestroy  (GPUstream[n]); // destroy all streams
    for (int n=0; n<NEVENTS ; ++n) cudaEventDestroy   (event_set[n]); // and events

    delete[] GPUstream;
    delete[] event_set;
}


// there's an incompatibility between some gcc builtins and CUDA;
// defining this skips the cache64 and cache32 definitions, which are unnecessary for this part of the code
#include "low/cacheN.hpp"
#include "linear/newsparse.hpp"
#include "basis/SPprototype.hpp"
#include "basis/shellpair.hpp"
#include "basis/atomprod.hpp"
#include "libquimera/libquimera.hpp"
#include "libquimera/ERIgeom.hpp"
#include "EFS/rotations.hpp"


void BatchEvaluator::CopyWtoDevice      (const ERIBatch & TheBatch, cudaStream_t * stream) {

    cudaError_t cudaResult;
    cudaResult = cudaGetLastError();
    if (cudaResult != cudaSuccess) cout << endl << "some error ocurred before W:  " << cudaGetErrorString(cudaResult) << endl;


    if (TheBatch.geometry!=AACC && TheBatch.geometry!=AACD) {

        int nK2 = TheBatch.ABp->Ka*TheBatch.ABp->Kb;

        double * Ws = (double*)b4blockGPU.AB8;

        for (int n=0; n<TheBatch.NtilesN; ++n) {
            double * Wt = Ws + n*nK2 * DPB;

            const LibQuimera::ERITile<DPB> & ET = TheBatch.TileListN[n];

            for (int k=0; k<DPB; ++k) {
                int addr = ET.ap12[k]*nK2;

                const double * ds = &ET.wSP12[addr];

                for (int p=0; p<nK2; ++p) {
                    Wt[p*DPB+k] = ds[p];
                }
            }

        }

        //cudaMemcpy (b4blockGPU.dAB8, Ws, TheBatch.NtilesN * nK2  * DPB * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpyAsync (b4blockGPU.dAB8, Ws, TheBatch.NtilesN * nK2  * DPB * sizeof(double), cudaMemcpyHostToDevice, *stream);

        cudaError_t cudaResult;
        cudaResult = cudaGetLastError();
        if (cudaResult != cudaSuccess)
            cout << endl << "some error ocurred in W (1):  " << cudaGetErrorString(cudaResult) << " " << (void*)b4blockGPU.AB8 << "->" << (void*)b4blockGPU.dAB8 << endl;
    }

    if (TheBatch.geometry!=AACC) {

        int nK2 = TheBatch.CDp->Ka*TheBatch.CDp->Kb;

        double * Ws = (double*)b4blockGPU.CD8;

        for (int n=0; n<TheBatch.NtilesN; ++n) {
            double * Wt = Ws + n*nK2 * DPB;

            const LibQuimera::ERITile<DPB> & ET = TheBatch.TileListN[n];

            for (int k=0; k<DPB; ++k) {
                int addr = ET.ap34[k]*nK2;

                const double * ds = &ET.wSP34[addr];

                for (int p=0; p<nK2; ++p) {
                    Wt[p*DPB+k] = ds[p];
                }
            }

        }

        //cudaMemcpy (b4blockGPU.dCD8, Ws, TheBatch.NtilesN * nK2 * DPB * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpyAsync (b4blockGPU.dCD8, Ws, TheBatch.NtilesN * nK2 * DPB * sizeof(double), cudaMemcpyHostToDevice, *stream);

        cudaError_t cudaResult;
        cudaResult = cudaGetLastError();
        if (cudaResult != cudaSuccess)
            cout << endl << "some error ocurred in W (2):  " << cudaGetErrorString(cudaResult) << " " << (void*)b4blockGPU.CD8 << "->" << (void*)b4blockGPU.dCD8 << endl;
    }

}


void BatchEvaluator::CopyDJtoDevice     (const ERIBatch & TheBatch, cudaStream_t * stream, const Sparse & Ds) {


    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;


    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;


    cachelineN<DPB> *Dab, *Dcd;
    Dab = (cachelineN<DPB>*)b4blockGPU.dab;
    Dcd = (cachelineN<DPB>*)b4blockGPU.dcd;

    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {

        for (uint32_t k=0; k<DPB; ++k) {
            uint32_t ap12 = TheBatch.TileListN[i].ap12[k];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[k];

            uint32_t ta = TheBatch.SP12[ap12].ata;
            uint32_t tb = TheBatch.SP12[ap12].atb;
            uint32_t tc = TheBatch.SP34[ap34].ata;
            uint32_t td = TheBatch.SP34[ap34].atb;

            const double * dDab = Ds(ta,tb) + offAB;
            const double * dDcd = Ds(tc,td) + offCD;
            for (int mm=0; mm<sAB; ++mm) Dab[mm](k) = dDab[mm];
            for (int mm=0; mm<sCD; ++mm) Dcd[mm](k) = dDcd[mm];
        }

        Dab += sAB;
        Dcd += sCD;
    }

    //cudaMemcpy (b4blockGPU.dDab, b4blockGPU.dab, TheBatch.NtilesN * sAB * DPB * sizeof(double), cudaMemcpyHostToDevice);
    //cudaMemcpy (b4blockGPU.dDcd, b4blockGPU.dcd, TheBatch.NtilesN * sCD * DPB * sizeof(double), cudaMemcpyHostToDevice);

    //trick to copy in one operation
    //cudaMemcpy (b4blockGPU.dDab, b4blockGPU.dab, TheBatch.NtilesN * (sAB+sCD) * DPB * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpyAsync (b4blockGPU.dDab, b4blockGPU.dab, TheBatch.NtilesN * (sAB+sCD) * DPB * sizeof(double), cudaMemcpyHostToDevice, *stream);

    //do it in 1 call
    //cudaMemset(b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB+sCD)*sizeof(cachelineN<DPB>));
    cudaMemsetAsync(b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB+sCD)*sizeof(cachelineN<DPB>), *stream);
}

void BatchEvaluator::CopyDJfromDevice   (const ERIBatch & TheBatch, cudaStream_t * stream, Sparse & Js) {


    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;


    //cudaMemcpy (b4blockGPU.fab, b4blockGPU.dFab, TheBatch.NtilesN * sAB * DPB * sizeof(double), cudaMemcpyDeviceToHost);
    //cudaMemcpy (b4blockGPU.fcd, b4blockGPU.dFcd, TheBatch.NtilesN * sCD * DPB * sizeof(double), cudaMemcpyDeviceToHost);

    //same as last
    //cudaMemcpy (b4blockGPU.fab, b4blockGPU.dFab, TheBatch.NtilesN * (sAB+sCD) * DPB * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync (b4blockGPU.fab, b4blockGPU.dFab, TheBatch.NtilesN * (sAB+sCD) * DPB * sizeof(double), cudaMemcpyDeviceToHost, *stream);


    cachelineN<DPB> *Fab, *Fcd;
    Fab = (cachelineN<DPB>*)b4blockGPU.fab;
    Fcd = (cachelineN<DPB>*)b4blockGPU.fcd;

    bool S = ((TheBatch.geometry==ABAB) && TheBatch.SameShell) || (TheBatch.geometry==AACD);

    //wait until stream is done transfering before processing on CPU
    cudaStreamSynchronize(*stream);

    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {

        // update fock matrix
        for (uint32_t k=0; k<DPB; ++k) {
            if ((TheBatch.TileListN[i].useJ & (1UL<<k)) == 0) continue;

            uint32_t ap12 = TheBatch.TileListN[i].ap12[k];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[k];

            uint32_t ta = TheBatch.SP12[ap12].ata;
            uint32_t tb = TheBatch.SP12[ap12].atb;
            uint32_t tc = TheBatch.SP34[ap34].ata;
            uint32_t td = TheBatch.SP34[ap34].atb;

            //coulomb
            double * dFab = Js(ta,tb) + offAB;
            double * dFcd = Js(tc,td) + offCD;
            if (!S) {
                for (int mm=0; mm<sAB; ++mm) dFab[mm] += Fab[mm](k);
                for (int mm=0; mm<sCD; ++mm) dFcd[mm] += Fcd[mm](k);
            }
            else {
                for (int mm=0; mm<sAB; ++mm) dFab[mm] += 0.5*Fab[mm](k);
                for (int mm=0; mm<sCD; ++mm) dFcd[mm] += 0.5*Fcd[mm](k);
            }
        }

        Fab += sAB;
        Fcd += sCD;
    }
}


void BatchEvaluator::CopyDXtoDevice     (const ERIBatch & TheBatch, cudaStream_t * stream, const Sparse & Ds) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;


    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    cachelineN<DPB> *Dac, *Dbd, *Dad, *Dbc;
    Dac = (cachelineN<DPB>*)b4blockGPU.dac;
    Dad = (cachelineN<DPB>*)b4blockGPU.dad;
    Dbc = (cachelineN<DPB>*)b4blockGPU.dbc;
    Dbd = (cachelineN<DPB>*)b4blockGPU.dbd;


    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {

        for (uint32_t k=0; k<DPB; ++k) {
            uint32_t ap12 = TheBatch.TileListN[i].ap12[k];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[k];

            uint32_t ta = TheBatch.SP12[ap12].ata;
            uint32_t tb = TheBatch.SP12[ap12].atb;
            uint32_t tc = TheBatch.SP34[ap34].ata;
            uint32_t td = TheBatch.SP34[ap34].atb;

            const double * dDac = Ds(ta,tc) + offAC;
            const double * dDad = Ds(ta,td) + offAD;
            const double * dDbc = Ds(tb,tc) + offBC;
            const double * dDbd = Ds(tb,td) + offBD;

            for (int mm=0; mm<sAC; ++mm) Dac[mm](k) = dDac[mm];
            for (int mm=0; mm<sAD; ++mm) Dad[mm](k) = dDad[mm];
            for (int mm=0; mm<sBC; ++mm) Dbc[mm](k) = dDbc[mm];
            for (int mm=0; mm<sBD; ++mm) Dbd[mm](k) = dDbd[mm];
        }

        Dac += sAC;
        Dad += sAD;
        Dbc += sBC;
        Dbd += sBD;
    }

    //trick
    cudaMemcpyAsync (b4blockGPU.dDac, b4blockGPU.dac, TheBatch.NtilesN * (sAC+sAD+sBC+sBD) * DPB * sizeof(double), cudaMemcpyHostToDevice, *stream);

    cudaMemsetAsync(b4blockGPU.dFac, 0, TheBatch.NtilesN * (sAC+sAD+sBC+sBD)*sizeof(cachelineN<DPB>), *stream);
}

void BatchEvaluator::CopyDXfromDevice   (const ERIBatch & TheBatch, cudaStream_t * stream, Sparse & Xs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;


    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    //same trick
    cudaMemcpyAsync (b4blockGPU.fac, b4blockGPU.dFac, TheBatch.NtilesN * (sAC+sAD+sBC+sBD) * DPB * sizeof(double), cudaMemcpyDeviceToHost, *stream);


    cachelineN<DPB> *Fad, *Fbc, *Fac, *Fbd;
    Fad = (cachelineN<DPB>*)b4blockGPU.fad;
    Fbc = (cachelineN<DPB>*)b4blockGPU.fbc;
    Fac = (cachelineN<DPB>*)b4blockGPU.fac;
    Fbd = (cachelineN<DPB>*)b4blockGPU.fbd;


    bool S = ((TheBatch.geometry==ABAB) && TheBatch.SameShell) || (TheBatch.geometry==AACD);

    //wait until stream is done transfering before processing on CPU
    cudaStreamSynchronize(*stream);

    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {

        // update fock matrix
        for (uint32_t k=0; k<DPB; ++k) {
            if ((TheBatch.TileListN[i].useX & (1UL<<k)) == 0) continue;

            uint32_t ap12 = TheBatch.TileListN[i].ap12[k];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[k];

            uint32_t ta = TheBatch.SP12[ap12].ata;
            uint32_t tb = TheBatch.SP12[ap12].atb;
            uint32_t tc = TheBatch.SP34[ap34].ata;
            uint32_t td = TheBatch.SP34[ap34].atb;

            //exchange
            double * dFac = Xs(ta,tc) + offAC;
            double * dFad = Xs(ta,td) + offAD;
            double * dFbc = Xs(tb,tc) + offBC;
            double * dFbd = Xs(tb,td) + offBD;

            if (!S) {
                for (int mm=0; mm<sAC; ++mm) dFac[mm] += Fac[mm](k);
                for (int mm=0; mm<sAD; ++mm) dFad[mm] += Fad[mm](k);
                for (int mm=0; mm<sBC; ++mm) dFbc[mm] += Fbc[mm](k);
                for (int mm=0; mm<sBD; ++mm) dFbd[mm] += Fbd[mm](k);
            }
            //this is probably wrong for the antisymmetric component
            else {
                for (int mm=0; mm<sAC; ++mm) dFac[mm] += 0.5*Fac[mm](k);
                for (int mm=0; mm<sAD; ++mm) dFad[mm] += 0.5*Fad[mm](k);
                for (int mm=0; mm<sBC; ++mm) dFbc[mm] += 0.5*Fbc[mm](k);
                for (int mm=0; mm<sBD; ++mm) dFbd[mm] += 0.5*Fbd[mm](k);
            }

        }

        Fad += sAD;
        Fbc += sBC;
        Fac += sAC;
        Fbd += sBD;
    }
}

static const uint32_t MASK32 = 4294967295; // 2^32 -1

__global__ void Half      (double * v, int nelem) {
    int n = blockIdx.x*blockDim.x + threadIdx.x;

    if (n>=nelem) return;
    v[n] *= 0.5;
}

__global__ void Quarter   (double * v, int nelem) {
    int n = blockIdx.x*blockDim.x + threadIdx.x;

    if (n>=nelem) return;
    v[n] *= 0.25;
}

void BatchEvaluator::CopyDJXtoDevice    (const ERIBatch & TheBatch, cudaStream_t * stream, const Sparse & Ds) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    cachelineN<DPB> *Dab, *Dcd;
    Dab = (cachelineN<DPB>*)b4blockGPU.dab;
    Dcd = (cachelineN<DPB>*)b4blockGPU.dcd;

    cachelineN<DPB> *Dac, *Dbd, *Dad, *Dbc;
    Dac = (cachelineN<DPB>*)b4blockGPU.dac;
    Dad = (cachelineN<DPB>*)b4blockGPU.dad;
    Dbc = (cachelineN<DPB>*)b4blockGPU.dbc;
    Dbd = (cachelineN<DPB>*)b4blockGPU.dbd;


    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {

        for (uint32_t k=0; k<DPB; ++k) {
            uint32_t ap12 = TheBatch.TileListN[i].ap12[k];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[k];

            uint32_t ta = TheBatch.SP12[ap12].ata;
            uint32_t tb = TheBatch.SP12[ap12].atb;
            uint32_t tc = TheBatch.SP34[ap34].ata;
            uint32_t td = TheBatch.SP34[ap34].atb;

            const double * dDab = Ds(ta,tb) + offAB;
            const double * dDcd = Ds(tc,td) + offCD;

            const double * dDac = Ds(ta,tc) + offAC;
            const double * dDad = Ds(ta,td) + offAD;
            const double * dDbc = Ds(tb,tc) + offBC;
            const double * dDbd = Ds(tb,td) + offBD;

            for (int mm=0; mm<sAB; ++mm) Dab[mm](k) = dDab[mm];
            for (int mm=0; mm<sCD; ++mm) Dcd[mm](k) = dDcd[mm];

            for (int mm=0; mm<sAC; ++mm) Dac[mm](k) = dDac[mm];
            for (int mm=0; mm<sAD; ++mm) Dad[mm](k) = dDad[mm];
            for (int mm=0; mm<sBC; ++mm) Dbc[mm](k) = dDbc[mm];
            for (int mm=0; mm<sBD; ++mm) Dbd[mm](k) = dDbd[mm];
        }

        Dab += sAB;
        Dcd += sCD;

        Dac += sAC;
        Dad += sAD;
        Dbc += sBC;
        Dbd += sBD;
    }

    //copy ALL in one operation
    cudaMemcpyAsync (b4blockGPU.dDab, b4blockGPU.dab, TheBatch.NtilesN * (sAB+sCD+sAD+sAC+sBC+sBD) * DPB * sizeof(double), cudaMemcpyHostToDevice, *stream);

    //all 6 in 1 call
    cudaMemsetAsync (b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB+sCD+sAC+sAD+sBC+sBD)*sizeof(cachelineN<DPB>), *stream);
}


void BatchEvaluator::CopyERIsFromDevice (const ERIBatch & TheBatch, cudaStream_t * stream) {

    double * h_ERI = (double*)b4blockGPU.T8;
    double * d_ERI = (double*)b4blockGPU.dT8;

    cudaMemcpyAsync (h_ERI,  d_ERI, TheBatch.NtilesN * TheBatch.wsize4 * DPB * sizeof(double), cudaMemcpyDeviceToHost, *stream);
}

void BatchEvaluator::CopyKRLsFromDevice (const ERIBatch & TheBatch, cudaStream_t * stream) {

    double * h_ERI = (double*)b4blockGPU.I8;
    double * d_ERI = (double*)b4blockGPU.dI8;

    cudaMemcpyAsync (h_ERI,  d_ERI, TheBatch.NtilesN * TheBatch.msize4 * DPB * sizeof(double), cudaMemcpyDeviceToHost, *stream);
}


__global__ void ScatterDM  (const int * p1, const int * p2,   const double * vD, const unsigned int * pDm, int nAtoms,
                            int J1, int J2,    int l1, int l2,   uint32_t off12,   double * D12,  int Ntiles) {


    const int x = threadIdx.x;
    const int y = threadIdx.y;

    const int n = blockIdx.y;

    const int NTY = blockDim.y;

    if (n>=Ntiles) return;

    const int M1 = 2*l1+1;
    const int M2 = 2*l2+1;

    const int S12 = J1*J2*M1*M2;


    //__shared__ int t2[NTX];
    __shared__ const double * src[NTX];
    extern __shared__ double shared [];

    if (y==0) {
        const int nx = n*NTX + x;
        int t1 = p1[nx];
        int t2 = p2[nx];
        src[x] = vD + pDm [ t1*nAtoms + t2 ] + off12; // this is totally non-coalesced
    }
    __syncthreads();


    int yx = y*NTX + x;

    int s = yx/NTY;
    int d = yx%NTY;
    int ds = d*NTX + s;

    shared[ds] = src[s][d]; // coalesced!!!! (as much as possible)

    //shared[yx] = src[x][y]; //not coalesced!!!!
    __syncthreads();

    D12 [n*S12*NTX + yx] = shared[yx]; // perfectly coalesced
}

void BatchEvaluator::CopyDJXtoDevice    (const ERIBatch & TheBatch, cudaStream_t * stream, SparseGPU & gDs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    int Ntiles = TheBatch.NtilesN;

    dim3 Nbl (  1, Ntiles);
    dim3 Nab (NTX, sAB);
    dim3 Ncd (NTX, sCD);
    dim3 Nac (NTX, sAC);
    dim3 Nad (NTX, sAD);
    dim3 Nbc (NTX, sBC);
    dim3 Nbd (NTX, sBD);

    const int * pA = b4blockGPU.dAtA;
    const int * pB = b4blockGPU.dAtB;
    const int * pC = b4blockGPU.dAtC;
    const int * pD = b4blockGPU.dAtD;

    if (TheBatch.ABp->inverted) swap(pA,pB);
    if (TheBatch.CDp->inverted) swap(pC,pD);


    // make sure rhs is done doing whatever it is doing before adding everything up
    cudaEvent_t event;
    cudaEventCreate(&event);
    cudaEventRecord     (event, *gDs.stream[GPUdevice]); // add an event to the queue of gDs
    cudaStreamWaitEvent (stream[0],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (stream[1],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (stream[2],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (stream[3],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (stream[4],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (stream[5],event,0);    // wait for the event to complete
    cudaEventDestroy(event);


    // complementary order (F dependencies make sense this way)
    ScatterDM <<<Nbl,Ncd, sCD*NTX*8,stream[0]>>> (pC, pD,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JC,JD,  lc,ld,  offCD,  b4blockGPU.dDcd,  Ntiles);
    ScatterDM <<<Nbl,Nab, sAB*NTX*8,stream[1]>>> (pA, pB,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JA,JB,  la,lb,  offAB,  b4blockGPU.dDab,  Ntiles);

    ScatterDM <<<Nbl,Nbd, sBD*NTX*8,stream[2]>>> (pB, pD,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JB,JD,  lb,ld,  offBD,  b4blockGPU.dDbd,  Ntiles);
    ScatterDM <<<Nbl,Nbc, sBC*NTX*8,stream[3]>>> (pB, pC,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JB,JC,  lb,lc,  offBC,  b4blockGPU.dDbc,  Ntiles);
    ScatterDM <<<Nbl,Nad, sAD*NTX*8,stream[4]>>> (pA, pD,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JA,JD,  la,ld,  offAD,  b4blockGPU.dDad,  Ntiles);
    ScatterDM <<<Nbl,Nac, sAC*NTX*8,stream[5]>>> (pA, pC,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JA,JC,  la,lc,  offAC,  b4blockGPU.dDac,  Ntiles);

    //all 6 in 1 call
    //cudaMemsetAsync (b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB+sCD+sAC+sAD+sBC+sBD)*sizeof(cachelineN<DPB>), *stream);

    cudaMemsetAsync (b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB)*sizeof(cachelineN<DPB>), stream[0]);
    cudaMemsetAsync (b4blockGPU.dFcd, 0, TheBatch.NtilesN * (sCD)*sizeof(cachelineN<DPB>), stream[1]);

    cudaMemsetAsync (b4blockGPU.dFac, 0, TheBatch.NtilesN * (sAC)*sizeof(cachelineN<DPB>), stream[2]);
    cudaMemsetAsync (b4blockGPU.dFad, 0, TheBatch.NtilesN * (sAD)*sizeof(cachelineN<DPB>), stream[3]);
    cudaMemsetAsync (b4blockGPU.dFbc, 0, TheBatch.NtilesN * (sBC)*sizeof(cachelineN<DPB>), stream[4]);
    cudaMemsetAsync (b4blockGPU.dFbd, 0, TheBatch.NtilesN * (sBD)*sizeof(cachelineN<DPB>), stream[5]);
}

void BatchEvaluator::CopyDJXtoDevice    (const ERIBatch & TheBatch, cudaStream_t ** stream, SparseGPU & gDs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    int Ntiles = TheBatch.NtilesN;

    dim3 Nbl (  1, Ntiles);
    dim3 Nab (NTX, sAB);
    dim3 Ncd (NTX, sCD);
    dim3 Nac (NTX, sAC);
    dim3 Nad (NTX, sAD);
    dim3 Nbc (NTX, sBC);
    dim3 Nbd (NTX, sBD);

    const int * pA = b4blockGPU.dAtA;
    const int * pB = b4blockGPU.dAtB;
    const int * pC = b4blockGPU.dAtC;
    const int * pD = b4blockGPU.dAtD;

    if (TheBatch.ABp->inverted) swap(pA,pB);
    if (TheBatch.CDp->inverted) swap(pC,pD);


    // make sure rhs is done doing whatever it is doing before adding everything up
    /*
    cudaEvent_t event;
    cudaEventCreate(&event);
    cudaEventRecord     (event, *gDs.stream[GPUdevice]); // add an event to the queue of gDs
    cudaStreamWaitEvent (*stream[0],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (*stream[1],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (*stream[2],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (*stream[3],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (*stream[4],event,0);    // wait for the event to complete
    cudaStreamWaitEvent (*stream[5],event,0);    // wait for the event to complete
    cudaEventDestroy(event);*/


    // complementary order (F dependencies make sense this way)
    ScatterDM <<<Nbl,Ncd, sCD*NTX*8,*stream[0]>>> (pC, pD,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JC,JD,  lc,ld,  offCD,  b4blockGPU.dDcd,  Ntiles);
    ScatterDM <<<Nbl,Nab, sAB*NTX*8,*stream[1]>>> (pA, pB,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JA,JB,  la,lb,  offAB,  b4blockGPU.dDab,  Ntiles);

    ScatterDM <<<Nbl,Nbd, sBD*NTX*8,*stream[2]>>> (pB, pD,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JB,JD,  lb,ld,  offBD,  b4blockGPU.dDbd,  Ntiles);
    ScatterDM <<<Nbl,Nbc, sBC*NTX*8,*stream[3]>>> (pB, pC,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JB,JC,  lb,lc,  offBC,  b4blockGPU.dDbc,  Ntiles);
    ScatterDM <<<Nbl,Nad, sAD*NTX*8,*stream[4]>>> (pA, pD,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JA,JD,  la,ld,  offAD,  b4blockGPU.dDad,  Ntiles);
    ScatterDM <<<Nbl,Nac, sAC*NTX*8,*stream[5]>>> (pA, pC,   gDs.v[GPUdevice], gDs.p[GPUdevice], gDs.len,   JA,JC,  la,lc,  offAC,  b4blockGPU.dDac,  Ntiles);

    //all 6 in 1 call
    cudaMemsetAsync (b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB+sCD+sAC+sAD+sBC+sBD)*sizeof(cachelineN<DPB>), **stream);


    bool SS = ((TheBatch.geometry==ABAB) && TheBatch.SameShell);
    bool SAB = TheBatch.ABp->samef;
    bool SCD = TheBatch.CDp->samef;

    if      (SAB && SCD) {
        int nel = TheBatch.NtilesN * (sAB+sCD+sAC+sAD+sBC+sBD)* DPB;
        Quarter <<<(nel+1023)/1024, 1024, 0,**stream>>> (b4blockGPU.dDab, nel);
    }
    else if (SS || SAB || SCD) {
        int nel = TheBatch.NtilesN * (sAB+sCD+sAC+sAD+sBC+sBD)* DPB;
        Half    <<<(nel+1023)/1024, 1024, 0,**stream>>> (b4blockGPU.dDab, nel);
    }

    /*
    cudaMemsetAsync (b4blockGPU.dFab, 0, TheBatch.NtilesN * (sAB)*sizeof(cachelineN<DPB>), *stream[0]);
    cudaMemsetAsync (b4blockGPU.dFcd, 0, TheBatch.NtilesN * (sCD)*sizeof(cachelineN<DPB>), *stream[1]);

    cudaMemsetAsync (b4blockGPU.dFac, 0, TheBatch.NtilesN * (sAC)*sizeof(cachelineN<DPB>), *stream[2]);
    cudaMemsetAsync (b4blockGPU.dFad, 0, TheBatch.NtilesN * (sAD)*sizeof(cachelineN<DPB>), *stream[3]);
    cudaMemsetAsync (b4blockGPU.dFbc, 0, TheBatch.NtilesN * (sBC)*sizeof(cachelineN<DPB>), *stream[4]);
    cudaMemsetAsync (b4blockGPU.dFbd, 0, TheBatch.NtilesN * (sBD)*sizeof(cachelineN<DPB>), *stream[5]);
    */
}


static const int BTNT = 1024;

void BTsort (uint64_t * list, uint64_t nList, cudaStream_t * stream);
void BTsort (uint64_t * list, uint64_t nList, uint32_t nLists, cudaStream_t * stream);

// adds/reduces all submatrices corresponding to the same position in the partial fock update;
// requires previous transposition of the partial updates
__global__ void ReduceGatherFM2T(double * vF, const unsigned int * pFm,
                                 uint32_t S12, uint32_t off12,   const double * F12,
                                 const uint32_t * a2,
                                 const uint32_t * pa2,
                                 const uint32_t * ea2,
                                 const uint32_t * elements,
                                 uint32_t * nA2) {

    const uint32_t bx = blockIdx.x;
    const uint32_t by = blockIdx.y;

    const uint32_t byx = by*blockDim.x + bx;

    if (byx>=*nA2) return;

    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;
    const uint32_t tyx = ty*blockDim.x + tx; // this is sorted by warps

    // these are constant for the block
    __shared__ uint32_t ne;
    __shared__ const uint32_t * p;
    __shared__ double * dest;

    if (tyx==0 ) {
        uint32_t at12 = a2[byx];
        ne   = ea2[byx];
        p    = elements + pa2[byx];
        dest = vF + pFm[at12] + off12; // destination in matrix
    }
    __syncthreads();


    // mapping of (tx,ty) onto a transposed indexing
    // this helps coalesce the reads
    const uint32_t ttx = tyx/S12; // this runs slow, up to DPB
    const uint32_t tty = tyx%S12; // this runs fast, up to S12
    const uint32_t ttxy = ttx*S12 + tty;

    extern __shared__ double sdata[];
    sdata[tyx] = 0; // all threads initialize to 0

    for (uint32_t i=0; i<ne; i+=DPB) {
        if (i+ttx<ne)           sdata [ttxy] += F12[p[i+ttx]*S12 + tty]; // this access is very inefficient; F12 needs transposition
    }
    __syncthreads();


    //synchronization required, as sdata spans multiple warps
    if (ttx <  16)
        sdata[ttx*S12 + tty] += sdata[(ttx+16)*S12 + tty];
    __syncthreads();
    if (ttx <   8)
        sdata[ttx*S12 + tty] += sdata[(ttx+ 8)*S12 + tty];
    __syncthreads();
    if (ttx <   4)
        sdata[ttx*S12 + tty] += sdata[(ttx+ 4)*S12 + tty];
    __syncthreads();
    if (ttx <   2)
        sdata[ttx*S12 + tty] += sdata[(ttx+ 2)*S12 + tty];
    __syncthreads();
    if (ttx <   1)
        sdata[          tty] += sdata[         S12 + tty];
    __syncthreads();

    // update the local Fock update
    if (tyx<S12) dest[tx] += sdata[tx];
}

__global__ void TransposeFM (double * F, const uint32_t * used, uint32_t nTiles) {

    const uint32_t bx = blockIdx.x;
    const uint32_t by = blockIdx.y;

    const uint32_t byx = by*blockDim.x + bx;

    if (byx>=nTiles) return;

    extern __shared__ double sdata[];

    const uint32_t S12 = blockDim.y;
    const uint32_t W32 = blockDim.x;

    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;
    const uint32_t tyx = ty*blockDim.x + tx; // this is sorted by warps
    const uint32_t txy = tx*blockDim.y + ty;

    double * Fs = F + byx*S12*W32;

    // load the warps into shared memory
    sdata[tyx] = Fs[tyx];
    __syncthreads();

    uint32_t use = used[byx]; //LDU hopefully
    if ((use & (1<<tx))==0) sdata[tyx] = 0;
    __syncthreads();

    // write transposed
    Fs[txy] = sdata[tyx];
}

// extract lower bits to e,
// invalidate repeated a2s,
// set a pointer

__global__ void RepackUI64   (uint32_t * e, uint64_t * a2p, const uint64_t * a2e, int nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;
    if (n>=nElements) return;

    // gather
    uint64_t a2en = a2e[n];

    // split
    uint64_t an   = a2en >> 32;
    uint64_t aan; if (n==0) aan=-1; else aan = a2e[n-1] >> 32;
    uint32_t en   = a2en;

    // if it's first of the sublist
    if (an != aan)  a2p[n] = (an<<32) + n;
    else            a2p[n] = -1;

    // write element to memory
    e[n] = en;
}

__global__ void ExtractUI64   (uint32_t * na2, uint32_t * a2, uint32_t * p, uint32_t * c, const uint64_t * a2p, int nElements) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;
    if (n>=nElements) return;

    // gather
    uint64_t a2xn = a2p[n];
    if (a2xn==-1) return; // nothing to be done

    // split
    uint32_t an  = a2xn >> 32;    // hi bits
    uint32_t pn  = a2xn;
    uint32_t pnn;

    // for the very last meaningful element
    if (n+1==nElements) {
        pnn = nElements;
        *na2 = n+1;
    }
    else if (a2p[n+1]==-1) {
        pnn = nElements;
        *na2 = n+1;
    }
    else {
        pnn = a2p[n+1];
    }

    // write element to memory
    a2[n] = an;
    p [n] = pn;
    c [n] = pnn-pn;
}

// generate initial list of packed int64 values
__global__ void A2Elist   (uint64_t * a2e, const int * p1, const int * p2, const uint32_t * use, uint64_t nAtoms, uint64_t nElements) {

    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;

    const uint64_t tyx = ty*32 + tx;
    const uint64_t n = blockIdx.x*32*32  +  tyx;


    __shared__ uint32_t used[32]; // 32 * 32 =
    if (ty==0) used[tx] = use[blockIdx.x*32 + tx];
    __syncthreads();

    if (n>=nElements) return;

    uint64_t at1 = p1[n];
    uint64_t at2 = p2[n];

    if (used[ty]&(1<<tx)) {
        uint64_t at12 = (at1*nAtoms + at2);
        a2e[n] = (at12<<32) + n;
    }
    else
        a2e[n] = -1; //don't use (will float to the top when sorted)
}

// generate initial list of packed int64 values
__global__ void A2Elist   (uint64_t * a2e, const int * p1, const int * p2, uint64_t nAtoms, uint64_t nElements) {

    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;

    const uint64_t tyx = ty*32 + tx;
    const uint64_t n = blockIdx.x*32*32  +  tyx;

    if (n>=nElements) return;

    uint64_t at1 = p1[n];
    uint64_t at2 = p2[n];

    uint64_t at12 = (at1*nAtoms + at2);
    a2e[n] = (at12<<32) + n;
}

// generate initial list of packed int64 values
__global__ void A2Elist   (uint64_t * a2e1, uint64_t * a2e2, const int * p1, const int * p2, const int * p3, const int * p4,  uint64_t nAtoms, uint64_t nElements) {

    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;

    const uint64_t tyx = ty*32 + tx;
    const uint64_t n = blockIdx.x*32*32  +  tyx;

    if (n>=nElements) return;

    uint64_t at1 = p1[n];
    uint64_t at2 = p2[n];
    uint64_t at3 = p3[n];
    uint64_t at4 = p4[n];

    uint64_t at12 = (at1*nAtoms + at2);
    uint64_t at34 = (at3*nAtoms + at4);

    a2e1[n] = (at12<<32) + n;
    a2e2[n] = (at34<<32) + n;
}





// works, but unfortuanately it's too slow for short lists (especially the two sortings)
void SetReduce (uint32_t * ip,
                const int * p1, const int * p2,  uint32_t nAtoms, uint32_t nTiles,
                cudaStream_t * stream, uint32_t * nA2r) {

     //device temporal arrays
    uint64_t * a2p   = (uint64_t*)ip;
    uint32_t * e    = ip + 2*DPB*nTiles;
    uint32_t * c    = ip + 3*DPB*nTiles;
    uint32_t * p    = ip + 4*DPB*nTiles;
    uint32_t * a2r  = ip + 5*DPB*nTiles;
    uint32_t * na2  = ip + 6*DPB*nTiles;



    //if (nTiles>64)
    {
        uint64_t * a2e   = (uint64_t*)p;

        uint32_t nElements = nTiles*DPB;  // the transposition kernel makes sure of zeroing the unused matrices

        //create the list
        const int nBlocks = (nElements + BTNT-1)/BTNT;
        dim3 nTh2(32,32);
        A2Elist <<<nBlocks,nTh2,0,*stream>>> (a2e, p1,p2, nAtoms, nElements); // don't set the unused to -1; they will be taken care of by the transposition kernel
        //sort it (at1 > at2 > e)
        BTsort (a2e, nElements, stream);
        //extract the elements from the sorted list; invalidate duplicated keys and substitute e for p in array
        RepackUI64 <<<nBlocks,BTNT,0,*stream>>> (e, a2p, a2e, nElements);
        //get rid of all -1's in the middle
        BTsort (a2p, nElements, stream);
        //extract data to multiple lists
        ExtractUI64 <<<nBlocks,BTNT,0,*stream>>> (na2, a2r,p,c, a2p, nElements);
    }
    /*
    else if (nTiles>32)
        xReduce <10> <<<1,1024, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
    else if (nTiles>16)
        xReduce < 9> <<<1, 512, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
    else if (nTiles> 8)
        xReduce < 8> <<<1, 256, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
    else if (nTiles> 4)
        xReduce < 7> <<<1, 128, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
    else if (nTiles> 2)
        xReduce < 6> <<<1,  64, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
    else if (nTiles> 1)
        xReduce < 5> <<<1,  32, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
    else
        xReduce < 4> <<<1,  16, 0,*stream>>>  (na2, a2r, p, c, e,    a2p,    p1, p2, nAtoms, nTiles*DPB);
        */


    cudaMemcpyAsync(nA2r, na2, sizeof(int32_t), cudaMemcpyDeviceToHost, *stream); // copy number of elements in the packed matrix
}

void DoReduce ( double * dF, // device array with all the relevant subblocks
                uint32_t * ip,
                const uint32_t * use, uint32_t nAtoms, uint32_t nTiles, uint32_t off12, uint32_t S12, cudaStream_t * stream, uint32_t * n2XX,
                double * vF, const uint32_t * pF) {

    //device temporal arrays
    uint64_t * a2x   = (uint64_t*)ip;
    uint32_t * e    = ip + 2*DPB*nTiles;
    uint32_t * c    = ip + 3*DPB*nTiles;
    uint32_t * p    = ip + 4*DPB*nTiles;
    uint32_t * a2r  = ip + 5*DPB*nTiles;
    uint32_t * na2  = ip + 6*DPB*nTiles;

    dim3 rgTh (DPB, S12); // threads
    const int MAXB = 32*1024; // actually for <=2.x is almost twice as much

    #ifdef __GATHER_CPU__
    if (S12>1) // skip transposition if it is a column vector (if lists are set on CPU, unused elements are skipped and are not required to be set to 0)
    #endif
    {
        int bby = (nTiles+MAXB-1) / MAXB;
        int bbx = (nTiles+bby-1)  / bby;
        dim3 tpBl (bbx, bby);

        // no event needed for the same stream
        TransposeFM <<<tpBl,rgTh, S12*DPB*sizeof(double),*stream>>> (dF, use, nTiles);
    }


    uint32_t nA2r = *n2XX;  // all BatchEvaluator becomes blocked only because of this thing

    {
        // find nbx,nby compatible with CUDA block dimension limitations (for compute capability up to 2.x)
        int nby = (nA2r+MAXB-1) / MAXB;
        int nbx = (nA2r+nby-1)  / nby;
        dim3 rgBl (nbx, nby);

        ReduceGatherFM2T            <<<rgBl,rgTh, S12*DPB*sizeof(double),*stream>>>
                                    ( vF, pF,  //
                                     S12, off12,   dF,
                                     a2r,   //  reduced  a2
                                     p,     // (reduced) pointer to e
                                     c,     // (reduced) number of elements of e
                                     e,     //  raw array of elements
                                     na2); //  number of A2
    }
}


/*

// attempt to use a stride twice the size

__global__ void ReduceGatherFM3 (double * vF, const unsigned int * pFm,  // sparse matrix values and pointers
                                 uint32_t S12, uint32_t off12,           // size of blocks, offset is sparse matrix subblock
                                 double * F12,                           // block matrices' values
                                 const uint64_t * a2e,                   // packed a2 plus the pointer
                                 uint32_t nE,                            // number of elements
                                 uint32_t S                              // stride of input array
                                 ) {

    const int N = 64; // 64*3*3 < 1024

    const uint32_t bx = blockIdx.x;
    const uint32_t BX = blockDim.x;
    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;

    const uint32_t tyx = (ty*N + tx);
    const uint32_t ttx = tyx/S12;
    const uint32_t tty = tyx%S12;


    if (bx*2*N*S >= nE) return; // skip whole block

    int i = (bx*2*N + tx) * S;  // read every S elements

    __shared__ double * d[2*N];  // pointer to destination submatrix
    __shared__ double * f[2*N];  // pointer to source

           __shared__ uint32_t a2   [2*N];  //
    extern __shared__ double   buff [ ];  //

    if (ty==0) a2[tx] = a2[tx+N] = -1;
    buff[ty*2*N + tx] = buff[ty*2*N + tx+N] = 0; //init to 0

    if (ty==0) {
        if (i<nE) {
            // split a and e
            uint64_t ae = a2e[i];
            uint32_t a = (ae>>32);
            uint32_t e = ae;

            // save stuff
            a2[tx] = a;
            d [tx] = vF + pFm[a] + off12;
            f [tx] = F12 + (e * S12);
        }
        if (i+N*S<nE) {
            // split a and e
            uint64_t ae = a2e[i+N*S];
            uint32_t a = (ae>>32);
            uint32_t e = ae;

            // save stuff
            a2[N+tx] = a;
            d [N+tx] = vF + pFm[a] + off12;
            f [N+tx] = F12 + (e * S12);
        }
    }

    __syncthreads();

    if ((ttx==0)&&(gridDim.x>1))
        d[0] = f[0]; // use first element as partial accumulator
    else if ((a2[ttx]!=-1))
        buff[tty*2*N+ttx] = f[ttx][tty]; // read subblocks from array (VERY BAD)

    if ((a2[ttx+N]!=-1))
        buff[tty*2*N+ttx+N] = f[ttx+N][tty]; // read subblocks from array (VERY BAD)

    __syncthreads();


    // parallel reduce elements
    // with same destination
    // ========================

    __shared__ uint8_t c[2*N];
    if (ty==0) c[tx] = c[tx+N] = 0;
    if (ty==0) {
        if ( (tx==0) || ((tx>0)&&(a2[tx]!=-1)&&(a2[tx]!=a2[tx-1])) ) c[tx] = 2*N/2; // mark every first element
        if (            (  (a2[tx+N]!=-1)&&(a2[tx+N]!=a2[tx+N-1])) ) c[tx+N] = 2*N/2; // mark every first element
     }
    __syncthreads();

    #pragma unroll
    for (int e=N/2; e>=1; e/=2) {
        if (ty==0)
        if (c[tx]>0) {
            // if there is a similar element in the position
            if ((tx+e<N) && (a2[tx]==a2[tx+e])) {
                if (c[tx+e]==0) c[tx+e] = e/2; // don't overwrite if already a higher value present
            }
            else c[tx] = e/2; // not that many elements
        }
        __syncthreads();
    }

    // usual parallel reduce
    #pragma unroll
    for (int e=1; e<N; e*=2) {
        if (c[tx]>=e) {
            buff[ty*BX+tx] += buff[ty*BX+tx+e]; // reduce
            if (ty==0) a2[tx+e] = -1; // invalidate buffer
        }
       __syncthreads();
    }

    // update target matrix (only surviving elements)
    if (a2[ttx]!=-1) {
        d[ttx][tty] += buff[tty*BX+ttx];
    }
}

*/


// simplified scheme: requires a sorted (grouped) input A2
// adds to the sparse matrix all contributions except for the first one in each block
// the reduced submatrix for the first contribution is added to the original list

// requires previous transposition of the partial updates

__global__ void ReduceGatherFM3 (double * vF, const unsigned int * pFm,  // sparse matrix values and pointers
                                 uint32_t S12, uint32_t off12,           // size of blocks, offset is sparse matrix subblock
                                 double * F12,                           // block matrices' values
                                 const uint64_t * a2e,                   // packed a2 plus the pointer
                                 uint32_t nE,                            // number of elements
                                 uint32_t S                              // stride of input array
                                 ) {

    const int N = 64; // 64*3*3 < 1024

    const uint32_t bx = blockIdx.x;
    const uint32_t BX = blockDim.x;
    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;

    const uint32_t tyx = (ty*N + tx);
    const uint32_t ttx = tyx/S12;
    const uint32_t tty = tyx%S12;


    if (bx*BX*S >= nE) return; // skip whole block

    int i = (bx*BX + tx) * S;  // read every S elements

    __shared__ double * d[N];  // pointer to destination submatrix
    __shared__ double * f[N];  // pointer to source

           __shared__ uint32_t a2   [N];  //
    extern __shared__ double   buff [ ];  //

    if (ty==0) a2[tx] = -1;
    buff[ty*N+tx] = 0; //init to 0

    if (ty==0) if (i<nE) {
        // split a and e
        uint64_t ae = a2e[i];
        uint32_t a = (ae>>32);
        uint32_t e = ae;

        // save stuff
        a2[tx] = a;
        d [tx] = vF + pFm[a] + off12;
        f [tx] = F12 + (e * S12);
    }

    __syncthreads();

    if ((ttx==0)&&(gridDim.x>1))
        d[0] = f[0]; // use first element as partial accumulator
    else if ((a2[ttx]!=-1))
        buff[tty*BX+ttx] = f[ttx][tty]; // read subblocks from array (VERY BAD)

    __syncthreads();


    // parallel reduce elements
    // with same destination
    // ========================

    __shared__ uint8_t c[N];
    if (ty==0) c[tx] = 0;
    if (ty==0) if ( (tx==0) || ((tx>0)&&(a2[tx]!=-1)&&(a2[tx]!=a2[tx-1])) ) c[tx] = N/2; // mark every first element
    __syncthreads();

    #pragma unroll
    for (int e=N/2; e>=1; e/=2) {
        if (ty==0)
        if (c[tx]>0) {
            // if there is a similar element in the position
            if ((tx+e<N) && (a2[tx]==a2[tx+e])) {
                if (c[tx+e]==0) c[tx+e] = e/2; // don't overwrite if already a higher value present
            }
            else c[tx] = e/2; // not that many elements
        }
        __syncthreads();
    }

    // usual parallel reduce
    #pragma unroll
    for (int e=1; e<N; e*=2) {
        if (c[tx]>=e) {
            buff[ty*BX+tx] += buff[ty*BX+tx+e]; // reduce
            if (ty==0) a2[tx+e] = -1; // invalidate buffer
        }
       __syncthreads();
    }

    // update target matrix (only surviving elements)
    if (a2[ttx]!=-1) {
        d[ttx][tty] += buff[tty*BX+ttx];
    }
}



// somewhat improved running time wrt older approach
void SetDoReduce ( double * dF, // device array with all the relevant subblocks
                uint64_t * a2p,
                const int * p1, const int * p2,  const uint32_t * use, uint32_t nAtoms, uint32_t nTiles, uint32_t off12, uint32_t S12, cudaStream_t * stream,
                double * vF, const uint32_t * pF) {

    uint32_t nElements = nTiles*DPB;  // the transposition kernel makes sure of zeroing the unused matrices


    dim3 tpTh (DPB, S12); // threads

    const int MAXB = 32*1024; // actually for <=2.x is almost twice as much
    int bby = (nTiles+MAXB-1) / MAXB;
    int bbx = (nTiles+bby-1)  / bby;

    dim3 tpBl (bbx, bby);

    TransposeFM <<<tpBl,tpTh, S12*DPB*sizeof(double),*stream>>> (dF, use, nTiles);


    int nBlocks = nElements;
    int S = 1;

    dim3 rgTh (2*DPB, S12); // threads (half the number of elements)

    while (nBlocks>1) {
        nBlocks = (nBlocks + 63) / 64;

        ReduceGatherFM3 <<<nBlocks, rgTh, S12*4*DPB*sizeof(double),*stream>>>
                                ( vF, pF, S12, off12, // output
                                 dF, a2p,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride
        S *= 64;
    }


}


__global__ void ReduceGatherFM4 (double * vF, const unsigned int * pFm,  // sparse matrix values and pointers
                                 uint32_t S12, uint32_t off12,           // size of blocks, offset is sparse matrix subblock
                                 double * F12,                           // block matrices' values
                                 const uint64_t * a2e,                   // packed a2 plus the pointer
                                 uint32_t nE,                            // number of elements
                                 uint32_t S                              // stride of input array
                                 ) {

    const int N = 64; // 64*3*3 < 1024

    const uint32_t bx = blockIdx.x;
    const uint32_t BX = blockDim.x;
    const uint32_t tx = threadIdx.x;
    const uint32_t ty = threadIdx.y;

    const uint32_t tyx = (ty*N + tx);
    const uint32_t ttx = tyx/S12;
    const uint32_t tty = tyx%S12;


    if (bx*BX*S >= nE) return; // skip whole block

    int i = (bx*BX + tx) * S;  // read every S elements

    __shared__ double * d[N];  // pointer to destination submatrix
    __shared__ double * f[N];  // pointer to source

           __shared__ uint32_t a2   [N];  //
    extern __shared__ double   buff [ ];  //

    if (ty==0) a2[tx] = -1;
    buff[ty*N+tx] = 0; //init to 0

    if (ty==0) if (i<nE) {
        // split a and e
        uint64_t ae = a2e[i];
        uint32_t a = (ae>>32);
        uint32_t e = ae;

        // save stuff
        a2[tx] = a;
        d [tx] = vF + pFm[a] + off12;
        f [tx] = F12 + (i * S12); // changed
    }

    __syncthreads();

    if ((ttx==0)&&(gridDim.x>1))
        d[0] = f[0]; // use first element as partial accumulator
    else if ((a2[ttx]!=-1))
        buff[tty*BX+ttx] = f[ttx][tty]; // read subblocks from array (VERY BAD)

    __syncthreads();


    // parallel reduce elements
    // with same destination
    // ========================

    __shared__ uint8_t c[N];
    if (ty==0) c[tx] = 0;
    if (ty==0) if ( (tx==0) || ((tx>0)&&(a2[tx]!=-1)&&(a2[tx]!=a2[tx-1])) ) c[tx] = N/2; // mark every first element
    __syncthreads();

    #pragma unroll
    for (int e=N/2; e>=1; e/=2) {
        if (ty==0)
        if (c[tx]>0) {
            // if there is a similar element in the position
            if ((tx+e<N) && (a2[tx]==a2[tx+e])) {
                if (c[tx+e]==0) c[tx+e] = e/2; // don't overwrite if already a higher value present
            }
            else c[tx] = e/2; // not that many elements
        }
        __syncthreads();
    }

    // usual parallel reduce
    #pragma unroll
    for (int e=1; e<N; e*=2) {
        if (c[tx]>=e) {
            buff[ty*BX+tx] += buff[ty*BX+tx+e]; // reduce
            if (ty==0) a2[tx+e] = -1; // invalidate buffer
        }
       __syncthreads();
    }

    // update target matrix (only surviving elements)
    if (a2[ttx]!=-1) {
        d[ttx][tty] += buff[tty*BX+ttx];
    }
}



__global__ void PointersDM  (double ** d_D, uint64_t * a2e,
                                const int * p1, const int * p2, const int * p3, const int * p4,
                                double * vD, const unsigned int * pDm, int nAtoms,
                                const int S12,   uint32_t off12,   int nE) {

    const int t = threadIdx.x;
    const int b = blockIdx.x;
    const int n = b*blockDim.x + t;

    if (n>=nE) return;

    int t1 = p1[n];
    int t2 = p2[n];

    uint64_t t3 = p3[n];
    uint64_t t4 = p4[n];

    uint64_t t34 = (t3*nAtoms + t4);

    d_D[n] = vD + pDm [ t1*nAtoms + t2 ] + off12;
    a2e[n] = (t34<<32) + n;
}

__global__ void PointersFM  (double ** d_F, double * F, const uint64_t * a2e,  int S12, int nE) {

    const int t = threadIdx.x;
    const int b = blockIdx.x;
    const int n = b*blockDim.x + t;

    if (n>=nE) return;

    uint32_t p = a2e[n]; // get lowest bits
    d_F[p] = F + n*S12;
}

__global__ void FixFM (double * F, const uint32_t * used, const uint64_t * a2e, uint32_t S12, double scale, uint32_t nE) {

    const uint32_t bx = blockIdx.x;
    const uint32_t tx = threadIdx.x;
    const uint32_t n = bx*blockDim.x + tx;

    if (n>=nE) return;

    uint32_t p = a2e[n]; // get lowest bits (index)

    uint32_t use = used[p/32]; //LDU hopefully
    bool u = true;
    if ((use & (1<<(p%32)))==0) u = false;

    if      (!u)       for (int s=0; s<S12; ++s) F[p*S12+s]  = 0;
    else if (scale!=1) for (int s=0; s<S12; ++s) F[p*S12+s] *= scale;
}


void BatchEvaluator::FullDJXcontraction (const ERIBatch & TheBatch, cudaStream_t ** stream,   SparseGPU & gDs, SparseGPU & gJs, SparseGPU & gXs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    uint32_t nAtoms = gJs.len;
    uint32_t nTiles = TheBatch.NtilesN;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    const int * dA = b4blockGPU.dAtA;
    const int * dB = b4blockGPU.dAtB;
    const int * dC = b4blockGPU.dAtC;
    const int * dD = b4blockGPU.dAtD;

    if (TheBatch.ABp->inverted) swap(dA,dB);
    if (TheBatch.CDp->inverted) swap(dC,dD);



    uint32_t nElements = nTiles*DPB;  // the transposition kernel makes sure of zeroing the unused matrices

    //split the int buffer on GPU in multiple arrays
    uint64_t * dj1   = (uint64_t*) b4blockGPU.dIntBuff;
    uint64_t * dj2   = dj1 + nElements;
    uint64_t * dx1   = dj2 + nElements;
    uint64_t * dx2   = dx1 + nElements;
    uint64_t * dx3   = dx2 + nElements;
    uint64_t * dx4   = dx3 + nElements;


    //pointers to device partial updates
    double ** ddd = (double**)(b4blockGPU.dIntBuff + 12*nElements);

    double ** d_Dab = ddd;
    double ** d_Dcd = ddd +    nElements;
    double ** d_Dad = ddd +  2*nElements;
    double ** d_Dbc = ddd +  3*nElements;
    double ** d_Dac = ddd +  4*nElements;
    double ** d_Dbd = ddd +  5*nElements;

    double ** d_Jab = ddd +  6*nElements;
    double ** d_Jcd = ddd +  7*nElements;
    double ** d_Xad = ddd +  8*nElements;
    double ** d_Xbc = ddd +  9*nElements;
    double ** d_Xac = ddd + 10*nElements;
    double ** d_Xbd = ddd + 11*nElements;


    cudaMemsetAsync (         ddd, 0,                      (12*nElements)*sizeof(double**), **stream);
    cudaMemsetAsync (b4blockGPU.dFab, 0, (sAB+sCD+sAC+sAD+sBC+sBD)*nElements*sizeof(double)  , **stream);


    // make sure D matrix is ready
    {
        cudaEvent_t event;
        cudaEventCreate    (&event);
        cudaEventRecord     (event, *gDs.stream[GPUdevice]); // add an event to the queue of gDs
        cudaStreamWaitEvent (**stream,event,0);    // wait for the event to complete
        cudaEventDestroy    (event);
    }

    // set, sort
    {
        int nBlocks = (nElements + 1024-1)/1024;
        PointersDM <<<nBlocks,1024,0,**stream>>>  (d_Dab,  dj2,  dA,dB,dC,dD,  gDs.v[GPUdevice], gDs.p[GPUdevice], nAtoms,    sAB, offAB,   nElements);
        PointersDM <<<nBlocks,1024,0,**stream>>>  (d_Dcd,  dj1,  dC,dD,dA,dB,  gDs.v[GPUdevice], gDs.p[GPUdevice], nAtoms,    sCD, offCD,   nElements);

        PointersDM <<<nBlocks,1024,0,**stream>>>  (d_Dac,  dx4,  dA,dC,dB,dD,  gDs.v[GPUdevice], gDs.p[GPUdevice], nAtoms,    sAC, offAC,   nElements);
        PointersDM <<<nBlocks,1024,0,**stream>>>  (d_Dad,  dx3,  dA,dD,dB,dC,  gDs.v[GPUdevice], gDs.p[GPUdevice], nAtoms,    sAD, offAD,   nElements);
        PointersDM <<<nBlocks,1024,0,**stream>>>  (d_Dbc,  dx2,  dA,dD,dB,dC,  gDs.v[GPUdevice], gDs.p[GPUdevice], nAtoms,    sBC, offBC,   nElements);
        PointersDM <<<nBlocks,1024,0,**stream>>>  (d_Dbd,  dx1,  dA,dC,dB,dD,  gDs.v[GPUdevice], gDs.p[GPUdevice], nAtoms,    sBD, offBD,   nElements);

        if (0) {
        //(double**)(b4blockGPU.dIntBuff + 12*nElements);

            cudaMemcpyAsync (b4blockGPU.IntBuff, b4blockGPU.dIntBuff+12*nElements, nElements*6*sizeof(double**), cudaMemcpyHostToDevice, **stream);

            //cudaMemcpyAsync (b4blockGPU.IntBuff, b4blockGPU.dIntBuff, nElements*6*sizeof(uint64_t), cudaMemcpyHostToDevice, **stream);

            cudaStreamSynchronize(**stream);

            for (int jx=0; jx<6; ++jx) {
                //uint64_t * ii =  (uint64_t*)(b4blockGPU.IntBuff + 2*jx*nElements);
                //cout << jx << " ::: ";
                //for (int i=0; i<nElements; ++i)
                //    cout << (ii[i]>>32) << "," << (ii[i]&(1024*1024-1)) << "   ";

                double ** dd = (double**)(b4blockGPU.IntBuff + 2*jx*nElements);
                cout << jx << " ::: ";
                for (int i=0; i<nElements; ++i) {
                    cout << (dd[i]) << "  ";
                }
                cout << endl;
                cout << endl;
            }
            cout << endl;
            char a; cin >> a;

        }

        BTsort (dj1, nElements, 6, *stream);

        PointersFM <<<nBlocks,1024,0,**stream>>>  (d_Jcd,  b4blockGPU.dFcd,  dj2,  sCD, nElements);
        PointersFM <<<nBlocks,1024,0,**stream>>>  (d_Jab,  b4blockGPU.dFab,  dj1,  sAB, nElements);

        PointersFM <<<nBlocks,1024,0,**stream>>>  (d_Xbd,  b4blockGPU.dFbd,  dx4,  sBD, nElements);
        PointersFM <<<nBlocks,1024,0,**stream>>>  (d_Xbc,  b4blockGPU.dFbc,  dx3,  sBC, nElements);
        PointersFM <<<nBlocks,1024,0,**stream>>>  (d_Xad,  b4blockGPU.dFad,  dx2,  sAD, nElements);
        PointersFM <<<nBlocks,1024,0,**stream>>>  (d_Xac,  b4blockGPU.dFac,  dx1,  sAC, nElements);
    }

    // this produces 0s
    ContractDJXs       (TheBatch, stream,
                            d_Dab, d_Dcd,  d_Dac, d_Dad, d_Dbc, d_Dbd,
                            d_Jab, d_Jcd,  d_Xac, d_Xad, d_Xbc, d_Xbd);


        {
        //(double**)(b4blockGPU.dIntBuff + 12*nElements);

            cudaMemcpyAsync (b4blockGPU.fab, b4blockGPU.dFab, (sAB+sCD+sAC+sAD+sBC+sBD)*nElements*sizeof(double), cudaMemcpyHostToDevice, **stream);

            //cudaMemcpyAsync (b4blockGPU.IntBuff, b4blockGPU.dIntBuff, nElements*6*sizeof(uint64_t), cudaMemcpyHostToDevice, **stream);

            cudaStreamSynchronize(**stream);

            for (int jx=0; jx<6; ++jx) {
                //uint64_t * ii =  (uint64_t*)(b4blockGPU.IntBuff + 2*jx*nElements);
                //cout << jx << " ::: ";
                //for (int i=0; i<nElements; ++i)
                //    cout << (ii[i]>>32) << "," << (ii[i]&(1024*1024-1)) << "   ";

                double * dd = (double*)b4blockGPU.fab + sAB*nElements;


                cout << jx << " ::: ";
                for (int i=0; i<nElements*sAB; ++i) {
                    cout << (dd[i]) << "  ";
                }
                cout << endl;
                cout << endl;
            }
            cout << endl;
            char a; cin >> a;

        }


    //cudaMemsetAsync (b4blockGPU.dFab, -10000000, (sAB+sCD+sAC+sAD+sBC+sBD)*nElements*sizeof(double), **stream);

    // fix unused, scale required

    // this is not the correct pointer for disabling
    if (1) {
        double scale = 1;
        bool SS = ((TheBatch.geometry==ABAB) && TheBatch.SameShell);
        bool SAB = TheBatch.ABp->samef;
        bool SCD = TheBatch.CDp->samef;

        if      (SAB && SCD)       scale = 0.25;
        else if (SS || SAB || SCD) scale = 0.5;

        FixFM <<<nTiles,32,0,**stream>>> (b4blockGPU.dFab, b4blockGPU.dJuse, dj1,  sAB, scale, nElements);
        FixFM <<<nTiles,32,0,**stream>>> (b4blockGPU.dFcd, b4blockGPU.dJuse, dj2,  sCD, scale, nElements);
        FixFM <<<nTiles,32,0,**stream>>> (b4blockGPU.dFac, b4blockGPU.dXuse, dx1,  sAC, scale, nElements);
        FixFM <<<nTiles,32,0,**stream>>> (b4blockGPU.dFad, b4blockGPU.dXuse, dx2,  sAD, scale, nElements);
        FixFM <<<nTiles,32,0,**stream>>> (b4blockGPU.dFbc, b4blockGPU.dXuse, dx3,  sBC, scale, nElements);
        FixFM <<<nTiles,32,0,**stream>>> (b4blockGPU.dFbd, b4blockGPU.dXuse, dx4,  sBD, scale, nElements);
    }





    // gather - reduce
    int nBlocks = nElements;
    int S = 1;


    while (nBlocks>1) {
        nBlocks = (nBlocks + 2*DPB-1) / (2*DPB);

        dim3 rAB (2*DPB, sAB);
        dim3 rCD (2*DPB, sCD);
        dim3 rAC (2*DPB, sAC);
        dim3 rAD (2*DPB, sAD);
        dim3 rBC (2*DPB, sBC);
        dim3 rBD (2*DPB, sBD);


        ReduceGatherFM4 <<<nBlocks, rAB, sAB*4*DPB*sizeof(double),**stream>>>
                                ( gJs.v[GPUdevice], gJs.p[GPUdevice],  sAB, offAB, // output
                                 b4blockGPU.dFab, dj1,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride

        ReduceGatherFM4 <<<nBlocks, rCD, sCD*4*DPB*sizeof(double),**stream>>>
                                ( gJs.v[GPUdevice], gJs.p[GPUdevice],  sCD, offCD, // output
                                 b4blockGPU.dFcd, dj2,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride


        ReduceGatherFM4 <<<nBlocks, rAC, sAC*4*DPB*sizeof(double),**stream>>>
                                ( gXs.v[GPUdevice], gXs.p[GPUdevice],  sAC, offAC, // output
                                 b4blockGPU.dFac, dx1,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride

        ReduceGatherFM4 <<<nBlocks, rAD, sAD*4*DPB*sizeof(double),**stream>>>
                                ( gXs.v[GPUdevice], gXs.p[GPUdevice],  sAD, offAD, // output
                                 b4blockGPU.dFad, dx2,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride

        ReduceGatherFM4 <<<nBlocks, rBC, sBC*4*DPB*sizeof(double),**stream>>>
                                ( gXs.v[GPUdevice], gXs.p[GPUdevice],  sBC, offBC, // output
                                 b4blockGPU.dFbc, dx3,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride

        ReduceGatherFM4 <<<nBlocks, rBD, sBD*4*DPB*sizeof(double),**stream>>>
                                ( gXs.v[GPUdevice], gXs.p[GPUdevice],  sBD, offBD, // output
                                 b4blockGPU.dFbd, dx4,             // input (some parts of dF also act as partial accumulators)
                                 nElements, S);       // elements and stride

        S *= 2*DPB;
    }


}

void BatchEvaluator::CopyDJXfromDevice  (const ERIBatch & TheBatch, cudaStream_t ** stream, uint32_t * n2XX, SparseGPU & gJs, SparseGPU & gXs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    uint32_t nAtoms = gJs.len;
    uint32_t nTiles = TheBatch.NtilesN;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;

    //pointers to device partial updates
    double *dFab, *dFcd;
    dFab = b4blockGPU.dFab;
    dFcd = b4blockGPU.dFcd;

    double *dFad, *dFbc, *dFac, *dFbd;
    dFad = b4blockGPU.dFad;
    dFbc = b4blockGPU.dFbc;
    dFac = b4blockGPU.dFac;
    dFbd = b4blockGPU.dFbd;

    /*

    //split the int buffer on GPU in multiple arrays
    uint32_t * dj1   = (uint32_t*) b4blockGPU.dIntBuff;
    uint32_t * dj2   = dj1 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx1   = dj2 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx2   = dx1 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx3   = dx2 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx4   = dx3 + 8*DPB*TheBatch.NtilesN;
*/

    /*

    #ifndef __GATHER_CPU__
    cudaStreamSynchronize(**stream); // synchronize for n2XX (unnecessary if on CPU)
    #endif

    // reduce Coulomb-like contributions
    DoReduce (dFab,  dj1,   b4blockGPU.dJuse,   nAtoms, nTiles, offAB, sAB, stream[0], &n2XX[0], gJs.v[GPUdevice], gJs.p[GPUdevice]);
    DoReduce (dFcd,  dj2,   b4blockGPU.dJuse,   nAtoms, nTiles, offCD, sCD, stream[1], &n2XX[1], gJs.v[GPUdevice], gJs.p[GPUdevice]);

    // reduce Exchange-like contributions
    DoReduce (dFac,  dx1,   b4blockGPU.dXuse,   nAtoms, nTiles, offAC, sAC, stream[2], &n2XX[2], gXs.v[GPUdevice], gXs.p[GPUdevice]);
    DoReduce (dFad,  dx2,   b4blockGPU.dXuse,   nAtoms, nTiles, offAD, sAD, stream[3], &n2XX[3], gXs.v[GPUdevice], gXs.p[GPUdevice]);
    DoReduce (dFbc,  dx3,   b4blockGPU.dXuse,   nAtoms, nTiles, offBC, sBC, stream[4], &n2XX[4], gXs.v[GPUdevice], gXs.p[GPUdevice]);
    DoReduce (dFbd,  dx4,   b4blockGPU.dXuse,   nAtoms, nTiles, offBD, sBD, stream[5], &n2XX[5], gXs.v[GPUdevice], gXs.p[GPUdevice]);
    */


    const int * dA = b4blockGPU.dAtA;
    const int * dB = b4blockGPU.dAtB;
    const int * dC = b4blockGPU.dAtC;
    const int * dD = b4blockGPU.dAtD;

    if (TheBatch.ABp->inverted) swap(dA,dB);
    if (TheBatch.CDp->inverted) swap(dC,dD);



    uint32_t nElements = nTiles*DPB;  // the transposition kernel makes sure of zeroing the unused matrices

    //split the int buffer on GPU in multiple arrays
    uint64_t * dj1   = (uint64_t*) b4blockGPU.dIntBuff;
    uint64_t * dj2   = dj1 + nElements;
    uint64_t * dx1   = dj2 + nElements;
    uint64_t * dx2   = dx1 + nElements;
    uint64_t * dx3   = dx2 + nElements;
    uint64_t * dx4   = dx3 + nElements;




    //create the list
    {
        const int nBlocks = (nElements + 1024-1)/1024;
        dim3 nTh2(32,32);

        A2Elist <<<nBlocks,nTh2,0,**stream>>> (dj1, dj2, dA,dB,dC,dD, nAtoms, nElements); // don't set the unused to -1; they will be taken care of by the transposition kernel
        A2Elist <<<nBlocks,nTh2,0,**stream>>> (dx1, dx4, dA,dC,dB,dD, nAtoms, nElements); // don't set the unused to -1; they will be taken care of by the transposition kernel
        A2Elist <<<nBlocks,nTh2,0,**stream>>> (dx2, dx3, dA,dD,dB,dC, nAtoms, nElements); // don't set the unused to -1; they will be taken care of by the transposition kernel

        //sort it (at1 > at2 > e)
        BTsort (dj1, nElements, 6, *stream);
    }


    // reduce Coulomb-like contributions
    SetDoReduce (dFab,  dj1,   dA, dB,  b4blockGPU.dJuse,   nAtoms, nTiles, offAB, sAB, *stream, gJs.v[GPUdevice], gJs.p[GPUdevice]);
    SetDoReduce (dFcd,  dj2,   dC, dD,  b4blockGPU.dJuse,   nAtoms, nTiles, offCD, sCD, *stream, gJs.v[GPUdevice], gJs.p[GPUdevice]);

    // reduce Exchange-like contributions
    SetDoReduce (dFac,  dx1,   dA, dC,  b4blockGPU.dXuse,   nAtoms, nTiles, offAC, sAC, *stream, gXs.v[GPUdevice], gXs.p[GPUdevice]);
    SetDoReduce (dFad,  dx2,   dA, dD,  b4blockGPU.dXuse,   nAtoms, nTiles, offAD, sAD, *stream, gXs.v[GPUdevice], gXs.p[GPUdevice]);
    SetDoReduce (dFbc,  dx3,   dB, dC,  b4blockGPU.dXuse,   nAtoms, nTiles, offBC, sBC, *stream, gXs.v[GPUdevice], gXs.p[GPUdevice]);
    SetDoReduce (dFbd,  dx4,   dB, dD,  b4blockGPU.dXuse,   nAtoms, nTiles, offBD, sBD, *stream, gXs.v[GPUdevice], gXs.p[GPUdevice]);

}


void BatchEvaluator::CopyDJXfromDevice  (const ERIBatch & TheBatch, cudaStream_t * stream, Sparse & Js, Sparse & Xs) {

    const int JA = TheBatch.ABp->Ja;
    const int JB = TheBatch.ABp->Jb;
    const int JC = TheBatch.CDp->Ja;
    const int JD = TheBatch.CDp->Jb;

    const int la = TheBatch.la;
    const int lb = TheBatch.lb;
    const int lc = TheBatch.lc;
    const int ld = TheBatch.ld;

    const int MA = 2*la+1;
    const int MB = 2*lb+1;
    const int MC = 2*lc+1;
    const int MD = 2*ld+1;

    const int sAB = JA*JB*MA*MB;
    const int sCD = JC*JD*MC*MD;

    const int sAC = JA*JC*MA*MC;
    const int sAD = JA*JD*MA*MD;
    const int sBC = JB*JC*MB*MC;
    const int sBD = JB*JD*MB*MD;

    //they are all the same
    uint32_t offAB = TheBatch.TileListN[0].offAB;
    uint32_t offCD = TheBatch.TileListN[0].offCD;
    //they are all the same
    uint32_t offAC = TheBatch.TileListN[0].offAC;
    uint32_t offAD = TheBatch.TileListN[0].offAD;
    uint32_t offBC = TheBatch.TileListN[0].offBC;
    uint32_t offBD = TheBatch.TileListN[0].offBD;


    bool SS = ((TheBatch.geometry==ABAB) && TheBatch.SameShell);
    bool SAB = TheBatch.ABp->samef;
    bool SCD = TheBatch.CDp->samef;


    //copy everything
    cudaMemcpyAsync (b4blockGPU.fab, b4blockGPU.dFab, TheBatch.NtilesN * (sAB+sCD+sAD+sAC+sBC+sBD) * DPB * sizeof(double), cudaMemcpyDeviceToHost, *stream);


    cachelineN<DPB> *Fab, *Fcd;
    Fab = (cachelineN<DPB>*)b4blockGPU.fab;
    Fcd = (cachelineN<DPB>*)b4blockGPU.fcd;

    cachelineN<DPB> *Fad, *Fbc, *Fac, *Fbd;
    Fad = (cachelineN<DPB>*)b4blockGPU.fad;
    Fbc = (cachelineN<DPB>*)b4blockGPU.fbc;
    Fac = (cachelineN<DPB>*)b4blockGPU.fac;
    Fbd = (cachelineN<DPB>*)b4blockGPU.fbd;

    //wait until stream is done transfering before processing on CPU
    cudaStreamSynchronize(*stream);

    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {

        // update fock matrix
        for (uint32_t k=0; k<DPB; ++k) {

            uint32_t ap12 = TheBatch.TileListN[i].ap12[k];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[k];

            uint32_t ta = TheBatch.SP12[ap12].ata;
            uint32_t tb = TheBatch.SP12[ap12].atb;
            uint32_t tc = TheBatch.SP34[ap34].ata;
            uint32_t td = TheBatch.SP34[ap34].atb;

            //coulomb
            if ((TheBatch.TileListN[i].useJ & (1UL<<k)) ) {
                double * dFab = Js(ta,tb) + offAB;
                double * dFcd = Js(tc,td) + offCD;

                for (int mm=0; mm<sAB; ++mm) dFab[mm] += Fab[mm](k);
                for (int mm=0; mm<sCD; ++mm) dFcd[mm] += Fcd[mm](k);
            }

            //exchange
            if ((TheBatch.TileListN[i].useX & (1UL<<k)) ) {
                double * dFac = Xs(ta,tc) + offAC;
                double * dFad = Xs(ta,td) + offAD;
                double * dFbc = Xs(tb,tc) + offBC;
                double * dFbd = Xs(tb,td) + offBD;

                for (int mm=0; mm<sAC; ++mm) dFac[mm] += Fac[mm](k);
                for (int mm=0; mm<sAD; ++mm) dFad[mm] += Fad[mm](k);
                for (int mm=0; mm<sBC; ++mm) dFbc[mm] += Fbc[mm](k);
                for (int mm=0; mm<sBD; ++mm) dFbd[mm] += Fbd[mm](k);
            }

        }

        Fab += sAB;
        Fcd += sCD;

        Fad += sAD;
        Fbc += sBC;
        Fac += sAC;
        Fbd += sBD;
    }

}


/*
                OCL STEPS
                =========
*/

template <int NPB>
void BatchEvaluator::OC      (const ERIBatch & TheBatch, cudaStream_t * stream) {

    bool OnlyJ = TheBatch.J && !TheBatch.X;

    double              * dg8  = b4blockGPU.dG8;
    double              * di8  = b4blockGPU.dI8;
    double              * de8  = b4blockGPU.dE8;

    double              * dab8  = b4blockGPU.dAB8;
    double              * dcd8  = b4blockGPU.dCD8;

    TheBatch.ERIalgorithm->K4 (de8, dg8, dab8, dcd8, TheBatch.TileListN, *TheBatch.ABp, *TheBatch.CDp, di8, TheBatch.NtilesN, stream);
}


template <int NPB>
void BatchEvaluator::L       (const ERIBatch & TheBatch, cudaStream_t * stream) {

    double              * de8  = b4blockGPU.dE8;
    double              * di8  = b4blockGPU.dI8;
    double              * dt8  = b4blockGPU.dT8;

    TheBatch.ERIalgorithm->MIRROR   (di8, de8, dt8, TheBatch.NtilesN, TheBatch.J4, stream);
}


__global__ void MakeRotation4  (double * Es, double * RMs,
                                const int * A, const int * B, const int * C, const int * D,
                                const rAtom * centers,
                                bool invAB, bool invCD,
                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles);

__global__ void MakeRotation3  (double * Es, double * RMs,
                                const int * A,                const int * C, const int * D,
                                const rAtom * centers,
                                            bool invCD,
                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles);

__global__ void MakeRotation2  (double * Es, double * RMs,
                                const int * A,                const int * C,
                                const rAtom * centers,

                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles);

__global__ void MakeRotation2S (double * Es, double * RMs,
                                const int * A, const int * B,
                                const rAtom * centers,
                                bool invAB, bool invCD,
                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles);

// fuse two lists, discarding repeated elements
template<typename T> void FuseSort (      T * f,       uint32_t * cf, uint32_t & nf,
                                    const T * l, const uint32_t * cl, uint32_t   Nl,
                                    const T * r, const uint32_t * cr, uint32_t   Nr) {

    uint32_t nl, nr;
    nf = nl = nr = 0;

    while (nl<Nl && nr<Nr) {
        if      (l[nl]<r[nr]) {
            f  [nf] = l [nl];
            cf [nf] = cl[nl];
            ++nf; ++nl;
        }
        else if (l[nl]>r[nr]) {
            f  [nf] = r [nr];
            cf [nf] = cr[nr];
            ++nf; ++nr;
        }
        else {
            f  [nf] = l[nl];
            cf [nf] = cl[nl] + cr[nr];
            ++nf; ++nl; ++nr;
        }
    }
    while (nl<Nl) {
        f  [nf] = l [nl];
        cf [nf] = cl[nl];
        ++nf; ++nl;
    }
    while (nr<Nr) {
        f  [nf] = r [nr];
        cf [nf] = cr[nr];
        ++nf; ++nr;
    }
}


void BatchEvaluator::CopyAtomstoDevice (const ERIBatch & TheBatch, cudaStream_t * stream, cudaEvent_t * event_CopyAtoms, int nAtoms) {

    // COPY ATOM LISTS TO GPU
    for (uint32_t i=0; i<TheBatch.NtilesN; ++i) {
        for (uint32_t j=0; j<DPB; ++j) {
            uint32_t ap12 = TheBatch.TileListN[i].ap12[j]; const AtomProd & AP12 = (*TheBatch.AP12list)[ap12];
            uint32_t ap34 = TheBatch.TileListN[i].ap34[j]; const AtomProd & AP34 = (*TheBatch.AP34list)[ap34];

            int ij = i*DPB + j;

            b4blockGPU.AtA[ij] = AP12.at1;
            b4blockGPU.AtB[ij] = AP12.at2;
            b4blockGPU.AtC[ij] = AP34.at1;
            b4blockGPU.AtD[ij] = AP34.at2;
        }

        //discards upper bits, which are unused
        b4blockGPU.Juse[i] = TheBatch.TileListN[i].useJ;
        b4blockGPU.Xuse[i] = TheBatch.TileListN[i].useX;
    }

    // COPY ALL DATA TO GPU
    cudaMemcpyAsync (b4blockGPU.dAtA, b4blockGPU.AtA, TheBatch.NtilesN * (DPB * 4 + 2) * sizeof(int), cudaMemcpyHostToDevice, *stream);

    #ifndef __GATHER_CPU__
    cudaEventRecord (*event_CopyAtoms, *stream);
    #endif
}


// simple associative array for use within
// CPU solution to the gather problem
struct AArray {
    uint32_t * k;
    uint32_t * v;
    int N;

    AArray() {
        N = 0;
        k = NULL;
        v = NULL;
    }

    void Set(uint32_t * kk, uint32_t * vv) {
         k=kk;
         v=vv;
    }

    uint32_t size() const {
        return N;
    }

    //insert the element in position pos, displacing the current element and all to its right one position
    void insert (uint32_t key, int pos) {

        for (int i=N-1; i>=pos; --i) {
            k[i+1] = k[i];
            v[i+1] = v[i];
        }
        k[pos] = key;
        v[pos] = 0;
        ++N;
    }

    uint32_t & operator[](uint32_t key) {

        if (N==0 || key>k[N-1]) {insert(key, N); return v[N-1];} // key larger than list; add as last element

        int imin = 0;
        int imax = N-1;

        while(imin<imax) {
            int imed = (imin+imax)/2;

            if   (key>k[imed]) imin = imed+1;
            else               imax = imed;
        }

        if (key!=k[imin]) insert(key, imin);

        return v[imin];
    }
};



// fuse two lists, discarding repeated elements
template<typename T> void FuseMerge (     T * f,       uint32_t * cf, uint32_t & nf,
                                    const T * l, const uint32_t * cl, uint32_t   Nl,
                                    const T * r, const uint32_t * cr, uint32_t   Nr) {

    uint32_t nl, nr;
    nf = nl = nr = 0;

    while (nl<Nl && nr<Nr) {
        if      (l[nl]<r[nr]) {
            f  [nf] = l [nl];
            cf [nf] = cl[nl];
            ++nf; ++nl;
        }
        else if (l[nl]>r[nr]) {
            f  [nf] = r [nr];
            cf [nf] = cr[nr];
            ++nf; ++nr;
        }
        else {
            f  [nf] = l[nl];
            cf [nf] = cl[nl] + cr[nr];
            ++nf; ++nl; ++nr;
        }
    }
    while (nl<Nl) {
        f  [nf] = l [nl];
        cf [nf] = cl[nl];
        ++nf; ++nl;
    }
    while (nr<Nr) {
        f  [nf] = r [nr];
        cf [nf] = cr[nr];
        ++nf; ++nr;
    }
}

// strictly speaking, this is the number of used bits
static inline int hbit (int m) {
    int lg=0;
    for (int n=m-1; n>0; n/=2) ++lg;
    return lg;
}

static inline void Switch(uint32_t & a,uint32_t & b) {
    register uint32_t aa = a;
    register uint32_t bb = b;

    if (bb>=aa) {
        a = aa;
        b = bb;
    }
    else {
        b = aa;
        a = bb;
    }
}

// Pairwise Sorting Network for 16 elements
void PSN16 (uint32_t * p) {

    Switch (p[ 0],p[ 1]);
    Switch (p[ 2],p[ 3]);
    Switch (p[ 4],p[ 5]);
    Switch (p[ 6],p[ 7]);
    Switch (p[ 8],p[ 9]);
    Switch (p[10],p[11]);
    Switch (p[12],p[13]);
    Switch (p[14],p[15]);


    Switch (p[ 0],p[ 3]);
    Switch (p[ 1],p[ 2]);
    Switch (p[ 4],p[ 7]);
    Switch (p[ 5],p[ 6]);
    Switch (p[ 8],p[11]);
    Switch (p[ 9],p[10]);
    Switch (p[12],p[15]);
    Switch (p[13],p[14]);

    Switch (p[ 0],p[ 1]);
    Switch (p[ 2],p[ 3]);
    Switch (p[ 4],p[ 5]);
    Switch (p[ 6],p[ 7]);
    Switch (p[ 8],p[ 9]);
    Switch (p[10],p[11]);
    Switch (p[12],p[13]);
    Switch (p[14],p[15]);


    Switch (p[ 0],p[ 7]);
    Switch (p[ 1],p[ 6]);
    Switch (p[ 2],p[ 5]);
    Switch (p[ 3],p[ 4]);
    Switch (p[ 8],p[15]);
    Switch (p[ 9],p[14]);
    Switch (p[10],p[13]);
    Switch (p[11],p[12]);

    Switch (p[ 0],p[ 2]);
    Switch (p[ 1],p[ 3]);
    Switch (p[ 4],p[ 6]);
    Switch (p[ 5],p[ 7]);
    Switch (p[ 8],p[10]);
    Switch (p[ 9],p[11]);
    Switch (p[12],p[14]);
    Switch (p[13],p[15]);

    Switch (p[ 0],p[ 1]);
    Switch (p[ 2],p[ 3]);
    Switch (p[ 4],p[ 5]);
    Switch (p[ 6],p[ 7]);
    Switch (p[ 8],p[ 9]);
    Switch (p[10],p[11]);
    Switch (p[12],p[13]);
    Switch (p[14],p[15]);


    Switch (p[ 0],p[15]);
    Switch (p[ 1],p[14]);
    Switch (p[ 2],p[13]);
    Switch (p[ 3],p[12]);
    Switch (p[ 4],p[11]);
    Switch (p[ 5],p[10]);
    Switch (p[ 6],p[ 9]);
    Switch (p[ 7],p[ 8]);

    Switch (p[ 0],p[ 4]);
    Switch (p[ 1],p[ 5]);
    Switch (p[ 2],p[ 6]);
    Switch (p[ 3],p[ 7]);
    Switch (p[ 8],p[12]);
    Switch (p[ 9],p[13]);
    Switch (p[10],p[14]);
    Switch (p[11],p[15]);

    Switch (p[ 0],p[ 2]);
    Switch (p[ 1],p[ 3]);
    Switch (p[ 4],p[ 6]);
    Switch (p[ 5],p[ 7]);
    Switch (p[ 8],p[10]);
    Switch (p[ 9],p[11]);
    Switch (p[12],p[14]);
    Switch (p[13],p[15]);

    Switch (p[ 0],p[ 1]);
    Switch (p[ 2],p[ 3]);
    Switch (p[ 4],p[ 5]);
    Switch (p[ 6],p[ 7]);
    Switch (p[ 8],p[ 9]);
    Switch (p[10],p[11]);
    Switch (p[12],p[13]);
    Switch (p[14],p[15]);
}

//compact 16 elements
void Compact16 (uint32_t * k, uint32_t * c, uint32_t & n) {

    uint32_t N = n;

    n = 0;
    c[0] = 1;

    for (int i=1; i<N; ++i) {
        if (k[i]==k[n]) ++c[n];
        else {
            ++n;
            k[n] = k[i];
            c[n] = 1;
        }
    }
    ++n;
}

struct IParray {

    // pointers to stuff
    uint32_t * ptk[32];
    uint32_t * ptc[32];
    uint32_t   ns [32]; // number of elements in sublist
    uint8_t    nmx[32]; // maximum   elements in sublist

    //pointer to array
    uint32_t * k;
    uint32_t * c;

    //auxilliary
    uint32_t * kx;
    uint32_t * cx;

    // last subarray being used
    uint8_t    last;

    uint32_t N;



    uint32_t size() const {
        return N;
    }

    uint32_t & operator[](uint32_t key) {

        int i=0;

        //#pragma omp critical
        {
            for (int l=last; l>0; --l) {
                while (key>ptk[l][i]) {
                    ++i;
                    /*
                    if (i>=ns[l]) {
                        cout << "error: " << key << endl;
                        cout << l << " " << i << " " << ns[l] << endl;

                        for (int l=0; l<=last; ++l) {
                            cout << l << "   ";
                            for (int i=0; i<ns[l]; ++i) cout << ptk[l][i] << " "; cout << endl;
                        }
                        char a; cin >> a;
                    }
                    */
                }

                i *= 16;
            }
            while (key>k[i]) ++i;

        }
        return c[i];


        /*
        if (N==0 || key>k[N-1]) {cout << "error " << " " << key << " " << N << endl;} // error!!

        int imin = 0;
        int imax = N-1;

        while(imin<imax) {
            int imed = (imin+imax)/2;

            if   (key>k[imed]) imin = imed+1;
            else               imax = imed;
        }

        if (key!=k[imin]) {cout << "error " << " " << key << endl;} // error!!

        return c[imin];
        */

    }



    IParray() {
        k = c = kx = cx = NULL;
        for (int i=0; i<32; ++i) ptk[i] = NULL;
        for (int i=0; i<32; ++i) ptc[i] = NULL;
        for (int i=0; i<32; ++i) ns [i] = 0;
        for (int i=0; i<32; ++i) nmx[i] = 0;
        last = 0;
        ptk[0] = k;
        ptc[0] = c;
        N = 0;
    }

    //merge/fuse with another tile
    void Fuse (bool balance=false) {

        // add last tile
        if (!balance) {
            if (ns[last]>0) {
                uint32_t & nn = ns[last];

                for (int i=nn; i<16; ++i) ptk[last][i] = -1;

                // sort (should be fast, as it only takes 2 cachelines at most)
                PSN16 (ptk[last]);

                // compact the array (also counts number of repetitions)
                Compact16 (ptk[last], ptc[last], ns[last]);
                nmx[last] = 1;

                ++last;
            }
        }

        while (last>1) {
            if (balance) if (nmx[last-2]!=nmx[last-1]) break; //incompatible sizes

            --last;

            uint32_t nn = ns[last-1];

            // copy all this out
            // (try to avoid copying this)
            for (int i=0; i<nn; ++i) {
                kx[i] = ptk[last-1][i];
                cx[i] = ptc[last-1][i];
            }

            FuseMerge (      ptk[last-1], ptc[last-1], ns[last-1],
                             kx,          cx,          nn,
                             ptk[last  ], ptc[last  ], ns[last  ]);

            uint8_t nnn = nmx[last-1];

            nmx[last-1] = hbit((ns[last-1]+15)/16)+1; // count number of blocks of 16

            // clean last tile
            ns [last] = 0;
            nmx[last] = 0;
        }

        N = ns[0]; // number of elements of last tile
    }

    void Fix () {

        uint32_t M = N;

        ptk[0] = k;
        ns [0] = M;
        ptk[1] = kx;

        last   = 0;

        while (M>16) {
            for (int i=0; i+16<M; i+=16) ptk[last+1][i/16] = ptk[last][i+15];
            ptk[last+1][(M+15)/16 - 1] = ptk[last][M-1];

            ns [last+1] = M = (M+15)/16;

            ++last;
            ptk[last+1] = ptk[last] + M;
        }
    }

    // add an element
    void Add (uint32_t rhs) {
        // add an element
        ptk [last][ns[last]] = rhs;
        ++ns[last];

        // if enough elements have been added
        if (ns[last]==16) {
            // sort (should be fast, as it only takes 2 cachelines at most)
            PSN16 (ptk[last]);

            // compact the array (also counts number of repetitions)
            Compact16 (ptk[last], ptc[last], ns[last]);
            nmx[last] = 1;

            ++last;

            // fuse elements, if properly balanced
            Fuse(true);

            // set pointers for next element
            ptk [last] = ptk [last-1] + ns[last-1];
            ptc [last] = ptc [last-1] + ns[last-1];
            ns  [last] = 0;
            nmx [last] = 0;
        }
    }

    void Set(uint32_t * pk, uint32_t * pc, uint32_t *paux, int NMAX) {

        k  = pk;
        c  = pc;
        kx = paux;
        cx = paux + NMAX;

        ptk[0] = k;
        ptc[0] = c;
    }
};


uint32_t InitInteractions ( AArray & mapIJ, uint32_t * pij, uint32_t nTiles) {

    uint64_t * a2p   = (uint64_t*)pij;
    uint32_t * e    = pij + 2*DPB*nTiles;
    uint32_t * c    = pij + 3*DPB*nTiles;
    uint32_t * p    = pij + 4*DPB*nTiles;
    uint32_t * a2r  = pij + 5*DPB*nTiles;
    uint32_t * na2  = pij + 6*DPB*nTiles;

    *na2 = mapIJ.size();
    int pe  = 0;

    for (int ia2=0; ia2<*na2; ++ia2) {
        p[ia2] = pe;
        //increment iterators
        pe += c[ia2];
    }

    // substitute count for pointer
    mapIJ.Set (a2r,pij);
    for (int ia2=0; ia2<*na2; ++ia2)
        mapIJ.v[ia2] = p[ia2];

    return *na2;
}

uint32_t InitInteractions ( IParray & mapIJ, uint32_t * pij, uint32_t nTiles) {

    uint64_t * a2p   = (uint64_t*)pij;
    uint32_t * e    = pij + 2*DPB*nTiles;
    uint32_t * c    = pij + 3*DPB*nTiles;
    uint32_t * p    = pij + 4*DPB*nTiles;
    uint32_t * a2r  = pij + 5*DPB*nTiles;
    uint32_t * na2  = pij + 6*DPB*nTiles;

     // fuse remaining subarrays
    mapIJ.Fuse();
    *na2 = mapIJ.size();

    int pe  = 0;
    for (int ia2=0; ia2<*na2; ++ia2) {
        p[ia2] = pe;
        //increment iterators
        pe += c[ia2];
    }

    // substitute count for pointer
    mapIJ.Set (a2r, pij, pij+DPB*nTiles, *na2);

    mapIJ.Fix();

    for (int ia2=0; ia2<*na2; ++ia2)
        mapIJ.c[ia2] = p[ia2];

    return *na2;
}



#include <algorithm>    // std::sort
#include <vector>       // std::vector


void BatchEvaluator::SetReduces        (const ERIBatch & TheBatch, cudaStream_t ** stream, cudaEvent_t * event_CopyAtoms, uint32_t * n2XX, int nAtoms) {

  #ifndef __GATHER_CPU__
  {

    cudaStreamWaitEvent (*stream[0], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[1], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[2], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[3], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[4], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[5], *event_CopyAtoms, 0);

    // prepare stuff necessary for reduction
    uint32_t * dj1   = (uint32_t*) b4blockGPU.dIntBuff;
    uint32_t * dj2   = dj1 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx1   = dj2 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx2   = dx1 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx3   = dx2 + 8*DPB*TheBatch.NtilesN;
    uint32_t * dx4   = dx3 + 8*DPB*TheBatch.NtilesN;

    int nTiles = TheBatch.NtilesN;

    const int * dA = b4blockGPU.dAtA;
    const int * dB = b4blockGPU.dAtB;
    const int * dC = b4blockGPU.dAtC;
    const int * dD = b4blockGPU.dAtD;

    if (TheBatch.ABp->inverted) swap(dA,dB);
    if (TheBatch.CDp->inverted) swap(dC,dD);

    cudaStreamWaitEvent (*stream[0], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[1], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[2], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[3], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[4], *event_CopyAtoms, 0);
    //cudaStreamWaitEvent (*stream[5], *event_CopyAtoms, 0);

    SetReduce (dj1, dA, dB,  nAtoms, nTiles, stream[0],  &n2XX[0]);
    SetReduce (dj2, dC, dD,  nAtoms, nTiles, stream[1],  &n2XX[1]);

    SetReduce (dx1, dA, dC,  nAtoms, nTiles, stream[2],  &n2XX[2]);
    SetReduce (dx2, dA, dD,  nAtoms, nTiles, stream[3],  &n2XX[3]);
    SetReduce (dx3, dB, dC,  nAtoms, nTiles, stream[4],  &n2XX[4]);
    SetReduce (dx4, dB, dD,  nAtoms, nTiles, stream[5],  &n2XX[5]);
  }

  #else
  if (0) {

    // this adds 18 seconds to nylon_gpu, so find a more efficient way (34 s)
    // changing set for list (which given the ordering is equivalent) reduces the difference to 10 seconds (26 s)
    // making a double pass to reserve space for elements and using a simple map<int,int> further reduces the difference to 5 seconds (19s)
    // first attempt to use hand-coded structure reduces about .5 seconds (18.5 s)


    //make lists
    const int * A = b4blockGPU.AtA;
    const int * B = b4blockGPU.AtB;
    const int * C = b4blockGPU.AtC;
    const int * D = b4blockGPU.AtD;

    if (TheBatch.ABp->inverted) swap(A,B);
    if (TheBatch.CDp->inverted) swap(C,D);

    uint32_t nTiles = TheBatch.NtilesN;

    // prepare stuff necessary for reduction
    uint32_t * j1   = (uint32_t*) b4blockGPU.IntBuff;
    uint32_t * j2   = j1 + 8*DPB*nTiles;
    uint32_t * x1   = j2 + 8*DPB*nTiles;
    uint32_t * x2   = x1 + 8*DPB*nTiles;
    uint32_t * x3   = x2 + 8*DPB*nTiles;
    uint32_t * x4   = x3 + 8*DPB*nTiles;


    //AArray mapAB, mapCD, mapAC, mapAD, mapBC, mapBD;

    //mapAB.Set ( j1+5*DPB*nTiles, j1+3*DPB*nTiles );
    //mapCD.Set ( j2+5*DPB*nTiles, j2+3*DPB*nTiles );
    //mapAC.Set ( x1+5*DPB*nTiles, x1+3*DPB*nTiles );
    //mapAD.Set ( x2+5*DPB*nTiles, x2+3*DPB*nTiles );
    //mapBC.Set ( x3+5*DPB*nTiles, x3+3*DPB*nTiles );
    //mapBD.Set ( x4+5*DPB*nTiles, x4+3*DPB*nTiles );


    IParray mapAB, mapCD, mapAC, mapAD, mapBC, mapBD;

    mapAB.Set ( j1+5*DPB*nTiles, j1+3*DPB*nTiles, j1, DPB*nTiles );
    mapCD.Set ( j2+5*DPB*nTiles, j2+3*DPB*nTiles, j2, DPB*nTiles );
    mapAC.Set ( x1+5*DPB*nTiles, x1+3*DPB*nTiles, x1, DPB*nTiles );
    mapAD.Set ( x2+5*DPB*nTiles, x2+3*DPB*nTiles, x2, DPB*nTiles );
    mapBC.Set ( x3+5*DPB*nTiles, x3+3*DPB*nTiles, x3, DPB*nTiles );
    mapBD.Set ( x4+5*DPB*nTiles, x4+3*DPB*nTiles, x4, DPB*nTiles );



    for (uint32_t i=0; i<nTiles; ++i) {
        for (uint32_t j=0; j<DPB; ++j) {

            uint32_t n = i*DPB + j;

            if (b4blockGPU.Juse[i] & (1<<j)) {
                uint32_t AB = A[n] * nAtoms + B[n];
                uint32_t CD = C[n] * nAtoms + D[n];

                //++mapAB[AB];
                //++mapCD[CD];
                mapAB.Add(AB);
                mapCD.Add(CD);
            }

            if (b4blockGPU.Xuse[i] & (1<<j)) {
                uint32_t AC = A[n] * nAtoms + C[n];
                uint32_t AD = A[n] * nAtoms + D[n];

                uint32_t BC = B[n] * nAtoms + C[n];
                uint32_t BD = B[n] * nAtoms + D[n];

                //++mapAC[AC];
                //++mapAD[AD];
                //++mapBC[BC];
                //++mapBD[BD];
                mapAC.Add(AC);
                mapAD.Add(AD);
                mapBC.Add(BC);
                mapBD.Add(BD);
            }
        }
    }

    n2XX[0] = InitInteractions (mapAB, j1, nTiles);
    n2XX[1] = InitInteractions (mapCD, j2, nTiles);
    n2XX[2] = InitInteractions (mapAC, x1, nTiles);
    n2XX[3] = InitInteractions (mapAD, x2, nTiles);
    n2XX[4] = InitInteractions (mapBC, x3, nTiles);
    n2XX[5] = InitInteractions (mapBD, x4, nTiles);

    // count a second time, filling the e's
    uint32_t * eab    = j1 + 2*DPB*nTiles;
    uint32_t * ecd    = j2 + 2*DPB*nTiles;

    uint32_t * eac    = x1 + 2*DPB*nTiles;
    uint32_t * ead    = x2 + 2*DPB*nTiles;
    uint32_t * ebc    = x3 + 2*DPB*nTiles;
    uint32_t * ebd    = x4 + 2*DPB*nTiles;


    for (uint32_t i=0; i<nTiles; ++i) {
        for (uint32_t j=0; j<DPB; ++j) {

            uint32_t n = i*DPB + j;

            if (b4blockGPU.Juse[i] & (1<<j)) {
                uint32_t AB = A[n] * nAtoms + B[n];
                uint32_t CD = C[n] * nAtoms + D[n];

                eab[mapAB[AB]++] = n;
                ecd[mapCD[CD]++] = n;
            }

            if (b4blockGPU.Xuse[i] & (1<<j)) {
                uint32_t AC = A[n] * nAtoms + C[n];
                uint32_t AD = A[n] * nAtoms + D[n];
                uint32_t BC = B[n] * nAtoms + C[n];
                uint32_t BD = B[n] * nAtoms + D[n];

                eac[mapAC[AC]++] = n;
                ead[mapAD[AD]++] = n;
                ebc[mapBC[BC]++] = n;
                ebd[mapBD[BD]++] = n;
            }
        }
    }

    cudaMemcpyAsync (b4blockGPU.dIntBuff, b4blockGPU.IntBuff, nTiles * (48*DPB) * sizeof(int), cudaMemcpyHostToDevice, **stream);
  }
  else {

    //make lists
    const int * A = b4blockGPU.AtA;
    const int * B = b4blockGPU.AtB;
    const int * C = b4blockGPU.AtC;
    const int * D = b4blockGPU.AtD;

    if (TheBatch.ABp->inverted) swap(A,B);
    if (TheBatch.CDp->inverted) swap(C,D);


    uint32_t nTiles = TheBatch.NtilesN;

    // prepare stuff necessary for reduction
    uint32_t * j1   = (uint32_t*) b4blockGPU.IntBuff;
    uint32_t * j2   = j1 + 8*DPB*nTiles;
    uint32_t * x1   = j2 + 8*DPB*nTiles;
    uint32_t * x2   = x1 + 8*DPB*nTiles;
    uint32_t * x3   = x2 + 8*DPB*nTiles;
    uint32_t * x4   = x3 + 8*DPB*nTiles;




    for (uint32_t i=0; i<nTiles; ++i) {
        for (uint32_t j=0; j<DPB; ++j) {

            uint32_t n = i*DPB + j;

            if (b4blockGPU.Juse[i] & (1<<j)) {
                uint32_t AB = A[n] * nAtoms + B[n];
                uint32_t CD = C[n] * nAtoms + D[n];

                //++mapAB[AB];
                //++mapCD[CD];
                mapAB.Add(AB);
                mapCD.Add(CD);
            }

            if (b4blockGPU.Xuse[i] & (1<<j)) {
                uint32_t AC = A[n] * nAtoms + C[n];
                uint32_t AD = A[n] * nAtoms + D[n];

                uint32_t BC = B[n] * nAtoms + C[n];
                uint32_t BD = B[n] * nAtoms + D[n];

                //++mapAC[AC];
                //++mapAD[AD];
                //++mapBC[BC];
                //++mapBD[BD];
                mapAC.Add(AC);
                mapAD.Add(AD);
                mapBC.Add(BC);
                mapBD.Add(BD);
            }
        }
    }

    n2XX[0] = InitInteractions (mapAB, j1, nTiles);
    n2XX[1] = InitInteractions (mapCD, j2, nTiles);
    n2XX[2] = InitInteractions (mapAC, x1, nTiles);
    n2XX[3] = InitInteractions (mapAD, x2, nTiles);
    n2XX[4] = InitInteractions (mapBC, x3, nTiles);
    n2XX[5] = InitInteractions (mapBD, x4, nTiles);

    // count a second time, filling the e's
    uint32_t * eab    = j1 + 2*DPB*nTiles;
    uint32_t * ecd    = j2 + 2*DPB*nTiles;

    uint32_t * eac    = x1 + 2*DPB*nTiles;
    uint32_t * ead    = x2 + 2*DPB*nTiles;
    uint32_t * ebc    = x3 + 2*DPB*nTiles;
    uint32_t * ebd    = x4 + 2*DPB*nTiles;


    for (uint32_t i=0; i<nTiles; ++i) {
        for (uint32_t j=0; j<DPB; ++j) {

            uint32_t n = i*DPB + j;

            if (b4blockGPU.Juse[i] & (1<<j)) {
                uint32_t AB = A[n] * nAtoms + B[n];
                uint32_t CD = C[n] * nAtoms + D[n];

                eab[mapAB[AB]++] = n;
                ecd[mapCD[CD]++] = n;
            }

            if (b4blockGPU.Xuse[i] & (1<<j)) {
                uint32_t AC = A[n] * nAtoms + C[n];
                uint32_t AD = A[n] * nAtoms + D[n];
                uint32_t BC = B[n] * nAtoms + C[n];
                uint32_t BD = B[n] * nAtoms + D[n];

                eac[mapAC[AC]++] = n;
                ead[mapAD[AD]++] = n;
                ebc[mapBC[BC]++] = n;
                ebd[mapBD[BD]++] = n;
            }
        }
    }

    cudaMemcpyAsync (b4blockGPU.dIntBuff, b4blockGPU.IntBuff, nTiles * (48*DPB) * sizeof(int), cudaMemcpyHostToDevice, **stream);
  }

  #endif

}

void BatchEvaluator::LPRM_ABCD (const ERIBatch & TheBatch, cudaStream_t * stream) {

    bool hasP = ( (TheBatch.la==1)||(TheBatch.lb==1)||(TheBatch.lc==1)||(TheBatch.ld==1) );
    bool hasD = ( (TheBatch.la==2)||(TheBatch.lb==2)||(TheBatch.lc==2)||(TheBatch.ld==2) );
    bool hasF = ( (TheBatch.la==3)||(TheBatch.lb==3)||(TheBatch.lc==3)||(TheBatch.ld==3) );
    bool hasG = ( (TheBatch.la==4)||(TheBatch.lb==4)||(TheBatch.lc==4)||(TheBatch.ld==4) );

    int Ntiles = TheBatch.NtilesN;
    const int NNY = 4;

    int NYtiles = (Ntiles+NNY-1)/NNY;

    dim3 Nbl (  1, NYtiles);
    dim3 Nth (NTX, NNY);

    MakeRotation4 <<<Nbl, Nth, 0, *stream>>>
                             ( (double*)b4blockGPU.dE8, (double*)b4blockGPU.dRMS8,
                                b4blockGPU.dAtA, b4blockGPU.dAtB, b4blockGPU.dAtC, b4blockGPU.dAtD,
                                AtomListGpus,
                                //false, false,
                                TheBatch.ABp->inverted, TheBatch.CDp->inverted,
                                hasP, hasD, hasF, hasG,
                                Ntiles);
}

void BatchEvaluator::LPRM_AACD (const ERIBatch & TheBatch, cudaStream_t * stream) {

    bool hasP = ( (TheBatch.la==1)||(TheBatch.lb==1)||(TheBatch.lc==1)||(TheBatch.ld==1) );
    bool hasD = ( (TheBatch.la==2)||(TheBatch.lb==2)||(TheBatch.lc==2)||(TheBatch.ld==2) );
    bool hasF = ( (TheBatch.la==3)||(TheBatch.lb==3)||(TheBatch.lc==3)||(TheBatch.ld==3) );
    bool hasG = ( (TheBatch.la==4)||(TheBatch.lb==4)||(TheBatch.lc==4)||(TheBatch.ld==4) );

    int Ntiles = TheBatch.NtilesN;
    const int NNY = 4;

    int NYtiles = (Ntiles+NNY-1)/NNY;

    dim3 Nbl (  1, NYtiles);
    dim3 Nth (NTX, NNY);

    MakeRotation3 <<<Nbl, Nth, 0, *stream>>>
                             ( (double*)b4blockGPU.dE8, (double*)b4blockGPU.dRMS8,
                                b4blockGPU.dAtA,              b4blockGPU.dAtC, b4blockGPU.dAtD,
                                AtomListGpus,
                                //false,
                                                        TheBatch.CDp->inverted,
                                hasP, hasD, hasF, hasG,
                                Ntiles);
}

void BatchEvaluator::LPRM_AACC (const ERIBatch & TheBatch, cudaStream_t * stream) {

    bool hasP = ( (TheBatch.la==1)||(TheBatch.lb==1)||(TheBatch.lc==1)||(TheBatch.ld==1) );
    bool hasD = ( (TheBatch.la==2)||(TheBatch.lb==2)||(TheBatch.lc==2)||(TheBatch.ld==2) );
    bool hasF = ( (TheBatch.la==3)||(TheBatch.lb==3)||(TheBatch.lc==3)||(TheBatch.ld==3) );
    bool hasG = ( (TheBatch.la==4)||(TheBatch.lb==4)||(TheBatch.lc==4)||(TheBatch.ld==4) );

    int Ntiles = TheBatch.NtilesN;
    const int NNY = 4;

    int NYtiles = (Ntiles+NNY-1)/NNY;

    dim3 Nbl (  1, NYtiles);
    dim3 Nth (NTX, NNY);

    MakeRotation2 <<<Nbl, Nth, 0, *stream>>>
                             ( (double*)b4blockGPU.dE8, (double*)b4blockGPU.dRMS8,
                                b4blockGPU.dAtA,              b4blockGPU.dAtC,
                                AtomListGpus,
                                hasP, hasD, hasF, hasG,
                                Ntiles);
}

void BatchEvaluator::LPRM_ABAB (const ERIBatch & TheBatch, cudaStream_t * stream) {

    bool hasP = ( (TheBatch.la==1)||(TheBatch.lb==1)||(TheBatch.lc==1)||(TheBatch.ld==1) );
    bool hasD = ( (TheBatch.la==2)||(TheBatch.lb==2)||(TheBatch.lc==2)||(TheBatch.ld==2) );
    bool hasF = ( (TheBatch.la==3)||(TheBatch.lb==3)||(TheBatch.lc==3)||(TheBatch.ld==3) );
    bool hasG = ( (TheBatch.la==4)||(TheBatch.lb==4)||(TheBatch.lc==4)||(TheBatch.ld==4) );

    int Ntiles = TheBatch.NtilesN;
    const int NNY = 4;

    int NYtiles = (Ntiles+NNY-1)/NNY;

    dim3 Nbl (  1, NYtiles);
    dim3 Nth (NTX, NNY);

    MakeRotation2S<<<Nbl, Nth, 0, *stream>>>
                             ( (double*)b4blockGPU.dE8, (double*)b4blockGPU.dRMS8,
                                b4blockGPU.dAtA, b4blockGPU.dAtB,
                                AtomListGpus,
                                TheBatch.ABp->inverted, TheBatch.CDp->inverted,
                                //false, false,
                                hasP, hasD, hasF, hasG,
                                Ntiles);
}


double BatchEvaluator::GetTime (int i) {
    return Chronos[i].GetTotalTime();
}

bool BatchEvaluator::IsFree() const {
    return ((cudaStreamQuery(GPUstream[0])==cudaSuccess) && (cudaStreamQuery(GPUstream[2])==cudaSuccess));
}

void BatchEvaluator::Evaluate  (const ERIBatch & TheBatch, const Sparse * Ds, const Sparse * Da, SparseGPU * gDs, SparseGPU * gDa, bool * UseA, int nmat)  {

    //set the proper device for all the following kernels and transactions
    cudaSetDevice(GPUdevice);

    GEOM geometry = TheBatch.geometry;

    bool J = TheBatch.J;
    bool X = TheBatch.X;

    const int nAtoms = Jgpu[0].len; // how come this information is not available also somewhere else?

    b4blockGPU.SetBuffersN(TheBatch, b4ERIs);

    cudaStream_t * streams[6];
    for (int n=0; n<6; ++n) streams[n] = &GPUstream[2];
    cudaEvent_t & event_CopyAtoms  = event_set[0];
    cudaEvent_t & event_ERIs_ready = event_set[1];

    //don't copy primitive product weights at the moment
    //CopyWtoDevice (TheBatch, GPUstream);

    uint32_t n2XX[6]; // contains the number of atoms pairs used on every kind of contraction

    //synchronize previous streams before starting
    for (int s=0; s<NSTREAMS; ++s) cudaStreamSynchronize(GPUstream[s]);

    cudaError_t cudaResult;
    cudaResult = cudaGetLastError();
    if (cudaResult != cudaSuccess) {
        cout << endl<< "Some error ocurred in device ERI evaluation:  " << cudaGetErrorString(cudaResult) << endl;
    }

/*
    #pragma omp critical
    {
        cout << omp_get_thread_num() << "   " << int(geometry) << "    " << int(TheBatch.la) << " " << int(TheBatch.lb) << " " << int(TheBatch.lc) << " " << int(TheBatch.ld) << "    " << TheBatch.ABp << " " << TheBatch.CDp << "  " << TheBatch.NtilesN << endl;
        for (int i=0; i<7; ++i) cout << Chronos[i].GetLastTime() << "   "; cout << endl;
    }
*/

    // copy the tiles' atoms
    Chronos[0].Start (GPUstream);
    CopyAtomstoDevice (TheBatch, GPUstream,     &event_CopyAtoms, nAtoms);
    Chronos[0].Stop  (GPUstream);

    //make the ERI geometries and rotations
    Chronos[1].Start (GPUstream);
    if      (geometry==ABCD) LPRM_ABCD (TheBatch, GPUstream);
    else if (geometry==ABAB) LPRM_ABAB (TheBatch, GPUstream);
    else if (geometry==AACD) LPRM_AACD (TheBatch, GPUstream);
    else if (geometry==AACC) LPRM_AACC (TheBatch, GPUstream);
    Chronos[1].Stop  (GPUstream);

    //OC step: K4 contractions
    Chronos[2].Start (GPUstream);
    OC <DPB> (TheBatch, GPUstream);
    Chronos[2].Stop  (GPUstream);

    //L step: MIRROR transformations
    Chronos[3].Start (GPUstream);
    L  <DPB> (TheBatch, GPUstream);
    Chronos[3].Stop  (GPUstream);


    // init the reduction lists
    SetReduces        (TheBatch, streams,       &event_CopyAtoms, n2XX, nAtoms);
    //ERIs are ready!
    cudaEventRecord (event_ERIs_ready, GPUstream[0]);

    //D step: ERI Digestion with the DM
    //if (J && X)
    for (int n=0;n<nmat;++n) {

        //if (n==0) cudaStreamWaitEvent  (*streams[0], event_ERIs_ready, 0);
        //FullDJXcontraction (TheBatch, streams,   gDs[n], Jgpu[n], Xgpu[n]);

        Chronos[4].Start (*streams);
        CopyDJXtoDevice      (TheBatch, streams, gDs[n]);
        Chronos[4].Stop  (*streams);

        // wait for ERIs to be ready
        if (n==0) cudaStreamWaitEvent  (*streams[0], event_ERIs_ready, 0);

        Chronos[5].Start (*streams);
        ContractDJXs         (TheBatch, streams);
        Chronos[5].Stop  (*streams);

        Chronos[6].Start (*streams);
        CopyDJXfromDevice    (TheBatch, streams, n2XX, Jgpu[n], Xgpu[n]); //CopyDJXfromDevice (TheBatch, &GPUstream[1], Jresp[n], Xresp[n]);
        Chronos[6].Stop  (*streams);
    }

    //no A whatsoever
    /*
    for (int n=0; n<nmat; ++n) {
        CopyDXtoDevice(TheBatch, Da[n]);
        ContractDXs (TheBatch);
        CopyDXfromDevice (TheBatch, Aresp[n]);
    }
    */

};


