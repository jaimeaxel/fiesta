


#ifndef __BATCH_EVALUATOR__
#define __BATCH_EVALUATOR__

#include <map>
#include "BatchInfo.hpp"
#include "low/chrono.hpp"

#include "libquimera/libquimera.hpp"

#ifdef _OPENMP
    #include <omp.h>
#endif

template <class T> class r2tensor;
template <class T> class r1tensor;
class tensor4;
class tensor3;
class tensor2;

class sparsetensorpattern;
class Sparse;
class SparseGPU;

class ERIgeometry;
class ERIgeometries64;
class ERIgeometries32;
class cacheline64;
class cacheline32;
class RotationMatrices64;
class RotationMatrices32;

class BatchEvaluator;
class BlockBuffer;
class ERIBatch;
class ERIblock;
class Fock2e;

class ShellPair;
class rAtom;

class StreamChrono;

namespace LibQuimera {
    class ERITile64;
    class ERITile32;
    template<int N> class ERITile;
    class ERIbuffer;
}


// fool the c++ compiler so that the class is not undefined,
// but avoid name collisions with nvcc
#ifndef __CUDACC__
class cudaStream_t;
class cudaEvent_t;
#endif

//a memory buffer for the CPU batch evaluations
class BlockBufferCPU {

  public:
    // device memory
    // allow only one instance
    static size_t    TotalMaxBlockMem;
    static size_t    LocalMaxBlockMem;
    static uint8_t * TotalBlockMem;

    static void      InitMem    (size_t MEM, BatchEvaluator * BE, int Nprox);  // total memory initialization
    static void      FreeMem    ();

    static uint64_t  MaxTiles32 (const BatchInfo & TheBatch, int NMs=1);
    static uint64_t  MaxTiles64 (const BatchInfo & TheBatch, int NMs=1);

  public:

    // host memory
    size_t MaxBlockMem;
    uint8_t * BlockMem;


    ERIgeometry   * E0d;
    ERIgeometry   * E0s;

    ERIgeometries64 * E8d;
    ERIgeometries32 * E8s;

    cacheline64     * I8d;
    cacheline32     * I8s;

    cacheline64     * T8d;
    cacheline32     * T8s;

    RotationMatrices64 * RM8d;
    RotationMatrices32 * RM8s;

    cacheline64 * Dab, * Dcd, * Dac, * Dad, * Dbc, * Dbd;
    cacheline64 * Fab, * Fcd, * Fac, * Fad, * Fbc, * Fbd;

    double * FFF;


    ERIgeometry * E0; // this one is always the same type
    void        * E8;
    void        * AB8; //for storing precomputed  primitive product weights
    void        * CD8; //same
    void        * I8;
    void        * T8;
    void        * RM8;
    void        * RMP8;
    void        * RMD8;
    void        * RMF8;
    void        * RMG8;

    void * dab, * dcd, * dac, * dad, * dbc, * dbd;
    void * fab, * fcd, * fac, * fad, * fbc, * fbd;

    void SetBuffers   (const ERIBatch & TheBatch, LibQuimera::ERIbuffer & ERIbuff, int NMS=1);

    void SetBuffers64 (const ERIBatch & TheBatch, LibQuimera::ERIbuffer & ERIbuff, int NMs=1);
    void SetBuffers32 (const ERIBatch & TheBatch, LibQuimera::ERIbuffer & ERIbuff, int NMs=1);
};

//a memory buffer for the GPU batch evaluations
class BlockBufferGPU {

  public:
    // device memory
    // allow only one instance
    static size_t    TotalMaxBlockMem;
    static size_t    LocalMaxBlockMem;
    static uint8_t * TotalBlockMem;

    static size_t    TotalMaxBlockMemGPU [MAX_GPUS];
    static size_t    LocalMaxBlockMemGPU [MAX_GPUS];
    static uint8_t * TotalBlockMemGPU    [MAX_GPUS];

    static int       NumGPUs;

    static void      InitMem    (size_t MEMGPU, BatchEvaluator * BE, int Nprox);  // total memory initialization
    static void      FreeMem    ();

    static uint64_t  MaxTilesN  (const BatchInfo & TheBatch);

  public:

    // device memory
    size_t MaxBlockMemGPU;
    uint8_t * BlockMemGPU;

    // host memory
    size_t MaxBlockMem;
    uint8_t * BlockMem;


    // generic void pointers which
    // can be cast to point to whatever
    int         * AtA;
    int         * AtB;
    int         * AtC;
    int         * AtD;
    uint32_t    * Juse;
    uint32_t    * Xuse;

    int         * IntBuff;

    ERIgeometry * E0; // this one is always the same type
    void        * E8;
    void        * AB8; //for storing precomputed  primitive product weights
    void        * CD8; //same
    void        * I8;
    void        * T8;
    void        * RM8;
    void        * RMP8;
    void        * RMD8;
    void        * RMF8;
    void        * RMG8;

    void * dab, * dcd, * dac, * dad, * dbc, * dbd;
    void * fab, * fcd, * fac, * fad, * fbc, * fbd;

    //device pointers
    //ERIgeometry * dE0; // this one is always the same type
    int         * dAtA;
    int         * dAtB;
    int         * dAtC;
    int         * dAtD;

    //bit fields
    uint32_t    * dJuse;
    uint32_t    * dXuse;

    int         * dIntBuff;

    double      * dE8;
    double      * dAB8; //for storing precomputed  primitive product weights
    double      * dCD8; //same
    double      * dG8; //for [0] kernels
    double      * dI8;
    double      * dT8;
    double      * dRM8;
    double      * dRMS8;
    double      * dRMP8;
    double      * dRMD8;
    double      * dRMF8;
    double      * dRMG8;


    double * dDab, * dDcd, * dDac, * dDad, * dDbc, * dDbd;
    double * dFab, * dFcd, * dFac, * dFad, * dFbc, * dFbd;

    void SetBuffersN  (const ERIBatch & TheBatch, LibQuimera::ERIbuffer & ERIbuff);
};




class ERIbatchBuffer {
  private:
    //LibQuimera::ERITile64  * TheTiles;

    uint8_t * mem;

    uint64_t TotalMem;

    std::map<uint64_t, uint64_t> memFree; // use faster than map !

    uint32_t count;
    uint64_t UsedMem;

    #ifdef _OPENMP
        omp_lock_t Lock;
    #endif

  public:
    ERIbatchBuffer();
    void Init(size_t MEM);
    void * Allocate (uint64_t & Size);
    void ReAllocate(void * List, uint64_t OSize, uint64_t NSize);
    void Free(void * List, uint64_t Size);

    void Check();
   ~ERIbatchBuffer();
};


//info about the kind of integral (BatchInfo)
class ERIBatch:public BatchInfo {
    friend class BatchEvaluator;
    friend class ERIblock;
    friend class Fock2e;
    friend class BlockBuffer;

  public:
    LibQuimera::ERITile64  * TileList64;
    LibQuimera::ERITile32  * TileList32;
    LibQuimera::ERITile<DOUBLES_PER_BLOCK>  * TileListN;

    uint64_t       Ntiles64;
    uint64_t       Ntiles32;
    uint64_t       NtilesN;

    uint64_t       memAllocated;

    uint64_t Nprescreened;

    bool      J;
    bool      X;


  public:
    void EvaluateOCLD (const Sparse & Ds);

    const ERIBatch & operator=(const ERIblock & rhs);

    ERIBatch() {
        TileList64 = NULL;
        Ntiles64   = 0;
        TileList32 = NULL;
        Ntiles32   = 0;
        TileListN  = NULL;
        NtilesN    = 0;
    }

    ERIBatch(const Fock2e * pfock2e) {
        TileList64 = NULL;
        Ntiles64   = 0;
        TileList32 = NULL;
        Ntiles32   = 0;
        TileListN  = NULL;
        NtilesN    = 0;
        pFock      = pfock2e;
    }

    void clear(ERIbatchBuffer * EbatchBuffer) {
        if (TileList64!=NULL) EbatchBuffer->Free(TileList64, memAllocated);
        if (TileList32!=NULL) EbatchBuffer->Free(TileList32, memAllocated);
        if (TileListN !=NULL) EbatchBuffer->Free(TileListN , memAllocated);
        TileList64 = NULL;
        Ntiles64   = 0;
        TileList32 = NULL;
        Ntiles32   = 0;
        TileListN  = NULL;
        NtilesN    = 0;
    }

};


// create 8 CUDA streams: 2 for double-buffering and 6 for digestion (2J+4X)
const int NSTREAMS = 8;
const int NEVENTS = 2;



class NTconst;
class NTmulti;

class BatchEvaluator {
    friend class BlockBufferCPU;
    friend class BlockBufferGPU;

  private:

    LibQuimera::ERIbuffer  b4ERIs;     //a CPU (host) buffer for evaluating ERI batches
    BlockBufferCPU         b4blockCPU; //a CPU (host) buffer for storing lists, etc.
    BlockBufferGPU         b4blockGPU; //a GPU (device) buffer for storing lists, etc.


    cudaStream_t * GPUstream; //)[NSTREAMS];
    cudaEvent_t  * event_set;

    StreamChrono * Chronos;

    //GEOMETRY AND ROTATION MATRIX GENERATION
    //***************************************
    void Evaluate_LP_ABCD (const ERIBatch & TheBatch);
    void Evaluate_LP_AACD (const ERIBatch & TheBatch);
    void Evaluate_LP_AACC (const ERIBatch & TheBatch);
    void Evaluate_LP_ABAB (const ERIBatch & TheBatch);

    void Evaluate_RM_ABCD (const ERIBatch & TheBatch);
    void Evaluate_RM_AACD (const ERIBatch & TheBatch);
    void Evaluate_RM_AACC (const ERIBatch & TheBatch);
    void Evaluate_RM_ABAB (const ERIBatch & TheBatch);

    //COLD PRISM STEPS
    //****************
    void Evaluate_OC      (const ERIBatch & TheBatch);
    void Evaluate_OC_SR   (const ERIBatch & TheBatch, double w2);
    void Evaluate_OC_LR   (const ERIBatch & TheBatch, double w2);

    void Evaluate_L       (const ERIBatch & TheBatch);
    void Evaluate_L_sameF (const ERIBatch & TheBatch);

    void Evaluate_D_Js       (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs);
    void Evaluate_D_Xs       (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMss, double w=1.);
    void Evaluate_D_Js_sameF (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs);
    void Evaluate_D_Xs_sameF (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMss, double w=1.);


    void DistributeWs        (const ERIBatch & TheBatch, tensor4 & W,            const sparsetensorpattern * STP); // all
    void DistributeWs        (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP); // AACC
    void DistributeWs        (const ERIBatch & TheBatch, r1tensor<tensor4> & W3, const sparsetensorpattern * STP); // AACD
    void DistributeDs        (const ERIBatch & TheBatch, tensor2           & D,  const sparsetensorpattern * STP); // abab (diagonal)
    void DistributeDs        (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP); // ABAB (block diagonal)

    void DistributeCholesky  (const ERIBatch & TheBatch, tensor2           & W2, const sparsetensorpattern * STP); //



    //routines to move data between host and device
    void CopyWtoDevice      (const ERIBatch & TheBatch, cudaStream_t * stream);
    void CopyKRLsFromDevice (const ERIBatch & TheBatch, cudaStream_t * stream);
    void CopyERIsFromDevice (const ERIBatch & TheBatch, cudaStream_t * stream);

    void CopyDJtoDevice     (const ERIBatch & TheBatch, cudaStream_t * stream, const Sparse & Ds);
    void CopyDJfromDevice   (const ERIBatch & TheBatch, cudaStream_t * stream, Sparse & Js);
    void CopyDXtoDevice     (const ERIBatch & TheBatch, cudaStream_t * stream, const Sparse & Ds);
    void CopyDXfromDevice   (const ERIBatch & TheBatch, cudaStream_t * stream, Sparse & Xs);

    //only these are being used at the moment
    void CopyDJXtoDevice    (const ERIBatch & TheBatch, cudaStream_t * stream, const Sparse & Ds);
    void CopyDJXtoDevice    (const ERIBatch & TheBatch, cudaStream_t * stream, SparseGPU & gDs);

    void CopyDJXfromDevice  (const ERIBatch & TheBatch, cudaStream_t * stream, Sparse & Js, Sparse & Xs);
    void CopyDJXfromDevice  (const ERIBatch & TheBatch, cudaStream_t * stream, uint32_t * n2XX, SparseGPU & gJs, SparseGPU & gXs);

    // contract all density matrix subblocks
    void ContractDJXs       (const ERIBatch & TheBatch, SparseGPU * DM, cudaStream_t * stream);

    void CopyAtomstoDevice  (const ERIBatch & TheBatch, cudaStream_t * stream, cudaEvent_t * event_copy, int nAtoms);


    void AdjustForSymmetry  (const ERIBatch & TheBatch, cudaStream_t * stream);




    //GEOMETRY AND ROTATION MATRIX GENERATION
    //***************************************
    void LPRM_ABCD (const ERIBatch & TheBatch, cudaStream_t * stream);
    void LPRM_AACD (const ERIBatch & TheBatch, cudaStream_t * stream);
    void LPRM_AACC (const ERIBatch & TheBatch, cudaStream_t * stream);
    void LPRM_ABAB (const ERIBatch & TheBatch, cudaStream_t * stream);



    //COLD PRISM STEPS
    //****************
    template<int NPB> void OC      (const ERIBatch & TheBatch);
    template<int NPB> void L       (const ERIBatch & TheBatch);
    template<int NPB> void L_sameF (const ERIBatch & TheBatch);

    template<int NPB> void D_Js (const ERIBatch & TheBatch, const Sparse & Ds,  Sparse & Js);
    template<int NPB> void D_Xs (const ERIBatch & TheBatch, const Sparse & Ds,  Sparse & Xs);

    template<int NPB> void D_Js_sameF (const ERIBatch & TheBatch, const Sparse & Ds,  Sparse & Js);
    template<int NPB> void D_Xs_sameF (const ERIBatch & TheBatch, const Sparse & Ds,  Sparse & Xs);

    //use templated CUDA code
    void ContractDJs  (const ERIBatch & TheBatch, cudaStream_t * stream);
    void ContractDXs  (const ERIBatch & TheBatch, cudaStream_t * stream);
    void ContractDJXs (const ERIBatch & TheBatch, cudaStream_t * stream);

    template<int NPB> void OC      (const ERIBatch & TheBatch, cudaStream_t * stream);
    template<int NPB> void L       (const ERIBatch & TheBatch, cudaStream_t * stream);


    void SetReduces         (const ERIBatch & TheBatch, cudaStream_t ** stream, cudaEvent_t * event_copy, uint32_t * n2XX, int nAtoms);
    void SetReducesCPU      (const ERIBatch & TheBatch, cudaStream_t ** stream, uint32_t * n2XX, int nAtoms);
    void CopyDJXtoDevice    (const ERIBatch & TheBatch, cudaStream_t ** stream, SparseGPU & gDs);
    void ContractDJXs       (const ERIBatch & TheBatch, cudaStream_t ** stream);
    void CopyDJXfromDevice  (const ERIBatch & TheBatch, cudaStream_t ** stream, uint32_t * n2XX, SparseGPU & gJs, SparseGPU & gXs);


    void FullDJXcontraction (const ERIBatch & TheBatch, cudaStream_t ** stream,   SparseGPU & gDs, SparseGPU & gJs, SparseGPU & gXs);
    void ContractDJXs       (const ERIBatch & TheBatch, cudaStream_t ** stream,
                                    double ** d_Dab,  double ** d_Dcd,   double ** d_Dac, double ** d_Dad, double ** d_Dbc, double ** d_Dbd,
                                    double ** d_Fab,  double ** d_Fcd,   double ** d_Fac, double ** d_Fad, double ** d_Fbc, double ** d_Fbd);

  //functions and variables to be accessed publicly
  public:

    int GPUdevice;


    // pointer to atom centers on device
    const rAtom * AtomListGpus; // atom positions

    // partial updates on device
    SparseGPU * Jgpu;
    SparseGPU * Xgpu;
    SparseGPU * Agpu;

    static void InitMem (size_t MEM, size_t MEMGPU, BatchEvaluator * BE, int Nprox);
    static void InitMem (size_t MEM,                BatchEvaluator * BE, int Nprox);
    static void FreeMem();

    //FUNCTIONS FOR BLOCK EVALUATION OF ERIs
    //**************************************

    BatchEvaluator   ();
   ~BatchEvaluator   ();

    double GetTime   (int i);

    bool IsFree      () const;


    void SetBuffers  (const ERIBatch & TheBatch); //set the buffers externally, so the routines can be invoked for Cauchy-Schwarz

    void Evaluate    (const ERIBatch & TheBatch, int nmat,    const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant,    double gamma=1.); // evaluates coulomb and contraction separately
    void EvaluateCAM (const ERIBatch & TheBatch, int nmat,    const NTconst & Dsnt, const NTconst & Dant, NTmulti & Jnt, NTmulti & Xnt, NTmulti & Ant,    double alpha, double beta, double mu);

    void Evaluate         (const ERIBatch & TheBatch, const Sparse * Ds, const Sparse * Da, SparseGPU * gDs, SparseGPU * gDa, bool * UseA, int nmat); // evaluates coulomb and contraction separately
    void EvaluateT        (const ERIBatch & TheBatch, tensor4 & W, const sparsetensorpattern * STP);       // evaluate full tensor

    void EvaluateDiagonal (const ERIBatch & TheBatch, tensor2           & D , const sparsetensorpattern * STP); // evaluate tensor diagonal
    void EvaluateDiagonal (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP); // evaluate tensor diagonal
    void Evaluate2Center  (const ERIBatch & TheBatch, r2tensor<tensor4> & W2, const sparsetensorpattern * STP);
    void Evaluate3Center  (const ERIBatch & TheBatch, r1tensor<tensor4> & W3, const sparsetensorpattern * STP);
    void EvaluateCholesky (const ERIBatch & TheBatch, tensor2           & W2, const sparsetensorpattern * STP); // evaluate relevant entries of the Cholesky tensor


    // compute the ERI trace for Cauchy-Schwarz
    cacheline64 ERItrace    (const ERIBatch & TheBatch, const ShellPair & ABs, const ERIgeometries64 & eriG8, const LibQuimera::ERITile64 & ET, uint8_t lenk);
    double      ERItrace    (const ERIBatch & TheBatch, const ShellPair & ABs, const ERIgeometry & eriG);

    // compute the
    void        ERIbasis    (const ERIBatch & TheBatch, const ShellPair & ABs, const ERIgeometries64 & eriG8, const LibQuimera::ERITile64 & ET, uint8_t lenk);
    void        ERIbasis    (const ERIBatch & TheBatch, const ShellPair & ABs, const ERIgeometry & eriG);

    // construct 1-center ERI
    void        ConstructW  (const ERIBatch & TheBatch, const ERIgeometry & eriG, double * WWW, uint32_t wa);
    void        ConstructW  (const ERIBatch & TheBatch, const ERIgeometry & eriG, double * WWW, uint32_t wa, double alpha, double beta, double w2); // for CAMB3LYP or similar

    void ConstructWS (const ERIBatch & TheBatch, double w, const ShellPair & ABs, const ShellPair & CDs, const ERIgeometry & eriG, double * WWW, uint32_t wa);
    void ConstructWL (const ERIBatch & TheBatch, double w, const ShellPair & ABs, const ShellPair & CDs, const ERIgeometry & eriG, double * WWW, uint32_t wa);

    // compute the 1-center ERIs, but only store the diagonal elemnents
    void ConstructDiagonal(const ERIBatch & TheBatch, const ERIgeometry & eriG, double * WWW, uint32_t wa);


    // new 1-center ERI
    void AAAA        (const ERIBatch & TheBatch, const ERIgeometry & eriG);
    void AAAA        (const ERIBatch & TheBatch, const ERIgeometry & eriG, double alpha, double beta, double w2);

    void AddCoulomb  (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs, uint32_t at);
    void AddExchange (const ERIBatch & TheBatch, const NTconst & Dnt, NTmulti & Jnt, int NMs, uint32_t at);
};


#endif
