#include "defs.hpp"

static const double MINS  = 1.e-30;
static const double MINS2 = 1.e-15;

__constant__ double dSHrot [LMAX+1][2*LMAX+1][2*LMAX+1];
__constant__ double dSHrotI[LMAX+1][2*LMAX+1][2*LMAX+1];


struct vector3 {
    double x;
    double y;
    double z;

    inline __device__ double operator*(const vector3 & q) const {
        return x*q.x + y*q.y + z*q.z;
    }

    inline __device__ void operator() (double xx, double yy, double zz) {
        x = xx;
        y = yy;
        z = zz;
    }

    inline __device__ vector3 operator*(double s) const {
        vector3 r;
        r.x = s*x;
        r.y = s*y;
        r.z = s*z;
        return r;
    }

    inline __device__ vector3 operator+(const vector3 & s) const {
        vector3 r;
        r.x = x + s.x;
        r.y = y + s.y;
        r.z = z + s.z;
        return r;
    }

    inline __device__ vector3 operator-(const vector3 & s) const {
        vector3 r;
        r.x = x - s.x;
        r.y = y - s.y;
        r.z = z - s.z;
        return r;
    }
};

struct rAtom   {
    double x;
    double y;
    double z;
    double * p;

    inline __device__ vector3 operator-(const rAtom & q) const {
        vector3 v;
        v.x = x - q.x;
        v.y = y - q.y;
        v.z = z - q.z;
        return v;
    }
};


template<int L> __device__ void dSphHarRot (double (&M)[2*L+1][2*L+1], const vector3 & vx, const vector3 & vy, const vector3 & vz) {

    double P [2*L+1][2*L+1];
    double Q [2*L+1][2*L+1];
    double S [2*L+1][2*L+1];

    double ca, sa;
    double cb, sb;
    double cc, sc;

    {
        double s = sqrt(vx.z*vx.z + vy.z*vy.z);
        cb =  vz.z;
        sb = -s;

        if (s>MINS2) {
            double is = 1/s;
            ca =   vx.z * is;
            sa = - vy.z * is;
            cc = - vz.x * is;
            sc = - vz.y * is;
        }
        else {
            ca = 1;
            sa = 0;
            cc = 1;
            sc = 0;
        }
    }

    double cn, sn;
    double cm, sm;

    {
        for (int j=0; j<=2*L; ++j) {
            P[j][L] = dSHrotI[L] [j][L];
        }

        cn = 1; sn = 0;
        for (int l=1; l<=L; ++l) {
            cm = cn*ca - sn*sa;
            sm = cn*sa + sn*ca;

            for (int j=0; j<=2*L; ++j) {
                P[j][L+l] = cm * dSHrotI[L] [j][L+l] + sm * dSHrotI[L] [j][L-l];
                P[j][L-l] = cm * dSHrotI[L] [j][L-l] - sm * dSHrotI[L] [j][L+l];
            }

            cn = cm;
            sn = sm;
        }
    }

    //x-y (z-x) in-plane rotation
    {
        for (int j=0; j<=2*L; ++j)
            Q[L][j] = P[L][j];

        cn = 1; sn = 0;
        for (int l=1; l<=L; ++l) {
            cm = cn*cb - sn*sb;
            sm = cn*sb + sn*cb;

            for (int j=0; j<=2*L; ++j) {
                Q[L+l][j] = cm * P[L+l][j] - sm * P[L-l][j];
                Q[L-l][j] = cm * P[L-l][j] + sm * P[L+l][j];
            }

            cn = cm;
            sn = sm;
        }
    }

    //index permutation ^-1
    for (int i=0; i<=2*L; ++i) {
        for (int j=0; j<=2*L; ++j) {
            double sum = 0;
            for (int k=0; k<=2*L; ++k) {
                sum += dSHrot[L][i][k] * Q[k][j];
            }
            S[i][j] = sum;
        }
    }

    //x-y in-plane rotation
    {
        for (int j=0; j<=2*L; ++j)
            M[L][j] = S[L][j];

        cn = 1; sn = 0;
        for (int l=1; l<=L; ++l) {
            cm = cn*cc - sn*sc;
            sm = cn*sc + sn*cc;

            for (int j=0; j<=2*L; ++j) {
                M[L+l][j] = cm * S[L+l][j] - sm * S[L-l][j];
                M[L-l][j] = cm * S[L-l][j] + sm * S[L+l][j];
            }

            cn = cm;
            sn = sm;
        }
    }
}

//rotates a spherical harmonic rotation matrix around the z axis
template<int L> __device__ void dSphHarRot (double (&R)[2*L+1][2*L+1], const double (&Rz)[2*L+1][2*L+1], double cc, double ss) {
    double cn = 1;
    double sn = 0;
    double cm, sm;

    for (int j=0; j<=2*L; ++j) {
        R[j][L] =  Rz[j][L];
    }

    for (int l=1; l<=L; ++l) {
        cm = cn*cc - sn*ss;
        sm = cn*ss + sn*cc;

        for (int j=0; j<=2*L; ++j) {
            R[j][L-l] =  cm*Rz[j][L-l] - sm*Rz[j][L+l];
            R[j][L+l] =  sm*Rz[j][L-l] + cm*Rz[j][L+l];
        }

        cn = cm;
        sn = sm;
    }
}



inline __device__ void ConstructRotHH (vector3 & vx, vector3 & vy, vector3 & vz,  const vector3 & u) {
    vx(1,0,0);
    vy(0,1,0);
    vz(0,0,1);

    double m = u.x*u.x + u.y*u.y;

    if (m > MINS) {
        m += u.z*u.z;
        m = sqrt(m);

        //vector3 w = u;
        double wzz;

        double H;

        if (u.z>0) {
            wzz = u.z + m;
            H = m*wzz;
        }
        else {
            wzz = u.z - m;
            H = -m*wzz;
        }

        double iH = 1/H;

        vx.x -= u.x*u.x*iH;
        vx.y -= u.x*u.y*iH;
        vx.z -= u.x*wzz*iH;

        vy.x -= u.x*u.y*iH;
        vy.y -= u.y*u.y*iH;
        vy.z -= u.y*wzz*iH;

        vz.x -= u.x*wzz*iH;
        vz.y -= u.y*wzz*iH;
        vz.z -= wzz*wzz*iH;

        vz.x = -vz.x;
        vz.y = -vz.y;
        vz.z = -vz.z;
    }
}

inline __device__ void ConstructRotHH (double & cc, double & ss, const vector3 & vx, const vector3 & vy, const vector3 & vz,  const vector3 & v) {
    double sx, sy;
    sx = vx.x*v.x + vx.y*v.y + vx.z*v.z;
    sy = vy.x*v.x + vy.y*v.y + vy.z*v.z;

    if (fabs(sx) > MINS2) {
        double m = sqrt(sx*sx + sy*sy);

        double H;

        if (sy>0) {
            sy += m;
            H = m*sy;
        }
        else {
            sy -= m;
            H = -m*sy;
        }

        double iH = 1/H;

        cc = 1 - sx*sx*iH;
        ss =   - sx*sy*iH;
    }
    else {
        cc = 1;
        ss = 0;
    }
}



__global__ void MakeRotation4  (double * Es, double * RMs,
                                const int * A, const int * B, const int * C, const int * D,
                                const rAtom * centers,
                                bool invAB, bool invCD,
                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles) {

    int ii = threadIdx.x;
    int kk = blockIdx.y * blockDim.y + threadIdx.y;
    const int NTX = blockDim.x;

    if (kk>=Ntiles) return;

    int jj = kk*NTX + ii;


    // frame of reference
    vector3 AB = centers[B[jj]] - centers[A[jj]];
    vector3 CD = centers[D[jj]] - centers[C[jj]];
    vector3 AC = centers[C[jj]] - centers[A[jj]];


    vector3 vx, vy, vz;
    ConstructRotHH(vx,vy,vz, AB);

    double c,s;
    ConstructRotHH(c,s, vx, vy, vz, CD);

    vector3 wx, wy;
    wx = vx*c    + vy*s;
    wy = vx*(-s) + vy*c;
    const vector3 & wz = vz;


    double ABz = AB * wz;
    double CDy = CD * wy;
    double CDz = CD * wz;
    double ACx = AC * wx;
    double ACy = AC * wy;
    double ACz = AC * wz;


    if (invAB) {
        ACz -=  ABz; //BC
        ABz  = -ABz; //BA
    }
    if (invCD) {
        ACy +=  CDy; //AD
        ACz +=  CDz; //AD
        CDy  = -CDy; //DC
        CDz  = -CDz; //DC
    }

    // write ERI geometries
    Es[kk*6*NTX +       ii] = ABz;
    Es[kk*6*NTX +   NTX+ii] = CDy;
    Es[kk*6*NTX + 2*NTX+ii] = CDz;
    Es[kk*6*NTX + 3*NTX+ii] = ACx;
    Es[kk*6*NTX + 4*NTX+ii] = ACy;
    Es[kk*6*NTX + 5*NTX+ii] = ACz;

    // rotation matrices
    if (useP) {
        double PP0 [3] [3];
        double PP  [3] [3];
        dSphHarRot<1> ( PP0, vx, vy, vz);
        dSphHarRot<1> ( PP, PP0, c, s);

        for (int i=0; i< 3; ++i)
            for (int j=0; j< 3; ++j)
                RMs[9*NTX*kk + (3*i+j)*NTX + ii] = PP[i][j];

        RMs += 9*NTX*Ntiles;
    }

    if (useD) {
        double DD0 [5] [5];
        double DD  [5] [5];
        dSphHarRot<2> ( DD0, vx, vy, vz);
        dSphHarRot<2> ( DD, DD0, c, s);

        for (int i=0; i< 5; ++i)
            for (int j=0; j< 5; ++j)
                RMs[25*NTX*kk + (5*i+j)*NTX + ii] = DD[i][j];

        RMs += 25*NTX*Ntiles;
    }
    /*

    if (useF) {
        double FF0 [7] [7];
        double FF  [7] [7];
        dSphHarRot<3> ( FF0, vx, vy, vz);
        dSphHarRot<3> ( FF, FF0, c, s);

        for (int i=0; i< 7; ++i)
            for (int j=0; j< 7; ++j)
                RMs[49*NTX*kk + (7*i+j)*NTX + ii] = FF[i][j];

        RMs += 49*NTX*Ntiles;
    }

    if (useG) {
        double GG0 [9] [9];
        double GG  [9] [9];
        dSphHarRot<4> ( GG0, vx, vy, vz);
        dSphHarRot<4> ( GG, GG0, c, s);

        for (int i=0; i< 9; ++i)
            for (int j=0; j< 9; ++j)
                RMs[81*NTX*kk + (9*i+j)*NTX + ii] = GG[i][j];

        RMs += 81*NTX*Ntiles;
    }
    */

}


__global__ void MakeRotation3  (double * Es, double * RMs,
                                const int * A,                const int * C, const int * D,
                                const rAtom * centers,
                                            bool invCD,
                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles) {

    int ii = threadIdx.x;
    int kk = blockIdx.y * blockDim.y + threadIdx.y;
    const int NTX = blockDim.x;

    if (kk>=Ntiles) return;

    int jj = kk*NTX + ii;


    // frame of reference
    vector3 CD = centers[D[jj]] - centers[C[jj]];
    vector3 AC = centers[C[jj]] - centers[A[jj]];


    vector3 vx, vy, vz;
    ConstructRotHH(vx,vy,vz, CD);

    double c,s;
    ConstructRotHH(c,s, vx, vy, vz, AC);

    vector3 wx, wy;
    wx = vx*c    + vy*s;
    wy = vx*(-s) + vy*c;
    const vector3 & wz = vz;


    double ABz = 0;
    double CDy = 0;
    double CDz = CD * wz;
    double ACx = 0;
    double ACy = AC * wy;
    double ACz = AC * wz;


    if (invCD) {
        ACz +=  CDz; //AD
        CDz  = -CDz; //DC
    }

    // write ERI geometries
    Es[kk*6*NTX +       ii] = ABz;
    Es[kk*6*NTX +   NTX+ii] = CDy;
    Es[kk*6*NTX + 2*NTX+ii] = CDz;
    Es[kk*6*NTX + 3*NTX+ii] = ACx;
    Es[kk*6*NTX + 4*NTX+ii] = ACy;
    Es[kk*6*NTX + 5*NTX+ii] = ACz;


    // rotation matrices
    if (useP) {
        double PP0 [3] [3];
        double PP  [3] [3];
        dSphHarRot<1> ( PP0, vx, vy, vz);
        dSphHarRot<1> ( PP, PP0, c, s);

        for (int i=0; i< 3; ++i)
            for (int j=0; j< 3; ++j)
                RMs[9*NTX*kk + (3*i+j)*NTX + ii] = PP[i][j];

        RMs += 9*NTX*Ntiles;
    }

    if (useD) {
        double DD0 [5] [5];
        double DD  [5] [5];
        dSphHarRot<2> ( DD0, vx, vy, vz);
        dSphHarRot<2> ( DD, DD0, c, s);

        for (int i=0; i< 5; ++i)
            for (int j=0; j< 5; ++j)
                RMs[25*NTX*kk + (5*i+j)*NTX + ii] = DD[i][j];

        RMs += 25*NTX*Ntiles;
    }
    /*
    if (useF) {
        double FF0 [7] [7];
        double FF  [7] [7];
        dSphHarRot<3> ( FF0, vx, vy, vz);
        dSphHarRot<3> ( FF, FF0, c, s);

        for (int i=0; i< 7; ++i)
            for (int j=0; j< 7; ++j)
                RMs[49*NTX*kk + (7*i+j)*NTX + ii] = FF[i][j];

        RMs += 49*NTX*Ntiles;
    }

    if (useG) {
        double GG0 [9] [9];
        double GG  [9] [9];
        dSphHarRot<4> ( GG0, vx, vy, vz);
        dSphHarRot<4> ( GG, GG0, c, s);

        for (int i=0; i< 9; ++i)
            for (int j=0; j< 9; ++j)
                RMs[81*NTX*kk + (9*i+j)*NTX + ii] = GG[i][j];

        RMs += 81*NTX*Ntiles;
    }
    */

}

__global__ void MakeRotation2  (double * Es, double * RMs,
                                const int * A,                const int * C,
                                const rAtom * centers,

                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles) {

    int ii = threadIdx.x;
    int kk = blockIdx.y * blockDim.y + threadIdx.y;
    const int NTX = blockDim.x;

    if (kk>=Ntiles) return;

    int jj = kk*NTX + ii;


    // frame of reference
    vector3 AC = centers[C[jj]] - centers[A[jj]];


    vector3 vx, vy, vz;
    ConstructRotHH(vx,vy,vz, AC);


    double ABz = 0;
    double CDy = 0;
    double CDz = 0;
    double ACx = 0;
    double ACy = 0;
    double ACz = AC * vz;

    // write ERI geometries
    Es[kk*6*NTX +       ii] = ABz;
    Es[kk*6*NTX +   NTX+ii] = CDy;
    Es[kk*6*NTX + 2*NTX+ii] = CDz;
    Es[kk*6*NTX + 3*NTX+ii] = ACx;
    Es[kk*6*NTX + 4*NTX+ii] = ACy;
    Es[kk*6*NTX + 5*NTX+ii] = ACz;


    // rotation matrices
    if (useP) {
        double PP  [3] [3];
        dSphHarRot<1> ( PP, vx, vy, vz);

        for (int i=0; i< 3; ++i)
            for (int j=0; j< 3; ++j)
                RMs[9*NTX*kk + (3*i+j)*NTX + ii] = PP[i][j];

        RMs += 9*NTX*Ntiles;
    }

    if (useD) {
        double DD  [5] [5];
        dSphHarRot<2> ( DD, vx, vy, vz);

        for (int i=0; i< 5; ++i)
            for (int j=0; j< 5; ++j)
                RMs[25*NTX*kk + (5*i+j)*NTX + ii] = DD[i][j];

        RMs += 25*NTX*Ntiles;
    }
    /*
    if (useF) {
        double FF  [7] [7];
        dSphHarRot<3> ( FF, vx, vy, vz);

        for (int i=0; i< 7; ++i)
            for (int j=0; j< 7; ++j)
                RMs[49*NTX*kk + (7*i+j)*NTX + ii] = FF[i][j];

        RMs += 49*NTX*Ntiles;
    }

    if (useG) {
        double GG  [9] [9];
        dSphHarRot<4> ( GG, vx, vy, vz);

        for (int i=0; i< 9; ++i)
            for (int j=0; j< 9; ++j)
                RMs[81*NTX*kk + (9*i+j)*NTX + ii] = GG[i][j];

        RMs += 81*NTX*Ntiles;
    }
    */

}

__global__ void MakeRotation2S (double * Es, double * RMs,
                                const int * A, const int * B,
                                const rAtom * centers,
                                bool invAB, bool invCD,
                                bool useP, bool useD, bool useF, bool useG,
                                int Ntiles) {

    int ii = threadIdx.x;
    int kk = blockIdx.y * blockDim.y + threadIdx.y;
    const int NTX = blockDim.x;

    if (kk>=Ntiles) return;

    int jj = kk*NTX + ii;


    // frame of reference
    vector3 AB = centers[B[jj]] - centers[A[jj]];


    vector3 vx, vy, vz;
    ConstructRotHH(vx,vy,vz, AB);


    double ABz = AB*vz;
    double CDy = 0;
    double CDz = ABz;
    double ACx = 0;
    double ACy = 0;
    double ACz = 0;

    if (invAB) {
        ACz -=  ABz; //BC
        ABz  = -ABz; //BA
    }
    if (invCD) {
        ACz +=  CDz; //AD
        CDz  = -CDz; //DC
    }

    // write ERI geometries
    Es[kk*6*NTX +       ii] = ABz;
    Es[kk*6*NTX +   NTX+ii] = CDy;
    Es[kk*6*NTX + 2*NTX+ii] = CDz;
    Es[kk*6*NTX + 3*NTX+ii] = ACx;
    Es[kk*6*NTX + 4*NTX+ii] = ACy;
    Es[kk*6*NTX + 5*NTX+ii] = ACz;

    // rotation matrices
    if (useP) {
        double PP  [3] [3];
        dSphHarRot<1> ( PP, vx, vy, vz);

        for (int i=0; i< 3; ++i)
            for (int j=0; j< 3; ++j)
                RMs[9*NTX*kk + (3*i+j)*NTX + ii] = PP[i][j];

        RMs += 9*NTX*Ntiles;
    }

    if (useD) {
        double DD  [5] [5];
        dSphHarRot<2> ( DD, vx, vy, vz);

        for (int i=0; i< 5; ++i)
            for (int j=0; j< 5; ++j)
                RMs[25*NTX*kk + (5*i+j)*NTX + ii] = DD[i][j];

        RMs += 25*NTX*Ntiles;
    }
    /*

    if (useF) {
        double FF  [7] [7];
        dSphHarRot<3> ( FF, vx, vy, vz);

        for (int i=0; i< 7; ++i)
            for (int j=0; j< 7; ++j)
                RMs[49*NTX*kk + (7*i+j)*NTX + ii] = FF[i][j];

        RMs += 49*NTX*Ntiles;
    }

    if (useG) {
        double GG  [9] [9];
        dSphHarRot<4> ( GG, vx, vy, vz);

        for (int i=0; i< 9; ++i)
            for (int j=0; j< 9; ++j)
                RMs[81*NTX*kk + (9*i+j)*NTX + ii] = GG[i][j];

        RMs += 81*NTX*Ntiles;
    }
    */

}



#include "math/angular.hpp"

void InitDeviceSHrot () {

    size_t size = (LMAX+1)*(2*LMAX+1)*(2*LMAX+1) * sizeof(double);

    int NumGPUs;
    cudaGetDeviceCount(&NumGPUs);

    for (int n=0; n<NumGPUs; ++n) {
        //select device
        cudaSetDevice(n);

        //copy all tables
        cudaMemcpyToSymbol (dSHrot , LibAngular::SHrot , size);
        cudaMemcpyToSymbol (dSHrotI, LibAngular::SHrotI, size);
    }

}

