

#ifndef __ERIblocks__
#define __ERIblocks__

#include <map>
#include <set>

#include "BatchInfo.hpp"

#ifdef _OPENMP
    #include <omp.h>
#endif

#include "low/rtensors.hpp"


class ERIBatch;
class ERIbatchBuffer;
class ERIgeometries64;
class ERIgeometry;
class ShellPair;
class ShellPairPair;
class cacheline64;
class sparsetensorpattern;

namespace LibQuimera {
    struct ERITile64;
    struct ERITile32;
    template<int N> struct ERITile;
};



// CONCURRENT, MULTISTEP PRODUCERS/CONSUMERS PROBLEM
// blox will everntually be dispatched to any available hardware

#include "low/locks.hpp"

template<class T> class TaskQueue {
  private:
    std::deque<T*> TaskFIFOQueue;
    #ifdef _OPENMP
        omp_lock_t Lock;
    #endif
    volatile int len;

  public:

    TaskQueue() {
        #ifdef _OPENMP
        omp_init_lock (&Lock);
        #endif
        len=0;
    }

   ~TaskQueue() {
        #ifdef _OPENMP
        omp_destroy_lock (&Lock);
        #endif
    }

    void push(T * rhs) {
        #pragma omp atomic
        ++len;

        #ifdef _OPENMP
        omp_set_lock (&Lock);
        #endif
        {
            TaskFIFOQueue.push_back(rhs);
        }
        #ifdef _OPENMP
        omp_unset_lock (&Lock);
        #endif
    }

    void push_top(T * rhs) {
        #pragma omp atomic
        ++len;

        #ifdef _OPENMP
        omp_set_lock (&Lock);
        #endif
        {
            TaskFIFOQueue.push_front(rhs);
        }
        #ifdef _OPENMP
        omp_unset_lock (&Lock);
        #endif
    }

    T * pop() {
        T * ret;
        #ifdef _OPENMP
        omp_set_lock (&Lock);
        #endif
        {
            if (TaskFIFOQueue.empty())
                ret = NULL;
            else {
                ret = TaskFIFOQueue.front();
                TaskFIFOQueue.pop_front();
                #pragma omp atomic
                --len;
            }
        }
        #ifdef _OPENMP
        omp_unset_lock (&Lock);
        #endif

        return ret;
    }

    T * pop_bottom() {
        T * ret;
        #ifdef _OPENMP
        omp_set_lock (&Lock);
        #endif
        {
            if (TaskFIFOQueue.empty())
                ret = NULL;
            else {
                ret = TaskFIFOQueue.back();
                TaskFIFOQueue.pop_back();
                #pragma omp atomic
                --len;
            }
        }
        #ifdef _OPENMP
        omp_unset_lock (&Lock);
        #endif

        return ret;
    }

    int length() {
        return len; // TaskFIFOQueue.size();
    }
};

//semi-useless classes
//********************

class CasePrescreener {
  public:

    static const int NLogsShort = 1024;
    double LogShort[NLogsShort];
    double maxR2;


    double LogShortCoulomb (double R2);
    void   InitLogsShort ();

    double LogsShort (double R2);
};

//actual classes
//**************

struct ShellPairPair {
    uint32_t n12;
    uint32_t n34;

    ShellPairPair() {};

    ShellPairPair(uint32_t a, uint32_t b) {
        n12 = a;
        n34 = b;
    }
};

//to classify shellpairs according to total contraction degree
struct SPlist {
    uint32_t  Nsps;
    uint32_t  Isps;
    uint32_t  vList;
    uint32_t  rList; //not used for now

    SPlist() {
        Nsps  = Isps = 0;
        vList = 0;
        rList = 0;
    }
};

struct SPPblock {
    uint64_t            Nints; //LOL @ uint16_t bug
    uint64_t            Iints;
    ShellPairPair * vList;


    SPPblock() {
        Nints = Iints = 0;
        vList = NULL;
    }

    void reset() {
        Nints = Iints = 0;
        vList = NULL;
    }

    ShellPairPair pop() {
        if (Iints>0) {
            --Iints;
            return vList[Iints];
            //vList[Iints].n12 = vList[Iints].n34 = 0; //unnecessary
        }
        else {
            return vList[0]; //not sure
        }
    }

    const ShellPairPair & top() const {
        return vList[Iints-1];
    }

    void push(const ShellPairPair & rhs) {
        vList[Iints] = rhs;
        ++Iints;
    }

    int size() const {
        return Iints;
    }

    int capacity() const {
        return Nints;
    }

    void reserve() {
        ++Nints;
    }

};

struct SPJ {
    float cs;
    uint32_t   n;

    inline bool operator< (const SPJ & rhs) const {return (cs<rhs.cs);}
    inline bool operator==(const SPJ & rhs) const {return (cs<rhs.cs);}
    inline bool operator> (const SPJ & rhs) const {return (cs>rhs.cs);}
    inline bool operator>=(const SPJ & rhs) const {return (cs>=rhs.cs);}
    inline bool operator<=(const SPJ & rhs) const {return (cs<=rhs.cs);}
};



// new attempt to simplify stuff on the run
// *****************************************

struct Tij {
    uint32_t i;
    uint32_t j;

    bool operator<(const Tij & rhs)  const {
        if (i!=rhs.i) return (i<rhs.i);
        if (j!=rhs.j) return (j<rhs.j);
        return false;
    }
};

struct TileH {
    ShellPairPair * SP;

    Tij ij;
    int used;

    void set(ShellPairPair * pSP, const TileH & p1, const TileH & p2);
    void set(ShellPairPair * pSP, const TileH & p1);
};

struct TileSet {

    //for storing intermediate stuff
    ShellPairPair * SP1;
    ShellPairPair * SP2;

    TileH * T0;
    TileH * T1;
    TileH * T2;
    TileH * T3;
    TileH * T4;
    TileH * T5;

    uint64_t Npairs;


    TileSet();
    void Set(uint64_t MaxERIs);
    void push(uint32_t i, uint32_t j, const ShellPairPair & SPP);
    void PairAll ();
    ~TileSet();
};




//blox will be dispatched to any available hardware
class ERIblock:public BatchInfo {

    friend class ERIBatch;
    friend class Fock2e;

    //memory buffers and pointers for ERI evaluation
  private:
    ERIbatchBuffer * EBB;

    size_t ElementSize;
    ShellPairPair * SPPlist;
    double       * R2;

    TileSet TS;

    CasePrescreener CPEB;


    //atom blocks
    uint32_t min12;
    uint32_t max12;
    uint32_t * min34l;
    uint32_t * max34l;

    //the transposed ones
    uint32_t min34;
    uint32_t max34;
    uint32_t * min12l;
    uint32_t * max12l;

    r1tensor <SPlist>   SortedABs;
    r1tensor <SPlist>   SortedCDs;
    r2tensor <SPPblock> SortedERIs;


    //number of GDO buckets for each elemental basis function pair
    uint16_t K2max12;
    uint16_t K2max34;

    SPJ * ABlogs;
    SPJ * CDlogs;

    constexpr static const double maxX = 1e100;

    const sparsetensorpattern * STP;


    template <class T> struct Xrow {
        std::map <uint32_t,T> r;
        //int n;
        float minf;

        void add (uint32_t atb, const T & v) {
            r[atb] = v;
        }

        int size() const {
            return r.size();
        }

        T & operator[] (uint32_t atb) {
            return r[atb]; //r[atb];
        }

        T operator[] (uint32_t atb) const {
            if (r.count(atb)) return r.at(atb);
            else              return maxX;
        }

        T GetMin() {
            minf = maxX;

            std::map <uint32_t,float>::const_iterator it;

            for (it=r.begin(); it!=r.end(); ++it) minf = std::min (minf, it->second);

            return minf;
        }
    };

    template <class T> struct Xsparse {
        Xrow<T> * v;
        int n;
        float minf;

        Xsparse() {
            n = 0;
            v = NULL;
        }

        void set(int nn) {
            n = nn;
            v = new Xrow<T>[n];
        }

        void clean() {
            delete[] v;
            v = NULL;
            n = 0;
        }

        void add(int a, int b, float vv) {
            v[a][b] = vv;
        }

        ~Xsparse() {
            delete[] v;
        }

        const Xrow<T> & operator[] (int i) const {
            return v[i];
        }

        Xrow<T> & operator[] (int i) {
            return v[i];
        }


        T operator() (int i, int j) const {
            if (v[i].r.count(j)==0) return maxX;
            return v[i].r.at(j);
        }

        T & operator() (int i, int j) {
            return v[i].r[j];
        }

        float GetMin() {
            minf = v[0].GetMin();

            for (int i=1; i<n; ++i) minf = std::min (minf, v[i].GetMin());

            return minf;
        }
    };

    struct {
        int natoms;

        //make a list of ata, atb, atc, atd
        std::set<uint32_t> AtomsA, AtomsB, AtomsC, AtomsD;

        Xsparse<uint32_t>    AtomsAB, AtomsCD;
        Xsparse<float>   AtomsAC, AtomsBD, AtomsAD, AtomsBC;
    } ONXtensors;


    void InitONX(const b2screener & logFs, const b2screener & logDs);
    void CleanONX();


    //FUNCTIONS FOR GENERATING LISTS, PRESCREENING AND PACKING ERIs
    //*************************************************************

    //mode: 0 -> regular  1 -> only count 3-center ABAD integrals   2 -> discard 3-center ABAD integrals
    template<bool G, bool J, bool X, bool R, int mode> uint64_t PrescreenedList (float logD, float logS, const b2screener & logAD, const b2screener & logDs, uint64_t NINTS);
    template<bool G, bool J, bool X, bool R>           uint64_t PrescreenedListS(float logD, float logS, const b2screener & logAD, const b2screener & logDs, uint64_t NINTS);


    template<bool J, bool X>                   uint64_t PSCListS(float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS);
    template<bool J, bool X, int mode>         uint64_t PSCList (float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS);



    void GenerateCasePrescreenedList(float logD, float logS, const b2screener & logDs);

    template<bool G, int mode> uint64_t ONXPrescreenedList (float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS);
    template<bool G, int mode> uint64_t JPrescreenedList   (float logD, float logS,  const b2screener & logFs, const b2screener & logDs, uint64_t NINTS);

    void SortPrescreenedLists();
    void CaseSortPrescreenedLists();
    void ClearBlocks();
    void InitLists();
    void InitListsJ(const b2screener & logDs);
    template <int LEN> void InitBlocks();
    template <int LEN> void FuseIncompleteTiles();

    template <bool J, bool X>         void SetTilesUse (LibQuimera::ERITile64    * pTile, uint64_t Ntiles,  float logT, const b2screener & logDs);
    template <bool J, bool X,  int N> void SetTilesUse (LibQuimera::ERITile<N>  * pTile, uint64_t Ntiles,  float logT, const b2screener & logDs);


    //functions for prescreening
    template <int LEN> uint64_t GetNTiles() const;
    uint64_t MakeAllTiles(LibQuimera::ERITile64 * pTile);
    uint64_t MakeAllTiles(LibQuimera::ERITile32 * pTile);
    template <int N> uint64_t MakeAllTiles(LibQuimera::ERITile<N> * pTile);
    uint64_t MakeAllTilesTS(LibQuimera::ERITile<DOUBLES_PER_BLOCK> * pTile);



    void SetInitialList       (float lmo, bool noK4 = false);
    void SetInitialList_ABCD  (float lmo);
    void SetInitialList_ABCDs (float lmo);
    void SetInitialList_ABAB  (float lmo);
    void SetInitialList_AACD  (float lmo);
    void SetInitialList_AACC  (float lmo);
    void SetInitialList_AACCs (float lmo);

    bool Check3Center();

    //add several blocks to a list
    // J: add only Coulomb, X: only exchange, mode: 1: ABAB integrals, 2: ABCD integrals with potential center degeneracy, 0: all other kinds
    template<bool J, bool X, int mode> bool MakeBlocks      (float logD, float logS, const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);

    bool include(bool J, bool X, bool R, bool useJD, bool useJS, bool useXD, bool useXS);

    template<bool J, bool X, int mode> bool MakeBlocksN     (float logD, float logS, const b2screener & logAD,  const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);

    //functions and variables to be accessed publicly
  public:

    //chronometers
    //************
    uint64_t nJs;
    uint64_t nXs;

    uint64_t mJs;
    uint64_t mXs;

    double tJs;
    double tXs;

    double pJs;
    double pXs;


    ERIblock();
   ~ERIblock();

    void Init(size_t max_at_int, ERIbatchBuffer * ERIBB, const sparsetensorpattern * STP);              //very first initialization
    void Reset();
    const ERIblock & operator=(const BatchInfo & rhs); //sets the information for the next shellpair pair batch of integrals


    //FUNCTIONS FOR BLOCK EVALUATION OF ERIs
    //**************************************

    //general block generation
    bool MakeCoulomb   (float logD, float logS,  const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);
    bool MakeExchange  (float logD, float logS,  const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);
    bool MakeAll       (float logD, float logS,  const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);

    //generation of free vector blocks
    bool MakeCoulombN  (float logD, float logS,  const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);
    bool MakeExchangeN (float logD, float logS,  const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);
    bool MakeAllN      (float logD, float logS,  const b2screener & logADs, const b2screener & logDs, TaskQueue<ERIBatch> & TaskList);



    //OTHER FUNCTION EVALUATIONS
    //**************************
    cacheline64  CalcCauchySchwarz(const ShellPair & ABs, const ERIgeometries64 & eriG8, const LibQuimera::ERITile64 & ET, uint8_t lenk);
    float        CalcCauchySchwarz(const ShellPair & ABs, const ERIgeometry & eriG);

    void      ConstructW       (           const ShellPair & ABs, const ShellPair & CDs, const ERIgeometry & eriG, double * WWW, uint32_t wa, bool UseCase = false);
    void      ConstructWS      (double w, const ShellPair & ABs, const ShellPair & CDs, const ERIgeometry & eriG, double * WWW, uint32_t wa);
    void      ConstructWL      (double w, const ShellPair & ABs, const ShellPair & CDs, const ERIgeometry & eriG, double * WWW, uint32_t wa);
} __attribute__((aligned(64))); //don't wverlap in cache!



#endif
