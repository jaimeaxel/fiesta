


#ifndef __ROTATIONS__
#define __ROTATIONS__

#include "defs.hpp"
#include "math/affine.hpp"
#include "low/cache.hpp"


class AtomProd;

class ERIgeometries64;
class ERIgeometries32;
template<int N> class ERIgeometriesN;


class ERIgeometry {
  public:
    double ABz;
    double CDy;
    double CDz;
    vector3 ACv;

    double c;
    double s;

    void MakeRotation4  (const AtomProd & AP12, const AtomProd & AP34);
    void MakeRotation3  (const AtomProd & AP12, const AtomProd & AP34);
    void MakeRotation2S (const AtomProd & AP12);
    void MakeRotation2  (const point & A, const point & C);
    void MakeRotation1  (const point & A);

    void Adjust(bool invAB, bool invCD);
};

//dummy struct
struct ERIgeometries {
    cacheline ABz;
    cacheline CDy;
    cacheline CDz;
    cacheline ACx;
    cacheline ACy;
    cacheline ACz;
} __attribute__((aligned(CACHE_LINE_SIZE)));


void PackGeometries (const ERIgeometry     * vars,  ERIgeometriesN<DOUBLES_PER_BLOCK> & varsN);
void PackGeometries (const ERIgeometry     * vars,  ERIgeometries64   & vars8);
void PackGeometries (const ERIgeometry     * vars,  ERIgeometries32   & vars16);
void PackGeometries (const ERIgeometries64 * vars8, ERIgeometries32 & vars16);
void PackGeometries (const ERIgeometries64 & vars8, ERIgeometries32 & vars16);



class RotationMatrix {
  public:
    double PP[3][3];
    double DD[5][5];
    double FF[7][7];
    double GG[9][9];
    //double HH[11][11];

    void From(const RotationMatrix & rhs, const ERIgeometry & rot, int L);
    void From(const ERIgeometry & rot, int L);
    void From(const point & Ap, const point & Cp, int L);

    RotationMatrix() {
        for (int i=0; i<3; ++i) for (int j=0; j<3; ++j) PP[i][j] = 0;
        for (int i=0; i<5; ++i) for (int j=0; j<5; ++j) DD[i][j] = 0;
        for (int i=0; i<7; ++i) for (int j=0; j<7; ++j) FF[i][j] = 0;
        for (int i=0; i<9; ++i) for (int j=0; j<9; ++j) GG[i][j] = 0;
    }
};

class RotationMatrix;

//CUDA 5.5 can't handle SSE intrinsics properly
#ifndef __CUDACC__

#include "low/cache64.hpp"
#include "low/cache32.hpp"

struct RotationMatrices64 {
    cacheline64 PP [3*3];
    cacheline64 DD [5*5];
    cacheline64 FF [7*7];
    cacheline64 GG [9*9];
    //cacheline64 HH[11*11];

    void From (const RotationMatrix * RM, int L);
} __attribute__((aligned(CACHE_LINE_SIZE)));

struct RotationMatrices32 {
    cacheline32 PP [3*3];
    cacheline32 DD [5*5];
    cacheline32 FF [7*7];
    cacheline32 GG [9*9];
    //cacheline32 HH[11*11];

    void From (const RotationMatrix     * RM, int L);
    void From (const RotationMatrices64 * RM, int L);
} __attribute__((aligned(CACHE_LINE_SIZE)));

#else
struct RotationMatrices64;
struct RotationMatrices32;
#endif


#include "low/cacheN.hpp"
typedef cachelineN<DOUBLES_PER_BLOCK> cachelineNB;

struct RotationMatricesN {
    cachelineNB PP [3*3];
    cachelineNB DD [5*5];
    cachelineNB FF [7*7];
    cachelineNB GG [9*9];
    //cachelineNB HH[11*11];

    void From (const RotationMatrix * RM, int L);
};


template <int L>
struct RotationMatrixN {
    cachelineNB RR [(2*L+1)*(2*L+1)];

    void From (const RotationMatrix * RM) {

        const int NPB = DOUBLES_PER_BLOCK;

        if      (L==1) {
            for (int i=0; i< 3; ++i)
                for (int j=0; j< 3; ++j) {
                    int ij = 3*i+j;

                    for (int k=0; k<NPB; ++k)
                        RR[ij](k) = RM[k].PP[i][j];
                }
        }
        else if (L==2) {
            for (int i=0; i<5; ++i)
                for (int j=0; j<5; ++j) {
                    int ij = 5*i+j;
                    for (int k=0; k<NPB; ++k)
                        RR[ij](k) = RM[k].DD[i][j];
                }
        }
        else if (L==3) {
            for (int i=0; i<7; ++i)
                for (int j=0; j<7; ++j)  {
                    int ij = 7*i+j;
                    for (int k=0; k<NPB; ++k)
                        RR[ij](k) = RM[k].FF[i][j];
                }
        }
        else if (L==4) {
            for (int i=0; i<9; ++i)
                for (int j=0; j<9; ++j) {
                    int ij = 9*i+j;
                    for (int k=0; k<NPB; ++k)
                        RR[ij](k) = RM[k].GG[i][j];
                }
        }
        /*
        else if (L==5) {
            for (int i=0; i<11; ++i)
                for (int j=0; j<11; ++j) {
                    int ij = 11*i+j;
                    for (int k=0; k<NPB; ++k)
                        RR[ij](k) = RM[k].HH[i][j];
                }
        }
        */
    }

};


#endif
