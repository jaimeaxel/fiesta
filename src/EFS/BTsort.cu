//#include <iostream>
//using namespace std;

#include <inttypes.h>

typedef unsigned long long int ui64;

// sort the elements within the block
__forceinline__ __device__ void Sort2 (volatile ui64 * a, volatile ui64 * b) {

    volatile ui64 * pa =  a;
    volatile ui64 * pb =  b;

    register ui64 c = *pa;
    register ui64 d = *pb;

    *pa = min(c,d);
    *pb = max(c,d);
}

template <int L, bool I>
__forceinline__ __device__ void BTStep  (int ni, ui64 * list) {

    const int N = 1<<L; // resolved by the compiler

    int nb = ni>>L; // number of algorithm block
    int nl = ni&(N-1); // number of lane

    int base = nb<<(L+1);

    int i1 = base + nl;
    int i2;

    if (I) i2 = base + (2*N-1) - nl;
    else   i2 = base +    N    + nl;

    Sort2 (&list[i1],&list[i2]);

    //__syncthreads();
}

//block minus first step
template <int L> __forceinline__ __device__ void BTBlock        (int ni, ui64 * list) {
    BTStep  <L,false> (ni, list);
    __syncthreads();
    BTBlock <L-1>     (ni, list);
}
//end recursion
template <>      __forceinline__ __device__ void BTBlock <0>    (int ni, ui64 * list) {
    BTStep  <0,false> (ni, list);
    __syncthreads();
}


/*
//whole sorting network
template <int L> __forceinline__ __device__ void BTNetwork      (int ni, ui64 * list) {
    BTNetwork <L-1>    (ni, list);
    BTStep    <L,true> (ni, list);
    __syncthreads();
    BTBlock   <L-1>    (ni, list);
}
//end recursion
template <>      __forceinline__ __device__ void BTNetwork <0>  (int ni, ui64 * list) {
    BTStep <0,false>   (ni, list);
    __syncthreads();
}

*/

template <int L>  __forceinline__ __device__ void BTNetwork (int ni, ui64 * list) {

    // can't use warp-synchronous programming since sort2 is branch-divergent

    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<1) return;

    BTStep <1,true > (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<2) return;

    BTStep <2,true > (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<3) return;

    BTStep <3,true > (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<4) return;

    BTStep <4,true > (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<5) return;

    BTStep <5,true > (ni, list);    __syncthreads();
    BTStep <4,false> (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<6) return;

    BTStep <6,true > (ni, list);    __syncthreads();
    BTStep <5,false> (ni, list);    __syncthreads();
    BTStep <4,false> (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<7) return;

    BTStep <7,true > (ni, list);    __syncthreads();
    BTStep <6,false> (ni, list);    __syncthreads();
    BTStep <5,false> (ni, list);    __syncthreads();
    BTStep <4,false> (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<8) return;

    BTStep <8,true > (ni, list);    __syncthreads();
    BTStep <7,false> (ni, list);    __syncthreads();
    BTStep <6,false> (ni, list);    __syncthreads();
    BTStep <5,false> (ni, list);    __syncthreads();
    BTStep <4,false> (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<9) return;

    BTStep <9,true > (ni, list);    __syncthreads();
    BTStep <8,false> (ni, list);    __syncthreads();
    BTStep <7,false> (ni, list);    __syncthreads();
    BTStep <6,false> (ni, list);    __syncthreads();
    BTStep <5,false> (ni, list);    __syncthreads();
    BTStep <4,false> (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
    if (L<10) return;

    BTStep<10,true > (ni, list);    __syncthreads();
    BTStep <9,false> (ni, list);    __syncthreads();
    BTStep <8,false> (ni, list);    __syncthreads();
    BTStep <7,false> (ni, list);    __syncthreads();
    BTStep <6,false> (ni, list);    __syncthreads();
    BTStep <5,false> (ni, list);    __syncthreads();
    BTStep <4,false> (ni, list);    __syncthreads();
    BTStep <3,false> (ni, list);    __syncthreads();
    BTStep <2,false> (ni, list);    __syncthreads();
    BTStep <1,false> (ni, list);    __syncthreads();
    BTStep <0,false> (ni, list);    __syncthreads();
}


// perform steps in shared memory when possible
template <int L>
__global__ void BTAllNetworks (ui64 * list, ui64 nList) {

    const int N = 1024;

    __shared__ ui64 shared[2*N]; // 16K; no problem

    const int i = threadIdx.x;
    const int o = blockIdx.x *2*N;

    list += (blockIdx.y * nList);


    shared [    i] = shared [N + i] = -1; // maximum possible value

    //copy in
    if (  o+i<nList) shared [    i] = list[    o + i];
    if (N+o+i<nList) shared [N + i] = list[N + o + i];
    __syncthreads();

    //sort
    BTNetwork <L> (i, shared);
    __syncthreads();

    //move out
    if (  o+i<nList) list[    o + i] = shared [    i];
    if (N+o+i<nList) list[N + o + i] = shared [N + i];
}

template <int L>
__global__ void BTAllBlocks   (ui64 * list, ui64 nList) {

    const int N = 1024;

    __shared__ ui64 shared[2*N]; // 16K

    const int i = threadIdx.x;
    const int o = blockIdx.x *2*N;

    list += (blockIdx.y * nList);

    shared [    i] = shared [N + i] = -1; // maximum possible value

    //copy in
    if (  o+i<nList) shared [    i] = list[    o + i];
    if (N+o+i<nList) shared [N + i] = list[N + o + i];
    __syncthreads();

    //sort
    BTBlock <L> (i, shared);
    //__syncthreads();

    //move out
    if (  o+i<nList) list[    o + i] = shared [    i];
    if (N+o+i<nList) list[N + o + i] = shared [N + i];
}


template <int L>
__global__ void BTSuperBlocks (ui64 * list, ui64 nList) {

    const ui64 BOXSIZE = 1<<(L+1);

    const ui64 WARPSIZE  = 32;
    const ui64 NLANES    = 64;

    // divide the box
    const ui64 LANESIZE = BOXSIZE / NLANES;
    const ui64 NWARPS   = LANESIZE / WARPSIZE; // warps per lane

    const ui64 i = threadIdx.x;  // up to 32
    const ui64 j = threadIdx.y;  // up to 32
    const ui64 m = blockIdx.x;

    const ui64 nbox  = m/NWARPS;
    const ui64 nwarp = m%NWARPS;

    list += (blockIdx.y * nList);


    __shared__ ui64 shared[NLANES][WARPSIZE]; // <=8K

    shared[         j][i] = -1;
    shared[NLANES/2+j][i] = -1;

    const ui64 p1 = nbox*BOXSIZE +            j*LANESIZE + nwarp*WARPSIZE + i;
    const ui64 p2 = nbox*BOXSIZE + (j+NLANES/2)*LANESIZE + nwarp*WARPSIZE + i;

    if (p1<nList) shared[         j][i] = list[p1];
    if (p2<nList) shared[NLANES/2+j][i] = list[p2];

    __syncthreads();

    ui64 jj = j;
    ui64 TK = NLANES/2;

    #pragma unroll
    for (int l=0; l<6; ++l) {
        Sort2 (&shared[jj][i], &shared[TK+jj][i]);
        __syncthreads();
        TK /= 2;
        jj += (jj&TK)?TK:0;
    }

    //move out
    if (p1<nList) list[p1] = shared[         j][i];
    if (p2<nList) list[p2] = shared[NLANES/2+j][i];
}


// simplest (and least efficient) single-step kernel
template <int L, bool I>
__global__ void BTstep   (ui64 * list, ui64 nList) {

    const ui64 N = 1<<L; // resolved by the compiler

    const ui64 ni = blockIdx.x*blockDim.x + threadIdx.x;

    const ui64 nb = ni/N; // number of box in the algorithm
    const ui64 nl = ni%N; // number of lane in the box

    const ui64 BOXSIZE = 1<<(L+1);

    const ui64 base = BOXSIZE*nb;

    list += (blockIdx.y * nList);

    ui64 i1 = base +  nl;

    ui64 i2;
    if (I) i2 = base + (BOXSIZE-1) - nl;
    else   i2 = base + (BOXSIZE/2) + nl;

    if (i1>=nList) return;
    if (i2>=nList) return;

    //input
    ui64 a = list[i1];
    ui64 b = list[i2];

    //compare
    Sort2(&a,&b);

    //output
    list[i1] = a;
    list[i2] = b;
}


//classes allow partial template specialization, while functions don't
template<bool> struct Range;

template<int N, typename = Range<true> > class BT {
  public:
    static void BTblock   (ui64 * list, ui64 nList, cudaStream_t * stream);
    static void BTnetwork (ui64 * list, ui64 nList, cudaStream_t * stream);
};

template<int N> class BT<N, Range<(0 <= N && N <= 10)> > {
  public:
    static void BTblock   (ui64 * list, ui64 nList, cudaStream_t * stream, uint32_t nLists=1) {
        // compute the size and number of boxes for this step of the algorithm
        const int BOXSIZE = 1<<(N+1);
        const int nboxes  = (nList+BOXSIZE-1)/BOXSIZE;

        // assign number of threads
        const int nTotalThreads = nboxes*(BOXSIZE/2); // we only need half threads per box

        const int nTh = 1024;
        const int nBl = (nTotalThreads+nTh-1)/nTh;
        dim3 nBl2 (nBl,nLists);

        BTAllBlocks<N> <<<nBl2,nTh,0,*stream>>> (list, nList);
    }

    static void BTnetwork (ui64 * list, ui64 nList, cudaStream_t * stream, uint32_t nLists=1) {
        // compute the size and number of boxes for this step of the algorithm
        const int BOXSIZE = 1<<(N+1);
        const int nboxes  = (nList+BOXSIZE-1)/BOXSIZE;

        // assign number of threads
        const int nTotalThreads = nboxes*(BOXSIZE/2); // we only need half threads per box

        const int nTh = 1024;
        const int nBl = (nTotalThreads+nTh-1)/nTh;
        dim3 nBl2 (nBl,nLists);

        BTAllNetworks<N> <<<nBl2,nTh,0,*stream>>>  (list, nList);
    }
};

template<int N> class BT<N, Range<(10 < N)> > {
  public:
    static void BTblock   (ui64 * list, ui64 nList, cudaStream_t * stream, uint32_t nLists=1) {
        // compute the size and number of boxes for this step of the algorithm
        const int BOXSIZE = 1<<(N+1);
        const int nboxes  = (nList+BOXSIZE-1)/BOXSIZE;

        // assign number of threads
        const int nTotalThreads = nboxes*(BOXSIZE/2); // we only need half threads per box

        // contraction with 512 threads per block
        const int NX = 32;
        const int NY = 32;

        const int BX = nTotalThreads/1024;

        dim3 nBl(BX,nLists);
        dim3 nTh(NX,NY);

        BTSuperBlocks <N> <<<nBl,nTh,0,*stream>>> (list, nList);

        BT<N-6,Range<true> >::BTblock(list, nList, stream, nLists);
    }

    static void BTnetwork (ui64 * list, ui64 nList, cudaStream_t * stream, uint32_t nLists=1) {

        // compute the size and number of boxes for this step of the algorithm
        const int BOXSIZE = 1<<(N+1);
        const int nboxes  = (nList+BOXSIZE-1)/BOXSIZE;

        // assign number of threads
        const int nTotalThreads = nboxes*(BOXSIZE/2); // we only need half threads per box

        const int nTh = 1024;
        const int nBl = (nTotalThreads+nTh-1)/nTh;
        dim3 nBl2 (nBl,nLists);

        BT <N-1,Range<true> >::BTnetwork(list, nList, stream, nLists);
        BTstep <N,true> <<<nBl2,nTh,0,*stream>>> (list, nList);
        BT <N-1,Range<true> >::BTblock(list, nList, stream, nLists);
    }
};


void BTsort (uint64_t * ilist, uint64_t nList, cudaStream_t * stream) {

    ui64 * list = (ui64*) ilist;

    if (nList<2) return;

    ui64 LL = 0;
    for (ui64 n = nList-1; n>0; n/=2) ++LL;

    if      (LL== 1) BT < 0, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 2) BT < 1, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 3) BT < 2, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 4) BT < 3, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 5) BT < 4, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 6) BT < 5, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 7) BT < 6, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 8) BT < 7, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL== 9) BT < 8, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==10) BT < 9, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==11) BT <10, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==12) BT <11, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==13) BT <12, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==14) BT <13, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==15) BT <14, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==16) BT <15, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==17) BT <16, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==18) BT <17, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==19) BT <18, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==20) BT <19, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==21) BT <20, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==22) BT <21, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==23) BT <22, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==24) BT <23, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==25) BT <24, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==26) BT <25, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==27) BT <26, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==28) BT <27, Range<true> >::BTnetwork (list, nList, stream);
    else if (LL==29) BT <28, Range<true> >::BTnetwork (list, nList, stream);
    else {
        //cout << "Error: too many elements" << endl;
    }
}


void BTsort (uint64_t * ilist, uint64_t nList, uint32_t nLists, cudaStream_t * stream) {

    ui64 * list = (ui64*) ilist;

    if (nList<2) return;

    ui64 LL = 0;
    for (ui64 n = nList-1; n>0; n/=2) ++LL;

    if      (LL== 1) BT < 0, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 2) BT < 1, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 3) BT < 2, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 4) BT < 3, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 5) BT < 4, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 6) BT < 5, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 7) BT < 6, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 8) BT < 7, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL== 9) BT < 8, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==10) BT < 9, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==11) BT <10, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==12) BT <11, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==13) BT <12, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==14) BT <13, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==15) BT <14, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==16) BT <15, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==17) BT <16, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==18) BT <17, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==19) BT <18, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==20) BT <19, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==21) BT <20, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==22) BT <21, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==23) BT <22, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==24) BT <23, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==25) BT <24, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==26) BT <25, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==27) BT <26, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==28) BT <27, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else if (LL==29) BT <28, Range<true> >::BTnetwork (list, nList, stream, nLists);
    else {
        //cout << "Error: too many elements" << endl;
    }
}


