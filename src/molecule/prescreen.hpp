#ifndef __PRESCREEN__
#define __PRESCREEN__
#include <list>

#include "low/mem.hpp"
#include "low/memblock.hpp"
#include "math/affine.hpp"
#include "low/rtensors.hpp"

class Atom;
class point;
class Nucleus;
class GTObasis;

//lista de elementos mas tres coordenadas enteras que lo identifican en un espacio spaneado por un oct-tree
template<class T> struct CubeList {
    LinkedList<T> * list;
    int nx;
    int ny;
    int nz;
};

//esta estructura implementa un oct-tree de buckets, donde ir colocando los atomos
template<class T> struct BucketTree {
    //minimos y maximos del arbol
    int minx, maxx;
    int miny, maxy;
    int minz, maxz;

    //si es una hoja
    bool isleaf;

    //ramas
    BucketTree<T> * subtree000;
    BucketTree<T> * subtree001;
    BucketTree<T> * subtree010;
    BucketTree<T> * subtree011;
    BucketTree<T> * subtree100;
    BucketTree<T> * subtree101;
    BucketTree<T> * subtree110;
    BucketTree<T> * subtree111;

    //lista linkada de atomos incluidos en la rama (hoja)
    LinkedList<T> * bucket;
    LinkedList<T> * lastatom;

    int nBucket;

    //inicializa todo
    BucketTree() {
        subtree000 = NULL;
        subtree001 = NULL;
        subtree010 = NULL;
        subtree011 = NULL;
        subtree100 = NULL;
        subtree101 = NULL;
        subtree110 = NULL;
        subtree111 = NULL;

        bucket   = NULL;
        lastatom = NULL;
        nBucket  = 0;
    }

    void SetSize(const int & xmin, const int & xmax, const int & ymin, const int & ymax, const int & zmin, const int & zmax) {
        minx = xmin;
        miny = ymin;
        minz = zmin;
        maxx = xmax;
        maxy = ymax;
        maxz = zmax;

        if ((xmin == xmax) && (ymin == ymax) && (zmin == zmax))
            isleaf = true;
        else
            isleaf = false;
    }

    //a�ade un atomo al bucket correspondiente
    void AddElem(T pRet, const int & nx, const int & ny, const int & nz) {

        //si es una hoja...
        if (isleaf) {
            ++nBucket;
            if (bucket == NULL) {
                bucket = new LinkedList<T>;
                lastatom = bucket;
            } else {
                lastatom->next = new LinkedList<T>;
                lastatom = lastatom->next;
            }
            lastatom->next = NULL;

            lastatom->key = pRet;
            return;
        }

        //selecciona la sub-rama
        int mx = (maxx+minx <0)?(maxx+minx-1)/2:(maxx+minx)/2;
        int my = (maxy+miny <0)?(maxy+miny-1)/2:(maxy+miny)/2;
        int mz = (maxz+minz <0)?(maxz+minz-1)/2:(maxz+minz)/2;

        bool sx = (nx > mx);
        bool sy = (ny > my);
        bool sz = (nz > mz);


        BucketTree<T> ** pSTree;
        if      (!sx && !sy && !sz) pSTree = &subtree000;
        else if (!sx && !sy &&  sz) pSTree = &subtree001;
        else if (!sx &&  sy && !sz) pSTree = &subtree010;
        else if (!sx &&  sy &&  sz) pSTree = &subtree011;
        else if ( sx && !sy && !sz) pSTree = &subtree100;
        else if ( sx && !sy &&  sz) pSTree = &subtree101;
        else if ( sx &&  sy && !sz) pSTree = &subtree110;
        else if ( sx &&  sy &&  sz) pSTree = &subtree111;

        //si es una rama nueva la crea
        if ((*pSTree) == NULL) {
            (*pSTree) = new BucketTree<T>;
            int nmaxx, nminx, nmaxy, nminy, nmaxz, nminz;

            if (sx) {
                nmaxx = maxx;
                nminx = mx + 1;
            }
            else {
                nmaxx = mx;
                nminx = minx;
            }

            if (sy) {
                nmaxy = maxy;
                nminy = my + 1;
            }
            else {
                nmaxy = my;
                nminy = miny;
            }

            if (sz) {
                nmaxz = maxz;
                nminz = mz + 1;
            }
            else {
                nmaxz = mz;
                nminz = minz;
            }

            (*pSTree)->SetSize(nminx, nmaxx, nminy, nmaxy, nminz, nmaxz);
        }

        //a�ade el atomo a la subrama
        (*pSTree)->AddElem(pRet, nx, ny, nz);

    }

    //devuelve un bucket
    //si no lo encuentra devuelve un puntero nulo
    LinkedList<T> * RetrieveBucket(const int & nx, const int & ny, const int & nz) {

        //comprueba la ultima rama
        if (isleaf) {
            if ((nx==minx) && (ny==miny) && (nz==minz)) return bucket;
            else return NULL;
        }

        //selecciona la sub-rama
        int mx = (maxx+minx <0)?(maxx+minx-1)/2:(maxx+minx)/2;
        int my = (maxy+miny <0)?(maxy+miny-1)/2:(maxy+miny)/2;
        int mz = (maxz+minz <0)?(maxz+minz-1)/2:(maxz+minz)/2;

        bool sx = (nx > mx);
        bool sy = (ny > my);
        bool sz = (nz > mz);

        //mejorable con tres ifs
        BucketTree * pSTree;
        if (sx) {
            if (sy) {
                if (sz) pSTree = subtree111;
                else    pSTree = subtree110;
            }
            else {
                if (sz) pSTree = subtree101;
                else    pSTree = subtree100;
            }
        }
        else {
            if (sy) {
                if (sz) pSTree = subtree011;
                else    pSTree = subtree010;
            }
            else {
                if (sz) pSTree = subtree001;
                else    pSTree = subtree000;
            }
        }


        if (pSTree == NULL) return NULL;
        else return pSTree->RetrieveBucket(nx,ny,nz);
    }

    //devuelve una lista linkada de buckets
    LinkedList < CubeList<T> > * TreeList (LinkedList < CubeList<T> > * last) {

        //si es un nodo devuelve el linkedlist mas las coordenadas
        if (isleaf) {
            //std::cout << minx << " " << miny << " " << minz << std::endl;
            LinkedList < CubeList<T> > * ret = new LinkedList < CubeList<T> >;
            ret->key.list = bucket;
            ret->key.nx = minx;
            ret->key.ny = miny;
            ret->key.nz = minz;
            ret->next = last;
            return ret;
        }



        //calcula y enlaza las ocho sublistas (esto se hace asi para poder ser paralelizado)
        LinkedList < CubeList<T> > * plast = last;
        if (subtree111 != NULL) plast = subtree111->TreeList(plast);
        if (subtree110 != NULL) plast = subtree110->TreeList(plast);
        if (subtree101 != NULL) plast = subtree101->TreeList(plast);
        if (subtree100 != NULL) plast = subtree100->TreeList(plast);
        if (subtree011 != NULL) plast = subtree011->TreeList(plast);
        if (subtree010 != NULL) plast = subtree010->TreeList(plast);
        if (subtree001 != NULL) plast = subtree001->TreeList(plast);
        if (subtree000 != NULL) plast = subtree000->TreeList(plast);

        return plast;
    }

    //destructor!!!
    ~BucketTree() {
        //primero se carga las subramas
        if (subtree000!= NULL) delete subtree000;
        if (subtree001!= NULL) delete subtree001;
        if (subtree010!= NULL) delete subtree010;
        if (subtree011!= NULL) delete subtree011;
        if (subtree100!= NULL) delete subtree100;
        if (subtree101!= NULL) delete subtree101;
        if (subtree110!= NULL) delete subtree110;
        if (subtree111!= NULL) delete subtree111;

        //ahora la lista enlazada
        LinkedList<T> * tmp;
        while (bucket!=NULL) {
            tmp = bucket;
            bucket = bucket->next;
            delete tmp;
        }

    }

};

class Interactions {
  private:


  public:
    Array<Queue<int> > List;


    void PrescreenInteractions        (                const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO);
    void PrescreenInteractionsZorder  (Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO); // 3D z order curve
    void PrescreenInteractionsZ       (Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO); // ordered by z component
    void PrescreenInteractionsC       (Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO); // synthetic method
    void PrescreenInteractionsGSC     (Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO); // Graph spectral clustering


    void SortList();
    void CuthillMcKee(Array<int> & R);
    void From (const Interactions & IL, const Array<int> & R);

    int GetTotN() const;

    int Getn() const;
    int GetIntn(int i) const;

    Interactions();
    Interactions &     operator=  (const Interactions & rhs);
    Interactions &     operator+= (const Interactions & rhs);
    const Interactions operator*  (const Interactions & rhs) const;
    const Interactions Transpose  () const;
    ~Interactions();
};

Array<int> * PrescreenInteractions(point & vmin, point & vmax, Nucleus * nuclei, int nnuclei, const double bucketSize);
Array<int> * PrescreenInteractions(Nucleus * (&nuclei), int nnuclei, const Array<int> & ATypeList, const GTObasis & BasisSet, double logminover);

double CalcBucketSize (Nucleus * nuclei, int nnuclei, const GTObasis & BasisSet, const double minover);



template<class T> struct Box {
    std::list<T> elements;

    double J() const;
};

template<class T> struct Boxing {
    vector3 vmin;
    vector3 vmax;

    double L; //length of block

    //number of blocks in each direction
    int Lx;
    int Ly;
    int Lz;

    r3tensor<Box<T> > boxes;

    static void InitFrom(const Nucleus * nuclei, int nnuclei);

    void InitFrom(const Nucleus * nuclei, int nnuclei, double ll);


    const Box<T> * GetBox(const point & P) const;
    void RetrieveBoxes(const point & P, const Box<T> * TheBoxes[8]) const;


    double CalcJ() const;
    void   PrintPop() const;
};

union intdoub {
    double w;
    int n[2];
};

struct PW {
    point   P; //point
    intdoub w; //weight of some sort
};

struct PWBox {
    std::list<PW> elements;
};

struct PWBoxing {
    vector3 vmin;
    vector3 vmax;

    double L; //length of block

    //number of blocks in each direction
    int Lx;
    int Ly;
    int Lz;

    r3tensor<PWBox> boxes;

    void          InitFrom      (const PW * nuclei, int n, double ll);
    const PWBox * GetBox        (const point & P) const;
    void          RetrieveBoxes (const point & P, const PWBox * TheBoxes[8]) const;
    void          PrintPop      () const;
};


#endif
