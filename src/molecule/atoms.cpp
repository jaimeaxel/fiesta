/*
    atoms.cpp

    definicion de nucleo; lista de definiciones atomicas
*/

#include <sstream>
#include "defs.hpp"
#include "fiesta/fiesta.hpp"
#include "molecule/atoms.hpp"
#include "basis/atomdef.hpp"
using namespace std;


Nucleus & Nucleus::operator=(const Nucleus & rhs) {
    name = rhs.name;
    num  = rhs.num;
    mass = rhs.mass;
    Z    = rhs.Z;
    c    = rhs.c;
    type = rhs.type;
    intc = rhs.intc;
    cart = rhs.cart;
    id   = rhs.id;
    return *this;
}

Nucleus & Nucleus::operator=(const Atom & rhs) {
    num  = rhs.num;
    c    = rhs.c;
    type = rhs.type;
    intc = rhs.intc;
    cart = rhs.cart;
    id   = -1;

    name = Fiesta::AtomDefs[type].name;
    mass = Fiesta::AtomDefs[type].mass;
    Z    = Fiesta::AtomDefs[type].Z;

    return *this;
}

