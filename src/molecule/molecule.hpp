#ifndef __MOLECULE__
#define __MOLECULE__

#include "low/mem.hpp"
#include "molecule/edata.hpp"
#include "basis/atomprod.hpp"

class Nucleus;
class GTObasis;
class Atom;
class GTObasis;
class System;


//molecula: contiene informacion redundante
struct Molecule {
    int nAtoms;         //numero de atomos en el sistema

    // TODO: remove redundancy
    Nucleus * Nuclei;   //lista de nucleos en el sistema
    rAtom   * Atoms;    //redundant but useful right now
    Array<Atom> mAtoms;


    rAtomPrototypes InternalDefs;
    MolElData ElectronData;

    Array<int> Relabel;


    int charge;
    int multiplicity;

    int nElec;          // total electrons
    int nEla;           // spin alpha electrons
    int nElb;           // spin beta  electrons

    double ENuc;        // nuclear Coulomb energy

    void SetCharge(int c);
    void SetMultiplicity(int m);
    void mSumElectrons();
    void Center();
    void From(const Queue<Atom> & AtomPool);

    Molecule ();

    void From(const GTObasis & BasisSet);
    void SumElectrons();

    std::vector<double> GetCenterOfMass();

    void CenterMol  ();
    void PrintMol   ();
    void SortAtoms  (const Array<Atom> & mAtoms, const GTObasis & gBasis, double logminover);

    void CalcNuclear  ();
    void CalcNuclearX ();

    void WriteCube(GTObasis&);
    Molecule & operator=(const Molecule & mol);

    ~Molecule ();
};


#endif
