#ifndef __GEOMETRY__
#define __GEOMETRY__

class point;
class icoord;

point Icoord2vec(const icoord & ic, const point & at2, const point & at3, const point & at4);
icoord vec2icoord (const point & at1, const point & at2, const point & at3, const point & at4);
#endif
