/*
    prescreen.cpp

    rutinas para obtener solo las interacciones atomicas relevantes
*/

#include <set>
#include <cmath>
#include "defs.hpp"
#include "fiesta/fiesta.hpp"
#include "basis/GTO.hpp"
#include "molecule/atoms.hpp"
#include "low/mem.hpp"
#include "low/less.hpp"
#include "low/io.hpp"
#include "linear/eigen.hpp"
#include "molecule/prescreen.hpp"
#include "fiesta/calculate.hpp"

using namespace std;
using namespace Fiesta;

struct intpair {
    int atom;
    int nnodes;
};


//ordena los atomos en diferentes cubos en O(N log N); devuelve una lista de atomos ordenada
Array<int> * PrescreenInteractions(point & vmin, point & vmax, Nucleus * nuclei, int nnuclei, const double bucketSize) {

    //busca los atomos mas alejados del centro
    double & maxx = vmax.x;
    double & minx = vmin.x;
    double & maxy = vmax.y;
    double & miny = vmin.y;
    double & maxz = vmax.z;
    double & minz = vmin.z;

    maxx = minx = nuclei[0].c.x;
    maxy = miny = nuclei[0].c.y;
    maxz = minz = nuclei[0].c.z;

    for (int i=1; i<nnuclei; ++i) {
        if (nuclei[i].c.x < minx) minx = nuclei[i].c.x;
        if (nuclei[i].c.x > maxx) maxx = nuclei[i].c.x;

        if (nuclei[i].c.y < miny) miny = nuclei[i].c.y;
        if (nuclei[i].c.y > maxy) maxy = nuclei[i].c.y;

        if (nuclei[i].c.z < minz) minz = nuclei[i].c.z;
        if (nuclei[i].c.z > maxz) maxz = nuclei[i].c.z;
    }

    //calcula los cubos
    int nminx, nmaxx, nminy, nmaxy, nminz, nmaxz;

    nminx = int(minx/bucketSize + 0.5);
    nmaxx = int(maxx/bucketSize + 0.5);
    nminy = int(miny/bucketSize + 0.5);
    nmaxy = int(maxy/bucketSize + 0.5);
    nminz = int(minz/bucketSize + 0.5);
    nmaxz = int(maxz/bucketSize + 0.5);


    //crea un oct-tree de atomos y mete cada atomo en el bucket correspondiente
    BucketTree< int > AtomTree;
    AtomTree.SetSize(nminx, nmaxx, nminy, nmaxy, nminz, nmaxz);

    for (int i=0; i<nnuclei; ++i) {
        int nx = int(nuclei[i].c.x/bucketSize + 0.5);
        int ny = int(nuclei[i].c.y/bucketSize + 0.5);
        int nz = int(nuclei[i].c.z/bucketSize + 0.5);
        AtomTree.AddElem(i, nx, ny, nz);
    }

    //crea una lista de potenciales interacciones de cada atomo con los atomos cercanos
    //*********************************************************************************

    //lista de cubos del sistema
    LinkedList < CubeList<int> > * Interactions = AtomTree.TreeList(NULL);

    LinkedList <int> ** IntListFirst = new LinkedList<int> * [nnuclei];
    LinkedList <int> ** IntListLast  = new LinkedList<int> * [nnuclei];

    for (int i=0; i<nnuclei; ++i) {
        IntListFirst[i] = NULL;
        IntListLast[i]  = NULL;
    }


    while (Interactions != NULL) {
        //para cada atomo del cubo
        LinkedList<int> * pAtom = Interactions->key.list; //puntero al primer key de atomo

        while (pAtom != NULL) {

            //escanea todos los cubos alrededor de la posicion
            for (int mx=-1; mx<=1; ++mx) {
                for (int my=-1; my<=1; ++my) {
                    for (int mz=-1; mz<=1; ++mz) {
                        //devuelve un linked list de punteros a los atomos
                        LinkedList<int> * thebucket = AtomTree.RetrieveBucket(Interactions->key.nx+mx, Interactions->key.ny+my, Interactions->key.nz+mz);


                        //a�ade todos los elementos a la lista de interacciones para ese atomo
                        while (thebucket != NULL) {
                            if (IntListFirst[pAtom->key] == NULL) {
                                IntListFirst[pAtom->key] = new LinkedList<int>;
                                IntListLast [pAtom->key] = IntListFirst [pAtom->key];
                            }
                            else {
                                IntListLast[pAtom->key]->next = new LinkedList<int>;
                                IntListLast [pAtom->key] = IntListLast[pAtom->key]->next;
                            }

                            IntListLast[pAtom->key]->next = NULL;
                            IntListLast[pAtom->key]->key  = thebucket->key;

                            thebucket = thebucket->next;
                        }

                    }
                }
            }

            //pasa al siguiente atomo; el linked list de atomos es destruido junto
            //con el arbol, que que es en realidad un puntero
            pAtom = pAtom->next;
        }

        //borra el elemento y pasa al siguiente
        LinkedList < CubeList<int> > * tmpint = Interactions;
        Interactions = Interactions->next;
        delete tmpint;
    }



    //ahora debe liberar la memoria:
    //  BucketTree<int> AtomTree ya se lo carga el destructor


    //pasa todos los datos
    Array<int> * List = new Array<int>[nnuclei];

    for (int i=0; i<nnuclei; ++i) {
        int n=0;
        LinkedList<int> * last = IntListFirst[i];

        while (last != NULL) {
            ++n;
            last = last->next;
        }

        List[i].set(n);
        last = IntListFirst[i];

        for (int j=0; j<n; ++j) {
            List[i][j] = last->key;
            LinkedList<int> * tmp = last;
            last = last->next;
            delete tmp; // borra el elemento de la lista
        }
    }

    //ordena la lista de atomos por numero de entrada
    for (int i=0; i<nnuclei; ++i)
        RadixSort(List[i].keys, List[i].n);


    //borra las listas
    delete[] IntListFirst;
    delete[] IntListLast;

	return List;
}

//chapuza para que pare de quejarse el compilador;
//usar stdlib


//prescrinea en O(N^2) por el momento; un 3D bucket sort previo deberia reducirlo a O(N)
//Cuthill-McKee bandwith reduction algorithm
Array<int> * PrescreenInteractions(Nucleus * (&nuclei), int nnuclei, const Array<int> & ATypeList, const GTObasis & BasisSet, double logminover) {
    double * iminK = new double[ATypeList.n];

    for (int i=0; i<ATypeList.n; ++i) {
        int id1 = ATypeList[i];

        double min = BasisSet[id1][0].k[0];

        for (int b1=0; b1<BasisSet[id1].n; ++b1) {
            const GTO & gto = BasisSet[id1][b1];
            for (int g=0; g<gto.K; ++g)
                if (gto.k[g]<min) min = gto.k[g];
        }

        iminK[i] = 1/min;
    }


    //
    Array< Queue<int> > List(nnuclei);

    for (int at1=0; at1<nnuclei; ++at1) {
		int t1 = ATypeList.search(nuclei[at1].type);

        for (int at2=0; at2<nnuclei; ++at2) {
            int t2 = ATypeList.search(nuclei[at2].type);

            vector3 r = nuclei[at2].c - nuclei[at1].c;
            double R2 = r*r;

            //adds atom to queue
            if (R2<=logminover*(iminK[t1]+iminK[t2])) {
                List[at1].PushBack(at2);
            }
        }
    }


    //Cuthill-McKee bandwith reduction algorithm

    //Step 0: Prepare an empty queue Q and an empty result array R.
    Array<int>  R(nnuclei);
    //Array<bool> R(nnuclei);
    Queue<int> Q; Q.root = NULL;
    int lastR = 0;


    for (int at1=0; at1<nnuclei; ++at1) {
        Messenger << at1 << "    " << List[at1] << endl;
    }


    //Step 1: Select the node in G(n) with the lowest degree (ties are broken arbitrarily) that hasn't previously been inserted in the result array. Let us name it P (for Parent).
    while (lastR != nnuclei-1) {
        int min;
        int imin;

        for (int i=0; i<nnuclei; ++i) {
            if (!R.isin(i,lastR)) {
                imin = i;
                break;
            }
        }
        min = List[imin].n;


        for (int i=imin+1; i<nnuclei; ++i) {
            if (!R.isin(i,lastR)) {
                if (List[i].n < min) {
                    imin = i;
                    min = List[imin].n;
                }
            }
        }

        //Step 2: Add P in the first free position of R.
        R[lastR] = imin;

        //Step 3: Add to the queue all the nodes adjacent with P in the increasing order of their degree.
        Array<intpair> nodes(nnuclei);
        int nnodes;

        nnodes = List[R[lastR]].n;

        //copies all nodes of R[last] to an auxiliary array
        DLinkedList<int> * thenode = List[R[lastR]].last;
        for (int i=0; i<nnodes; ++i) {
            nodes[i].atom   = thenode->key;
            nodes[i].nnodes = List[nodes[i].atom].n;
            thenode = thenode->prev;
        }

        //sorts the array by number of nodes
        for (int i=0; i<nnodes; ++i) {
            int imin = i;
            int nmin = nodes[i].nnodes;

            for (int j=i+1; j<nnodes; ++j) if(nodes[i].nnodes<nmin) {imin=j; nmin = nodes[i].nnodes;}

            //swaps
            int tmp;
            tmp = nodes[i].atom;   nodes[i].atom   = nodes[imin].atom;   nodes[imin].atom   = tmp;
            tmp = nodes[i].nnodes; nodes[i].nnodes = nodes[imin].nnodes; nodes[imin].nnodes = tmp;
        }

        //dumps the array into the queue
        for (int i=0; i<nnodes; ++i)
            Q.PushBack(nodes[i].atom);

        while (!Q.IsVoid()) {
            //Step 4.1: Extract the first node from the queue and examine it. Let us name it C (for Child).
            int C = Q.PopBack();

            //Step 4.2: If C hasn't previously been inserted in R, add it in the first free position and add to Q all the neighbours of C that are not in R in the increasing order of their degree.
            if (!R.isin(C,lastR+1)) {
                ++lastR;
                R[lastR] = C;

                nnodes = List[R[lastR]].n;
                //copies all nodes of R[last] to an auxiliary array
                DLinkedList<int> * thenode = List[R[lastR]].last;
                for (int i=0; i<nnodes; ++i) {
                    nodes[i].atom   = thenode->key;
                    nodes[i].nnodes = List[nodes[i].atom].n;
                    thenode = thenode->prev;
                }
                //sorts the array by number of nodes
                for (int i=0; i<nnodes; ++i) {
                    int imin = i;
                    int nmin = nodes[i].nnodes;
                    for (int j=i+1; j<nnodes; ++j) if(nodes[i].nnodes<nmin) {imin=j; nmin = nodes[i].nnodes;}

                    //swaps
                    int tmp;
                    tmp = nodes[i].atom;   nodes[i].atom   = nodes[imin].atom;   nodes[imin].atom   = tmp;
                    tmp = nodes[i].nnodes; nodes[i].nnodes = nodes[imin].nnodes; nodes[imin].nnodes = tmp;
                }

                //dumps the array into the queue
                for (int i=0; i<nnodes; ++i)
                    Q.PushBack(nodes[i].atom);
            }

            //Step 5: If Q is not empty repeat from Step 4.1
        }

    //Step 6: If there are unexplored nodes (the graph is not connected) repeat from Step 1
    }

    for (int at1=0; at1<nnuclei; ++at1) {
        Messenger << R[at1] << "  ";
    }
    Messenger << endl;

    //restructure nuclei list
    Nucleus * nuclei2 = new Nucleus[nnuclei];
    for (int i=0; i<nnuclei; ++i) {
        nuclei2[i] = nuclei[R[i]];
    }
    delete[] nuclei;
    nuclei = nuclei2;

    Array<int> * IL = new Array<int>[nnuclei];

    for (int i=0; i<nnuclei; ++i) {
        int oi = R[i];
        IL[oi].set(List[oi].n);

        //copies the result
        DLinkedList<int> * thep = List[oi].root;
        for (int j=0; j<List[oi].n; ++j) {
            IL[oi][j] = R[thep->key];
            thep = thep->next;
        }

        //sorts the atomic interactions by atom index
        for (int j=0; j<IL[oi].n;++j) {
            int jmin = j;
            int amin = IL[oi][j];
            for (int k= j+1; k<IL[oi].n; ++k)
                if (IL[oi][k]<amin) {amin=IL[oi][k]; jmin = k;}
            swap(IL[oi][j], IL[oi][jmin]);
        }
    }

    return IL;
}


//calcula el tama�o maximo del cubo en funcion de las FBs presentes
double CalcBucketSize (Nucleus * nuclei, int nnuclei, const GTObasis & BasisSet, const double minover) {
/*
    bool * Tested = new bool[AtomDefs.nAtomDefs];

    for (int i=0; i<AtomDefs.nAtomDefs; ++i)
        Tested[i] = false;

    double minr = 0;

    //mira todos los atomos de distinto tipo que aparecen en la molecula
    for (int at=0; at<nnuclei; ++at) {
        if (Tested[nuclei[at].type]) continue; //se salta el atomo
        Tested[nuclei[at].type] = 1;

        //calcula
		int id = nuclei[at].type;
        for (int ngto=0; ngto<BasisSet[id].n; ++ngto) {
            const GTO & gto = BasisSet[id][ngto];

            for (int i=0; i<gto.K; ++i) {
                double A = -2*log(minover * pow(2*gto.k[i], double(2*gto.l)+1.5) / (gto.N[i]*gto.N[i]) ) / gto.k[i];

                double r = sqrt(A);

                if (gto.k[i]*r>1.)
                    for (int q=0; q<5; ++q) {
                        double B = 4*gto.l * log(gto.k[i]*r) / gto.k[i];
                        r = sqrt(A+B);
                    }

                //compara el valor
                if (r>minr) minr = r;

          }
        }

    }
    delete[] Tested;

    return minr;
*/
    return 0;
}


void Interactions::PrescreenInteractions(const Array<Atom> & Atoms, const GTObasis & BasisSet, double logminover) {

    //this can be done much better, but whatever,  it's still O(N)
    double * iminK = new double[Atoms.n];

    for (int i=0; i<Atoms.n; ++i) {
        int id1 = Atoms[i].type;

        float kmin = (BasisSet[id1])[0].k[0];

        for (int b1=0; b1<BasisSet[id1].n; ++b1) {
            const GTO & gto = BasisSet[id1][b1];
            if (gto.k[0]<kmin) kmin = gto.k[0];
        }

        iminK[i] = 1./kmin;
    }


    //O(N^2); not very important at this point
    List.set(Atoms.n);

    for (int at1=0; at1<Atoms.n; ++at1) {
        for (int at2=0; at2<Atoms.n; ++at2) {
            vector3 r = Atoms[at2].c - Atoms[at1].c;
            double R2 = r*r;

            //adds atom to queue
            if (R2<=logminover*(iminK[at1]+iminK[at2])) {
                List[at1].PushBack(at2);
            }
        }
    }

    delete[] iminK;
}


void Interactions::PrescreenInteractionsZorder(Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO) {

    //this can be done much better, but whatever,  it's still O(N)
    double * iminK = new double[Atoms.n];

    double imax = 0;

    point pmin, pmax;
    pmin = Atoms[0].c;
    pmax = Atoms[0].c;

    for (int i=0; i<Atoms.n; ++i) {
        int id1 = Atoms[i].type;

        float kmin = (BasisSet[id1])[0].k[0];

        for (int b1=0; b1<BasisSet[id1].n; ++b1) {
            const GTO & gto = BasisSet[id1][b1];
            if (gto.k[0]<kmin) kmin = gto.k[0];
        }

        iminK[i] = 1./kmin;

        imax = max(imax, iminK[i]);

        pmin.x = min (pmin.x, Atoms[i].c.x);
        pmin.y = min (pmin.y, Atoms[i].c.y);
        pmin.z = min (pmin.z, Atoms[i].c.z);

        pmax.x = max (pmax.x, Atoms[i].c.x);
        pmax.y = max (pmax.y, Atoms[i].c.y);
        pmax.z = max (pmax.z, Atoms[i].c.z);
    }

    double W = sqrt(2*logGDO*imax);

    vector3 Dist = pmax-pmin;
    int Dx = int(Dist.x / W);
    int Dy = int(Dist.y / W);
    int Dz = int(Dist.z / W);

    int D = max(max(Dx,Dy), Dz);

    //find next power of two
    D |= D >> 1;   // Divide by 2^k for consecutive doublings of k up to 32,
    D |= D >> 2;   // and then or the results.
    D |= D >> 4;
    D |= D >> 8;
    D |= D >> 16;
    ++D;

    //make the box
    vector3 AD;
    AD.x = 0.5*(D*W - Dist.x);
    AD.y = 0.5*(D*W - Dist.y);
    AD.z = 0.5*(D*W - Dist.z);

    pmin -= AD;
    pmax += AD;

    r3tensor <stack<int> > boxing(D,D,D);

    //put the atoms
    for (int i=0; i<Atoms.n; ++i) {
        vector3 cube;

        cube = (Atoms[i].c - pmin)/W;

        int nx = int(cube.x);
        int ny = int(cube.y);
        int nz = int(cube.z);

        boxing(nx,ny,nz).push(i);
    }


    R.set(Atoms.n);
    int nn=0;

    for (int N=0; N<D*D*D; ++N) {
        int nx, ny, nz;
        nx = ny = nz = 0;

        //deinterleave the coordinates for a 3D Z-ordering curve

        for (int n=0; n<10; ++n) {
            nx |= (N&(1<<(3*n+0)))?1<<n:0;
            ny |= (N&(1<<(3*n+1)))?1<<n:0;
            nz |= (N&(1<<(3*n+2)))?1<<n:0;
        }

        while(!boxing(nx,ny,nz).empty()) {
            R[nn] = boxing(nx,ny,nz).top();
            boxing(nx,ny,nz).pop();
            ++nn;
        }

    }



    //O(N^2); not very important at this point
    List.set(Atoms.n);

    for (int at1=0; at1<Atoms.n; ++at1) {
        for (int at2=0; at2<Atoms.n; ++at2) {
            vector3 r = Atoms[at2].c - Atoms[at1].c;
            double R2 = r*r;

            //adds atom to queue
            if (R2<=logGDO*(iminK[at1]+iminK[at2])) {
                List[at1].PushBack(at2);
            }
        }
    }

    delete[] iminK;
}


void Interactions::PrescreenInteractionsZ(Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO) {

    PrescreenInteractions(Atoms, BasisSet, logGDO);


    R.set(Atoms.n);

    for (int at1=0; at1<Atoms.n; ++at1) R[at1] = at1;

    for (int at1=0; at1<Atoms.n; ++at1) {
        int ats = at1;

        for (int at2=at1+1; at2<Atoms.n; ++at2) {
            if (Atoms[R[at2]].c.z < Atoms[R[ats]].c.z) {
                ats = at2;
            }
        }

        swap (R[at1], R[ats]);
    }

}


void Interactions::PrescreenInteractionsC(Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO) {

    PrescreenInteractions(Atoms, BasisSet, logGDO);




    //counts the coincidences between atoms
    set<int> * inter = new set<int>[Atoms.n];

    for (int at1=0; at1<Atoms.n; ++at1) {
        DLinkedList<int> * r;
        for (r=List[at1].root; r!=NULL; r=r->next) inter[at1].insert(r->key);
    }


    R.set(Atoms.n);

    /* order the atoms by z order */  {
        for (int at1=0; at1<Atoms.n; ++at1) R[at1] = at1;

        for (int at1=0; at1<Atoms.n; ++at1) {
            int ats = at1;

            for (int at2=at1+1; at2<Atoms.n; ++at2)
                if (Atoms[R[at2]].c.z < Atoms[R[ats]].c.z) ats = at2;

            swap (R[at1], R[ats]);
        }
    }




    map<int, int> block;
    deque<int> Latoms; for (int at=0; at<Atoms.n; ++at) Latoms.push_back(R[at]);


    int ipos = 0;
    int nblock = 0;
    bool isnew = true;

    /*
        * take into account also the coincidences between the 0 values
        * use the tails from the previous blocks
        * make all this using blocks
        * use springs with different constant forces
    */

    while (Latoms.size() !=0) {

        //new block
        if (isnew) {
            int at1 = Latoms.front();

            //copy the block
            set<int>::iterator it;
            for(it=inter[at1].begin(); it!=inter[at1].end(); ++it) block[*it] = 1;

            isnew = false;
            R[ipos] = at1;
            ++ipos;

            Latoms.pop_front();

            nblock = 0;
        }

        else {
            ++nblock;

            int maxcc = 0;
            deque<int>::iterator itmax = Latoms.begin();

            // find the atom with highest overlap
            deque<int>::iterator itc;

            // count coincidences
            for(itc=Latoms.begin(); itc!=Latoms.end(); ++itc) {
                int at2 = *itc;
                int cc  =    0;

                map<int, int>::iterator itcc;
                for(itcc=block.begin(); itcc!=block.end(); ++itcc)
                    if (inter[at2].count(itcc->first)) cc += itcc->second;

                if (cc>maxcc) {
                    maxcc = cc;
                    itmax = itc;
                }
            }

            //some coincidence
            if ( float(maxcc) >= 0.25*float(nblock*block.size()) ) {
                int atmax = *itmax;

                // make the intersection of block with at2
                set<int>::iterator it;
                for (it=inter[atmax].begin(); it!=inter[atmax].end(); ++it) {
                    if (block.count(*it)) ++block[*it];
                    else                   block[*it] = 1;
                }

                // delete the best match in the atoms list
                Latoms.erase(itmax);

                // put the best match in the labels
                R[ipos] = atmax;
                ++ipos;
            }

            //end of block
            else {
                block.clear();
                isnew = true;
            }

        }
    }

    delete[] inter;

    cout << endl;


    for (int at=0; at<Atoms.n; ++at) cout << at << " " << R[at] << endl;
    cout << endl;

}





void GraphSpectralSort(const tensor2 & W, int * R, int * S, int N, int NS, bool LR) {

    if (N<2) return;

    //construct the adjacency matrix
    tensor2 L;
    L.setsize(N);
    L.zeroize();

    double * s  = new double[N];

    //count the number of adjacent atoms
    for (int i=0; i<N; ++i) {
        int nd = 0;

        for (int j=0; j<N; ++j)  if ( W(R[i],R[j]) != 0) ++nd;

        s[i] = 1./sqrt(nd);
        L(i,i) += nd;
    }

    //make the Lagrangian
    for (int i=0; i<N; ++i) for (int j=0; j<N; ++j) L(i,j) -= W(R[i],R[j]);

    //normalize the lagrangian matrix
    for (int i=0; i<N; ++i) for (int j=0; j<N; ++j) L(i,j) *= s[i] * s[j];

    //extract the second generalized eigenvector
    {
        double * ll = new double[N];
        DiagonalizeV(L, ll);
        delete[] ll;

        for (int i=0; i<N; ++i) s[i] = L(1,i) * s[i];
    }


    int NF = 0;
    for (int i=0; i<N; ++i) {
        double smin = s[i];
        int imin = i;
        for (int j=i+1; j<N; ++j) {
            if (s[j]<smin) {
                smin = s[j];
                imin = j;
            }
        }
        swap (R[i], R[imin]);
        swap (s[i], s[imin]);

        if (s[i]<0) ++NF;
    }

    //pack the last elements of the first block
    {
        int * cnt = new int[NF];

        for (int i=0; i<NF; ++i) {
            int cc = 0;
            for (int j=NF; j<N; ++j)
                if ( W(R[i],R[j]) != 0) ++cc;
            cnt[i] = cc;
        }


        for (int i=NF-1; i>=0; --i) {
            int maxcc = cnt[i];
            int imax  = i;

            for (int j=i-1; j>=0; --j) {
                if (cnt[j]>maxcc) {
                    maxcc = cnt[j];
                    imax = j;
                }
            }

            swap (R[i]  , R[imax]);
            swap (s[i]  , s[imax]);
            swap (cnt[i], cnt[imax]);
        }

        delete[] cnt;
    }


    // and the first elements of the second block
    {
        int * cnt = new int[N];

        for (int i=NF; i<N; ++i) {
            int cc = 0;
            for (int j=0; j<NF; ++j)
                if ( W(R[i],R[j]) != 0) ++cc;
            cnt[i] = cc;
        }


        for (int i=NF; i<N; ++i) {
            int maxcc = cnt[i];
            int imax  = i;

            for (int j=i+1; j<N; ++j) {
                if (cnt[j]>maxcc) {
                    maxcc = cnt[j];
                    imax = j;
                }
            }

            swap (R[i]  , R[imax]);
            swap (s[i]  , s[imax]);
            swap (cnt[i], cnt[imax]);
        }

        delete[] cnt;
    }


    return;





    //count number of elements lower than the pivot
    int Nn, Np;
    int * NN, * PP;
    {
        Nn = 0;
        for (int i=0; i<N; ++i) if (s[i]<0) ++Nn;
        Np = N-Nn;

        //fill the sublists
        NN = new int[Nn];
        PP = new int[Np];

        int nnn=0;
        int ppp=0;

        for (int i=0; i<N; ++i) {
            if (s[i] <  0) { NN[nnn] = R[i]; ++nnn;}
            else           { PP[ppp] = R[i]; ++ppp;}
        }
    }

    delete[] s;



    // count the number of interactions with the sibling block
    int nIn = 0;
    for (int i=0; i<Nn; ++i)
        for (int j=0; j<NS; ++j)
            if ( W(NN[i],S[j]) != 0) ++nIn;

    int nIp = 0;
    for (int i=0; i<Np; ++i)
        for (int j=0; j<NS; ++j)
            if ( W(PP[i],S[j]) != 0) ++nIp;



    int *LL, *HH;
    int nl, nh;

    bool swapped;

    //swap

    if ( (LR==true  && (nIn*Np > nIp*Nn)) || (LR==false && (nIn*Np < nIp*Nn))    ) {
        LL = PP;
        HH = NN;
        nl = Np;
        nh = Nn;

        swapped = true;
    }
    else {
        LL = NN;
        HH = PP;
        nl = Nn;
        nh = Np;

        swapped = false;
    }


    //if (S==NULL) {
    if (1) {
        GraphSpectralSort (W , LL , HH, nl, nh, true);
        GraphSpectralSort (W , HH , LL, nh, nl, false);
    }
    else {
        GraphSpectralSort (W , LL , S, nl, NS, true);
        GraphSpectralSort (W , HH , S, nh, NS, false);
    }


    cout << Nn << " " << Np << "   " << nIn << " " << nIp << "   " << swapped << endl;

    //fuse the two lists
    for (int i=0; i<nl; ++i) R[i]    = LL[i];
    for (int i=0; i<nh; ++i) R[i+nl] = HH[i];

    delete[] NN;
    delete[] PP;
}


void Interactions::PrescreenInteractionsGSC(Array<int> & R, const Array<Atom> & Atoms, const GTObasis & BasisSet, double logGDO) {

    // compute interaction
    PrescreenInteractions(Atoms, BasisSet, logGDO);

    // generate the adjacency matrix
    tensor2 W;
    W.setsize(Atoms.n);
    W.zeroize();

    for (int at1=0; at1<Atoms.n; ++at1) {
        DLinkedList<int> * r;

        int nd = 0;
        for (r=List[at1].root; r!=NULL; r=r->next) {
            int at2 = r->key;

            W(at1,at2) = W(at2,at1) = 1;
            ++nd;
        }
    }

    // initial labelling
    R.set(Atoms.n);
    for (int i=0; i<Atoms.n; ++i)
        R[i] = i;

    // graph spectral clustering sort
    GraphSpectralSort(W, R.keys, NULL, Atoms.n, 0, true);

    tensor2 S;
    S.setsize(Atoms.n);
    S.zeroize();

    for (int i=0; i<Atoms.n; ++i)
        for (int j=0; j<Atoms.n; ++j)
            if ( W(R[i],R[j]) != 0)
                S(i,j)=1;

    //WriteBMP("SS.bmp", S);

    // print
    //cout.precision(16);
    //for (int at1=0; at1<Atoms.n; ++at1) cout << R[at1] << " " << l[at1] << "   " << s[at1] << endl;

    //char a;
    //cin >> a;

}







//sorts the queue by atom id
void Interactions::SortList() {

    for (int at=0; at<List.n; ++at) {
        DLinkedList<int> * i;

        for (i = List[at].root; i!=NULL; i=i->next) {
            DLinkedList<int> * jmin = i;
            DLinkedList<int> * j;
            for (j = i->next; j!=NULL; j=j->next)
                if (j->key < jmin->key) jmin = j;
            swap (i->key, jmin->key);
        }
    }
}

//Cuthill-McKee bandwith reduction algorithm
void Interactions::CuthillMcKee(Array<int> & R) {

    int nn = List.n;

    //Step 0: Prepare an empty queue Q and an empty result array R.
    R.set(nn);
    Array<bool> isinR(nn);
    for (int i=0; i<nn; ++i) isinR[i] = false;
    int lastR = 0;

    int nnn = 0; for (int i=0; i<nn; ++i) nnn += List[i].n;

    Array<int> Q(nnn);
    Array<bool> isinQ(nn);
    for (int i=0; i<nn; ++i) isinQ[i] = false;
    int lastQ =0;



    Array<intpair> nodes(nn);
    //Step 1: Select the node in G(n) with the lowest degree (ties are broken arbitrarily) that hasn't previously been inserted in the result array. Let us name it P (for Parent).
    while (lastR < nn) {
        int min;
        int P;

        for (int i=0; i<nn; ++i) {
            if (!isinR[i]) {
                P = i;
                break;
            }
        }
        min = List[P].n;


        for (int i=P+1; i<nn; ++i) {
            if (!isinR[i]) {
                if (List[i].n < min) {
                    P = i;
                    min = List[i].n;
                }
            }
        }

        //cout << P << endl;

        //Step 2: Add P in the first free position of R.
        R[lastR] = P;
        isinR[P] = true;
        ++lastR;

        //Step 3: Add to the queue all the nodes adjacent with P in the increasing order of their degree.

        int nnodes;

        nnodes = List[P].n;

        {
            //copies all nodes of R[last] to an auxiliary array
            DLinkedList<int> * thenode = List[P].root;
            for (int i=0; i<nnodes; ++i) {
                nodes[i].atom   = thenode->key;
                nodes[i].nnodes = List[nodes[i].atom].n;
                thenode = thenode->next;
            }

            //sorts the array by number of nodes
            for (int i=0; i<nnodes; ++i) {
                int imin = i;
                int nmin = nodes[i].nnodes;

                for (int j=i+1; j<nnodes; ++j) if(nodes[i].nnodes<nmin) {imin=j; nmin = nodes[i].nnodes;}

                //swaps
                int tmp;
                tmp = nodes[i].atom;   nodes[i].atom   = nodes[imin].atom;   nodes[imin].atom   = tmp;
                tmp = nodes[i].nnodes; nodes[i].nnodes = nodes[imin].nnodes; nodes[imin].nnodes = tmp;
            }

            //dumps the array into the queue
            for (int i=0; i<nnodes; ++i) {
                int C = nodes[i].atom;
                if (!isinR[C] && !isinQ[C]) {
                    Q[lastQ] = C;
                    isinQ[C] = true;
                    ++lastQ;
                }
            }

        }



        //while (!Q.IsVoid()) {
        while (lastQ > 0) {
            //Step 4.1: Extract the first node from the queue and examine it. Let us name it C (for Child).
            --lastQ;
            int C = Q[lastQ];

            isinQ[C] = false;

            //Step 4.2: If C hasn't previously been inserted in R, add it in the first free position and add to Q all the neighbours of C that are not in R in the increasing order of their degree.
            if (!isinR[C]) {
                R[lastR] = C;
                isinR[C] = true;
                ++lastR;

                {
                    nnodes = List[C].n;
                    //copies all nodes of R[last] to an auxiliary array
                    DLinkedList<int> * thenode = List[C].root;
                    for (int i=0; i<nnodes; ++i) {
                        nodes[i].atom   = thenode->key;
                        nodes[i].nnodes = List[nodes[i].atom].n;
                        thenode = thenode->next;
                    }
                    //sorts the array by number of nodes
                    for (int i=0; i<nnodes; ++i) {
                        int imin = i;
                        int nmin = nodes[i].nnodes;
                        for (int j=i+1; j<nnodes; ++j) if(nodes[i].nnodes<nmin) {imin=j; nmin = nodes[i].nnodes;}

                        //swaps
                        int tmp;
                        tmp = nodes[i].atom;   nodes[i].atom   = nodes[imin].atom;   nodes[imin].atom   = tmp;
                        tmp = nodes[i].nnodes; nodes[i].nnodes = nodes[imin].nnodes; nodes[imin].nnodes = tmp;
                    }


                    //dumps the array into the queue
                    for (int i=0; i<nnodes; ++i) {
                        int C = nodes[i].atom;
                        if (!isinR[C] && !isinQ[C]) {
                            Q[lastQ] = C;
                            isinQ[C] = true;
                            ++lastQ;
                        }
                    }

                }

            }

            //Step 5: If Q is not empty repeat from Step 4.1
        }

    //Step 6: If there are unexplored nodes (the graph is not connected) repeat from Step 1
    }


    //reverse
    for (int i=0; i<R.n/2; ++i)
        swap(R[i],R[R.n-1-i]);

}


void Interactions::From (const Interactions & IL, const Array<int> & R) {

    //inverts list
    Array<int> iR(R.n);
    for (int i=0; i<R.n; ++i)
        iR[R[i]] = i;

    List.set(IL.List.n);


    for (int i=0; i<IL.List.n; ++i) {
        DLinkedList<int> * r = IL.List[R[i]].root;


        //crea una lista
        std::set<int> slist;
        for (0; r!=NULL; r=r->next) {
            slist.insert(iR[r->key]);
        }

        std::set<int>::iterator it;

        for (it=slist.begin(); it!=slist.end(); ++it) {
            List[i].PushBack(*it);
            if (*it == i) break;
        }


        /*
        for (int j=0; j<IL.List[R[i]].n; ++j) {
            List[i].PushBack(iR[r->key]);
            r = r->next;
        }
        */

    }

    //SortList();
}

Interactions::Interactions() {

}

Interactions::~Interactions() {
    List.clear();
}

Interactions & Interactions::operator=  (const Interactions & rhs) {
    List.clear();
    List.set(rhs.List.n);
    for (int i=0; i<List.n; ++i)
        List[i] = rhs.List[i];

    return *this;
}

Interactions & Interactions::operator+= (const Interactions & rhs) {
    for (int i=0; i<List.n; ++i) {
        std::set<int> slist;

        const DLinkedList<int> * root = List[i].root;
        for (0; root!=NULL; root=root->next)  slist.insert(root->key);
        root = rhs.List[i].root;
        for (0; root!=NULL; root=root->next)  slist.insert(root->key);

        List[i].Clear();
        std::set<int>::iterator it;

        for (it=slist.begin(); it!=slist.end(); ++it) {
            List[i].PushBack(*it);
        }
    }

    return *this;
}


int Interactions::GetTotN() const{
    int tot = 0;

    for (int i=0; i<List.n; ++i) {
        tot += List[i].n;
    }

    /*
    for (int i=0; i<List.n; ++i) {
        cout << i << ": ";

        const DLinkedList<int> * root = List[i].root;
        for (0; root!=NULL; root=root->next)  cout << " " << root->key;

        cout << endl;
    }
    */

    return tot;
}

const Interactions Interactions::operator*(const Interactions & rhs) const {
    Interactions Prod;

    int n = List.n;

    Prod.List.set(n);

    for (int i=0; i<n; ++i) {
        std::set<int> slist;
        const DLinkedList<int> * root = List[i].root;

        for (0; root!=NULL; root=root->next) {
            int k=root->key;

            //merge
            const DLinkedList<int> * root2 = rhs.List[k].root;

            for (0; root2!=NULL; root2=root2->next) {
                slist.insert(root2->key);
            }
        }

        //copy set
        std::set<int>::iterator it;

        for (it=slist.begin(); it!=slist.end(); ++it) {
            Prod.List[i].PushBack(*it);
            //if (*it == i) break;
        }
    }

    return Prod;
}

const Interactions Interactions::Transpose() const {
    Interactions Prod;

    int n = List.n;

    Prod.List.set(n);

    for (int i=0; i<n; ++i) {
        const DLinkedList<int> * root = List[i].root;

        for (0; root!=NULL; root=root->next) {
            int j=root->key;

            Prod.List[j].PushBack(i);
        }
    }

    return Prod;
}



template <> void Boxing<Nucleus>::InitFrom(const Nucleus * nuclei, int nnuclei, double l) {

    //set min and max
    vmax.x = vmin.x = nuclei[0].c.x;
    vmax.y = vmin.y = nuclei[0].c.y;
    vmax.z = vmin.z = nuclei[0].c.z;

    for (int i=0; i<nnuclei; ++i) {
        vmin.x = min(vmin.x,nuclei[i].c.x);
        vmin.y = min(vmin.y,nuclei[i].c.y);
        vmin.z = min(vmin.z,nuclei[i].c.z);

        vmax.x = max(vmax.x,nuclei[i].c.x);
        vmax.y = max(vmax.y,nuclei[i].c.y);
        vmax.z = max(vmax.z,nuclei[i].c.z);
    }

    //just to have some value
    L  = l; //1.0;

    //number of boxes
    Lx = 1+ int((vmax.x-vmin.x)/L);
    Ly = 1+ int((vmax.y-vmin.y)/L);
    Lz = 1+ int((vmax.z-vmin.z)/L);

    //adjust vmin and vmax
    double Ax = (Lx * L) - (vmax.x-vmin.x);
    double Ay = (Ly * L) - (vmax.y-vmin.y);
    double Az = (Lz * L) - (vmax.z-vmin.z);

    vmin.x -= Ax/2;
    vmin.y -= Ay/2;
    vmin.z -= Az/2;

    vmax.x += Ax/2;
    vmax.y += Ay/2;
    vmax.z += Az/2;

    //initialize the boxes
    boxes.set(Lx, Ly, Lz); //add empty boxes at the ends

    //add everything
    for (int i=0; i<nnuclei; ++i) {
        //compute block

        point p;
        p = nuclei[i].c - vmin;

        int nx = p.x/L;
        int ny = p.y/L;
        int nz = p.z/L;

        boxes(nx,ny,nz).elements.push_back(nuclei[i]);
    }

}


#include "math/gamma.hpp"

int ccount;

//self-interaction
static double J(const Box<Nucleus> & box1) {
/*
    list<Nucleus>::const_iterator it1;
    list<Nucleus>::const_iterator it2;

    double E = 0;

    for (it1=box1.elements.begin(); it1!=box1.elements.end(); ++it1) {
        for (it2=box1.elements.begin(); it2!=box1.elements.end(); ++it2) {
            if (it1==it2) continue;
            vector3 v; v = it1->c - it2->c;
            double R2 = v*v;
            E += (it1->Z * it2->Z) * ShortCoulomb.CalcS(R2);
            ++ccount;
        }
    }
     return E;
*/
return 0;
}

//interaction between boxes
static double J(const Box<Nucleus> & box1, const Box<Nucleus> & box2) {
/*
    list<Nucleus>::const_iterator it1;
    list<Nucleus>::const_iterator it2;

    double E = 0;

    for (it1=box1.elements.begin(); it1!=box1.elements.end(); ++it1) {
        for (it2=box2.elements.begin(); it2!=box2.elements.end(); ++it2) {
            vector3 v; v = it1->c - it2->c;
            double R2 = v*v;
            E += (it1->Z * it2->Z) * ShortCoulomb.CalcS(R2);
            ++ccount;
        }
    }

    return E;
    */
    return 0;
}

template <> const Box<Nucleus> * Boxing<Nucleus>::GetBox(const point & P) const {

    point O = P - vmin;

    int nx = (O.x/L);
    int ny = (O.y/L);
    int nz = (O.z/L);

    if (nx < 0 || ny < 0 || nz < 0) return NULL;
    if (nx>=Lx || ny>=Ly || nz>=Lz) return NULL;

    return &(boxes(nx,ny,nz));
}


template <> void Boxing<Nucleus>::RetrieveBoxes(const point & P, const Box<Nucleus> * TheBoxes[8]) const {
    //from the point, get the boxes

    int i = 0;

    for (int Ax=-1; Ax<=1; Ax+=2) {
        for (int Ay=-1; Ay<=1; Ay+=2) {
            for (int Az=-1; Az<=1; Az+=2) {
                //compute the points
                point PP = P;

                if (Ax<0) PP.x -= L/2;
                else      PP.x += L/2;

                if (Ay<0) PP.y -= L/2;
                else      PP.y += L/2;

                if (Az<0) PP.z -= L/2;
                else      PP.z += L/2;

                //
                TheBoxes[i] = GetBox(PP);

                ++i;
            }
        }
    }

}


template <> double Boxing<Nucleus>::CalcJ() const {

    double E = 0;

    ccount = 0;


    for (int Ax=-1; Ax<=1; ++Ax) {
        for (int Ay=-1; Ay<=1; ++Ay) {
            for (int Az=-1; Az<=1; ++Az) {
                //same box
                if (Ax==0 && Ay==0 && Az==0) {

                    for (int nx=0; nx<Lx; ++nx) {
                        for (int ny=0; ny<Ly; ++ny) {
                            for (int nz=0; nz<Lz; ++nz) {
                                E += J(boxes(nx,ny,nz));
                            }
                        }
                    }

                }
                //differrent boxes
                else {
                    int minnx = max(0,-Ax);
                    int maxnx = Lx-max(0,Ax);

                    int minny = max(0,-Ay);
                    int maxny = Ly-max(0,Ay);

                    int minnz = max(0,-Az);
                    int maxnz = Lz-max(0,Az);

                    for (int nx=minnx; nx<maxnx; ++nx) {
                        for (int ny=minny; ny<maxny; ++ny) {
                            for (int nz=minnz; nz<maxnz; ++nz) {
                                E += J(boxes(nx,ny,nz), boxes(nx+Ax  ,ny+Ay  ,nz+Az) );
                            }
                        }
                    }

                }

            }
        }
    }

    ccount /=2;
    E /=2;

    return E;

}

template <> void Boxing<Nucleus>::PrintPop() const {
    int ntot = 0;

    cout << Lx << " " << Ly << " " << Lz << endl;

    for (int nx=0; nx<Lx; ++nx) {
        for (int ny=0; ny<Ly; ++ny) {
            for (int nz=0; nz<Lz; ++nz) {
                int np = boxes(nx,ny,nz).elements.size();
                Messenger << nx << " " << ny << " " << nz << "   " << np << " particles" << endl;
                ntot += np;
            }
        }
    }

    Messenger << " total: " << ntot << endl;
    Messenger << ccount << "  interactions" << endl;

}




//**********************************************************************************//



void PWBoxing::InitFrom(const PW * nuclei, int nnuclei, double l) {

    //set min and max
    vmax.x = vmin.x = nuclei[0].P.x;
    vmax.y = vmin.y = nuclei[0].P.y;
    vmax.z = vmin.z = nuclei[0].P.z;

    for (int i=0; i<nnuclei; ++i) {
        vmin.x = min(vmin.x,nuclei[i].P.x);
        vmin.y = min(vmin.y,nuclei[i].P.y);
        vmin.z = min(vmin.z,nuclei[i].P.z);

        vmax.x = max(vmax.x,nuclei[i].P.x);
        vmax.y = max(vmax.y,nuclei[i].P.y);
        vmax.z = max(vmax.z,nuclei[i].P.z);
    }

    L  = l;

    //number of boxes
    Lx = 1+ int((vmax.x-vmin.x)/L);
    Ly = 1+ int((vmax.y-vmin.y)/L);
    Lz = 1+ int((vmax.z-vmin.z)/L);

    //adjust vmin and vmax
    double Ax = (Lx * L) - (vmax.x-vmin.x);
    double Ay = (Ly * L) - (vmax.y-vmin.y);
    double Az = (Lz * L) - (vmax.z-vmin.z);

    vmin.x -= Ax/2;
    vmin.y -= Ay/2;
    vmin.z -= Az/2;

    vmax.x += Ax/2;
    vmax.y += Ay/2;
    vmax.z += Az/2;

    //initialize the boxes
    boxes.set(Lx, Ly, Lz); //add empty boxes at the ends

    //add everything
    for (int i=0; i<nnuclei; ++i) {
        //compute block

        point p;
        p = nuclei[i].P - vmin;

        int nx = p.x/L;
        int ny = p.y/L;
        int nz = p.z/L;

        boxes(nx,ny,nz).elements.push_back(nuclei[i]);
    }

}


const PWBox * PWBoxing::GetBox(const point & P) const {

    point O = P - vmin;

    int nx = (O.x/L);
    int ny = (O.y/L);
    int nz = (O.z/L);

    if (nx < 0 || ny < 0 || nz < 0) return NULL;
    if (nx>=Lx || ny>=Ly || nz>=Lz) return NULL;

    return &(boxes(nx,ny,nz));
}


void PWBoxing::RetrieveBoxes(const point & P, const PWBox * TheBoxes[8]) const {
    //from the point, get the boxes

    int i = 0;

    for (int Ax=-1; Ax<=1; Ax+=2) {
        for (int Ay=-1; Ay<=1; Ay+=2) {
            for (int Az=-1; Az<=1; Az+=2) {
                //compute the points
                point PP = P;

                if (Ax<0) PP.x -= L/2;
                else      PP.x += L/2;

                if (Ay<0) PP.y -= L/2;
                else      PP.y += L/2;

                if (Az<0) PP.z -= L/2;
                else      PP.z += L/2;

                //
                TheBoxes[i] = GetBox(PP);

                ++i;
            }
        }
    }

}


void PWBoxing::PrintPop() const {
    int ntot = 0;

    cout << Lx << " " << Ly << " " << Lz << endl;

    for (int nx=0; nx<Lx; ++nx) {
        for (int ny=0; ny<Ly; ++ny) {
            for (int nz=0; nz<Lz; ++nz) {
                int np = boxes(nx,ny,nz).elements.size();
                Messenger << nx << " " << ny << " " << nz << "   " << np << " particles" << endl;
                ntot += np;
            }
        }
    }

    Messenger << " total: " << ntot << endl;

}




void tensor::touch(const Interactions & AtomInteractions) {
    //inits tensors
    int nat = AtomInteractions.List.n;
	for (int at1=0; at1<nat; ++at1) {
        DLinkedList<int> * rint = AtomInteractions.List[at1].root;

        //at1 > at2
        for (rint = AtomInteractions.List[at1].root; rint!=NULL; rint=rint->next) {
            int at2 = rint->key;
            //if (at2>=at1) break;
            touch(at1,at2);
        }
        touch(at1,at1);
	}
}

void symtensor::touch(const Interactions & AtomInteractions) {
    //inits tensors
    int nat = AtomInteractions.List.n;
	for (int at1=0; at1<nat; ++at1) {
        DLinkedList<int> * rint = AtomInteractions.List[at1].root;

        //at1 > at2
        for (rint = AtomInteractions.List[at1].root; rint!=NULL; rint=rint->next) {
            int at2 = rint->key;
            if (at2>=at1) break;
            touch(at1,at2);
        }
        touch(at1,at1);
	}
}
