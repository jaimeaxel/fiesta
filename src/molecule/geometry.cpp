/*
    geometry.cpp

    rutinas relacionadas con la geometria molecular y sus cambios

    TODO: hay que arreglar la geometria molecular para que funcione en coma fija y
          redondee los errores a magnitudes exactas
*/

#include <cmath>
#include "math/affine.hpp"
#include "math/smath.hpp"

//convierte de coordenadas internas a cartesianas
point Icoord2vec(const icoord & ic, const point & at2, const point & at3, const point & at4) {

	vector3 ez;
	ez = at3-at2;
	normalize(ez);


	vector3 v42;
	v42 = at4-at2;

	double mu = v42*ez;

	vector3 ex;
	ex.x = v42.x - mu*ez.x;
	ex.y = v42.y - mu*ez.y;
	ex.z = v42.z - mu*ez.z;
	normalize(ex);


	vector3 ey = Xprod(ez, ex);


	double cx = ic.R * cos(ic.b) * sin(ic.a);
	double cy = ic.R * sin(ic.b) * sin(ic.a);
	double cz = ic.R *             cos(ic.a);

	//nuevo punto
	point ret;
	ret.x = at2.x + ex.x*cx + ey.x*cy + ez.x*cz;
	ret.y = at2.y + ex.y*cx + ey.y*cy + ez.y*cz;
	ret.z = at2.z + ex.z*cx + ey.z*cy + ez.z*cz;

	return ret;
}


/*
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
*/

//falta decir qu� atomos
icoord vec2icoord (const point & at1, const point & at2, const point & at3, const point & at4) {
    icoord ret;

    vector3 d12, d23, d34;
    d12 = at2 - at1;
    d23 = at3 - at2;
    d34 = at4 - at3;

    ret.R = sqrt(d12*d12);

    normalize(d12);
    normalize(d23);
    normalize(d34);

    double cosa =  -(d12*d23);
    double cosb =  Xprod(d12, d23) * Xprod(d23, d34);

    double det = (d12.x*d23.y*d34.z + d12.y*d23.z*d34.x + d12.z*d23.x*d34.y) - (d12.z*d23.y*d34.x + d12.x*d23.z*d34.y + d12.y*d23.x*d34.z);
    //int    s   = sgn(det);

    ret.a = acos(cosa);
    ret.b = acos(cosb);

    if (det<0) ret.b = -ret.b; //since the determinant is 0 when b=PI, can't use the signum function

    return ret;
}
