/*
    molecule.cpp

    definicion de molecula; inicializacion y calculo de propiedades moleculares
*/
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "low/strings.hpp"
#include "low/chrono.hpp"
#include "low/io.hpp"
#include "basis/GTO.hpp"
#include "basis/atomdef.hpp"
#include "basis/shellpair.hpp"
#include "molecule/atoms.hpp"
#include "molecule/molecule.hpp"
#include "molecule/geometry.hpp"
#include "molecule/prescreen.hpp"
#include "linear/sparse.hpp"
#include "linear/eigen.hpp"
#include "math/gamma.hpp"

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_num_threads() 1
#endif

using namespace Fiesta;
using namespace std;


Molecule::Molecule () {
    nAtoms       = 0;
    nElec        = 0;
    nEla         = 0;
    nElb         = 0;
    charge       = 0;
    multiplicity = 0;
    Nuclei       = NULL;
    Atoms        = NULL;
}

Molecule::~Molecule () {
    delete[] Nuclei;
    delete[] Atoms;
}

//imprime una molecula
void Molecule::PrintMol () {
    Messenger.precision(6);
    Messenger << "System in cartesian coordinates: " << endl;

    for (int i=0; i<nAtoms; ++i) {
        int id = i+1;
        Messenger.width(5);
        Messenger << id << "  " << Nuclei[i].name << "  ";
        Messenger.width(14);
        Messenger << Nuclei[i].c.x;
        Messenger.width(14);
        Messenger << Nuclei[i].c.y;
        Messenger.width(14);
        Messenger << Nuclei[i].c.z;
        Messenger << endl;
    }

    Messenger << endl;
}

std::vector<double> Molecule::GetCenterOfMass() {

    double mx = 0;
    double my = 0;
    double mz = 0;
    double mass = 0;

    for (int i=0; i<nAtoms; ++i) {
        mx += Nuclei[i].c.x * Nuclei[i].mass;
        my += Nuclei[i].c.y * Nuclei[i].mass;
        mz += Nuclei[i].c.z * Nuclei[i].mass;
        mass += Nuclei[i].mass;
    }

    mx /= mass;
    my /= mass;
    mz /= mass;

    std::vector<double> center;
    center.push_back(mx);
    center.push_back(my);
    center.push_back(mz);
    return center;
}

//centra la molecula en el sistema de referencia
void Molecule::CenterMol() {
    //centra la molecula en el sistema de referencia
    double mx = 0;
    double my = 0;
    double mz = 0;
    double mass = 0;

    for (int i=0; i<nAtoms; ++i) {
        mx += Nuclei[i].c.x * Nuclei[i].mass;
        my += Nuclei[i].c.y * Nuclei[i].mass;
        mz += Nuclei[i].c.z * Nuclei[i].mass;
        mass += Nuclei[i].mass;
    }

    mx /= mass;
    my /= mass;
    mz /= mass;

    for (int i=0; i<nAtoms; ++i) {
        Nuclei[i].c.x -= mx;
        Nuclei[i].c.y -= my;
        Nuclei[i].c.z -= mz;
    }

    //coloca la molecula a lo largo de los ejes de inercia
    symtensor2 Inertia;
    Inertia.setsize(3);
    Inertia.zeroize();

    for (int i=0; i<nAtoms; ++i) {
        Inertia(0,0) += Nuclei[i].c.x * Nuclei[i].c.x * Nuclei[i].mass;
        Inertia(1,0) += Nuclei[i].c.x * Nuclei[i].c.y * Nuclei[i].mass;
        Inertia(2,0) += Nuclei[i].c.x * Nuclei[i].c.z * Nuclei[i].mass;
        Inertia(1,1) += Nuclei[i].c.y * Nuclei[i].c.y * Nuclei[i].mass;
        Inertia(2,1) += Nuclei[i].c.y * Nuclei[i].c.z * Nuclei[i].mass;
        Inertia(2,2) += Nuclei[i].c.z * Nuclei[i].c.z * Nuclei[i].mass;
    }

    double inval[3];
    tensor2 axes;
    axes = Inertia;
    //axes.setsize(3);
    //Diagonalize(Inertia, 3, axes, inval, 0);
    DiagonalizeV(axes, inval);

    //rota la molecula hasta los ejes de inercia
    for (int i=0; i<nAtoms; ++i) {
        double x = Nuclei[i].c.x;
        double y = Nuclei[i].c.y;
        double z = Nuclei[i].c.z;

        Nuclei[i].c.x = x*axes(0,0) + y*axes(0,1) + z*axes(0,2);
        Nuclei[i].c.y = x*axes(1,0) + y*axes(1,1) + z*axes(1,2);
        Nuclei[i].c.z = x*axes(2,0) + y*axes(2,1) + z*axes(2,2);
    }

    Messenger << "Molecule centered and reoriented" << endl;
    Messenger << "Principal components of the moment of inertia are: " << endl;
    Messenger << " x: " << inval[0] << endl;
    Messenger << " y: " << inval[1] << endl;
    Messenger << " z: " << inval[2] << endl;

    axes.clear();
    Inertia.clear();
}

void Molecule::SumElectrons() {
    // add electrons
    nElec = 0;
    for (int at=0; at<nAtoms; ++at)
        nElec += int(Nuclei[at].Z); //la carga nuclear es igual al numero de electrones en un sistema neutro

    nElec -= charge;

    nEla = (nElec + multiplicity)/2;
    nElb = (nElec - multiplicity)/2;

    ElectronData.npairs = nElec/2;
    ElectronData.nalpha = nEla;
    ElectronData.nbeta  = nElb;
}


void Molecule::From(const GTObasis & BasisSet) {

   // create nuclei
    nAtoms = mAtoms.n;
    Nuclei = new Nucleus[nAtoms];

    nEla = (nElec + multiplicity)/2;
    nElb = (nElec - multiplicity)/2;

    for (int i=0; i<nAtoms; ++i) {
        Nuclei[i] = mAtoms[Relabel[i]];
    }

    // generate  a reduced list of elements; generate a list of atoms and
    // link them to the new definitions; make a list of the atoms for every element

    std::set<uint16_t> ATList;
    for (int i=0; i<nAtoms; ++i) ATList.insert(Nuclei[i].type);

    InternalDefs.prototypes.set(ATList.size());

    std::set<uint16_t>::iterator it; int Id=0;
    for (it=ATList.begin(); it!=ATList.end(); ++it, ++Id) {
        const AtomDef & atom = AtomDefs[*it];
        InternalDefs.prototypes[Id].name = atom.name;
        InternalDefs.prototypes[Id].mass = atom.mass;
        InternalDefs.prototypes[Id].Z    = atom.Z;

        InternalDefs.prototypes[Id].DId  = *it;
        InternalDefs.prototypes[Id].RId  =  Id;


        int nb = BasisSet[*it].n;
        InternalDefs.prototypes[Id].basis.set(nb);

        for (int b=0; b<nb; ++b) InternalDefs.prototypes[Id].basis[b] = BasisSet[*it][b];

        InternalDefs.prototypes[Id].Atoms  = NULL;
        InternalDefs.prototypes[Id].nAtoms = 0;
    }

    std::map<uint16_t,uint16_t> ATMap;
    int RId=0;
    for (it=ATList.begin(); it!=ATList.end(); ++it, ++RId) ATMap[*it] = RId;


    Atoms = new rAtom[nAtoms];

    for (int i=0; i<nAtoms; ++i) {
        int RId = ATMap[Nuclei[i].type];
        Atoms[i].c = Nuclei[i].c;
        Atoms[i].rAP = &(InternalDefs.prototypes[RId]);

        ++InternalDefs.prototypes[RId].nAtoms;
    }

    for (int rid=0; rid<InternalDefs.prototypes.N(); ++rid) {
        InternalDefs.prototypes[rid].Atoms = new rAtom[InternalDefs.prototypes[rid].nAtoms];
        InternalDefs.prototypes[rid].nAtoms = 0;
    }

    for (int i=0; i<nAtoms; ++i) {
        Atoms[i].rAP->Atoms[Atoms[i].rAP->nAtoms] = Atoms[i];
        ++Atoms[i].rAP->nAtoms;
    }
}

//calcula la energia nuclear
void Molecule::CalcNuclear() {

    double enuc = 0;

    vector3 R = Nuclei[0].c - Nuclei[1].c;
    double minr2 = R*R;

    for (int i=0; i<nAtoms-1; ++i) {
        double pEnuc = 0;

        for (int j=i+1; j<nAtoms; ++j) {
            vector3 R;
            R = Nuclei[i].c - Nuclei[j].c;
            double R2 = R*R;
            minr2 = min(minr2, R2);
            pEnuc += (Nuclei[j].Z) / sqrt(R2);
        }
        enuc += Nuclei[i].Z * pEnuc;
    }
    ENuc = enuc;

    CalcNuclearX();
}

//calcula la energia nuclear
void Molecule::CalcNuclearX() {

    double enuc = 0;

    for (int i=0; i<nAtoms; ++i) {
        double pEnuc = 0;

        const ExternalPotential & XV = ElectronData.ExternalV;

        // monopole contribution
        for (int j=0; j<XV.Ms; ++j) {
            vector3 R;
            R = Nuclei[i].c - XV.MC[j];
            double R2 = R*R;
            pEnuc += (XV.M[j]) / sqrt(R2);
        }

        // dipole contribution
        for (int j=0; j<XV.Ds; ++j) {
            vector3 R;
            R = Nuclei[i].c - XV.DC[j];
            double R2 = R*R;
            pEnuc += (R*XV.D[j])/(R2*sqrt(R2));
        }

        enuc += Nuclei[i].Z * pEnuc;
    }

    ENuc += enuc;
}

void Molecule::SortAtoms(const Array<Atom> & Atoms, const GTObasis & gBasis, double logminover) {
    ElectronData.SortAtoms(Relabel, Atoms, gBasis, logminover);
}

void Molecule::WriteCube(GTObasis& BasisSet)
{
    //Messenger << " coefficients " << endl ;
    //Messenger << ElectronData.C ;
    //Messenger << endl;
    double x_min=-15e0;
    double y_min=-15e0;
    double z_min=-15e0;
    double x_max=15e0;
    double y_max=15e0;
    double z_max=15e0;
    double step = 0.2;
    int n_x = std::ceil((x_max-x_min)/step);
    int n_y = std::ceil((y_max-y_min)/step);
    int n_z = std::ceil((z_max-z_min)/step);
    double voxel =  step;
    //for (int i=0; i< ElectronData.nBFs ; ++i)
    //for (int i=120; i< 200 ; ++i)
    int end = 0.5*nElec + 10;
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i < end ; ++i)
    //for (int i=0; i< 20 ; ++i)
    {
       // CK temporary >> : character test
       double schar = 0.0e0;
       double pchar = 0.0e0;
       double dchar = 0.0e0;
       double ochar = 0.0e0;
       {
          int bn = 0;
          for (int n=0 ; n < nAtoms ; ++n )
          {
             int id = Nuclei[n].type;
             int nbs = BasisSet.AtomBasis[id].Nshells();
             for (int bs = 0 ; bs < nbs ; ++bs)
             {
                 double c=0.0e0;
                 int n_ang = 2* BasisSet.AtomBasis[id][bs].l +1;
                 for (int l = 0; l < n_ang ; ++l)
                 {
                    for (int k = 0 ; k < BasisSet.AtomBasis[id][bs].K; ++k)
                    {
                        double tmp = ElectronData.C(bn,i)*BasisSet.AtomBasis[id][bs].N[0][k];
                        c+= tmp*tmp;
                    }
                    ++bn;
                 }
                 if ( BasisSet.AtomBasis[id][bs].l == 0)
                    schar += c;
                 else if ( BasisSet.AtomBasis[id][bs].l == 1)
                    pchar += c;
                 else if ( BasisSet.AtomBasis[id][bs].l == 2)
                    dchar += c;
                 else
                    ochar += c;

             }
          }
          schar = sqrt(schar);
          pchar = sqrt(pchar);
          dchar = sqrt(dchar);
          ochar = sqrt(ochar);
       }
       if ((schar+pchar) < (dchar+ochar))
       {
          Messenger << " Skip cube file for orbital " << i << " due to higher non-s/p character " << endl;
       }
       // CK temporary <<  : character test
       std::filebuf fb;
       Messenger << OutputDir << endl;
       fb.open (OutputDir+"/orb_"+std::to_string(i)+"p.cub",std::ios::out);
       std::ostream os(&fb);
       os.setf ( std::ios::fixed);
       os.precision(6);
       os <<  " character: s : " << schar << ";  p : " << pchar << ";  d: " << dchar << "; others: " << ochar <<  std::endl;
       os <<  " comment line 2" << std::endl;
       os <<  nAtoms << "   " << x_min << "   " << y_min << "   " << z_min << std::endl;
       os << n_x <<  "   " << voxel << "   "  << (double)0.0e0  << "   " << (double)0.0e0  << std::endl;
       os << n_y << "   " << (double)0.0e0 << "   " << voxel  << "   "  << (double)0.0e0 << std::endl;
       os << n_z << "   " << (double)0.0e0 << "   "  << (double)0.0e0  << "   "  << voxel  << std::endl;

       std::filebuf fb2;
       fb2.open (OutputDir+"/orb_"+std::to_string(i)+"m.cub",std::ios::out);
       std::ostream os2(&fb2);
       os2.setf ( std::ios::fixed);
       os2.precision(6);
       os2 <<  " character: s : " << schar << ";  p : " << pchar << ";  d: " << dchar << "; others: " << ochar <<  std::endl;
       os2 <<  " comment line 2" << std::endl;
       os2 <<  nAtoms << "   " << x_min << "   " << y_min << "   " << z_min << std::endl;
       os2 << n_x <<  "   " << voxel << "   "  << (double)0.0e0  << "   " << (double)0.0e0  << std::endl;
       os2 << n_y << "   " << (double)0.0e0 << "   " << voxel  << "   "  << (double)0.0e0 << std::endl;
       os2 << n_z << "   " << (double)0.0e0 << "   "  << (double)0.0e0  << "   "  << voxel  << std::endl;
       for (int n=0 ; n < nAtoms ; ++n )
       {
          os << (int) Nuclei[n].Z << "  " <<  (double)0 << "  " << Nuclei[n].c.x << "  " << Nuclei[n].c.y << "  " << Nuclei[n].c.z << std::endl;
          os2 << (int) Nuclei[n].Z << "  " <<  (double)0 << "  " << Nuclei[n].c.x << "  " << Nuclei[n].c.y << "  " << Nuclei[n].c.z << std::endl;
       }


       for (int x=0; x < n_x; ++x)
       {
          double x_value = x_min+step*x;
          for (int y=0; y < n_y; ++y)
          {
             double y_value = y_min+step*y;
             for (int z=0; z < n_z; ++z)
             {
                double z_value = z_min+step*z;
                double value=0.0e0;
                int bn = 0;
                for (int n=0 ; n < nAtoms ; ++n )
                {
                   double dx = (Nuclei[n].c.x-x_value);
                   double dy = (Nuclei[n].c.y-y_value);
                   double dz = (Nuclei[n].c.z-z_value);
                   double R2 = dx*dx + dy*dy +dz*dz;
                   double r = sqrt(R2);
                   int id = Nuclei[n].type;
                   int nbs = BasisSet.AtomBasis[id].Nshells();
                   for (int bs = 0 ; bs < nbs ; ++bs)
                   {
                      double Y = 0.0e0;
                      int n_ang = 2* BasisSet.AtomBasis[id][bs].l +1;
                      std::vector<double> angvalues(n_ang);
                      if (BasisSet.AtomBasis[id][bs].l==0)
                      {
                         angvalues.at(0)= sqrt(1.0e0/2.0e0/PI);
                      }
                      else if (BasisSet.AtomBasis[id][bs].l==1)
                      {
                         const double c = sqrt(3.0e0/4.0e0/PI);
                         angvalues.at(0)=c*dy/r;
                         angvalues.at(1)=c*dz/r;
                         angvalues.at(2)=c*dx/r;
                      }
                      /*
                      else if (BasisSet.AtomBasis[id][bs].l==2)
                      {
                         const double c = 0.5e0*sqrt(15.0e0/PI);
                         angvalues.at(0)=c*dx*dy/R2;
                         angvalues.at(1)=c*dy*dz/R2;
                         angvalues.at(2)=0.5e0/sqrt(3)*c*(2*dz*dz-dx*dx-dy*dy)/R2;
                         angvalues.at(3)=c*dz*dx/R2;
                         angvalues.at(4)=0.5e0*c*(dx*dx-dy*dy)/R2;
                      }
                      */
                      else
                      {
                         //Messenger << " SKIP l > 2 basis in the cube files " << endl;
                         bn +=n_ang;
                         break;
                      }
                      for (int l = 0; l < n_ang ; ++l)
                      {
                         for (int k = 0 ; k < BasisSet.AtomBasis[id][bs].K; ++k)
                         {
                            double rvalue = ElectronData.C(bn,i)*BasisSet.AtomBasis[id][bs].N[0][k]*exp(-BasisSet.AtomBasis[id][bs].k[k]*R2);
                            value +=rvalue * angvalues.at(l);
                         }
                         ++bn;
                      }
                   }
                }
                if (value <0)
                {
                  os2 << -value << " " ;
                  os << (double)0 << " " ;
                }
                else
                {
                  os << value << " " ;
                  os2 << (double)0 << " " ;
                }
                if (z % 6 == 5)
                {
                   os << std::endl;
                   os2 << std::endl;
                }
             }
             os << std::endl;
             os2 << std::endl;
          }
       }
       Messenger << " orb_"<< i << ".cub written" << endl;
       fb.close();
       fb2.close();
    }
}

void Molecule::SetCharge(int c) {
    charge = c;
}

void Molecule::SetMultiplicity(int m) {
    multiplicity = m-1;
}

//sums all electrons
void Molecule::mSumElectrons() {
    nElec = 0;
    for (int at=0; at<mAtoms.n; ++at) {
        const AtomDef & element = AtomDefs[mAtoms[at].type];
        nElec += int(element.Z);
    }

    nElec -= charge;
}

//centers the system and orientates it according to its inertia axes
void Molecule::Center() {
    double mx = 0;
    double my = 0;
    double mz = 0;
    double mass = 0;

    for (int i=0; i<mAtoms.n; ++i) {
        ElementID atype = mAtoms[i].type;
        double amass = AtomDefs[atype].mass;
        mx += Atoms[i].c.x * amass;
        my += Atoms[i].c.y * amass;
        mz += Atoms[i].c.z * amass;
        mass += amass;
    }

    mx /= mass;
    my /= mass;
    mz /= mass;

    for (int i=0; i<mAtoms.n; ++i) {
        Atoms[i].c.x -= mx;
        Atoms[i].c.y -= my;
        Atoms[i].c.z -= mz;
    }

    //coloca la molecula a lo largo de los ejes de inercia
    symtensor2 Inertia;
    Inertia.setsize(3);
    Inertia.zeroize();

    tensor2 axes(3);
    axes.zeroize();

    for (int i=0; i<mAtoms.n; ++i) {
        double amass = AtomDefs[mAtoms[i].type].mass;
        axes(0,0) += Atoms[i].c.x * Atoms[i].c.x * amass;
        axes(1,0) += Atoms[i].c.x * Atoms[i].c.y * amass;
        axes(2,0) += Atoms[i].c.x * Atoms[i].c.z * amass;
        axes(1,1) += Atoms[i].c.y * Atoms[i].c.y * amass;
        axes(2,1) += Atoms[i].c.y * Atoms[i].c.z * amass;
        axes(2,2) += Atoms[i].c.z * Atoms[i].c.z * amass;
    }

    axes(0,1) = axes(1,0);
    axes(0,2) = axes(2,0);
    axes(1,2) = axes(2,1);


    double inval[3];
    DiagonalizeV(axes, inval);


    //rota la molecula hasta los ejes de inercia
    for (int i=0; i<mAtoms.n; ++i) {
        double x = Atoms[i].c.x;
        double y = Atoms[i].c.y;
        double z = Atoms[i].c.z;

        Atoms[i].c.x = x*axes(0,0) + y*axes(0,1) + z*axes(0,2);
        Atoms[i].c.y = x*axes(1,0) + y*axes(1,1) + z*axes(1,2);
        Atoms[i].c.z = x*axes(2,0) + y*axes(2,1) + z*axes(2,2);
    }

    Messenger << "Molecule centered and reoriented" << endl;
    Messenger << "Principal components of the moment of inertia are: " << endl;
    Messenger << " x: " << inval[0] << endl;
    Messenger << " y: " << inval[1] << endl;
    Messenger << " z: " << inval[2] << endl;

    axes.clear();
    Inertia.clear();
}


void Molecule::From(const Queue<Atom> & AtomPool) {
    mAtoms.set(AtomPool.n);

    DLinkedList<Atom> * last = AtomPool.root;


    for (int i=0; i<AtomPool.n; ++i) {
        const Atom & nAtom = last->key;

        mAtoms[i] = nAtom;
        mAtoms[i].num = i;


        //convierte a cartesianas si es necesario
        if (!nAtom.cart) {
            if (i==0) {
                Atoms[i].c.x  = 0;
                Atoms[i].c.y  = 0;
                Atoms[i].c.z  = 0;
            }
            else if (i==1) {
                Atoms[i].c.x  = 0;
                Atoms[i].c.y  = 0;
                Atoms[i].c.z  = nAtom.intc.R;
            }
            else if (i==2) {
                Atoms[i].c.y  = 0;
                Atoms[i].c.x = nAtom.intc.R * sin(nAtom.intc.a);

                if (nAtom.intc.at1 == 1)
                    Atoms[i].c.z = nAtom.intc.R * cos(nAtom.intc.a);
                else if (nAtom.intc.at1 == 2)
                    Atoms[i].c.z = Atoms[1].c.z - nAtom.intc.R * cos(nAtom.intc.a);
            }
			else {
				int at1 = nAtom.intc.at1-1;
				int at2 = nAtom.intc.at2-1;
				int at3 = nAtom.intc.at3-1;
                Atoms[i].c = Icoord2vec(nAtom.intc,Atoms[at1].c, Atoms[at2].c, Atoms[at3].c);
			}

        }

        last = last->next;
    }
}
