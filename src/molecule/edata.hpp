#ifndef __EDATA__
#define __EDATA__

#include <string>
#include <cinttypes>

#include "low/mem.hpp"
#include "basis/SPprototype.hpp"
#include "basis/atomprod.hpp"
#include "basis/atomdef.hpp"

#include "EFS/fock2e.hpp"
#include "molecule/prescreen.hpp"
#include "1eints/1electron.hpp"
#include "1eints/Jengine.hpp"

#include "linear/tensors.hpp"
#include "linear/newtensors.hpp"

#include "libechidna/libechidna.hpp"

#include "1eints/new/ModelCorePotential.hpp"
#include "1eints/new/EffectiveCorePotential.hpp"

#include "dft/dft.hpp"
#include "solvers/rsp.hpp"
#include "solvers/SCF.hpp"
#include "solvers/cis.hpp"

class Nucleus;
class GTObasis;

enum class eGuess        {CORE, KIN, SAVE, SAP, SAD, DFIT, ADMA, ANO};


/*
    every solver will contain the following:
    *) its own communicator (messenger), linked to that of the parent solver or object
    *) a hardware configuration object, also linked to its parent's
    *) a solver configuration object which will allow passing parameters directly from the parser to the internal state, while keeping the same public interface across versions

    the typical solver behaviour is the following
    *) data initialization; data about the problem that allows the solver to configure its own state to solve the problem efficiently
    *) complete input; to solve the problem
    *) actual computation; different strategies may be used
    *) completion; destruction of all intermediate data
    *) final destruction; once the final state has been used and the data is no longer useful
*/


// this god-like object contains all integral information, density matrices, system properties and a few functions
struct MolElData {

    bool OpenShell; //indica que tensores se deben usar
    bool Sparse;

    int nBFs;

    rAtom         * atoms; // list of the elements of each atom
    uint32_t       natoms;
    rAtomPrototypes InternalDefs;

    const GTObasis * basis; // i know, i know, i'll sanitize this once it's working


    Interactions    AtomInteractions;
    r2tensor<int>   AtomPairs;
    Echidna         EchidnaSolver;
    tensorpattern   TensorPattern;


    ExternalPotential      ExternalV;
    SystemDensity          Density;
    ModelCorePotential     Mcp;
    EffectiveCorePotential Ecp;

    DFTgrid                DFT;
    SCFsolver              SCF;
    CISsolver              CIS;
    RSPparams              RSP;

    Array<int> Relabel; // copy of the relabelled interactions


    // for CIS/TDA
    int cis_nstates=0;
    double cis_conv=0.0;
    // for exciton dimer CT
    int dimer_ct_nocc=2;
    int dimer_ct_nvir=2;


    // integrals
    symtensor2 S;     // overlap tensor
    symtensor2 T;     // kinetic energy
    symtensor2 V;     // e- nuclear potential
    symtensor2 H;     // core hamiltonian

    symtensor2 XV;    // external potential

    tensor2 X,Y,Z;    // (a|r|b) : these are actually symmetric

    symtensor2 D, Da, Db; // converged density matrices
    tensor2    C, Cb;     // converged orbitals

    tensor2    D2, F2;    // unpacked density & Fock


    // same integrals as above, just in a more
    // access-friendly memory layout
    symtensor Sp;
    symtensor Tp;
    symtensor Vp;
    symtensor Hp;

    symtensor XVp;

    symtensor Xp,Yp,Zp;

    symtensor DXp,DYp,DZp;
    tensor    PXp,PYp,PZp;
    tensor    LXp,LYp,LZp;

    tensor2 DX, DY, DZ;
    tensor2 PX, PY, PZ;
    tensor2 LX, LY, LZ;

    symtensor MCPp;
    symtensor ECPp;

    symtensor Dp, Dsp, Dap, Dbp;
    tensor    Cp, Cbp;


    double ENuc;   // nuclear Coulomb energy
    double ESCF;   // SCF energy



    bool AllocTensors;
    bool AllocSparse;


    double * En, * Enb; // orbital energies
    int npairs, nalpha, nbeta; // number of electrons

    // this belongs with the convergence acceleration options
    double iEsmear = 0;




    void CalcElectronic    (eGuess InitialGuess, eMethod ElectronMethod, double DMprecision, double HFprecision, double CDprecision, int maxSCFc,
                            std::string & DMload, std::string & DMsave, const std::vector<int> nuc_chg,
                            std::string JobType="", std::string InpDens="", tensor2* DensityPointer=NULL);

    void SortAtoms         (Array<int> & Label, const Array<Atom> & Atoms, const GTObasis & gBasis, double logminover);

    // all of this happens more or less sequentially
    // =============================================
    void SumBasisFunctions (const rAtom * Atoms, int nAtoms, const GTObasis & BasisSet);
    void InitTensors       (bool IsOpenShell);
    void InitPositions     (const GTObasis & BasisSet);
    void InitMcp           (const MCPbasis & McpBasis);
    void InitEcp           (const ECPbasis & EcpBasis);
    void CalcShellPairs    (const GTObasis & BasisSet, double logGDO);

    void Start1eGTO        (const std::vector<double>& center);
    void Start1eGTO        ();
    void Start2eGTO        ();


    void DoGuess    (symtensor & Fp, eGuess InitialGuess, eMethod ElectronMethod, const std::string & file, const std::vector<int> nuc_chg);

    void DoRMP2     (double & EElec);
    void DoRSP      ();
    void DoCMM      ();


    void LinearMultiRSP (multivec * ZZ, const multivec * RR, const double * wr, const double * wi, const int NW, const int NR,   const tensor2 & D0);

    void ComputeJ   ();

    MolElData() ;
    MolElData & operator=(const MolElData & edata);
    void InitDMs(bool OS);
    void InitSparseDMs(bool OS);
    ~MolElData();
};
#endif
