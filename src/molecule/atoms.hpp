#ifndef __ATOMS__
#define __ATOMS__

#include <string>
#include <map>
#include "defs.hpp"
#include "math/affine.hpp"
#include "math/smath.hpp"
#include "basis/atomdef.hpp"


struct Atom {

    ElementID type; // ID in AtomDefs
    uint16_t num;   // number tag in molecule/system
    point c;        // position in space
    icoord intc;    // internal coordinates for Z-matrix representation
    bool cart;      // whether or not the the atom is originally in carterian coordinates

    Atom & operator=(const Atom & rhs) {
        type = rhs.type;
        num  = rhs.num;
        c    = rhs.c;
        intc = rhs.intc;
        cart = rhs.cart;
        return *this;
    }
};

// one atomic nucleus
struct Nucleus {
    //ID
    std::string name;  // nombre
    uint32_t num;      // number tag in molecule/system

    //propiedades
    double mass;       // mass
    double Z;          // charge
    point c;           // position

    //para borrar
    ElementID type;    // ID in AtomDefs
    ElementID id;      // ID in reduced defs list

    icoord intc;       // internal coordinates for Z-matrix representation
    bool cart;         // whether or not the the atom is originally in carterian coordinates


    Nucleus & operator=(const Nucleus & rhs);
    Nucleus & operator=(const Atom & rhs);
};

#endif
