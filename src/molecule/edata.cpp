/*
    edata.cpp

    datos de estructura eletronica y de integrales;
    rutinas de inicializacion de dichos datos y de calculo y resolucion de la estructura electronica
*/

#include <fstream>
#include <sstream>
#include <set>
#include <exception>
#include <cassert>

#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "solvers/SCF.hpp"
#include "solvers/cis.hpp"
#include "molecule/edata.hpp"
#include "molecule/atoms.hpp"
#include "molecule/prescreen.hpp"
#include "basis/GTO.hpp"
#include "basis/atomprod.hpp"
#include "basis/shellpair.hpp"
#include "math/angular.hpp"
#include "math/modifiedbessel.hpp"
#include "low/chrono.hpp"
#include "low/io.hpp"
#include "low/strings.hpp"

#include "1eints/1electron.hpp"
#include "1eints/new/GenPot.hpp"
#include "1eints/new/GenEcp.hpp"
#include "1eints/new/ExternalMultipolePotential.hpp"

#include "dft/xcgrid/lebedev.hpp"

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
    #define omp_get_num_threads() 1
#endif

using namespace LibAngular;
using namespace Fiesta;

MolElData::MolElData() {
    natoms = 0;
    En = Enb = NULL;
    nBFs      = 0;
    AllocTensors = false;
    AllocSparse  = false;

    atoms = NULL;

    Echidna::EMessenger.SetParent   (&Messenger);
    Echidna::EBenchmarker.SetParent (&Benchmarker);
    Echidna::EDebugger.SetParent    (&Debugger);
}

//piensalo dos veces
MolElData & MolElData::operator=(const MolElData & edata) {
    //impide problemas con la autoasignacion
    if (this == &edata) return *this;

    return *this;
}

MolElData::~MolElData() {
    delete[] En;
    delete[] Enb;
    delete[] atoms;
}


void MolElData::InitTensors (bool IsOpenShell) {
    Sparse    = false; //IsSparse;
    OpenShell = IsOpenShell;

    {
        Sp.init(TensorPattern);     // overlap integrals

        Xp.init(TensorPattern);     //
        Yp.init(TensorPattern);
        Zp.init(TensorPattern);

        DXp.init(TensorPattern);
        DYp.init(TensorPattern);
        DZp.init(TensorPattern);

        PXp.init(TensorPattern);
        PYp.init(TensorPattern);
        PZp.init(TensorPattern);

        LXp.init(TensorPattern);
        LYp.init(TensorPattern);
        LZp.init(TensorPattern);

        Hp.init(TensorPattern);     // core hamiltonian
        Tp.init(TensorPattern);     // Kinetic energy
        Vp.init(TensorPattern);     // nuclear potential energy

        XVp.init(TensorPattern);    // external potential
        //Jp.init(TensorPattern);     // coulomb
        //Kp.init(TensorPattern);     // exchange

        Cp.init(TensorPattern);     // MO coefficients
        Dp.init(TensorPattern);     // density matrix

        MCPp.init(TensorPattern) ;
        ECPp.init(TensorPattern) ;

        if (OpenShell) {
            Cbp.init(TensorPattern);
            Dap.init(TensorPattern);
            Dbp.init(TensorPattern);
        }
    }


	if (!Sparse) {
        S.setsize(nBFs);
        T.setsize(nBFs);

        X.setsize(nBFs);
        Y.setsize(nBFs);
        Z.setsize(nBFs);

        V.setsize(nBFs);
        H.setsize(nBFs);

        XV.setsize(nBFs);

        //J.setsize(nBFs);
        //K.setsize(nBFs);

        C.setsize(nBFs);
        D.setsize(nBFs);

        D2.setsize(nBFs);
        F2.setsize(nBFs);

        if (OpenShell) {
            //Ca.clear();
            Cb.setsize(nBFs);
            Da.setsize(nBFs);
            Db.setsize(nBFs);

            Enb = new double[nBFs];
        }

        En = new double[nBFs];


		AllocTensors = true;
		AllocSparse  = false;
	}

}

void MolElData::SumBasisFunctions(const rAtom * Atoms, int nAtoms, const GTObasis & BasisSet) {

    nBFs = 0;
    for (int at=0; at<nAtoms; ++at) {
		ElementID type = Atoms[at].rAP->DId;

        for (int b=0; b<BasisSet[type].n; ++b) {
            int l = BasisSet[type][b].l;
            nBFs += nmS[l] * BasisSet[type][b].J;
        }
    }

    // make a hard copy of the elements' ID
    // ====================================
    natoms = nAtoms;
    atoms  = new rAtom[natoms];

    for (int at=0; at<nAtoms; ++at) atoms[at] = Atoms[at];

    // store a pointer to the basis set
    basis = &BasisSet;


    // reset the basis in the local copy
    // =================================
    {
        // generate  a reduced list of elements; generate a list of atoms and
        // link them to the new definitions; make a list of the atoms for every element

        std::set<ElementID> ATList;

        for (int i=0; i<nAtoms; ++i) {
            ElementID type = Atoms[i].rAP->DId;
            ATList.insert(type);
        }


        InternalDefs.prototypes.set(ATList.size());

        std::set<ElementID>::iterator it; ElementID Id=0;
        for (it=ATList.begin(); it!=ATList.end(); ++it, ++Id) {
            const AtomDef & atom = AtomDefs[*it];
            InternalDefs.prototypes[Id].name = atom.name;
            InternalDefs.prototypes[Id].mass = atom.mass;
            InternalDefs.prototypes[Id].Z    = atom.Z;

            InternalDefs.prototypes[Id].DId  = *it;
            InternalDefs.prototypes[Id].RId  =  Id;


            int nb = BasisSet[*it].n;
            InternalDefs.prototypes[Id].basis.set(nb);

            for (int b=0; b<nb; ++b) InternalDefs.prototypes[Id].basis[b] = BasisSet[*it][b];

            InternalDefs.prototypes[Id].Atoms  = NULL;
            InternalDefs.prototypes[Id].nAtoms = 0;
        }

        std::map<ElementID,ElementID> ATMap;
        ElementID RId=0;
        for (it=ATList.begin(); it!=ATList.end(); ++it, ++RId) ATMap[*it] = RId;


        for (int i=0; i<nAtoms; ++i) {
            ElementID DId = Atoms[i].rAP->DId;
            ElementID RId = ATMap[DId];
            atoms[i].rAP = &(InternalDefs.prototypes[RId]);

            ++InternalDefs.prototypes[RId].nAtoms;
        }


        for (ElementID rid=0; rid<InternalDefs.prototypes.N(); ++rid) {
            InternalDefs.prototypes[rid].Atoms = new rAtom[InternalDefs.prototypes[rid].nAtoms];
            InternalDefs.prototypes[rid].nAtoms = 0;
        }


        for (int i=0; i<nAtoms; ++i) {
            atoms[i].rAP->Atoms[atoms[i].rAP->nAtoms] = atoms[i];
            ++atoms[i].rAP->nAtoms;
        }
    }

}

void MolElData::InitPositions (const GTObasis & BasisSet) {

    uint32_t pos = 0;

    uint32_t* AtomP;
    AtomP = new uint32_t[natoms+1]; //indica donde comienzan los datos de cada atomo en los tensores

    for (uint32_t at=0; at<natoms; ++at) {
        AtomP[at] = pos;
		ElementID ID = atoms[at].rAP->DId;
		const int B = BasisSet[ID].n;

        for (int b=0; b<B; ++b) {
            const int L = BasisSet[ID][b].l; // BasisSet[Id][b].l;
            pos += nmS[L]* BasisSet[ID][b].J; //BasisSet[Id][b].J;
        }
    }

    AtomP[natoms] = pos; //store last position, too

    //inicia el patron de tensores
    Array<int> aAtomP(natoms);

    for (uint32_t at=0; at<natoms; ++at) {
        int t=0;
		ElementID ID = atoms[at].rAP->DId;
		const int B = BasisSet[ID].n;

        for (int b=0; b<B; ++b) {
            const int L = BasisSet[ID][b].l; // BasisSet[Id][b].l;
            t += nmS[L]* BasisSet[ID][b].J; //BasisSet[Id][b].J;
        }

        aAtomP[at] = t;
    }

    //MUST CHECK AGAIN!!!
    //double tensor_threshold = 1e-10;ĵ
    const double BFthresh = 1e-16;
    TensorPattern.SetPositions(aAtomP, BFthresh);

    // THIS BREAKS
    EchidnaSolver.Init (atoms, natoms);

    delete[] AtomP;
}

void MolElData::InitMcp (const MCPbasis & McpBasis) {

    if (McpBasis.Empty()) return;

    Messenger << "Initializing MCP" << std::endl;
    if (!Mcp.Empty()) throw Exception ("trying to initialize a non-empty MCP");

    //
    Mcp.Init(natoms);
    Mcp.nnuclei=0;

    for (int i=0; i<natoms; ++i)
    {
        ElementID type = atoms[i].rAP->DId;

        if (McpBasis.Contains(type))
        {
           // the following not particularly smart as it assumes the same number of
           // exp and expR Gaussians - i.e., not generally usefull..
          Mcp.positions[Mcp.nnuclei] = atoms[i].c;
          Mcp.Zeff[Mcp.nnuclei] = McpBasis.GetZeff(type);

          Mcp.ExpExp[Mcp.nnuclei] =McpBasis.GetExpExp(type);
          Mcp.CoeffExp[Mcp.nnuclei] =McpBasis.GetCoefExp(type);
          Mcp.ExpExp_r[Mcp.nnuclei] =McpBasis.GetExpExpR(type);
          Mcp.CoeffExp_r[Mcp.nnuclei] =McpBasis.GetCoefExpR(type);
          Mcp.ProjOrbs[Mcp.nnuclei] =McpBasis.GetProjOrbs(type);
          Mcp.EcpOrbs[Mcp.nnuclei] =McpBasis.GetEcpOrbs(type);
          Mcp.EcpMats[Mcp.nnuclei] =McpBasis.GetEcpMats(type);
          ++Mcp.nnuclei;
        }
    }

    Messenger << "MCP initialized for " << Mcp.nnuclei << " nuclei" << endl;
}

void MolElData::InitEcp (const ECPbasis & EcpBasis) {

    if (EcpBasis.Empty()) return;

    Messenger << "Initializing ECP" << std::endl;
    if (!Ecp.Empty()) throw Exception ("trying to initialize a non-empty ECP");

    Ecp.Init(natoms);
    Ecp.nnuclei=0;

    for (int i=0; i<natoms; ++i)
    {
        ElementID type = atoms[i].rAP->DId;

        if (EcpBasis.Contains(type))
        {
           Ecp.positions[Ecp.nnuclei] = atoms[i].c;
           Ecp.parameter[Ecp.nnuclei] = EcpBasis.GetEcpContr(type);
           ++Ecp.nnuclei;
        }
    }

    Messenger << "ECP initialized for " << Ecp.nnuclei << " nuclei" << endl;
}
// prescreens atom pairs and sorts them using the Cuthill-McKee algorithm
void MolElData::SortAtoms(Array<int> & Label, const Array<Atom> & Atoms, const GTObasis & gBasis, double logGDO) {

    //Cuthill McKee
    if (0) {
        Interactions IList;

        IList.PrescreenInteractions(Atoms, gBasis, logGDO);
        IList.CuthillMcKee(Label);
        AtomInteractions.From (IList, Label); //generates new relabelled atom interactions
    }
    //no order
    else if (1) {
        Label.set(Atoms.n);
        for (int i=0; i<Atoms.n; ++i) Label[i] = i;
        AtomInteractions.PrescreenInteractions(Atoms, gBasis, logGDO);
    }
    //Z order
    else if (0) {
        Interactions IList;
        IList.PrescreenInteractionsZorder(Label, Atoms, gBasis, logGDO);

        AtomInteractions.From (IList, Label);
    }
    //ordered by Z
    else if (0) {
        Interactions IList;
        IList.PrescreenInteractionsZ(Label, Atoms, gBasis, logGDO);

        AtomInteractions.From (IList, Label);
    }

    // synthetic method
    else if (0) {
        Interactions IList;
        IList.PrescreenInteractionsC(Label, Atoms, gBasis, logGDO);

        AtomInteractions.From (IList, Label);
    }

    else {
        Interactions IList;
        IList.PrescreenInteractionsGSC(Label, Atoms, gBasis, logGDO);

        AtomInteractions.From (IList, Label);
    }

    Relabel = Label;
}


void SetMonoElectronic(symtensor & T, const Interactions & AtomInteractions) {

    //inits tensors
    int nat = AtomInteractions.List.n;
	for (int at1=0; at1<nat; ++at1) {
        DLinkedList<int> * rint = AtomInteractions.List[at1].root;

        //at1 > at2
        for (rint = AtomInteractions.List[at1].root; rint!=NULL; rint=rint->next) {
            int at2 = rint->key;
            if (at2>=at1) break;
            T.touch(at1,at2);
        }
        T.touch(at1,at1);
	}
}


//calcula todos los productos de gausianas necesarios
void MolElData::CalcShellPairs (const GTObasis & BasisSet, double logGDO) {

    //Messenger << "Computing shell pairs " << endl;

    uint16_t * nAtomInts = new uint16_t[AtomInteractions.List.n];

    for (int i=0; i<AtomInteractions.List.n; ++i) nAtomInts[i] = AtomInteractions.List[i].n;

    AtomPairs.set(nAtomInts, AtomInteractions.List.n);

    delete[] nAtomInts;

    for (int i=0; i<AtomInteractions.List.n; ++i) {
        DLinkedList<int> * r = AtomInteractions.List[i].root;

        for (int j=0; j<AtomInteractions.List[i].n; ++j) {
            AtomPairs[i][j] = r->key;
            r = r->next;
        }
    }

    //Messenger << "Using " << AtomInteractions.GetTotN() << " atomic interactions from a set of " << natoms*natoms << endl;
    //Messenger << "Computing GTO products";
    //Messenger.Push(); {
        EchidnaSolver.ComputeAtomPairs (AtomPairs, logGDO);
    //} Messenger.Pop();
}


// the following three auxilliary functions should be moved along with the rest of BED integrals

void GetLebOrder (int & npoints, int nscheme){

    if (nscheme <= 15 && nscheme > 11) npoints = 86;
    else if (nscheme <= 11 && nscheme > 9 ) npoints = 50;
    else if (nscheme <= 9  && nscheme > 7 ) npoints = 38;
    else if (nscheme <= 7  && nscheme > 5 ) npoints = 26;
    else if (nscheme <= 5  && nscheme > 3 ) npoints = 14;
    else if (nscheme <= 3 ) npoints = 6;

}

void ArrayToVector3 (vector3 * xyz, double * x, double * y, double * z, int nlen){

    for (int i=0; i<nlen; i++){
        xyz[2*i].x = x[i];
        xyz[2*i].y = y[i];
        xyz[2*i].z = z[i];
        xyz[2*i+1].x = x[i];
        xyz[2*i+1].y = y[i];
        xyz[2*i+1].z = z[i];
    }
}

void GetLeb (vector3 * xyz, double * wt, int npoints){

    double * x = new double[npoints];
    double * y = new double[npoints];
    double * z = new double[npoints];
    wt = new double[npoints];

    ld_by_order(npoints, x, y, z, wt);
    ArrayToVector3 (xyz, x, y, z, npoints);
}




#include "math/gamma.hpp"

#include <cstdlib>
#include <string>

void CollectOperator (symtensor & Op, const string & filename) {

    symtensor2 M;

    M  = Op;          // transform to dense symtensor
    Tensor2Reduce(M); // dense reduce-scatter
    Op = M;           // now set all operators

    // write the matrix to file
    if (NodeGroup.IsMaster()) {
        //WriteTensorBin (filename + ".bin", M);
        //WriteTensor    (filename + ".mat", M);
    }
}

void CollectOperator (tensor & Op, const string & filename) {

    tensor2 M;

    M  = Op;          // transform to dense symtensor
    Tensor2Reduce(M); // dense reduce-scatter
    Op = M;           // now set all operators

    // write the matrix to file
    if (NodeGroup.IsMaster()) {
        //WriteTensorBin (filename + ".bin", M);
        //WriteTensor    (filename + ".mat", M);
    }
}

void MolElData::Start1eGTO () {
    std::vector<double> center;
    center.push_back(0.0);
    center.push_back(0.0);
    center.push_back(0.0);
    Start1eGTO(center);
}

//calcula las integrales monoelectronicas necesarias
void MolElData::Start1eGTO (const std::vector<double>& center) {

    //Messenger << "Initializing tensors" << endl;;

    //inits tensors

    // screams for a factory method
    Sp.touch(AtomInteractions);
    Tp.touch(AtomInteractions);
    Xp.touch(AtomInteractions);
    Yp.touch(AtomInteractions);
    Zp.touch(AtomInteractions);

    DXp.touch(AtomInteractions);
    DYp.touch(AtomInteractions);
    DZp.touch(AtomInteractions);

    PXp.touch(AtomInteractions);
    PYp.touch(AtomInteractions);
    PZp.touch(AtomInteractions);

    LXp.touch(AtomInteractions);
    LYp.touch(AtomInteractions);
    LZp.touch(AtomInteractions);

    Vp.touch(AtomInteractions);
    XVp.touch(AtomInteractions);

    MCPp.touch(AtomInteractions);
    ECPp.touch(AtomInteractions);

    // for iguess ?
    //Jp.touch(AtomInteractions);
    //Kp.touch(AtomInteractions);

    Dp.touch(AtomInteractions);

    C.setsize(nBFs);



    // for node parallelization
    const int NNODE = NodeGroup.GetTotalNodes();
    const int NRANK = NodeGroup.GetRank();


    const ShellPair * SPs = EchidnaSolver.GetShellPairs();
    const int NNSP        = EchidnaSolver.nShellPairs();

    Messenger << "Computing 1-electron integrals";
    Messenger.Push(); {

        #pragma omp parallel for schedule(dynamic)
        for (int i=NRANK; i<NNSP; i+=NNODE)
        {
            CalcMonoElectronic(SPs[i], atoms, Sp,Tp,Xp,Yp,Zp,DXp,DYp,DZp,PXp,PYp,PZp,LXp,LYp,LZp,center);
        }

        CollectOperator (Sp, "Sp");
        CollectOperator (Tp, "Tp");
        CollectOperator (Xp, "Xp");
        CollectOperator (Yp, "Yp");
        CollectOperator (Zp, "Zp");

        CollectOperator (DXp, "DXp");
        CollectOperator (DYp, "DYp");
        CollectOperator (DZp, "DZp");

        CollectOperator (PXp, "PXp");
        CollectOperator (PYp, "PYp");
        CollectOperator (PZp, "PZp");

        CollectOperator (LXp, "LXp");
        CollectOperator (LYp, "LYp");
        CollectOperator (LZp, "LZp");

        X = Xp; Y = Yp; Z = Zp;

    } Messenger.Pop();

    Messenger.precision(12);

    DX = DXp; DY = DYp; DZ = DZp;
    PX = PXp; PY = PYp; PZ = PZp;
    LX = LXp; LY = LYp; LZ = LZp;

    Messenger << "Computing nuclear-electronic interaction energy tensor";
    Messenger.Push(); {
        //classic (and more regular) O(N^2) algorithm
        #pragma omp parallel for schedule(dynamic)
        for (int i=NRANK; i<NNSP; i+=NNODE)
        {
            CalcPotentialCart(SPs[i],  atoms, natoms, Vp);
        }

        CollectOperator (Vp, "Vp");

    } Messenger.Pop();


    if(const char* file = std::getenv("FIESTA_EXT")) {
        ReadTensorBin (XV, file);
        XVp = XV;
    }
    else {
        Messenger << "Computing external potential integrals";
        Messenger.Push(); {
            // classic (and more regular) O(N^2) algorithm
            #pragma omp parallel for schedule(dynamic)
            for (int i=NRANK; i<NNSP; i+=NNODE)
            {
                CalcExternalPotential(SPs[i],  atoms, ExternalV, XVp);
                if (ExternalV.MC > 0)
                {
                   ExternalMultipolePotential new_pot(ExternalV);
                   NewInts::CalcExtMPPotentialCart(SPs[i],  atoms, natoms, new_pot, XVp);
                }
            }

            CollectOperator (XVp, "XVp");

        } Messenger.Pop();
    }

    if (!Mcp.Empty()) {

        const char* file = std::getenv("FIESTA_MCP");

        if (file) {
            Messenger << "Reading model core potential integrals from file : " << file << endl;
            ReadTensorBin (D, file);
            MCPp = D;
        }
        else {
            Messenger << "Computing model core potential integrals";



            Messenger.Push(); {

                // touch is not thread-safe
                for (int n=0; n<natoms; ++n)
                    for (int m=0; m<=n; ++m)
                        MCPp.touch(n,m); // in case the block does not exist

                #pragma omp parallel
                {
                    int it=0;

                    const int NTHREADS = omp_get_num_threads();
                    const int THREADID = omp_get_thread_num();

                    const int NPROCS = NNODE*NTHREADS;            // total number of processing elements
                    const int PROCID = NRANK*NTHREADS + THREADID; // combined ID of the PE (MPI + OMP)


                    for (int n=0; n<natoms; ++n) {
                        for (int m=0; m<n; ++m) {

                            for (int i=0, osa=0; i<atoms[n].rAP->basis.N(); ++i) {
                                const GTO & GTOA = atoms[n].rAP->basis[i];

                                for (int j=0,osb=0; j<atoms[m].rAP->basis.N(); ++j) {
                                    const GTO & GTOB = atoms[m].rAP->basis[j];

                                    if (it%NPROCS == PROCID) NewInts::CalcMCPPotentialCart (atoms[n].c, atoms[m].c, GTOA, GTOB, Mcp, *MCPp(n,m),  osa,osb);

                                    ++it;

                                    osb += GTOB.J * (2*GTOB.l+1); // update in-block offset
                                }
                                osa += GTOA.J * (2*GTOA.l+1); // update in-block offset
                            }
                        }
                        {
                            for (int i=0, osa=0; i<atoms[n].rAP->basis.N(); ++i) {
                                const GTO & GTOA = atoms[n].rAP->basis[i];

                                for (int j=0, osb=0; j<=i; ++j) {
                                    const GTO & GTOB = atoms[n].rAP->basis[j];

                                    if (it%NPROCS == PROCID) NewInts::CalcMCPPotentialCart (atoms[n].c, atoms[n].c, GTOA, GTOB, Mcp, *MCPp(n,n),  osa,osb);

                                    ++it;

                                    osb += GTOB.J * (2*GTOB.l+1); // update in-block offset
                                }
                                osa += GTOA.J * (2*GTOA.l+1); // update in-block offset
                            }
                        }
                    }
                }

                CollectOperator (MCPp, "MCp");

            } Messenger.Pop();
        }
    }

    if (!Ecp.Empty()) {

        const char* file = std::getenv("FIESTA_ECP");

        if (file) {
            Messenger << "Reading effective core potential integrals from file : " << file << endl;
            ReadTensorBin (D, file);
            ECPp = D;
        }
        else {
            Messenger << "Computing effective core potential integrals";

            // this is an extraordinarily poor parallelization scheme;
            // a better way of parallelizing this is by looping over rAP first
            // then over the relevant atoms in the atom list,

            Messenger.Push(); {
                // touch is not thread-safe
                for (int n=0; n<natoms; ++n)
                    for (int m=0; m<=n; ++m)
                        ECPp.touch(n,m); // in case the block does not exist

                #pragma omp parallel
                {
                    int it=0;


                    const int NTHREADS = omp_get_num_threads();
                    const int THREADID = omp_get_thread_num();

                    const int NPROCS = NNODE*NTHREADS;            // total number of processing elements
                    const int PROCID = NRANK*NTHREADS + THREADID; // combined ID of the PE (MPI + OMP)


                    for (int n=0; n<natoms; ++n) {
                        for (int m=0; m<n; ++m) {

                            for (int i=0, osa=0; i<atoms[n].rAP->basis.N(); ++i) {
                                const GTO & GTOA = atoms[n].rAP->basis[i];

                                for (int j=0,osb=0; j<atoms[m].rAP->basis.N(); ++j) {
                                    const GTO & GTOB = atoms[m].rAP->basis[j];

                                    if (it%NPROCS == PROCID) EcpInts::GenEcp (atoms[n].c, atoms[m].c, GTOA, GTOB,    Ecp, *ECPp(n,m), osa, osb );

                                    ++it;

                                    osb += GTOB.J * (2*GTOB.l+1); // update in-block offset
                                }
                                osa += GTOA.J * (2*GTOA.l+1); // update in-block offset
                            }
                        }
                        {
                            for (int i=0, osa=0; i<atoms[n].rAP->basis.N(); ++i) {
                                const GTO & GTOA = atoms[n].rAP->basis[i];

                                for (int j=0, osb=0; j<=i; ++j) {
                                    const GTO & GTOB = atoms[n].rAP->basis[j];

                                    if (it%NPROCS == PROCID) EcpInts::GenEcp (atoms[n].c, atoms[n].c, GTOA, GTOB,   Ecp, *ECPp(n,n), osa, osb );

                                    ++it;

                                    osb += GTOB.J * (2*GTOB.l+1); // update in-block offset
                                }
                                osa += GTOA.J * (2*GTOA.l+1); // update in-block offset
                            }
                        }
                    }
                }

                CollectOperator (ECPp, "ECp");

            } Messenger.Pop();
        }
    }

    // this is for RSP and should go elsewhere
    if (0) {
        int npoints = 38;
        double * wt;
        double norm;

        GetLebOrder(npoints, 2);

        vector3 kDirection[npoints][2]; //  = new vector3[N][2];
        vector3 eDirection[npoints][2]; // = new vector3[N][2];
        symtensor ApReal[npoints][2]; // = new symtensor[N][2];
        symtensor ApImag[npoints][2]; //ApImag = new symtensor[N][2];

        GetLeb(&kDirection[0][0], wt, npoints);

        for (int i=0; i<npoints; ++i) {
    //      Generation of first polarization vector
            if (kDirection[i][0].x == 0.0 && kDirection[i][0].y == 0.0 ){
                eDirection[i][0].x = 1.0;
                eDirection[i][0].y = 0.0;
                eDirection[i][0].z = 0.0;
            }
            else {
                eDirection[i][0].x =  kDirection[i][0].y;
                eDirection[i][0].y = -kDirection[i][0].x;
                eDirection[i][0].z = 0.0;
            }
            norm = sqrt(eDirection[i][0]*eDirection[i][0]);
            eDirection[i][0] = eDirection[i][0]/norm;

    //      Generation of second polarization vector
            eDirection[i][1] = kDirection[i][0]|eDirection[i][0];
            norm = sqrt(eDirection[i][1]*eDirection[i][1]);
            eDirection[i][1] = eDirection[i][1]/norm;

            for (int j=0; j<2; j++) {
                ApReal[i][j].init();
                ApImag[i][j].init();
                ApReal[i][j].touch(AtomInteractions);
                ApImag[i][j].touch(AtomInteractions);
            }
        }


    //  NHL 31032017: Call for light-matter interaction integrals
        Messenger << "Computing A*p light-matter interaction integrals";
        Messenger.Push(); {
            //classic (and more regular) O(N^2) algorithm
            #pragma omp parallel for schedule(dynamic)
            for (int i=0; i<NNSP; ++i)
                CalcXRayProperty(SPs[i],  atoms, &ApReal[0][0], &ApImag[0][0], &kDirection[0][0], &eDirection[0][0], 2*npoints);
        } Messenger.Pop();

    }


    // collect all nuclear potentials & core pseudopotentials
    Vp += XVp;
    Vp += MCPp;
    Vp += ECPp;

    // build core Hamiltonian
    Hp  = Tp;
    Hp += Vp;
}

//calcula las integrales bielectronicas
void MolElData::Start2eGTO () {

    if (DFT.use_dft) {
        Messenger << endl;
        Messenger << "DFT exact exchange = " << DFT.xs_w << endl;
        if (DFT.range_separated) {
            Messenger << "CAM alpha, beta, mu = " << DFT.alpha_w  << " " << DFT.beta_w << " " << DFT.mu_w << endl;
        }

        if      (DFT.range_separated) EchidnaSolver.InitFock2e(DFT.alpha_w, DFT.beta_w, DFT.mu_w);
        else if (DFT.global_hybrid)   EchidnaSolver.InitFock2e(DFT.xs_w);
        else                          EchidnaSolver.InitFock2e(0.); // no exchange
    }
    else {
        EchidnaSolver.InitFock2e(1.); // full exchange
    }

}


void MolElData::CalcElectronic (enum eGuess InitialGuess, enum eMethod ElectronMethod,
                                double CSthreshold, double SCFprecision, double CDprecision, int maxSCFc,
                                std::string & DMload, std::string & DMsave, const std::vector<int> nuc_chg,
                                std::string JobType, std::string InpDens,
                                tensor2* DensityPointer) {


    symtensor Fp; // initial guess
    Fp.init(TensorPattern);

    if (JobType == "EXMOD_DIMER" || JobType == "TEST") {
        DoGuess (Fp, eGuess::CORE, ElectronMethod, DMload, nuc_chg);
    } else {
        DoGuess (Fp, InitialGuess, ElectronMethod, DMload, nuc_chg);
    }

    SCF.SetSystem  (Sp, Hp, nalpha, nbeta, ENuc, ElectronMethod);
    SCF.SetSolvers (&EchidnaSolver, &DFT);
    SCF.SetIguess  (Fp);
    SCF.SetCD      (CDprecision);


    SCF.SetDipoleInts(DX, DY, DZ);
    SCF.SetNablaInts (PX, PY, PZ);
    SCF.SetAngMomInts(LX, LY, LZ);


    if (JobType == "EXMOD_DIMER") {
        SCF.ReadDensityFromTensor(DensityPointer);
    } else if (JobType == "TEST") {
        SCF.ReadDensity(InpDens);
    }

    SCF.SetParams  (DMsave,  CSthreshold, SCFprecision, maxSCFc);

    Messenger << "Solving SCF";
    Messenger.Push(); {
        if (JobType == "EXMOD_DIMER" || JobType == "TEST") {
            SCF.AssembleFock();
        } else {
            SCF.RunSCF();
        }
    } Messenger.Pop();


    // optimize basis set
    if (ElectronMethod == eMethod::BSO)
        SCF.RunBasisOptimizer (atoms, natoms);


    ESCF = SCF.GetSCFenergy();
    D2   = SCF.GetDensity();



    Messenger << endl;

    Messenger << "Electronic energy : ";
    Messenger.width(26);
    Messenger << ESCF << endl;

    Messenger << "Nuclear energy    : ";
    Messenger.width(26);
    Messenger << ENuc << endl;

    Messenger << "Total energy      : ";
    Messenger.width(26);
    Messenger << ESCF + ENuc << endl;

    Messenger.width(6);

    Messenger << endl;


    if (JobType == "EXMOD_MONOMER" || JobType == "CIS") {
        // skip CIS for open-shell system
        if (ElectronMethod == eMethod::RHF || ElectronMethod == eMethod::RKS) {
            CIS.InitCIS(&SCF, &EchidnaSolver, &DFT);
            if (cis_nstates > 0) { CIS.set_nstates(cis_nstates); }
            if (cis_conv  > 0.0) { CIS.set_conv(cis_conv);       }
            CIS.RunCIS();
        }
    }

    if (JobType == "EXMOD_DIMER") {
        // skip CIS for open-shell system
        if (ElectronMethod == eMethod::RHF || ElectronMethod == eMethod::RKS) {
            CIS.InitCIS(&SCF, &EchidnaSolver, &DFT);
            CIS.RunDimerCIS(DensityPointer, dimer_ct_nocc, dimer_ct_nvir);
        }
    }


    // post-HF methods
    if (ElectronMethod == eMethod::RHFRSP) {
        Messenger << "Solving linear response";
        Messenger.Push();
        DoRSP ();
        Messenger.Pop();
    }

    if (ElectronMethod == eMethod::RMP2) {
        Messenger << "Solving MP2";
        Messenger.Push();
        double EnMP2;
        DoRMP2 (EnMP2);
        Messenger.Pop();
    }


}


