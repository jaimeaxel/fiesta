#include <cstring>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "matrix.hpp"

Matrix::Matrix() : nrows_(0), ncols_(0) {}
Matrix::Matrix(int n) { setsize(n,n); }
Matrix::Matrix(int n, int m) { setsize(n,m); }

Matrix::~Matrix() {}

void Matrix::setsize(int n)
{
    setsize(n,n);
}

void Matrix::setsize(int n, int m)
{
    nrows_ = n;
    ncols_ = m;
    data_.resize(nrows_*ncols_);
}

void Matrix::zeroize()
{
    memset(data_.data(), 0, sizeof(double)*nrows_*ncols_);
}

void Matrix::check_size(int n, int m)
{
    assert(nrows_ == n);
    assert(ncols_ == m);
}

std::ostream & operator<<(std::ostream & os, const Matrix & D)
{
    int col = 0;
    int incr = 5;

    while (col < D.ncols()) {

        os << std::setw(7) << "";
        for (int j = col; j < std::min(D.ncols(), col+incr); j++) {
            os << std::setw(15) << j;
        }
        os << std::endl;

        for (int i = 0; i < D.nrows(); i++) {
            os << std::setw(7) << i;
            for (int j = col; j < std::min(D.ncols(), col+incr); j++) {
                os << std::setw(15) << std::setprecision(8) << D[i][j];
            }
            os << std::endl;
        }
        os << std::endl;

        col += incr;
    }

    return os;
}

extern "C" void dsyev_(const char * jobz, const char * uplo,
                       const int * n, double * a, const int * lda,
                       double * w, double * work, const int * lwork, int * info);

void DiagonalizeMatrix(const Matrix& Mat, Matrix& evec, std::vector<double>& eval)
{
    int n = Mat.nrows();

    evec = Mat;
    eval.resize(n);

    int info, lwork;
    double wkopt;
    double * work;

    const char JOBZ = 'V'; // compute eigenvalues and eigenvectors
    const char UL   = 'L'; // matrix storage patern

    lwork = -1;
    dsyev_( &JOBZ, &UL, &n, evec.data(), &n, eval.data(), &wkopt, &lwork, &info );
    lwork = (int)wkopt;
    work = new double[lwork];

    dsyev_( &JOBZ, &UL, &n, evec.data(), &n, eval.data(), work, &lwork, &info );

    if (info>0) {
        std::cout << std::endl;
        std::cout << "Error: Failed to diagonalize matrix (info="  << info << ")!" << std::endl;
        std::cout << std::endl;
    }

    delete[] work;
}
