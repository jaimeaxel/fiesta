#include <string>
#include <sstream>
#include <iostream>
#include "interface.hpp"
#include "matrix.hpp"

// ==> DataInfo <===

DataInfo::DataInfo() {}
DataInfo::~DataInfo() {}

DataInfo::DataInfo(const DataInfo& info)
{
    (*this) = info;
}

std::string DataInfo::string() const
{
    std::stringstream ss;
    ss << "MPI master:        " << (is_master?"yes":"no") << std::endl;
    ss << "Number of atoms:   " << num_atoms << std::endl;
    ss << "Input file:        " << input_xyz << std::endl;
    ss << "Fragment info:     " << input_frag << std::endl;
    return ss.str();
}

int DataInfo::get_num_atoms() const
{
    return num_atoms;
}

std::string DataInfo::get_input_xyz() const
{
    return input_xyz;
}

std::string DataInfo_str_ (const DataInfo& self)
{
    return self.string();
}

// ==> DataRequest <===

DataRequest::DataRequest() {}
DataRequest::~DataRequest() {}

DataRequest::DataRequest(const DataRequest& req)
{
    (*this) = req;
}

std::string DataRequest::string() const
{
    std::stringstream ss;
    ss << "Input file:           " << input_xyz << std::endl;
    ss << "Job type:             " << job_type << std::endl;
    ss << "Number of matrices:   " << num_dens << std::endl;
    return ss.str();
}

std::string DataRequest_str_ (const DataRequest& self)
{
    return self.string();
}

// ==> DataResult <===

DataResult::DataResult() {}
DataResult::~DataResult() {}

DataResult::DataResult(const DataResult& res)
{
    (*this) = res;
}

std::string DataResult::string() const
{
    std::stringstream ss;
    ss << "SCF energy:           " << E << std::endl;
    ss << "Alpha electrons:      " << nocc << std::endl;
    ss << "Atomic orbitals:      " << Da.nrows() << std::endl;
    ss << "Molecular orbitals:   " << C.nrows() << std::endl;
    return ss.str();
}

std::string DataResult_str_ (const DataResult& self)
{
    return self.string();
}
