#ifndef _INTERFACE_HPP_
#define _INTERFACE_HPP_

#include <string>
#include "matrix.hpp"

class DataInfo {
  public:
    bool is_master;
    int num_atoms;
    std::string input_xyz;
    std::string input_frag;

    DataInfo();
    DataInfo(const DataInfo& info);
    ~DataInfo();

    std::string string() const;

    int get_num_atoms() const;
    std::string get_input_xyz() const;
};

std::string DataInfo_str_ (const DataInfo& self);

class DataRequest {
  public:
    std::string input_xyz;
    std::string job_type;
    int num_dens;

    Matrix Da;
    Matrix Db;
    Matrix C;

    Matrix CI_vecs_A;
    Matrix CI_vecs_B;

    int nocc_A;
    int nvir_A;
    int nocc_B;
    int nvir_B;

    int index_A;
    int index_B;

    DataRequest();
    DataRequest(const DataRequest& req);
    ~DataRequest();

    std::string string() const;
};

std::string DataRequest_str_ (const DataRequest& self);

class DataResult {
  public:
    double E;
    int nocc;

    Matrix Da;
    Matrix Db;
    Matrix C;

    Matrix F_MO;

    Matrix E_cis;
    Matrix CI_vecs;

    Matrix e_tr_dip;
    Matrix v_tr_dip;
    Matrix m_tr_dip;

    Matrix LE_LE;
    Matrix LE_CT;
    Matrix CT_CT;

    DataResult();
    DataResult(const DataResult& res);
    ~DataResult();

    std::string string() const;
};

std::string DataResult_str_ (const DataResult& self);

#endif
