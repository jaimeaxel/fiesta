#ifndef _MATRIX_HPP_
#define _MATRIX_HPP_

#include <vector>
#include <iostream>

class Matrix
{
  private:
    std::vector<double> data_;
    int nrows_;
    int ncols_;

  public:
    Matrix();
    Matrix(int n);
    Matrix(int n, int m);
    ~Matrix();

    void setsize(int n);
    void setsize(int n, int m);
    void zeroize();

    const int nrows() const { return nrows_; }
    const int ncols() const { return ncols_; }
    const double* operator[](int i) const { return data_.data() + i * ncols_; }
    double*       operator[](int i)       { return data_.data() + i * ncols_; }

    const int size() const { return data_.size(); }
    double* data() { return data_.data(); }

    void check_size(int n, int m);
};

std::ostream & operator<<(std::ostream & os, const Matrix & D);

void DiagonalizeMatrix(const Matrix& Mat, Matrix& evec, std::vector<double>& eval);

#endif
