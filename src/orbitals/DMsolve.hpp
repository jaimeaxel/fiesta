#ifndef __DMSOLVE__
#define __DMSOLVE__

class tensor2;

// compute the [F,D]s commuter
void CalcCommuter(tensor2 & X, const tensor2 & F, const tensor2 & D);

// purify the density matrix using various schemes
double McWeenyPurification (tensor2 & D, const tensor2 & S);
double McWeenyPurification (tensor2 & D);
double TC2Purification     (tensor2 & D, const tensor2 & S);


// computes density matrix, return energies, coefficients
void CalcDmat  (tensor2 & D, double * En, const tensor2 & H, int n, int o, tensor2 & C);
void CalcDmat  (tensor2 & D, tensor2 & C, double * En, const tensor2 & H, int n, int o);

// returns the denisty matrix components corresponding to the [o...n] canonical orbitals, previously computed on C
void AssembleCanonicalOrbitals (tensor2 * D, tensor2 & C, int o, int n);


// these may be useful
void CalcHfermi(tensor2 & D, double * En, const tensor2 & H, const tensor2 & L, int n, double bb);
void CalcDentropy (const tensor2 & D, const tensor2 & S, const tensor2 & L);


#endif
