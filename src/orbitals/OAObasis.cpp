/*
    ortho.cpp

    rutinas para calcular una base ortogonal a partir de una matriz S
    de productos internos (integrales de solapamiento)
*/


#include <cmath>
#include "defs.hpp"
#include "fiesta/fiesta.hpp"
#include "low/io.hpp"
#include "linear/eigen.hpp"
#include "linear/sparse.hpp"
#include "linear/tensors.hpp"
#include "linear/newtensors.hpp"
#include "fiesta/calculate.hpp"
using namespace Fiesta;


double SAOTHRESH = 1.e-8; // threshold for discarding orthonormal bases close to linear dependency


void CalcBaseSqrt (const tensor2 & S, tensor2 & C) {

    const int nAO = S.n;

    double* av = new double [nAO];

    tensor2 D = S;

    bool descending_order = true;
    DiagonalizeV (D, av, descending_order);

    const double thresh = SAOTHRESH * av[0]; // minimum admissible eigenvalue

    // count number of MOs
    int nMO = 0;
    for (int i = 0; i < nAO; i++) {
        if (av[i] >= thresh) { nMO ++; }
    }

    Messenger << "Number of Atomic Oribtals:      " << nAO << endl;
    Messenger << "Number of Molecular Oribtals:   " << nMO << endl;

    // now compute S^-1/2
    // ==================

    // set the shape of the basis matrix
    C.setsize(nAO, nMO);
    C.zeroize();

    for (int i=0; i<nMO; ++i) { av[i] = 1./sqrt(av[i]); }

    // transpose D and scale
    D.Transpose();
    for (int j=0; j<nAO; ++j)
        for (int i=0; i<nMO; ++i)
            C(j,i) = D(j,i)*av[i];

    delete[] av;
};


void CalcBaseSqrt (const symtensor2 & S, tensor2 & C) {
    tensor2 S2;
    S2 = S;
    CalcBaseSqrt (S2, C);
}

// two-step basis set; matrix B for block-preconfitioning, and matrix C for
void CalcBasePC(const symtensor & S, tensor2 & B, tensor2 & C) {

    const int M = S.gettotn();

    // compute the block preconditioner
    {
        B.setsize(M);
        B.zeroize();

        const int N = S.getn();

        int os=0;

        for (int n=0; n<N; ++n) {
            const tensor2 * T = S (n,n);

            tensor2    CC(T->n);
            CalcBaseSqrt (*T, CC);

            int len = S.getpos(n);

            for (int ii=0; ii<len; ++ii) {
                for (int jj=0; jj<len; ++jj) {
                    B(os+ii,os+jj) = CC (ii,jj);
                }
            }


            os += len;
        }
    }

    // transform S and compute the preconditioned basis
    {
        tensor2    S2, T;
        S2 = S;

        TensorProduct    (T, S2, B);
        TensorProductNTN (S2, B, T);

        S2.Symmetrize(); S2 *= 0.5;

        C.setsize (M);
        CalcBaseSqrt(S2, C);
    }

};


void CalcBase (tensor2 & C)  {
    Cholesky (C);
    InvChol  (C);
}

void CalcBase (const    tensor2 & S, tensor2 & C)  {
    C = S;
    CalcBase (C);
}

void CalcBase (const symtensor2 & S, tensor2 & C)  {
    C = S;
    CalcBase (C);
}




// transforms an operator evaluated in AOs to the orthonormal basis
// (possibly losing orbitals)
void AO2basis (tensor2 & T, const tensor2 & TT, const tensor2 & L) {
    tensor2 TL;
    TensorProduct    (TL, TT, L);
    TensorProductNTN (T,  L, TL);
    T.Symmetrize();
    T *= 0.5;
}

// transforms an operator of the density type from the orthonormal basis to the AO representation
// (possibly losing orbitals)
void basis2AO (tensor2 & D, const tensor2 & DD, const tensor2 & L) {
    tensor2 DL;
    TensorProductNNT (DL, DD, L);
    TensorProduct    (D,  L, DL);
    D.Symmetrize();
    D *= 0.5;
}



void AO2basis (tensor2 & D, const tensor2 & DD, const tensor2 & L, const tensor2 & S) {

    tensor2 SL;
    TensorProduct    (SL, S,  L);

    AO2basis (D, DD, SL);
}

void basis2AO (tensor2 & T, const tensor2 & TT, const tensor2 & L, const tensor2 & S) {

    tensor2 SL;
    TensorProduct    (SL, S,  L);

    basis2AO (T, TT, SL);
}







