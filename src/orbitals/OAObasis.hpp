#ifndef __ORTHO__
#define __ORTHO__


class tensor2;
class symtensor;

// misleading name at the moment; the resulting basis is not symmetric, i.e. it's not S^(-1/2)
void CalcBaseSqrt  (const symtensor2 & S, tensor2 & C);
void CalcBaseSqrt  (const    tensor2 & S, tensor2 & C);

// computes basis with Cholesky
void CalcBase      (const symtensor2 & S, tensor2 & C);
void CalcBase      (const    tensor2 & S, tensor2 & C);

// two-step basis; B for block-preconditioning and C for the remaining basis
void CalcBasePC    (const symtensor & S, tensor2 & B, tensor2 & C);


// transforms an operator projection evaluated in AOs to the orthonormal basis
void AO2basis (tensor2 & T, const tensor2 & TT, const tensor2 & L);

// transforms an operator of the density type from the orthonormal basis to the AO representation
void basis2AO (tensor2 & D, const tensor2 & DD, const tensor2 & L);

// transforms a density-type projection from AO space to orthonormalized basis
void AO2basis (tensor2 & D, const tensor2 & DD, const tensor2 & L, const tensor2 & S);

// transforms an operator projection in the L basis back to AOs
void basis2AO (tensor2 & T, const tensor2 & TT, const tensor2 & L, const tensor2 & S);



extern double SAOTHRESH; // threshold for discarding linearly-dependent OAOs

#endif
