/*
    DMsolve.cpp

    rutinas para resuolver la matriz densidad a partir de un hamiltoniano dado,
    tanto para matrices densas como sparse
*/


#include <string>
#include <cmath>

#include "defs.hpp"
#include "fiesta/fiesta.hpp"

#include "low/chrono.hpp"
#include "linear/tensors.hpp"
#include "linear/newtensors.hpp"
#include "linear/eigen.hpp"
#include "fiesta/calculate.hpp"


#include "orbitals/OAObasis.hpp"


using namespace std;
using namespace Fiesta;


void CalcCommuter(tensor2 & X, const tensor2 & F, const tensor2 & D) {
    TensorProduct (X, F, D);
    X.AntiSymmetrize();
}


double McWeenyPurification (tensor2 & D, const tensor2 & S) {

    // 3 D S D - 2 D S D S D
    tensor2 DS;
    TensorProduct (DS, D, S);
    tensor2 DSDS;
    TensorProduct (DSDS, DS, DS);

    DS   *= 3;
    DSDS *= 2;
    DS -= DSDS;

    TensorProduct (DSDS, DS, D);

    // check norm
    D -= DSDS;
    double D2 = Contract(D,D);

    D = DSDS;

    return D2;
}

double McWeenyPurification (tensor2 & D) {

    // 3 D^2 - 2 D^3
    tensor2 D2, D3;
    TensorProduct (D2, D,  D);
    TensorProduct (D3, D2, D);

    D2 *= 3;
    D3 *= 2;

    D2 -= D3; // purified

    // check frobenius norm
    D -= D2;
    double F2 = Contract(D,D);

    D = D2;

    return F2;
}

double TC2Purification (tensor2 & D, const tensor2 & S) {

    // D = D S D or D = 2D-DSD

    tensor2 DS;
    TensorProduct (DS, D, S);
    tensor2 DSD;
    TensorProduct (DSD, DS, D);

    double D2 = Contract(DSD,S);
    D = DSD;
    return D2;
}


void CalcDmat  (tensor2 & D, tensor2 & C, double * En, const tensor2 & H, int n, int o) {

    // diagonalize H in OAO basis
    C = H;
    DiagonalizeV(C, En);

    //Messenger << "Extreme eigenvalues:     " << En[0] << " " << En[N-1] << endl;
    //Messenger << "HOMO - LUMO eigenvalues: " << En[n-1] << " " << En[n] << endl;
    //Messenger << "HOMO - LUMO gap:         " << En[n] - En[n-1] << endl;

    tensor2 V (n-o, C.n, C[o]);
    TensorProductNTN(D, V, V);
}

void CalcDmat  (tensor2 & D, double * En, const tensor2 & H, int n, int o, tensor2 & C) {

    // diagonalize H in OAO basis
    C = H;
    DiagonalizeV(C, En);

    //Messenger << "Extreme eigenvalues:     " << En[0] << " " << En[NMO-1] << endl;
    //Messenger << "HOMO - LUMO eigenvalues: " << En[n-1] << " " << En[n] << endl;
    //Messenger << "HOMO - LUMO gap:         " << En[n] - En[n-1] << endl;

    tensor2 V (n-o, C.n, C[o]);
    TensorProductNTN(D, V, V);
}


void AssembleCanonicalOrbitals (tensor2 * D, tensor2 & C, int o, int n) {

    int NMO = C.n;
    int NAO = C.m;

    Messenger.Push("Assembling canonical orbitals");
    for (int k=o; k<n; ++k) {
        D[k-o].setsize(NAO,NAO); // just in case
        tensor2 V(n-o, NAO, C[o]);
        TensorProductNTN (D[k-o], V, V);
    }
    Messenger.Pop();
}


double FermiCount (const double * ee, int N, double bb, double mu) {
    double nn=0;

    for (int j=0; j<N; ++j) {
        if (ee[j] < mu)
            nn += 1. / (1. + exp(bb * (ee[j]-mu) ) );
        else
            nn += exp(bb * (mu-ee[j]) ) / (1. + exp(bb * (mu-ee[j]) ) );
    }

    return nn;
}

double FermiCount (const double * ee, int M, double bm) {

    double nn=0;

    for (int j=0; j<M; ++j) {
        nn += 1. / (bm + ee[j]);
    }

    return bm*nn;
}

void FermiSmear (double * ee, int M, double bm) {

    for (int j=0; j<M; ++j) {
        ee[j] = bm / (bm + ee[j]);
    }
}






// ff : returns fractional occupation
// mu : fermi energy
// N  : number of fermions
// M  : number of states
// ee : state energies
// bb : thermodynamics beta
bool FermiSmearing (double * ff, double & mu, const double * ee, int N, int M, double bb) {


    double nn;
    double EL;
    double EH;

    for (int j=0; j<M; ++j) ff[j] = exp(bb * ee[j]); // precompute

    // bracket the occupancies
    {
        // find uppor and lower energy bounds:
        int nl=N;
        int nh=N-1;

        do {
            --nl;
            if (nl==-1) break;
            double bm = exp(bb * ee[nl]);
            nn = FermiCount(ff,M, bm);
            //nn = FermiCount(ee,M, bb, ee[nl]);
        } while (nn>double(N));

        do {
            ++nh;
            if (nh==M) break;
            double bm = exp(bb * ee[nh]);
            nn = FermiCount(ff,M, bm);
            //nn = FermiCount(ee,M, bb, ee[nh]);
        } while (nn<double(N));

        if (nl==-1) return false; // not possible
        if (nh==M)  return false; // not possible

        EL = ee[nl];
        EH = ee[nh];
    }

    const double thresh = 1e-10; // occupancy threshold

    // compute the Fermi energy by interval bisection
    while(1) {
        mu = 0.5*(EL+EH);

        double bm = exp(bb * mu);
        nn = FermiCount(ff,M, bm);
        //nn = FermiCount(ee,M, bb, mu);

        if (nn > double(N)) EH = mu;
        if (nn < double(N)) EL = mu;

        if ( fabs(nn - double(N)) < thresh ) break;
    }

    // fill occupancies, avoiding over/underflows
    /*
    for (int j=0; j<M; ++j) {
        if (ee[j] < mu)
            ff[j] = 1. / (1. + exp(bb * (ee[j]-mu) ) );
        else
            ff[j] = exp(bb * (mu-ee[j]) ) / (1. + exp(bb * (mu-ee[j]) ) );
    }
    */
    {
        double bm = exp(bb * mu);
        FermiSmear(ff,M, bm);
    }

    return true;
}

double Entropy (const double * ff, int M) {

    double ss=0;

    for (int j=0; j<M; ++j) {
        if (ff[j]>0)
            ss += ff[j] * log(ff[j]);
    }

    return -ss;
}

void CalcDentropy (const tensor2 & D, const tensor2 & S, const tensor2 & L) {

    int NAO = L.n;
    int NMO = L.m;

    tensor2 DD(NMO,NMO);
    {
        //tensor2 SDS(NAO,NAO);
        //tensor2 DS(NAO,NAO);
        //tensor2 DL(NAO,NMO);

        //proyecta el Hamiltoniano en la base ortonormal
        //Messenger(100) << "Projecting hamiltonian onto orthonormal basis";
        Messenger.Push("Projecting pseudodensity onto orthonormal basis"); {
            AO2basis (DD, D, L, S);

            //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, NAO, NAO, NAO, 1.0, D.c2, NAO,  S.c2, NAO, 0.0, DS.c2, NAO);
            //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, NAO, NAO, NAO, 1.0, S.c2, NAO, DS.c2, NAO, 0.0, SDS.c2, NAO);
            // D * L
            //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, NAO, NMO, NAO, 1.0, SDS.c2, NAO,  L.c2, NMO, 0.0, DL.c2, NMO);
            // Lt * D
            //cblas_dgemm(CblasRowMajor, CblasTrans,   CblasNoTrans, NMO, NMO, NAO, 1.0, L.c2, NMO, DL.c2, NMO, 0.0, DD.c2, NMO);
        } Messenger.Pop();

    }

    double dd[NMO];

    //diagonaliza el nuevo Hamiltoniano
    Messenger.Push("Diagonalizing pseudodensity"); {
        DD*=-1;
        DiagonalizeV(DD, dd);
        for (int j=0; j<NMO; ++j) dd[j]*=-1;
    } Messenger.Pop();

    {
          Messenger(0) <<  "occupation : " << endl;

          Messenger.precision(8);

          for (int j = 0; j < NMO; ++j) {
             if ((j)%10 ==0) Messenger(0) << j << "   ";
             Messenger(0) << dd[j] << "  ";
             if ((j+1)%10 ==0) Messenger(0) << endl;
          }
          Messenger(0) << endl;

          Messenger.precision(16);
    }



    double ss = Entropy(dd,NMO);

    //Messenger << "Thermodynamic beta :     " << bb << "     ";
    //Messenger << "Fermi energy       :     " << mu << "     ";
    Messenger << "Entropy            :     " << ss << endl;
}




void CalcHfermi(tensor2 & D, double * En, const tensor2 & H, const tensor2 & L, int n, double bb) {

    int NAO = L.n;
    int NMO = L.m;

    tensor2 HH(NMO,NMO);
    tensor2 LH(NMO,NAO);

    Messenger.Push("Projecting hamiltonian onto orthonormal basis"); {
        TensorProductNTN (LH, L, H); //Lt * H
        TensorProduct    (HH, LH, L); //H * L
    } Messenger.Pop();


    Messenger.Push("Diagonalizing hamiltonian"); {
        DiagonalizeV(HH, En);
    } Messenger.Pop();

    Messenger << "Extreme eigenvalues:     " << En[0] << " " << En[NMO-1] << endl;
    Messenger << "HOMO - LUMO eigenvalues: " << En[n-1] << " " << En[n] << endl;



    double ff[NMO]; // occupancies
    double mu;      // fermi energy

    bool err = FermiSmearing (ff, mu, En, n, NMO, bb);
    if (!err) Messenger << "Fermi energy       :     " << mu << endl;
    else      Messenger << "Error computing Fermi level" << endl;


    Messenger.Push("Recomputing coefficients"); {
        TensorProductNNT (LH, HH, L); // transpose of old method
    } Messenger.Pop();

    {
          // suppresses output unless priority is 100 or so
          Messenger(0) <<  "orbital energies" << endl;

          Messenger.precision(8);

          for (int j = 0; j < NMO; ++j) {
             if ((j)%10 ==0) Messenger(0) << j << "   ";
             Messenger(0) << En[j] << "  ";
             if ((j+1)%10 ==0) Messenger(0) << endl;
          }
          Messenger(0) << endl;

          Messenger.precision(16);
    }

    //calcula la matriz de densidad
    Messenger.Push("Assembling density matrix"); {
        int o=0;
        tensor2 V(n-o, NAO, LH[o]);
        TensorProductNTN (D, V, V);
    } Messenger.Pop();
}
