#ifndef __ERIgeom__
#define __ERIgeom__

#include "defs.hpp"

// skip the class definition if included from a CUDA source
// aparently NVCC can't handle SSE intrinsics well

#ifndef __CUDACC__

#include "low/cache64.hpp"
#include "low/cache32.hpp"

struct ERIgeometries64 {
    cacheline64 ABz;
    cacheline64 CDy;
    cacheline64 CDz;
    cacheline64 ACx;
    cacheline64 ACy;
    cacheline64 ACz;
} __attribute__((aligned(CACHE_LINE_SIZE)));

struct ERIgeometries32 {
    cacheline32 ABz;
    cacheline32 CDy;
    cacheline32 CDz;
    cacheline32 ACx;
    cacheline32 ACy;
    cacheline32 ACz;
} __attribute__((aligned(CACHE_LINE_SIZE)));

#else
struct ERIgeometries64;
struct ERIgeometries32;
#endif


#include "low/cacheN.hpp"
template<int N> struct ERIgeometriesN {
    cachelineN<N> ABz;
    cachelineN<N> CDy;
    cachelineN<N> CDz;
    cachelineN<N> ACx;
    cachelineN<N> ACy;
    cachelineN<N> ACz;
} __attribute__((aligned(256)));

#endif
