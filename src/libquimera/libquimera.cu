
/*

    Just the CUDA-dependent methods of LibQuimera

*/

#include "libquimera.hpp"
using namespace LibQuimera;

#include "2eints/quimera.hpp"

//GPU routines
void Qalgorithm::K4     (double * d_geom,  double * d_F0, double * d_ab, double * d_cd, const ERITile<DOUBLES_PER_BLOCK> * ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, uint32_t Ntiles, cudaStream_t * stream) const {
    pQalgorithm->K4     (d_geom,  d_F0, d_ab, d_cd, ET, ABp, CDp, uv_m_st, Ntiles, stream);
}

void Qalgorithm::MIRROR (double * d_kern,  double * d_geom,  double * d_ERI, uint32_t Ntiles, int J4, cudaStream_t * stream) const {
    pQalgorithm->MIRROR (d_kern,  d_geom,  d_ERI, Ntiles, J4, stream);
}

