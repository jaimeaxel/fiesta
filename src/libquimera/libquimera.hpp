/*
    QUIMERA library;
    Query-based Unified Interface for Managing Electron-Repulsion Algorithms

    Much like the mythological animal and like the actual biological chimeras, the QUIMERA module
    is composed by potentially very different implementations of ERI algorithms in a single functional
    entity.

    The QUIMERA module is query-based. That is, once initialized, the function 'SelectAlgorithm'
    returns a pointer to an object which implements both the K4() and MIRROR() functions, to generate
    ERI kernels and transform them into batches, respectively.

    The module grants an interface to a working algorithm, but its implementation is hidden from the
    user. The object returned by 'SelectAlgorithm' can work by interpreting ERI Intermediate Code, by
    providing a pointer to a hard-coded static routine, or to a dynamically linked routine, depending
    on whether efficient hard-coded libraries are present.

    In a future it might also encapsulate OTF-generated specialized code and code executed in MIC
    coprocessors or CUDA-able cards. This, however, should be transparent to the user.


    2013.01.20, Jaime Axel Rosal Sandberg
    * initial version, product of major refactoring of earlier code
*/


#ifndef __LIB_QUIMERA__
#define __LIB_QUIMERA__

#include "defs.hpp"

#include <cstdlib>
#include <string>
#include "low/plotters.hpp"


class p_ERIbuffer;
class p_Qalgorithm;
class p_Quimera;

class cacheline;
class cacheline64;
class cacheline32;
template <int N> class cachelineN;

class ShellPairPrototype;
class ERIgeometries64;
class ERIgeometries32;
template <int N> class ERIgeometriesN;

#ifndef __CUDACC__
class cudaStream_t;
#endif


namespace LibQuimera {

// may cause problems with Mac compilers
// __attribute__ ((visibility ("default"))) {

    //ERI computational quantum(s)
    struct ERITile64 {
        uint32_t ap12[DOUBLES_PER_CACHE_LINE];  // 32 bytes
        uint32_t ap34[DOUBLES_PER_CACHE_LINE];  // 32 bytes

        const double * wSP12;  // 8 bytes
        const double * wSP34;  // 8 bytes

        // these are the offsets within the atom pair block for Sparse tensors;
        // they are the same  for all integrals involving the same elemental GTO quadruplet,
        // but storing them makes it possible to decouple the OC and DL parts of the computation
        // later on, so that all transformations/contraction involving same angular momenta can be
        // processed in a batch; also, the structure is already >64 bytes, so it doesnt harm performance
        uint32_t offAB;
        uint32_t offCD;
        uint32_t offAC;
        uint32_t offAD;

        uint32_t offBC;
        uint32_t offBD;

        //uint32_t offBA; // this and offAB
        //uint32_t offDC; // this and offCD
        uint16_t ebA;   // store the element + basis ID according to the new ntensors method
        uint16_t ebB;
        uint16_t ebC;
        uint16_t ebD;

        uint16_t nKab;  //number of gaussian products to be evaluated -1
        uint16_t nKcd;

        uint16_t nK2ab; // total number of gaussian products in the elemental shellpair
        uint16_t nK2cd;

        uint8_t used;      //number of used elements (0-DOUBLES_PER_CACHE_LINE)
        uint8_t distance;  //distance block

        uint8_t lowgeom;   // for avoiding 3-center ABAD-type integrals when using CDR

        //whether or not it contains valid information at all
        uint8_t inuse;

        //bit fields
        uint8_t useJ;
        uint8_t useX;

        uint8_t align[2];

        //uint8_t align[34]; // maybe i'll find something more useful later
    } __attribute__((aligned(128)));

    struct ERITile32 {
        uint32_t ap12[FLOATS_PER_CACHE_LINE];  // 64 bytes
        uint32_t ap34[FLOATS_PER_CACHE_LINE];  // 64 bytes

        const double * wSP12;  // 8 bytes
        const double * wSP34;  // 8 bytes

        // these are the offsets within the atom pair block for Sparse tensors;
        // they are the same  for all integrals involving the same elemental GTO quadruplet,
        // but storing them makes it possible to decouple the OC and DL parts of the computation
        // later on, so that all transformations/contraction involving same angular momenta can be
        // processed in a batch; also, the structure is already >64 bytes, so it doesnt harm performance
        uint32_t offAB;
        uint32_t offCD;
        uint32_t offAC;
        uint32_t offAD;

        uint32_t offBC;
        uint32_t offBD;

        uint32_t offBA; // this and offAB
        uint32_t offDC; // this and offCD

        uint16_t nKab;  //number of gaussian products to be evaluated -1
        uint16_t nKcd;

        uint16_t nK2ab; // total number of gaussian products in the elemental shellpair
        uint16_t nK2cd;

        uint8_t used;      //number of used elements (0-FLOATS_PER_CACHE_LINE)
        uint8_t distance;  //distance block

        uint8_t use[FLOATS_PER_CACHE_LINE];

        uint8_t align2[54];

        //uint8_t align[34]; // maybe i'll find something more useful later
    } __attribute__((aligned(256)));

    template <int N> struct ERITile {
        uint32_t ap12[N];
        uint32_t ap34[N];

        const double * wSP12;
        const double * wSP34;

        //bit fields
        uint64_t useJ;
        uint64_t useX;

        //whether or not it contains valid information at all
        uint64_t inuse;

        //more stuff
        uint32_t offAB;
        uint32_t offCD;
        uint32_t offAC;
        uint32_t offAD;

        uint32_t offBC;
        uint32_t offBD;

        uint32_t offBA; // this and offAB
        uint32_t offDC; // this and offCD

        uint16_t nKab;  //number of gaussian products to be evaluated -1
        uint16_t nKcd;

        uint16_t nK2ab; // total number of gaussian products in the elemental shellpair
        uint16_t nK2cd;

        uint8_t used;      //number of used elements (0-DOUBLES_PER_CACHE_LINE)
        uint8_t distance;  //distance block

        uint8_t lowgeom;   // for avoiding 3-center ABAD-type integrals when using CDR

        uint8_t align[173]; // for 32
        //uint8_t align[117]; // for 8

    } //__attribute__((aligned(256)));
    __attribute__((aligned(512)));


    class Qalgorithm;

    class ERIbuffer {
        friend class Qalgorithm;

      private:
        p_ERIbuffer * pERIbuffer; //private implementation

      public:
        ERIbuffer();
       ~ERIbuffer();
        size_t SetBuffers (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const Qalgorithm & routine);
    };

    class Qalgorithm {
        friend class ::p_Quimera;
        friend class ERIbuffer;

      private:
        p_Qalgorithm * pQalgorithm; //private implementation

      public:
        Qalgorithm();
       ~Qalgorithm();

        uint32_t GetNKernels() const;

        size_t MemSize (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp) const;

        //1 center routines
        void K4     (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer) const;
        void K4SR   (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer, double w2sr) const;
        void K4LR   (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer, double w2lr) const;
        void K4IR   (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer, double w2sr, double w2lr) const;
        void MIRROR (const double *  uv_m_st, double * W, ERIbuffer & buffer) const;

        //packed double routines
        void K4     (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer) const;
        void K4SR   (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer, double w2sr) const;
        void K4LR   (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer, double w2lr) const;
        void K4IR   (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer, double w2sr, double w2lr) const;
        void MIRROR (const cacheline64 *   mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, ERIbuffer & buffer) const;

        //packed single routines
        void K4     (const ERIgeometries32 & vars16, const ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline32 * uv_m_st16, ERIbuffer & buffer, bool OnlyJ = false) const;
        void MIRROR (const cacheline32 *   mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, ERIbuffer & buffer) const;

        //free vector routines
        void K4     (const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, const ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cachelineN<DOUBLES_PER_BLOCK> * uv_m_st, ERIbuffer & buffer, bool OnlyJ = false) const;
        void MIRROR (const cachelineN<DOUBLES_PER_BLOCK> * mem,  const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, cachelineN<DOUBLES_PER_BLOCK> * ERI, ERIbuffer & buffer) const;

        //GPU routines
        void K4     (double * d_geom,  double * d_F0, double * d_ab, double * d_cd, const ERITile<DOUBLES_PER_BLOCK> * ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, uint32_t Ntiles, cudaStream_t * stream) const;
        void MIRROR (double * d_kern,  double * d_geom,  double * d_ERI, uint32_t Ntiles, int J4, cudaStream_t * stream) const;
    };


    // There should be 2 classes of Qalgorithms: K4 and MIRROR ones; in case their inner implementation
    // is through ERIroutine, there is two approaches:
    // *) ERIroutine can be also split
    // *) The K4 and MIRROR algorithms point INTERNALLY to the same ERIroutine
    // The Qalgorithm class should have only a () operator which acts as a functor
    // Each Qalgorithm-derived class should override the base () operator with an argument list
    // depending on the hardware and precision required

    class Quimera {
        friend class Qalgorithm;

        // All this data and routines should be non-static for each QUIMERA class;
      private:

        static p_Quimera * pQuimera; //private implementation

        static bool    Initialized;

        static std::string IC_DIR;

        //not used in DALTON
        static double  case_w2;
        static bool    UseCASE;
        static bool    UseCDR;
        static bool    UseGC;

        static bool    BenchmarkAlgorithms;

      public:
        static MessagePlotter QMessenger;
        static MessagePlotter QBenchmarker;

        // class setters, to be used before initialization
        static int  SetW2        (double w2);
        static int  SetCASE      (bool CASE);
        static int  SetCDR       (bool CDR);
        static int  SetGC        (bool GC);
        static int  SetBenchmark (bool BA);
        static int  SetQICdir    (const std::string & dir);

        //initialization
        static int InitLibQuimera();

        // class constant getters; trying ro modify these AFTER initialization makes no sense
        static double      GetW2        () {return case_w2;            }
        static bool        GetCASE      () {return UseCASE;            }
        static bool        GetCDR       () {return UseCDR;             }
        static bool        GetGC        () {return UseGC;              }
        static bool        GetBenchmark () {return BenchmarkAlgorithms;}
        static std::string GetQICdir    () {return IC_DIR;             }

      public:
        Quimera();
       ~Quimera();

        const Qalgorithm * SelectAlgorithm (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld) const;

        void ListNeeded     (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld);
        void GenerateNeeded (bool overwrite);

        void Statistics() const;
    } extern Q;

}



#endif
