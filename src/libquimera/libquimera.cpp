

#include "libquimera.hpp"
#include "2eints/quimera.hpp"
#include "math/angular.hpp"
#include "math/gamma.hpp"


namespace LibQuimera  {

// skip this, since it causes problems in some systems
// __attribute__((visibility ("default"))) {

    //ERIbuffer methods

    ERIbuffer::ERIbuffer() {
        pERIbuffer = new p_ERIbuffer;
    }

    ERIbuffer::~ERIbuffer() {
        delete pERIbuffer;
    }

    size_t ERIbuffer::SetBuffers(void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const Qalgorithm & routine) {
        return pERIbuffer->SetBuffers( mem, ssize, ABp, CDp,  *(routine.pQalgorithm));
    }


    //Qalgorithm methods

    Qalgorithm::Qalgorithm() {
        pQalgorithm = new p_Qalgorithm();
    }

    Qalgorithm::~Qalgorithm() {
        delete pQalgorithm;
    }

    uint32_t Qalgorithm::GetNKernels() const {
        return pQalgorithm->nKernels;
    }

    size_t Qalgorithm::MemSize(const ShellPairPrototype & ABp, const ShellPairPrototype & CDp) const {
        return pQalgorithm->MemSize(ABp, CDp);
    }



    //1 center routines
    void Qalgorithm::K4     (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer) const {
        if (Quimera::GetCASE()) pQalgorithm->K4 (ABp, CDp, uv_m_st, *(buffer.pERIbuffer), 0., 0.5/Quimera::GetW2());
        else                    pQalgorithm->K4 (ABp, CDp, uv_m_st, *(buffer.pERIbuffer),0.,0.);
    }

    void Qalgorithm::K4SR   (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer, double w2sr) const {
        pQalgorithm->K4  (ABp, CDp, uv_m_st, *(buffer.pERIbuffer), 0.5/w2sr,0.);
    }

    void Qalgorithm::K4LR   (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer, double w2lr) const {
        pQalgorithm->K4 (ABp, CDp, uv_m_st, *(buffer.pERIbuffer), 0., 0.5/w2lr);
    }

    void Qalgorithm::K4IR   (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, ERIbuffer & buffer, double w2sr, double w2lr) const {
        pQalgorithm->K4 (ABp, CDp, uv_m_st, *(buffer.pERIbuffer), 0.5/w2sr, 0.5/w2lr);
    }

    void Qalgorithm::MIRROR (const double *  uv_m_st, double * W, ERIbuffer & buffer) const {
        pQalgorithm->MIRROR (uv_m_st, W, *(buffer.pERIbuffer));
    }

    //packed double routines
    void Qalgorithm::K4     (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer) const {
        if (Quimera::GetCASE()) pQalgorithm->K4  (vars8, ET, ABp, CDp, uv_m_st8, *(buffer.pERIbuffer), 0., 0.5/Quimera::GetW2());
        else                    pQalgorithm->K4  (vars8, ET, ABp, CDp, uv_m_st8, *(buffer.pERIbuffer), 0., 0.);
    }

    void Qalgorithm::K4SR   (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer, double w2sr) const {
        pQalgorithm->K4  (vars8, ET, ABp, CDp, uv_m_st8, *(buffer.pERIbuffer), 0.5/w2sr, 0.);
    }

    void Qalgorithm::K4LR   (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer, double w2lr) const {
        pQalgorithm->K4  (vars8, ET, ABp, CDp, uv_m_st8, *(buffer.pERIbuffer), 0., 0.5/w2lr);
    }

    void Qalgorithm::K4IR   (const ERIgeometries64 & vars8, const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, ERIbuffer & buffer, double w2sr, double w2lr) const {
        pQalgorithm->K4  (vars8, ET, ABp, CDp, uv_m_st8, *(buffer.pERIbuffer), 0.5/w2sr, 0.5/w2lr);
    }



    void Qalgorithm::MIRROR (const cacheline64 *   mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, ERIbuffer & buffer) const {
        pQalgorithm->MIRROR (mem, vars8, ERI8, *(buffer.pERIbuffer));
    }

    //packed single routines
    void Qalgorithm::K4     (const ERIgeometries32 & vars16, const ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline32 * uv_m_st16, ERIbuffer & buffer, bool UseCase) const {
        pQalgorithm->K4     (vars16, ET, ABp, CDp, uv_m_st16, *(buffer.pERIbuffer), UseCase);
    }

    void Qalgorithm::MIRROR (const cacheline32 *   mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, ERIbuffer & buffer) const {
        pQalgorithm->MIRROR (mem, vars16, ERI16, *(buffer.pERIbuffer));
    }

    //free vector routines
    void Qalgorithm::K4     (const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, const ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cachelineN<DOUBLES_PER_BLOCK> * uv_m_st, ERIbuffer & buffer, bool UseCase) const {
        pQalgorithm->K4     (vars, ET, ABp, CDp, uv_m_st, *(buffer.pERIbuffer), UseCase);
    }

    void Qalgorithm::MIRROR (const cachelineN<DOUBLES_PER_BLOCK> * mem,  const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, cachelineN<DOUBLES_PER_BLOCK> * ERI, ERIbuffer & buffer) const {
        pQalgorithm->MIRROR (mem, vars, ERI, *(buffer.pERIbuffer));
    }


    Quimera Q; // use a singleton for the moment

    //Quimera methods and static variables
    MessagePlotter Quimera::QMessenger;
    MessagePlotter Quimera::QBenchmarker;

    p_Quimera * Quimera::pQuimera = &p_Q;

    bool        Quimera::Initialized = false;
    std::string Quimera::IC_DIR = "";

    double      Quimera::case_w2 = 1.;
    bool        Quimera::UseCASE = false;
    bool        Quimera::UseCDR  = false;
    bool        Quimera::UseGC   = true;

    bool        Quimera::BenchmarkAlgorithms = false;


    Quimera::Quimera() {
    }

    Quimera::~Quimera() {
    }

    const Qalgorithm * Quimera::SelectAlgorithm (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld) const {
        return pQuimera->SelectAlgorithm(geometry, La, Lb, Lc, Ld);
    }

    void Quimera::ListNeeded     (GEOM geometry, uint8_t La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld) {
        pQuimera->ListNeeded(geometry, La,  Lb, Lc, Ld);
    }

    void Quimera::GenerateNeeded (bool overwrite) {
        pQuimera->GenerateNeeded (overwrite);
    }

    void Quimera::Statistics() const {
        pQuimera->Statistics();
    }


    int  Quimera::SetW2        (double w2) {
//        if (!Initialized) {
            if (w2<=0) return -2; //check for consistency
            case_w2 = w2; // set the stuff
            return 0; // return normal
//        }
//        else
//            return -1; //return error
    }
    int  Quimera::SetCASE      (bool CASE) {
//        if (!Initialized) {
            UseCASE = CASE; // set the stuff
            return 0; // return normal
//        }
//        else
//            return -1; //return error
    }
    int  Quimera::SetCDR       (bool CDR) {
        if (!Initialized) {
            UseCDR = CDR; // set the stuff
            return 0; // return normal
        }
        else
            return -1; //return error
    }
    int  Quimera::SetGC        (bool GC) {
        if (!Initialized) {
            UseGC = GC; // set the stuff
            return 0; // return normal
        }
        else
            return -1; //return error
    }
    int  Quimera::SetBenchmark (bool BA) {
        if (!Initialized) {
            BenchmarkAlgorithms = BA; // set the stuff
            return 0; // return normal
        }
        else
            return -1; //return error
    }

    int  Quimera::SetQICdir    (const std::string & dir) {
        if (!Initialized) {
            IC_DIR = dir; // set the stuff
            return 0; // return normal
        }
        else
            return -1; //return error
    }

    // initializes some computing constants, including the gamma function and spherical harmonics
    int Quimera::InitLibQuimera() {

        if (!Initialized) {
            LibAngular::InitAngular();
            LibIGamma::InitIncompleteGammas(); //this includes constant mem initialization

            QMessenger.SetModule  ("Quimera module");
            QBenchmarker.SetModule ("Quimera module benchmark");

            pQuimera = &p_Q;

            p_Qalgorithm::BenchmarkAlgorithms = BenchmarkAlgorithms;

            QBenchmarker.SetFile("quimera.benchmark.out");
            Initialized = true;
            return 0;
        }
        // already initialized?
        else
            return -1;
    }

}

