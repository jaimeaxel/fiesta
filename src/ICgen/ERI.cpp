/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/




#include <iostream>
#include <set>
using namespace std;

#include "math/angular.hpp"
using namespace LibAngular;
void InitDeviceSphH() {} //dummy function (required in LibAngular initialization)
void InitDeviceSHrot() {}

#include "defs.hpp"
#include "EVS.hpp"
#include "EVS_SKS.hpp"
#include "ERI.hpp"




static const int PF = 8; //prefetch diostabnce


ERIroutine::ERIroutine() {
    NFLOPSK = 0;
    NK4 = 0;

    IsSet = false;
    IsInitialized = false;
    useCDR = false;

    for (int i=0; i<32; ++i) {
        ninstrK4[i] = 0;
        ninstr[i]   = 0;

        nseqK4[i] = NULL;
        nseq[i]   = NULL;
    }

    eseqK4 = NULL;
    eseq   = NULL;
}

ERIroutine::~ERIroutine() {
    delete[] eseqK4;
    delete[] eseq;
}


void ERIroutine::Set(uint8_t La, uint8_t Lb, uint8_t Lc, uint8_t Ld, GEOM geom, bool cdr=false) {
    geometry = geom;
    la = La;
    lb = Lb;
    lc = Lc;
    ld = Ld;
    IsSet = true;
    useCDR = cdr;

    for (int i=0; i<32; ++i) ninstrK4[i] = 0;
    for (int i=0; i<32; ++i) ninstr[i] = 0;
}

void ERIroutine::Generate (bool wQIC, bool wK2C, bool wK4CU, bool wMCU) {

    if (IsInitialized) return;
    IsInitialized = true;

    Lt = la+lb+lc+ld;
    Am = la+lb+lc+ld+1;


    bool ABx, ABy, ABz;
    bool CDx, CDy, CDz;
    bool ACx, ACy, ACz;

    bool ABt, CDt, ACt;

    //initialize

    switch(geometry) {
        case ABCD:
            ABx = ABy = CDx = false;
            ABz = CDy = CDz = true;
            ACx = ACy = ACz = true;
        break;

        case ABAD:
            ABx = ABy = CDx = false;
            ABz = CDy = CDz = true;
            ACx = ACy = ACz = true;
        /*
            ABx = ABy = CDx = false;
            ABz = CDy = CDz = true;
            ACx = ACy = ACz = false;
            */
        break;

        case AACD:
            ABx = ABy = ABz = CDx = CDy = false;
            CDz = true;
            ACx = false;
            ACy = ACz = true;
        break;

        case AACC:
            ABx = ABy = ABz = CDx = CDy = CDz = false;
            ACx = ACy = false;
            ACz = true;
        break;

        case ABAB:
            ABx = ABy = CDx = CDy = false;
            ABz = CDz = true;
            ACx = ACy = false;
            ACz = true;
        break;

        case AAAD:
            ABx = ABy = ABz = CDx = CDy = false;
            CDz = true;
            ACx = ACy = ACz = false;
        break;

        case AAAA:
            ABx = ABy = ABz = CDx = CDy = CDz = false;
            ACx = ACy = ACz = false;
        break;


        default:
            ABx = ABy = ABz = CDx = CDy = CDz = false;
            ACx = ACy = ACz = false;
        break;
    }

    ABt = ABx or ABy or ABz;
    CDt = CDx or CDy or CDz;
    ACt = ACx or ACy or ACz;

    //don't use CDR for one-center integrals (will crash) or for center-degenerate ABCD integrals
    //(produces numerical inaccuracy in some cases)
    useCDR =  false; // (geometry==ABCD); // (geometry==AACD) || (geometry==AACC) || (geometry==AACD) || (geometry==ABCD); //(geometry!=AAAA) && (geometry!=ABAB)  && (geometry!=ABAD) && (geometry!=ABCD);
    useGC = true;


    EvaluationScheme Scheme;

    {
        //populates the list of needed integrals
        {
            set<integralCart> setERI;
            set<ikernel> UVmST;

            for (uint8_t a=0; a<nmS[la]; ++a) {
                for (uint8_t b=0; b<nmS[lb]; ++b) {
                    for (uint8_t c=0; c<nmS[lc]; ++c) {
                        for (uint8_t d=0; d<nmS[ld]; ++d) {
                            integralCart ERI(a,b,c,d);
                            ERI.m = -1; //mark it
                            setERI.insert(ERI);
                        }
                    }
                }
            }

            Scheme.Set(la,lb,lc,ld,geometry,useGC);

            //MIRROR
            {
                set<integralCart> setSSSS;
                set<integralCart> setSSSC;
                set<integralCart> setSSCC;
                set<integralCart> setSCCC;
                set<integralCart> setCCCC;

                set<integralCart> setHRRkx;
                set<integralCart> setHRRbx;
                set<integralCart> setHRRky;
                set<integralCart> setHRRby;
                set<integralCart> setHRRkz;
                set<integralCart> setHRRbz;

                set<integralCart> setCTEkx;
                set<integralCart> setCTEbx;
                set<integralCart> setCTEky;
                set<integralCart> setCTEby;
                set<integralCart> setCTEkz;
                set<integralCart> setCTEbz;

                set<integralCart> setRx;
                set<integralCart> setRy;
                set<integralCart> setRz;


                Scheme.addInitial(setSSSS);

                Scheme.addSphD (setSSSS , setSSSC);
                Scheme.addSphC (setSSSC , setSSCC);

                Scheme.addHRRkx(setSSCC , setHRRkx, CDx);
                Scheme.addHRRky(setHRRkx, setHRRky, CDy);
                Scheme.addHRRkz(setHRRky, setHRRkz, CDz);

                Scheme.addSphB (setHRRkz, setSCCC);
                Scheme.addSphA (setSCCC , setCCCC);

                Scheme.addHRRbx(setCCCC , setHRRbx, ABx);
                Scheme.addHRRby(setHRRbx, setHRRby, ABy);
                Scheme.addHRRbz(setHRRby, setHRRbz, ABz);

                Scheme.addCTEbx(setHRRbz, setCTEbx, ABx);
                Scheme.addCTEby(setCTEbx, setCTEby, ABy);
                Scheme.addCTEkx(setCTEby, setCTEkx, CDx);
                Scheme.addCTEky(setCTEkx, setCTEky, CDy);

                Scheme.addRxcc (setCTEky, setRx   , ABx, CDx, ACx);
                Scheme.addRycc (setRx   , setRy   , ABy, CDy, ACy);
                Scheme.addCTEkz(setRy   , setCTEkz, CDz);
                Scheme.addCTEbz(setCTEkz, setCTEbz, ABz);
                Scheme.addRzcc (setCTEbz, setRz   , ABz,  CDz, ACz);

                Scheme.MakeKernels(setRz, UVmST);
            }


            //generate the set of spherical kernels
            set<ikernel> sphKs;
            TestSKS(sphKs, geometry, la, lb, lc, ld);


            //K4
            if (!useCDR) {
                set<ikernel> UVmT;
                set<ikernel> UVm;
                set<ikernel> Vm;
                set<ikernel> m;
                set<ikernel> e;

                Scheme.addS(sphKs, UVmT);
                Scheme.addT(UVmT, UVm);
                Scheme.addU(UVm, Vm);
                Scheme.addV(Vm, m);

                if (geometry!=AAAA) {
                    Scheme.UseCDR4 (m, e, Lt);
                    Scheme.UseAERR4(e, Lt);
                }
                else
                    Scheme.add00(Lt);
            }
            else {
                set<ikernel> K1fe;
                set<ikernel> K2fe;


                set<ikernel> K0c, K0b, K0f, K0e;
                set<ikernel> K1c, K1b, K1f, K1e;
                set<ikernel> K2c, K2b, K2f, K2e;
                set<ikernel> K3c, K3b, K3f, K3e;
                set<ikernel> K4f, K4e;

                Scheme.UseCDR0  (sphKs, K0e, K0c, ABt, CDt, ACt);
                Scheme.UseAERR0 (K0e, K0b);

                Scheme.addS(K0c,  K1f);
                Scheme.addS(K0b,  K1e);

                Scheme.UseCDR1  (K1f, K1e, K1c, ABt, CDt, ACt);
                Scheme.UseAERR1 (K1e, K1b);

                Scheme.addT(K1c,  K2f);
                Scheme.addT(K1b,  K2e);

                Scheme.UseCDR2  (K2f, K2e, K2c, ABt, CDt, ACt);
                Scheme.UseAERR2 (K2e, K2b);

                Scheme.addU (K2c, K3f);
                Scheme.addU (K2b, K3e);

                Scheme.UseCDR3  (K3f, K3e, K3c, ABt, CDt, ACt);
                Scheme.UseAERR3 (K3e, K3b);

                Scheme.addV (K3c, K4f);
                Scheme.addV (K3b, K4e);

                if (geometry!=AAAA) {
                    Scheme.UseCDR4 (K4f, K4e, Lt);
                    Scheme.UseAERR4(K4e, Lt);
                }
                else
                    Scheme.add00(Lt);
            }


            Scheme.LinkVarsK4(UVmST);
            Scheme.LinkVars(setERI);

            Scheme.LinkBackK4();
            Scheme.LinkBack();

            Scheme.Simplify();
            Scheme.AssignMemory();

            Scheme.GenerateCode();
        }

        //K4
        {
            uint32_t totalK4 = 0;
            for (uint8_t i=BOYS; i<NADA; ++i) ninstrK4[i] = Scheme.ninstrK4[i];
            ninstrK4[NADA] = 0;

            for (uint8_t iblock=BOYS; iblock<NADA; ++iblock) totalK4 += ninstrK4[iblock];

            eseqK4 = new Op_K4[totalK4 + PF]; //add the prefetching space for the last

            int p=0;


            for (uint8_t iblock=BOYS; iblock<=NADA; ++iblock) {
                nseqK4[iblock] = eseqK4 + p;

                for (int i=0; i<ninstrK4[iblock]; ++i) {
                    nseqK4[iblock][i].dest  = Scheme.seqsK4[iblock][i].dest;
                    nseqK4[iblock][i].op1   = Scheme.seqsK4[iblock][i].op1;
                    nseqK4[iblock][i].op2   = Scheme.seqsK4[iblock][i].op2;
                    nseqK4[iblock][i].op3   = Scheme.seqsK4[iblock][i].op3;
                    nseqK4[iblock][i].op4   = Scheme.seqsK4[iblock][i].op4;
                    nseqK4[iblock][i].op5   = Scheme.seqsK4[iblock][i].op5;
                    nseqK4[iblock][i].op6   = Scheme.seqsK4[iblock][i].op6;
                    nseqK4[iblock][i].ope   = Scheme.seqsK4[iblock][i].ope;
                    nseqK4[iblock][i].aux   = Scheme.seqsK4[iblock][i].aux;
                }
                p += ninstrK4[iblock];
            }
        }

        //MIRROR
        {
            for (uint8_t i=KERNELS; i<=REORDER; ++i) ninstr[i] = Scheme.ninstr[i];

            uint32_t total = 0;
            for (uint8_t iblock=KERNELS; iblock<=REORDER; ++iblock) total += ninstr[iblock];

            eseq = new Op_MIRROR[total + PF]; //add the prefetching space for the last

            int p=0;

            for (uint8_t iblock=KERNELS; iblock<=REORDER; ++iblock) {
                for (int i=0; i<ninstr[iblock]; ++i) {
                    eseq[p].dest  = Scheme.seqs[iblock][i].dest;
                    eseq[p].op1   = Scheme.seqs[iblock][i].op1;
                    eseq[p].op2   = Scheme.seqs[iblock][i].op2;
                    eseq[p].op3   = Scheme.seqs[iblock][i].op3;
                    eseq[p].op4   = Scheme.seqs[iblock][i].op4;
                    eseq[p].op5   = Scheme.seqs[iblock][i].op5;
                    eseq[p].op6   = Scheme.seqs[iblock][i].op6;
                    eseq[p].aux   = Scheme.seqs[iblock][i].aux;
                    ++p;
                }
            }

        }

        MaxMem  = Scheme.MaxMem;
        NFLOPS  = Scheme.NFLOPS;
    }

    //COPY EVERYTHING
    //===============

    {
        nKernels =  ninstr[KERNELS];

        // set pointers; observe that K4 and MIRROR behave differently regarding the instruction pointer
        int p=0, q=0;

        for (uint8_t iblock=BOYS; iblock<=NADA; ++iblock) {
            nseqK4[iblock] = eseqK4 + p;
            p += ninstrK4[iblock];
        }



        for (uint8_t iblock=KERNELS; iblock<=REORDER; ++iblock) {
            q += ninstr[iblock];
            nseq[iblock] = eseq + q;
        }


        // find the largest powers of the gaussian exponents needed to compute the kernels

        maxV = 0;
        for (int i=0; i<ninstrK4[K4E]; ++i) maxV = max(maxV, nseqK4[K4E][i].aux);
        for (int i=0; i<ninstrK4[K4F]; ++i) maxV = max(maxV, nseqK4[K4F][i].aux);
        maxU = 0;
        for (int i=0; i<ninstrK4[K3E]; ++i) maxU = max(maxU, nseqK4[K3E][i].aux);
        for (int i=0; i<ninstrK4[K3F]; ++i) maxU = max(maxU, nseqK4[K3F][i].aux);
        maxT = 0;
        for (int i=0; i<ninstrK4[K2E]; ++i) maxT = max(maxT, nseqK4[K2E][i].aux);
        for (int i=0; i<ninstrK4[K2F]; ++i) maxT = max(maxT, nseqK4[K2F][i].aux);
        maxS = 0;
        for (int i=0; i<ninstrK4[K1E]; ++i) maxS = max(maxS, nseqK4[K1E][i].aux);
        for (int i=0; i<ninstrK4[K1F]; ++i) maxS = max(maxS, nseqK4[K1F][i].aux);


        // make a local copy of the Spherical Harmonic
        // coefficients (necessary when for writing MIRROR code)
        if (la>1) {
            for (uint8_t m=0; m<2*la+1; ++m) {
                for (int n=0; n<6; ++n) Ca[m][n] = 0;
                for (int n=0; n<SHList[la][m].nps; ++n) Ca[m][n] = SHList[la][m].T[n].cN;
            }
        }

        if (lb>1) {
            for (uint8_t m=0; m<2*lb+1; ++m) {
                for (uint8_t n=0; n<6; ++n) Cb[m][n] = 0;
                for (uint8_t n=0; n<SHList[lb][m].nps; ++n) Cb[m][n] = SHList[lb][m].T[n].cN;
            }
        }

        if (lc>1) {
            for (uint8_t m=0; m<2*lc+1; ++m) {
                for (uint8_t n=0; n<6; ++n) Cc[m][n] = 0;
                for (uint8_t n=0; n<SHList[lc][m].nps; ++n) Cc[m][n] = SHList[lc][m].T[n].cN;
            }
        }

        if (ld>1) {
            for (uint8_t m=0; m<2*ld+1; ++m) {
                for (uint8_t n=0; n<6; ++n) Cd[m][n] = 0;
                for (uint8_t n=0; n<SHList[ld][m].nps; ++n) Cd[m][n] = SHList[ld][m].T[n].cN;
            }
        }


        // pad the IC array so that the last data prefecthes won't attempt to load garbage into the cache

        K4Mem   = 0;
        for (uint8_t i=BOYS; i<NADA; ++i) K4Mem += ninstrK4[i];

        for (int i=0; i<PF; ++ i) {
            uint32_t p = K4Mem + i;
            eseqK4[p].dest = 0;
            eseqK4[p].op1  = 0;
            eseqK4[p].op2  = 0;
            eseqK4[p].op3  = 0;
            eseqK4[p].op4  = 0;
            eseqK4[p].op5  = 0;
            eseqK4[p].op6  = 0;
            eseqK4[p].ope  = 0;
            eseqK4[p].aux  = 0;
        }


        uint32_t total = 0;
        for (uint8_t iblock=KERNELS; iblock<=REORDER; ++iblock)
            total += ninstr[iblock];

        for (int i=0; i<PF; ++ i) {
            uint32_t p = total + i;
            eseq[p].dest = 0;
            eseq[p].op1  = 0;
            eseq[p].op2  = 0;
            eseq[p].op3  = 0;
            eseq[p].op4  = 0;
            eseq[p].op5  = 0;
            eseq[p].op6  = 0;
            eseq[p].aux  = 0;
        }


        // number of FLOPs for each block

        nK4J0 =   ninstrK4[AERR4] +  3*ninstrK4[CDR4];
        nK4J1 = 2*ninstrK4[K4E]   +  2*ninstrK4[K4F];
        nK3J1 = 2*ninstrK4[AERR3] +  7*ninstrK4[CDR3] - ninstrK4[K4E] - ninstrK4[K4F];
        nK3J2 = 2*ninstrK4[K3E]   +  2*ninstrK4[K3F];
        nK2J2 = 2*ninstrK4[AERR2] +  7*ninstrK4[CDR2] - ninstrK4[K3E] - ninstrK4[K3F];
        nK2J3 = 2*ninstrK4[K2E]   +  2*ninstrK4[K2F];
        nK1J3 =   ninstrK4[AERR1] + 13*ninstrK4[CDR1] - ninstrK4[K2E] - ninstrK4[K2F];
        nK1J4 = 2*ninstrK4[K1E]   +  2*ninstrK4[K1F];
        nK0J4 =   ninstrK4[AERR0] + 13*ninstrK4[CDR0] - ninstrK4[K1E] - ninstrK4[K1F];


        nK4J0e =   ninstrK4[AERR4]-1 + ninstrK4[CDR4]-1;
        nK4J1e = 2*ninstrK4[K4E];
        nK3J1e = 2*ninstrK4[AERR3] + ninstrK4[CDR3] - ninstrK4[K4E];
        nK3J2e = 2*ninstrK4[K3E];
        nK2J2e = 2*ninstrK4[AERR2] + ninstrK4[CDR2] - ninstrK4[K3E];
        nK2J3e = 2*ninstrK4[K2E];
        nK1J3e =   ninstrK4[AERR1] + ninstrK4[CDR1] - ninstrK4[K2E];
        nK1J4e = 2*ninstrK4[K1E];
        nK0J4e =   ninstrK4[AERR0] + ninstrK4[CDR0] - ninstrK4[K1E];

        nK4J0f =  2*ninstrK4[CDR4]-2;
        nK4J1f =  2*ninstrK4[K4F];
        nK3J1f =  6*ninstrK4[CDR3] - ninstrK4[K4F];
        nK3J2f =  2*ninstrK4[K3F];
        nK2J2f =  6*ninstrK4[CDR2] - ninstrK4[K3F];
        nK2J3f =  2*ninstrK4[K2F];
        nK1J3f = 12*ninstrK4[CDR1] - ninstrK4[K2F];
        nK1J4f =  2*ninstrK4[K1F];
        nK0J4f = 12*ninstrK4[CDR0] - ninstrK4[K1F];


        // memory required for the K4 contraction
        memF0  =     2; //ninstrK4[BOYS];
        memF0e =  Lt+1; //ninstrK4[AERR4];
        memF0f =  Lt+1; //ninstrK4[CDR4];

        memK4J1e =  ninstrK4[K4E]  + ninstrK4[AERR3];
        memK4J1f =  ninstrK4[K4F]  + ninstrK4[CDR3];
        memK3J2e =  ninstrK4[K3E]  + ninstrK4[AERR2];
        memK3J2f =  ninstrK4[K3F]  + ninstrK4[CDR2];
        memK2J3e =  ninstrK4[K2E]  + ninstrK4[AERR1];
        memK2J3f =  ninstrK4[K2F]  + ninstrK4[CDR1];
        memK1J4e =  ninstrK4[K1E]  + ninstrK4[AERR0];
        memK1J4f =  ninstrK4[K1F]  + ninstrK4[CDR0];
    }



    //GENERATE ALL REQUIRED METAPROGRAMMED FILES
    //******************************************

    if (wQIC)
        WriteQIC();

    if (wK2C)
        if (geometry==ABCD) // do this only for 4-center integrals
            WriteK2C();

    if (wK4CU)
        if (geometry==ABCD || geometry==AACD || geometry==AACC) // generate GPU code cor common 4,3 and 2-center integrals
            WriteGK4CUDA();

    if (wMCU)
        if ((geometry==ABCD) || geometry==AACD || geometry==AACC) // generate GPU code cor common 4,3 and 2-center integrals
            WriteMCUDA();
}

string ERIroutine::IdString(bool fuse) const {

    string fname;

    string geo;

    switch(geometry) {
        case ABCD:
            geo = "ABCD";
        break;

        case ABAD:
            geo = "ABAD";
        break;

        case AACD:
            geo = "AACD";
        break;

        case AACC:
            geo = "AACC";
        break;

        case ABAB:
            geo = "ABAB";
        break;

        case AAAA:
            geo = "AAAA";
        break;

        case NONE:
            geo = "NONE";
        break;

        default:
        break;
    }

    fname = geo + "_";

    fname += L2S(la);
    fname += L2S(lb);

    if (fuse)
        fname += L2S(lc+ld);
    else {
        fname += L2S(lc);
        fname += L2S(ld);
    }

    return fname;
}

