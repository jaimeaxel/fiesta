/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __EVS_VARS__
#define __EVS_VARS__

#include "defs.hpp"

typedef uint32_t IDINT;
typedef uint8_t  AM;

class integralCart;

class ikernel {
  public:
    uint8_t u;
    uint8_t v;
    uint8_t s;
    uint8_t t;
    uint8_t m;

    uint8_t K4L; // loop
    uint8_t FE;  // type of kernel (0: regular  1: auxiliary  2: second auxiliary  -1: don't compute)
    uint8_t align[1];

    ikernel();
    ikernel(int ru, int rv, int rs, int rt, int rm);
    ikernel & operator=(const integralCart & rhs);
    ikernel & operator+=(const ikernel & rhs);
    ikernel & operator-=(const ikernel & rhs);
    ikernel & operator|=(const ikernel & rhs);
    ikernel operator|(const ikernel & rhs) const;
    ikernel operator+(const ikernel & rhs) const;
    ikernel operator-(const ikernel & rhs) const;

    bool operator<(const ikernel & rhs) const;
};

class integralCart {
    public:
    uint8_t ex; uint8_t ey; uint8_t ez;
    uint8_t bx; uint8_t by; uint8_t bz;
    uint8_t fx; uint8_t fy; uint8_t fz;
    uint8_t dx; uint8_t dy; uint8_t dz;
    uint8_t px; uint8_t py; uint8_t pz;
    uint8_t qx; uint8_t qy; uint8_t qz;
    uint8_t rx; uint8_t ry; uint8_t rz;
    uint8_t a; uint8_t b; uint8_t p; uint8_t c; uint8_t d; uint8_t q; uint8_t m;
    uint8_t la; uint8_t lb; uint8_t lc; uint8_t ld;

    integralCart();
    integralCart(int nn);
    integralCart(int rla, int rlb, int rlc, int rld);    //(4)
    integralCart(int rax, int ray, int raz, int rbx, int rby, int rbz, int rcx, int rcy, int rcz, int rdx, int rdy, int rdz);    //(12)
    integralCart(int rex, int rey, int rez, int rfx, int rfy, int rfz, int rrx, int rry, int rrz, int ra, int rb, int rp, int rc, int rd, int rq, int rm); //(16)
    integralCart(int rex, int rey, int rez, int rfx, int rfy, int rfz, int rpx, int rpy, int rpz, int rqx, int rqy, int rqz, int rrx, int rry, int rrz,
                 int ra, int rb, int rp, int rc, int rd, int rq, int rm); //(22)
    integralCart(int rex, int rey, int rez, int rbx, int rby, int rbz, int rfx, int rfy, int rfz, int rdx, int rdy, int rdz, int rpx, int rpy, int rpz,
                 int rqx, int rqy, int rqz, int rrx, int rry, int rrz, int ra, int rb, int rp, int rc, int rd, int rq, int rm, int rla, int rlb, int rlc, int rld); //(32)
    integralCart & operator=(const ikernel & rhs);
    integralCart & operator|=(const integralCart & rhs);
    integralCart operator|(const integralCart & rhs) const;
    integralCart & operator+=(const integralCart & rhs);
    integralCart operator+(const integralCart & rhs) const;
    virtual bool operator<(const integralCart & rhs) const;
    bool operator==(const integralCart & rhs) const;
    bool operator!=(const integralCart & rhs) const;
} __attribute__ ((aligned(32)));

class integralSph {
  public:
    int8_t la;
    int8_t ma;
    int8_t lb;
    int8_t mb;
    int8_t lc;
    int8_t mc;
    int8_t ld;
    int8_t md;

    int8_t b;
    int8_t p;
    int8_t d;
    int8_t q;
    int8_t m;

    integralSph();
    integralSph(int rla, int rma, int rlb, int rmb, int rlc, int rmc, int rld, int rmd);
    integralSph(int rla, int rma, int rlb, int rmb, int rlc, int rmc, int rld, int rmd,
                int rb, int rp, int rd, int rq, int rm);
    integralSph(int rla, int rma, int rlb, int rmb, int rlc, int rmc, int rld, int rmd,
                int rb, int rp, int rd, int rq, int rm, bool rab);
    integralSph(int rla, int rma, int rlb, int rmb, int rlc, int rmc, int rld, int rmd,
                int rb, int rp, int rd, int rq, int rm, bool rab, bool rcdab);

    integralSph operator|(const integralSph & rhs) const;
    integralSph & operator+=(const integralSph & rhs);
    integralSph operator+(const integralSph & rhs) const;
    integralSph & operator-=(const integralSph & rhs);
    integralSph operator-(const integralSph & rhs) const;
    bool operator<(const integralSph & rhs) const;
};

#include <iostream>

std::ostream & operator<< (std::ostream & os, const ikernel & rhs);
std::ostream & operator<< (std::ostream & os, const integralCart & rhs);
std::ostream & operator<< (std::ostream & os, const integralSph & rhs);

#endif
