/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <set>
using namespace std;

#include "defs.hpp"
#include "ICgen.hpp"
#include "ERI.hpp"


void ERIroutine::WriteMCUDA() const {

    string CURname = "Mgpu_" + IdString(false);
    string KNLname = CURname; //"Kernel_MIRROR_" + IdString(false);

    string EFSdir = MCU_DIR;
    if (EFSdir=="") return; //skip

    string Mfile = EFSdir + "/" + CURname + ".cu";

    ofstream file;
    file.open(Mfile.c_str());
    file.precision(16);

    file << endl;

    file << "__global__ void "<<KNLname<<" (const double  * mem, const double * vars, double * ERIN, int maxTiles) {" << endl;

    file << "    int i = threadIdx.x;" << endl;
    file << "    int j = blockIdx.x;" << endl;
    file << "    int k = blockIdx.y * blockDim.y + threadIdx.y;" << endl;

    file << "    if (k>=maxTiles) return;" << endl;

    file << "    const int NJ4 = gridDim.x;" << endl;
    file << "    const int NTX = blockDim.x;" << endl;
    file << "    const int NTY = blockDim.y;" << endl;

    bool ABz, CDy, CDz, ACx, ACy, ACz;
    if (geometry==AACD) {
        file << "    const double ABz = 0;" << endl;
        file << "    const double CDy = 0;" << endl;
        file << "    const double CDz = vars[k*6*NTX + 2*NTX+i];" << endl;
        file << "    const double ACx = 0;" << endl;
        file << "    const double ACy = vars[k*6*NTX + 4*NTX+i];" << endl;
        file << "    const double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;
        ABz = CDy = ACx = false;
        CDz = ACy = ACz = true;
    } else if (geometry==AACC) {
        file << "    const double ABz = 0;" << endl;
        file << "    const double CDy = 0;" << endl;
        file << "    const double CDz = 0;" << endl;
        file << "    const double ACx = 0;" << endl;
        file << "    const double ACy = 0;" << endl;
        file << "    const double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;
        ABz = CDy = ACx = false;
        CDz = ACy = false;
        ACz = true;
    } else { //this includes ABCD
        file << "    const double ABz = vars[k*6*NTX +     i];" << endl;
        file << "    const double CDy = vars[k*6*NTX +   NTX+i];" << endl;
        file << "    const double CDz = vars[k*6*NTX + 2*NTX+i];" << endl;
        file << "    const double ACx = vars[k*6*NTX + 3*NTX+i];" << endl;
        file << "    const double ACy = vars[k*6*NTX + 4*NTX+i];" << endl;
        file << "    const double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;
        ABz = CDy = ACx = true;
        CDz = ACy = ACz = true;
    }



    file << "    double buffer["<<MaxMem<<"];" << endl;

    file << "    memset(buffer, 0, "<<sizeof(double)*MaxMem<<");" << endl;

    Op_MIRROR * s2 = eseq;

    //copy kernels to new memory
    for (int p=0; s2<nseq[KERNELS]; ++s2,++p) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        file << "    buffer["<<p<<"] = mem[(k*NJ4+j)*"<<nKernels<<"*NTX  + "<<p<<"*NTX + i];" << endl;
    }

    //dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];
    for (; s2<nseq[MMDZ]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = ACz*buffer["<<s2->op3<<"]";
        if (ABz) file << " - ABz*buffer["<<s2->op1<<"]";
        if (CDz) file << " + CDz*buffer["<<s2->op2<<"]";
        if      (s2->aux >1) file << " - "<<s2->aux<<"*buffer["<<s2->op4<<"]";
        else if (s2->aux==1) file << " - buffer["<<s2->op4<<"]";
        file << ";" << endl;
    }

    //dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
    for (; s2<nseq[CTEBZ]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op3<<"]";
        if (ABz) file << " + ABz*buffer["<<s2->op2<<"]";
        if      (s2->aux >1) file << " + "<<s2->aux<<"*buffer["<<s2->op1<<"]";
        else if (s2->aux==1) file << " + buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
    for (; s2<nseq[CTEKZ]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = -buffer["<<s2->op3<<"]";
        if (CDz) file << " + CDz*buffer["<<s2->op2<<"]";
        if      (s2->aux >1) file << " - "<<s2->aux<<"*buffer["<<s2->op1<<"]";
        else if (s2->aux==1) file << " - buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
    for (; s2<nseq[MMDY]; ++s2) {
        file << "    buffer["<<s2->dest<<"] =";
        if (ACy) file << " ACy*buffer["<<s2->op3<<"]";
        if (CDy) file << " + CDy*buffer["<<s2->op2<<"]";
        if      (s2->aux >1) file << " - "<<s2->aux<<"*buffer["<<s2->op4<<"]";
        else if (s2->aux==1) file << " - buffer["<<s2->op4<<"]";
        file << " + 0;" << endl;
    }

    //dest[i] = ACx[i] * op3[i] - x * op4[i];
    for (; s2<nseq[MMDX]; ++s2) {
        file << "    buffer["<<s2->dest<<"] =";
        if (ACx) file << " ACx*buffer["<<s2->op3<<"]";
        if      (s2->aux >1) file << " - "<<s2->aux<<"*buffer["<<s2->op4<<"]";
        else if (s2->aux==1) file << " - buffer["<<s2->op4<<"]";
        file << " + 0;" << endl;
    }

    //dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
    for (; s2<nseq[CTEKY]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = -buffer["<<s2->op3<<"]";
        if (CDy) file << " + CDy*buffer["<<s2->op2<<"]";
        if      (s2->aux >1) file << " - "<<s2->aux<<"*buffer["<<s2->op1<<"]";
        else if (s2->aux==1) file << " - buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] = -x * op1[i] - op3[i];
    for (; s2<nseq[CTEKX]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = -buffer["<<s2->op3<<"]";
        if      (s2->aux >1) file << " - "<<s2->aux<<"*buffer["<<s2->op1<<"]";
        else if (s2->aux==1) file << " - buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] =  y * op1[i] + op3[i];
    for (; s2<nseq[CTEBY]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op3<<"]";
        if      (s2->aux >1) file << " + "<<s2->aux<<"*buffer["<<s2->op1<<"]";
        else if (s2->aux==1) file << " + buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] = x * op1[i] + op3[i];
    for (; s2<nseq[CTEBX]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op3<<"]";
        if      (s2->aux >1) file << " + "<<s2->aux<<"*buffer["<<s2->op1<<"]";
        else if (s2->aux==1) file << " + buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] = op1[i] - ABz[i] * op2[i];
    for (; s2<nseq[HRRBZ]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"]";
        if (ABz) file << " - ABz*buffer["<<s2->op2<<"]";
        file << ";" << endl;
    }

    //dest[i] = op1[i];
    for (; s2<nseq[HRRBY]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //dest[i] = op1[i];
    for (; s2<nseq[HRRBX]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //spherical A
    for (; s2<nseq[SPHA]; ++s2) {
        uint16_t m    = s2->aux;

        if (la>1) {
            file << "    buffer["<<s2->dest<<"] = ";
            if (Ca[m][0]!=0) file << " + ("<<Ca[m][0]<<") * buffer["<<s2->op1<<"]";
            if (Ca[m][1]!=0) file << " + ("<<Ca[m][1]<<") * buffer["<<s2->op2<<"]";
            if (Ca[m][2]!=0) file << " + ("<<Ca[m][2]<<") * buffer["<<s2->op3<<"]";
            if (Ca[m][3]!=0) file << " + ("<<Ca[m][3]<<") * buffer["<<s2->op4<<"]";
            if (Ca[m][4]!=0) file << " + ("<<Ca[m][4]<<") * buffer["<<s2->op5<<"]";
            if (Ca[m][5]!=0) file << " + ("<<Ca[m][5]<<") * buffer["<<s2->op6<<"]";
            file << ";" << endl;
        }
        else
            file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"];" << endl;
    }

    //spherical B
    for (; s2<nseq[SPHB]; ++s2) {
        uint16_t m    = s2->aux;

        if (lb>1) {
            file << "    buffer["<<s2->dest<<"] = ";
            if (Cb[m][0]!=0) file << " + ("<<Cb[m][0]<<") * buffer["<<s2->op1<<"]";
            if (Cb[m][1]!=0) file << " + ("<<Cb[m][1]<<") * buffer["<<s2->op2<<"]";
            if (Cb[m][2]!=0) file << " + ("<<Cb[m][2]<<") * buffer["<<s2->op3<<"]";
            if (Cb[m][3]!=0) file << " + ("<<Cb[m][3]<<") * buffer["<<s2->op4<<"]";
            if (Cb[m][4]!=0) file << " + ("<<Cb[m][4]<<") * buffer["<<s2->op5<<"]";
            if (Cb[m][5]!=0) file << " + ("<<Cb[m][5]<<") * buffer["<<s2->op6<<"]";
            file << ";" << endl;
        }
        else
            file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"];" << endl;
    }

    //dest[i] = op1[i] - CDz[i] * op2[i];
    for (; s2<nseq[HRRKZ]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"]";
        if (CDz) file << " - CDz*buffer["<<s2->op2<<"]";
        file << ";" << endl;
    }

    //dest[i] = op1[i] - CDy[i] * op2[i];
    for (; s2<nseq[HRRKY]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"]";
        if (CDy) file << " - CDy*buffer["<<s2->op2<<"]";
        file << ";" << endl;
    }

    //dest[i] = op1[i];
    for (; s2<nseq[HRRKX]; ++s2) {
        file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"]";
        file << ";" << endl;
    }

    //spherical C
    for (; s2<nseq[SPHC]; ++s2) {
        uint16_t m    = s2->aux;

        if (lc>1) {
            file << "    buffer["<<s2->dest<<"] = ";
            if (Cc[m][0]!=0) file << " + ("<<Cc[m][0]<<") * buffer["<<s2->op1<<"]";
            if (Cc[m][1]!=0) file << " + ("<<Cc[m][1]<<") * buffer["<<s2->op2<<"]";
            if (Cc[m][2]!=0) file << " + ("<<Cc[m][2]<<") * buffer["<<s2->op3<<"]";
            if (Cc[m][3]!=0) file << " + ("<<Cc[m][3]<<") * buffer["<<s2->op4<<"]";
            if (Cc[m][4]!=0) file << " + ("<<Cc[m][4]<<") * buffer["<<s2->op5<<"]";
            if (Cc[m][5]!=0) file << " + ("<<Cc[m][5]<<") * buffer["<<s2->op6<<"]";
            file << ";" << endl;
        }
        else
            file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"];" << endl;
    }

    //spherical D
    for (; s2<nseq[SPHD]; ++s2) {
        uint16_t m    = s2->aux;

        if (ld>1) {
            file << "    buffer["<<s2->dest<<"] = ";
            if (Cd[m][0]!=0) file << " + ("<<Cd[m][0]<<") * buffer["<<s2->op1<<"]";
            if (Cd[m][1]!=0) file << " + ("<<Cd[m][1]<<") * buffer["<<s2->op2<<"]";
            if (Cd[m][2]!=0) file << " + ("<<Cd[m][2]<<") * buffer["<<s2->op3<<"]";
            if (Cd[m][3]!=0) file << " + ("<<Cd[m][3]<<") * buffer["<<s2->op4<<"]";
            if (Cd[m][4]!=0) file << " + ("<<Cd[m][4]<<") * buffer["<<s2->op5<<"]";
            if (Cd[m][5]!=0) file << " + ("<<Cd[m][5]<<") * buffer["<<s2->op6<<"]";
            file << ";" << endl;
        }
        else
            file << "    buffer["<<s2->dest<<"] = buffer["<<s2->op1<<"];" << endl;
    }

    int NM4 = int(2*la+1) * int(2*lb+1) * int(2*lc+1) * int(2*ld+1);

    for (; s2<nseq[REORDER]; ++s2) {
        file << "    ERIN[(k*NJ4+j)*"<<NM4<<"*NTX + "<<s2->dest<<"*NTX + i] = buffer["<<s2->op1<<"];" << endl;
    }

    file << "}" << endl;
    file << endl;

    file.close();
}
