/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <string>
using namespace std;

#include "ICgen.hpp"
#include "ERI.hpp"

void ERIroutine::WriteQIC() const {

    string fname = "IC_" + IdString() + ".qic";
    string fdir = QIC_DIR;

    if (fdir=="") return; //don't write
    fname = fdir + "/" + fname;
    //write to file
    ofstream file;
    file.open(fname.c_str(), ios::out | ios::binary);

    // write info about the routine

    file.write ((char*)(&la), sizeof (uint8_t));
    file.write ((char*)(&lb), sizeof (uint8_t));
    file.write ((char*)(&lc), sizeof (uint8_t));
    file.write ((char*)(&ld), sizeof (uint8_t));
    file.write ((char*)&geometry, sizeof (GEOM));
    file.write ((char*)&useCDR,   sizeof (bool));
    file.write ((char*)&useGC,    sizeof (bool));


    //K4
    {
        //number of instructions in each step
        for (uint8_t i=0; i<32; ++i)     file.write ((char*)&ninstrK4[i], sizeof (uint32_t));

        int totalK4 = 0;
        for (uint8_t iblock=BOYS; iblock<NADA; ++iblock) totalK4 += ninstrK4[iblock];

        //actual instructions
        for (uint32_t i=0; i<totalK4; ++i) file.write ((char*)&eseqK4[i], sizeof (Op_K4));
    }

    //MIRROR
    {
        //number of instructions in each step
        for (uint8_t i=0; i<32; ++i)     file.write ((char*)&ninstr[i], sizeof (uint32_t));

        int total = 0;
        for (uint8_t iblock=KERNELS; iblock<=REORDER; ++iblock)  total += ninstr[iblock];

        for (uint32_t i=0; i<total; ++i) file.write ((char*)&eseq[i], sizeof (Op_MIRROR));
    }

    file.write ((char*)&MaxMem, sizeof (uint32_t));
    file.write ((char*)&NFLOPS, sizeof (uint64_t));

    file.close();
}

