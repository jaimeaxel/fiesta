/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <set>

#include "../defs.hpp"

#include "IICgen.hpp"
#include "SKS.hpp"
#include "IICinit.hpp"

#include "../math/angular.hpp"




void ERIroutine::WriteGPULoopsContraction2(ofstream & file, const string & D, const string & S, const Op_K4 * IICbegin, const Op_K4 * IICend, bool firstloop) {

    for (const Op_K4 * s2=IICbegin; s2<IICend; ++s2) {
        //const cacheline64 * op1 = S + s2->op1;
        //cacheline64       * dest = D + s2->dest;
        //uint16_t aux                 =     s2->aux;

        //__builtin_prefetch(D + s2[PF].dest, 1, 1);
        if (firstloop)
            file << "        store("<<D<<"+"<<s2->dest<<", load("<<S<<"+"<<s2->op1<<") * Ws["<<s2->aux<<"]);" << endl; // " << s2->K << endl;
        else
            file << "        store("<<D<<"+"<<s2->dest<<", load("<<D<<"+"<<s2->dest<<") + load("<<S<<"+"<<s2->op1<<") * Ws["<<s2->aux<<"]);" << endl; // " << s2->K << endl;
    }

}


void ERIroutine::WriteGPULoopsContraction(ofstream & file, const string & D, const string & S,
                                int memOffset,
                                const string & W, int maxW,
                                const string & NN,
                                const string & JJ,
                                const Op_K4 * IICbegin,
                                const Op_K4 * IICend,
                                bool firstloop
                                ) {

    //for (int j=0; j<min(J, k+1); ++j)

    file << "      for (int j=0; j<"<<JJ<<"; ++j) {" << endl;
    file << "        double Ws["<<maxW+1<<"] __attribute__((aligned("<<CACHE_LINE_SIZE<<")));" << endl;

//        for (int w=0; w<=maxW; ++w)
//    file << "        Ws["<<w<<"] = "<<W<<"["<<w<<"] * "<<NN<<"[j];" << endl;

    file << "        Ws[0] = "<<NN<<"[j];" << endl;

        for (int w=1; w<=maxW; ++w)
    file << "        Ws["<<w<<"] = Ws["<<w-1<<"] * "<<W<<";" << endl;

        WriteLoopsContraction2(file, D, S, IICbegin, IICend, firstloop);

    file << "        "<<D<<" += "<<memOffset<<";" << endl;
    file << "      }" << endl;
}


void ERIroutine::WriteGPUInnerContraction() {

    string ICRname = "K2C_" + IdString(true);
    string EFSdir = K2C_DIR;
    if (EFSdir=="") return; //skip

    string K2file = EFSdir + "/" + ICRname + ".cpp";

    ofstream file;
    file.open(K2file.c_str());
    file.precision(16);

    file << "#include <string.h>" << endl;
    file << "#include \"low/cache64.hpp\"" << endl;
    file << "#include \"integrals/rotations.hpp\"" << endl;
    file << "#include \"basis/SPprototype.hpp\"" << endl;
    file << "#include \"2eints/quimera.hpp\"" << endl;

    file << endl;

    file << "static const double im2[32] = {";
    for (int m=0; m<31; ++m)
        file << "1./" << 2*m+1 << "., ";
    file << "1./63.};" << endl;

    file << endl;



    file << "void " << ICRname << "(cacheline64 * (&F0), const ERIgeometries64 & vars8, const PrimitiveSet & PSab, const ShellPairPrototype & ABp, double ikcd, double rcd, p_ERIbuffer & buffer, cacheline64 & AB2, cacheline64 & X2) { " << endl;

    file << "  const cacheline64 & ABz = vars8.ABz;" << endl;
    file << "  const cacheline64 & CDy = vars8.CDy;" << endl;
    file << "  const cacheline64 & CDz = vars8.CDz;" << endl;

    file << "  const cacheline64 & ACx = vars8.ACx;" << endl;
    file << "  const cacheline64 & ACy = vars8.ACy;" << endl;
    file << "  const cacheline64 & ACz = vars8.ACz;" << endl;

    file << "  const int JA = ABp.Ja;" << endl;
    file << "  const int JB = ABp.Jb;" << endl;

    file << "  const int nJ1 = JA;" << endl;
    file << "  const int nJ2 = JA*JB;" << endl;

    file << "  const int mK3f = nJ2*"<<memK3J2f<<";" << endl;

    file << "  const int mK4f = nJ1*"<<memK4J1f<<";" << endl;

    file << "  const int mK3e = nJ2*"<<memK3J2e<<";" << endl;
    file << "  const int mK4e = nJ1*"<<memK4J1e<<";" << endl;

        if (geometry==ABCD || geometry==ABAD) {
    file << "  cacheline64 AQy;" << endl;
    file << "  cacheline64 AQz;" << endl;

    file << "  cacheline64 X2Y2;" << endl;
    file << "  cacheline64 AQ2;" << endl;

    file << "  cacheline64 AQAB;" << endl;

    file << "  AQy = ACy + CDy * rcd;" << endl;
    file << "  AQz = ACz + CDz * rcd;" << endl;

    file << "  X2Y2 = X2   + AQy*AQy;" << endl;
    file << "  AQ2  = X2Y2 + AQz*AQz;" << endl;

    file << "  AQAB = (AQz*ABz); AQAB += AQAB;" << endl;
        }
        else if (geometry==AACD) {
    file << "  cacheline64 AQz;" << endl;
    file << "  cacheline64 AQ2;" << endl;

    file << "  AQz = ACz + CDz * rcd;" << endl;
    file << "  AQ2  = X2 + AQz*AQz;" << endl;
        }



    file << "  memset(buffer.K3J2e, 0, mK3e*sizeof(cacheline64));" << endl;
    file << "  memset(buffer.K3J2f, 0, mK3f*sizeof(cacheline64));" << endl;


    file << "  for (int b=0; b<PSab.nKb; ++b) {" << endl; {
    file << "    {" << endl; {

        if (geometry==ABCD || geometry==ABAD) {
    file << "      double rab  = ABp.BCP.r[b][0];" << endl;
    file << "      cacheline64 PQz;  PQz = AQz - ABz*rab;" << endl;
    file << "      cacheline64 R2;   R2 = X2Y2 + PQz*PQz;" << endl;
                }

    file << "      double ikab = ABp.BCP.k[b][0];" << endl;
    file << "      double iKpq  = (ikab + ikcd);" << endl;

    file << "      cacheline64 * F0e = (cacheline64*)buffer.F0e;" << endl;
    file << "      cacheline64 * F0f = (cacheline64*)buffer.F0f;" << endl;

                //copy the gamma of higher L and the contracted exponential
    file << "      F0e[0] = F0[0];" << endl;
    file << "      F0f[0] = F0[1];" << endl;

                //AERR K4 (uncontracted)
                {
                    Op_K4 * s2=nseqK4[AERR4]; ++s2; //skip the first exponential
                    for (; s2<nseqK4[AERR4+1]; ++s2)
    file << "      F0e["<<s2->dest<<"] = F0e["<<s2->op1<<"] * iKpq;" << endl;// " << s2->K << endl;
                }

                //CDR K4 (uncontracted downward recursion)
                {
                    Op_K4 * s2=nseqK4[CDR4]; ++s2; //skip the first element, which is the gamma function
                    for (; s2<nseqK4[CDR4+1]; ++s2) {
                        if (geometry==ABCD || geometry==ABAD)
    file << "      F0f["<<s2->dest<<"] = (R2 *  F0f["<<s2->op1<<"] + F0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl; // " << s2->K << endl;
                        else if (geometry==AACD)
    file << "      F0f["<<s2->dest<<"] = (AQ2 * F0f["<<s2->op1<<"] + F0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl; // " << s2->K << endl;
                        else if (geometry==AACC)
    file << "      F0f["<<s2->dest<<"] = (X2  * F0f["<<s2->op1<<"] + F0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl; // " << s2->K << endl;
                    }
                }


                //K4J1
                {
    file << "      cacheline64 * Je = (cacheline64*)buffer.K4J1e;" << endl;
    file << "      cacheline64 * Jf = (cacheline64*)buffer.K4J1f;" << endl;

    //for (int j=0; j<min(J, k+1); ++j)
    file << "      for (int j=0; j<JA; ++j) {" << endl;
    file << "        double Ws["<<maxV+1<<"] __attribute__((aligned("<<CACHE_LINE_SIZE<<")));" << endl;
    file << "        Ws[0] = ABp.BCP.Na[0][j];" << endl;
        for (int w=1; w<=maxV; ++w)
    file << "        Ws["<<w<<"] = Ws["<<w-1<<"] * ABp.BCP.k[b][0];" << endl;
        WriteLoopsContraction2(file, "Je", "F0e", nseqK4[K4E], nseqK4[K4E+1], true);
        WriteLoopsContraction2(file, "Jf", "F0f", nseqK4[K4F], nseqK4[K4F+1], true);
    file << "        Je += "<<memK4J1e<<";" << endl;
    file << "        Jf += "<<memK4J1f<<";" << endl;
    file << "      }" << endl;

                }

    file << "      F0 += 2;" << endl;
    file << "    }" << endl; }


    file << "    for (int a=1; a<PSab.nKa[b]; ++a) {" << endl; {

        if (geometry==ABCD || geometry==ABAD) {
    file << "      double rab  = ABp.BCP.r[b][a];" << endl;
    file << "      cacheline64 PQz;  PQz = AQz - ABz*rab;" << endl;
    file << "      cacheline64 R2;   R2 = X2Y2 + PQz*PQz;" << endl;
                }

    file << "      double ikab = ABp.BCP.k[b][a];" << endl;
    file << "      double iKpq  = (ikab + ikcd);" << endl;

    file << "      cacheline64 * F0e = (cacheline64*)buffer.F0e;" << endl;
    file << "      cacheline64 * F0f = (cacheline64*)buffer.F0f;" << endl;

                //copy the gamma of higher L and the contracted exponential
    file << "      F0e[0] = F0[0];" << endl;
    file << "      F0f[0] = F0[1];" << endl;


                //AERR K4 (uncontracted)
                {
                    Op_K4 * s2=nseqK4[AERR4]; ++s2; //skip the first exponential
                    for (; s2<nseqK4[AERR4+1]; ++s2)
    file << "      F0e["<<s2->dest<<"] = F0e["<<s2->op1<<"] * iKpq;" << endl; // " << s2->K << endl;
                }

                //CDR K4 (uncontracted downward recursion)
                {
                    Op_K4 * s2=nseqK4[CDR4]; ++s2; //skip the first element, which is the gamma function
                    for (; s2<nseqK4[CDR4+1]; ++s2) {
                        if (geometry==ABCD || geometry==ABAD)
    file << "      F0f["<<s2->dest<<"] = (R2 * F0f["<<s2->op1<<"] + F0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl; // " << s2->K << endl;
                        else if (geometry==AACD)
    file << "      F0f["<<s2->dest<<"] = (AQ2 * F0f["<<s2->op1<<"] + F0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl; // " << s2->K << endl;
                        else if (geometry==AACC)
    file << "      F0f["<<s2->dest<<"] = (X2  * F0f["<<s2->op1<<"] + F0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl; // " << s2->K << endl;
                    }
                }




                {
    file << "      cacheline64 * Je = (cacheline64*)buffer.K4J1e;" << endl;
    file << "      cacheline64 * Jf = (cacheline64*)buffer.K4J1f;" << endl;

    file << "      for (int j=0; j<JA; ++j) {" << endl;
    file << "        double Ws["<<maxV+1<<"] __attribute__((aligned("<<CACHE_LINE_SIZE<<")));" << endl;
    file << "        Ws[0] = ABp.BCP.Na[a][j];" << endl;
        for (int w=1; w<=maxV; ++w)
    file << "        Ws["<<w<<"] = Ws["<<w-1<<"] * ABp.BCP.k[b][a];" << endl;
        WriteLoopsContraction2(file, "Je", "F0e", nseqK4[K4E], nseqK4[K4E+1], false);
        WriteLoopsContraction2(file, "Jf", "F0f", nseqK4[K4F], nseqK4[K4F+1], false);
    file << "        Je += "<<memK4J1e<<";" << endl;
    file << "        Jf += "<<memK4J1f<<";" << endl;
    file << "      }" << endl;

                }

    file << "      F0 += 2;" << endl;
    file << "    }" << endl; }



            //K3J1 //AERR K3
    file << "    {" << endl; {
    file << "      cacheline64 * Jf = (cacheline64*)buffer.K4J1f;" << endl;
    file << "      cacheline64 * Je = (cacheline64*)buffer.K4J1e;" << endl;

    file << "      __m128d ikcd_v = _mm_load1_pd(&ikcd);" << endl;
                //u_atom ikcd_v(ikcd);

    file << "      for (int nj1=0; nj1<nJ1; ++nj1) {" << endl;
                   for (Op_K4 * s2=nseqK4[AERR3]; s2<nseqK4[AERR3+1]; ++s2) {
    file << "        store(Je+"<<s2->dest<<", load(Je+"<<s2->op1<<") + load(Je+"<<s2->op2<<") * ikcd_v);" << endl; // " << s2->K << endl;
                    }

    file << "        Je += "<<memK4J1e<<";" << endl;
    file << "      }" << endl;
    file << "    }" << endl; }

            //CDR3
    file << "    {" << endl; {

                if (geometry==ABCD || geometry==ABAD) {
    file << "      cacheline64 AQABb; AQABb = AQAB * ABp.BCP.b1[b];" << endl;
    file << "      cacheline64 AB2b2; AB2b2 = AB2  * ABp.BCP.b2[b];" << endl;
                }

    file << "      cacheline64 * Jf = (cacheline64*)buffer.K4J1f;" << endl;
    file << "      cacheline64 * Je = (cacheline64*)buffer.K4J1e;" << endl;

    file << "      for (int nj1=0; nj1<nJ1; ++nj1) {" << endl;
                    for (Op_K4 * s2=nseqK4[CDR3]; s2<nseqK4[CDR3+1]; ++s2) {
                      if (geometry==ABCD || geometry==ABAD) {
    file << "        store(Jf+"<<s2->dest<<", ("
                      <<    " AQ2*load(Jf+"<<s2->op1<<")"
                     << " - AQABb*load(Jf+"<<s2->op2<<")"
                     << " + AB2b2*load(Jf+"<<s2->op3<<")"
                           << " + load(Je+"<<s2->ope<<")"
                           << ") * im2["<<s2->aux<<"] );" << endl; // " << s2->K << endl;
                      }
                      else if (geometry==AACD) {
    file << "        store(Jf+"<<s2->dest<<", ("
                      <<    " AQ2*load(Jf+"<<s2->op1<<")"
                           << " + load(Je+"<<s2->ope<<")"
                           << ") * im2["<<s2->aux<<"] );" << endl; // " << s2->K << endl;
                      }
                      else if (geometry==AACC) {
    file << "        store(Jf+"<<s2->dest<<", ("
                      <<    "  X2*load(Jf+"<<s2->op1<<")"
                           << " + load(Je+"<<s2->ope<<")"
                           << ") * im2["<<s2->aux<<"] );" << endl; // " << s2->K << endl;
                      }
                    }

    file << "        Jf += "<<memK4J1f<<";" << endl;
    file << "        Je += "<<memK4J1e<<";" << endl;
    file << "      }" << endl;
    file << "    }" << endl; }

            //K3E
    file << "    {" << endl; {
    file << "      const cacheline64 * Je = (cacheline64*)buffer.K4J1e;" << endl;
    file << "      const cacheline64 * Jf = (cacheline64*)buffer.K4J1f;" << endl;
    file << "      cacheline64       * J2e = (cacheline64*)buffer.K3J2e;" << endl;
    file << "      cacheline64       * J2f = (cacheline64*)buffer.K3J2f;" << endl;

    file << "      double Uu["<<int(maxJ)<<"]["<<maxU+1<<"] __attribute__((aligned("<<CACHE_LINE_SIZE<<")));" << endl;

    file << "      for (int j=0; j<JB; ++j) {" << endl;
    file << "        Uu[j][0] = ABp.BCP.Nb[b][j];" << endl;

    file << "        for (int u=1; u<="<<maxU<<"; ++u)" << endl;
    file << "            Uu[j][u] = Uu[j][u-1] * ABp.BCP.b1[b];" << endl;
    file << "      }" << endl;


    file << "      for (int nj1=0; nj1<nJ1; ++nj1) {" << endl;

    file << "        for (int j=0; j<JB; ++j) {" << endl;

    for (const Op_K4 * s2=nseqK4[K3E]; s2<nseqK4[K3E+1]; ++s2)
            file << "          store(J2e + "<<s2->dest<<"+j*"<<memK3J2e<<", load(J2e + "<<s2->dest<<"+j*"<<memK3J2e<<") + load(Je + "<<s2->op1<<") * Uu[j]["<<s2->aux<<"]);" << endl;

    for (const Op_K4 * s2=nseqK4[K3F]; s2<nseqK4[K3F+1]; ++s2)
            file << "          store(J2f + "<<s2->dest<<"+j*"<<memK3J2f<<", load(J2f + "<<s2->dest<<"+j*"<<memK3J2f<<") + load(Jf + "<<s2->op1<<") * Uu[j]["<<s2->aux<<"]);" << endl;

    file << "        }" << endl;

    file << "        J2e += JB*"<<memK3J2e<<";" << endl;
    file << "        J2f += JB*"<<memK3J2f<<";" << endl;
    file << "        Je += "<<memK4J1e<<";" << endl;
    file << "        Jf += "<<memK4J1f<<";" << endl;

    file << "      }" << endl;
    file << "    }" << endl; }


    file << "  }" << endl; } //loop over b


    //AERR K2
    file << "  {" << endl; {
    file << "    cacheline64 * J2 = (cacheline64*)buffer.K3J2e;" << endl;

    file << "    __m128d ikcd_v = _mm_load1_pd(&ikcd);" << endl;
            //u_atom ikcd_v(ikcd);

            //K2J2
    file << "    for (int nj2=0; nj2<nJ2; ++nj2) {" << endl;
                for (Op_K4 * s2=nseqK4[AERR2]; s2<nseqK4[AERR2+1]; ++s2) {
    file << "        store(J2+"<<s2->dest<<", load(J2+"<<s2->op1<<") + load(J2+"<<s2->op2<<") * ikcd_v);" << endl; // " << s2->K << endl;

                }
    file << "      J2 += "<<memK3J2e<<";" << endl;
    file << "    }" << endl;
    file << "  }" << endl; }

    //CDR K2
    file << "  {" << endl; {
    file << "    cacheline64 * J2 = (cacheline64*)buffer.K3J2f;" << endl;
    file << "    cacheline64 * Je = (cacheline64*)buffer.K3J2e;" << endl;

    file << "    for (int nj2=0; nj2<nJ2; ++nj2) {" << endl;
                for (Op_K4*s2 = nseqK4[CDR2]; s2<nseqK4[CDR2+1]; ++s2) {


                      if (geometry==ABCD || geometry==ABAD) {
    file << "        store(J2+"<<s2->dest<<", ("
                      <<    " AQ2*load(J2+"<<s2->op1<<")"
                      << " - AQAB*load(J2+"<<s2->op2<<")"
                       << " + AB2*load(J2+"<<s2->op3<<")"
                           << " + load(Je+"<<s2->ope<<")"
                           << ") * im2["<<s2->aux<<"] );" << endl; // " << s2->K << endl;
                      }
                      else if (geometry==AACD) {
    file << "        store(J2+"<<s2->dest<<", ("
                      <<    " AQ2*load(J2+"<<s2->op1<<")"
                           << " + load(Je+"<<s2->ope<<")"
                           << ") * im2["<<s2->aux<<"] );" << endl; // " << s2->K << endl;
                      }
                      else if (geometry==AACC) {
    file << "        store(J2+"<<s2->dest<<", ("
                      <<    " X2*load(J2+"<<s2->op1<<")"
                           << " + load(Je+"<<s2->ope<<")"
                           << ") * im2["<<s2->aux<<"] );" << endl; // " << s2->K << endl;
                      }
                }
    file << "      J2 += "<<memK3J2f<<";" << endl;
    file << "      Je += "<<memK3J2e<<";" << endl;
    file << "    }" << endl;
    file << "  }" << endl; }



    file << "}" << endl;


    file.close();
}




