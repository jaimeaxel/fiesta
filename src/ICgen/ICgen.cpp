/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
    Interpreted code and Inner Contraction routines generator for the Echidna Fock Solver

    Jaime Axel Rosal Sandberg, August 2013

    Input required:
    L        :   maximum angular momentum
    QIC_DIR  :   directory where interpreted code files are to be stored
    K2C_DIR  :   directory where the specialized innermost contractions of the K4 routines will be written
    K4CU_DIR :   directory for CUDA metaprogrammed K4 routines
    MCU_DIR  :   directory for CUDA metaprogrammed MIRROR transformations

*/


#include <iostream>
#include <queue>
#include <string>
#include <string.h>
using namespace std;

#include "math/angular.hpp"
using namespace LibAngular;

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
#endif

#include "ERI.hpp"

std::string QIC_DIR;
std::string K2C_DIR;
std::string K4CU_DIR;
std::string MCU_DIR;


int main(int argc, char* arg[]) {

    int L = 3; //default

    //int L_QIC  =  3; // wouldn't make sense
    //don't do anything by default
    int L_K2C  = -1;
    int L_K4CU = -1;
    int L_MCU  = -1;


    QIC_DIR   = ""; //qic/";
    K2C_DIR   = ""; //./k2c/";
    K4CU_DIR  = ""; //./k4cu/";
    MCU_DIR   = ""; //./mcu/";

    bool wQIC  = false;
    bool wK2C  = false;
    bool wK4CU = false;
    bool wMCU  = false;

    for (int i=1; i<argc; ++i) {
        if (strncmp(arg[i], "-L=", 3) == 0) {
            L = atoi(arg[i]+3);
        }

        //else if (strncmp(arg[i], "-LQIC=",  6) == 0) {
        //    L_QIC  = atoi(arg[i]+6);
        //}
        else if (strncmp(arg[i], "-LK2C=",  6) == 0) {
            L_K2C  = atoi(arg[i]+6);
        }
        else if (strncmp(arg[i], "-LK4CU=", 7) == 0) {
            L_K4CU = atoi(arg[i]+7);
        }
        else if (strncmp(arg[i], "-LMCU=",  6) == 0) {
            L_MCU  = atoi(arg[i]+6);
        }


        else if (strncmp(arg[i], "-QIC=",  5) == 0) {
            QIC_DIR = string(arg[i]+5);
            wQIC = true;
        }
        else if (strncmp(arg[i], "-K2C=",  5) == 0) {
            K2C_DIR = string(arg[i]+5);
            wK2C = true;
        }
        else if (strncmp(arg[i], "-K4CU=", 6) == 0) {
            K4CU_DIR = string(arg[i]+6);
            wK4CU = true;
        }
        else if (strncmp(arg[i], "-MCU=",  5) == 0) {
            MCU_DIR = string(arg[i]+5);
            wMCU = true;
        }
    }


    if ( L<0 || L>5) {
        cout << "Error: angular momentum out of bounds (0-5)" << endl;
        return 1;
    }

    cout << "Maximum angular momentum       : " << L        << endl;
    cout << "Maximum total L for K2C        : " << L_K2C    << endl;
    cout << "Maximum total L for GPU K4     : " << L_K4CU   << endl;
    cout << "Maximum total L for GPU MIRROR : " << L_MCU    << endl;
    cout << "QIC directory                  : " << QIC_DIR  << endl;
    cout << "K2C directory                  : " << K2C_DIR  << endl;
    cout << "CUDA K4 directory              : " << K4CU_DIR << endl;
    cout << "CUDA MIRROR directory          : " << MCU_DIR  << endl;


    //initialize spherical harmonics coefficients
    InitAngular();

    std::priority_queue <ERItype> SetLater;

    for (int la=0; la<=L; ++la) {
        for (int lb=0; lb<=la; ++lb) {
            for (int lc=0; lc<=L; ++lc) {
                for (int ld=0; ld<=lc; ++ld) {

                    ERItype type;

                    // 4-center and degenerate 4-center integrals
                    if (la>lc || la==lc && lb>=ld) {
                        type(ABCD, la, lb, lc, ld, true, false, false);
                        SetLater.push(type);

                        type(ABAD, la, lb, lc, ld, true, false, false);
                        SetLater.push(type);

                        type(ABAB, la, lb, lc, ld, true, false, false);
                        SetLater.push(type);
                    }

                    // 3-center integrals
                    type(AACD, la, lb, lc, ld, true, false, false);
                    SetLater.push(type);

                    // 2-center integrals
                    if (la>lc || la==lc && lb>=ld) {
                        type(AACC, la, lb, lc, ld, true, false, false);
                        SetLater.push(type);
                    }

                    // 1-center integrals
                    if ((la+lb+lc+ld)%2)                continue;
                    if ((lb==0) && (ld==0) && (la!=lc)) continue;

                    type(AAAA, la, lb, lc, ld, true, false, false);
                    SetLater.push(type);
                }
            }
        }
    }


    cout << "Writing interpreted code routines" << endl;

    int n = SetLater.size();

    #pragma omp parallel for schedule(dynamic)
    for (uint32_t i=0; i<n; ++i) {

        ERItype type;

        #pragma omp critical
        {
            type = SetLater.top();
            SetLater.pop();
            cout << ".";
            cout.flush();
        }

        int Lt = type.La + type.Lb + type.Lc + type.Ld;

        ERIroutine IC;

        IC.Set      (type.La, type.Lb, type.Lc, type.Ld, type.geometry, type.isCDR);
        IC.Generate (wQIC, wK2C && (Lt<=L_K2C), wK4CU && (Lt<=L_K4CU), wMCU && (Lt<=L_MCU));
    }
    cout << endl;




    cout << "Writing headers and loading routines" << endl;

    //inner contraction CPU routines
    if (wK2C) {
        string k2cfile= K2C_DIR + "/K2C.cpp";
        ofstream file;

        file.open(k2cfile.c_str());
        file.precision(16);


        file << "class cacheline64;" << endl;
        file << "class ERIgeometries64;" << endl;
        file << "class PrimitiveSet;" << endl;
        file << "class ShellPairPrototype;" << endl;
        file << "class p_ERIbuffer;" << endl;
        file << endl;

        //
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lcd=0; lcd<=la+lb; ++lcd) {
                    if (la+lb+lcd <= L_K2C)
                        file << "void K2C_ABCD_" << L2S(la) << L2S(lb) << L2S(lcd) <<"(cacheline64 * (&F0), const ERIgeometries64 & vars8, const PrimitiveSet & PSab, const ShellPairPrototype & ABp, double ikcd, double rcd, p_ERIbuffer & buffer, cacheline64 & AB2, cacheline64 & X2, double iw2);" << endl;
                }
            }
        }

        file << endl;
        file << "#include \"2eints/IIC.hpp\"" << endl;
        file << endl;

        file << "void ERIroutine::LoadK2C() {" << endl;

        file << "  int lcd = lc+ld;"  << endl;
        file << "  if (geometry==ABCD) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lcd=0; lcd<=la+lb; ++lcd) {
                    if (la+lb+lcd <= L_K2C)
                        file << "    if (la=="<<la<<" && lb=="<<lb<<" && lcd=="<<lcd<<") InnerContractionRoutine = K2C_ABCD_"<<L2S(la)<<L2S(lb)<<L2S(lcd)<<";" << endl;
                }
            }
        }
        file << "  }" << endl;
        file << "}" << endl;

        file << endl;

    }

    //whole contraction GPU routines
    if (wK4CU) {
        string k4gpufile= K4CU_DIR + "/K4gpu.cu";
        ofstream file;

        file.open(k4gpufile.c_str());
        file.precision(16);

        file << "#include <stdint.h>" << endl; //for the integer types

        //file << "class ShellPairPrototype;" << endl;
        //file << "class p_ERIbuffer;" << endl;
        file << endl;

        //
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_K4CU) {
                            file << "__global__ void K4_ABCD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << "(const double * vars, const double * d_F0,   double * uv_m_st, " << endl;
                            file << "        uint8_t   KA, uint8_t   KB, uint8_t   KC, uint8_t   KD, "  << endl;
                            file << "        uint8_t   JA, uint8_t   JB, uint8_t   JC, uint8_t   JD, "  << endl;
                            file << "        uint16_t nJ1, uint16_t nJ2, uint16_t nJ3, uint16_t nJ4, "  << endl;
                            file << "        const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd, "  << endl;
                            file << "        const double * iKa, const double * iKb, const double * iKc, const double * iKd, "  << endl;
                            file << "        const double * iKba, const double * iKdc, "  << endl;
                            file << "        const double * iKs3, const double * iKt3, "  << endl;
                            file << "        const double *  Kba, const double *  Kdc, "  << endl;

                            file << "        const double * Nav, const double * Nbu, const double * Nct, const double * Nds, "  << endl;
                            file << "        const double * Na, const double * Nb, const double * Nc, const double * Nd, "  << endl;
                            file << "        uint32_t maxTiles, double ilogThresh);" << endl;
                        }
                    }
                }
            }
        }

        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=L; ++lc) {
                    for (int ld=0; ld<=lc; ++ld) {
                        if (la+lb+lc+ld<=L_K4CU) {
                            file << "__global__ void K4_AACD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << "(const double * vars, const double * d_F0,   double * uv_m_st, " << endl;
                            file << "        uint8_t   KA, uint8_t   KB, uint8_t   KC, uint8_t   KD, "  << endl;
                            file << "        uint8_t   JA, uint8_t   JB, uint8_t   JC, uint8_t   JD, "  << endl;
                            file << "        uint16_t nJ1, uint16_t nJ2, uint16_t nJ3, uint16_t nJ4, "  << endl;
                            file << "        const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd, "  << endl;
                            file << "        const double * iKa, const double * iKb, const double * iKc, const double * iKd, "  << endl;
                            file << "        const double * iKba, const double * iKdc, "  << endl;
                            file << "        const double * iKs3, const double * iKt3, "  << endl;
                            file << "        const double *  Kba, const double *  Kdc, "  << endl;

                            file << "        const double * Nav, const double * Nbu, const double * Nct, const double * Nds, "  << endl;
                            file << "        const double * Na, const double * Nb, const double * Nc, const double * Nd, "  << endl;
                            file << "        uint32_t maxTiles, double ilogThresh);" << endl;
                        }
                    }
                }
            }
        }

        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_K4CU) {
                            file << "__global__ void K4_AACC_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << "(const double * vars, const double * d_F0,   double * uv_m_st, " << endl;
                            file << "        uint8_t   KA, uint8_t   KB, uint8_t   KC, uint8_t   KD, "  << endl;
                            file << "        uint8_t   JA, uint8_t   JB, uint8_t   JC, uint8_t   JD, "  << endl;
                            file << "        uint16_t nJ1, uint16_t nJ2, uint16_t nJ3, uint16_t nJ4, "  << endl;
                            file << "        const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd, "  << endl;
                            file << "        const double * iKa, const double * iKb, const double * iKc, const double * iKd, "  << endl;
                            file << "        const double * iKba, const double * iKdc, "  << endl;
                            file << "        const double * iKs3, const double * iKt3, "  << endl;
                            file << "        const double *  Kba, const double *  Kdc, "  << endl;

                            file << "        const double * Nav, const double * Nbu, const double * Nct, const double * Nds, "  << endl;
                            file << "        const double * Na, const double * Nb, const double * Nc, const double * Nd, "  << endl;
                            file << "        uint32_t maxTiles, double ilogThresh);" << endl;
                        }
                    }
                }
            }
        }

        file << endl;
        file << "#include \"2eints/IIC.hpp\"" << endl;
        file << endl;

        file << "void ERIroutine::LoadK4GPU() {" << endl;

        file << "  GPUK4ContractionRoutine = NULL;" << endl;

        file << "  if (geometry==ABCD || geometry==ABAD || geometry==ABAB) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_K4CU)
                            file << "    if (la=="<<la<<" && lb=="<<lb<<" && lc=="<<lc<<" && ld=="<<ld<<") GPUK4ContractionRoutine = K4_ABCD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << ";" << endl;
                    }
                }
            }
        }
        file << "  }" << endl;

        file << "  else if (geometry==AACD) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=L; ++lc) {
                    for (int ld=0; ld<=lc; ++ld) {
                        if (la+lb+lc+ld<=L_K4CU)
                            file << "    if (la=="<<la<<" && lb=="<<lb<<" && lc=="<<lc<<" && ld=="<<ld<<") GPUK4ContractionRoutine = K4_AACD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << ";" << endl;
                    }
                }
            }
        }
        file << "  }" << endl;

        file << "  else if (geometry==AACC) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_K4CU)
                            file << "    if (la=="<<la<<" && lb=="<<lb<<" && lc=="<<lc<<" && ld=="<<ld<<") GPUK4ContractionRoutine = K4_AACC_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << ";" << endl;
                    }
                }
            }
        }
        file << "  }" << endl;
        file << "}" << endl;

        file << endl;

        file << "void DummyIncludeGPUKernels() {" << endl;
        file << "  double *vars, *d_F0, *uv_m_st;" << endl;
        file << "  uint8_t   KA, KB, KC, KD;" << endl;
        file << "  uint8_t   JA, JB, JC, JD;" << endl;
        file << "  uint16_t nJ1, nJ2, nJ3, nJ4;" << endl;
        file << "  double *Ka, *Kb, *Kc, *Kd,  *iKa, *iKb, *iKc, *iKd, *iKab, *iKcd, *Na, *Nb, *Nc, *Nd;" << endl;
        file << "  double *iKs3, *iKt3, *Kab, *Kcd, *Nav, *Nbu, *Nct, *Nds;" << endl;
        file << "  uint32_t maxTiles;" << endl;
        file << "  double iLogThresh;" << endl;

        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_K4CU)
                            file << "  K4_ABCD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << " <<<1,1>>> (vars,d_F0,uv_m_st, KA,KB,KC,KD, JA,JB,JC,JD, nJ1,nJ2,nJ3,nJ4, Ka,Kb,Kc,Kd,iKa,iKb,iKc,iKd, iKab,iKcd, iKs3,iKt3,Kab,Kcd,Nav,Nbu,Nct,Nds, Na,Nb,Nc,Nd, maxTiles, iLogThresh);" << endl;
                    }
                }
            }
        }
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=L; ++lc) {
                    for (int ld=0; ld<=lc; ++ld) {
                        if (la+lb+lc+ld<=L_K4CU)
                            file << "  K4_AACD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << " <<<1,1>>> (vars,d_F0,uv_m_st, KA,KB,KC,KD, JA,JB,JC,JD, nJ1,nJ2,nJ3,nJ4, Ka,Kb,Kc,Kd,iKa,iKb,iKc,iKd, iKab,iKcd, iKs3,iKt3,Kab,Kcd,Nav,Nbu,Nct,Nds, Na,Nb,Nc,Nd, maxTiles, iLogThresh);" << endl;
                    }
                }
            }
        }
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_K4CU)
                            file << "  K4_AACC_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << " <<<1,1>>> (vars,d_F0,uv_m_st, KA,KB,KC,KD, JA,JB,JC,JD, nJ1,nJ2,nJ3,nJ4, Ka,Kb,Kc,Kd,iKa,iKb,iKc,iKd, iKab,iKcd, iKs3,iKt3,Kab,Kcd,Nav,Nbu,Nct,Nds, Na,Nb,Nc,Nd, maxTiles, iLogThresh);" << endl;
                    }
                }
            }
        }

        file << "}" << endl;

        file << endl;

    }

    //transformation GPU routines
    if (wMCU) {
        string Mgpufile= MCU_DIR + "/Mgpu.cu";
        ofstream file;

        file.open(Mgpufile.c_str());
        file.precision(16);

        file << endl;

        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_MCU)
                            file << "__global__ void Mgpu_ABCD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) <<"(const double  * mem, const double * vars, double * ERIN, int maxTiles);" << endl;
                    }
                }
            }
        }

        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=L; ++lc) {
                    for (int ld=0; ld<=lc; ++ld) {
                        if (la+lb+lc+ld<=L_MCU)
                            file << "__global__ void Mgpu_AACD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) <<"(const double  * mem, const double * vars, double * ERIN, int maxTiles);" << endl;
                    }
                }
            }
        }

        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_MCU)
                            file << "__global__ void Mgpu_AACC_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) <<"(const double  * mem, const double * vars, double * ERIN, int maxTiles);" << endl;
                    }
                }
            }
        }

        file << endl;
        file << "#include \"2eints/IIC.hpp\"" << endl;
        file << endl;

        file << "void ERIroutine::LoadMGPU() {" << endl;

        file << "  GPUMirrorTransformation = NULL;" << endl;

        file << "  if (geometry==ABCD || geometry==ABAD || geometry==ABAB) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_MCU)
                            file << "    if (la=="<<la<<" && lb=="<<lb<<" && lc=="<<lc<<" && ld=="<<ld<<") GPUMirrorTransformation = Mgpu_ABCD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << ";" << endl;
                    }
                }
            }
        }
        file << "  }" << endl;

        file << "  else if (geometry==AACD) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=L; ++lc) {
                    for (int ld=0; ld<=lc; ++ld) {
                        if (la+lb+lc+ld<=L_MCU)
                            file << "    if (la=="<<la<<" && lb=="<<lb<<" && lc=="<<lc<<" && ld=="<<ld<<") GPUMirrorTransformation = Mgpu_AACD_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << ";" << endl;
                    }
                }
            }
        }
        file << "  }" << endl;

        file << "  if (geometry==AACC) {" << endl;
        for (int la=0; la<=L; ++la) {
            for (int lb=0; lb<=la; ++lb) {
                for (int lc=0; lc<=la; ++lc) {
                    for (int ld=0; ld<=((lc==la)?lb:lc); ++ld) {
                        if (la+lb+lc+ld<=L_MCU)
                            file << "    if (la=="<<la<<" && lb=="<<lb<<" && lc=="<<lc<<" && ld=="<<ld<<") GPUMirrorTransformation = Mgpu_AACC_" << L2S(la) << L2S(lb) << L2S(lc) << L2S(ld) << ";" << endl;
                    }
                }
            }
        }
        file << "  }" << endl;




        file << "}" << endl;

        file << endl;
    }


    return 0;
}
