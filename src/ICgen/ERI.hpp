/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/




#ifndef __ERI_ROUTINE__
#define __ERI_ROUTINE__

#include <cstdlib>
#include <string>
#include <fstream>
#include "defs.hpp"

enum RRTYPE  {KERNELS, MMDZ, CTEBZ, CTEKZ, MMDY, MMDX, CTEKY, CTEKX, CTEBY, CTEBX, HRRBZ, HRRBY, HRRBX, SPHA, SPHB, HRRKZ, HRRKY, HRRKX, SPHC, SPHD, REORDER, OTHER};

enum RRTYPE2 {BOYS,
                ADRR4, AERR4, CDR4, K4D, K4E, K4F,
                ADRR3, AERR3, CDR3, K3D, K3E, K3F,
                ADRR2, AERR2, CDR2, K2D, K2E, K2F,
                ADRR1, AERR1, CDR1, K1D, K1E, K1F,
                ADRR0, AERR0, CDR0,
                NADA};  // 29 < 32


//this is HUGE!!!
class Op_K4 {
  public:
    uint16_t dest;
    uint16_t op1;
    uint16_t op2;
    uint16_t op3;
    uint16_t op4;
    uint16_t op5;
    uint16_t op6;
    uint16_t ope;
    uint16_t aux;
}; // __attribute__((aligned(32)));

class Op_MIRROR {
  public:
    uint32_t dest;
    uint32_t op1;
    uint32_t op2;
    uint32_t op3;
    uint32_t op4;
    uint32_t op5;
    uint32_t op6;
    uint32_t aux; //also ope
}; // __attribute__((aligned(32)));



struct ERItype {
    //basic information
    GEOM geometry;
    uint8_t  La;
    uint8_t  Lb;
    uint8_t  Lc;
    uint8_t  Ld;

    bool isGC;  //general contraction
    bool isLR;  //long range integrals
    bool isCDR; //CDR in K4

    ERItype & operator()(GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld, bool gc, bool lr, bool cdr) {
        geometry = geo;
        La = la;
        Lb = lb;
        Lc = lc;
        Ld = ld;
        isGC  = gc;
        isLR  = lr;
        isCDR = cdr;

        return *this;
    }

    //to enumerate them
    inline bool operator<(const ERItype & rhs) const {
        if (geometry!=rhs.geometry) return geometry<rhs.geometry;
        if (La      !=rhs.La      ) return La      <rhs.La;
        if (Lb      !=rhs.Lb      ) return Lb      <rhs.Lb;
        if (Lc      !=rhs.Lc      ) return Lc      <rhs.Lc;
        if (Ld      !=rhs.Ld      ) return Ld      <rhs.Ld;

        if (isGC  != rhs.isGC)  return isGC;
        if (isCDR != rhs.isCDR) return isCDR;
        if (isLR  != rhs.isLR)  return isLR;

        return false;
    }
};


class ERIroutine {

  private:
    //state
    bool IsSet;
    bool IsInitialized;

    //type of ERI batch
    uint8_t la;
    uint8_t lb;
    uint8_t lc;
    uint8_t ld;
    uint8_t Am;
    GEOM geometry;
    bool useCDR;
    bool useGC;

    //instructions/pointer to function
    uint32_t ninstrK4[32];
    uint32_t ninstr  [32];

    Op_K4 * nseqK4[32];
    Op_K4 * eseqK4;

    Op_MIRROR * nseq[32];
    Op_MIRROR * eseq;


    int Ntotal;   //number of instructions
    int NtotalK4;


    //copy the coefficients to a local, more compact representation
    double Ca[11][6];
    double Cb[11][6];
    double Cc[11][6];
    double Cd[11][6];


    //useful info
    uint32_t MaxMem;
    uint32_t K4Mem;
    uint32_t nKernels;

    uint8_t Lt;


    int memF0, memF0e, memF0f;
    int memK4J1e, memK4J1f;
    int memK3J2e, memK3J2f;
    int memK2J3e, memK2J3f;
    int memK1J4e, memK1J4f;

    uint16_t maxV, maxU, maxT, maxS;

    //for benchmarking
    //****************
    uint64_t NFLOPS;
    uint64_t NFLOPSK;
    uint64_t NK4;


    //FLOPs in each loop
    int nK4J0,  nK4J1,  nK3J1,  nK3J2,  nK2J2,  nK2J3,  nK1J3,  nK1J4,  nK0J4;
    int nK4J0e, nK4J1e, nK3J1e, nK3J2e, nK2J2e, nK2J3e, nK1J3e, nK1J4e, nK0J4e;
    int nK4J0f, nK4J1f, nK3J1f, nK3J2f, nK2J2f, nK2J3f, nK1J3f, nK1J4f, nK0J4f;


    //routines not accessed outside the class
    std::string IdString(bool fuse=false) const;

    void WriteQIC() const;

    void WriteK2C() const;

    void WriteGK4CUDA() const;

    void WriteK4CUDA() const;

    void WriteMCUDA() const;

  public:

    ERIroutine();
   ~ERIroutine();

    void Set(uint8_t La, uint8_t Lb, uint8_t Lc, uint8_t Ld, GEOM geom, bool cdr);
    void Generate (bool wQIC, bool wK2C, bool wK4CU, bool wMCU);
};

#endif
