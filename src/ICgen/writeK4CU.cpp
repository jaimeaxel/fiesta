/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#include "defs.hpp"
#include "ICgen.hpp"
#include "ERI.hpp"


//  WRITE CUDA CODE FOR THE K4 CONTRACTION ROUTINES
// =================================================

void ERIroutine::WriteK4CUDA() const {


    const bool writeCDR = false;


    string CURname = "K4gpu_" + IdString(false);
    string KNLname = "Kernel_K4_" + IdString(false);

    string EFSdir = K4CU_DIR;
    if (EFSdir=="") return; //skip

    string K4file = EFSdir + "/" + CURname + ".cu";

    ofstream file;
    file.open(K4file.c_str());
    file.precision(16);

    //file << "#include \"K4gpu.hpp\"" << endl;
    file << "#include \"defs.hpp\"" << endl;
    //file << "#include \"2eints/quimera.hpp\"" << endl;
    file << endl;

    //geometric vars
    bool SAB, SCD;

    if      (geometry==ABCD || geometry==ABAD) {
        SAB = true;
        SCD = true;
    }
    else if (geometry==AACD) {
        SAB = false;
        SCD = true;
    }
    else if (geometry==AACC) {
        SAB = false;
        SCD = false;
    }


    file << "__global__ void "<<CURname<<" (const double * vars, const double * d_F0,   double * uv_m_st, " << endl;
    file << "        uint8_t   KA, uint8_t   KB, uint8_t   KC, uint8_t   KD, "  << endl;
    file << "        uint8_t   JA, uint8_t   JB, uint8_t   JC, uint8_t   JD, "  << endl;
    file << "        uint16_t nJ1, uint16_t nJ2, uint16_t nJ3, uint16_t nJ4, "  << endl;
    file << "        const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd, "  << endl;
    file << "        const double * iKa, const double * iKb, const double * iKc, const double * iKd, "  << endl;
    file << "        const double * iKba, const double * iKdc, "  << endl;
    file << "        const double * Na, const double * Nb, const double * Nc, const double * Nd, "  << endl;
    file << "        uint32_t maxTiles, double ilogThresh) {" << endl;

    file << "    int i = threadIdx.x;" << endl;
    file << "    int k = blockIdx.y * blockDim.y + threadIdx.y;" << endl; // this is effectively the number of warp (32-tile)

    file << "    if (k>=maxTiles) return;" << endl;

    //include only the necessary ones
    file << "    const double im2[] = {";
    for (int l=0; l<Lt; ++l) file << "1./" << (2*l+1) << ". ,";
    file << "1./" << (2*Lt+1) << ".};" << endl;


    if      ( SAB &&  SCD) {
        file << "    double ABz = vars[k*6*NTX +       i];" << endl;
        file << "    double CDy = vars[k*6*NTX +   NTX+i];" << endl;
        file << "    double CDz = vars[k*6*NTX + 2*NTX+i];" << endl;
        file << "    double ACx = vars[k*6*NTX + 3*NTX+i];" << endl;
        file << "    double ACy = vars[k*6*NTX + 4*NTX+i];" << endl;
        file << "    double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;

        file << "    double CD2  =        CDy*CDy + CDz*CDz;" << endl;
        file << "    double ACCD =        ACy*CDy + ACz*CDz; ACCD += ACCD;" << endl;
        file << "    double ABCD =                  ABz*CDz; ABCD += ABCD;" << endl;
        file << "    double X2   = ACx*ACx;" << endl;
        file << "    double AC2  =      X2+ACy*ACy+ACz*ACz;" << endl;
        file << "    double ACAB =                 ABz*ACz; ACAB += ACAB;" << endl;
        file << "    double AB2  =                 ABz*ABz;" << endl;

        file << "    const double ikmab = AB2 * ilogThresh;" << endl;
        file << "    const double ikmcd = CD2 * ilogThresh;" << endl;
    }
    else if (!SAB &&  SCD) {
        file << "    double CDz = vars[k*6*NTX + 2*NTX+i];" << endl;
        file << "    double ACy = vars[k*6*NTX + 4*NTX+i];" << endl;
        file << "    double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;

        file << "    double CD2  =                  CDz*CDz;" << endl;
        file << "    double ACCD =                  ACz*CDz; ACCD += ACCD;" << endl;
        file << "    double Y2   =           ACy*ACy;" << endl;
        file << "    double AC2  =        Y2+ACz*ACz;" << endl;

        file << "    const double ikmcd = CD2 * ilogThresh;" << endl;
    }
    else if (!SAB && !SCD) {
        file << "    double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;

        file << "    double AC2  =      ACz*ACz;" << endl;
    }


    file << "    const double * F0 = d_F0 + 2*k*KA*KB*KC*KD*NTX;" << endl;


        if (writeCDR)
    file << "    double K1J4e["<<memK1J4e<<"];" << endl;
    file << "    double K1J4f["<<memK1J4f<<"];" << endl;
        if (writeCDR)
    file << "    double K2J3e["<<memK2J3e<<"];" << endl;
    file << "    double K2J3f["<<memK2J3f<<"];" << endl;
        if (writeCDR)
    file << "    double K3J2e["<<memK3J2e<<"];" << endl;
    file << "    double K3J2f["<<memK3J2f<<"];" << endl;
        if (writeCDR)
    file << "    double K4J1e["<<memK4J1e<<"];" << endl;
    file << "    double K4J1f["<<memK4J1f<<"];" << endl;


        if (writeCDR)
    file << "    memset(K1J4e, 0, nJ4*"<<memK1J4e*sizeof(double)<<");" << endl;
    file << "    memset(K1J4f, 0, nJ4*"<<memK1J4f*sizeof(double)<<");" << endl;

    file << "    for (uint8_t d=0; d<KD; ++d) {" << endl; {

        if (writeCDR)
    file << "        memset(K2J3e, 0, nJ3*"<<memK2J3e*sizeof(double)<<");" << endl;
    file << "        memset(K2J3f, 0, nJ3*"<<memK2J3f*sizeof(double)<<");" << endl;
    if      (SCD)
    file << "        double ikmc = ikmcd-iKd[d];" << endl;

    file << "        const double * F1 = F0 + d*KC*KB*KA * 2*NTX;" << endl;

    file << "        for (uint8_t c=0; c<KC; ++c) {" << endl; {
    if      (SCD)
    file << "            if (iKc[c] < ikmc) break;" << endl;

    file << "            const double ikcd = iKdc[d*KC+c];" << endl;
    if      (SCD)
    file << "            const double rcd = Kd[d] * ikcd;" << endl;

        if (writeCDR)
    file << "            memset(K3J2e, 0, nJ2*"<<memK3J2e*sizeof(double)<<");" << endl;
    file << "            memset(K3J2f, 0, nJ2*"<<memK3J2f*sizeof(double)<<");" << endl;


    if      ( SAB &&  SCD) {
    file << "            double AQy  = ACy + CDy * rcd;" << endl;
    file << "            double AQz  = ACz + CDz * rcd;" << endl;

    file << "            double X2Y2 = X2   + AQy*AQy;" << endl;
    file << "            double AQ2  = X2Y2 + AQz*AQz;" << endl;

    file << "            double AQAB = (AQz*ABz); AQAB += AQAB;" << endl;
    }
    else if (!SAB &&  SCD) {
    file << "            double AQz  = ACz + CDz * rcd;" << endl;
    file << "            double AQ2  = Y2  + AQz*AQz;" << endl;
    }

    file << "            const double * F2 = F1 + c*KB*KA * 2*NTX;" << endl;

    file << "            for (uint8_t b=0; b<KB; ++b) {" << endl; {
        if (writeCDR)
    file << "                memset(K4J1e, 0, nJ1*"<<memK4J1e*sizeof(double)<<");" << endl;
    file << "                memset(K4J1f, 0, nJ1*"<<memK4J1f*sizeof(double)<<");" << endl;
    if      (SAB)
    file << "                double ikma = ikmab-iKb[b];" << endl;

    file << "                const double * F3 = F2 + b*KA * 2*NTX;" << endl;

    file << "                for (uint8_t a=0; a<KA; ++a) {" << endl; {
    if      (SAB)
    file << "                    if (iKa[a] < ikma) break;" << endl;

    file << "                    const double ikab = iKba[b*KA+a];" << endl;
    file << "                    const double iKpq = 0.5*(ikab + ikcd);" << endl;

    if (SAB && SCD) {
    file << "                    const double rab = Kb[b] * ikab;" << endl;
    file << "                    double PQz = AQz - ABz*rab;" << endl;
    file << "                    double PQ2  = X2Y2 + PQz*PQz;" << endl;
    }


/*
    file << "                    double * F0e = ( (double*)buffers[k].F0e);" << endl;
    file << "                    double * F0f = ( (double*)buffers[k].F0f);" << endl;

    //copy the gamma of higher L and the contracted exponential
    file << "                    F0e[i] = F0[    i];" << endl;
    file << "                    F0f[i] = F0[NTX+i];" << endl;

    //AERR K4 (uncontracted)
    {
    file << "                    {" << endl;

                                 for (Op_K4 * s2=nseqK4[AERR4]+1; s2<nseqK4[AERR4+1]; ++s2)
    file << "                        F0e["<<s2->dest<<"*NTX+i] = F0e["<<s2->op1<<"*NTX+i] * iKpq;" << endl;
    file << "                    }" << endl;
    }

    //CDR K4 (uncontracted downward recursion)
    {
    file << "                    {" << endl;
                                    for (Op_K4 * s2=nseqK4[CDR4]+1; s2<nseqK4[CDR4+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                        F0f["<<s2->dest<<"*NTX+i] = (R2*F0f["<<s2->op1<<"*NTX+i] + F0e["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                        F0f["<<s2->dest<<"*NTX+i] = (AQ2*F0f["<<s2->op1<<"*NTX+i] + F0e["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                        F0f["<<s2->dest<<"*NTX+i] = (X2*F0f["<<s2->op1<<"*NTX+i] + F0e["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                                    }
    file << "                    }" << endl;
    }
*/

    file << "                    double J0e["<<int(Lt+1)<<"];" << endl;
    file << "                    double J0f["<<int(Lt+1)<<"];" << endl;

    file << "                    const double * F4 = F3 + a * 2*NTX;" << endl;

    //copy the gamma of higher L and the contracted exponential
    file << "                    J0e[0] = F4[    i];" << endl;
    file << "                    J0f[0] = F4[NTX+i];" << endl;

    //AERR K4 (uncontracted)
    {

                                 for (Op_K4 * s2=nseqK4[AERR4]+1; s2<nseqK4[AERR4+1]; ++s2)
    file << "                    J0e["<<s2->dest<<"] = J0e["<<s2->op1<<"] * iKpq;" << endl;
    }

    //CDR K4 (uncontracted downward recursion)
    {
                                    for (Op_K4 * s2=nseqK4[CDR4]+1; s2<nseqK4[CDR4+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                    J0f["<<s2->dest<<"] = (PQ2*J0f["<<s2->op1<<"] + J0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                    J0f["<<s2->dest<<"] = (AQ2*J0f["<<s2->op1<<"] + J0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                    J0f["<<s2->dest<<"] = (AC2*J0f["<<s2->op1<<"] + J0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl;
                                    }
    }


    //K4 E + F
    {
    file << "                    {" << endl;
    //file << "                        const double * J0e = F0e;" << endl;
    //file << "                        const double * J0f = F0f;" << endl;

    //file << "                        double       * J1e = (double*)buffers[k].K4J1e;" << endl;
    //file << "                        double       * J1f = (double*)buffers[k].K4J1f;" << endl;

        if (writeCDR)
    file << "                        double       * J1e = K4J1e;" << endl;
    file << "                        double       * J1f = K4J1f;" << endl;


    file << "                        double Vv[maxJ]["<<maxV+1<<"];" << endl;

    file << "                        for (uint8_t jj=0; jj<JA; ++jj) {" << endl;
    file << "                            Vv[jj][0] = Na[a*JA+jj];" << endl;

                                for (int v=1; v<=maxV; ++v)
    file << "                            Vv[jj]["<<v<<"] = Vv[jj]["<<v-1<<"] * (0.5*ikab);" << endl;
    file << "                        }" << endl;


                                   #ifdef __QR_GC__
    file << "                        for (int jj=0; jj<min(JA, a+1); ++jj) {" << endl;
                                   #else
    file << "                        for (uint8_t jj=0; jj<JA; ++jj) {" << endl;
                                   #endif
        if (writeCDR)
                                     for (Op_K4 * s2=nseqK4[K4E]; s2<nseqK4[K4E+1]; ++s2)
    file << "                            J1e["<<s2->dest<<"] += J0e["<<s2->op1<<"] * Vv[jj]["<<s2->aux<<"];" << endl;

                                     for (Op_K4 * s2=nseqK4[K4F]; s2<nseqK4[K4F+1]; ++s2)
    file << "                            J1f["<<s2->dest<<"] += J0f["<<s2->op1<<"] * Vv[jj]["<<s2->aux<<"];" << endl;

        if (writeCDR)
    file << "                            J1e += "<<memK4J1e<<";" << endl;
    file << "                            J1f += "<<memK4J1f<<";" << endl;


/*
                                     for (Op_K4 * s2=nseqK4[K4E]; s2<nseqK4[K4E+1]; ++s2)
    //file << "                            J1e["<<s2->dest<<"*NTX+i] += J0e["<<s2->op1<<"*NTX+i] * Vv[jj]["<<s2->aux<<"];" << endl;
    file << "                            J1e["<<s2->dest<<"*NTX+i] += J0e["<<s2->op1<<"] * Vv[jj]["<<s2->aux<<"];" << endl;

                                     for (Op_K4 * s2=nseqK4[K4F]; s2<nseqK4[K4F+1]; ++s2)
    //file << "                            J1f["<<s2->dest<<"*NTX+i] += J0f["<<s2->op1<<"*NTX+i] * Vv[jj]["<<s2->aux<<"];" << endl;
    file << "                            J1f["<<s2->dest<<"*NTX+i] += J0f["<<s2->op1<<"] * Vv[jj]["<<s2->aux<<"];" << endl;

    file << "                            J1e += "<<memK4J1e<<"*NTX;" << endl;
    file << "                            J1f += "<<memK4J1f<<"*NTX;" << endl;
*/

    file << "                        }" << endl;

    file << "                    }" << endl;
    }

    //file << "                    F0 += 2*NTX;" << endl;
    file << "                }" << endl;}

    //AERR K3
    if (writeCDR) {
    file << "                {" << endl;
    file << "                    double * Je = (double*)buffers[k].K4J1e;" << endl;

    file << "                    for (uint8_t nj1=0; nj1<J1; ++nj1) {" << endl;

                                  for (Op_K4 * s2=nseqK4[AERR3]; s2<nseqK4[AERR3+1]; ++s2) {
    file << "                        Je["<<s2->dest<<"*NTX+i] = Je["<<s2->op1<<"*NTX+i] + Je["<<s2->op2<<"*NTX+i] * (0.5*ikcd);" << endl;
                                  }
    file << "                        Je += "<<memK4J1e<<"*NTX;" << endl;

    file << "                    }" << endl;
    file << "                }" << endl;
    }

    //CDR3
    if (writeCDR) {
    file << "                {" << endl;
    if (SAB && SCD) {
    file << "                    double AQABb = AQAB * (2*Kb[b]);" << endl;
    file << "                    double AB2b2 = AB2  * (4*Kb[b]*Kb[b]);" << endl;
    }

    file << "                    double * Jf = (double*)buffers[k].K4J1f;" << endl;
    file << "                    double * Je = (double*)buffers[k].K4J1e;" << endl;

    file << "                    for (uint8_t nj1=0; nj1<J1; ++nj1) {" << endl;

                                 for (Op_K4 * s2=nseqK4[CDR3]; s2<nseqK4[CDR3+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                        Jf["<<s2->dest<<"*NTX+i] = (AQ2*Jf["<<s2->op1<<"*NTX+i] - AQABb*Jf["<<s2->op2<<"*NTX+i] + AB2b2*Jf["<<s2->op3<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                        Jf["<<s2->dest<<"*NTX+i] = (AQ2*Jf["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                        Jf["<<s2->dest<<"*NTX+i] = (AC2*Jf["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                            }

    file << "                        Jf += "<<memK4J1f<<"*NTX;" << endl;
    file << "                        Je += "<<memK4J1e<<"*NTX;" << endl;
    file << "                    }" << endl;
    file << "                }" << endl;
    }

    //K3 E + F
    /*
    {
    file << "                {" << endl;
    file << "                    const double * J1e = (double*)buffers[k].K4J1e;" << endl;
    file << "                    const double * J1f = (double*)buffers[k].K4J1f;" << endl;

    file << "                    double * J2e = (double*)buffers[k].K3J2e;" << endl;
    file << "                    double * J2f = (double*)buffers[k].K3J2f;" << endl;

    file << "                    __shared__ double Uu[maxJ]["<<maxU+1<<"];" << endl;


    file << "                    if (i<JB) {" << endl;
    file << "                        Uu[i][0] = Nb[b][i];" << endl;

    file << "                        for (int u=1; u<="<<maxU<<"; ++u)" << endl;
    file << "                            Uu[i][u] = Uu[i][u-1] * (2*cuKb[b]);" << endl;
    file << "                    }" << endl;

    file << "                    __syncthreads();" << endl;


    file << "                    for (int nj1=0; nj1<nJ1; ++nj1) {" << endl;
                            #ifdef __QR_GC__
    file << "                        for (int jj=0; jj<min(JB, b+1); ++jj) {" << endl;
                            #else
    file << "                        for (int jj=0; jj<JB; ++jj) {" << endl;
                            #endif

                                    for (Op_K4 * s2=nseqK4[K3E]; s2<nseqK4[K3E+1]; ++s2)
    file << "                            J2e["<<s2->dest<<"*NTX+i] += J1e["<<s2->op1<<"*NTX+i] * Uu[jj]["<<s2->aux<<"];" << endl;
                                    for (Op_K4 * s2=nseqK4[K3F]; s2<nseqK4[K3F+1]; ++s2)
    file << "                            J2f["<<s2->dest<<"*NTX+i] += J1f["<<s2->op1<<"*NTX+i] * Uu[jj]["<<s2->aux<<"];" << endl;

    file << "                            J2e += "<<memK3J2e<<"*NTX;" << endl;
    file << "                            J2f += "<<memK3J2f<<"*NTX;" << endl;
    file << "                        }" << endl;

                  #ifdef __QR_GC__
    file << "                        J2e += max(JB-(b+1),0)*"<<memK3J2e<<"*NTX;" << endl;
    file << "                        J2f += max(JB-(b+1),0)*"<<memK3J2f<<"*NTX;" << endl;
                  #endif
    file << "                        J1e += "<<memK4J1e<<"*NTX;" << endl;
    file << "                        J1f += "<<memK4J1f<<"*NTX;" << endl;

    file << "                    }" << endl;
    file << "                }" << endl;
    }
    */
    {
    file << "                {" << endl;
        if (writeCDR)
    file << "                    const double * J1e = K4J1e;" << endl;
    file << "                    const double * J1f = K4J1f;" << endl;
        if (writeCDR)
    file << "                    double * J2e = K3J2e;" << endl;
    file << "                    double * J2f = K3J2f;" << endl;

    file << "                    double Uu[maxJ]["<<maxU+1<<"];" << endl;

    file << "                    for (uint8_t jj=0; jj<JB; ++jj) {" << endl;
    file << "                        Uu[jj][0] = Nb[b*JB+jj];" << endl;

                            for (int u=1; u<=maxU; ++u)
    file << "                        Uu[jj]["<<u<<"] = Uu[jj]["<<u-1<<"] * (2*Kb[b]);" << endl;
    file << "                    }" << endl;


    file << "                    for (uint16_t nj1=0; nj1<nJ1; ++nj1) {" << endl;
                            #ifdef __QR_GC__
    file << "                        for (int jj=0; jj<min(JB, b+1); ++jj) {" << endl;
                            #else
    file << "                        for (uint8_t jj=0; jj<JB; ++jj) {" << endl;
                            #endif
        if (writeCDR)
                                    for (Op_K4 * s2=nseqK4[K3E]; s2<nseqK4[K3E+1]; ++s2)
    file << "                            J2e["<<s2->dest<<"] += J1e["<<s2->op1<<"] * Uu[jj]["<<s2->aux<<"];" << endl;
                                    for (Op_K4 * s2=nseqK4[K3F]; s2<nseqK4[K3F+1]; ++s2)
    file << "                            J2f["<<s2->dest<<"] += J1f["<<s2->op1<<"] * Uu[jj]["<<s2->aux<<"];" << endl;
        if (writeCDR)
    file << "                            J2e += "<<memK3J2e<<";" << endl;
    file << "                            J2f += "<<memK3J2f<<";" << endl;
    file << "                        }" << endl;

                  #ifdef __QR_GC__
        if (writeCDR)
    file << "                        if (c+1<JC) J2e += (JB-(b+1))*"<<memK3J2e<<";" << endl;
    file << "                        if (c+1<JC) J2f += (JB-(b+1))*"<<memK3J2f<<";" << endl;
                  #endif
        if (writeCDR)
    file << "                        J1e += "<<memK4J1e<<";" << endl;
    file << "                        J1f += "<<memK4J1f<<";" << endl;

    file << "                    }" << endl;
    file << "                }" << endl;
    }

    file << "            }" << endl; }


    //AERR K2
    if (writeCDR) {
    file << "            {" << endl;
    file << "                double * J2 = (double*)buffers[k].K3J2e;" << endl;

                            //K2J2
    file << "                for (uint16_t nj2=0; nj2<J2; ++nj2) {" << endl;


                             for (Op_K4 * s2=nseqK4[AERR2]; s2<nseqK4[AERR2+1]; ++s2) {
    file << "                    J2["<<s2->dest<<"*NTX+i] = J2["<<s2->op1<<"*NTX+i] + J2["<<s2->op2<<"*NTX+i] * (0.5*ikcd);" << endl;
                             }
    file << "                    J2 += "<<memK3J2e<<"*NTX;" << endl;

    file << "                }" << endl;

    file << "            }" << endl;
    }

    //CDR K2
    if (writeCDR) {
    file << "            {" << endl;
    file << "                double * J2 = (double*)buffers[k].K3J2f;" << endl;
    file << "                double * Je = (double*)buffers[k].K3J2e;" << endl;

    file << "                for (uint16_t nj2=0; nj2<J2; ++nj2) {" << endl;
                            for (Op_K4 * s2=nseqK4[CDR2]; s2<nseqK4[CDR2+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                    J2["<<s2->dest<<"*NTX+i] = (AQ2*J2["<<s2->op1<<"*NTX+i] - AQAB*J2["<<s2->op2<<"*NTX+i] + AB2*J2["<<s2->op3<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                    J2["<<s2->dest<<"*NTX+i] = (AQ2*J2["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                    J2["<<s2->dest<<"*NTX+i] = (AC2*J2["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                            }

    file << "                    J2 += "<<memK3J2f<<"*NTX;" << endl;
    file << "                    Je += "<<memK3J2e<<"*NTX;" << endl;
    file << "                }" << endl;
    file << "            }" << endl;
    }

    //K2 E + F
    /*
    {
    file << "            {" << endl;
    file << "                const double * J2e = (double*)buffers[k].K3J2e;" << endl;
    file << "                const double * J2f = (double*)buffers[k].K3J2f;" << endl;

    file << "                double * J3e = (double*)buffers[k].K2J3e;" << endl;
    file << "                double * J3f = (double*)buffers[k].K2J3f;" << endl;

    file << "                double Tt[maxJ]["<<maxT+1<<"];" << endl;

    file << "                for (int jj=0; jj<JC; ++jj) {" << endl;
    file << "                    Tt[jj][0] = Nc[c][jj];" << endl;

    file << "                    for (int t=1; t<="<<maxT<<"; ++t)" << endl;
    file << "                        Tt[jj][t] = Tt[jj][t-1] * (0.5*ikcd);" << endl;
    file << "                }" << endl;

                            //K2J3e+f contraction
    file << "                for (int nj2=0; nj2<nJ2; ++nj2) {" << endl;
                  #ifdef __QR_GC__
    file << "                    for (int jj=0; jj<min(JC, c+1); ++jj) {" << endl;
                  #else
    file << "                    for (int jj=0; jj<JC; ++jj) {" << endl;
                  #endif

                                for (Op_K4 * s2=nseqK4[K2E]; s2<nseqK4[K2E+1]; ++s2)
    file << "                        J3e["<<s2->dest<<"*NTX+i] += J2e["<<s2->op1<<"*NTX+i] * Tt[jj]["<<s2->aux<<"];" << endl;
                                for (Op_K4 * s2=nseqK4[K2F]; s2<nseqK4[K2F+1]; ++s2)
    file << "                        J3f["<<s2->dest<<"*NTX+i] += J2f["<<s2->op1<<"*NTX+i] * Tt[jj]["<<s2->aux<<"];" << endl;

    file << "                        J3e += "<<memK2J3e<<"*NTX;" << endl;
    file << "                        J3f += "<<memK2J3f<<"*NTX;" << endl;
    file << "                    }" << endl;

                  #ifdef __QR_GC__
    file << "                    J3e += max(JC-(c+1),0)*"<<memK2J3e<<"*NTX;" << endl;
    file << "                    J3f += max(JC-(c+1),0)*"<<memK2J3f<<"*NTX;" << endl;
                  #endif

    file << "                    J2e += "<<memK3J2e<<"*NTX;" << endl;
    file << "                    J2f += "<<memK3J2f<<"*NTX;" << endl;
    file << "                }" << endl;
    file << "            }" << endl;
    }
*/
    {
    file << "            {" << endl;
        if (writeCDR)
    file << "                const double * J2e = K3J2e;" << endl;
    file << "                const double * J2f = K3J2f;" << endl;
        if (writeCDR)
    file << "                double * J3e = K2J3e;" << endl;
    file << "                double * J3f = K2J3f;" << endl;

    file << "                double Tt[maxJ]["<<maxT+1<<"];" << endl;

    file << "                for (uint8_t jj=0; jj<JC; ++jj) {" << endl;
    file << "                    Tt[jj][0] = Nc[c*JC+jj];" << endl;

                        for (int t=1; t<=maxT; ++t)
    file << "                        Tt[jj]["<<t<<"] = Tt[jj]["<<t-1<<"] * (0.5*ikcd);" << endl;
    file << "                }" << endl;

                            //K2J3e+f contraction
    file << "                for (uint16_t nj2=0; nj2<nJ2; ++nj2) {" << endl;
                  #ifdef __QR_GC__
    file << "                    for (uint8_t jj=0; jj<min(JC, c+1); ++jj) {" << endl;
                  #else
    file << "                    for (uint8_t jj=0; jj<JC; ++jj) {" << endl;
                  #endif
        if (writeCDR)
                                for (Op_K4 * s2=nseqK4[K2E]; s2<nseqK4[K2E+1]; ++s2)
    file << "                        J3e["<<s2->dest<<"] += J2e["<<s2->op1<<"] * Tt[jj]["<<s2->aux<<"];" << endl;
                                for (Op_K4 * s2=nseqK4[K2F]; s2<nseqK4[K2F+1]; ++s2)
    file << "                        J3f["<<s2->dest<<"] += J2f["<<s2->op1<<"] * Tt[jj]["<<s2->aux<<"];" << endl;

        if (writeCDR)
    file << "                        J3e += "<<memK2J3e<<";" << endl;
    file << "                        J3f += "<<memK2J3f<<";" << endl;
    file << "                    }" << endl;

                  #ifdef __QR_GC__
        if (writeCDR)
    file << "                    if (c+1<JC) J3e += (JC-(c+1))*"<<memK2J3e<<";" << endl;
    file << "                    if (c+1<JC) J3f += (JC-(c+1))*"<<memK2J3f<<";" << endl;
                  #endif
        if (writeCDR)
    file << "                    J2e += "<<memK3J2e<<";" << endl;
    file << "                    J2f += "<<memK3J2f<<";" << endl;
    file << "                }" << endl;
    file << "            }" << endl;
    }

    file << "        }" << endl;}

    //AERR K1
    if (writeCDR) {
    file << "        {" << endl;
    file << "            double * J3 = (double*)buffers[k].K2J3e;" << endl;

    file << "            for (int nj3=0; nj3<nJ3; ++nj3) {" << endl;
                for (Op_K4 * s2=nseqK4[AERR1]; s2<nseqK4[AERR1+1]; ++s2) {
    file << "                J3["<<s2->dest<<"*NTX+i] = J3["<<s2->op1<<"*NTX+i] + J3["<<s2->op2<<"*NTX+i];" << endl;
                }
    file << "                J3 += "<<memK2J3e<<"*NTX;" << endl;

    file << "            }" << endl;
    file << "        }" << endl;
    }

    //CDR K1
    if (writeCDR) {
        if      ( SAB && SCD) {
    file << "        double CD2d2 = CD2  * (4*cuKd[d]*cuKd[d]);" << endl;
    file << "        double ACCDd = ACCD * (2*cuKd[d]);" << endl;
    file << "        double ABCDd = ABCD * (2*cuKd[d]);" << endl;
        }
        else if (!SAB && SCD) {
    file << "        double CD2d2 = CD2  * (4*cuKd[d]*cuKd[d]);" << endl;
    file << "        double ACCDd = ACCD * (2*cuKd[d]);" << endl;
        }

    file << "        {" << endl;
    file << "            double * J3 = (double*)buffers[k].K2J3f;" << endl;
    file << "            double * Je = (double*)buffers[k].K2J3e;" << endl;

    file << "            for (int nj3=0; nj3<nJ3; ++nj3) {" << endl;

                for (Op_K4 * s2=nseqK4[CDR1]; s2<nseqK4[CDR1+1]; ++s2) {
                    if      ( SAB &&  SCD)
    file << "                J3["<<s2->dest<<"*NTX+i] = (AC2*J3["<<s2->op1<<"*NTX+i] - ACAB*J3["<<s2->op2<<"*NTX+i] + ACCDd*J3["<<s2->op3<<"*NTX+i] + AB2*J3["<<s2->op4<<"*NTX+i] - ABCDd*J3["<<s2->op5<<"*NTX+i] + CD2d2*J3["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB &&  SCD)
    file << "                J3["<<s2->dest<<"*NTX+i] = (AC2*J3["<<s2->op1<<"*NTX+i] + ACCDd*J3["<<s2->op3<<"*NTX+i] + CD2d2*J3["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB && !SCD)
    file << "                J3["<<s2->dest<<"*NTX+i] = (AC2*J3["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                }

    file << "                J3 += "<<memK2J3f<<"*NTX;" << endl;
    file << "                Je += "<<memK2J3e<<"*NTX;" << endl;
    file << "            }" << endl;
    file << "        }" << endl;
    }

    //K1 E + F
    /*
    {
    file << "        {" << endl;
    file << "            const double * J3e = (double*)buffers[k].K2J3e;" << endl;
    file << "            const double * J3f = (double*)buffers[k].K2J3f;" << endl;

    file << "            double * J4e = (double*)buffers[k].K1J4e;" << endl;
    file << "            double * J4f = (double*)buffers[k].K1J4f;" << endl;

    file << "            double Ss[maxJ]["<<maxS+1<<"];" << endl;

    file << "            for (int jj=0; jj<JD; ++jj) {" << endl;
    file << "                Ss[jj][0] = Nd[d][jj];" << endl;

    file << "                for (int s=1; s<="<<maxS<<"; ++s)" << endl;
    file << "                    Ss[jj][s] = Ss[jj][s-1] * (2*cuKd[d]);" << endl;
    file << "            }" << endl;


    file << "            for (int nj3=0; nj3<nJ3; ++nj3) {" << endl;

              #ifdef __QR_GC__
    file << "                for (int jj=0; jj<min(JD, d+1); ++jj) {" << endl;
              #else
    file << "                for (int jj=0; jj<JD; ++jj) {" << endl;
              #endif

                            for (Op_K4 * s2=nseqK4[K1E]; s2<nseqK4[K1E+1]; ++s2)
    file << "                    J4e["<<s2->dest<<"*NTX+i] += J3e["<<s2->op1<<"*NTX+i] * Ss[jj]["<<s2->aux<<"];" << endl;
                            for (Op_K4 * s2=nseqK4[K1F]; s2<nseqK4[K1F+1]; ++s2)
    file << "                    J4f["<<s2->dest<<"*NTX+i] += J3f["<<s2->op1<<"*NTX+i] * Ss[jj]["<<s2->aux<<"];" << endl;

    file << "                    J4e += "<<memK2J3e<<"*NTX;" << endl;
    file << "                    J4f += "<<memK2J3f<<"*NTX;" << endl;
    file << "                }" << endl;

                  #ifdef __QR_GC__
    file << "                J4e += max(JD-(d+1),0)*"<<memK1J4e<<"*NTX;" << endl;
    file << "                J4f += max(JD-(d+1),0)*"<<memK1J4f<<"*NTX;" << endl;
                  #endif

    file << "                J3e += "<<memK2J3e<<"*NTX;" << endl;
    file << "                J3f += "<<memK2J3f<<"*NTX;" << endl;
    file << "            }" << endl;
    file << "        }" << endl;
    }
*/
    {
    file << "        {" << endl;
        if (writeCDR)
    file << "            const double * J3e = K2J3e;" << endl;
    file << "            const double * J3f = K2J3f;" << endl;

        if (writeCDR)
    file << "            double * J4e = K1J4e;" << endl;
    file << "            double * J4f = K1J4f;" << endl;

    file << "            double Ss[maxJ]["<<maxS+1<<"];" << endl;

    file << "            for (uint8_t jj=0; jj<JD; ++jj) {" << endl;
    file << "                Ss[jj][0] = Nd[d*JD+jj];" << endl;

                    for (int s=1; s<=maxS; ++s)
    file << "                Ss[jj]["<<s<<"] = Ss[jj]["<<s-1<<"] * (2*Kd[d]);" << endl;
    file << "            }" << endl;


    file << "            for (uint16_t nj3=0; nj3<nJ3; ++nj3) {" << endl;

              #ifdef __QR_GC__
    file << "                for (uint8_t jj=0; jj<min(JD, d+1); ++jj) {" << endl;
              #else
    file << "                for (uint8_t jj=0; jj<JD; ++jj) {" << endl;
              #endif
        if (writeCDR)
                            for (Op_K4 * s2=nseqK4[K1E]; s2<nseqK4[K1E+1]; ++s2)
    file << "                    J4e["<<s2->dest<<"] += J3e["<<s2->op1<<"] * Ss[jj]["<<s2->aux<<"];" << endl;
                            for (Op_K4 * s2=nseqK4[K1F]; s2<nseqK4[K1F+1]; ++s2)
    file << "                    J4f["<<s2->dest<<"] += J3f["<<s2->op1<<"] * Ss[jj]["<<s2->aux<<"];" << endl;
        if (writeCDR)
    file << "                    J4e += "<<memK1J4e<<";" << endl;
    file << "                    J4f += "<<memK1J4f<<";" << endl;
    file << "                }" << endl;

                  #ifdef __QR_GC__
        if (writeCDR)
    file << "                if (d+1<JD) J4e += (JD-(d+1))*"<<memK1J4e<<";" << endl;
    file << "                if (d+1<JD) J4f += (JD-(d+1))*"<<memK1J4f<<";" << endl;
                  #endif

        if (writeCDR)
    file << "                J3e += "<<memK2J3e<<";" << endl;
    file << "                J3f += "<<memK2J3f<<";" << endl;
    file << "            }" << endl;
    file << "        }" << endl;
    }


    file << "    }" << endl; }

    // THIS SHOULD BE PART OF THE TRANSFORMATIONS; SINCE THE LAST RRS
    // INCREMENT THE NUMBER OF KERNELS FROM O(L^5) TO O(L^4)

    //AERR K0
    if (writeCDR) {
    file << "    {" << endl;
    file << "        double * J4 = (double*)buffers[k].K1J4e;" << endl;

    file << "        for (int nj4=0; nj4<nJ4; ++nj4) {" << endl;

                for (Op_K4 * s2=nseqK4[AERR0]; s2<nseqK4[AERR0+1]; ++s2) {
    file << "            J4["<<s2->dest<<"*NTX+i] = J4["<<s2->op1<<"*NTX+i] + J4["<<s2->op2<<"*NTX+i];" << endl;
                }

    file << "            J4 += "<<memK1J4e<<"*NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }

    //CDR K0
    if (writeCDR) {
    file << "    {" << endl;
    file << "        double * J4 = (double*)buffers[k].K1J4f;" << endl;
    file << "        double * Je = (double*)buffers[k].K1J4e;" << endl;

    file << "        for (int nj4=0; nj4<nJ4; ++nj4) {" << endl;

                for (Op_K4 * s2=nseqK4[CDR0]; s2<nseqK4[CDR0+1]; ++s2) {
                    if      ( SAB &&  SCD)
    file << "            J4["<<s2->dest<<"*NTX+i] = (AC2*J4["<<s2->op1<<"*NTX+i] - ACAB*J4["<<s2->op2<<"*NTX+i] + ACCD*J4["<<s2->op3<<"*NTX+i] + AB2*J4["<<s2->op4<<"*NTX+i] - ABCD*J4["<<s2->op5<<"*NTX+i] + CD2*J4["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB &&  SCD)
    file << "            J4["<<s2->dest<<"*NTX+i] = (AC2*J4["<<s2->op1<<"*NTX+i] + ACCD*J4["<<s2->op3<<"*NTX+i] + CD2*J4["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB && !SCD)
    file << "            J4["<<s2->dest<<"*NTX+i] = (AC2*J4["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                }

    file << "            J4 += "<<memK1J4f<<"*NTX;" << endl;
    file << "            Je += "<<memK1J4e<<"*NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }

    //copy to the kernel buffer
    /*
    {
    file << "    {" << endl;
    file << "        const double * J4 = (double*)buffers[k].K1J4f;" << endl;
    file << "        double * I =  &uv_m_st[k*nJ4*"<<nKernels<< "*NTX];" << endl;

    file << "        for (int nj4=0; nj4<nJ4; ++nj4) {" << endl;
            const Op_MIRROR * s2 = eseq;

            for (int p=0; p<nKernels; ++p,++s2) {
                if (s2->aux == 0) //dest[i] = op1[i];
    file << "            I["<<s2->dest<<"*NTX+i] = J4["<<s2->op1<<"*NTX+i];" << endl;
                else //             dest[i] = 0;     //each thread zeroes one value
    file << "            I["<<s2->dest<<"*NTX+i] = 0;" << endl;
            }

    file << "            J4 += "<<memK1J4f<<"*NTX;" << endl;
    file << "            I  += "<<nKernels<<" * NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }
    */
    {
    file << "    {" << endl;
    file << "        const double * J4 = K1J4f;" << endl;
    file << "        double * I =  &uv_m_st[k*nJ4*"<<nKernels<< "*NTX];" << endl;

    file << "        for (uint16_t nj4=0; nj4<nJ4; ++nj4) {" << endl;
            const Op_MIRROR * s2 = eseq;

            for (int p=0; p<nKernels; ++p,++s2) {
                if (s2->aux == 0) //dest[i] = op1[i];
    file << "            I["<<s2->dest<<"*NTX+i] = J4["<<s2->op1<<"];" << endl;
                else //             dest[i] = 0;     //each thread zeroes one value
    file << "            I["<<s2->dest<<"*NTX+i] = 0;" << endl;
            }

    file << "            J4 += "<<memK1J4f<<";" << endl;
    file << "            I  += "<<nKernels<<" * NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }

/*
    //free the local memory
    file << "    free(K1J4e);" << endl;
    file << "    free(K1J4f);" << endl;

    file << "    free(K2J3e);" << endl;
    file << "    free(K2J3f);" << endl;

    file << "    free(K3J2e);" << endl;
    file << "    free(K3J2f);" << endl;

    file << "    free(K4J1e);" << endl;
    file << "    free(K4J1f);" << endl;
*/

    file << "}" << endl;
    file << endl;

    /*
    file << "#include \"basis/SPprototype.hpp\"" << endl;
    file << endl;

    // wrapper for __constant__ memory
    // initialization plus CUDA kernel call
    // ====================================
    file << "void "<<CURname<<" ( const ShellPairPrototype * ABp, const ShellPairPrototype * CDp, uint32_t yNtiles," << endl;
    file << "                     const double * vars, double * F0,   double * uv_m_st,  cudaStream_t * stream) {" << endl;


    file << "    uint8_t   KA,KB,KC,KD;"  << endl;
    file << "    uint8_t   JA,JB,JC,JD;"  << endl;
    file << "    uint16_t  nJ1,nJ2,nJ3,nJ4;"  << endl;

    file << "    KA = ABp->Ka;" << endl;
    file << "    KB = ABp->Kb;" << endl;
    file << "    KC = CDp->Ka;" << endl;
    file << "    KD = CDp->Kb;" << endl;

    file << "    JA = ABp->Ja;" << endl;
    file << "    JB = ABp->Jb;" << endl;
    file << "    JC = CDp->Ja;" << endl;
    file << "    JD = CDp->Jb;" << endl;

    file << "    nJ1 =     JA;" << endl;
    file << "    nJ2 = nJ1*JB;" << endl;
    file << "    nJ3 = nJ2*JC;" << endl;
    file << "    nJ4 = nJ3*JD;" << endl;

    file << "    dim3 Nbl (  1, yNtiles/NTY);" << endl;
    file << "    dim3 Nth (NTX, NTY);" << endl;

    file << "    "<<KNLname<<"<<<Nbl,Nth,0,*stream>>> (vars, F0, uv_m_st,   KA,KB,KC,KD,  JA,JB,JC,JD,  nJ1,nJ2,nJ3,nJ4,  ABp->dka,ABp->dkb,CDp->dka,CDp->dkb,  ABp->dNa,ABp->dNb,CDp->dNa,CDp->dNb,  yNtiles);" << endl;

    file << "}" << endl;
    file << endl;
    */

    file.close();
}

#include "math/gamma.hpp"
using namespace LibIGamma;


void WriteGammaLR (const string & Gname, ofstream & file, int Lt) {

    const double iii[] =
    {1, 1, 3, 15, 105, 945, 10395, 135135, 2027025, 34459425, 654729075,
     13749310575., 316234143225., 7905853580625., 213458046676875.,
     6190283353629375., 191898783962510625., 6332659870762850625.,
     221643095476699771875., 8200794532637891559375.,
     319830986772877770815625., 13113070457687988603440625.,
     563862029680583509947946875., 25373791335626257947657609375.,
     1192568192774434123539907640625., 58435841445947272053455474390625.};

    file << "static __device__ void "<<Gname<<"_LR(double & m, const double R2) {" << endl;

    file << "    double ir = rsqrt(R2);" << endl;

    file << "    m = " << (PI3*iii[Lt]) << " * ir;" << endl;

    if (Lt>0) {
        if (Lt>0)  file << "    double ir2  = ir   * ir;"   << endl;
        if (Lt&1)  file << "    m *= ir2;" << endl;
        if (Lt>1)  file << "    double ir4  = ir2  * ir2;"  << endl;
        if (Lt&2)  file << "    m *= ir4;" << endl;
        if (Lt>3)  file << "    double ir8  = ir4  * ir4;"  << endl;
        if (Lt&4)  file << "    m *= ir8;" << endl;
        if (Lt>7)  file << "    double ir16 = ir8  * ir8;"  << endl;
        if (Lt&8)  file << "    m *= ir16;" << endl;
        if (Lt>15) file << "    double ir32 = ir16 * ir16;" << endl;
        if (Lt&16) file << "    m *= ir32;" << endl;
        if (Lt>31) file << "    double ir64 = ir32 * ir32;" << endl;
        if (Lt&32) file << "    m *= ir64;" << endl;
    }

    file << "}" << endl << endl;
}

void WriteGammaSR (const string & Gname, ofstream & file, int Lt) {

    file << "static __device__ void "<<Gname<<"_SR(double & e, double & m, const double KpqR2) {" << endl;

    file << "    double p = "<<IFSTEP<<" * KpqR2;" << endl;
    file << "    int pos = int(p+0.5);" << endl;
    file << "    double x0 = "<<FSTEP<<"*double(pos);" << endl;
    file << "    double Ax = KpqR2-x0;" << endl;

    file << "    m = 0;" << endl;
    for (int p=NEXPC-1; p>=0; --p)
    file << "    m = gamma_cfs["<<Lt<<"][pos]["<<p<<"] + Ax*m;" << endl;

    if (Lt>0)    file << "    e = exp(-KpqR2);" << endl;
    else         file << "    e = 0;" << endl;

    file << "}" << endl << endl;
}

void WriteGammaT  (const string & Gname, ofstream & file, int Lt, bool gABCD = true) {

    file << "static __device__ void "<<Gname<<"(double & e, double & m, ";
    if (!gABCD) file << "const double m0, ";
    file << "const double iKpq, const double Kpq, const double Kpqn, const double R2, const double W) {" << endl;

    file << "    if (R2 > iKpq*"<<(double(MAXZ)-FSTEP)<<") {" << endl;
    if ( gABCD){file << "        "<<Gname<<"_LR (m, R2);" << endl;
                file << "        m = W*m;" << endl;}
    else        file << "        m = W*m0;" << endl;
    file << "        e = 0;" << endl;
    file << "    }" << endl;
    file << "    else {" << endl;
    //file << "        double Kpq = 1./iKpq;" << endl;
    file << "        double KpqR2 = Kpq * R2;" << endl;

    file << "        "<<Gname<<"_SR (e,m, KpqR2);" << endl;

    file << "        m *= Kpqn*W;" << endl;
    if (Lt>0)
    file << "        e *= Kpqn*W;" << endl;
    file << "    }" << endl;
    file << "}" << endl << endl;
}


void ERIroutine::WriteGK4CUDA() const {


    const bool writeCDR = false;

    stringstream Lstr; Lstr << int(Lt);

    string CURname = "K4_"    + IdString(false);
    string Gname   = "Gamma_" + Lstr.str();
    string LRname  = Gname+"_LR";
    string SRname  = Gname+"_SR";


    string EFSdir = K4CU_DIR;
    if (EFSdir=="") return; //skip

    string K4file = EFSdir + "/" + CURname + ".cu";

    ofstream file;
    file.open(K4file.c_str());
    file.precision(16);

    file << "#include \"defs.hpp\"" << endl;
    file << endl;

    //geometric vars
    bool SAB, SCD;

    if      (geometry==ABCD || geometry==ABAD) {
        SAB = true;
        SCD = true;
    }
    else if (geometry==AACD) {
        SAB = false;
        SCD = true;
    }
    else if (geometry==AACC) {
        SAB = false;
        SCD = false;
    }

    file << "extern __constant__ double gamma_cfs["<<4*LMAXG+1<<"]["<<NVIGF<<"]["<<NEXPC<<"];" << endl << endl;

    WriteGammaSR (Gname, file, Lt);
    WriteGammaLR (Gname, file, Lt);
    WriteGammaT  (Gname, file, Lt, (SAB && SCD));


    file << "__global__ void "<<CURname<<" (const double * vars, const double * F0,   double * uv_m_st, " << endl;
    file << "        uint8_t   KA, uint8_t   KB, uint8_t   KC, uint8_t   KD, "  << endl;
    file << "        uint8_t   JA, uint8_t   JB, uint8_t   JC, uint8_t   JD, "  << endl;
    file << "        uint16_t nJ1, uint16_t nJ2, uint16_t nJ3, uint16_t nJ4, "  << endl;
    file << "        const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd, "  << endl;
    file << "        const double * iKa, const double * iKb, const double * iKc, const double * iKd, "  << endl;
    file << "        const double * iKba, const double * iKdc, "  << endl;
    file << "        const double * iKs3, const double * iKt3, "  << endl;
    file << "        const double *  Kba, const double *  Kdc, "  << endl;

    file << "        const double * Nav, const double * Nbu, const double * Nct, const double * Nds, "  << endl;
    file << "        const double * Na, const double * Nb, const double * Nc, const double * Nd, "  << endl;
    file << "        uint32_t maxTiles, double ilogThresh) {" << endl;

    file << "    int i = threadIdx.x;" << endl;
    file << "    int k = blockIdx.y * blockDim.y + threadIdx.y;" << endl; // this is effectively the number of warp (32-tile)

    file << "    if (k>=maxTiles) return;" << endl;

    //include only the necessary ones
    file << "    const double im2[] = {";
    for (int l=0; l<Lt; ++l) file << "1./" << (2*l+1) << ". ,";
    file << "1./" << (2*Lt+1) << ".};" << endl;


    if      ( SAB &&  SCD) {
        file << "    double ABz = vars[k*6*NTX +       i];" << endl;
        file << "    double CDy = vars[k*6*NTX +   NTX+i];" << endl;
        file << "    double CDz = vars[k*6*NTX + 2*NTX+i];" << endl;
        file << "    double ACx = vars[k*6*NTX + 3*NTX+i];" << endl;
        file << "    double ACy = vars[k*6*NTX + 4*NTX+i];" << endl;
        file << "    double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;

        file << "    double CD2  =        CDy*CDy + CDz*CDz;" << endl;
        file << "    double ACCD =        ACy*CDy + ACz*CDz; ACCD += ACCD;" << endl;
        file << "    double ABCD =                  ABz*CDz; ABCD += ABCD;" << endl;
        file << "    double X2   = ACx*ACx;" << endl;
        file << "    double AC2  =      X2+ACy*ACy+ACz*ACz;" << endl;
        file << "    double ACAB =                 ABz*ACz; ACAB += ACAB;" << endl;
        file << "    double AB2  =                 ABz*ABz;" << endl;

        file << "    const double ikmab = AB2 * ilogThresh;" << endl;
        file << "    const double ikmcd = CD2 * ilogThresh;" << endl;
    }
    else if (!SAB &&  SCD) {
        file << "    double CDz = vars[k*6*NTX + 2*NTX+i];" << endl;
        file << "    double ACy = vars[k*6*NTX + 4*NTX+i];" << endl;
        file << "    double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;

        file << "    double CD2  =                  CDz*CDz;" << endl;
        file << "    double ACCD =                  ACz*CDz; ACCD += ACCD;" << endl;
        file << "    double Y2   =           ACy*ACy;" << endl;
        file << "    double AC2  =        Y2+ACz*ACz;" << endl;

        file << "    const double ikmcd = CD2 * ilogThresh;" << endl;
    }
    else if (!SAB && !SCD) {
        file << "    double ACz = vars[k*6*NTX + 5*NTX+i];" << endl;

        file << "    double AC2  =      ACz*ACz;" << endl;
    }

        if (writeCDR)
    file << "    double K1J4e["<<memK1J4e<<"];" << endl;
    file << "    double K1J4f["<<memK1J4f<<"];" << endl;
        if (writeCDR)
    file << "    double K2J3e["<<memK2J3e<<"];" << endl;
    file << "    double K2J3f["<<memK2J3f<<"];" << endl;
        if (writeCDR)
    file << "    double K3J2e["<<memK3J2e<<"];" << endl;
    file << "    double K3J2f["<<memK3J2f<<"];" << endl;
        if (writeCDR)
    file << "    double K4J1e["<<memK4J1e<<"];" << endl;
    file << "    double K4J1f["<<memK4J1f<<"];" << endl;


    file << "    double wab[maxK2];" << endl;
    file << "    int ba=0;" << endl;
    file << "    for (int b=0; b<KB; ++b) {" << endl;
    if      (SAB)
    file << "        double ikma = ikmab-iKb[b];" << endl;
    file << "        for (int a=0; a<KA; ++a) {" << endl;
    if (SAB)
    file << "            if (iKa[a] < ikma) break;" << endl;
    file << "            wab[ba] = iKs3[b*KA+a]";
    if (SAB) file << " * exp(-Kba[b*KA+a] * AB2)";
    file << ";" << endl;
    file << "            ++ba;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;



    if (!SAB && !SCD) {
    file << "    double mK0;" << endl;
    file << "    "<<LRname<<"(mK0, AC2);" << endl;
    }


        if (writeCDR)
    file << "    memset(K1J4e, 0, nJ4*"<<memK1J4e*sizeof(double)<<");" << endl;
    file << "    memset(K1J4f, 0, nJ4*"<<memK1J4f*sizeof(double)<<");" << endl;

    file << "    for (int d=0; d<KD; ++d) {" << endl; {
    file << "        const double * F1 = F0 + d*KC*KB*KA * 2;" << endl;

        if (writeCDR)
    file << "        memset(K2J3e, 0, nJ3*"<<memK2J3e*sizeof(double)<<");" << endl;
    file << "        memset(K2J3f, 0, nJ3*"<<memK2J3f*sizeof(double)<<");" << endl;
    if      (SCD)
    file << "        double ikmc = ikmcd-iKd[d];" << endl;

    file << "        for (int c=0; c<KC; ++c) {" << endl; {
    if      (SCD)
    file << "            if (iKc[c] < ikmc) break;" << endl;

    file << "            const double * F2 = F1 + c*KB*KA * 2;" << endl;

    file << "            const double ikcd = iKdc[d*KC+c];" << endl;
    if      (SCD)
    file << "            const double rcd = Kd[d] * ikcd;" << endl;

    file << "            double wcd = iKt3[d*KC+c]"; //(ikcd) * sqrt(ikcd)";
    if (SCD)  file << " * exp(-Kdc[d*KC+c] * CD2)";
     file << ";" << endl;

        if (writeCDR)
    file << "            memset(K3J2e, 0, nJ2*"<<memK3J2e*sizeof(double)<<");" << endl;
    file << "            memset(K3J2f, 0, nJ2*"<<memK3J2f*sizeof(double)<<");" << endl;


    if      ( SAB &&  SCD) {
    file << "            double AQy  = ACy + CDy * rcd;" << endl;
    file << "            double AQz  = ACz + CDz * rcd;" << endl;

    file << "            double X2Y2 = X2   + AQy*AQy;" << endl;
    file << "            double AQ2  = X2Y2 + AQz*AQz;" << endl;

    file << "            double AQAB = (AQz*ABz); AQAB += AQAB;" << endl;
    }
    else if (!SAB &&  SCD) {
    file << "            double AQz  = ACz + CDz * rcd;" << endl;
    file << "            double AQ2  = Y2  + AQz*AQz;" << endl;

    file << "            double mK2;" << endl;
    file << "            "<<LRname<<"(mK2, AQ2);" << endl;
    }

    file << "            int ba=0;" << endl;

    file << "            for (int b=0; b<KB; ++b) {" << endl; {
    file << "                const double * F3 = F2 + b*KA * 2;" << endl;

        if (writeCDR)
    file << "                memset(K4J1e, 0, nJ1*"<<memK4J1e*sizeof(double)<<");" << endl;
    file << "                memset(K4J1f, 0, nJ1*"<<memK4J1f*sizeof(double)<<");" << endl;
    if      (SAB)
    file << "                double ikma = ikmab-iKb[b];" << endl;

    //file << "                const double * F3 = F2 + b*KA * 2*NTX;" << endl;

    file << "                for (int a=0; a<KA; ++a) {" << endl; {
    if      (SAB)
    file << "                    if (iKa[a] < ikma) break;" << endl;

    file << "                    const double * F4 = F3 + a * 2;" << endl;

    file << "                    const double ikab = iKba[b*KA+a];" << endl;
    file << "                    const double iKpq = (ikab + ikcd);" << endl;

    file << "                    double ww = wcd*wab[ba];" << endl;
    file << "                    ++ba;" << endl;

    if (SAB && SCD) {
    file << "                    const double rab = Kb[b] * ikab;" << endl;
    file << "                    double PQz = AQz - ABz*rab;" << endl;
    file << "                    double PQ2 = X2Y2 + PQz*PQz;" << endl;
    }

    file << "                    const double Kpq  = F4[0];" << endl;
    file << "                    const double Kpqn = F4[1];" << endl;

    file << "                    double J0e["<<int(Lt+1)<<"];" << endl;
    file << "                    double J0f["<<int(Lt+1)<<"];" << endl;

    if (SAB && SCD)
    file << "                    "<<Gname<<"(J0e[0], J0f[0],      iKpq, Kpq, Kpqn,  PQ2,  ww);" << endl;
    else if (!SAB &&  SCD)
    file << "                    "<<Gname<<"(J0e[0], J0f[0], mK2, iKpq, Kpq, Kpqn,  AQ2,  ww);" << endl;
    else if (!SAB && !SCD)
    file << "                    "<<Gname<<"(J0e[0], J0f[0], mK0, iKpq, Kpq, Kpqn,  AC2,  ww);" << endl;

    //AERR K4 (uncontracted)
    {
                                 for (Op_K4 * s2=nseqK4[AERR4]+1; s2<nseqK4[AERR4+1]; ++s2)
    file << "                    J0e["<<s2->dest<<"] = J0e["<<s2->op1<<"] * (0.5*iKpq);" << endl;
    }

    //CDR K4 (uncontracted downward recursion)
    {
                                    for (Op_K4 * s2=nseqK4[CDR4]+1; s2<nseqK4[CDR4+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                    J0f["<<s2->dest<<"] = (PQ2*J0f["<<s2->op1<<"] + J0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                    J0f["<<s2->dest<<"] = (AQ2*J0f["<<s2->op1<<"] + J0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                    J0f["<<s2->dest<<"] = (AC2*J0f["<<s2->op1<<"] + J0e["<<s2->ope<<"]) * im2["<<s2->aux<<"];" << endl;
                                    }
    }


    //K4 E + F
    {
    file << "                    {" << endl;
        if (writeCDR)
    file << "                        double       * J1e = K4J1e;" << endl;
    file << "                        double       * J1f = K4J1f;" << endl;

                                   #ifdef __QR_GC__
    file << "                        for (int jj=0; jj<min(JA, a+1); ++jj) {" << endl;
                                   #else
    file << "                        for (uint8_t jj=0; jj<JA; ++jj) {" << endl;
                                   #endif
    file << "                        const double * V = Nav + ((b*KA + a)*JA+jj)*16;" << endl;

        if (writeCDR)
                                     for (Op_K4 * s2=nseqK4[K4E]; s2<nseqK4[K4E+1]; ++s2)
    file << "                            J1e["<<s2->dest<<"] += J0e["<<s2->op1<<"] * V["<<s2->aux<<"];" << endl;

                                     for (Op_K4 * s2=nseqK4[K4F]; s2<nseqK4[K4F+1]; ++s2)
    file << "                            J1f["<<s2->dest<<"] += J0f["<<s2->op1<<"] * V["<<s2->aux<<"];" << endl;

        if (writeCDR)
    file << "                            J1e += "<<memK4J1e<<";" << endl;
    file << "                            J1f += "<<memK4J1f<<";" << endl;

    file << "                        }" << endl;

    file << "                    }" << endl;
    }

    //file << "                    F0 += 2*NTX;" << endl;
    file << "                }" << endl;}

    //AERR K3
    if (writeCDR) {
    file << "                {" << endl;
    file << "                    double * Je = (double*)buffers[k].K4J1e;" << endl;

    file << "                    for (uint8_t nj1=0; nj1<J1; ++nj1) {" << endl;

                                  for (Op_K4 * s2=nseqK4[AERR3]; s2<nseqK4[AERR3+1]; ++s2) {
    file << "                        Je["<<s2->dest<<"*NTX+i] = Je["<<s2->op1<<"*NTX+i] + Je["<<s2->op2<<"*NTX+i] * (0.5*ikcd);" << endl;
                                  }
    file << "                        Je += "<<memK4J1e<<"*NTX;" << endl;

    file << "                    }" << endl;
    file << "                }" << endl;
    }

    //CDR3
    if (writeCDR) {
    file << "                {" << endl;
    if (SAB && SCD) {
    file << "                    double AQABb = AQAB * (2*Kb[b]);" << endl;
    file << "                    double AB2b2 = AB2  * (4*Kb[b]*Kb[b]);" << endl;
    }

    file << "                    double * Jf = (double*)buffers[k].K4J1f;" << endl;
    file << "                    double * Je = (double*)buffers[k].K4J1e;" << endl;

    file << "                    for (uint8_t nj1=0; nj1<J1; ++nj1) {" << endl;

                                 for (Op_K4 * s2=nseqK4[CDR3]; s2<nseqK4[CDR3+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                        Jf["<<s2->dest<<"*NTX+i] = (AQ2*Jf["<<s2->op1<<"*NTX+i] - AQABb*Jf["<<s2->op2<<"*NTX+i] + AB2b2*Jf["<<s2->op3<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                        Jf["<<s2->dest<<"*NTX+i] = (AQ2*Jf["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                        Jf["<<s2->dest<<"*NTX+i] = (AC2*Jf["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                            }

    file << "                        Jf += "<<memK4J1f<<"*NTX;" << endl;
    file << "                        Je += "<<memK4J1e<<"*NTX;" << endl;
    file << "                    }" << endl;
    file << "                }" << endl;
    }

    {
    file << "                {" << endl;
        if (writeCDR)
    file << "                    const double * J1e = K4J1e;" << endl;
    file << "                    const double * J1f = K4J1f;" << endl;
        if (writeCDR)
    file << "                    double * J2e = K3J2e;" << endl;
    file << "                    double * J2f = K3J2f;" << endl;

    file << "                    for (uint16_t nj1=0; nj1<nJ1; ++nj1) {" << endl;
                            #ifdef __QR_GC__
    file << "                        for (int jj=0; jj<min(JB, b+1); ++jj) {" << endl;
                            #else
    file << "                        for (uint8_t jj=0; jj<JB; ++jj) {" << endl;
                            #endif
    file << "                        const double * U = Nbu + (b*JB+jj)*16;" << endl;
        if (writeCDR)
                                    for (Op_K4 * s2=nseqK4[K3E]; s2<nseqK4[K3E+1]; ++s2)
    file << "                            J2e["<<s2->dest<<"] += J1e["<<s2->op1<<"] * U["<<s2->aux<<"];" << endl;
                                    for (Op_K4 * s2=nseqK4[K3F]; s2<nseqK4[K3F+1]; ++s2)
    file << "                            J2f["<<s2->dest<<"] += J1f["<<s2->op1<<"] * U["<<s2->aux<<"];" << endl;
        if (writeCDR)
    file << "                            J2e += "<<memK3J2e<<";" << endl;
    file << "                            J2f += "<<memK3J2f<<";" << endl;
    file << "                        }" << endl;

                  #ifdef __QR_GC__
        if (writeCDR)
    file << "                        if (c+1<JC) J2e += (JB-(b+1))*"<<memK3J2e<<";" << endl;
    file << "                        if (c+1<JC) J2f += (JB-(b+1))*"<<memK3J2f<<";" << endl;
                  #endif
        if (writeCDR)
    file << "                        J1e += "<<memK4J1e<<";" << endl;
    file << "                        J1f += "<<memK4J1f<<";" << endl;

    file << "                    }" << endl;
    file << "                }" << endl;
    }

    file << "            }" << endl; }


    //AERR K2
    if (writeCDR) {
    file << "            {" << endl;
    file << "                double * J2 = (double*)buffers[k].K3J2e;" << endl;

                            //K2J2
    file << "                for (uint16_t nj2=0; nj2<J2; ++nj2) {" << endl;


                             for (Op_K4 * s2=nseqK4[AERR2]; s2<nseqK4[AERR2+1]; ++s2) {
    file << "                    J2["<<s2->dest<<"*NTX+i] = J2["<<s2->op1<<"*NTX+i] + J2["<<s2->op2<<"*NTX+i] * (0.5*ikcd);" << endl;
                             }
    file << "                    J2 += "<<memK3J2e<<"*NTX;" << endl;

    file << "                }" << endl;

    file << "            }" << endl;
    }

    //CDR K2
    if (writeCDR) {
    file << "            {" << endl;
    file << "                double * J2 = (double*)buffers[k].K3J2f;" << endl;
    file << "                double * Je = (double*)buffers[k].K3J2e;" << endl;

    file << "                for (uint16_t nj2=0; nj2<J2; ++nj2) {" << endl;
                            for (Op_K4 * s2=nseqK4[CDR2]; s2<nseqK4[CDR2+1]; ++s2) {
    if      ( SAB &&  SCD)
    file << "                    J2["<<s2->dest<<"*NTX+i] = (AQ2*J2["<<s2->op1<<"*NTX+i] - AQAB*J2["<<s2->op2<<"*NTX+i] + AB2*J2["<<s2->op3<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB &&  SCD)
    file << "                    J2["<<s2->dest<<"*NTX+i] = (AQ2*J2["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
    else if (!SAB && !SCD)
    file << "                    J2["<<s2->dest<<"*NTX+i] = (AC2*J2["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                            }

    file << "                    J2 += "<<memK3J2f<<"*NTX;" << endl;
    file << "                    Je += "<<memK3J2e<<"*NTX;" << endl;
    file << "                }" << endl;
    file << "            }" << endl;
    }

    {
    file << "            {" << endl;
        if (writeCDR)
    file << "                const double * J2e = K3J2e;" << endl;
    file << "                const double * J2f = K3J2f;" << endl;
        if (writeCDR)
    file << "                double * J3e = K2J3e;" << endl;
    file << "                double * J3f = K2J3f;" << endl;

                            //K2J3e+f contraction
    file << "                for (uint16_t nj2=0; nj2<nJ2; ++nj2) {" << endl;
                  #ifdef __QR_GC__
    file << "                    for (uint8_t jj=0; jj<min(JC, c+1); ++jj) {" << endl;
                  #else
    file << "                    for (uint8_t jj=0; jj<JC; ++jj) {" << endl;
                  #endif
    file << "                        const double * T = Nct + ((d*KC + c)*JC+jj)*16;" << endl;

        if (writeCDR)
                                for (Op_K4 * s2=nseqK4[K2E]; s2<nseqK4[K2E+1]; ++s2)
    file << "                        J3e["<<s2->dest<<"] += J2e["<<s2->op1<<"] * T["<<s2->aux<<"];" << endl;
                                for (Op_K4 * s2=nseqK4[K2F]; s2<nseqK4[K2F+1]; ++s2)
    file << "                        J3f["<<s2->dest<<"] += J2f["<<s2->op1<<"] * T["<<s2->aux<<"];" << endl;

        if (writeCDR)
    file << "                        J3e += "<<memK2J3e<<";" << endl;
    file << "                        J3f += "<<memK2J3f<<";" << endl;
    file << "                    }" << endl;

                  #ifdef __QR_GC__
        if (writeCDR)
    file << "                    if (c+1<JC) J3e += (JC-(c+1))*"<<memK2J3e<<";" << endl;
    file << "                    if (c+1<JC) J3f += (JC-(c+1))*"<<memK2J3f<<";" << endl;
                  #endif
        if (writeCDR)
    file << "                    J2e += "<<memK3J2e<<";" << endl;
    file << "                    J2f += "<<memK3J2f<<";" << endl;
    file << "                }" << endl;
    file << "            }" << endl;
    }

    file << "        }" << endl;}

    //AERR K1
    if (writeCDR) {
    file << "        {" << endl;
    file << "            double * J3 = (double*)buffers[k].K2J3e;" << endl;

    file << "            for (int nj3=0; nj3<nJ3; ++nj3) {" << endl;
                for (Op_K4 * s2=nseqK4[AERR1]; s2<nseqK4[AERR1+1]; ++s2) {
    file << "                J3["<<s2->dest<<"*NTX+i] = J3["<<s2->op1<<"*NTX+i] + J3["<<s2->op2<<"*NTX+i];" << endl;
                }
    file << "                J3 += "<<memK2J3e<<"*NTX;" << endl;

    file << "            }" << endl;
    file << "        }" << endl;
    }

    //CDR K1
    if (writeCDR) {
        if      ( SAB && SCD) {
    file << "        double CD2d2 = CD2  * (4*cuKd[d]*cuKd[d]);" << endl;
    file << "        double ACCDd = ACCD * (2*cuKd[d]);" << endl;
    file << "        double ABCDd = ABCD * (2*cuKd[d]);" << endl;
        }
        else if (!SAB && SCD) {
    file << "        double CD2d2 = CD2  * (4*cuKd[d]*cuKd[d]);" << endl;
    file << "        double ACCDd = ACCD * (2*cuKd[d]);" << endl;
        }

    file << "        {" << endl;
    file << "            double * J3 = (double*)buffers[k].K2J3f;" << endl;
    file << "            double * Je = (double*)buffers[k].K2J3e;" << endl;

    file << "            for (int nj3=0; nj3<nJ3; ++nj3) {" << endl;

                for (Op_K4 * s2=nseqK4[CDR1]; s2<nseqK4[CDR1+1]; ++s2) {
                    if      ( SAB &&  SCD)
    file << "                J3["<<s2->dest<<"*NTX+i] = (AC2*J3["<<s2->op1<<"*NTX+i] - ACAB*J3["<<s2->op2<<"*NTX+i] + ACCDd*J3["<<s2->op3<<"*NTX+i] + AB2*J3["<<s2->op4<<"*NTX+i] - ABCDd*J3["<<s2->op5<<"*NTX+i] + CD2d2*J3["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB &&  SCD)
    file << "                J3["<<s2->dest<<"*NTX+i] = (AC2*J3["<<s2->op1<<"*NTX+i] + ACCDd*J3["<<s2->op3<<"*NTX+i] + CD2d2*J3["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB && !SCD)
    file << "                J3["<<s2->dest<<"*NTX+i] = (AC2*J3["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                }

    file << "                J3 += "<<memK2J3f<<"*NTX;" << endl;
    file << "                Je += "<<memK2J3e<<"*NTX;" << endl;
    file << "            }" << endl;
    file << "        }" << endl;
    }

    {
    file << "        {" << endl;
        if (writeCDR)
    file << "            const double * J3e = K2J3e;" << endl;
    file << "            const double * J3f = K2J3f;" << endl;

        if (writeCDR)
    file << "            double * J4e = K1J4e;" << endl;
    file << "            double * J4f = K1J4f;" << endl;

    file << "            for (uint16_t nj3=0; nj3<nJ3; ++nj3) {" << endl;
              #ifdef __QR_GC__
    file << "                for (uint8_t jj=0; jj<min(JD, d+1); ++jj) {" << endl;
              #else
    file << "                for (uint8_t jj=0; jj<JD; ++jj) {" << endl;
              #endif
    file << "                    const double * S = Nds + (d*JC+jj)*16;" << endl;

        if (writeCDR)
                            for (Op_K4 * s2=nseqK4[K1E]; s2<nseqK4[K1E+1]; ++s2)
    file << "                    J4e["<<s2->dest<<"] += J3e["<<s2->op1<<"] * S["<<s2->aux<<"];" << endl;
                            for (Op_K4 * s2=nseqK4[K1F]; s2<nseqK4[K1F+1]; ++s2)
    file << "                    J4f["<<s2->dest<<"] += J3f["<<s2->op1<<"] * S["<<s2->aux<<"];" << endl;
        if (writeCDR)
    file << "                    J4e += "<<memK1J4e<<";" << endl;
    file << "                    J4f += "<<memK1J4f<<";" << endl;
    file << "                }" << endl;

                  #ifdef __QR_GC__
        if (writeCDR)
    file << "                if (d+1<JD) J4e += (JD-(d+1))*"<<memK1J4e<<";" << endl;
    file << "                if (d+1<JD) J4f += (JD-(d+1))*"<<memK1J4f<<";" << endl;
                  #endif

        if (writeCDR)
    file << "                J3e += "<<memK2J3e<<";" << endl;
    file << "                J3f += "<<memK2J3f<<";" << endl;
    file << "            }" << endl;
    file << "        }" << endl;
    }


    file << "    }" << endl; }

    // THIS SHOULD BE PART OF THE TRANSFORMATIONS; SINCE THE LAST RRS
    // INCREMENT THE NUMBER OF KERNELS FROM O(L^5) TO O(L^4)

    //AERR K0
    if (writeCDR) {
    file << "    {" << endl;
    file << "        double * J4 = (double*)buffers[k].K1J4e;" << endl;

    file << "        for (int nj4=0; nj4<nJ4; ++nj4) {" << endl;

                for (Op_K4 * s2=nseqK4[AERR0]; s2<nseqK4[AERR0+1]; ++s2) {
    file << "            J4["<<s2->dest<<"*NTX+i] = J4["<<s2->op1<<"*NTX+i] + J4["<<s2->op2<<"*NTX+i];" << endl;
                }

    file << "            J4 += "<<memK1J4e<<"*NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }

    //CDR K0
    if (writeCDR) {
    file << "    {" << endl;
    file << "        double * J4 = (double*)buffers[k].K1J4f;" << endl;
    file << "        double * Je = (double*)buffers[k].K1J4e;" << endl;

    file << "        for (int nj4=0; nj4<nJ4; ++nj4) {" << endl;

                for (Op_K4 * s2=nseqK4[CDR0]; s2<nseqK4[CDR0+1]; ++s2) {
                    if      ( SAB &&  SCD)
    file << "            J4["<<s2->dest<<"*NTX+i] = (AC2*J4["<<s2->op1<<"*NTX+i] - ACAB*J4["<<s2->op2<<"*NTX+i] + ACCD*J4["<<s2->op3<<"*NTX+i] + AB2*J4["<<s2->op4<<"*NTX+i] - ABCD*J4["<<s2->op5<<"*NTX+i] + CD2*J4["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB &&  SCD)
    file << "            J4["<<s2->dest<<"*NTX+i] = (AC2*J4["<<s2->op1<<"*NTX+i] + ACCD*J4["<<s2->op3<<"*NTX+i] + CD2*J4["<<s2->op6<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                    else if (!SAB && !SCD)
    file << "            J4["<<s2->dest<<"*NTX+i] = (AC2*J4["<<s2->op1<<"*NTX+i] + Je["<<s2->ope<<"*NTX+i]) * im2["<<s2->aux<<"];" << endl;
                }

    file << "            J4 += "<<memK1J4f<<"*NTX;" << endl;
    file << "            Je += "<<memK1J4e<<"*NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }

    //copy to the kernel buffer
    {
    file << "    {" << endl;
    file << "        const double * J4 = K1J4f;" << endl;
    file << "        double * I =  &uv_m_st[k*nJ4*"<<nKernels<< "*NTX];" << endl;

    file << "        for (uint16_t nj4=0; nj4<nJ4; ++nj4) {" << endl;
            const Op_MIRROR * s2 = eseq;

            for (int p=0; p<nKernels; ++p,++s2) {
                if (s2->aux == 0) //dest[i] = op1[i];
    file << "            I["<<s2->dest<<"*NTX+i] = J4["<<s2->op1<<"];" << endl;
                else //             dest[i] = 0;     //each thread zeroes one value
    file << "            I["<<s2->dest<<"*NTX+i] = 0;" << endl;
            }

    file << "            J4 += "<<memK1J4f<<";" << endl;
    file << "            I  += "<<nKernels<<" * NTX;" << endl;
    file << "        }" << endl;
    file << "    }" << endl;
    }


    file << "}" << endl;
    file << endl;

    file.close();
}



