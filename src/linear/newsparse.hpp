


#ifndef __NEW_SPARSE__
#define __NEW_SPARSE__

#include "defs.hpp"
#include "low/rtensors.hpp"


class tensor2;
class tensor;
class rAtom;
class rAtomPrototype;
class SparseGPU;
class BatchEvaluator;

struct SparseGPU;


// many of these variiables should be rewritten for internal consistency

class sparsetensorpattern {
    friend class tensor2;
    friend class tensor;
    friend class SparseGPU;
    friend class BatchEvaluator;

  public:
    //first level, atom blocks
    r2tensor<uint32_t> a2pos;
    //second level, GC function blocks
    r2tensor< r2tensor<uint32_t> > f2pos; //offsets for the block of GC functions of a given block respective to the beginning of the atomair block
    //third level, individual function blocks
    r2tensor< r2tensor<r2tensor<uint32_t> > > j2pos; //offsets for a function pair respective to the beginning of the GC block block

    //first level, atom blocks
    uint32_t * a1pos = NULL;
    //second level, GC function blocks
    r2tensor<uint32_t> f1pos;
    //third level, individual function blocks
    r2tensor<r2tensor<uint32_t> > j1pos;


    uint16_t nids;
    uint16_t * ids = NULL;
    uint16_t * nfs = NULL;   // number of functions for the given atom

    uint16_t * alen = NULL;  // total length of the atom block (total number of basis functions)
    r2tensor<uint16_t> flen; // total length of a given function (at, nf)
    r2tensor<uint8_t>  js;   // j's
    r2tensor<uint8_t>  ms;   // ms

    //one has to store which 'element' every atom is so to avoid wasting so much space
    uint32_t natoms;
    uint32_t nbasis;
    uint32_t ntotal;
    uint32_t ntotal2;

  public:

    void SetBlocks(const rAtom * Atoms, int nAtoms, const r1tensor<rAtomPrototype*> & AtomTypeList);

    void UnSetBlocks();

    // THESE FUNCTIONS SHOULD BE NAMED DIFFERENTLY FROM EACH OTHER

    // get the rank-1 offsets
    uint32_t GetOffset1 (uint32_t at1) const;
    uint32_t GetOffset1 (uint32_t at1, uint16_t nf1) const;
    uint32_t GetOffset1 (uint32_t at1, uint16_t nf1, uint16_t nj1) const;

    uint32_t GetLen1    (uint32_t at1) const;

    // get the rank-2 offsets
    uint32_t GetOffset (uint32_t at1, uint32_t at2) const;
    uint32_t GetOffset (uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2) const;
    uint32_t GetOffset (uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2, uint16_t nj1, uint16_t nj2) const;
};



class Sparse // :private sparsetensorpattern
{
    friend class tensor2;
    friend class tensor;
    friend class Fock2e;
    friend class SparseGPU;
    friend class BatchEvaluator;

  public:
    double * values;
    const sparsetensorpattern * P;

  public:

    Sparse() {
        values   = NULL;
        P        = NULL;
    }

   ~Sparse() {
        if (values!=NULL) delete[] values;
    }

    void Set(const sparsetensorpattern & T) {
        P = &T;
        if (values!=NULL) delete[] values;
        values = new double[P->ntotal2];
    }

    void UnSet() {
        if (values!=NULL) delete[] values;
        values = NULL;
    }

    void zeroize() {
        for (int i=0; i<P->ntotal2; ++i) values[i] = 0;
    }

    inline double * operator()(uint32_t at1, uint32_t at2) {
        return values + P->a2pos(at1, at2);
    }

    inline const double * operator()(uint32_t at1, uint32_t at2) const {
        return values + P->a2pos(at1, at2);
    }

    inline double * operator()(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2) {
        uint16_t id1 = P->ids[at1];
        uint16_t id2 = P->ids[at2];

        return values + P->a2pos(at1, at2) + P->f2pos(id1,id2)(nf1,nf2);
    }

    inline const double * operator()(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2) const {
        uint16_t id1 = P->ids[at1];
        uint16_t id2 = P->ids[at2];

        return values + P->a2pos(at1, at2) + P->f2pos(id1,id2)(nf1,nf2);
    }

    inline double * operator()(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2, uint8_t nj1, uint8_t nj2) {
        uint16_t id1 = P->ids[at1];
        uint16_t id2 = P->ids[at2];
        uint32_t mj1 = P->js[id1][nf1];
        uint32_t mj2 = P->js[id2][nf2];
        uint32_t mm1 = P->ms[id1][nf1];
        uint32_t mm2 = P->ms[id2][nf2];

        return values + P->a2pos(at1, at2) + P->f2pos(id1,id2)(nf1,nf2) + (nj1*mj2 + nj2)*(mm1*mm2);
    }

    inline const double * operator()(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2, uint8_t nj1, uint8_t nj2) const {
        uint16_t id1 = P->ids[at1];
        uint16_t id2 = P->ids[at2];
        uint32_t mj1 = P->js[id1][nf1];
        uint32_t mj2 = P->js[id2][nf2];
        uint32_t mm1 = P->ms[id1][nf1];
        uint32_t mm2 = P->ms[id2][nf2];

        return values + P->a2pos(at1, at2) + P->f2pos(id1,id2)(nf1,nf2) + (nj1*mj2 + nj2)*(mm1*mm2);
    }

    inline double & operator()(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2, uint8_t nj1, uint8_t nj2, uint8_t m1, uint8_t m2) {
        uint16_t id1 = P->ids[at1];
        uint16_t id2 = P->ids[at2];
        uint32_t mj1 = P->js[id1][nf1];
        uint32_t mj2 = P->js[id2][nf2];
        uint32_t mm1 = P->ms[id1][nf1];
        uint32_t mm2 = P->ms[id2][nf2];

        return *(values + P->a2pos(at1, at2) + P->f2pos(id1,id2)(nf1,nf2) + (nj1*mj2 + nj2)*(mm1*mm2) + (m2*mm1 + m1) );
    }

    inline const double & operator()(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2, uint8_t nj1, uint8_t nj2, uint8_t m1, uint8_t m2) const {
        uint16_t id1 = P->ids[at1];
        uint16_t id2 = P->ids[at2];
        uint32_t mj1 = P->js[id1][nf1];
        uint32_t mj2 = P->js[id2][nf2];
        uint32_t mm1 = P->ms[id1][nf1];
        uint32_t mm2 = P->ms[id2][nf2];

        return *(values + P->a2pos(at1, at2) + P->f2pos(id1,id2)(nf1,nf2) + (nj1*mj2 + nj2)*(mm1*mm2)  + (m2*mm1 + m1));
    }

    void Reduce    (const SparseGPU & rhs);


    Sparse & GetSymmetric     (const tensor2 & rhs);
    Sparse & GetAntiSymmetric (const tensor2 & rhs);


    Sparse & operator=(const tensor2 & rhs);

    Sparse & operator=(const Sparse & rhs);

    Sparse & operator=(const tensor & rhs);

    Sparse & operator+=(const Sparse & rhs);

    Sparse & operator-=(const Sparse & rhs);

    Sparse & operator*=(double rhs);

    Sparse & operator/=(double rhs);

};

//extern sparsetensorpattern SparseTensorPattern;
#ifndef __CUDACC__
class cudaStream_t;
class cudaEvent_t;
#endif


struct SparseGPU {

    double       * v[MAX_GPUS]; // pointer to the values on each GPU
    unsigned int * p[MAX_GPUS]; // relative offsets for each atom block

    cudaStream_t * stream [MAX_GPUS];

    double * h;           // pointer to host memory buffer
    int len;              // matrix length (number of atoms)
    int size2;            // all

    int nGpus;

  //all
    SparseGPU() {
        h = NULL;
        for (int i=0; i<MAX_GPUS; ++i) v[i] = NULL;
        for (int i=0; i<MAX_GPUS; ++i) p[i] = NULL;
        for (int i=0; i<MAX_GPUS; ++i) stream[i] = NULL;
        len = 0;
        nGpus = 0;
    }

   ~SparseGPU() {
        Clear();
    }


    void Set     (const sparsetensorpattern & STP);
    void Copy    (const Sparse & rhs);
    void Add     (const Sparse & rhs);
    void Zeroize ();
    void Clear   ();

    const SparseGPU & operator+=(const SparseGPU & rhs);
};


#endif

