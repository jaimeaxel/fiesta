#include "newtensors.hpp"
#include "newsparse.hpp"


static inline int Tpos (int M1, int M2, int M3, int M4, int v1, int v2, int v3, int v4) {
    return v4 * M3*M2*M1 + v3*M2*M1 + v2*M1 + v1;
}

static inline int Tpos (int M1, int M2, int v1, int v2) {
    return v2*M1 + v1;
}



std::ostream & operator<<(std::ostream & os, const tensor & T) {

    for (int i=0; i<T.getn(); ++i) {
        for (int ii=0; ii<T.pos[i]; ++ii) {
            for (int j=0; j<T.getn(); ++j) {
                for (int jj=0; jj<T.pos[j]; ++jj) {
                    os << T(i,j,ii,jj) << " ";
                }
            }
            os << std::endl << std::endl;
        }
    }
    return os;
}

std::ostream & operator<<(std::ostream & os, const symtensor & T) {
    for (int i=0; i<T.getn(); ++i) {
        for (int ii=0; ii<T.pos[i]; ++ii) {
            for (int j=0; j<T.getn(); ++j) {
                for (int jj=0; jj<T.pos[j]; ++jj) {
                    os << T(i,j,ii,jj) << " ";
                }
            }
            os << std::endl << std::endl;
        }
    }
    return os;
}

tensor::tensor() {
    c  = NULL;
    c2 = NULL;
}


void tensor::init() {

    if (c==NULL) {
        c  = new tensor2**[getn()];
        c2 = new tensor2*[n2];

        for (int i=0;i<getn();++i)
            c[i] = c2 + i*getn();

        for (int i=0; i<getn(); ++i)
            for (int j=0; j<getn(); ++j)
                c[i][j] = NULL;
    }
}

void tensor::init(const tensorpattern & TP) {

    // make a local hard copy
    // of TP's position data
    pos    = TP.pos;
    n2     = TP.n2;
    n2s    = TP.n2s;
    totn   = TP.totn;
    thresh = TP.thresh;

    init();
}

void tensor::clear() {

    if (c2!=NULL)
        for (int i=0; i<getn(); ++i)
            for (int j=0; j<getn(); ++j)
                delete c[i][j];

    delete[] c2;
    delete[] c;
    c2=NULL;
    c =NULL;
}

void tensor::zeroize() {
    for (int i=0; i<getn(); ++i)
        for (int j=0; j<getn(); ++j) {
            touch(i,j);
            if (c[i][j]!=NULL)
                c[i][j]->zeroize();
        }
}

void tensor::trim() {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<getn(); ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                bool inc = false;
                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        if (fabs( C->c[ii][jj] )>=thresh) inc = true;
                    }
                }

                if (!inc) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}

void tensor::trim(double th) {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<getn(); ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                bool inc = false;
                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        if (fabs( C->c[ii][jj] )>= th) inc = true;
                    }
                }

                if (!inc) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}

void tensor::trim(const tensor & s) {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<getn(); ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                bool inc = false;

                if (s.c[i][j]==NULL) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}

void tensor::trim(const symtensor & s) {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<=i; ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                if (s.c[i][j]==NULL) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }

        for (int j=i; j<getn(); ++j) {
            const tensor2 * C = c[j][i];

            if (C!=NULL) {
                if (s.c[j][i]==NULL) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}


tensor2 * tensor::touch(int i, int j) {
    if (c[i][j]==NULL) {
        c[i][j] = new tensor2;
        c[i][j]->setsize(pos[i],pos[j]);
        c[i][j]->zeroize();
        //Pattern.List[i].PushBack(j);
    }
    return c[i][j];
}


tensor & tensor::operator=(const tensor & s) {
    clear();
    init();

    for (int i=0;i<getn();++i) {
        for (int j=0;j<getn();++j) {
            if (s.c[i][j]!=NULL) {
                tensor2 * C = touch(i,j);
                *C = *s.c[i][j];
            }
        }
    }

    return *this;
}

tensor & tensor::operator=(const symtensor & s) {
    clear();
    init();

    for (int i=0;i<getn();++i) {
        for (int j=0;j<i;++j) {
            const tensor2 * B = s.c[i][j];
            if (B!=NULL) {
                touch(i,j);

                tensor2 * A = c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        A->c[ii][jj] = B->c[ii][jj];
                    }
                }

            }
        }

        {
            const tensor2 * B = s.c[i][i];
            if (B!=NULL) {
                touch(i,i);

                tensor2 * A = c[i][i];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        A->c[ii][jj] = B->c[ii][jj];
                    }
                    for (int jj=ii+1; jj<pos[i]; ++jj) {
                        A->c[ii][jj] = B->c[ii][jj];
                    }
                }

            }
        }

        for (int j=i+1;j<getn();++j) {
            const tensor2 * B = s.c[j][i];
            if (B!=NULL) {
                touch(i,j);

                tensor2 * A = c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        A->c[ii][jj] = B->c[jj][ii];
                    }
                }

            }
        }
    }

    return *this;

}

tensor & tensor::operator=(const tensor2 & s) {
    clear();
    init();

    for (int i=0, toti=0;i<getn(); toti+=pos[i], ++i) {
        for (int j=0, totj=0; j<getn(); totj+=pos[j],++j) {
            bool init = false;

            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<pos[j]; ++jj) {
                    if (fabs(s(toti+ii, totj+jj))>thresh) init = true;
                }
            }

            if (init) {
                c[i][j] = new tensor2;
                c[i][j]->setsize(pos[i],pos[j]);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        (*c[i][j])(ii,jj) = s(toti+ii, totj+jj);
                    }
                }
            }
        }
    }
    return *this;
}

tensor & tensor::operator=(const symtensor2 & s) {
    clear();
    init();

    if (c==NULL) {
        c  = new tensor2**[getn()];
        c2 = new tensor2*[n2];

        for (int i=0;i<getn();++i)
            c[i] = c2 + i*getn();

        for (int i=0, toti=0;i<getn(); toti+=pos[i], ++i) {
            for (int j=0, totj=0; j<getn(); totj+=pos[j],++j) {
                bool init = false;

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        if (fabs(s(toti+ii, totj+jj))>thresh) init = true;
                    }
                }

                if (init) {
                    c[i][j] = new tensor2;
                    c[i][j]->setsize(pos[i],pos[j]);

                    for (int ii=0; ii<pos[i]; ++ii) {
                        for (int jj=0; jj<pos[j]; ++jj) {
                            (*c[i][j])(ii,jj) = s(toti+ii, totj+jj);
                        }
                    }
                }

            }
        }
    }
    else {
        for (int i=0, toti=0;i<getn(); toti+=pos[i], ++i) {
            for (int j=0, totj=0; j<getn(); totj+=pos[j],++j) {
                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        (*c[i][j])(ii,jj) = s(toti+ii, totj+jj);
                    }
                }
            }
        }
    }

    return *this;
}


tensor & tensor::operator=(const Sparse & rhs) {
    clear();
    init();

    const sparsetensorpattern & stp = *rhs.P;

    if (rhs.values==NULL) return *this;

    //foreach atom
    for (uint32_t i=0; i<stp.natoms; ++i) {
        for (uint32_t j=0; j<stp.natoms; ++j) {

            uint16_t id1   = stp.ids[i];
            uint16_t id2   = stp.ids[j];

            const double * offset1 = rhs.values + stp.a2pos[i][j];

            bool init = false;

            for (int k=0; k<stp.alen[stp.ids[i]] * stp.alen[stp.ids[j]]; ++k) {
                init |= (fabs(offset1[k])>thresh);
            }

            if (!init) continue;

            c[i][j] = new tensor2;
            c[i][j]->setsize(pos[i],pos[j]);


            for (int b1=0; b1<stp.nfs[id1]; ++b1) {
                for (int b2=0; b2<stp.nfs[id2]; ++b2) {
                    const double * offset2 = offset1 + stp.f2pos[id1][id2][b1][b2];

                    uint32_t p2 = stp.f1pos[id1][b1];
                    uint32_t q2 = stp.f1pos[id2][b2];

                    uint32_t mj1 = stp.js[id1][b1];
                    uint32_t mj2 = stp.js[id2][b2];
                    uint32_t mm1 = stp.ms[id1][b1];
                    uint32_t mm2 = stp.ms[id2][b2];

                    for (uint8_t j1=0; j1<mj1; ++j1) {
                        for (uint8_t j2=0; j2<mj2; ++j2) {
                            const double * offset3 = offset2  +  (j1*mj2+j2)*(mm1*mm2);

                            uint32_t p3 = p2 + j1*mm1;
                            uint32_t q3 = q2 + j2*mm2;


                            for (uint8_t m1=0; m1<mm1; ++m1) {
                                for (uint8_t m2=0; m2<mm2; ++m2) {
                                    uint32_t Ap = Tpos(mm1,mm2, m1,m2);

                                    uint32_t ii = p3 + m1;
                                    uint32_t jj = q3 + m2;

                                    (*c[i][j])(ii,jj) = rhs(i,j, b1,b2, j1,j2)[Ap];
                                }
                            }

                        }
                    }

                }
            }


        }
    }

    return *this;
}



tensor & tensor::operator=(double rhs) {
    clear();
    init();
    for (int i=0; i<getn();++i) {
        touch(i,i);
        for (int ii=0; ii<pos[i]; ++ii) (*c[i][i])(ii,ii) = rhs;
    }
    return *this;
}

tensor & tensor::operator+=(const tensor & s) {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn();++i) {
        for (int j=0; j<getn();++j) {
            if (s.c[i][j] != NULL) {
                touch(i,j);
                *(c[i][j]) += *(s.c[i][j]);
            }
        }
    }
    return *this;
}

tensor & tensor::operator+=(double rhs) {
    for (int i=0; i<getn();++i) {
        touch(i,i);
        for (int ii=0; ii<pos[i]; ++ii) (*c[i][i])(ii,ii) += rhs;
    }
    return *this;
}

tensor & tensor::operator-=(const tensor & s) {
    //#pragma omp parallel for  schedule(dynamic)
    for (int i=0; i<getn();++i) {
        for (int j=0; j<getn();++j) {
            if (s.c[i][j] != NULL) {
                touch(i,j);
                *(c[i][j]) -= *(s.c[i][j]);
            }
        }
    }
    return *this;
}


// C = A * B
const tensor tensor::operator*(const tensor & s) const {
    tensor result;
    result.init();

    for (int k=0; k<getn(); ++k) {
        //#pragma omp parallel for
        for (int i=0; i<getn(); ++i) {
            const tensor2 * A = c[i][k];
            if (A==NULL) continue;
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[k][j];
                if (B==NULL) continue;

                tensor2 * C = result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[ii][kk]*B->c[kk][jj];
                        }
                        C->c[ii][jj] += sum;
                    }
                }

            }
        }
    }

    return result;
}

const tensor tensor::operator*(const symtensor & s) const {
    tensor result;
    result.init();

    //#pragma omp parallel for  schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<getn(); ++j) {

            //k<i
            for (int k=0; k<getn(); ++k) {
                //(i,j) <- (i,k)*(k,j)
                tensor2 * A = c[i][k];
                if (A==NULL) continue;
                tensor2 * B = s.c[k][j];
                if (B==NULL) continue;

                result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[ii][kk] * s(k,j,kk,jj);
                        }
                        result(i,j,ii,jj) += sum;
                    }
                }
            }
        }
    }
    return result;
}

// Cs = A+ * B
const symtensor tensor::operator/(const tensor & rhs) const {
    symtensor result;
    result.init();

    //#pragma omp parallel for  schedule(dynamic)
    for (int k=0; k<getn(); ++k) {
        for (int i=0; i<getn(); ++i) {
            const tensor2 * A = c[k][i];
            if (A==NULL) continue;

            for (int j=0; j<i; ++j) {
                const tensor2 * B = rhs.c[k][j];
                if (B==NULL) continue;

                result.touch(i,j);

                const tensor2 & C = *result.c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) sum += A->c[kk][ii]*B->c[kk][jj];
                        C.c[ii][jj] += sum;
                    }
                }
            }

            {
                int j=i;
                const tensor2 * B = rhs.c[k][j];
                if (B==NULL) continue;

                result.touch(i,j);

                const tensor2 & C = *result.c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) sum += A->c[kk][ii]*B->c[kk][jj];
                        C.c[ii][jj] += sum;
                    }
                }
            }


        }
    }

    return result;
}

// C = A+ * Bs
const tensor tensor::operator/(const symtensor & s) const {
    tensor result;
    result.init();

    //#pragma omp parallel for
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<getn(); ++j) {


            for (int k=j+1; k<getn(); ++k) {
                //(i,j) <- (i,k)*(k,j)
                tensor2 * A = c[k][i];
                if (A==NULL) continue;
                tensor2 * B = s.c[k][j];
                if (B==NULL) continue;

                result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[kk][ii] * B->c[kk][jj];
                        }
                        result(i,j,ii,jj) += sum;
                    }
                }
            }

            {
                int k=j;
                //(i,j) <- (i,k)*(k,j)
                tensor2 * A = c[k][i];
                if (A==NULL) continue;
                tensor2 * B = (k>=j)?s.c[k][j]:s.c[j][k];
                if (B==NULL) continue;

                result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[kk][ii] * s(k,j,kk,jj);
                        }
                        result(i,j,ii,jj) += sum;
                    }
                }
            }

            for (int k=0; k<j; ++k) {
                //(i,j) <- (i,k)*(k,j)
                tensor2 * A = c[k][i];
                if (A==NULL) continue;
                tensor2 * B = s.c[j][k];
                if (B==NULL) continue;

                result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[kk][ii] * B->c[jj][kk];
                        }
                        result(i,j,ii,jj) += sum;
                    }
                }
            }

        }
    }
    return result;
}

tensor::~tensor() {
    clear();
}

// ***************************************** //

symtensor::symtensor() {
    c  = NULL;
    c2 = NULL;
}

void symtensor::init() {

    if (c2==NULL) {
        c2 = new tensor2*[n2s];
        c  = new tensor2**[getn()];
        //asigna punteros
        int p2=0;

        for (int i=0; i<getn(); ++i) {
            c[i] = c2 + p2;
            p2 += i+1;
        }

        for (int i=0; i<getn(); ++i)
            for (int j=0; j<=i; ++j)
                c[i][j] = NULL;

        //c[i][j].setsize(pos[i],pos[j]);
        //SparsePattern = new Array<int>[n];
    }
}

void symtensor::init(const tensorpattern & TP) {

    // make a local hard copy
    // of TP's position data
    pos    = TP.pos;
    n2     = TP.n2;
    n2s    = TP.n2s;
    totn   = TP.totn;
    thresh = TP.thresh;

    init();
}

void symtensor::clear() {
    //delete[] SparsePattern;
    if (c2!=NULL)
        for (int i=0; i<getn(); ++i)
            for (int j=0; j<=i; ++j)
                delete c[i][j];
    delete[] c;
    delete[] c2;
    c2=NULL;
    c=NULL;
}

void symtensor::zeroize() {
    //cout << "p: " << c << endl;
    for (int i=0; i<getn(); ++i) {
        //cout << i << ":  " << c[i] << endl;
        for (int j=0; j<=i; ++j) {
            touch(i,j);

            if (c[i][j]!=NULL)
                c[i][j]->zeroize();
        }
        //cout << endl;
    }
}

void symtensor::trim() {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<=i; ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                bool inc = false;

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        if ( fabs(C->c[ii][jj]) >= thresh ) inc = true;
                    }
                }

                if (!inc) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}

void symtensor::trim(double th) {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<=i; ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                bool inc = false;

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        if ( fabs(C->c[ii][jj]) >= th) inc = true;
                    }
                }

                if (!inc) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}

void symtensor::trim(const symtensor & s) {
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<=i; ++j) {
            const tensor2 * C = c[i][j];

            if (C!=NULL) {
                bool inc = false;

                if (s.c[i][j]==NULL) {
                    delete c[i][j];
                    c[i][j] = NULL;
                }
            }
        }
    }
}




tensor2 * symtensor::touch(int i, int j) {
    if (i>=j) {
        if (c[i][j]==NULL) {
            c[i][j] = new tensor2;
            c[i][j]->setsize(pos[i],pos[j]);
            c[i][j]->zeroize();
        }
        return c[i][j];
    }
    else {
        if (c[j][i]==NULL) {
            c[j][i] = new tensor2;
            c[j][i]->setsize(pos[j],pos[i]);
            c[j][i]->zeroize();
        }
        return c[j][i];
    }
}


symtensor & symtensor::operator=(const symtensor & s) {
    clear();
    init();

    for (int i=0;i<getn();++i) {
        for (int j=0;j<=i;++j) {
            if (s.c[i][j]!=NULL) {
                tensor2 * T = touch(i,j);
                (*T) = (*s.c[i][j]);
            }
        }
    }

    return *this;
}

symtensor & symtensor::operator=(const tensor & s) {
    clear();
    init();

    for (int i=0;i<getn();++i) {
        for (int j=0;j<=i;++j) {
            if (s.c[i][j]!=NULL) {
                if (c[i][j]==NULL) touch(i,j);
                *c[i][j] = *s.c[i][j];
            }
        }
    }

    return *this;
}

symtensor & symtensor::operator=(const symtensor2 & s) {
    clear();
    init();

    for (int i=0, toti=0;i<getn(); toti+=pos[i], ++i) {
        for (int j=0, totj=0; j<i; totj+=pos[j], ++j) {

            /*
            bool inc = false;
            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<pos[j]; ++jj) {
                    if (fabs(s(toti+ii, totj+jj))>thresh) inc = true;
                }
            }

            if (!inc) continue;
            */

            touch(i,j);

            tensor2 & C = *c[i][j];

            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<pos[j]; ++jj) {
                    C.c[ii][jj] = s(toti+ii, totj+jj);
                }
            }
        }
        {
            touch(i,i);

            tensor2 & C = *c[i][i];

            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<=ii; ++jj) {
                    C.c[ii][jj] = s(toti+ii, toti+jj);
                }
            }
        }
    }
    return *this;
}


symtensor & symtensor::operator=(const tensor2 & s) {
    clear();
    init();

    for (int i=0, toti=0;i<getn(); toti+=pos[i], ++i) {
        for (int j=0, totj=0; j<=i; totj+=pos[j], ++j) {

            /*
            bool inc = false;
            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<pos[j]; ++jj) {
                    if (fabs(s(toti+ii, totj+jj))>thresh) inc = true;
                }
            }

            if (!inc) continue;
            */

            touch(i,j);

            tensor2 & C = *c[i][j];

            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<pos[j]; ++jj) {
                    C.c[ii][jj] = s(toti+ii, totj+jj);
                }
            }
        }
        {
            touch(i,i);

            tensor2 & C = *c[i][i];

            for (int ii=0; ii<pos[i]; ++ii) {
                for (int jj=0; jj<=ii; ++jj) {
                    C.c[ii][jj] = s(toti+ii, toti+jj);
                }
            }
        }
    }
    return *this;
}


symtensor & symtensor::operator=(double rhs) {
    clear();
    init();

    for (int i=0; i<getn();++i) {
        touch(i,i);
        for (int ii=0; ii<pos[i]; ++ii) (*c[i][i])(ii,ii) = rhs;
    }
    return *this;
}

symtensor & symtensor::operator+=(const symtensor & st) {
    //#pragma omp parallel for  schedule(dynamic)
    for (int i=0; i<getn();++i) {
        for (int j=0; j<=i;++j) {
            if (st.c[i][j] != NULL) {
                touch(i,j);
                *(c[i][j]) += *(st.c[i][j]);
            }
        }
    }
    return *this;
}

symtensor & symtensor::operator+=(const tensor & st) {
    //#pragma omp parallel for  schedule(dynamic)
    for (int i=0; i<getn();++i) {
        for (int j=0; j<i;++j) {
            const tensor2 * T = st.c[i][j];
            if (T != NULL) {
                tensor2 * S = touch(i,j);

                for (int ii=0; ii<T->n; ++ii)
                    for (int jj=0; jj<T->m; ++jj)
                        (*S)(ii,jj) += (*T)(ii,jj);
            }
        }

        {
            /*
            const tensor2 * T = st.c[i][i];
            if (T != NULL) {
                tensor2 * S = touch(i,i);

                for (int ii=0; ii<T->n; ++ii)
                    for (int jj=0; jj<=ii; ++jj)
                        (*S)(ii,jj) += (*T)(ii,jj);
            }
            */

            const tensor2 * T = st.c[i][i];
            if (T != NULL) {
                tensor2 * S = touch(i,i);

                for (int ii=0; ii<T->n; ++ii)
                    for (int jj=0; jj<=ii; ++jj)
                        (*S)(ii,jj) += (*T)(ii,jj);

                for (int ii=0; ii<T->n; ++ii)
                    for (int jj=0; jj<=ii; ++jj)
                        (*S)(ii,jj) += (*T)(jj,ii);
            }
        }

        for (int j=i+1; j<getn();++j) {
            const tensor2 * T = st.c[i][j];

            if (T != NULL) {
                tensor2 * S = touch(j,i);

                for (int ii=0; ii<T->n; ++ii)
                    for (int jj=0; jj<T->m; ++jj)
                        (*S)(jj,ii) += (*T)(ii,jj);
            }
        }

    }


    return *this;
}



symtensor & symtensor::operator+=(double rhs) {
    for (int i=0; i<getn();++i) {
        tensor2 * T = touch(i,i);
        for (int ii=0; ii<pos[i]; ++ii) (*T)(ii,ii) += rhs;
    }
    return *this;
}

symtensor & symtensor::operator-=(const symtensor & rhs) {
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<getn();++i) {
        for (int j=0; j<=i;++j) {
            if (rhs.c[i][j] != NULL) {
                tensor2 * T = touch(i,j);
                *T -= *rhs.c[i][j];
            }
        }
    }
    return *this;
}


const tensor symtensor::operator*(const tensor & s) const {
    tensor result;
    result.init();

    //

    for (int k=0; k<getn(); ++k) {

        for (int i=k+1; i<getn(); ++i) {
            const tensor2 * A = c[i][k];
            if (A==NULL) continue;

            //#pragma omp parallel for schedule(dynamic)
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[k][j];
                if (B==NULL) continue;

                result.touch(i,j);

                const tensor2 & C = *result.c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[ii][kk]*B->c[kk][jj];
                        }
                        C.c[ii][jj] += sum;
                    }
                }

            }
        }


        const tensor2 * A = c[k][k];
        if (A!=NULL)
        {
            //#pragma omp parallel for schedule(dynamic)
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[k][j];
                if (B==NULL) continue;

                result.touch(k,j);

                const tensor2 & C = *result.c[k][j];

                for (int ii=0; ii<pos[k]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<=ii; ++kk)
                             sum += A->c[ii][kk]*B->c[kk][jj];
                        for (int kk=ii+1; kk<pos[k]; ++kk)
                             sum += A->c[kk][ii]*B->c[kk][jj];
                        C.c[ii][jj] += sum;
                    }
                }

            }
        }

        for (int i=0; i<k; ++i) {
            const tensor2 * A = c[k][i];
            if (A==NULL) continue;

            //#pragma omp parallel for schedule(dynamic)
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[k][j];
                if (B==NULL) continue;

                result.touch(i,j);

                const tensor2 & C = *result.c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[kk][ii]*B->c[kk][jj];
                        }
                        C.c[ii][jj] += sum;
                    }
                }

            }
        }
    }

    return result;
}

const symtensor symtensor::operator*(const symtensor & s) const {
    symtensor result;
    result.init();

    #pragma omp parallel for  schedule(dynamic)
    for (int i=0; i<getn(); ++i) {
        for (int j=0; j<i; ++j) {

            //k<i
            for (int k=0; k<getn(); ++k) {
                //(i,j) <- (i,k)*(k,j)
                tensor2 * A = (i>=k)?c[i][k]:c[k][i];
                if (A==NULL) continue;
                tensor2 * B = (k>=j)?s.c[k][j]:s.c[j][k];
                if (B==NULL) continue;

                result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += (*this)(i,k,ii,kk) * s(k,j,kk,jj);
                        }
                        result(i,j,ii,jj) += sum;
                    }
                }
            }

        }

        {
            int j=i;
            for (int k=0; k<getn(); ++k) {

                //(i,j) <- (i,k)*(k,j)
                tensor2 * A = (i>=k)?c[i][k]:c[k][i];
                if (A==NULL) continue;
                tensor2 * B = (k>=j)?s.c[k][j]:s.c[j][k];
                if (B==NULL) continue;

                result.touch(i,j);

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += (*this)(i,k,ii,kk) * s(k,j,kk,jj);
                        }
                        result(i,j,ii,jj) += sum;
                    }
                }

            }
        }
    }



    return result;
}

//R = S * M+
const tensor symtensor::operator/(const tensor & s) const {
    tensor result;
    result.init();

    for (int k=0; k<getn(); ++k) {
        for (int i=k+1; i<getn(); ++i) {
            const tensor2 * A = c[i][k];
            if (A==NULL) continue;
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[j][k];
                if (B==NULL) continue;

                result.touch(i,j);

                const tensor2 & C = *result.c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[ii][kk]*B->c[jj][kk];
                        }
                        C.c[ii][jj] += sum;
                    }
                }

            }
        }

        {
            const tensor2 * A = c[k][k];
            if (A==NULL) continue;
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[j][k];
                if (B==NULL) continue;

                result.touch(k,j);

                const tensor2 & C = *result.c[k][j];

                for (int ii=0; ii<pos[k]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<=ii; ++kk)
                             sum += A->c[ii][kk]*B->c[jj][kk];
                        for (int kk=ii+1; kk<pos[k]; ++kk)
                             sum += A->c[kk][ii]*B->c[jj][kk];
                        C.c[ii][jj] += sum;
                    }
                }

            }
        }

        for (int i=0; i<k; ++i) {
            const tensor2 * A = c[k][i];
            if (A==NULL) continue;
            for (int j=0; j<getn(); ++j) {
                const tensor2 * B = s.c[j][k];
                if (B==NULL) continue;

                result.touch(i,j);

                const tensor2 & C = *result.c[i][j];

                for (int ii=0; ii<pos[i]; ++ii) {
                    for (int jj=0; jj<pos[j]; ++jj) {
                        double sum=0;
                        for (int kk=0; kk<pos[k]; ++kk) {
                             sum += A->c[kk][ii]*B->c[jj][kk];
                        }
                        C.c[ii][jj] += sum;
                    }
                }

            }
        }
    }

    return result;
}

symtensor::~symtensor() {
    clear();
}

const symtensor BlockedProjection(const tensor & C, const symtensor & S) {
    tensor xS;

    symtensor xSx;
    xS = C/S;
    xSx = xS*C;
    xSx.trim();

    return xSx;
}


tensor2 & tensor2::operator=(const tensor & st) {
    if (n!=st.totn) {
        if (n>0)
            clear();
        setsize(st.totn);
    }

    zeroize();

    for (int i=0, toti=0;i<st.getn(); toti+=st.pos[i], ++i) {
        for (int j=0, totj=0; j<st.getn(); totj+=st.pos[j],++j) {

            tensor2 * C = st.c[i][j];

            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<st.pos[j]; ++jj) {
                        c[toti+ii][totj+jj] = C->c[ii][jj];
                    }
                }
            }
        }
    }
    return *this;
}

tensor2 & tensor2::operator=(const symtensor & st) {

    setsize(st.totn);
    zeroize();

    for (int i=0, toti=0;i<st.getn(); toti+=st.pos[i], ++i) {
        //i>j
        for (int j=0, totj=0; j<i; totj+=st.pos[j], ++j) {

            tensor2 * C = st.c[i][j];


            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<st.pos[j]; ++jj) {
                        c[toti+ii][totj+jj] = C->c[ii][jj];
                    }
                }
            }
        }
        //i==j
        {
            tensor2 * C = st.c[i][i];


            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        c[toti+ii][toti+jj] = C->c[ii][jj];
                    }
                }
            }
        }
    }

    Symmetrize();
    for (int i=0; i<n; ++i) c[i][i] *= 0.5; // notice that the diagonal is twice its value after the call to Symmetrize()

    return *this;
}

symtensor2 & symtensor2::operator=(const symtensor & st) {

    if (n!=st.totn) {
        setsize(st.totn);
    }

    zeroize();

    for (int i=0, toti=0;i<st.getn(); toti+=st.pos[i], ++i) {
        //i>j
        for (int j=0, totj=0; j<i; totj+=st.pos[j], ++j) {

            tensor2 * C = st.c[i][j];


            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<st.pos[j]; ++jj) {
                        c[toti+ii][totj+jj] = C->c[ii][jj];
                    }
                }
            }
        }
        //i==j
        {
            tensor2 * C = st.c[i][i];


            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        c[toti+ii][toti+jj] = C->c[ii][jj];
                    }
                }
            }
        }
    }

    return *this;
}

symtensor2 & symtensor2::operator+=(const symtensor & st) {

    //#pragma omp parallel for
    for (int i=0, toti=0;i<st.getn(); toti+=st.pos[i], ++i) {
        //i>j
        for (int j=0, totj=0; j<i; totj+=st.pos[j], ++j) {

            tensor2 * C = st.c[i][j];

            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<st.pos[j]; ++jj) {
                        c[toti+ii][totj+jj] += C->c[ii][jj];
                    }
                }
            }
        }
        //i==j
        {
            tensor2 * C = st.c[i][i];

            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        c[toti+ii][toti+jj] += C->c[ii][jj];
                    }
                }
            }
        }
    }

    return *this;
}

//suma el triangulo inferior y el superior transpuesto
symtensor2 & symtensor2::operator+=(const tensor & st) {

    //#pragma omp parallel for
    for (int i=0, toti=0;i<st.getn(); toti+=st.pos[i], ++i) {
        //i>j
        int totj=0;

        for (int j=0; j<i; totj+=st.pos[j], ++j) {

            tensor2 * C = st.c[i][j];

            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<st.pos[j]; ++jj) {
                        c[toti+ii][totj+jj] += C->c[ii][jj];
                    }
                }
            }
        }
        //i==j
        {
            tensor2 * C = st.c[i][i];

            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<=ii; ++jj) {
                        c[toti+ii][toti+jj] += C->c[ii][jj];
                    }
                    for (int jj=ii+1; jj<st.pos[i]; ++jj) {
                        c[toti+jj][toti+ii] += C->c[jj][ii];
                    }
                }
            }
        }

        for (int j=i+1; j<st.getn(); totj+=st.pos[j], ++j) {

            tensor2 * C = st.c[i][j];

            if (C!=NULL) {
                for (int ii=0; ii<st.pos[i]; ++ii) {
                    for (int jj=0; jj<st.pos[j]; ++jj) {
                        c[totj+jj][toti+ii] += C->c[ii][jj];
                    }
                }
            }
        }
    }

    return *this;
}


