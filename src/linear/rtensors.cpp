#include <iostream>
using namespace std;

#include <inttypes.h>

// Knuth's multiplicative method:
uint32_t hash (uint32_t a, uint32_t  b) {

    uint64_t h = a*2654435761 + b;

    return h; // mod 2^32
}

static const int MXL = 8;



// implement assignment to array
// and/or stream operators for insertion/extraction

class Index {

    uint16_t idx[MXL]; // let's assume a maximum of 8 indirection levels at the moment

  protected:


  public:

    // I have to for the moment

    Index() {
        // only for Iterators
        for (int i=0; i<MXL; ++i) idx[i]=0;
    }


    // create an index object
    // with n linear dimensions
    Index (int n) {
        if (n>=MXL-1) throw "too many indices";

        // set everything to invalid handle
        for (int i=0; i<MXL; ++i) idx[i]=0;
        // n indices used
        idx[MXL-1] = n;
    }

    Index & operator*= (const Index & rhs) {

        int n = idx[MXL-1];
        int m = rhs.idx[MXL-1];

        if (n+m>=MXL-1) throw "too many indices";

        idx[MXL-1] += m;

        for (int o=0; o<m; ++o) idx[n+o] = idx[o];

        return *this;
    }

    Index operator* (const Index & rhs) const {
        Index ret(*this);
        ret *= rhs;
        return ret;
    }

    const Index & operator=(const int v[]) {
        int n = sizeof(v)/sizeof(int);
        idx[MXL-1] = n;

        for (int i=0; i<n; ++i) idx[i] = v[i];

        return *this;
    }

    virtual int GetDepth() const {return idx[MXL-1];}

    virtual uint16_t operator[](int i) const {
        if (i<0 || i>=MXL-1) throw "out of bounds";
        return idx[i];
    }

    virtual uint16_t & operator[](int i) {
        if (i<0 || i>=MXL-1) throw "out of bounds";
        return idx[i];
    }

};

ostream & operator<<(ostream & os, const Index & I) {
    os << "(";
    int N = I.GetDepth();
    for (int n=0; n<N; ++n)
        os << I[n];
    os << ")";
    return os;
}


class IndexLayout {

    IndexLayout ** SL;  // array of pointers
    uint32_t ID;  // unique hash
    uint32_t DIM; // dimension
    uint16_t NSB; // number of subblocks

    void SetN (int N) {
        NSB = N;
        SL = new IndexLayout*[NSB];
        for (int n=0; n<NSB; ++n) SL[n] = NULL;
        ID = 0;
    }

    int GetFreeN() {
        // FIXME: get first free n (terrible complexity)
        for (int n=0; n<NSB; ++n) {if (SL[n]==NULL) return n;}

        IndexLayout ** SL2 = new IndexLayout*[NSB+1];
        for (int n=0; n<NSB; ++n) SL2[n] = SL[n]; SL2[NSB] = NULL;
        delete[] SL;
        SL = SL2;
        ++NSB;
        return NSB-1;
    }

    // interface

  public:

    IndexLayout () {
        SL = NULL;
        ID = 0;
        DIM = 0;
        NSB = 0;
    }

    //this is a copy constructor
    IndexLayout (const IndexLayout & rhs) {

        SL  = NULL;
        ID  = rhs.ID;
        DIM = rhs.DIM;
        NSB = rhs.NSB;

        if (NSB>0) {
            SL = new IndexLayout*[NSB];
            for (int n=0; n<NSB; ++n) SL[n] = new IndexLayout(*rhs.SL[n]);
        }
    }

    IndexLayout (int N) {
        SL = NULL;
        DIM = N;
        ID = hash(DIM, 0);
        NSB = 0;
    }

    bool IsLeaf() const {
        return (NSB==0 && DIM>0);
    }


    IndexLayout & operator << (const IndexLayout & SSL) {
        int n = GetFreeN();
        SL[n] = new IndexLayout;
        *(SL[n]) = SSL;
        DIM += SL[n]->DIM;
        ID = hash(SL[n]->ID, ID);
        return *this;
    }

    IndexLayout operator & (const IndexLayout & rhs) {
        IndexLayout ret;

        ret.SetN (NSB+1); // reserve all subblocks
        for (int n=0; n<NSB; ++n) ret << *SL[n];
        ret << rhs;

        return ret;
    }

    IndexLayout operator*(int N) const {
        IndexLayout ret;
        ret.SetN(N); // reserve N subblocks
        for (int n=0; n<N; ++n) ret << *this;
        return ret;
    }

    IndexLayout operator+(const IndexLayout & rhs) const {
        IndexLayout ret;

        ret.SetN (NSB+rhs.NSB); // reserve all subblocks
        for (int n=0; n<NSB; ++n) ret << *SL[n];
        for (int n=0; n<rhs.NSB; ++n) ret << *rhs.SL[n];

        return ret;
    }

    int GetOffset(int i) const {
        if (IsLeaf()) return i;

        int os = 0;
        for (int a=0; a<i; ++a)
            os += SL[a]->GetDim();
        return os;
    }

    int GetIndex(int i, int * idx) const {

        if (IsLeaf()) {
            *idx = i;
            return 1;
        }

        int a=0;
        while (i >= SL[a]->GetDim()) {
            i -= SL[a]->GetDim();
            ++a;
        }
       *idx = a; // set the n-th index

        // do it recursively
        return 1+SL[a]->GetIndex(i, idx+1);
    }

    Index GetIndex(int i) const {

        int idx[MXL];
        for (int j=0; j<MXL; ++j) idx[j] = 0;

        int n = GetIndex(i, idx);

        Index I(n);
        for (int j=0; j<n; ++j) I[j] = idx[j];

        return I;
    }

    uint32_t GetID()  const {return ID;}
    uint32_t GetDim() const {return DIM;}
    uint32_t GetNSB() const {return IsLeaf()?DIM:NSB;}

    const IndexLayout & operator[](int n) const {
        return *(SL[n]);
    }

    const IndexLayout & operator() (const Index & I, int o=0) const {
        if (o==I.GetDepth()-1) return *(SL[ I[o] ]);
        return (*(SL[I[o]])) (I, o+1);
    }


    // collapse highest level of indirection
    IndexLayout Collapse(int L=0) const {

        if (IsLeaf()) return *this; // can't collapse any further

        IndexLayout ret;

        if (L>0) {
            ret.SetN (NSB);

            for (int n=0; n<NSB; ++n) {
                const IndexLayout & SSL = *(SL[n]);
                ret << SSL.Collapse(L-1);
            }
        }
        else {
            int TN=0;
            for (int n=0; n<NSB; ++n) TN += SL[n]->GetNSB();

            ret.SetN (TN); // reserve all subblocks

            for (int n=0; n<NSB; ++n) {

                const IndexLayout & SSL = *(SL[n]);

                if (SSL.IsLeaf())
                    ret << SSL;
                else
                    for (int m=0; m<SSL.GetNSB(); ++m) {
                        ret << SSL[m];
                    }
            }
        }

        return ret;
    }

};

ostream & operator<<(ostream & os, const IndexLayout & SL) {

    if (SL.IsLeaf()) os << SL.GetDim();
    else {
        os << "(";
        int N = SL.GetNSB();
        for (int n=0; n<N; ++n)
            os << SL[n];
        os << ")";
    }
    return os;
}



template <int R>
class SparseLayout() {

    IndexLayout ** IL;

  public:

};


class Iterator : public Index {

    uint16_t idx[MXL]; // let's assume a maximum of 8 indirection levels at the moment

    //these are only needed for iteration
    const IndexLayout * SL[MXL];
    uint8_t NL; // number of layers
    uint8_t ML; // nested recursion cutoff

    // fix SL pointers starting from rank l0
    void SetRankSL(int l0=0) {

        int l;
        const IndexLayout * pSL = SL[l0];

        for (l=l0; l<ML; ++l) {
            SL[l] = pSL;
            if (pSL->IsLeaf()) break;
            int i = idx[l];
            pSL = &( (*pSL)[i]); // commit next index
        }

        NL = l+1;
    }

  public:

    Iterator (const IndexLayout & SSL, uint8_t ml=MXL) {

        // max recursion
        ML = ml;

        // set everything to 0
        // only for Iterators
        for (int i=0; i<MXL; ++i) idx[i]=0;
        for (int i=0; i<MXL; ++i) SL[i] = NULL;

        // except the first SL pointer, which determines the bounds
        SL[0] = &SSL;

        // fix SL pointers
        SetRankSL();
    }

    Iterator & operator++() {

        int l = min(NL,ML)-1;

        while (1) {
            ++idx[l];
            if (idx[l]<SL[l]->GetNSB()) break;
            if (l==0) break;
            idx[l] = 0; // wrap around
            --l;
        }

        if (l>0) SetRankSL();

        return *this;
    }

    bool end() const {
        return (idx[0] == SL[0]->GetNSB());
    }

    // return the depth of recursion
    int GetDepth() const {return min(NL,ML);}

    // return the i-th index
    uint16_t operator[](int i) const {return idx[i];}

    // return an iterator spanning the subtree
    // tangent to the current position
    Iterator Span() const {
        // last but one
        const IndexLayout * SSL = SL[ML-1];
        const IndexLayout & RSL = (*SSL)[idx[ML-1]];
        Iterator I(RSL);
        return I;
    }
};


// pure virtual class
class rTensor {

};

// just some trial
class rTensor1 {

    double * v;
    const IndexLayout * SL;

    int offset1(const Index & I) const {

        int os = 0;
        const IndexLayout * SSL = SL;

        // accumulate the memory offset for each index
        for (int n=0; n<I.GetDepth(); ++n) {
            int i = I[n];

            // end of recursive structure
            if (SSL->IsLeaf()) {
                os += i;
                break;
            }

            // add the offset of all previous blocks of this level
            // FIXME: should actually be precomputed and stored
            for (int a=0; a<i; ++a)
                os += (*SSL)[a].GetDim();

            SSL = &( (*SSL)[i]);
        }

        return os;
    }

    // to build sub-block tensors
    rTensor1 () {
        SL = NULL;
        v  = NULL;
    }

    void Clear() {
        SL = NULL;
        v  = NULL;
    }

  public:

    rTensor1 (const IndexLayout & SSL) {
        SL = &SSL;
        v = new double[SL->GetDim()];
    }

   ~rTensor1 () {
        delete[] v;
    }

    rTensor1 (const rTensor1 & rhs) {
        SL = rhs.SL;
        int N = SL->GetDim();
        v = new double[N];
        for (int n=0; n<N; ++n) v[n] = rhs.v[n];
    }

    // accessors

    double operator[] (const Index & I) const {
        return v[offset1(I)];
    }

    double & operator[] (const Index & I) {
        return v[offset1(I)];
    }

    // incomplete index
    rTensor1 operator() (const Index & I) const {
        rTensor1 T;
        T.v  = v + offset1(I);
        T.SL = &((*SL) (I) );
        return T;
    }

};

class rTensor2 {
    double * v;
    const IndexLayout * SL1;
    const IndexLayout * SL2;

    int offset2(const Index & I, const Index & J) const {

        if (I.GetDepth()!=J.GetDepth()) throw "indices have incompatible shapes";

        int os = 0;
        const IndexLayout * SSL1 = SL1;
        const IndexLayout * SSL2 = SL2;

        // accumulate the memory offset for each index
        for (int l=0; l<I.GetDepth(); ++l) {

            // n-th 2-indices
            int i = I[l];
            int j = J[l];
            int N = SSL1->GetDim();
            int M = SSL2->GetDim();

            // end of recursive structure
            if (SSL1->IsLeaf()) {
                os += i*M+j;
                break;
            }

            // add the offset of all previous blocks of this level
            // FIXME: should actually be precomputed and stored
            int ios = SSL1->GetOffset(i);
            int jos = SSL2->GetOffset(j);

            os += ios*M+jos;

            // move to next level
            SSL1 = &( (*SSL1)[i]);
            SSL2 = &( (*SSL2)[j]);
        }

        return os;
    }

    // to build sub-block tensors
    rTensor2 () {
        SL1 = SL2 = NULL;
        v  = NULL;
    }

    void Clear() {
        SL1 = SL2 = NULL;
        v  = NULL;
    }

  public:

    rTensor2 (const IndexLayout & SSL1, const IndexLayout & SSL2) {
        SL1 = &SSL1;
        SL2 = &SSL2;
        v = new double[SL1->GetDim()*SL2->GetDim()];
    }

   ~rTensor2 () {
        delete[] v;
    }

    rTensor2 (const rTensor2 & rhs) {
        SL1 = rhs.SL1;
        SL2 = rhs.SL2;
        int N = SL1->GetDim();
        int M = SL2->GetDim();
        v = new double[N*M];
        for (int n=0; n<N*M; ++n) v[n] = rhs.v[n];
    }

    // accessors

    // complete indices
    /*
    double operator() (const Index & I, const Index & J) const {
        return v[offset2(I,J)];
    }

    double & operator() (const Index & I, const Index & J) {
        return v[offset2(I,J)];
    }
    */

    // incomplete indices
    rTensor2 operator() (const Index & I, const Index & J) const {
        rTensor2 T;
        T.v  = v + offset2(I,J);
        T.SL1 = &((*SL1) (I) );
        T.SL2 = &((*SL2) (J) );
        return T;
    }

};


class rTensor3 {
    double * v;
    const IndexLayout * SL1;
    const IndexLayout * SL2;
    const IndexLayout * SL3;

    int offset3(const Index & I, const Index & J, const Index & K) const {

        int depth = I.GetDepth();

        if (depth!=J.GetDepth() || depth!=K.GetDepth()) throw "indices have incompatible shapes";

        int os = 0;
        const IndexLayout * SSL1 = SL1;
        const IndexLayout * SSL2 = SL2;
        const IndexLayout * SSL3 = SL3;

        // accumulate the memory offset for each index
        for (int d=0; d<depth; ++d) {

            // n-th 3-indices
            int i = I[d];
            int j = J[d];
            int k = K[d];
            int N = SSL1->GetDim();
            int M = SSL2->GetDim();
            int L = SSL3->GetDim();

            // end of recursive structure
            if (SSL1->IsLeaf()) {
                os += i*M*L + j*L + k;
                break;
            }

            // add the offset of all previous blocks of this level
            // FIXME: should actually be precomputed and stored
            int ios = SSL1->GetOffset(i);
            int jos = SSL2->GetOffset(j);
            int kos = SSL3->GetOffset(k);

            os += ios*M*L + jos*L + kos;

            // move to next level
            SSL1 = &( (*SSL1)[i]);
            SSL2 = &( (*SSL2)[j]);
            SSL3 = &( (*SSL3)[k]);
        }

        return os;
    }

    // to build sub-block tensors
    rTensor3 () {
        SL1 = SL2 = SL3 = NULL;
        v  = NULL;
    }

    void Clear() {
        SL1 = SL2 = SL3 = NULL;
        v  = NULL;
    }

  public:

    rTensor3 (const IndexLayout & SSL1, const IndexLayout & SSL2, const IndexLayout & SSL3) {
        SL1 = &SSL1;
        SL2 = &SSL2;
        SL3 = &SSL3;
        v = new double[SL1->GetDim()*SL2->GetDim()*SL3->GetDim()];
    }

   ~rTensor3 () {
        delete[] v;
    }

    rTensor3 (const rTensor3 & rhs) {
        SL1 = rhs.SL1;
        SL2 = rhs.SL2;
        SL3 = rhs.SL3;
        int N = SL1->GetDim();
        int M = SL2->GetDim();
        int L = SL3->GetDim();
        v = new double[N*M*L];
        for (int n=0; n<N*M*L; ++n) v[n] = rhs.v[n];
    }

    // accessors

    // complete indices
    /*
    double operator() (const Index & I, const Index & J, const Index & K) const {
        return v[offset3(I,J,K)];
    }

    double & operator() (const Index & I, const Index & J, const Index & K) {
        return v[offset3(I,J,K)];
    }
    */

    // incomplete indices (should cast to double if they are complete)
    rTensor3 operator() (const Index & I, const Index & J, const Index & K) const {
        rTensor3 T;
        T.v  = v + offset3(I,J,K);
        T.SL1 = &((*SL1) (I) );
        T.SL2 = &((*SL2) (J) );
        T.SL3 = &((*SL3) (K) );
        return T;
    }

};

class rTensor4 {

    double * v;
    const IndexLayout * SL1;
    const IndexLayout * SL2;
    const IndexLayout * SL3;
    const IndexLayout * SL4;

    int offset4(const Index & I, const Index & J, const Index & K, const Index & L) const {

        int depth = I.GetDepth();

        if (depth!=J.GetDepth() || depth!=K.GetDepth() || depth!=L.GetDepth()) throw "indices have incompatible shapes";

        int os = 0;
        const IndexLayout * SSL1 = SL1;
        const IndexLayout * SSL2 = SL2;
        const IndexLayout * SSL3 = SL3;
        const IndexLayout * SSL4 = SL4;

        // accumulate the memory offset for each index
        for (int d=0; d<depth; ++d) {

            // n-th 4-indices
            int i = I[d];
            int j = J[d];
            int k = K[d];
            int l = L[d];
            int P = SSL1->GetDim();
            int Q = SSL2->GetDim();
            int R = SSL3->GetDim();
            int S = SSL4->GetDim();

            // end of recursive structure
            if (SSL1->IsLeaf()) {
                os += i*Q*R*S + j*R*S + k*S + l;
                break;
            }

            // add the offset of all previous blocks of this level
            // FIXME: should actually be precomputed and stored
            int ios = SSL1->GetOffset(i);
            int jos = SSL2->GetOffset(j);
            int kos = SSL3->GetOffset(k);
            int los = SSL4->GetOffset(l);

            os += ios*Q*R*S + jos*R*S + kos*S + los;

            // move to next level
            SSL1 = &( (*SSL1)[i]);
            SSL2 = &( (*SSL2)[j]);
            SSL3 = &( (*SSL3)[k]);
            SSL4 = &( (*SSL4)[l]);
        }

        return os;
    }

    int offset4(int i, int j, int k, int l) {
        Index I = SL1->GetIndex(i);
        Index J = SL2->GetIndex(j);
        Index K = SL3->GetIndex(k);
        Index L = SL4->GetIndex(l);

        return offset4(I,J,K,L);
    }


    // to build sub-block tensors
    rTensor4 () {
        SL1 = SL2 = SL3 = SL4 = NULL;
        v  = NULL;
    }

    void Clear() {
        SL1 = SL2 = SL3 = SL4 = NULL;
        v  = NULL;
    }

  public:

    rTensor4 (const IndexLayout & SSL1, const IndexLayout & SSL2, const IndexLayout & SSL3, const IndexLayout & SSL4) {
        SL1 = &SSL1;
        SL2 = &SSL2;
        SL3 = &SSL3;
        SL4 = &SSL4;
        v = new double[SL1->GetDim()*SL2->GetDim()*SL3->GetDim()*SL4->GetDim()];
    }

   ~rTensor4 () {
        delete[] v;
    }

    rTensor4 (const rTensor4 & rhs) {
        SL1 = rhs.SL1;
        SL2 = rhs.SL2;
        SL3 = rhs.SL3;
        SL4 = rhs.SL4;
        int P = SL1->GetDim();
        int Q = SL2->GetDim();
        int R = SL3->GetDim();
        int S = SL4->GetDim();
        v = new double[P*Q*R*S];
        for (int n=0; n<P*Q*R*S; ++n) v[n] = rhs.v[n];
    }

    // accessors

    // complete indices
    /*
    double operator() (const Index & I, const Index & J, const Index & K, const Index & L) const {
        return v[offset4(I,J,K,L)];
    }

    double & operator() (const Index & I, const Index & J, const Index & K, const Index & L) {
        return v[offset4(I,J,K,L)];
    }
    */

    double operator() (int i, int j, int k, int l) const {
        return v[offset4(i,j,k,l)];
    }

    double & operator() (int i, int j, int k, int l) {
        return v[offset4(i,j,k,l)];
    }

    // incomplete indices (should cast to double if they are complete)
    rTensor4 operator() (const Index & I, const Index & J, const Index & K, const Index & L) const {
        rTensor4 T;
        T.v  = v + offset4(I,J,K,L);
        T.SL1 = &((*SL1) (I) );
        T.SL2 = &((*SL2) (J) );
        T.SL3 = &((*SL3) (K) );
        T.SL4 = &((*SL4) (L) );
        return T;
    }

    // cast to double (i.e. when the accessing indices contain the full coordinates of the element)
    operator double () const {
        return *v;
    }

};




template <int R>
class rTensorR {

    double * v;
    const IndexLayout * SL[R];


    int offset(const Index I[R]) const {

        int depth = I[0].GetDepth();

        //if (depth!=J.GetDepth() || depth!=K.GetDepth() || depth!=L.GetDepth()) throw "indices have incompatible shapes";

        int os = 0;

        const IndexLayout * SSL[R];
        for (int r=0; r<R; ++r) SSL[r] = SL[r];


        // accumulate the memory offset for each index
        for (int d=0; d<depth; ++d) {

            int ii[R];
            for (int r=0; r<R; ++r) ii[r] = (I[r])[d];

            int PP[R];
            for (int r=0; r<R; ++r) PP[r] = SSL[r]->GetDim();

            // end of recursive structure
            if (SSL[0]->IsLeaf()) {

                int Aos = ii[0];
                for (int r=1; r<R; ++r) Aos = Aos*PP[r] + ii[r];

                os += Aos;
                break;
            }

            // add the offset of all previous blocks of this level
            // FIXME: should actually be precomputed and stored
            int ios[R];
            for (int r=0; r<R; ++r) ios[r] = SSL[r]->GetOffset(ii[r]);

            int Aos = ios[0];
            for (int r=1; r<R; ++r) Aos = Aos*PP[r] + ios[r];

            os += Aos;

            // move to next level
            for (int r=0; r<R; ++r) SSL[r] = &( (*SSL[r])[ii[r]]);
        }

        return os;
    }

    int offset(int i[R]) const {
        Index I[R];
        for (int r=0; r<R; ++r) {
            I[r] =  SL[r]->GetIndex(i[r]);
        }
        return offset(I);
    }


    // to build sub-block tensors
    rTensorR () {
        for (int r=0; r<R; ++r) SL[r] = NULL;
        v  = NULL;
    }

    void Clear() {
        for (int r=0; r<R; ++r) SL[r] = NULL;
        v  = NULL;
    }

  public:

    rTensorR (const IndexLayout * SSL[R]) {
        for (int r=0; r<R; ++r) SL[r] = SSL[r];
        int NR = 1;
        for (int r=0; r<R; ++r) NR *= SL[r]->GetDim();
        v = new double[NR];
    }

   ~rTensorR () {
        delete[] v;
    }

    rTensorR (const rTensorR & rhs) {
        for (int r=0; r<R; ++r) SL[r] = rhs.SL[r];
        int NR = 1;
        for (int r=0; r<R; ++r) NR *= SL[r]->GetDim();
        v = new double[NR];
        for (int n=0; n<NR; ++n) v[n] = rhs.v[n];
    }

    // accessors

    double operator[] (int (&ii)[R]) const {
        return v[offset(ii)];
    }

    double & operator[] (int (&ii)[R]) {
        return v[offset(ii)];
    }

    // incomplete indices (should cast to double if they are complete)
    rTensorR <R> operator() (const Index II[R]) const {
        rTensorR <R> T;
        T.v  = v + offset(II);
        for (int r=0; r<R; ++r) T.SL[r] =  &((*SL[r]) (II[r]) );
        return T;
    }

    // cast to double (i.e. when the accessing indices contain the full coordinates of the element)
    /*
    operator double () const {
        return *v;
    }
    */

    const IndexLayout & GetSL(int i) {
        return *(SL[i]);
    }

};



template <int R>
class rIterator {

    Iterator * Its[R];

  public:
    template <int S>
    rIterator (const rTensorR <S> & T, int indx[R]) {

        for (int r=0; r<R; ++r) {
            int s = indx[r];
            Its[r] = new Iterator(T.GetSL(s));
        }
    }

};

// general tensor product
template <int R, int S, int K> void Contract (rTensorR <R+S-2*K> & C, rTensorR <R> & A, rTensorR <S> & B, int Aindx[K], int Bindx[K]) {

    //rTensorR <R+S-2*K> C;

    // indices w/o contraction
    int indxa[R-K];
    int indxb[S-K];

    {
        int a=0;
        for (int r=0; r<R; ++r) {
            // find if r is mentioned in Aindx
            bool isk = false;
            for (int k=0; k<K; ++k)
                if (Aindx[k]==r) isk = true;
            if (!isk) {
                indxa[a] = r;
                ++a;
            }
        }
    }

    {
        int b=0;
        for (int s=0; s<S; ++s) {
            // find if s is mentioned in Bindx
            bool isk = false;
            for (int k=0; k<K; ++k)
                if (Bindx[k]==s) isk = true;
            if (!isk) {
                indxb[b] = s;
                ++b;
            }
        }
    }

    // use direct access to the tensor
    int dimsA[R-K];
    int dimsB[S-K];

    for (int r=0; r<R-K; ++r) dimsA[r] = A.GetSL(indxa[r]).GetDim();
    for (int s=0; s<S-K; ++s) dimsB[s] = B.GetSL(indxb[s]).GetDim();

    int dimsK[K];
    for (int k=0; k<K  ; ++k) dimsK[k] = A.GetSL(Aindx[k]).GetDim();


    /*
    // SCHEME:
    // loop over A's ranks not in K
    for () {
        // loop over B's ranks not in K
        for () {
            // loop-accumulate over indices K
            for () {
                // reconstruct A & B dimension vector
            }
            // construct C index vector
        }
    }
    */

    int aaa[R-K];
    for (int i=0; i<R-K; ++i) aaa[i] = 0;

    while (aaa[0]<dimsA[0]) {

        int bbb[S-K];
        for (int i=0; i<S-K; ++i) bbb[i] = 0;

        while (bbb[0]<dimsB[0]) {

            double sum=0;

            int kkk[K];
            for (int i=0; i<K; ++i) kkk[i] = 0;

            while (kkk[0]<dimsK[0]) {

                {
                    // generate full indices for the A and B tensors
                    int indxA[R];
                    int indxB[S];

                    for (int r=0; r<R-K; ++r) indxA[indxa[r]] = aaa[r];
                    for (int k=0; k<K  ; ++k) indxA[Aindx[k]] = kkk[k];

                    for (int s=0; s<S-K; ++s) indxB[indxb[s]] = bbb[s];
                    for (int k=0; k<K  ; ++k) indxB[Bindx[k]] = kkk[k];

                    // multiply-accumulate
                    sum +=
                     A[indxA]
                    * B[indxB];
                }


                // postincrement-check
                int k=K-1;
                ++kkk[k];
                while (kkk[k]==dimsK[k] and k>0) {
                   kkk[k] = 0;
                   --k;
                   ++kkk[k];
                }

            }

            // save the result in tensor C
            {
                int indxC[R+S-2*K];
                for (int r=0; r<R-K; ++r) indxC[    r] = aaa[r];
                for (int s=0; s<S-K; ++s) indxC[R-K+s] = bbb[s];

                C[indxC] = sum;
            }


            // postincrement-check
            int j=S-K-1;
            ++bbb[j];
            while (bbb[j]==dimsB[j] and j>0) {
               bbb[j] = 0;
               --j;
               ++bbb[j];
            }
        }

        // postincrement-check
        int i=R-K-1;
        ++aaa[i];
        while (aaa[i]==dimsA[i] and i>0) {
           aaa[i] = 0;
           --i;
           ++aaa[i];
        }
    }

    //return C;
}


// tensor contraction
// tensor flattening
// tensor reshaping )?)
// direct indexed access
// recognize Index ranks
// recursive block SDV (data compression)
// index flattening (i.e. rTensor4 -> rTensor2)
// support of sparse data
// multidimensional iterators


int main() {

    // simple shells
    IndexLayout SS(1), PP(3), DD(5), FF(7), GG(9), HH(11);

    // blocks of shells
    IndexLayout P4, P5, P6,P7,P8;
    P4 =      SS*5 + PP*4 + DD*3 + FF*2 + GG*1;
    P5 = P5 & SS*5 & PP*4 & DD*3 & FF*2 & GG*1;
    P6 = P5*2;
    P7 = P6*3;

    cout << DD.GetID() << " " << FF.GetID() << endl;


    cout << P4.GetID() << " " << P4.GetNSB() << " " << P4.GetDim() << endl;
    cout << P5.GetID() << " " << P5.GetNSB() << " " << P5.GetDim() << endl;

    cout << "p4:  " << P4 << endl;
    cout << "p5:  " << P5 << endl;
    cout << "p6:  " << P6 << endl;
    cout << "p7:  " << P7 << endl;

    //P8 = P7.Collapse();

    cout << P8 << endl;

    Iterator I(P6,2);

    while (!(I.end())) {
        cout << I << endl;

        Iterator J = I.Span();
        while (!(J.end())) {
            cout << "  " << J; cout << endl;
            ++J;
        }

        ++I;
    }

    Iterator K(P6);

    while (!(K.end())) {
        cout << K << endl;
        ++K;
    }


    // TEST TENSORS

    const IndexLayout * dims3[] = {&P5, &P5, &P5};
    const IndexLayout * dims2[] = {&P5, &P5};

    rTensorR<3> A(dims3), B(dims3);
    rTensorR<2> C(dims2);

    int indxa[] = {2,0};
    int indxb[] = {1,2};

    Contract<3,3,2>(C, A,B,indxa,indxb);



    return 0;
}
