#ifndef __EIGEN__
#define __EIGEN__

class tensor2;
class tensor3;
class symtensor2;

// eigensystem routines for: eigenvalues, eigenpairs, generalized eigensystems
void DiagonalizeE  (const  tensor2 & A,              double * w, bool descending=false); // just eigenvalues
void DiagonalizeV  (       tensor2 & A,              double * w, bool descending=false); // eigenpairs
void DiagonalizeGV (       tensor2 & A, tensor2 & B, double * w, int itype=1);           // GEV

void DiagonalizeNE (const tensor2 & A, double * wr, double * wi); // nonsymmetric eigenproblem; just eigenvalues



void Cholesky      (tensor2 & A);                         // Cholesky for SDP matrices
int  Cholesky      (tensor2 & A, int * p, double tol=0.); // Cholesky with pivoting; returns rank

// inverts a tridiagonal matrix obtained from a CD
void InvChol       (tensor2 & L);

// singular value decompositions

// computes all vectors; leaves the original matrix intact T = U s Vt
int SVD (const tensor2 & T, tensor2 & U, tensor2 & Vt, double * ss);

// works on thin (M>>N) arrays; returns only the first N columns of Vt in the tensor T matrix
// in case pf M<N, it will still return the square matrix U filled with zeros after th
int SVD (      tensor2 & T,              tensor2 & U, double * ss);

// principal component analysis (SVD)
int PCA (const tensor2 & T, double * ss);

int CPdecomp (tensor2 & U, tensor2 & V, const tensor3 & T, const double thresh = 1.e-6);
int HOSVD    (const tensor3 & T, const double thresh = 1.e-6);

int KhatriRao (tensor2 & AB, const tensor2 & A, const tensor2 & B);

// inverse or MP pseudoinverse a symmetric matrix,
// should precondition the diagonal
// quite expensive, so use for small matrices or testing
void Invert (tensor2 & W);

// invert a general matrix using LU factorization
void InvertLU (tensor2 & W);

// Moore-Penrose pseudoinverse (technically its transpose, i.e dimensions of T are not changed)
void PseudoInverse (tensor2 & T);

// returns a transposed matrix
tensor2 Transpose (const tensor2 & A);

//

// computes W = A D A+   (where D is a diagonal matrix)
void WeightedInnerProduct  (tensor2 & W, const tensor2 & A, const double * d);
// computes W = A+ D A   (where D is a diagonal matrix)
void WeightedInnerProductT (tensor2 & W, const tensor2 & A, const double * d);

// computes W = A D B+   (where D is a diagonal matrix)
void WeightedInnerProduct (tensor2 & W, const tensor2 & A, const tensor2 & B, const double * d);
// computes W = A+ D B   (where D is a diagonal matrix)
void WeightedInnerProductT  (tensor2 & W, const tensor2 & A, const tensor2 & B, const double * d);

// A <- d * A
void DiagonalScaling (const double * d, tensor2 & A);
// A <- A * d
void DiagonalScaling (tensor2 & A, const double * d);

// solve a set of linear equations
void SolveLinear   (char uplo, int N, double * A, double * b);

void SolveLinear   (tensor2 & A, double * b);

// dot product of two matrices
double DotProduct  (const tensor2 & A, const tensor2 & B);

double Contract    (const tensor2 & A, const tensor2 & B);

// dense matrix products
void TensorProduct    (tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha=0.);
void TensorProductNNT (tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha=0.);
void TensorProductNTN (tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha=0.);
void TensorProductNTT (tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha=0.);

#endif
