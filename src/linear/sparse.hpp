#ifndef __SPARSE__
#define __SPARSE__

#include <fstream>
#include "low/mem.hpp"

class GTObasis;
class Nucleus;
class tensor2;

//guarda el patron de bloques de una matriz sparse
struct SparseTensorBlockPattern {
    double thresh; //valor minimo a considerar
    int size;      //numero de elementos totales
    int nblocks;   //numero de bloques
    int * blsize;  //longitud del bloque
    int * blpoint; //puntero al n-simo bloque

    slock * clocks;
    slock * flocks;

    SparseTensorBlockPattern & operator=(const SparseTensorBlockPattern & BFpattern);

    void Init (Nucleus * nuclei, int nnuclei, const GTObasis & BasisSet, double BFthresh);
};

struct SparseTensor2 {
    int nblocks;    //numero de bloques (grupos de funciones base)

    SparseTensorBlockPattern * pattern;

    AVLTree<tensor2*> * filc;
    AVLTree<tensor2*> * colc;

    SparseTensor2();
    SparseTensor2 & operator=(const SparseTensor2 & spt);

	SparseTensor2( SparseTensorBlockPattern & pat);
    void set(SparseTensorBlockPattern & pat);
    void clear();//borra la informacion del tensor excepto su estructura basica
    ~SparseTensor2();//destruye el resto del tensor
};



class tensor2;
class symtensor2;


void SparseTensorProd  (SparseTensor2 & R, SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern);
void SparseTensorProdTN(SparseTensor2 & R, SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern);
void SparseTensorProdNT(SparseTensor2 & R, SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern);

void Tensor2Sparse(SparseTensor2 & R, symtensor2 & A,    const SparseTensorBlockPattern & pattern);
void Tensor2Sparse(SparseTensor2 & R, tensor2 & A,       const SparseTensorBlockPattern & pattern);
void Sparse2Tensor(tensor2 & R,       SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void Sparse2Tensor(symtensor2 & R,    SparseTensor2 & A, const SparseTensorBlockPattern & pattern);

void   SparseTensorProd        (SparseTensor2 & R, double v, SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void   SparseTensorProd        (SparseTensor2 & R, double v, const SparseTensorBlockPattern & pattern);
void   SparseTensorCopy        (SparseTensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void   SparseTensorCopyDel     (SparseTensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void   SparseTensorAdd         (SparseTensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void   SparseTensorAddDiagonal (SparseTensor2 & R, double v, const SparseTensorBlockPattern & pattern);

void   SparseTensorCombination (SparseTensor2 & R, SparseTensor2 & A, double lambda, const SparseTensorBlockPattern & pattern);
double SparseTensorContract    (SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern);

void SparseTensorPlot      (SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void SparseTensorSymmFill  (SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void SparseTensorSymmReFill(SparseTensor2 & A, const SparseTensorBlockPattern & pattern);
void SparseTensorSymmetrize(SparseTensor2 & A, const SparseTensorBlockPattern & pattern);

void SparseTensorRead (SparseTensor2 & A, ifstream & ofile, SparseTensorBlockPattern & pattern);
void SparseTensorWrite(std::ofstream & ofile, SparseTensor2 & A, SparseTensorBlockPattern & pattern);
//esto debe ser un "inline" y ser utilizado siempre que toque
//solo sirve para hacer los accesos en paralelo pero sin escribir a zonas compartidas
tensor2 * GetOrSet (SparseTensor2 & A, const SparseTensorBlockPattern & pattern, int i, int j);
#endif
