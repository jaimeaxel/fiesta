#ifndef __TENSOR__
#define __TENSOR__

typedef unsigned long long int uint64;
typedef unsigned      long int uint32;
typedef unsigned     short int uint16;

// HW LAYER
// ========
class HWtensor {


};

class BufferInterface {
  public:
    virtual BufferInterface (uint64 N);
    virtual size_t sizeof()             const;
}

template <class T> class ArrayInterface {
  public:
    virtual const T & operator[] (uint64 m) const;
    virtual       T & operator[] (uint64 m);
    virtual uint64    len()                 const;
    virtual uint64 totlen()                 const;
    virtual std::map<T, uint64> Transpose() const;
}



template <class T> class Array : public BufferInterface, public ArrayInterface<T> {

    T  * i = NULL;
    uint64  M = 0;

    Array() {
        M = 0;
        i = NULL;
    }

  public:

    // regular constructor
    Array(uint64 N) {
        M = N;
        i = new T[M];
    }

    // copy constructor
    Array(const Array<T> & r) {
        M = r.M;
        i = new T[M];
        for (uint64 m=0; m<M; ++m) i[m] = r.i[m];
    }

    // destructor
   ~Array() {
       delete[] i;
    }


    // return length of array
    uint64 len() const {return M;}

    // recursive length, if applicable
    uint64 totlen() const;

    // return total mem of array
    size_t sizeof() const;


    // return const access to object
    const T & operator[] (uint64 m) const {
        if (m>=M) throw 123123;
        return i[m];
    }

    // return access to object
    T & operator[] (uint64 m) {
        if (m>=M) throw 123123;
        return i[m];
    }

    std::map<T, uint64> Transpose() const;

    // sort them in some fashion
    // bool operator <= (const Array<T> & r ) const {}
};

// for nested arrays, etc.
template <class T> size_t Array<Array<T>>::sizeof() const {
    size_t s=0;
    for (uint64 m=0; m<M; ++m) s+=i[m].sizeof();
    return s;
};

// regular return
template <class T> size_t Array<T>::sizeof() const {
    return M * sizeof(T);
};

// for nested arrays, etc.
template <class T> uint64 Array<Array<T>>::totlen() const {
    uint64 t=0;
    for (uint64 m=0; m<M; ++m) t+=i[m].totlen();
    return t;
};

// regular return
template <class T> uint64 Array<T>::totlen() const {
    return M;
};


/*
template <class T> std::map<Array<T>, uint64> Array<Array<T>>::Transpose() const {
    std::map<Array<T>,Array<T>> ret;
    for (uint64 m=0; m<M; ++m) ret[i[m]] = m;
    return ret;
};
*/

// regular return
template <class T> std::map<T,Array<uint64>> Array<T>::Transpose() const {
    std::map<T,Array<uint64>> ret;
    for (uint64 m=0; m<M; ++m) ret[i[m]] = m;
    return ret;
};



// TOP LAYER
// =========

// nested arrays

template<int N> class IndexTemplate : public ArrayInterface<IndexTemplate<N-1>> {

    Array<IndexTemplate<N-1>> v;

  public:

    IndexTemplate (const IndexTemplate<N>          & r)  {v = v.r;}
    IndexTemplate (const Array<IndexTemplate<N-1>> & r)  {v = r;}
   ~IndexTemplate ()                                     {}

    uint64     len() const                     {return v.len();}
    uint64  totlen() const                     {return v.totlen();}

    const uint16 & operator[] (uint64 m) const {return v[m];}
          uint16 & operator[] (uint64 m)       {return v[m];}
};

// must specialize the lowest one
template<> class IndexTemplate<0> : public ArrayInterface<uint16> {

    Array<uint16> v;

  public:
    IndexTemplate (const IndexTemplate<0> & r)    {v = v.r;}
    IndexTemplate (const Array<uint16>    & r)    {v = r;}
   ~IndexTemplate ()                              {}

    uint64  totlen() const                     {return v.totlen();}
    uint64     len() const                     {return v.len();}
    const uint16 & operator[] (uint64 m) const {return v[m];}
          uint16 & operator[] (uint64 m)       {return v[m];}
};



class Index {
    Index(const IndexTemplate & T) {
    }
};

class Mask {

};

class BaseTensor {
  public:
    size_t       size() const;
    BaseTensor   operator+ (const BaseTensor & rhs) const;
    BaseTensor   operator- (const BaseTensor & rhs) const;
    BaseTensor & operator+=(const BaseTensor & rhs);
    BaseTensor & operator-=(const BaseTensor & rhs);
};

// generic Tensor
template<int N> class Tensor:BaseTensor {

  private:
    IndexTemplate index[N];
    HWtensor * T;


};

class Tensor<1> {
};

class Tensor<2> {
};

class Tensor<3> {
};

class Tensor<4> {
};



class index {
    uint16;
};

class ElementalBasis : index {
};

class ElementalShell : index {
};

class ElementalSubShell : index {
};



#endif
