#include <iostream>
#include <map>
#include <list>
using namespace std;

#include "ntensors.hpp"
#include "tensors.hpp"

#include "low/rtensors.hpp"

#include "libechidna/libechidna.hpp"


// for structure compatibility with newsparse
static inline uint32_t Tpos (uint32_t M1, uint32_t M2, uint32_t v1, uint32_t v2) {
    return v2*M1 + v1;
}

/*
    THE PATTERN FOR Ntensor STRUCTURES
*/

// contains all the data
class pNpattern {

    friend class Npattern;
    friend class Ntensor;
    friend class NTsub;
    friend class NTSsub;
    friend class pNtensor;
    friend class pNsub;
    friend class pNSsub;
    friend class ConstNpattern;

  private:

    class atomID {
      public:
        uint32_t id;
        void Set(uint8_t t, uint32_t n) {
            id = t;
            id<<=24;
            id += n;
        }
        uint32_t N() const {return id&(0x00FFFFFF);} // freturn lowest bits
        uint8_t  T() const {return uint8_t(id>>24);}
    };

    class ebInfo {
      public:
        uint8_t   J; // times a block is repeated
        uint8_t   M; // length of the block, usually 2M+1
        uint16_t id; // unique element:ebasis tag

        void Set(uint8_t j, uint8_t m, uint16_t n) {J=j; M=m; id=n;}
    };

    class ebTag {
      public:
        uint8_t e; // which element
        uint8_t b; // which ebasis
    };

    // basic info
    r1tensor <atomID>              IDs;    // outside_atomlist
    r1tensor <r1tensor<uint32_t> > tlists; // element:atomlist

    r1tensor <r1tensor<ebInfo> >   eblock; // element:ebasis
    r1tensor <ebTag>               eblist; // ebasis_tag

    // derived info
    r1tensor <uint32_t> edim; // dimension of one element
    r1tensor <uint32_t> aos; // atom offsets in linear array
    r1tensor <r1tensor<uint32_t> > bos; // elemental basis offsets

    uint32_t dim; // total (linear) dimension

    uint32_t nAtoms() const {return IDs.N();}
};

Npattern::Npattern() {p=NULL;}

Npattern::~Npattern() {delete p;}

// this belongs to pNpattern
void Npattern::Set (const rAtom * Atoms, int nAtoms) {

    p = new pNpattern;

    map <const rAtomPrototype *, list<int> > aLists;

    // classify atoms by element
    for (int i=0; i<nAtoms; ++i) {
        const rAtomPrototype * ap = Atoms[i].rAP;
        aLists[ap].push_back(i);
    }

    int nTypes = aLists.size();


    // set the new atom types in the structure
    p->IDs.set(nAtoms);
    p->tlists.set(nTypes);
    p->eblock.set(nTypes);

    uint16_t eid=0;

    map <const rAtomPrototype *, list<int> >::const_iterator cit; uint8_t ntyp;
    for (ntyp=0, cit=aLists.begin(); cit!=aLists.end(); ++ntyp, ++cit) {

        // COPY THE LISTS
        int lsize = cit->second.size();

        p->tlists[ntyp].set(lsize);

        list<int>::const_iterator itl; uint32_t nat;
        for (nat=0, itl=cit->second.begin(); itl!=cit->second.end(); ++nat, ++itl) {
            uint32_t iatom = *itl;         // the original number of the atom in the list
            p->tlists[ntyp][nat] = iatom;  // this holds the lists of atoms
            p->IDs[iatom].Set(ntyp, nat);  // this holds the type and internal list number for a given atom
        }

        // COPY THE BASIS INFO
        const rAtomPrototype * ap = cit->first;

        int bsize = ap->basis.N();

        p->eblock[ntyp].set(bsize);

        for (int i=0; i<bsize; ++i, ++eid) {
            p->eblock[ntyp][i].Set(ap->basis[i].J, 2*ap->basis[i].l + 1, eid);
        }
    }

    // do the elemental basis list
    p->eblist.set(eid);

    eid=0;

    for (uint8_t e=0; e<p->eblock.N(); ++e) {
        for (uint8_t b=0; b<p->eblock[e].N(); ++b, ++eid) {
            p->eblist[eid].e = e;
            p->eblist[eid].b = b;
        }
    }

    // OFFSETS
    p->aos.set(nAtoms);
    p->bos.set(nTypes);
    p->edim.set(nTypes);

    // element size and shell offsets
    for (uint8_t e=0; e<p->eblock.N(); ++e) {
        int esize = 0;

        p->bos[e].set(p->eblock[e].N());

        for (uint8_t b=0; b<p->eblock[e].N(); ++b) {
            p->bos[e][b] = esize;
            esize += p->eblock[e][b].J * p->eblock[e][b].M;
        }

        p->edim[e] = esize;
    }

    // atom linear offsets
    int os=0;

    for (int n=0; n<nAtoms; ++n) {
        p->aos[n] = os;

        uint8_t e = p->IDs[n].T(); // get element
        os += p->edim[e]; // add to offset
    }

    // total dimension
    p->dim = os;
}

uint32_t Npattern::GetAtoms () const {
    return p->nAtoms();
}

uint8_t  Npattern::GetID    (uint32_t atom) const {
    return p->IDs[atom].T();
}

uint8_t  Npattern::GetNIDs  () const {
    return p->eblock.N();
}

uint8_t  Npattern::GetFuncs (uint8_t  elem) const {
    return p->eblock[elem].N();
}

uint32_t Npattern::GetLen (uint8_t  elem, uint8_t ebss) const {
    const pNpattern::ebInfo & EBI = p->eblock[elem][ebss];
    return uint32_t(EBI.J)*uint32_t(EBI.M);
}

uint32_t Npattern::GetLen      (uint32_t n) const {
    uint8_t e = p->IDs[n].T(); // get element
    return p->edim[e];         // return element total size
}

uint32_t Npattern::GetTotalLen () const {
    return p->dim;
}

uint32_t Npattern::GetFuncs () const {

    uint32_t fs = 0;

    for (int e=0; e<p->tlists.N(); ++e) {
        fs += p->tlists[e].N() * p->eblock[e].N();
    }

    return fs;
}


class pxArray {
    friend class xArray;
    friend class pNtensor;
    friend class NTsub;
    friend class NTSsub;

  private:
    double ** p;
    double  * d;

    pxArray() {p=NULL; d=NULL;}

    // lock
    const pxArray & operator=(double ** rhs) {
        p = rhs;
        d = NULL;
        do {
            if (*p!=NULL)
                d = __sync_lock_test_and_set(p, NULL);
        } while(d==NULL);

        return *this;
    }

    // unlock
   ~pxArray() {if (p!=NULL) *p = d;}
};



xArray::xArray() {
    p = NULL;
}

xArray::~xArray() {
    if (p!=NULL) delete p;
}


xArray::xArray(pxArray * r) {
    p = r;
}

double & xArray::operator[] (int n) {
    return p->d[n];
}


/*
    THE PATTERN FOR Ntensor STRUCTURES
*/

#include <stdio.h>

class pNsub {
  public:
    int size;
    uint8_t J1,J2,M1,M2;
    const r1tensor <pNpattern::atomID> * IDs;
    r2tensor< double* > Arrays;
    double * v;
};

class pNSsub {
  public:
    int size;
    uint8_t J1,J2,M1,M2;
    const r1tensor <pNpattern::atomID> * IDs;
    std::map<uint64_t, double*> spArrays;
    double * v;
};


// contains all the data
class pNtensor {

    friend class Ntensor;
    friend class NTmulti;
    friend class NTconst;

  private:

    const pNpattern * p;     // definition
    double * v;              // where the data is stored; (use a buffer instead)
    uint64_t tsize;
    int NN;                  //


    r2tensor<pNsub> eBlocks; // element^2 subblocks

  public:

    pNtensor() {
        p=NULL;
        v=NULL;
        tsize = 0;
        NN = 0;
    }

   ~pNtensor() {
        delete[] v;
    }

    const pNpattern * GetPattern() const {
        return p;
    }

    void Set(const pNpattern * pp) {
        p  = pp;
        NN = 0;

        // set the arrays structure
        int nbs = p->eblist.N();
        eBlocks.set(nbs,nbs);

        for (int i=0; i<nbs; ++i) {
            for (int j=0; j<nbs; ++j) {
                uint8_t e1 = p->eblist[i].e;
                uint8_t e2 = p->eblist[j].e;

                int N1 = p->tlists[e1].N();
                int N2 = p->tlists[e2].N();

                eBlocks(i,j).Arrays.set(N1, N2);

                uint8_t b1 = p->eblist[i].b;
                uint8_t b2 = p->eblist[j].b;

                eBlocks(i,j).J1 = p->eblock[e1][b1].J;
                eBlocks(i,j).J2 = p->eblock[e2][b2].J;
                eBlocks(i,j).M1 = p->eblock[e1][b1].M;
                eBlocks(i,j).M2 = p->eblock[e2][b2].M;
            }
        }
    }

    // init a dense version of the structure
    void Init (int N) {

        // compute memory needed
        // =====================

        // number of matrices
        NN = N;

        // dimension of the system
        uint64_t nsize = 0;

        int nes = p->eblock.N();
        for (int e=0; e<nes; ++e) {

            // compute the size of the basis of a given element
            int esize = 0;

            int nbs = p->eblock[e].N();
            for (int b=0; b<nbs; ++b) {
                esize += p->eblock[e][b].J * p->eblock[e][b].M;
            }

            // update the total dimension of the system
            nsize += esize * p->tlists[e].N();
        }

        // memory required
        tsize = N * nsize*nsize;

        v = new double[tsize];


        // set the pointers
        // ================
        double * vv = v; // offset

        int nbs = p->eblist.N();

        for (int i=0; i<nbs; ++i) {
            for (int j=0; j<nbs; ++j) {

                uint8_t e1 = p->eblist[i].e;
                uint8_t e2 = p->eblist[j].e;

                uint8_t b1 = p->eblist[i].b;
                uint8_t b2 = p->eblist[j].b;

                // compute the size of the basis of a given element
                int esize1 = p->eblock[e1][b1].J * p->eblock[e1][b1].M;
                int esize2 = p->eblock[e2][b2].J * p->eblock[e2][b2].M;

                int esize12 = NN * esize1 * esize2;

                // set block info
                eBlocks(i,j).size = esize12;
                eBlocks(i,j).v = vv;
                eBlocks(i,j).IDs = &(p->IDs);


                // set all atom pairs' pointers
                int N1 = p->tlists[e1].N();
                int N2 = p->tlists[e2].N();

                for (int a1=0; a1<N1; ++a1) {
                    for (int a2=0; a2<N2; ++a2) {
                        eBlocks(i,j).Arrays(a1,a2) = vv;
                        vv += esize12;
                    }
                }

            }
        }
    }

    void Zero() {
        // use memset
        for (uint64_t t=0; t<tsize; ++t) v[t]=0;
    }

    double * const operator() (uint32_t at1, uint32_t at2,  uint8_t b1, uint8_t b2) {

        uint8_t e1 = p->IDs[at1].T();
        uint32_t n1 = p->IDs[at1].N();
        uint8_t e2 = p->IDs[at2].T();
        uint32_t n2 = p->IDs[at2].N();

        uint16_t eb1 = p->eblock[e1][b1].id;
        uint16_t eb2 = p->eblock[e2][b2].id;

        // TTAS lock using instrisics
        double ** p = &(eBlocks(eb1,eb2).Arrays(n1,n2));
        double * r = NULL;

        do {
            if (*p!=NULL)
                r = __sync_lock_test_and_set(p, NULL);
        } while(r==NULL);

        //cout << "lock acquired: " << r << " " << p << endl;

        return r;

        //return Arrays(eb1,eb2)(n1,n2);
    }

    pxArray * operator() (uint32_t at1, uint32_t at2,  uint8_t b1, uint8_t b2, bool dum1, bool dum2) {

        uint8_t e1 = p->IDs[at1].T();
        uint32_t n1 = p->IDs[at1].N();
        uint8_t e2 = p->IDs[at2].T();
        uint32_t n2 = p->IDs[at2].N();

        uint16_t eb1 = p->eblock[e1][b1].id;
        uint16_t eb2 = p->eblock[e2][b2].id;

        pxArray * px = new pxArray;
        (*px) = &(eBlocks(eb1,eb2).Arrays(n1,n2));

        return px;
    }

    void free (uint32_t at1, uint32_t at2,  uint8_t b1, uint8_t b2, double * const r) {

        uint8_t e1 = p->IDs[at1].T();
        uint32_t n1 = p->IDs[at1].N();
        uint8_t e2 = p->IDs[at2].T();
        uint32_t n2 = p->IDs[at2].N();

        uint16_t eb1 = p->eblock[e1][b1].id;
        uint16_t eb2 = p->eblock[e2][b2].id;

        double ** p = &(eBlocks(eb1,eb2).Arrays(n1,n2));

        *p = r;
    }

    const double * operator() (uint32_t at1, uint32_t at2,  uint8_t b1, uint8_t b2, bool dum) const {

        uint8_t e1 = p->IDs[at1].T();
        uint32_t n1 = p->IDs[at1].N();
        uint8_t e2 = p->IDs[at2].T();
        uint32_t n2 = p->IDs[at2].N();

        uint16_t eb1 = p->eblock[e1][b1].id;
        uint16_t eb2 = p->eblock[e2][b2].id;

        return eBlocks(eb1,eb2).Arrays(n1,n2);
    }

    pNsub * operator() (uint8_t e1, uint8_t e2, uint8_t b1, uint8_t b2) {

        uint16_t eb1 = p->eblock[e1][b1].id;
        uint16_t eb2 = p->eblock[e2][b2].id;

        pNsub * ps = &(eBlocks(eb1,eb2));

        return ps;
    }



    void Read (const tensor2 * T2) {

        for (uint32_t a1=0; a1<p->nAtoms(); ++a1) {
            for (uint32_t a2=0; a2<p->nAtoms(); ++a2) {
                uint8_t e1 = p->IDs[a1].T();
                uint8_t e2 = p->IDs[a2].T();

                for (uint8_t b1=0; b1<p->eblock[e1].N(); ++b1) {
                    for (uint8_t b2=0; b2<p->eblock[e2].N(); ++b2) {
                        // retrieve the copy
                        double * const t = (*this)(a1,a2,b1,b2);

                        uint16_t eb1 = p->eblock[e1][b1].id;
                        uint16_t eb2 = p->eblock[e2][b2].id;

                        uint8_t J1 = eBlocks(eb1, eb2).J1;
                        uint8_t J2 = eBlocks(eb1, eb2).J2;
                        uint8_t M1 = eBlocks(eb1, eb2).M1;
                        uint8_t M2 = eBlocks(eb1, eb2).M2;

                        // number of matrices
                        int os=0;
                        for (int n=0; n<NN; ++n) {

                            for (int j1=0; j1<J1; ++j1) {
                                for (int j2=0; j2<J2; ++j2) {

                                    // note the different order!
                                    for (int m2=0; m2<M2; ++m2) {
                                        for (int m1=0; m1<M1; ++m1) {
                                            int os1 = p->aos[a1] + p->bos[e1][b1] + j1*M1 + m1;
                                            int os2 = p->aos[a2] + p->bos[e2][b2] + j2*M2 + m2;
                                            t[os] = T2[n](os1, os2);
                                            ++os;
                                        }
                                    }
                                }
                            }

                        }

                        free(a1,a2,b1,b2, t);

                    }
                }

            }
        }

    }

    void ReadS (const tensor2 * T2) {

        for (uint32_t a1=0; a1<p->nAtoms(); ++a1) {
            for (uint32_t a2=0; a2<p->nAtoms(); ++a2) {
                uint8_t e1 = p->IDs[a1].T();
                uint8_t e2 = p->IDs[a2].T();

                for (uint8_t b1=0; b1<p->eblock[e1].N(); ++b1) {
                    for (uint8_t b2=0; b2<p->eblock[e2].N(); ++b2) {
                        // retrieve the copy
                        double * const t = (*this)(a1,a2,b1,b2);

                        uint16_t eb1 = p->eblock[e1][b1].id;
                        uint16_t eb2 = p->eblock[e2][b2].id;

                        uint8_t J1 = eBlocks(eb1, eb2).J1;
                        uint8_t J2 = eBlocks(eb1, eb2).J2;
                        uint8_t M1 = eBlocks(eb1, eb2).M1;
                        uint8_t M2 = eBlocks(eb1, eb2).M2;

                        // number of matrices
                        int os=0;
                        for (int n=0; n<NN; ++n) {

                            for (int j1=0; j1<J1; ++j1) {
                                for (int j2=0; j2<J2; ++j2) {

                                    // note the different order!
                                    for (int m2=0; m2<M2; ++m2) {
                                        for (int m1=0; m1<M1; ++m1) {
                                            int os1 = p->aos[a1] + p->bos[e1][b1] + j1*M1 + m1;
                                            int os2 = p->aos[a2] + p->bos[e2][b2] + j2*M2 + m2;
                                            t[os] = 0.5* (T2[n](os1, os2)+T2[n](os2, os1));
                                            ++os;
                                        }
                                    }
                                }
                            }

                        }

                        free(a1,a2,b1,b2, t);

                    }
                }

            }
        }

    }

    void ReadA (const tensor2 * T2) {

        for (uint32_t a1=0; a1<p->nAtoms(); ++a1) {
            for (uint32_t a2=0; a2<p->nAtoms(); ++a2) {
                uint8_t e1 = p->IDs[a1].T();
                uint8_t e2 = p->IDs[a2].T();

                for (uint8_t b1=0; b1<p->eblock[e1].N(); ++b1) {
                    for (uint8_t b2=0; b2<p->eblock[e2].N(); ++b2) {
                        // retrieve the copy
                        double * const t = (*this)(a1,a2,b1,b2);

                        uint16_t eb1 = p->eblock[e1][b1].id;
                        uint16_t eb2 = p->eblock[e2][b2].id;

                        uint8_t J1 = eBlocks(eb1, eb2).J1;
                        uint8_t J2 = eBlocks(eb1, eb2).J2;
                        uint8_t M1 = eBlocks(eb1, eb2).M1;
                        uint8_t M2 = eBlocks(eb1, eb2).M2;

                        // number of matrices
                        int os=0;
                        for (int n=0; n<NN; ++n) {

                            for (int j1=0; j1<J1; ++j1) {
                                for (int j2=0; j2<J2; ++j2) {

                                    // note the different order!
                                    for (int m2=0; m2<M2; ++m2) {
                                        for (int m1=0; m1<M1; ++m1) {
                                            int os1 = p->aos[a1] + p->bos[e1][b1] + j1*M1 + m1;
                                            int os2 = p->aos[a2] + p->bos[e2][b2] + j2*M2 + m2;
                                            t[os] = 0.5* (T2[n](os1, os2)-T2[n](os2, os1));
                                            ++os;
                                        }
                                    }
                                }
                            }

                        }

                        free(a1,a2,b1,b2, t);

                    }
                }

            }
        }

    }

    void Write (tensor2 * T2) const {

        for (uint32_t a1=0; a1<p->nAtoms(); ++a1) {
            for (uint32_t a2=0; a2<p->nAtoms(); ++a2) {
                uint8_t e1 = p->IDs[a1].T();
                uint8_t e2 = p->IDs[a2].T();

                for (uint8_t b1=0; b1<p->eblock[e1].N(); ++b1) {
                    for (uint8_t b2=0; b2<p->eblock[e2].N(); ++b2) {
                        // retrieve the copy
                        const double * t = (*this)(a1,a2,b1,b2, true);

                        uint16_t eb1 = p->eblock[e1][b1].id;
                        uint16_t eb2 = p->eblock[e2][b2].id;

                        uint8_t J1 = eBlocks(eb1, eb2).J1;
                        uint8_t J2 = eBlocks(eb1, eb2).J2;
                        uint8_t M1 = eBlocks(eb1, eb2).M1;
                        uint8_t M2 = eBlocks(eb1, eb2).M2;

                        // number of matrices
                        for (int n=0; n<NN; ++n) {

                            for (int j1=0; j1<J1; ++j1) {
                                for (int j2=0; j2<J2; ++j2) {
                                    // the order is important!
                                    for (int m2=0; m2<M2; ++m2) {
                                        for (int m1=0; m1<M1; ++m1) {
                                            int os1 = p->aos[a1] + p->bos[e1][b1] + j1*M1 + m1;
                                            int os2 = p->aos[a2] + p->bos[e2][b2] + j2*M2 + m2;
                                            T2[n](os1, os2) += (*t);
                                            ++t;
                                        }
                                    }
                                }
                            }

                        }

                    }
                }

            }
        }

    }

    void Write (tensor2 & T2, int n) const {

        for (uint32_t a1=0; a1<p->nAtoms(); ++a1) {
            for (uint32_t a2=0; a2<p->nAtoms(); ++a2) {
                uint8_t e1 = p->IDs[a1].T();
                uint8_t e2 = p->IDs[a2].T();

                for (uint8_t b1=0; b1<p->eblock[e1].N(); ++b1) {
                    for (uint8_t b2=0; b2<p->eblock[e2].N(); ++b2) {
                        // retrieve the copy
                        const double * t = (*this)(a1,a2,b1,b2, true);

                        uint16_t eb1 = p->eblock[e1][b1].id;
                        uint16_t eb2 = p->eblock[e2][b2].id;

                        uint8_t J1 = eBlocks(eb1, eb2).J1;
                        uint8_t J2 = eBlocks(eb1, eb2).J2;
                        uint8_t M1 = eBlocks(eb1, eb2).M1;
                        uint8_t M2 = eBlocks(eb1, eb2).M2;

                        // number of matrices
                        //for (int n=0; n<NN; ++n)
                        {
                            t += n*J1*J2*M1*M2; // skip the first n

                            for (int j1=0; j1<J1; ++j1) {
                                for (int j2=0; j2<J2; ++j2) {
                                    // the order is important!
                                    for (int m2=0; m2<M2; ++m2) {
                                        for (int m1=0; m1<M1; ++m1) {
                                            int os1 = p->aos[a1] + p->bos[e1][b1] + j1*M1 + m1;
                                            int os2 = p->aos[a2] + p->bos[e2][b2] + j2*M2 + m2;
                                            T2(os1, os2) += (*t);
                                            ++t;
                                        }
                                    }
                                }
                            }

                        }

                    }
                }

            }
        }

    }

};


Ntensor::Ntensor(const Npattern * T) {
    p = new pNtensor;
    p->Set(T->p);
}

Ntensor::~Ntensor() {delete p;}

//const ConstNpattern * Ntensor::GetPattern() const {
//    ConstNpattern * np = new ConstNpattern;
//    np->p = p->GetPattern();
//    return np;
//}

const ConstNpattern Ntensor::GetPattern() const {
    ConstNpattern cnp;
    cnp.p = p->GetPattern();
    return cnp;
}

ConstNpattern::ConstNpattern() {p=NULL;}

uint32_t ConstNpattern::GetAtoms () const { return p->nAtoms();}

uint8_t  ConstNpattern::GetID    (uint32_t atom) const {return p->IDs[atom].T();}

uint8_t  ConstNpattern::GetNIDs  () const { return p->eblock.N();}

uint8_t  ConstNpattern::GetFuncs (uint8_t  elem) const {return p->eblock[elem].N();}

uint32_t ConstNpattern::GetLen (uint8_t  elem, uint8_t ebss) const {
    const pNpattern::ebInfo & EBI = p->eblock[elem][ebss];
    return uint32_t(EBI.J)*uint32_t(EBI.M);
}

uint32_t ConstNpattern::GetFuncs () const {
    uint32_t fs = 0;
    for (int e=0; e<p->tlists.N(); ++e) {
        fs += p->tlists[e].N() * p->eblock[e].N();
    }
    return fs;
}

ConstNpattern::~ConstNpattern() {p=NULL;}


// pass the pattern and the number of matrices to be set
void NTmulti::Set (int N) {
    p->Init(N);
    p->Zero();
}

// extract the tensors (blocks all other operations)
void NTmulti::AddTo (tensor2 * T2) const {
    p->Write(T2);
}

void NTmulti::AddTo (tensor2 & T2, int n) const {
    p->Write(T2, n);
}

// get a fresh chunk of memory (nonblocking)
double * const NTmulti::operator() (uint32_t at1, uint32_t at2,  uint8_t eb1, uint8_t eb2) {
    return (*p)(at1,at2,eb1,eb2);
}

pxArray * NTmulti::operator() (uint32_t at1, uint32_t at2,  uint8_t eb1, uint8_t eb2, int y) {
    return  (*p)(at1,at2,eb1,eb2, true, true);
}

void NTmulti::free (uint32_t at1, uint32_t at2,  uint8_t b1, uint8_t b2, double * const r) {
    p->free(at1,at2, b1,b2, r);
}


pNsub * NTmulti::operator()(uint8_t e1, uint8_t e2, uint8_t b1, uint8_t b2) {
    return  (*p)(e1,e2,b1,b2);
}



NTsub::NTsub() {
    p=NULL;
}

NTsub::~NTsub() {
}

NTsub::NTsub(pNsub * r) {
    p=r;
}

pxArray * NTsub::operator() (uint32_t at1, uint32_t at2) {

    const r1tensor <pNpattern::atomID> & IDs = *(p->IDs);

    uint32_t n1 = IDs[at1].N();
    uint32_t n2 = IDs[at2].N();

    pxArray * px = new pxArray;
    (*px) = &(p->Arrays(n1,n2));

    return px;
}




NTSsub::NTSsub() {
    p=NULL;
}

NTSsub::~NTSsub() {
}

NTSsub::NTSsub(pNSsub * r) {
    p=r;
}

pxArray * NTSsub::operator() (uint32_t at1, uint32_t at2) {

    const r1tensor <pNpattern::atomID> & IDs = *(p->IDs);

    uint32_t n1 = IDs[at1].N();
    uint32_t n2 = IDs[at2].N();

    uint64_t n12 = (inter(n1)<<1) + inter(n2);

    double ** r;

    //
    #pragma omp critical
    {
        typename std::map<uint64_t, double*>::const_iterator it;
        double * v12;

        it = p->spArrays.find(n12);

        // not found: initialize
        if (it==p->spArrays.end()) {
            v12 = p->v + p->size * p->spArrays.size(); // pointer to first uninitialized chunk of memory
            p->spArrays[n12] = v12;
            for (int i=0; i<p->size; ++i) v12[i] = 0;
        }
        else
            v12 = (it->second);

        r = &(p->spArrays[n12]);
    }

    // THE REFERENCE TO THE POINTER MIGHT CHANGE!
    // BUT ONLY IF A THREAD INSERTS A NEW ELEMENT
    // SO THE WRITE LOCK SHOULD ALSO PROTECT THE MAP
    // FROM BEING ALTERED
    pxArray * px = new pxArray;
    (*px) = r;

    return px;
}

// member functions necessary for const access

// initialize the internal data by copying an array of matrices
void NTconst::Set (const tensor2 * T2, int N) {
    p->Init(N);
    p->Read(T2);
}

void NTconst::SetS (const tensor2 * T2, int N) {
    p->Init(N);
    p->ReadS(T2);
}

void NTconst::SetA (const tensor2 * T2, int N) {
    p->Init(N);
    p->ReadA(T2);
}

void NTconst::Put (      tensor2 * T2) const {
    for (int n=0; n<p->NN; ++n) T2[n].zeroize();
    p->Write(T2);
}


// get a pointer to const mem
const double * NTconst::operator() (uint32_t at1, uint32_t at2,  uint8_t eb1, uint8_t eb2) const {
    return (*p)(at1,at2,eb1,eb2,true);
}
