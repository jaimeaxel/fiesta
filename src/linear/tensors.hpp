#ifndef __TENSORS__
#define __TENSORS__

#include <iostream>
#include <fstream>

struct tensor;
struct symtensor;

struct tensor2;
struct symtensor2;

struct Sparse;

// linear array; this is a dummy class that only provides syntactic sugar for many expressions involving tensor2 and higher
struct tensor1 {

    double * c = NULL;
    int64_t  n = 0;    // dimension

	tensor1 () {};
	tensor1 (int64_t p, double * v) {c=v; n=p;};
   ~tensor1 () {};

    tensor1 (tensor2 & T);
    tensor1 (symtensor2 & T);
    tensor1 (const tensor2 & T);
    tensor1 (const symtensor2 & T);

    // C++11 move constructor
    tensor1(tensor1 && r) {
        c   = r.c;
        n   = r.n;
        r.c = NULL;
    }

    tensor1 & operator=(tensor1 & s) {
        c = s.c;
        n = s.n;
        return *this;
    }

    tensor1 & operator=(double * s) {
        c = s;
        return *this;
    }

	void setsize(int64_t p) { n=p; }

	bool isunset() const {
	    return (c==NULL)||(n<=0);
	}

	// dereference
	      double * operator* ()       {return c;}
	const double * operator* () const {return c;}

	// access
	inline double & operator()(int64_t i)                    {return c[i];}
	inline double   operator()(int64_t i, int64_t j) const   {return c[i];}

	inline double & operator[](int64_t i)                    {return c[i];}
	inline const double & operator[](int64_t i) const        {return c[i];}

    // implemented with BLAS
	void zeroize(); // set to 0

	tensor1 & operator+=(const tensor1 & s);
    tensor1 & operator-=(const tensor1 & s);
    tensor1 & operator*=(double s);
    tensor1 & FMA (const tensor1 & s, double a);

    double operator*(const tensor1 & s) const; // dot (inner) product

    double norm() const;
};

const int64_t FLAG_CONST  = 1;
const int64_t FLAG_DUMMY  = 2;
const int64_t FLAG_SQUARE = 4;

// n x m    matrix
struct tensor2 {

    double * c2 = NULL;
    double ** c = NULL;

    int64_t   n,   m; // first and second tensor indices
	int64_t   ldm;    // leading dimensions of the array
	int64_t   flags = 0; // useful flags

	// initializers & shape setters
	tensor2();
	tensor2(int64_t p);
	tensor2(int64_t p, int64_t q);
	tensor2(int64_t p, double * v);
	tensor2(int64_t p, int64_t q, double * v); // used to create a mock object in order to access an array as a 2D object
    tensor2(int64_t p, const tensor2 & T, int64_t i0); // initialize a const reference to a subblock of the array

   ~tensor2();
    tensor2(const tensor2 & T);

    // C++11 move constructor
    tensor2(tensor2 && r) {
        c2  = r.c2;
        c   = r.c;
        n   = r.n;
        m   = r.m;
        ldm = r.ldm;
        flags = r.flags;

        r.c2 = NULL;
        r.c  = NULL;
        r.n = r.m = r.ldm = 0;
    }

	void setsize(int64_t p);
	void setsize(int64_t p, int64_t q);
	void setsize(int64_t p, int64_t q, double * v);

	void setsize(const tensor2 & R);

	void zeroize();
	void clear();

	void Transpose();
	void Symmetrize();
	void AntiSymmetrize();

	// access &  dereference
	inline double & operator()(int64_t i, int64_t j)
	    {return c[i][j];}
	inline double   operator()(int64_t i, int64_t j) const
        {return c[i][j];}

	inline double * operator[](int64_t i)
	    {return c[i];}
	inline const double * operator[](int64_t i) const
        {return c[i];}

	      double * operator* ()       {return c[0];}
	const double * operator* () const {return c[0];}

	tensor2 operator() (const int * i, const int * j, int N, int M) const; // return subblock

    // format conversion
	tensor2 & operator=(const tensor2 & st);
	tensor2 & operator=(const tensor  & st);
	tensor2 & operator=(const symtensor  & st);

	tensor2 & operator=(const symtensor2 & st);
	tensor2 & operator=(const Sparse & rhs);

	// basic numerical operations
    tensor2 & operator+=(const tensor2 & s);
    tensor2 & operator-=(const tensor2 & s);
    tensor2 & operator*=(double s);

    tensor2 & operator*=(const tensor2 & s);      // element-wise scaling  (Hadamard product)
    tensor2 & FMA (const tensor2 & s, double a);

    double operator*(const tensor2 & s) const; // vector-like inner product

	double trace() const;
	double frobenius() const;
};

std::ostream & operator<<(std::ostream & os, const tensor2 & T);

// symmetric n x n matrix; stores values in packed format
struct symtensor2 {

	double * c2;
	double ** c;
	int64_t n;
	int64_t n2;

	symtensor2 ();
	symtensor2 (int64_t m);
	symtensor2 (int64_t p, double * v);
   ~symtensor2 ();

    // C++11 move constructor
    symtensor2(symtensor2 && r) {
        c2  = r.c2;
        c   = r.c;
        n   = r.n;
        n2  = r.n2;

        r.c2 = NULL;
        r.c  = NULL;
        r.n = r.n2 = 0;
    }

	void setsize(int m);
	void zeroize();
	void clear();

	inline double & operator()(int i, int j) {
		if(i>=j) return c[i][j];
		else return c[j][i];
	}

	inline double operator()(int i, int j) const {
		if(i>=j) return c[i][j];
		else return c[j][i];
	}

    symtensor2 & operator= (double rhs);
    symtensor2 & operator+=(double rhs);
    symtensor2 & operator*=(double rhs);
    symtensor2 & operator+=(const symtensor2 & st);
    symtensor2 & operator-=(const symtensor2 & st);

    symtensor2 & operator= (const tensor2 & st);
    symtensor2 & operator= (const symtensor2 & st);
    symtensor2 & operator= (const symtensor  & st);
    symtensor2 & operator+=(const symtensor  & st);
    symtensor2 & operator+=(const tensor  & st);

    const symtensor2 operator*(const symtensor2 & rhs) const;

    symtensor2 & FMA (const symtensor2 & s, double a);

	double trace() const;
	double frobenius() const;
};

std::ostream & operator<<(std::ostream & os, const symtensor2 & T);

// 4-index tensor; only for basic access operations
struct tensor4 {

    tensor2 T;
    int n2;
    int n;
    int m2;
    int m;

    tensor4 () {
        m= m2 = n = n2 = 0;
    }

    tensor4 (int p) {
        Set(p,p);
    }

    tensor4 (int p, int q) {
        Set (p,q);
    }

    tensor4 (int p, int q, int r, int s) {
        Set (p,q,r,s);
    }

    void Set (int p) {
        Set(p,p);
    }

    void Set (int p, int q) {
        n = p;
        n2 = n*n;
        m = q;
        m2 = m*m;
        T.setsize(n2, m2);
        T.zeroize();
    }

    void Set (int p, int q, int r, int s) {
        n = q;
        n2 = p*q;
        m = s;
        m2 = r*s;
        T.setsize(n2, m2);
        T.zeroize();
    }

	inline double & operator()(int i, int j, int k, int l) {
		return T(i*n+j, k*m+l);
	}

	inline double operator()(int i, int j, int k, int l) const {
		return T(i*n+j, k*m+l);
	}

	inline double & operator()(int i, int j, int kl) {
		return T(i*n+j, kl);
	}

	inline double operator()(int i, int j, int kl) const {
		return T(i*n+j, kl);
	}

};

// 3-intex tensor
struct tensor3 {

    tensor2 T;
    tensor2 * R = NULL;

    int n, m, l;

    tensor3 () {
        m = l = n = 0;
        R = NULL;
    }

   ~tensor3 () {
        delete[] R;
    }

    tensor3 (int p) {
        Set(p,p,p);
    }

    void Set (int p) {
        Set(p,p,p);
    }

    tensor3 (int p, int q) {
        Set (p,q,q);
    }

    tensor3 (int p, int q, int r) {
        Set (p,q,r);
    }

    tensor3 (int p, int q, int r, double * v) {
        Set (p,q,r, v);
    }

    void Set (int p, int q, int r) {
        n = p;
        m = q;
        l = r;
        T.setsize (n, m*l);
        T.zeroize();
        R = new tensor2[n];

        for (int i=0; i<n; ++i) R[i].setsize(m,l, T[i]);
    }

    void Set (int p, int q, int r, double * v) {
        n = p;
        m = q;
        l = r;
        T.setsize (n, m*l, v);
        R = new tensor2[n];

        for (int i=0; i<n; ++i) R[i].setsize(m,l, T[i]);
    }

    tensor3 & operator=(const tensor3 & rhs) {
        Set(rhs.n, rhs.m, rhs.l);
        T = rhs.T;
        return *this;
    }

    // sum of direct products
    tensor2 operator*(const tensor3 & rhs) const {
        tensor2 P(m,l);
        P.zeroize();

        // incompatible shapes
        if ((m!=rhs.m)||(l!=rhs.l)) {
            return P;
        }

        const int K = n<=rhs.n?n:rhs.n; //min(n,rhs.n);

        for (int k=0; k<K; ++k) {
            for (int ij=0; ij<m*l; ++ij) P[0][ij] += R[k][0][ij] * rhs.R[k][0][ij];
        }

        return P;
    }

	inline double & operator()(int p, int q, int r) {
		return T(p, q*l+r);
	}

	inline double operator()(int p, int q, int r) const {
		return T(p, q*l+r);
	}

	inline double & operator()(int p, int qr) {
		return T(p, qr);
	}

	inline double operator()(int p, int qr) const {
		return T(p, qr);
	}

	const tensor2 & operator[] (int p) const {
        return R[p];
	}

	tensor2 & operator[] (int p) {
        return R[p];
	}
};

// broadcast / allreduce functions for MPI parallel runs
void Tensor2Broadcast (   tensor2 & T);
void Tensor2Broadcast (symtensor2 & T);
void Tensor2Reduce    (   tensor2 & T);
void Tensor2Reduce    (symtensor2 & T);

#endif
