
#ifndef __NTENSORS__
#define __NTENSORS__

#include <inttypes.h>

// needed for member functions
class Ntensor;
class rAtom;
class tensor2;

// these are part of the PIMPL
class pNpattern;
class pNtensor;
class pxArray;
class pNsub;
class pNSsub;

class xArray {
    friend class pNtensor;

  private:
    pxArray * p;

  public:
    xArray();
   ~xArray();
    xArray(pxArray * r);
    double & operator[] (int n);
};

class Npattern {
    friend class Ntensor;

  private:
    pNpattern * p; // PIMPL idiom

  public:
    Npattern();
   ~Npattern();
    void Set (const rAtom * Atoms, int nAtoms); // set the internal structure of the tensor

    // getters
    virtual uint32_t GetAtoms () const;
    virtual uint32_t GetFuncs () const;
    virtual uint8_t  GetFuncs (uint8_t  elem) const;
    virtual uint8_t  GetID    (uint32_t atom) const;
    virtual uint8_t  GetNIDs  () const;
    virtual uint32_t GetLen      (uint8_t  elem, uint8_t ebss) const;
    virtual uint32_t GetLen      (uint32_t atom) const;
    virtual uint32_t GetTotalLen () const;
};

class ConstNpattern:public Npattern {
    friend class Ntensor;

  private:
    const pNpattern * p; // PIMPL idiom

  public:
    ConstNpattern();
   ~ConstNpattern();

    // const getters
    uint32_t GetAtoms () const;
    uint32_t GetFuncs () const;
    uint8_t  GetFuncs (uint8_t  elem) const;
    uint8_t  GetID    (uint32_t atom) const;
    uint8_t  GetNIDs  () const;
    uint32_t GetLen   (uint8_t  elem, uint8_t ebss) const;
};


class Ntensor {

  protected:
    pNtensor * p; // PIMPL idiom

  public:
    Ntensor(const Npattern * T);
   ~Ntensor();
    //const ConstNpattern * GetPattern() const;
    const ConstNpattern GetPattern() const;
};

// a structure for constant access
class NTconst:public Ntensor {

    // member functions necessary for const access
  public:
    NTconst(const Npattern * T) : Ntensor(T) {};

    void     Set (const tensor2 * T2, int N=1);             // initialize the internal data by copying an array of matrices
    void     SetS (const tensor2 * T2, int N=1);             // initialize the internal data by copying an array of matrices
    void     SetA (const tensor2 * T2, int N=1);             // initialize the internal data by copying an array of matrices
    void     Put (      tensor2 * T2) const;
    const double * operator()
                 (uint32_t at1, uint32_t at2,  uint8_t eb1, uint8_t eb2) const; // get a cpointer to const mem
};

// a dynamuc structure for multithreaded partial updates
class NTmulti:public Ntensor {

    // member functions necessary for modification
  public:

    NTmulti(const Npattern * T) : Ntensor(T) {};

    void     Set (int N=1);                                               // pass the pattern and the number of matrices to be set
    void     AddTo (tensor2 * T2) const;                                          // extract the tensors (blocks all other operations)
    void     AddTo (tensor2 & T2, int n) const;

    pNsub * operator()(uint8_t e1, uint8_t e2, uint8_t b1, uint8_t b2);

    double * const operator()
                 (uint32_t at1, uint32_t at2,  uint8_t eb1, uint8_t eb2); // get a fresh chunk of memory (nonblocking)

    pxArray * operator()
                 (uint32_t at1, uint32_t at2,  uint8_t eb1, uint8_t eb2, int y); // get a fresh chunk of memory (nonblocking)

    void free (uint32_t at1, uint32_t at2,  uint8_t b1, uint8_t b2, double * const r); // free the pointer

    //NTmulti & operator<< (double * p);                                    // commit the changes to the pointer; may periodically perform blocking maintenance work
    //void     Commit (double * p);                                         // commit the changes done to the double array to the index (nonblocking)
    //void     Compress();                                                  // sum all commits to the structure (blocking wrt other Compress() calls)
};

class NTsub {

  private:
    pNsub * p;

  public:
    NTsub();
   ~NTsub();
    NTsub(pNsub * r);

    pxArray * operator() (uint32_t at1, uint32_t at2); // get a fresh chunk of memory (nonblocking)
};

class NTSsub {

  private:
    pNSsub * p;

  public:
    NTSsub();
   ~NTSsub();
    NTSsub(pNSsub * r);

    pxArray * operator() (uint32_t at1, uint32_t at2); // get a fresh chunk of memory (nonblocking)
};


#endif
