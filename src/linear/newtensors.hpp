#ifndef __NEWTENSORS__
#define __NEWTENSORS__
#include <iostream>
#include <cmath>
#include "tensors.hpp"
#include "low/rtensors.hpp"
typedef unsigned int USI;

struct tensor;
struct symtensor;
struct Interactions;

// this is global state and makes no sense whatsoever
// what we need instead is a factory method

class tensorpattern {
    friend class tensor;
    friend class symtensor;
    friend class tensor2;
    friend class symtensor2;

  public:
    Array<int> pos;
    uint64_t n2;
    uint64_t n2s;
    uint64_t totn;
    double thresh;

  public:
    void SetPositions(const Array<int> & p, double t) {
        pos = p;
        int n = pos.n;
        n2  = n*n;
        n2s = (n*n+n)/2;
        totn = 0;
        for (int i=0;i<n;++i)
            totn += pos[i];

        thresh = t;
    }

    uint64_t getn() const {
        return pos.n;
    }

    uint64_t gettotn() const {
        return totn;
    }

    uint64_t getpos(int i) const {
        return pos[i];
    }
};


class tensor: public tensorpattern{
    friend class symtensor;
    friend class Sparse;
    friend class tensor2;
    friend class symtensor2;

    friend std::ostream & operator<<(std::ostream & os, const tensor & T);

  public: //private:
	tensor2 *** c;
	tensor2 ** c2;
	//Interactions Pattern;

  public:

	tensor();
   ~tensor();
	void init();
    void init(const tensorpattern & TP);
    void clear();

    void zeroize();
    void trim();
    void trim(double thrsh);
    void trim(const tensor & s);
    void trim(const symtensor & s);
    tensor2 * touch(int i, int j);
    void touch(const Interactions & AtomInteractions);


	inline tensor2 * operator()(int i, int j) {
		return c[i][j];
	}

	inline const tensor2 * operator()(int i, int j) const {
		return c[i][j];
	}

	inline double & operator()(int i, int j, int a, int b) {
	    touch(i,j);
		return (*c[i][j])(a,b);
	}

	inline double operator()(int i, int j, int a, int b) const {
	    if (c[i][j]==NULL) return 0;
		return (*c[i][j])(a,b);
	}

	/*
	const double * operator[](int i, int j) const {
	}
	double * operator[](int i, int j) {
	}
	*/

    tensor & operator=(const tensor & s);
    tensor & operator=(const symtensor & s);
    tensor & operator=(const tensor2 & s);
    tensor & operator=(const symtensor2 & s);
    tensor & operator=(const Sparse & s);

    tensor & operator+=(const tensor & s);
    tensor & operator-=(const tensor & s);

    tensor & operator+=(double rhs);
    tensor & operator=(double rhs);

    template <class S> tensor & operator*=(const S & s) {
        for (int i=0;i<n2;++i)
            if (c2[i]!=NULL)
                (*c2[i]) *= s;
        return *this;
    }

    //products
    const tensor    operator*(const tensor & s) const;
    const tensor    operator*(const symtensor & s) const;
    const symtensor operator/(const tensor & rhs) const;
    const tensor    operator/(const symtensor & rhs) const;

};

std::ostream & operator<<(std::ostream & os, const tensor & T);


/*
    a better implementation of this is to just hold the subblock copy
    on either side; in fact this can be done with just a 'tensor' and some extra symmetrization flag
    on a related note, this can also be used to implement an antisymmetric tensor object needed for BED integrals
*/


class symtensor: public tensorpattern {
    friend class tensor;

    friend class tensor2;
    friend class symtensor2;

    friend std::ostream & operator<<(std::ostream & os, const symtensor & T);

  public: //private:
	tensor2 *** c;
	tensor2 ** c2;
    //Interactions Pattern;

  public:
	symtensor();
   ~symtensor();
	void init();
    void init(const tensorpattern & TP);
    void clear();
    void zeroize();
    void trim();
    void trim(double thrsh);
    void trim(const symtensor & s);

    tensor2 * touch(int i, int j);
    void touch(const Interactions & AtomInteractions);


	inline tensor2 * operator()(int i, int j) {
		return c[i][j];
	}

	inline const tensor2 * operator()(int i, int j) const {
		return c[i][j];
	}

	inline double & operator()(int i, int j, int a, int b) {

	    touch(i,j);

	    if (i>j)
		    return (*c[i][j])(a,b);
        else if (i<j)
            return (*c[j][i])(b,a);
        else if (a>=b)
            return (*c[i][i])(a,b);
        else
            return (*c[i][i])(b,a);
	}

	inline double operator()(int i, int j, int a, int b) const {
        if (i>=j) {
		    if (c[i][j]==NULL) return 0;
        }
        else {
		    if (c[j][i]==NULL) return 0;
        }

	    if (i>j)
		    return (*c[i][j])(a,b);
        else if (i<j)
            return (*c[j][i])(b,a);
        else if (a>=b)
            return (*c[i][i])(a,b);
        else
            return (*c[i][i])(b,a);
	}

	/*
    const double * operator[](int i, int j) const {
	}
	double * operator[](int i, int j) {
	}
	*/

    symtensor & operator=(const symtensor & s);
    symtensor & operator=(const tensor & s);
    symtensor & operator=(const symtensor2 & s);
    symtensor & operator=(const tensor2 & s);
    symtensor & operator+=(const symtensor & st);
    symtensor & operator+=(const tensor & st); //special upper+lower contraction
    symtensor & operator+=(const tensor2 & st); //special upper+lower contraction
    symtensor & operator-=(const symtensor & st);

    symtensor & operator=(double rhs);
    symtensor & operator+=(double rhs);

    template <class S> symtensor & operator*=(const S & st) {
        for (int i=0;i<n2s;++i) {
            if (c2[i]!=NULL)
                (*c2[i]) *= st;
        }
        return *this;
    }

     //products
    const tensor    operator*(const tensor    & s) const;
    const symtensor operator*(const symtensor & s) const;

    const tensor    operator/(const tensor    & s) const;

    //misleading, but better than calling a function
    //symtensor & operator/(const tensor & s) const;
    //commutative matrices only!!!
    //symtensor & operator/(const symtensor & s) const;
};

std::ostream & operator<<(std::ostream & os, const symtensor & T);

const symtensor BlockedProjection(const tensor & C, const symtensor & S);

double Contract(const symtensor & A, const symtensor & B);

#endif
