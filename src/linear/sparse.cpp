/*
    sparse.cpp

    definicion de matrices sparse y operaciones comunes
*/

#include <cmath>
#include "low/io.hpp"

#include "basis/GTO.hpp"

#include "molecule/atoms.hpp"
#include "math/angular.hpp"

#include "tensors.hpp"
#include "sparse.hpp"

using namespace std;
using namespace LibAngular;

SparseTensorBlockPattern & SparseTensorBlockPattern::operator=(const SparseTensorBlockPattern & BFpattern) {
    if (this == &BFpattern) return *this;

    thresh  = BFpattern.thresh;
    size    = BFpattern.size;
    nblocks = BFpattern.nblocks;

    blsize  = new int[nblocks];
    blpoint = new int[nblocks];

    for (int i=0; i<nblocks; ++i) {
        blsize[i]  = BFpattern.blsize[i];
        blpoint[i] = BFpattern.blpoint[i];
    }

    clocks = new slock[nblocks];
    flocks = new slock[nblocks];

    return *this;
}

void SparseTensorBlockPattern::Init (Nucleus * nuclei, int nnuclei, const GTObasis & BasisSet, double BFthresh)  {
    //guarda los punteros a los bloques de funciones
    nblocks = 0;
    for (int at=0; at<nnuclei; ++at)
        nblocks += BasisSet[nuclei[at].type].n;

    blsize  = new int[nblocks];
    blpoint = new int[nblocks];

    int os = 0;
    size = 0;
    int os1 = 0;

    for (int at=0; at<nnuclei; ++at) {
		int id = nuclei[at].type;

        for (int b=0; b<BasisSet[id].n; ++b) {
            int l = BasisSet[id][b].l;
            blpoint[os] = os1;
            blsize[os] = nmC[l];
            ++os;
            os1  += nmC[l];
            size += nmC[l];
        }
    }

	thresh = BFthresh;

    clocks = new slock[nblocks];
    flocks = new slock[nblocks];
}



SparseTensor2::SparseTensor2() {
    filc = NULL;
    colc = NULL;
}

SparseTensor2::SparseTensor2( SparseTensorBlockPattern & pat) {
    pattern = &pat;
    int nb = pat.nblocks;
    nblocks = nb;
    filc  = new AVLTree<tensor2*>[nb];
    colc  = new AVLTree<tensor2*>[nb];

    //inicia los errores
    #pragma omp parallel for
    for (int i=0; i<nb; ++i) {
        filc[i].SetError(NULL);
        colc[i].SetError(NULL);
    }
}

void SparseTensor2::set(SparseTensorBlockPattern & pat) {
    pattern = &pat;
    int nb = pat.nblocks;
    nblocks = nb;
    filc  = new AVLTree<tensor2*>[nb];
    colc  = new AVLTree<tensor2*>[nb];

    //inicia los errores
    #pragma omp parallel for
    for (int i=0; i<nb; ++i) {
        filc[i].SetError(NULL);
        colc[i].SetError(NULL);
    }
}

//borra la informacion del tensor excepto su estructura basica
void SparseTensor2::clear() {
    //elimina los tensores
    #pragma omp parallel for schedule(dynamic)
    for ( int i=0; i<nblocks; ++i) {
        for ( int p=0; p<filc[i].nkeys; ++p) {
            tensor2 * todel = filc[i][p].key;

            todel->clear();
            delete todel; //se carga el tensor (el operador [] esta sobrecargado)
        }
        filc[i].Clear();
        colc[i].Clear();
    }
}

//destruye el resto del tensor
SparseTensor2::~SparseTensor2() {
    if (filc  != NULL) delete[] filc;
    if (colc  != NULL) delete[] colc;
}

SparseTensor2 & SparseTensor2::operator=(const SparseTensor2 & spt) {
    if (this == &spt) return *this;

    nblocks = spt.nblocks;

    pattern = NULL; //de momento no sirve para nada

    if (filc != NULL) delete[] filc;
    if (colc != NULL) delete[] colc;

    filc  = new AVLTree<tensor2*>[nblocks];
    colc  = new AVLTree<tensor2*>[nblocks];

    //los arboles AVL tienen un maximo de 'nbocks' bloques
    int     *  iarray = new int    [nblocks];
    tensor2 ** tarray = new tensor2*[nblocks];
    tensor2 ** narray = new tensor2*[nblocks];


    //copia los elementos de cada bloque
    for (int i=0; i<nblocks; ++i) {
        int nkeys = spt.filc[i].nkeys;
        spt.filc[i].Dump(iarray, tarray);

        //copia la informacion de los tensores
        for (int t=0; t<nkeys; ++t) {
            narray[t] = new tensor2;
            *narray[t] = *tarray[t];
        }

        filc[i].FromArrays(iarray, narray, nkeys);

        //pasa la informacion a las columnas
        for (int t=0; t<nkeys; ++t)
            colc[iarray[t]].AddKey(t, narray[t]);


        //introduce el key que indica error de acceso
        filc[i].SetError(NULL);
        colc[i].SetError(NULL);
    }

    delete[] iarray;
    delete[] tarray;

    return *this;
}


//multiplicacion en O(N)
//producto sparse; cuidado; no comprueba que los tensores sean compatibles
void SparseTensorProd(SparseTensor2 & R, SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int k=0; k<nb; ++k) {
        for ( int p=0; p<A.colc[k].nkeys; ++p) {
            for ( int q=0; q<B.filc[k].nkeys; ++q) {
                int i = A.colc[k][p].num;
                int j = B.filc[k][q].num;

                int n = pattern.blsize[i]; //A.bcol[k][p]->n;
                int l = pattern.blsize[k]; //A.bcol[k][p]->m;
                int m = pattern.blsize[j]; //B.bfil[k][q]->m;

                pattern.flocks[i].lock();

                tensor2 * Rt = R.filc[i](j);

                if (Rt == NULL) {
                    Rt = new tensor2;
                    Rt->setsize(n, m);
                    Rt->zeroize();

                    //a�ade a la estructura
                    R.filc[i].AddKey(j, Rt);
                }

                tensor2 * At = A.colc[k][p].key;
                tensor2 * Bt = B.filc[k][q].key;

                //producto, propiamente dicho
                for (int cn=0; cn<n; ++cn) {
                    for (int cm=0; cm<m; ++cm) {
                        double sum=0.;
                        for (int cl=0; cl<l; ++cl)
                            sum += (*At)(cn,cl) * (*Bt)(cl,cm);
                        (*Rt)(cn,cm) += sum;
                    }
                }

                pattern.flocks[i].unlock();

            }
        }
    }

    //comprueba bloque por bloque si son inferiores a 'thresh'
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        int nkeys = R.filc[i].nkeys; //al borrar un elemento cambia, pero no los numeros siguen igual

        int p=0;

        while(p<nkeys) {
            tensor2 * thetensor = R.filc[i][p].key;
            int               j = R.filc[i][p].num;

            bool todel = true;

            for (int s=0; s<thetensor->n; ++s)
                for (int t=0; t<thetensor->m; ++t)
                    if ( fabs((*thetensor)(s,t)) > pattern.thresh ) todel = false;

            if (todel) {
				R.filc[i][p].key = NULL;
                R.filc[i].Delete(j); //no es necesario hacer lock
                thetensor->clear();
                delete thetensor;
                --nkeys;
            }
            else {
                pattern.clocks[j].lock();
                R.colc[j].AddKey(i,thetensor);
                pattern.clocks[j].unlock();
                ++p;
            }
        }

    }

}


//multiplicacion en O(N); At * B
void SparseTensorProdTN(SparseTensor2 & R, SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int k=0; k<nb; ++k) {
        for ( int p=0; p<A.filc[k].nkeys; ++p) {
            for ( int q=0; q<B.filc[k].nkeys; ++q) {
                int i = A.filc[k][p].num;
                int j = B.filc[k][q].num;

                int n = pattern.blsize[i]; //A.bcol[k][p]->n;
                int l = pattern.blsize[k]; //A.bcol[k][p]->m;
                int m = pattern.blsize[j]; //B.bfil[k][q]->m;

                pattern.flocks[i].lock();

                tensor2 * Rt = R.filc[i](j);

                if (Rt == NULL) {
                    Rt = new tensor2;
                    Rt->setsize(n, m);
                    Rt->zeroize();

                    //a�ade a la estructura
                    R.filc[i].AddKey(j, Rt);
                }

                tensor2 * At = A.filc[k][p].key;
                tensor2 * Bt = B.filc[k][q].key;

                //producto, propiamente dicho
                for (int cn=0; cn<n; ++cn) {
                    for (int cm=0; cm<m; ++cm) {
                        double sum=0.;
                        for (int cl=0; cl<l; ++cl)
                            sum += (*At)(cl,cn) * (*Bt)(cl,cm);
                        (*Rt)(cn,cm) += sum;
                    }
                }

                pattern.flocks[i].unlock();

            }
        }
    }

    //comprueba bloque por bloque si son inferiores a 'thresh'
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        int nkeys = R.filc[i].nkeys; //al borrar un elemento cambia, pero no los numeros siguen igual

        int p=0;

        while(p<nkeys) {
            tensor2 * thetensor = R.filc[i][p].key;
            int j = R.filc[i][p].num;

            bool todel = true;

            for (int s=0; s<thetensor->n; ++s)
                for (int t=0; t<thetensor->m; ++t)
                    if ( fabs((*thetensor)(s,t)) > pattern.thresh ) todel = false;

            if (todel) {
                thetensor->clear();
                delete thetensor;
                R.filc[i].Delete(j);
                --nkeys;
            }
            else {
                pattern.clocks[j].lock();
                R.colc[j].AddKey(i,thetensor);
                pattern.clocks[j].unlock();
                ++p;
            }
        }

    }

}

//multiplicacion en O(N); A * Bt
void SparseTensorProdNT(SparseTensor2 & R, SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int k=0; k<nb; ++k) {
        for ( int p=0; p<A.colc[k].nkeys; ++p) {
            for ( int q=0; q<B.colc[k].nkeys; ++q) {
                int i = A.colc[k][p].num;
                int j = B.colc[k][q].num;

                int n = pattern.blsize[i]; //A.bcol[k][p]->n;
                int l = pattern.blsize[k]; //A.bcol[k][p]->m;
                int m = pattern.blsize[j]; //B.bfil[k][q]->m;

                pattern.flocks[i].lock();

                tensor2 * Rt = R.filc[i](j);

                if (Rt == NULL) {
                    Rt = new tensor2;
                    Rt->setsize(n, m);
                    Rt->zeroize();

                    //a�ade a la estructura
                    R.filc[i].AddKey(j, Rt);
                }

                tensor2 * At = A.colc[k][p].key;
                tensor2 * Bt = B.colc[k][q].key;

                //producto, propiamente dicho
                for (int cn=0; cn<n; ++cn) {
                    for (int cm=0; cm<m; ++cm) {
                        double sum=0.;
                        for (int cl=0; cl<l; ++cl)
                            sum += (*At)(cn,cl) * (*Bt)(cm,cl);
                        (*Rt)(cn,cm) += sum;
                    }
                }

                pattern.flocks[i].unlock();
            }
        }
    }

    //comprueba bloque por bloque si son inferiores a 'thresh'
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        int nkeys = R.filc[i].nkeys; //al borrar un elemento cambia, pero no los numeros siguen igual

        int p=0;

        while(p<nkeys) {
            tensor2 * thetensor = R.filc[i][p].key;
            int j = R.filc[i][p].num;

            bool todel = true;

            for (int s=0; s<thetensor->n; ++s)
                for (int t=0; t<thetensor->m; ++t)
                    if ( fabs((*thetensor)(s,t)) > pattern.thresh ) todel = false;

            if (todel) {
                thetensor->clear();
                delete thetensor;
                R.filc[i].Delete(j);
                --nkeys;
            }
            else {
                pattern.clocks[j].lock();
                R.colc[j].AddKey(i,thetensor);
                pattern.clocks[j].unlock();
                ++p;
            }
        }

    }

}


//tensor a sparse
void Tensor2Sparse(SparseTensor2 & R, symtensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for (int j=0; j<nb; ++j) {
            //comprueba si el bloque se debe a�adir o no
            bool isempty = true;
            for ( int p=0; p<pattern.blsize[i]; ++p)
                for ( int q=0; q<pattern.blsize[j]; ++q)
                    if (fabs(A(pattern.blpoint[i]+p, pattern.blpoint[j]+q))>pattern.thresh) isempty = false;

            //a�ade el bloque a la estructura sparse
            if (!isempty) {
                tensor2 * tmp = new tensor2(pattern.blsize[i], pattern.blsize[j]);

				for ( int p=0; p<pattern.blsize[i]; ++p)
                    for ( int q=0; q<pattern.blsize[j]; ++q)
						(*tmp)(p,q) = A(pattern.blpoint[i]+p, pattern.blpoint[j]+q);

                R.filc[i].AddKey(j, tmp);

                pattern.clocks[j].lock();
                R.colc[j].AddKey(i, tmp);
                pattern.clocks[j].unlock();
            }
        }
    }

}

//pasa una matriz sparse a un tensor denso
void Sparse2Tensor(tensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    R.zeroize();

    #pragma omp parallel for schedule(dynamic)
    for ( int i=0; i<nb; ++i) {
        for ( int a=0; a<A.filc[i].nkeys; ++a) {
            int j = A.filc[i][a].num;

            tensor2 & thetensor = *(A.filc[i][a].key);

            for ( int p=0; p<pattern.blsize[i]; ++p)
                for ( int q=0; q<pattern.blsize[j]; ++q)
                    R(pattern.blpoint[i]+p, pattern.blpoint[j]+q) = thetensor(p,q);
        }
    }
}

//pasa una matriz sparse a un tensor denso
void Sparse2Tensor(symtensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    R.zeroize();

    #pragma omp parallel for schedule(dynamic)
    for ( int i=0; i<nb; ++i) {
        for ( int a=0; a<A.filc[i].nkeys; ++a) {
            int j = A.filc[i][a].num;

            tensor2 & thetensor = *(A.filc[i][a].key);

            for ( int p=0; p<pattern.blsize[i]; ++p)
                for ( int q=0; q<pattern.blsize[j]; ++q)
                    R(pattern.blpoint[i]+p, pattern.blpoint[j]+q) = thetensor(p,q);
        }
    }
}

//tensor a sparse
void Tensor2Sparse(SparseTensor2 & R, tensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for (int j=0; j<nb; ++j) {
            //comprueba si el bloque se debe a�adir o no
            bool isempty = true;
            for ( int p=0; p<pattern.blsize[i]; ++p)
                for ( int q=0; q<pattern.blsize[j]; ++q)
                    if (fabs(A(pattern.blpoint[i]+p, pattern.blpoint[j]+q))>pattern.thresh) isempty = false;

            //a�ade el bloque a la estructura sparse
            if (!isempty) {
                tensor2 * tmp = new tensor2;

                tmp->setsize(pattern.blsize[i], pattern.blsize[j]);

				for ( int p=0; p<pattern.blsize[i]; ++p)
                    for ( int q=0; q<pattern.blsize[j]; ++q)
						(*tmp)(p,q) = A(pattern.blpoint[i]+p, pattern.blpoint[j]+q);

                R.filc[i].AddKey(j, tmp);

                pattern.clocks[j].lock();
                R.colc[j].AddKey(i, tmp);
                pattern.clocks[j].unlock();
            }
        }
    }

}

/*
//tensor a sparse
void Tensor2Sparse(SparseSymTensor2 & R, symtensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for (int j=0; j<nb; ++j) {
            //comprueba si el bloque se debe a�adir o no
            bool isempty = true;
            for ( int p=0; p<pattern.blsize[i]; ++p)
                for ( int q=0; q<pattern.blsize[j]; ++q)
                    if (fabs(A(pattern.blpoint[i]+p, pattern.blpoint[j]+q))>pattern.thresh) isempty = false;

            //a�ade el bloque a la estructura sparse
            if (!isempty) {
                tensor2 * tmp = new tensor2;

                tmp->setsize(pattern.blsize[i], pattern.blsize[j]);

				for ( int p=0; p<pattern.blsize[i]; ++p)
                    for ( int q=0; q<pattern.blsize[j]; ++q)
						(*tmp)(p,q) = A(pattern.blpoint[i]+p, pattern.blpoint[j]+q);

                R.filc[i].AddKey(j, tmp);

                pattern.clocks[j].lock();
                R.colc[j].AddKey(i, tmp);
                pattern.clocks[j].unlock();
            }
        }
    }

}

//pasa una matriz sparse a un tensor denso
void Sparse2Tensor(tensor2 & R, SparseSymTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    R.zeroize();

    #pragma omp parallel for schedule(dynamic)
    for ( int i=0; i<nb; ++i) {
        for ( int a=0; a<A.filc[i].nkeys; ++a) {
            int j = A.filc[i][a].num;

            tensor2 & thetensor = *(A.filc[i][a].key);

            for ( int p=0; p<pattern.blsize[i]; ++p)
                for ( int q=0; q<pattern.blsize[j]; ++q)
                    R(pattern.blpoint[i]+p, pattern.blpoint[j]+q) = thetensor(p,q);
        }
    }
}
*/


//copia (duplica) la informacion de un tensor 'sparse' a otro
void SparseTensorCopy (SparseTensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for ( int p=0; p<A.colc[i].nkeys; ++p) {
            int j = A.colc[i][p].num;
            tensor2 * T = A.colc [i](j);

			int n = pattern.blsize[j];
			int m = pattern.blsize[i];

            tensor2 * U = new tensor2(n,m);

            //copia (hard copy)
            for (int s=0; s<n; ++s)
                for (int t=0; t<m; ++t)
                    (*U)(s,t) = (*T)(s,t);

            R.colc[i].AddKey(j, U);

            pattern.flocks[j].lock();
            R.filc[j].AddKey(i, U);
            pattern.flocks[j].unlock();
        }
    }
}


//R = A; A = 0;
void SparseTensorCopyDel (SparseTensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    R.clear();
	AVLTree<tensor2*> * tmp;

	tmp = R.filc;
    R.filc = A.filc;
    A.filc = tmp;

	tmp = R.colc;
    R.colc = A.colc;
    A.colc = tmp;
}


void SparseTensorProd (SparseTensor2 & R, double v, SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for ( int p=0; p<A.colc[i].nkeys; ++p) {
            int j = A.colc[i][p].num;
            tensor2 * T = A.colc [i](j);

			int n = pattern.blsize[j];
			int m = pattern.blsize[i];

            tensor2 * U = new tensor2(n,m);

            //copia (hard copy)
            for (int s=0; s<n; ++s)
                for (int t=0; t<m; ++t)
                    (*U)(s,t) = v*(*T)(s,t);

            R.colc[i].AddKey(j, U);

            pattern.flocks[j].lock();
            R.filc[j].AddKey(i, U);
            pattern.flocks[j].unlock();
        }
    }
}

//R *= v
void SparseTensorProd (SparseTensor2 & R, double v, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for ( int p=0; p<R.colc[i].nkeys; ++p) {
            int j = R.colc[i][p].num;
            tensor2 * T = R.colc [i](j);

			int n = pattern.blsize[j];
			int m = pattern.blsize[i];

            for (int s=0; s<n; ++s)
                for (int t=0; t<m; ++t)
                    (*T)(s,t) *= v;
        }
    }
}

// R += v*I;
void SparseTensorAddDiagonal (SparseTensor2 & R, double v, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        tensor2 * U = R.colc[i](i); //con filc hay un error raro en openMP!!! (ICL 10.1.020)
        int n = pattern.blsize[i];

        if (U != NULL) {
            for (int p=0; p<n; ++p)
                (*U)(p,p) += v;
        }
        else {
            U = new tensor2(n);
            U->zeroize();

            for (int p=0; p<n; ++p)
                (*U)(p,p) = v;

            //no hace falta
            R.colc[i].AddKey(i, U);
            R.filc[i].AddKey(i, U);
        }
    }
}

// R += A;
void SparseTensorAdd (SparseTensor2 & R, SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for ( int p=0; p<A.colc[i].nkeys; ++p) {
            int j = A.colc[i][p].num;
            tensor2 * T = A.colc [i](j);

			int n = pattern.blsize[j];
			int m = pattern.blsize[i];

            tensor2 * U = R.colc [i](j);

            if (U != NULL) {
                for (int s=0; s<n; ++s)
                    for (int t=0; t<m; ++t)
                        (*U)(s,t) += (*T)(s,t);
            }
            else {
                U = new tensor2(n,m);

                for (int s=0; s<n; ++s)
                    for (int t=0; t<m; ++t)
                        (*U)(s,t) = (*T)(s,t);

                R.colc[i].AddKey(j, U);

                pattern.flocks[j].lock();
                R.filc[j].AddKey(i, U);
                pattern.flocks[j].unlock();
            }

        }
    }

}

// R += lambda * (A-R);
void SparseTensorCombination (SparseTensor2 & R, SparseTensor2 & A, double lambda, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        for ( int p=0; p<A.colc[i].nkeys; ++p) {
            int j = A.colc[i][p].num;
            tensor2 * T = A.colc [i](j);

			int n = pattern.blsize[j];
			int m = pattern.blsize[i];

            tensor2 * U = R.colc [i](j);

            if (U != NULL) {
                for (int s=0; s<n; ++s)
                    for (int t=0; t<m; ++t)
                        (*U)(s,t) += lambda*( (*T)(s,t) - (*U)(s,t) );
            }
            else {
                U = new tensor2(n,m);

                for (int s=0; s<n; ++s)
                    for (int t=0; t<m; ++t)
                        (*U)(s,t) = lambda*(*T)(s,t);

                R.colc[i].AddKey(j,U);

                pattern.flocks[j].lock();
                R.filc[j].AddKey(i, U);
                pattern.flocks[j].unlock();
            }

        }
    }

}

double SparseTensorContract (SparseTensor2 & A, SparseTensor2 & B, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for
    double tot = 0.;

    //ciclo sobre todos los bloques
    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        double tot2 = 0;
        for ( int p=0; p<A.colc[i].nkeys; ++p) {
            int j = A.colc[i][p].num;
            tensor2 * T = A.colc [i](j);
            tensor2 * U = B.colc [i](j);

            if (U != NULL) {
    	        int n = pattern.blsize[j];
				int m = pattern.blsize[i];

                for (int s=0; s<n; ++s)
                    for (int t=0; t<m; ++t)
                        tot2 += (*U)(s,t) * (*T)(s,t);
            }

        }

        #pragma omp atomic
        tot += tot2;
    }

    return tot;
}


void SparseTensorPlot(SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    tensor2 tmp(pattern.size);
    tmp.zeroize();

    //ciclo sobre todos los bloques
    for (int i=0; i<pattern.nblocks; ++i) {
        for ( int p=0; p<A.filc[i].nkeys; ++p) {
            int j = A.filc[i][p].num;
            tensor2 * T = A.filc[i](j);

			int n = pattern.blsize[i];
			int m = pattern.blsize[j];

            for (int s=0; s<n; ++s)
                for (int t=0; t<m; ++t)
                    tmp(pattern.blpoint[i]+s, pattern.blpoint[j]+t) = (*T)(s,t);
        }
    }

    for (int i=0; i<pattern.size; ++i) {
        for (int j=0; j<pattern.size; ++j)
            Messenger << tmp(i,j) << " ";
        Messenger << endl << endl;
    }

    tmp.clear();
}


//extrae el componente simetrico de la matriz
void SparseTensorSymmetrize(SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for

    //ciclo sobre todos los bloques
    //OTRO BUG DEL COMPILADOR DE INTEL ???????
    //#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        pattern.flocks[i].lock();
        int pmax = A.filc[i].nkeys;
        pattern.flocks[i].unlock();

        for ( int p=0; p<pmax; ++p) {
            pattern.flocks[i].lock();
            int j = A.filc[i][p].num;
            tensor2 * T  = A.filc[i](j);
            pattern.flocks[i].unlock();

			if (j>i)
				break;
			else if (i>j) {
			    pattern.flocks[j].lock();
                tensor2 * Ts = A.filc[j](i);
			    pattern.flocks[j].unlock();

		        int n = pattern.blsize[i];
			    int m = pattern.blsize[j];

    			if (Ts == NULL) {
	    			Ts = new tensor2(m,n);

	    			pattern.clocks[i].lock();
					A.colc[i].AddKey(j,Ts);
					pattern.clocks[i].unlock();

					pattern.flocks[j].lock();
			    	A.filc[j].AddKey(i,Ts);
                    pattern.flocks[j].unlock();

                    for (int s=0; s<n; ++s)
                        for (int t=0; t<m; ++t)
		                    (*Ts)(t,s) = (*T)(s,t) = (*T)(s,t)/2; //para no liarla con el left-2-right o viceversa
    			}
    			else {
                    for (int s=0; s<n; ++s)
                        for (int t=0; t<m; ++t)
		                    (*Ts)(t,s) = (*T)(s,t) = ((*T)(s,t) + (*Ts)(t,s))/2; //para no liarla con el left-2-right o viceversa
    			}

			}
			else {
                int n = pattern.blsize[i];

                for (int s=0; s<n; ++s)
                    for (int t=0; t<s; ++t)
			    		(*T)(t,s) = (*T)(s,t) = ((*T)(t,s) + (*T)(s,t))/2;
			}
        }
    }
}

//copia la mitad de la informacion en una matriz simetrica
void SparseTensorSymmFill(SparseTensor2 & A, const SparseTensorBlockPattern & pattern) {
    int nb = pattern.nblocks; //for omp's for
    //ciclo sobre todos los bloques

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<nb; ++i) {
        pattern.flocks[i].lock();
        int pmax = A.filc[i].nkeys;
        pattern.flocks[i].unlock();

        for ( int p=0; p<pmax; ++p) {
            pattern.flocks[i].lock();
            int j = A.filc[i][p].num;
            tensor2 * T  = A.filc[i](j);
            pattern.flocks[i].unlock();

			if (j>i)
				break;
			else if (i>j) {
			    pattern.flocks[j].lock();
                tensor2 * Ts = A.filc[j](i);
			    pattern.flocks[j].unlock();

		        int n = pattern.blsize[i];
			    int m = pattern.blsize[j];

    			if (Ts == NULL) {
	    			Ts = new tensor2(m,n);

	    			pattern.clocks[i].lock();
					A.colc[i].AddKey(j,Ts);
					pattern.clocks[i].unlock();

					pattern.flocks[j].lock();
			    	A.filc[j].AddKey(i,Ts);
                    pattern.flocks[j].unlock();
    			}

                for (int s=0; s<n; ++s)
                    for (int t=0; t<m; ++t)
			    		(*Ts)(t,s) = (*T)(s,t);
			}
			else {
                int n = pattern.blsize[i];

                for (int s=0; s<n; ++s)
                    for (int t=0; t<s; ++t)
			    		(*T)(t,s) = (*T)(s,t);
			}
        }
    }
}


//esto debe ser un "inline" y ser utilizado siempre que toque
//solo sirve para hacer los accesos en paralelo pero sin escribir a zonas compartidas
tensor2 * GetOrSet (SparseTensor2 & A, const SparseTensorBlockPattern & pattern, int i, int j) {
    //pattern.flocks[i].lock();
    tensor2 * ret = A.filc[i](j);
    //pattern.flocks[i].unlock();

    if (ret == NULL) {
        //crea el nuevo bloque
        ret = new tensor2(pattern.blsize[i], pattern.blsize[j]);
        ret->zeroize();

        //lo inicia
        //pattern.flocks[i].lock();
        A.filc[i].AddKey(j, ret);
        //pattern.flocks[i].unlock();

        //pattern.clocks[j].lock();
        A.colc[j].AddKey(i, ret);
        //pattern.clocks[j].unlock();
    }

    return ret;
}

void SparseTensorRead(SparseTensor2 & A, ifstream & ofile, SparseTensorBlockPattern & pattern) {
    for (int i=0; i<pattern.nblocks; ++i) {
        int nk;
        ofile.read  ((char*) &nk, sizeof(int));

        for ( int p=0; p<nk; ++p) {
            //guarda el numero de bloque
            int j;
            ofile.read  ((char*) &j, sizeof(int));

            int n = pattern.blsize[i];
            int m = pattern.blsize[j];

            tensor2 * At = new tensor2(n,m);

            //lee los datos del tensor
            for (int s=0; s<n; ++s)
                for (int t=0; t<m; ++t)
                    ofile.read((char*) &((*At)(s,t)), sizeof(double));

            A.filc[i].AddKey(j, At);
            A.colc[j].AddKey(i, At);
        }
    }
}

void SparseTensorWrite(ofstream & ofile, SparseTensor2 & A, SparseTensorBlockPattern & pattern) {
    for (int i=0; i<pattern.nblocks; ++i) {
        int nk = A.filc[i].nkeys;
        ofile.write ((char*)&nk, sizeof(int));

        for ( int p=0; p<nk; ++p) {
            //guarda el numero de bloque
            int        j = A.filc[i][p].num;
            tensor2 * At = A.filc[i][p].key;

            ofile.write ((char*)&j, sizeof(int));

            int n = pattern.blsize[i];
            int m = pattern.blsize[j];

            //guarda ls datos del tensor
            for (int s=0; s<n; ++s)
                for (int t=0; t<m; ++t)
                    ofile.write((char*) &((*At)(s,t)), sizeof(double));
        }
    }
}
