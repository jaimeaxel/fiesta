
#include "newsparse.hpp"
#include <iostream>
using namespace std;


__global__ void AddSparseGPU (double * v, const double * w, uint32_t N) {

    uint64_t n = blockIdx.x*blockDim.x + threadIdx.x;

    if (n>=N) return;

    v[n] += w[n];
}


const SparseGPU & SparseGPU::operator+=(const SparseGPU & rhs) {

    uint32_t nb = (size2+1023)/1024;

    // copy raw data to all GPUs
    for (int n=0; n<nGpus; ++n) {
        cudaSetDevice(n);

        // make sure rhs is done doing whatever it is doing before adding everything up
        cudaEvent_t event;
        cudaEventCreate(&event);
        cudaEventRecord     (event, *rhs.stream[n]); // add an event to the queue of rhs
        cudaStreamWaitEvent (*stream[n],event,0);    // wait for the event to complete
        cudaEventDestroy(event);

        AddSparseGPU <<<nb,1024,0,*stream[n]>>> (v[n], rhs.v[n], size2);
    }

    return *this;
}



void Sparse::Reduce  (const SparseGPU & rhs) {

  // copy raw data to all GPUs
    for (int n=0; n<rhs.nGpus; ++n) {
        cudaSetDevice(n);
        //copy to host
        cudaMemcpy ( rhs.h, rhs.v[n], P->ntotal2*sizeof(double), cudaMemcpyDeviceToHost);
        // accumulate the results
        for (int i=0; i<P->ntotal2; ++i) values[i] += rhs.h[i];
    }

}

// this one is blocking, but it's not used
void SparseGPU::Add  (const Sparse & rhs) {

    int n;
    cudaGetDevice(&n);

    // find number of available GPUs
    cudaMemcpy ( h, v[n], rhs.P->ntotal2*sizeof(double), cudaMemcpyDeviceToHost);

    // add the matrix on host
    for (int i=0; i<rhs.P->ntotal2; ++i) h[i] += rhs.values[i];

    // find number of available GPUs
    cudaMemcpy ( v[n], h, rhs.P->ntotal2*sizeof(double), cudaMemcpyHostToDevice);
}

void SparseGPU::Copy (const Sparse & rhs) {

    //copy the data to pinned memory
    for (int i=0; i<size2; ++i) h[i] = rhs.values[i];

  // copy raw data to all GPUs
    for (int n=0; n<nGpus; ++n) {
        cudaSetDevice(n);
        cudaMemcpyAsync (v[n], h, size2*sizeof(double), cudaMemcpyHostToDevice, *stream[n]);
    }
}

void SparseGPU::Zeroize () {

    //free all pointers
    for (int n=0; n<nGpus; ++n) {
        cudaSetDevice   (n);
        cudaMemsetAsync (v[n], 0, size2*sizeof(double), *stream[n]);
    }
}




void SparseGPU::Set  (const sparsetensorpattern & STP) {

    len = STP.natoms;
    cudaGetDeviceCount(&nGpus);
    size2 = STP.ntotal2;

    cudaMallocHost ( (void**)(&h), size2*sizeof(double));


  // copy raw data to nGpus GPUs
    for (int n=0; n<nGpus; ++n) {
        cudaSetDevice(n);

        stream[n] = new cudaStream_t;
        cudaStreamCreate(stream[n]);

        cudaMalloc ( (void**)(&(v[n])), size2*sizeof(double));
    }

  // get the offsets
    unsigned int * pointer = new unsigned int[len*len];

    for (int i=0; i<len; ++i)
        for (int j=0; j<len; ++j)
            pointer[i*len+j] = STP.a2pos(i,j);

  // copy pointers to all GPUs
    for (int n=0; n<nGpus; ++n) {
        cudaSetDevice(n);
        cudaMalloc ( (void**)(&(p[n])),         len*len*sizeof(unsigned int));
        cudaMemcpy (            p[n], pointer,  len*len*sizeof(unsigned int), cudaMemcpyHostToDevice);
    }

  // free the memory
    delete[] pointer;
}

void SparseGPU::Clear () {

    if (h!=NULL) cudaFreeHost(h);
    h = NULL;

  // free all pointers
    for (int n=0; n<nGpus; ++n) {
        cudaSetDevice(n);

        cudaStreamSynchronize (*stream[n]);
        cudaStreamDestroy     (*stream[n]);
        delete stream[n];

        if (v[n]!=NULL) cudaFree(v[n]);
        if (p[n]!=NULL) cudaFree(p[n]);

        v[n] = NULL;
        p[n] = NULL;
    }

    nGpus = 0;
}
