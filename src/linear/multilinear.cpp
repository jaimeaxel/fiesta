#include <iostream>
#include <cmath>
#include "mtrand.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <inttypes.h>

using namespace std;



const double PI = 3.141592653589793238;
// 1 AU in Bohr
const double AU = 1.889725989;



struct point  {
    double x __attribute__ ((aligned (16)));
    double y;
    double z;

    // for set
    bool operator<(const point & rhs) const {
        if (x!=rhs.x) return (x<rhs.x);
        if (y!=rhs.y) return (y<rhs.y);
        if (z!=rhs.z) return (z<rhs.z);
        return false; // exact same point
    }
};

struct center {
    double x,y,z;
    double w;
    double r2;
    short int i,j;
    int p;
};



struct symtensor2;

struct tensor2 {
    int n; //primer indice del tensor
	int m; //segundo indice del tensor

	double ** c;
	double * c2;

	tensor2() {n=0; m=0; c=NULL; c2=NULL;}
	tensor2(int p) {n=0; m=0; setsize(p);}
	tensor2(int p, int q) {n=0; m=0; setsize(p,q);}

	void setsize(int p);
	void setsize(int p, int q);
	void zeroize();
	void clear();

	inline double & operator()(int i, int j) {
		return c[i][j];
	}

	inline double operator()(int i, int j) const {
		return c[i][j];
	}

    tensor2 & operator+=(const tensor2 & s) {
        for (int i=0;i<n*m;++i)
            c2[i] += s.c2[i];
        return *this;
    }

    tensor2 & operator-=(const tensor2 & s) {
        for (int i=0;i<n*m;++i)
            c2[i] -= s.c2[i];
        return *this;
    }

    tensor2 & operator*=(double s) {
        for (int i=0;i<n*m;++i)
            c2[i] *= s;
        return *this;
    }

    const tensor2 operator*(const tensor2 & rhs) const;
	tensor2 & operator=(const tensor2 & st);
	tensor2 & operator=(const symtensor2 & st);
};

//tensor triangular de rango 2; cuidado: los indices van de 0 a n no inclusive
struct symtensor2 {
	int n;
	int n2;
	double **c;
	double *c2;

	symtensor2() {n=0; c=NULL; c2=NULL;}
	symtensor2(int m) {n=0; setsize(m);}

	void setsize(int m);
	void zeroize();
	void clear();

	inline double & operator()(int i, int j) {
		if(i>=j) return c[i][j];
		else return c[j][i];
	}

	inline double operator()(int i, int j) const {
		if(i>=j) return c[i][j];
		else return c[j][i];
	}

    symtensor2 & operator= (double rhs);
    symtensor2 & operator+=(double rhs);
    symtensor2 & operator*=(double rhs);
    symtensor2 & operator+=(const symtensor2 & st);
    symtensor2 & operator-=(const symtensor2 & st);

    symtensor2 & operator= (const tensor2 & st);
    symtensor2 & operator= (const symtensor2 & st);

    const symtensor2 operator*(const symtensor2 & rhs) const;
};


void tensor2::setsize(int p) {
    if (n!=0 || m!=0) {
        delete[] c2;
	    delete[] c;
    }

	n = p;
	m = p;

	c  = new double*[n];
    c2 = new double[n*n];

	//asigna los punteros
    //#pragma omp parallel for
	for (int i=0;i<n;++i)
		c[i] = c2 + i*n;
}

void tensor2::zeroize() {
    //#pragma omp parallel for
	for (int i=0; i<n*m; ++i)
		c2[i] = 0.;
}

void symtensor2::setsize(int m) {
    if (n!=0) {
        delete[] c2;
        delete[] c;
    }

	n = m;
	n2 = (n*n+n)/2;

	c2 = new double[n2];
	c  = new double*[n];

	//asigna punteros
	int p2=0;

	for (int i=0; i<n; ++i) {
		c[i] = c2 + p2;
		p2 += i+1;
	}
}

void tensor2::clear() {
	delete[] c2;
	delete[] c;
	c2=NULL;
	c =NULL;
	n=0;
	m=0;
}

void symtensor2::clear() {
	delete[] c;
	delete[] c2;
	c2=NULL;
	c=NULL;
	n=0;
}


tensor2 & tensor2::operator=(const symtensor2 & st) {
    for (int i=0; i<n; ++i)
        for (int j=0; j<n; ++j)
            c[i][j] = st(i,j);

    return *this;
}

tensor2 & tensor2::operator=(const tensor2 & st) {
    if (this == &st) return *this;

    setsize(st.n, st.m);

    //copia los valores
    for (int i=0;i<n*m;++i)
        c2[i] = st.c2[i];

    return *this;
}

void tensor2::setsize(int p, int q) {
    if (n==p && m==q) return;

    if (n!=0 || m!=0) {
        delete[] c2;
	    delete[] c;
    }

	n = p;
	m = q;

	c  = new double*[n];
    c2 = new double[n*m];

	//asigna los punteros
    //#pragma omp parallel for
	for (int i=0;i<n;++i)
		c[i] = c2 + i*m;
}

std::ostream & operator<<(std::ostream & os, const tensor2 & T) {
    for (int i=0; i<T.n; ++i) {
        for (int j=0; j<T.m; ++j) {
            os << T(i,j) << " ";
        }
        os << std::endl << std::endl;
    }
    return os;
}



struct tensor3 {
    int n; //primer indice del tensor
	int m; //segundo indice del tensor
	int l; // 3er

	double *** c;
	double ** c2;
	double * c3;

	tensor3(int p, int q, int r) {

        n = p;
        m = q;
        l = r;

        c  = new double**[n];
        c2 = new double*[n*m];
        c3 = new double[n*m*l];

        for (int i=0;i<n;++i)
            c[i] = c2 + i*m;

        for (int ij=0;ij<n*m;++ij)
            c2[ij] = c3 + ij*l;
	}
	~tensor3() {
	    delete[] c;
	    delete[] c2;
	    delete[] c3;
	}

	void zeroize() {
	    for (int ijk=0; ijk<n*m*l; ++ijk) c3[ijk]  = 0;
    }


	inline double & operator()(int i, int j, int k) {
		return c[i][j][k];
	}

	inline double operator()(int i, int j, int k) const {
		return c[i][j][k];
	}

	inline double & operator()(int k, int ij) {
		return c[k][0][ij];
	}

	inline double operator()(int k, int ij) const {
		return c[k][0][ij];
	}

	const tensor2 operator()(int k) const {
	    tensor2 T;
	    T.n = m;
	    T.m = l;
	    T.c = c[k];
	    T.c2 = c[k][0];
	    return T;
	}

};


class InterpolatedFunction {
    public:
        double ** gamma_table;
        double *  gamma_table_vals;
    public:
        void InitIncompleteGammaTable();
        double calcF2(double x) const;
};


const int NEXP = 4;
const int NVIG = 1024;
const int NGD = 1 + NEXP + 1;
const double vg_min  = 0;
const double vg_max  = 32;
const double vg_step  = (vg_max-vg_min)/double(NVIG);
const double ivg_step = double(NVIG)/(vg_max-vg_min);


InterpolatedFunction    IncompleteGamma;


inline void LenzUpdate(double & A, double & Ap, const double a, const double b) {
    double tmp = A;
    A = b*A + a*Ap;
    Ap = tmp;
}

//computational chemists' "lower gamma function"
//Table[Integrate[t^(2n) exp[-x t^2],{x,0,1}],{n,0,...}]
inline void calcF(double * F, int n, double x) {
    double expx = exp(-x);

    //calcula gamma(n + 1/2, x) / x^(n+1/2) y el resto por recurrencia descendente
    if (x < double(n) + 1.5) {
        double ac  = 1./(double(n)+0.5);
        double sum = ac;

        //loop until machine precission convergence
        for (int i=1; (sum+ac)!=sum; ++i) {
            ac *= (x)/(double(n+i)+0.5);
            sum +=ac;
        }

        F[n] = 0.5 * sum;

        //downward recursion
        for (int i=n-1; i>=0; --i)
            F[i] = (2*x*F[i+1] + 1.)/double(2*i+1);

        for (int i=n; i>=0; --i)
            F[i] *= expx;
    }

    //calcula F0[x] a partir de la fraccion continua y el resto por recurrencia ascendente
    else {
        double Ap = 1;
        double A  = 0; //b0=0

        double Bp = 0;
        double B  = 1;

        LenzUpdate(A,Ap,1.,x);
        LenzUpdate(B,Bp,1.,x);

        //OPT: optimal loop unrolling
        //OPT: function inlining
        for (int i=0; i<10; ++i) {
            LenzUpdate(A,Ap,double(i)+0.5,1.);
            LenzUpdate(B,Bp,double(i)+0.5,1.);

            LenzUpdate(A,Ap,double(i+1),x);
            LenzUpdate(B,Bp,double(i+1),x);
        }

        F[0] = sqrt(PI/(4*x)) -0.5 * expx * (A/B);

        //upward recursion
        for (int i=0; i<n; ++i)
            F[i+1] = (double(2*i+1)*F[i] - expx) / (2*x);
    }
}


//nueva funcion gamma
double InterpolatedFunction::calcF2(double x) const{
    //busca el valor mas cercano de F
    //cout << vg_max << " " << vg_step << endl;
    if (x>vg_max-vg_step) {
        return 0.5*sqrt(PI/x);
    }
    else {
        double p = ivg_step*(x-vg_min);
        int pos = int(p+0.5);
        double x0 = vg_min + vg_step*double(pos);
        double Ax = x0-x;

        double Axn[NEXP];
        Axn[0] = 1;
        for (int i=1; i<NEXP; ++i) {
            Axn[i] = (Ax * Axn[i-1])/double(i);
        }

        {
            double sum = 0;
            for (int j=0; j<NEXP; ++j)
                sum += gamma_table[pos][j+1] * Axn[j];
            return sum;
        }

    }
}


void InterpolatedFunction::InitIncompleteGammaTable() {
    //init arrays and array positions
    gamma_table      = new double*[NVIG];
    gamma_table_vals = new double[NGD*NVIG];
    for (int i=0; i<NVIG; ++i) gamma_table[i] = gamma_table_vals + i*NGD;

    double Av = (vg_max-vg_min) / double (NVIG);
    for (int i=0; i<NVIG; ++i) {
        double v = vg_min + i*Av;

        gamma_table[i][0] = exp(-v);
        calcF(gamma_table[i]+1, 1+NEXP, v);
    }
}



const double MINS = 1.e-30;

//QL algorithm with implicit shifts, adapted from 'numerical recipes'
void tqli(double *d, double *e, int n, tensor2 & P) {
    int m,iter,i,k;
    double s,r,p,g,f,dd,c,b;

    for (int l=0; l<=n; l++) {
        iter=0;
        do {
            for (m=l;m<=n-1;m++) { //Look for a single small subdiagonal element to split the matrix.
                dd=fabs(d[m])+fabs(d[m+1]);
                if ((double)(fabs(e[m])+dd) == dd) break;
            }
            if (m != l) {
                if (iter++ == 30) {
                    cout << "Too many iterations in tqli" << endl;
                }
                g=(d[l+1]-d[l])/(2.0*e[l]); //Form shift.
                r=sqrt(g*g+1.);

                if (g>=0)
                    g=d[m]-d[l]+e[l]/(g+fabs(r)); //This is dm - ks.
                else
                    g=d[m]-d[l]+e[l]/(g-fabs(r)); //This is dm - ks.

                s=c=1.0;
                p=0.0;
                //11.4 Hermitian Matrices 481
                for (i=m-1;i>=l;i--) { //A plane rotation as in the original QL, followed by Givens rotations to restore tridiagonal form.
                    f=s*e[i];
                    b=c*e[i];
                    r=sqrt(f*f+g*g);
                    e[i+1] = r;
                    if (r == 0.0) { //Recover from underflow.
                        d[i+1] -= p;
                        e[m]=0.0;
                        break;
                    }
                    s=f/r;
                    c=g/r;
                    g=d[i+1]-p;
                    r=(d[i]-g)*s+2.0*c*b;
                    d[i+1]=g+(p=s*r);
                    g=c*r-b;
                    // Next loop can be omitted if eigenvectors not wanted
                    for (k=0;k<=n;k++) { //Form eigenvectors.
                        f = P(k,i+1);
                        P(k,i+1) = s*P(k,i) + c*f;
                        P(k,i)   = c*P(k,i) - s*f;
                    }
                }
                if (r == 0.0 && i >= l) continue;
                d[l] -= p;
                e[l]=g;
                e[m]=0.0;
            }
        } while (m != l);
    }
}


//hay que lograr hacer desplazamientos implicitos
void Diagonalize(symtensor2 & A, int n, tensor2 & P, double * u, int mode) {
    //variables de almacenamiento intermedio
    //u = old double[n+1];         //se usa para calculos intermedios y para guardar los autovalores
    double * p = new double[n];  //tambien se usa para calculos intermedios: primero para guardar el vector de transformacion y luego para coeficientes de rotacion
    double * Di = new double[n]; //guarda el elemento diagonal
    double * S1 = new double[n];   //guarda el elemento superdiagonal

    //tensor2 P; //tensor de transformacion ortogonal (autovectores de A)
    P.setsize(n);
    P.zeroize();
    for (int i=0; i<n;++i)
        P(i,i) = 1.;

    //convierte la matriz en tridiagonal
    for (int i=0; i<n-2; ++i) {
         //crea el vector u [i+1 ... n]
         double sum;

         //chungo ??
         //#pragma omp parallel for
         for (int j=i+2; j<n; ++j)
             u[j] = A(i,j);


         sum = 0;
         //NO LO COGE ICL 10.1.020
         //#pragma omp parallel for reduction(+:sum)
         for (int j=i+2; j<n; ++j)
             sum += u[j]*u[j];

         //si ya es (tri)diagonal no hace nada
         if (sum < MINS) {
             Di[i] = A(i,i);
             S1[i] = A(i+1,i);
             continue;
         }

         u[i+1] = A(i+1,i);
         sum += u[i+1]*u[i+1];

         //intenta minimizar el error y evitar überflows
         double H;
         if (u[i+1]>0) {
             H = sum + sqrt(sum)*u[i+1];
             u[i+1] += sqrt(sum);
         }
         else {
             H = sum - sqrt(sum)*u[i+1];
             u[i+1] -= sqrt(sum);
         }

         //calcula p = (A*u)/H  y  K = (u+ * p)/(2 H)
         double K = 0;

         //NO LO COGE ICL 10.1.020
         //#pragma omp parallel for schedule(dynamic) //reduction(+:K)
         for (int j=i+1; j<n; ++j) {
             double sum = 0;
             for (int k=i+1; k<n; ++k)
                 sum += A(j,k)*u[k];
             p[j] = sum/H;

             //#pragma omp atomic
             K += p[j]*u[j];
         }
         K /= 2*H;


         //calcula p' = p - K*u
         //#pragma omp parallel for
         for (int j=i+1; j<n; ++j)
             p[j] -= K*u[j];

         // calcula la nueva matriz A
         //#pragma omp parallel for schedule(dynamic)
         for (int j=i+1; j<n; ++j)
             for (int k=i+1; k<=j; ++k)
                 A(j,k) -= p[j]*u[k] + p[k]*u[j];

         //nuevos valores diagonal y subdiagonal
         Di[i] = A(i,i);
         S1[i] = A(i,i+1) - u[i+1];

         //construye la matriz ortogonal
         //#pragma omp parallel for schedule(dynamic)
         for (int j=0; j<n; ++j) {
             double sum =0.;
             for (int k=i+1; k<n; k++)
                 sum += P(j,k) * u[k];
             p[j] = sum/H;
         }

         //#pragma omp parallel for schedule(dynamic)
         for (int j=0; j<n; ++j)
             for (int k=i+1; k<n; k++)
                 P(j,k) -= p[j]*u[k];
     }

    if (n>1) {
        Di[n-2] = A(n-2,n-2);
        S1[n-2] = A(n-2,n-1);
    }
    Di[n-1] = A(n-1,n-1);

    //A.clear();

    int minc=0;
    int maxc=n;

    //termina de diagonalizar la matriz
    tqli (Di, S1, n-1, P);


    delete[] p;
    delete[] S1;


    //finalmente copia los autovalores y autovectores ordenando segun el
    //criterio 'mode' y reordena el resto
    for (int i=0; i<n-1; ++i) {
        double dmax = Di[i];
        int imax = i;

        //encuentra el mayor (o menor) valor en la sublista
        if (mode == 0) {
            for (int j=i+1; j<n; ++j)
                if (Di[j] > dmax) {
                    dmax = Di[j];
                    imax = j;
                }
        } else if (mode == 1) {
            for (int j=i+1; j<n; ++j)
                if (Di[j] < dmax) {
                    dmax = Di[j];
                    imax = j;
                }
        } else if (mode == 2) {
            for (int j=i+1; j<n; ++j)
                if (abs(Di[j]) > abs(dmax)) {
                    dmax = Di[j];
                    imax = j;
                }
        } else {
            for (int j=i+1; j<n; ++j)
                if (abs(Di[j]) < abs(dmax)) {
                    dmax = Di[j];
                    imax = j;
                }
        }

        //permuta los valores y los vectores
        Di[imax] = Di[i];
        Di[i] = dmax;

        for (int j=0; j<n; ++j) {
            dmax = P(j,i);
            P(j,i) = P(j,imax);
            P(j,imax) = dmax;
        }
    }

    //#pragma omp parallel for
    for (int i=0; i<n; ++i)
        u[i] = Di[i];

    delete[] Di;
}


// LAPACK FORTRAN ROUTINES
extern "C" {
    void dgesv_(int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);

    void dsyev_(const char *JOBZ, const char *UPLO, const int *N, double *A, const int *LDA, double *W, double *WORK, const int *LWORK, int *INFO);

    void dsygv_(const int * itype, const char * jobz, const char * uplo, const int * n, double * a, const int * lda, double * b, const int * ldb, double * w, double * work, const int * lwork, int * info);

    void dgesvd_ ( char* jobu, char* jobvt, int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* info );

    void dgesdd_ ( char* jobz,              int* m, int* n, double* a, int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt, double* work, int* lwork, int* info );

    void dsyevx_ ( char* jobz, char* range, char* uplo, int* n, double* a, int* lda, double* vl, double* vu, int* il, int* iu, double* abstol, int* m,
                          double* w, double* z, int* ldz, double* work, int* lwork, int* iwork, int* ifail, int* info );
    // LU decomoposition of a general matrix
    void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);
    // generate inverse of a matrix given its LU decomposition
    void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);
}

void GEV (tensor2 & A, tensor2 & B, double * w, bool print=false) {

    A*=-1;

    int info, lwork;
    double wkopt;
    double * work;

    char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
    char UL   = 'U'; // matrix storage pàtern

    //int itype = 1; // A v = l B v
    int n = B.n;
    int itype = 1;

    lwork = -1;
    dsygv_ (&itype, &JOBZ, &UL, &n, A.c2, &n, B.c2, &n, w, &wkopt, &lwork, &info );
    lwork = (int)wkopt;
    work = new double[lwork];

    dsygv_ (&itype, &JOBZ, &UL, &n, A.c2, &n, B.c2, &n, w,   work, &lwork, &info );

    for (int i=0; i<n; ++i) w[i]*=-1;

    if (print) {
        for (int i=0; i<n; ++i) cout << w[i] << " "; cout << endl << endl;
    }

    delete[] work;

    if (info>0) {
        cout << "Error! " << info << endl;
        throw info; //meaningless error code
    }
}

void EV (tensor2 & S, double * w,bool print=false) {

    S*=-1; // sort them from larger to smaller

    char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
    char UL   = 'U'; // matrix storage pàtern
    int INFO;        // chack status
    int SIZE = S.n;
    int LWORK = SIZE*SIZE;  // size of working space
    double * WORK = new double[LWORK];   // nothing

    dsyev_ (&JOBZ, &UL, &SIZE, S.c2, &SIZE, w, WORK, &LWORK, &INFO);
    for (int i=0; i<SIZE; ++i) w[i] = -w[i];

    if (print) {
        for (int i=0; i<SIZE; ++i)
            cout << w[i] << " ";
        cout << endl << endl;
    }
    //if (info>0) throw info; //meaningless error code

    delete[] WORK;
}

int EVX (tensor2 & S, double * w, tensor2 & V, double thresh=1.e-14, bool print=false) {

    int N = S.n;
    int K = V.m;

    if ((S.m!=N)||(V.n!=N)) {
        cout << "invalid array dimensions in EVX" << endl;
        return -1;
    }

    char JOBZ  = 'V' ; //'V'; // compute eigenvalues and eigenvectors
    char RANGE = 'I'; // solve for the i-th eigenvalues
    char UL    = 'U'; // matrix storage pàtern

    double dummy;

    int IL=1;
    int IU=K;

    double * work;
    double wkopt;
    int lwork = -1;
    int iwork[5*N];
    int ifail[N];
    int info;        // chack status


    // return number of evecs
    int M;


    S*=-1; // sort them from larger to smaller

    dsyevx_(&JOBZ,&RANGE,&UL, &N, S.c2, &N, &dummy,&dummy,
            &IL,&IU, &thresh, &M,  w, V.c2, &N,
            &wkopt, &lwork, iwork, ifail, &info);

    lwork = (int)wkopt;
    work = new double[lwork];

    //cout << "lwork = " << lwork << endl;
    //cout << "info  = " << info << endl;

    dsyevx_(&JOBZ,&RANGE,&UL, &N, S.c2, &N, &dummy,&dummy,
            &IL,&IU, &thresh, &M,  w, V.c2, &N,
            work, &lwork, iwork, ifail, &info);


    for (int i=0; i<M; ++i) w[i] = -w[i];
    S*=-1;

    if (print) {
        for (int i=0; i<M; ++i)
            cout << w[i] << " ";
        cout << endl << endl;
    }

    delete[] work;

    if (info>0) {
        // ifail[] contains the indices of the eigenvecs that did not converge
        cout << "Error in EVX! " << info << endl;
        throw info; //meaningless error code
    }

    return M;
}


// SVD, the stupid way
int SVD (const tensor2 & T, tensor2 & U, tensor2 & V, double * ss) {

    // T(N,M);
    // U(N)
    // V(N);
    // ss[max(N,M)];

    const int N = T.n;
    const int M = T.m;
    const int R = min(N,M);

    if (U.n!=N || U.m!=N) return -1; // error
    if (V.n!=M || V.m!=M) return -1; // error

    // assume M>N

    #pragma omp parallel for schedule(dynamic)
    for (int k=0; k<N; ++k) {
        for (int l=0; l<=k; ++l) {
            double ss  = 0;

            for (int i=0; i<M; ++i)
                ss += T(k,i)*T(l,i);

            U(k,l) = U(l,k) = -ss;
        }
    }

    EV(U,ss);

    cout << "SVD^2  = ";  for (int i=0; i<R; ++i) cout << -ss[i] << " "; cout << endl << endl;
    cout << "SVD    = ";  for (int i=0; i<R; ++i) cout << sqrt(-ss[i]) << " "; cout << endl << endl;

    V.zeroize();

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<M; ++i)
        for (int k=0; k<R; ++k) {
            double ss = 0;
            for (int l=0; l<N; ++l) {
                ss+=U(k,l)*T(l,i);
            }
            V(k,i) = ss;
        }

    for (int k=0; k<R; ++k) {
        double vv=0;
        for (int i=0; i<M; ++i)
            vv+=V(k,i)*V(k,i);

        if (vv<=0) return k;

        vv=sqrt(vv);
        ss[k]=vv; // store the norm
        vv=1./vv;
        for (int i=0; i<M; ++i)
            V(k,i)*=vv;
    }

    cout << "SVD    = ";  for (int i=0; i<R; ++i) cout << ss[i] << " "; cout << endl << endl;

    return R;
}

// smarter? SVD
int SVD2 (const tensor2 & T, tensor2 & V, tensor2 & U, double * ss) {

    // T(N,M);
    // U(N)
    // V(N);
    // ss[max(N,M)];

    const int N = T.n;
    const int M = T.m;
    const int R = min(N,M);

    //if (U.n!=N || U.m!=N) return -1; // error
    //if (V.n!=M || V.m!=M) return -1; // error

    tensor2 A(N,M);
    A = T;

    //tensor2 U(NV,NV), V(NO,NO);

    char all[] = "A";
    int lwork = -1;
    double wokpt;
    double * work;
    int info;

    int NV = M;
    int NO = N;

    dgesvd_( all, all, &NV, &NO, A.c2, &NV, ss, U.c2, &NV, V.c2, &NO,  &wokpt, &lwork, &info);

    if (info!=0) cout << "SVD1 info = " << info << endl;

    lwork = int(wokpt);
    work = new double[max(1,lwork)];

    dgesvd_( all, all, &NV, &NO, A.c2, &NV, ss, U.c2, &NV, V.c2, &NO,  work, &lwork, &info);

    if (info!=0) cout << "SVD2 info = " << info << endl;

    delete[] work;

    cout << "SVD    = ";  for (int i=0; i<R; ++i) cout << ss[i] << " "; cout << endl << endl;

    return R;
}

// Moore-Penrose pseudoinverse
int MPPI2 (const tensor2 & T, tensor2 & P) {

    const int N = T.n;
    const int M = T.m;
          int R = min(N,M);

    if (P.n!=N || P.m!=M) return -1; // error


    tensor2 U(N), V(M);
    double s[R];

    R = SVD2(T,U,V,s);

    P.zeroize();

    for (int k=0; k<R; ++k) {
        double is=1./s[k];

        for (int i=0; i<N; ++i)
            for (int j=0; j<M; ++j)
                P(i,j) += is * U(i,k) * V(k,j);

        // U(k,i) * V(k,j);
        // U(i,k) * V(k,j);

    }
    return R;
}

// cholesky dec. to weighted eigenvectors
int CD2EV (const tensor2 & T, tensor2 & P) {

    const int N = T.n;    // this index is for the CD vectors
    const int M = T.m;    // vector elements
          int R = min(N,M);

    if (P.n!=N || P.m!=M) return -1; // error

    tensor2 U(N), V(M);
    double s[R];

    R = SVD2(T,U,V,s);

    P.zeroize();

    for (int k=0; k<N; ++k) {
        for (int j=0; j<M; ++j) P(k,j) = s[k] * V(k,j);
    }

    return R;
}

// Moore-Penrose pseudoinverse
int MPPI (const tensor2 & T, tensor2 & P, double thresh=1e-8) {


    const int N = T.n;
    const int M = T.m;
          int R = min(N,M);

    if (P.n!=N || P.m!=M) return -1; // error


    tensor2 U(N), V(M);
    double s[R];

    R = SVD2(T,U,V,s);

    P.zeroize();

    for (int k=0; k<R; ++k) {
        // must skip the really small ones
        if (s[k]<thresh*s[0]) return k;

        double is=1./s[k];

        for (int i=0; i<N; ++i)
            for (int j=0; j<M; ++j)
                P(i,j) += is * U(k,i) * V(k,j);
    }
    return R;

}

void INV (tensor2 & SS) {

    int R=SS.n;

    int *IPIV = new int[R+1];
    int LWORK = R*R;
    double *WORK = new double[LWORK];
    int INFO;

    dgetrf_(&R,&R,SS.c2,&R,IPIV,&INFO);
    cout << "dgetrf ready" << endl;
    dgetri_(&R,SS.c2,&R,IPIV,WORK,&LWORK,&INFO);
    cout << "dgetri ready" << endl;

    delete IPIV;
    delete WORK;
}



// sorting in reasonable time

void FlashSortC (center * v, int N) {

    //for short lists default to selection sort
    if (N<32) {
        for (int i=0; i<N; ++i)  {
            double best = v[i].w;
            int   k   = i;

            for (int j=i+1; j<N; ++j)
                if (v[j].w>best) {best = v[j].w; k = j;}

            swap(v[i], v[k]);
        }
        return;
    }

    double k1 = v[0].w;
    double k2 = v[N/2].w;
    double k3 = v[N-1].w;

    double kpiv;

    if      (k1<=k2 && k2<=k3) kpiv = k2;
    else if (k3<=k2 && k2<=k1) kpiv = k2;
    else if (k1<=k3 && k3<=k2) kpiv = k3;
    else if (k2<=k3 && k3<=k1) kpiv = k3;
    else                       kpiv = k1;


    //count number of elements lower than the pivot
    int nl=0;
    int ng=0;
    for (int i=0; i<N; ++i) {
        if      (v[i].w > kpiv) ++nl;
        else if (v[i].w < kpiv) ++ng;
    }

    int i,j,k;


    i=0;
    j=nl;

    while (1) {
        //find the first useful place in the first half of the list
        while (i<nl && v[i].w>kpiv) ++i;
        if (i>=nl) break;
        while (j<N  && v[j].w<=kpiv) ++j;
        if (j>=N) break;

        swap(v[i]    , v[j]);
    }

    j = nl;
    k = N-ng;

    while (1) {
        //find the first useful place in the first half of the list
        while (j<N-ng && v[j].w==kpiv) ++j;
        if (j>=N-ng) break;
        while (k<N  && v[k].w<kpiv) ++k;
        if (k>=N) break;

        swap(v[j]    , v[k]);
    }


    //FlashSort both sublists (skipping the pivot)
    FlashSortC (v,          nl);

    FlashSortC (v + (N-ng), ng);
}

template <class T, class K> void FlashSort(T * keys, K * elements, int N) {

    //for short lists default to selection sort
    if (N<32) {
        for (int i=0; i<N; ++i)  {
            T best = keys[i];
            int   k   = i;

            for (int j=i+1; j<N; ++j)
                if (keys[j]<best) {best = keys[j]; k = j;}

            swap(keys[i]    , keys[k]);
            swap(elements[i], elements[k]);
        }
        return;
    }

    T & k1 = keys[0];
    T & k2 = keys[N/2];
    T & k3 = keys[N-1];
    T kpiv;

    if      (k1<=k2 && k2<=k3) kpiv = k2;
    else if (k3<=k2 && k2<=k1) kpiv = k2;
    else if (k1<=k3 && k3<=k2) kpiv = k3;
    else if (k2<=k3 && k3<=k1) kpiv = k3;
    else                       kpiv = k1;
    //else if (k2<k1 && k1<k3) kpiv = k1;
    //else if (k3<k1 && k1<k2) kpiv = k1;


    //count number of elements lower than the pivot
    int nl=0;
    int ng=0;
    for (int i=0; i<N; ++i) {
        if      (keys[i] < kpiv) ++nl;
        else if (keys[i] > kpiv) ++ng;
    }

    int i,j,k;


    i=0;
    j=nl;

    while (1) {
        //find the first useful place in the first half of the list
        while (i<nl && keys[i]<kpiv) ++i;
        if (i>=nl) break;
        while (j<N  && keys[j]>=kpiv) ++j;
        if (j>=N) break;

        swap(keys[i]    , keys[j]);
        swap(elements[i], elements[j]);
    }

    j = nl;
    k = N-ng;

    while (1) {
        //find the first useful place in the first half of the list
        while (j<N-ng && keys[j]==kpiv) ++j;
        if (j>=N-ng) break;
        while (k<N  && keys[k]>kpiv) ++k;
        if (k>=N) break;

        swap(keys[j]    , keys[k]);
        swap(elements[j], elements[k]);
    }


    //FlashSort both sublists (skipping the pivot)
    FlashSort(keys         ,  elements         ,  nl);

    FlashSort(keys + (N-ng),  elements + (N-ng),  ng);
}







const int  SIZE = 1024;




int main2() {
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    MTRand_int32 irand(init, length); // 32-bit int generator
    // this is an example of initializing by an array
    // you may use MTRand(seed) with any 32bit integer
    // as a seed for a simpler initialization
    MTRand drand; // double in [0, 1) generator, already init

    // generate the same numbers as in the original C test program


    IncompleteGamma.InitIncompleteGammaTable();


    point * points = new point[SIZE];
    for (int i=0; i<SIZE; ++i) {
        points[i].x = drand();
        points[i].y = drand();
        points[i].z = drand();
    }


    symtensor2 V;
    V.setsize(SIZE);

    double sumi = 0;
    for (int i=0; i<SIZE; ++i) {

        double sumj = 0;
        for (int j=0; j<SIZE; ++j) {
            double x = points[i].x - points[j].x;
            double y = points[i].y - points[j].y;
            double z = points[i].z - points[j].z;

            double r2 = 64*(x*x+y*y+z*z);

            double VV = IncompleteGamma.calcF2(r2);

            sumj += VV;
            V(i,j) = VV;
        }

        sumi += sumj;
    }

    cout << sumi << endl;



    tensor2 T;
    double * v = new double[SIZE];

    if (1) {

        T.setsize(SIZE);
        T = V;

        cout << sumi << endl;

        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = SIZE*SIZE;  // size of working space
        double * WORK = new double[LWORK];   // nothing

        dsyev_ (&JOBZ, &UL, &SIZE, T.c2, &SIZE, v, WORK, &LWORK, &INFO);
    }

    else {
        Diagonalize(V, SIZE, T, v, 0);
    }


    for (int i=0; i<SIZE; ++i) cout << v[i] << " ";
    cout << endl;

    double sum = 0;
    for (int i=0; i<SIZE; ++i) sum += v[i];
    cout << sum << endl;

    double sum2 = 0;
    for (int i=0; i<SIZE; ++i) sum2 += v[i]*v[i];
    cout << sum2 << endl;



    delete[] points;
    delete[] v;

    V.clear();
    T.clear();

}


int main34(int argc,char **argv) {
    /*
    if (argc !=2) {
        cout << "dgesv dim" << endl;
        return 0;
    }
    */
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    MTRand_int32 irand(init, length); // 32-bit int generator
    MTRand drand; // double in [0, 1) generator, already init


    clock_t t1 = clock();
    int dim=1024;

    vector<double> a(dim*dim);
    vector<double> b(dim);
    vector<int> ipiv(dim);
    //srand(1);              // seed the random # generator with a known value
    //double maxr=(double)RAND_MAX;

    for(int r=0; r < dim; r++) {  // set a to a random matrix, i to the identity
        for(int c=0; c < dim; c++) {
          a[r + c*dim] = drand(); //rand()/maxr;
        }
        b[r] = drand(); //rand()/maxr;
    }

    vector<double> a1(a);
    vector<double> b1(b);

    int info;
    cout << "matrices allocated and initialised " << double(clock() - t1)/CLOCKS_PER_SEC << endl;
    clock_t c2 = clock();
    int one = 1;
    dgesv_(&dim, &one, &*a.begin(), &dim, &*ipiv.begin(), &*b.begin(), &dim, &info);
    clock_t c3 = clock();
    cout << "dgesv is over for " << double(c3 - c2)/CLOCKS_PER_SEC << endl;
    cout << "info is " << info << endl;
    double eps = 0.;
    for (int i = 0; i < dim; ++i)
    {
    double sum = 0.;
    for (int j = 0; j < dim; ++j)
      sum += a1[i + j*dim]*b[j];
    eps += fabs(b1[i] - sum);
    }
    cout << "check is " << eps << endl;
    return 0;
}


double occ (double x) {
    if (x<0) return  1./(1.+exp(x));
    else     return  exp(-x)/(1.+exp(-x));
}

double FDocc(double ef, const double * en, int N, double theta) {

    double nn = 0;

    for (int i=0; i<N; ++i) nn += occ(theta*(en[N-1-i]-ef));

    return nn;
}


double FermiLevel(int n, const double * en, int N, double theta) {

    double efmin = en[0];
    double efmax = en[N-1];

    double nnmin = FDocc(efmin, en, N, theta);
    double nnmax = FDocc(efmax, en, N, theta);

    //cout.precision(16);

    while(1) {
        double efmed = 0.5*(efmin+efmax);
        double nnmed = FDocc(efmed, en, N, theta);

        //cout << efmed << " " << nnmed << endl;

        if (efmax==efmed || efmin==efmed) break; // machine precision
        if (fabs(nnmed-double(n))<1e-11) break;

        if (nnmed>double(n)) efmax = efmed;
        if (nnmed<double(n)) efmin = efmed;
    }

    return efmax;
}



// Fermi-Dirac distribution (to avoid abrupt level shifts)
// theta = 1/kT
void FDD(tensor2 & D, const tensor2 & C, int n, const double * en, int N, double theta) {

    double ef = FermiLevel(n,en,N,theta);

    cout << ef << " : ";

    D.zeroize();
    for (int o=0; o<N; ++o) {
        // compute partition function
        double w = occ(theta*(en[o]-ef));
        cout << w << " ";

        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j)
                D(i,j) += w*C(o,i)*C(o,j);
    }
    cout << endl;

}



int main_CMM() {
    const double AMS = 1./0.52917721092;
    point au[] = {{27.645, 27.554, 31.144}, {27.495, 29.559, 33.208}, {29.803, 28.000, 32.892}, {29.281, 29.869, 30.789}, {21.984, 23.877, 17.795}, {26.615, 20.414, 17.047}, {25.708, 24.593, 17.184}, {28.103, 22.904, 16.788}, {29.716, 25.476, 16.435}, {19.619, 25.541, 17.475}, {23.484, 26.248, 17.350}, {24.966, 28.724, 17.082}, {27.272, 27.026, 16.860}, {29.121, 29.425, 16.375}, {22.454, 30.607, 17.252}, {26.543, 30.993, 16.641}, {23.876, 16.164, 21.999}, {27.942, 16.894, 21.453}, {31.939, 17.453, 20.640}, {19.306, 19.469, 22.678}, {21.629, 17.755, 22.250}, {20.971, 19.760, 20.375}, {23.315, 20.087, 21.970}, {25.655, 18.524, 21.816}, {24.959, 20.363, 19.548}, {27.311, 18.607, 19.289}, {27.251, 20.866, 21.333}, {29.638, 19.220, 21.023}, {28.834, 21.082, 19.033}, {31.184, 21.735, 20.705}, {33.599, 19.889, 20.325}, {32.772, 21.886, 17.949}, {35.126, 22.348, 20.002}, {17.036, 21.184, 22.881}, {18.642, 23.388, 22.353}, {20.933, 21.878, 22.241}, {20.239, 23.694, 20.068}, {22.714, 22.059, 19.957}, {22.545, 24.154, 21.833}, {24.926, 22.481, 21.526}, {24.266, 24.420, 19.567}, {26.523, 22.786, 19.249}, {26.323, 24.939, 21.276}, {28.699, 23.363, 20.842}, {28.060, 25.108, 18.801}, {30.420, 23.458, 18.564}, {30.299, 25.580, 20.423}, {32.769, 24.126, 20.215}, {32.036, 26.055, 18.040}, {34.214, 26.537, 19.870}, {36.640, 24.835, 19.619}, {16.424, 25.082, 22.658}, {17.842, 25.402, 20.254}, {17.898, 27.472, 22.315}, {20.081, 25.927, 22.065}, {19.421, 27.804, 19.829}, {21.849, 26.003, 19.670}, {21.776, 28.307, 21.593}, {24.050, 26.551, 21.356}, {23.289, 28.500, 19.274}, {25.714, 26.817, 19.152}, {25.669, 28.968, 21.097}, {27.925, 27.314, 20.804}, {27.291, 29.277, 18.765}, {29.595, 27.581, 18.470}, {29.588, 29.675, 20.385}, {31.876, 28.110, 20.063}, {31.284, 30.014, 18.051}, {33.737, 28.382, 17.859}, {33.482, 30.512, 19.893}, {35.777, 29.036, 19.517}, {14.970, 27.421, 21.843}, {16.541, 30.017, 21.573}, {19.414, 29.950, 21.953}, {21.044, 30.162, 19.691}, {20.946, 32.348, 21.550}, {23.379, 30.574, 21.384}, {22.750, 32.503, 19.359}, {24.966, 30.898, 19.080}, {24.941, 32.980, 20.931}, {27.324, 31.335, 20.802}, {26.517, 33.217, 18.688}, {28.822, 31.611, 18.349}, {28.855, 33.581, 20.383}, {31.161, 32.046, 20.078}, {30.433, 33.977, 17.910}, {32.635, 34.401, 19.687}, {35.138, 32.821, 19.378}, {18.163, 32.481, 21.229}, {20.014, 32.692, 18.507}, {22.606, 34.602, 21.179}, {24.125, 34.907, 18.877}, {24.176, 37.063, 20.811}, {26.402, 35.300, 20.555}, {27.928, 37.695, 20.033}, {30.302, 35.980, 19.962}, {20.646, 15.672, 26.717}, {23.034, 13.981, 26.539}, {22.290, 15.852, 24.489}, {24.647, 16.471, 26.064}, {27.011, 14.753, 25.791}, {26.287, 16.634, 23.810}, {28.740, 14.950, 23.418}, {28.643, 17.060, 25.433}, {30.992, 15.417, 25.152}, {30.436, 17.281, 22.974}, {32.579, 17.825, 24.732}, {32.680, 15.572, 22.853}, {34.231, 18.067, 22.464}, {36.495, 18.474, 24.163}, {14.655, 21.383, 24.677}, {18.342, 17.345, 27.099}, {17.498, 19.227, 24.925}, {19.995, 17.580, 24.677}, {19.856, 19.654, 26.589}, {22.216, 18.015, 26.346}, {21.594, 19.952, 24.289}, {23.896, 18.309, 24.028}, {23.895, 20.477, 25.961}, {26.259, 18.777, 25.675}, {25.549, 20.668, 23.647}, {27.975, 18.983, 23.390}, {27.787, 21.264, 25.314}, {30.267, 19.453, 24.979}, {29.506, 21.444, 22.959}, {31.899, 19.644, 22.650}, {31.780, 21.888, 24.579}, {34.197, 20.211, 24.346}, {33.450, 22.155, 22.356}, {35.759, 20.539, 22.054}, {35.748, 22.666, 23.987}, {38.041, 21.011, 23.767}, {37.393, 22.996, 21.737}, {39.100, 25.322, 21.511}, {15.196, 23.214, 27.062}, {17.485, 21.354, 26.832}, {17.010, 23.279, 24.801}, {19.296, 21.579, 24.617}, {19.158, 23.752, 26.497}, {21.592, 22.017, 26.306}, {20.803, 23.997, 24.211}, {23.283, 22.307, 23.825}, {23.193, 24.422, 25.805}, {25.522, 22.807, 25.586}, {24.803, 24.708, 23.547}, {27.107, 23.034, 23.230}, {27.023, 25.182, 25.316}, {29.439, 23.522, 24.943}, {28.684, 25.487, 22.920}, {31.039, 23.824, 22.560}, {30.921, 25.930, 24.547}, {33.405, 24.238, 24.226}, {32.559, 26.162, 22.225}, {35.040, 24.609, 21.990}, {34.878, 26.751, 23.929}, {37.341, 25.138, 23.731}, {36.530, 26.984, 21.588}, {38.833, 27.487, 23.275}, {14.577, 24.888, 24.753}, {14.501, 26.937, 26.914}, {16.812, 25.336, 26.667}, {16.211, 27.186, 24.484}, {18.591, 25.663, 24.409}, {18.464, 27.761, 26.308}, {20.789, 26.058, 26.068}, {20.142, 28.097, 24.021}, {22.317, 26.342, 23.763}, {22.358, 28.485, 25.691}, {24.621, 26.893, 25.504}, {24.029, 28.585, 23.357}, {26.346, 27.125, 23.196}, {26.300, 29.134, 25.204}, {28.697, 27.546, 24.907}, {27.984, 29.389, 22.846}, {30.356, 27.810, 22.490}, {30.211, 29.980, 24.514}, {32.581, 28.281, 24.281}, {31.827, 30.177, 22.196}, {34.156, 28.546, 21.811}, {34.166, 30.711, 23.819}, {36.468, 28.966, 23.602}, {35.796, 30.932, 21.571}, {38.103, 29.302, 21.202}, {38.082, 31.425, 23.303}, {13.139, 29.450, 26.370}, {16.149, 29.276, 26.518}, {14.943, 29.798, 23.964}, {17.731, 29.588, 24.188}, {17.686, 31.708, 26.143}, {20.041, 30.109, 25.904}, {19.325, 31.993, 23.828}, {21.796, 30.429, 23.659}, {21.628, 32.434, 25.634}, {24.009, 30.757, 25.435}, {23.351, 32.745, 23.295}, {25.687, 31.053, 23.120}, {25.546, 33.194, 24.969}, {27.871, 31.552, 24.678}, {27.193, 33.461, 22.709}, {29.505, 31.870, 22.367}, {29.462, 33.885, 24.456}, {31.780, 32.215, 24.092}, {31.107, 34.145, 22.081}, {33.410, 32.676, 21.824}, {33.414, 34.753, 23.746}, {35.705, 33.080, 23.479}, {35.071, 34.848, 21.392}, {37.432, 35.637, 23.214}, {14.829, 31.722, 26.061}, {16.520, 32.117, 23.565}, {16.256, 34.282, 25.560}, {19.182, 33.951, 25.814}, {19.763, 34.911, 20.596}, {20.933, 34.375, 23.552}, {20.716, 36.447, 25.547}, {23.166, 34.785, 25.313}, {22.440, 36.603, 23.211}, {24.830, 35.069, 22.961}, {24.723, 37.182, 24.924}, {27.085, 35.603, 24.642}, {26.373, 37.407, 22.640}, {28.720, 35.767, 22.277}, {28.728, 37.939, 24.276}, {31.035, 36.248, 23.981}, {30.163, 38.179, 21.805}, {32.638, 36.468, 21.515}, {32.555, 38.726, 23.489}, {34.886, 37.034, 23.184}, {17.957, 34.682, 23.302}, {22.444, 38.828, 25.123}, {24.078, 39.155, 22.829}, {26.306, 39.552, 24.517}, {30.157, 40.478, 23.736}, {26.105, 12.825, 30.167}, {27.750, 12.928, 27.746}, {30.051, 13.365, 29.488}, {30.268, 11.527, 24.361}, {33.956, 14.181, 28.760}, {19.153, 15.212, 29.149}, {21.255, 16.036, 30.724}, {23.590, 14.316, 30.554}, {22.954, 16.232, 28.475}, {25.334, 14.634, 28.106}, {25.297, 16.703, 30.118}, {27.716, 15.008, 29.831}, {26.933, 16.975, 27.771}, {29.319, 15.254, 27.488}, {29.306, 17.372, 29.504}, {31.541, 15.794, 29.264}, {30.932, 17.608, 27.103}, {33.243, 16.130, 26.806}, {33.131, 18.106, 28.812}, {35.464, 16.601, 28.543}, {34.859, 18.440, 26.544}, {37.071, 18.894, 28.112}, {39.688, 23.355, 23.439}, {16.416, 19.263, 31.240}, {18.905, 17.556, 30.942}, {18.245, 19.492, 29.004}, {20.642, 17.757, 28.727}, {20.409, 19.916, 30.588}, {22.967, 18.446, 30.288}, {22.193, 20.133, 28.255}, {24.611, 18.586, 28.000}, {24.535, 20.699, 30.041}, {26.960, 18.939, 29.766}, {26.162, 20.904, 27.671}, {28.583, 19.258, 27.286}, {28.408, 21.359, 29.365}, {30.832, 19.764, 29.091}, {30.146, 21.556, 26.994}, {32.464, 20.013, 26.687}, {32.389, 22.169, 28.684}, {34.775, 20.632, 28.438}, {34.079, 22.460, 26.239}, {36.361, 20.862, 26.077}, {36.360, 22.910, 28.029}, {38.670, 21.327, 27.838}, {38.017, 23.167, 25.576}, {40.186, 23.754, 27.418}, {14.109, 20.811, 31.464}, {13.445, 22.906, 29.390}, {15.786, 21.221, 29.088}, {15.760, 23.329, 31.143}, {18.068, 21.618, 30.935}, {17.513, 23.476, 28.782}, {19.765, 21.804, 28.549}, {19.788, 23.925, 30.611}, {22.091, 22.230, 30.196}, {21.436, 24.164, 28.264}, {23.777, 22.544, 27.886}, {23.672, 24.705, 29.867}, {26.042, 23.051, 29.556}, {25.364, 24.968, 27.527}, {27.751, 23.227, 27.276}, {27.655, 25.376, 29.313}, {30.010, 23.785, 29.075}, {29.268, 25.633, 26.912}, {31.668, 24.032, 26.654}, {31.598, 26.137, 28.631}, {33.953, 24.440, 28.293}, {33.333, 26.452, 26.348}, {35.626, 24.842, 25.947}, {35.582, 26.874, 28.056}, {37.943, 25.282, 27.677}, {37.164, 27.186, 25.709}, {39.654, 25.613, 25.393}, {39.558, 27.667, 27.314}, {42.032, 26.010, 26.924}, {41.197, 27.914, 24.864}, {13.422, 24.952, 31.362}, {12.845, 26.646, 29.250}, {15.229, 25.297, 28.975}, {15.068, 27.275, 31.103}, {17.441, 25.632, 30.857}, {16.929, 27.523, 28.791}, {19.147, 25.885, 28.408}, {19.114, 27.948, 30.431}, {21.303, 26.324, 30.179}, {20.706, 28.210, 28.045}, {23.137, 26.523, 27.766}, {23.013, 28.746, 29.792}, {25.344, 27.040, 29.470}, {24.542, 29.012, 27.438}, {26.952, 27.285, 27.223}, {26.871, 29.386, 29.178}, {29.266, 27.894, 28.802}, {28.578, 29.636, 26.797}, {30.937, 28.007, 26.526}, {30.899, 30.090, 28.515}, {33.214, 28.528, 28.221}, {32.455, 30.402, 26.163}, {34.881, 28.774, 25.976}, {34.790, 30.864, 27.976}, {37.117, 29.288, 27.641}, {36.306, 31.077, 25.575}, {38.835, 29.566, 25.187}, {38.788, 31.538, 27.068}, {41.166, 30.029, 26.837}, {40.514, 29.928, 22.852}, {12.629, 28.951, 31.015}, {14.549, 29.035, 28.957}, {12.950, 31.496, 28.635}, {16.671, 29.589, 30.726}, {15.918, 31.440, 28.546}, {18.288, 29.844, 28.266}, {18.212, 31.907, 30.272}, {20.606, 30.339, 30.088}, {19.819, 32.193, 27.836}, {22.216, 30.586, 27.755}, {21.973, 32.682, 29.623}, {24.471, 31.048, 29.387}, {23.905, 32.834, 27.344}, {26.309, 31.377, 27.119}, {26.192, 33.291, 29.039}, {28.578, 31.826, 28.859}, {27.795, 33.731, 26.631}, {30.178, 32.079, 26.587}, {30.171, 34.223, 28.404}, {32.480, 32.511, 28.246}, {31.762, 34.436, 26.061}, {34.029, 32.782, 25.814}, {34.095, 34.899, 27.721}, {36.399, 33.201, 27.392}, {35.710, 35.280, 25.388}, {37.856, 33.406, 25.134}, {37.963, 35.756, 27.018}, {40.317, 36.056, 28.687}, {15.789, 33.497, 30.576}, {14.518, 33.956, 28.097}, {17.426, 33.756, 28.208}, {17.356, 35.941, 30.111}, {19.632, 34.315, 29.971}, {19.121, 36.121, 27.842}, {21.370, 34.493, 27.488}, {21.322, 36.702, 29.597}, {23.687, 35.039, 29.231}, {23.064, 36.899, 27.254}, {25.495, 35.251, 26.999}, {25.388, 37.459, 28.867}, {27.723, 35.781, 28.656}, {27.030, 37.723, 26.626}, {29.404, 36.060, 26.321}, {29.303, 38.272, 28.290}, {31.683, 36.613, 27.910}, {30.948, 38.449, 25.809}, {33.379, 36.842, 25.637}, {33.267, 39.059, 27.542}, {35.512, 37.273, 27.426}, {34.947, 39.198, 25.132}, {18.902, 38.373, 29.860}, {20.703, 38.551, 27.515}, {20.397, 40.641, 29.489}, {22.981, 39.041, 29.249}, {22.446, 40.948, 27.154}, {24.635, 39.331, 26.917}, {24.519, 41.527, 28.954}, {26.941, 39.986, 28.534}, {26.253, 41.842, 26.470}, {28.545, 40.164, 26.224}, {28.630, 42.193, 28.005}, {30.867, 40.584, 27.828}, {26.718, 12.902, 34.388}, {28.461, 13.117, 31.903}, {30.715, 13.525, 33.634}, {32.347, 13.885, 31.350}, {34.973, 14.259, 33.000}, {19.562, 15.690, 33.016}, {24.607, 12.262, 32.546}, {21.869, 16.215, 34.802}, {24.173, 14.520, 34.449}, {23.618, 16.394, 32.296}, {26.009, 14.740, 32.254}, {25.811, 16.908, 34.144}, {28.235, 15.224, 33.968}, {27.545, 17.101, 31.800}, {29.908, 15.472, 31.578}, {29.830, 17.636, 33.562}, {32.320, 15.978, 33.303}, {31.585, 17.902, 31.264}, {33.936, 16.230, 30.897}, {33.819, 18.452, 33.011}, {36.194, 16.875, 32.540}, {35.403, 18.677, 30.456}, {37.839, 17.115, 30.111}, {37.743, 19.246, 32.237}, {39.590, 19.509, 29.914}, {17.326, 17.399, 33.272}, {17.133, 19.398, 35.418}, {19.518, 17.812, 35.031}, {18.783, 19.646, 33.021}, {21.290, 18.090, 32.679}, {21.207, 20.076, 34.696}, {23.511, 18.587, 34.390}, {22.694, 20.477, 32.303}, {25.057, 18.839, 32.120}, {25.083, 20.980, 34.043}, {27.372, 19.288, 33.699}, {26.694, 21.156, 31.680}, {29.145, 19.519, 31.463}, {29.101, 21.676, 33.412}, {31.377, 19.973, 33.118}, {30.723, 21.910, 31.076}, {33.109, 20.229, 30.788}, {33.072, 22.341, 32.738}, {35.376, 20.788, 32.497}, {34.726, 22.665, 30.372}, {37.043, 21.040, 30.113}, {37.022, 23.109, 32.121}, {39.375, 21.491, 31.873}, {38.678, 23.410, 29.828}, {41.146, 22.035, 29.500}, {40.913, 24.010, 31.513}, {14.894, 21.015, 35.594}, {11.840, 22.231, 31.781}, {16.364, 21.267, 33.230}, {16.453, 23.408, 35.225}, {18.713, 21.825, 34.964}, {18.102, 23.718, 32.873}, {20.402, 21.910, 32.644}, {20.400, 24.058, 34.568}, {22.734, 22.555, 34.300}, {22.005, 24.379, 32.210}, {24.349, 22.839, 31.951}, {24.409, 24.896, 33.949}, {26.725, 23.228, 33.623}, {25.970, 25.081, 31.535}, {28.340, 23.496, 31.374}, {28.242, 25.702, 33.229}, {30.558, 23.960, 33.002}, {29.984, 25.847, 30.944}, {32.346, 24.250, 30.650}, {32.285, 26.314, 32.546}, {34.636, 24.768, 32.444}, {33.900, 26.647, 30.208}, {36.292, 25.031, 30.035}, {36.260, 27.155, 32.089}, {38.488, 25.484, 31.748}, {37.815, 27.391, 29.664}, {40.249, 25.847, 29.397}, {40.137, 27.913, 31.282}, {42.622, 26.266, 31.109}, {41.728, 28.125, 29.038}, {14.044, 22.969, 33.477}, {13.233, 27.070, 33.297}, {15.642, 25.299, 33.137}, {15.651, 27.418, 35.101}, {18.119, 25.786, 34.879}, {17.256, 27.662, 32.887}, {19.679, 26.088, 32.571}, {19.625, 28.204, 34.471}, {21.922, 26.535, 34.167}, {21.285, 28.472, 32.106}, {23.608, 26.676, 31.782}, {23.637, 28.822, 33.769}, {25.881, 27.184, 33.562}, {25.229, 29.060, 31.501}, {31.542, 28.277, 30.582}, {31.518, 30.374, 32.502}, {33.890, 28.776, 32.306}, {33.197, 30.649, 30.254}, {35.531, 29.024, 29.978}, {35.465, 31.139, 31.849}, {37.791, 29.572, 31.609}, {37.125, 31.452, 29.462}, {39.347, 29.774, 29.292}, {39.377, 31.887, 31.192}, {41.700, 30.252, 30.909}, {40.971, 32.187, 28.653}, {13.092, 28.995, 35.341}, {14.852, 29.463, 32.976}, {14.841, 31.254, 35.061}, {17.227, 29.758, 34.809}, {16.596, 31.617, 32.697}, {18.950, 30.068, 32.409}, {18.797, 32.108, 34.425}, {21.237, 30.496, 34.051}, {20.485, 32.457, 32.037}, {22.851, 30.815, 31.720}, {22.819, 32.978, 33.656}, {25.077, 31.298, 33.417}, {24.501, 33.129, 31.238}, {26.775, 31.445, 31.090}, {26.782, 33.655, 32.995}, {29.266, 31.986, 32.758}, {28.523, 33.983, 30.742}, {30.847, 32.288, 30.535}, {30.864, 34.503, 32.416}, {33.100, 32.806, 32.090}, {32.391, 34.793, 30.040}, {34.751, 33.100, 29.744}, {34.641, 35.220, 31.748}, {37.128, 33.535, 31.494}, {36.397, 35.437, 29.468}, {38.666, 33.813, 29.057}, {38.799, 35.874, 31.049}, {41.135, 34.131, 30.843}, {13.969, 33.093, 32.783}, {16.419, 33.713, 34.625}, {15.558, 35.531, 32.613}, {18.010, 33.976, 32.332}, {17.980, 36.138, 34.283}, {20.355, 34.508, 33.913}, {19.645, 36.341, 31.937}, {22.122, 34.809, 31.626}, {21.952, 36.901, 33.617}, {24.459, 35.189, 33.360}, {23.735, 37.082, 31.314}, {26.036, 35.499, 30.897}, {26.082, 37.653, 32.919}, {28.366, 36.123, 32.613}, {27.645, 37.913, 30.524}, {30.087, 36.393, 30.344}, {29.961, 38.524, 32.229}, {32.348, 36.841, 31.997}, {31.599, 38.704, 29.894}, {33.960, 37.098, 29.665}, {33.873, 39.274, 31.638}, {36.157, 37.538, 31.344}, {35.611, 39.495, 29.279}, {37.963, 37.766, 28.979}, {17.112, 38.013, 32.059}, {19.516, 38.573, 33.892}, {21.164, 38.677, 31.566}, {21.187, 40.870, 33.643}, {23.619, 39.209, 33.198}, {22.795, 41.096, 31.142}, {25.325, 39.524, 30.877}, {25.167, 41.687, 32.782}, {27.648, 40.101, 32.464}, {26.913, 42.004, 30.491}, {29.175, 40.335, 30.276}, {29.268, 42.515, 32.150}, {31.592, 40.793, 31.929}, {33.510, 41.363, 29.544}, {29.032, 13.376, 35.951}, {33.116, 13.981, 35.278}, {19.884, 15.768, 37.032}, {22.506, 16.279, 38.853}, {22.400, 14.029, 36.966}, {24.182, 16.515, 36.481}, {26.512, 14.933, 36.249}, {26.442, 17.155, 38.245}, {28.897, 15.584, 37.952}, {28.057, 17.378, 35.787}, {30.673, 15.714, 35.623}, {30.496, 17.928, 37.502}, {32.844, 16.226, 37.338}, {32.175, 18.216, 35.229}, {34.588, 16.563, 34.952}, {34.360, 18.649, 37.011}, {37.151, 14.923, 34.846}, {36.233, 18.970, 34.545}, {38.576, 19.383, 36.216}, {40.340, 19.567, 33.762}, {17.715, 21.662, 41.299}, {19.967, 18.070, 39.290}, {19.512, 19.955, 37.013}, {21.778, 18.136, 36.833}, {21.784, 20.307, 38.722}, {24.094, 18.712, 38.409}, {23.382, 20.617, 36.337}, {25.788, 19.127, 36.031}, {25.746, 21.127, 38.067}, {28.140, 19.487, 37.754}, {27.413, 21.425, 35.726}, {29.706, 19.816, 35.459}, {29.682, 21.957, 37.455}, {32.011, 20.307, 37.147}, {31.323, 22.159, 35.089}, {33.752, 20.586, 34.904}, {33.571, 22.743, 36.896}, {36.033, 20.990, 36.540}, {35.375, 22.852, 34.394}, {37.865, 21.343, 34.185}, {37.757, 23.346, 36.163}, {40.114, 21.742, 35.803}, {39.317, 23.692, 33.807}, {41.689, 24.304, 35.499}, {14.719, 23.120, 37.496}, {17.108, 21.519, 37.416}, {17.007, 23.653, 39.200}, {19.424, 22.081, 38.998}, {18.726, 23.902, 36.906}, {21.055, 22.270, 36.597}, {21.051, 24.326, 38.570}, {23.319, 22.783, 38.304}, {22.625, 24.623, 36.260}, {24.920, 22.949, 36.031}, {24.862, 25.061, 37.978}, {27.268, 23.489, 37.700}, {26.517, 25.292, 35.545}, {28.912, 23.775, 35.335}, {28.901, 25.882, 37.263}, {31.247, 24.238, 36.961}, {30.625, 26.112, 34.849}, {32.900, 24.491, 34.649}, {32.882, 26.607, 36.598}, {35.212, 24.976, 36.402}, {34.577, 26.875, 34.377}, {36.995, 25.152, 34.108}, {36.817, 27.385, 35.978}, {39.241, 25.781, 35.727}, {38.587, 27.677, 33.705}, {40.913, 26.179, 33.431}, {40.802, 28.173, 35.420}, {42.621, 28.543, 32.924}, {14.039, 25.091, 35.376}, {16.395, 25.612, 37.122}, {16.283, 27.639, 39.118}, {18.603, 25.865, 38.892}, {18.047, 27.943, 36.776}, {20.334, 26.257, 36.654}, {20.286, 28.376, 38.468}, {22.584, 26.767, 38.212}, {21.865, 28.652, 36.102}, {24.236, 26.968, 35.947}, {24.207, 29.041, 37.755}, {26.549, 27.346, 37.495}, {25.837, 29.403, 35.513}, {28.180, 27.710, 35.156}, {28.144, 29.865, 37.136}, {30.476, 28.168, 36.798}, {29.873, 30.183, 34.791}, {32.194, 28.515, 34.622}, {32.182, 30.708, 36.653}, {34.501, 29.075, 36.258}, {33.778, 30.895, 34.202}, {36.162, 29.266, 33.899}, {36.055, 31.392, 35.829}, {38.435, 29.775, 35.640}, {37.727, 31.660, 33.578}, {40.153, 30.010, 33.223}, {39.959, 32.119, 35.196}, {41.824, 32.222, 32.975}, {15.499, 29.378, 37.030}, {15.301, 31.433, 38.949}, {17.798, 30.013, 38.766}, {17.046, 31.859, 36.717}, {19.525, 30.300, 36.459}, {19.399, 32.392, 38.442}, {21.800, 30.849, 38.104}, {21.124, 32.687, 35.976}, {23.516, 30.955, 35.767}, {23.328, 32.993, 37.724}, {25.788, 31.496, 37.431}, {25.150, 33.336, 35.335}, {27.536, 31.760, 34.963}, {27.369, 33.753, 37.059}, {29.859, 32.300, 36.762}, {29.104, 34.148, 34.675}, {31.552, 32.527, 34.431}, {31.411, 34.679, 36.370}, {33.709, 33.009, 36.196}, {33.014, 34.947, 34.150}, {35.346, 33.361, 33.795}, {35.317, 35.345, 35.843}, {37.664, 33.782, 35.435}, {37.032, 35.741, 33.393}, {39.403, 33.939, 33.156}, {39.959, 34.310, 37.157}, {17.006, 33.975, 38.772}, {15.850, 38.445, 35.033}, {18.748, 34.175, 36.375}, {18.620, 36.343, 38.221}, {21.092, 34.817, 38.007}, {20.244, 36.624, 35.991}, {22.733, 35.043, 35.626}, {22.766, 37.136, 37.600}, {25.053, 35.528, 37.356}, {24.342, 37.434, 35.206}, {26.758, 35.709, 35.000}, {26.705, 37.941, 36.907}, {29.003, 36.194, 36.610}, {28.246, 38.173, 34.599}, {30.718, 36.665, 34.312}, {30.602, 38.639, 36.422}, {33.051, 37.058, 36.170}, {32.268, 39.014, 34.053}, {34.571, 37.337, 33.716}, {34.607, 39.491, 35.809}, {36.850, 37.680, 35.532}, {33.989, 41.237, 33.685}, {21.843, 39.829, 38.471}, {21.913, 39.018, 35.704}, {24.217, 39.588, 37.148}, {22.075, 41.856, 36.306}, {25.934, 39.867, 34.817}, {28.231, 40.285, 36.539}, {27.559, 42.226, 34.453}, {30.043, 40.545, 34.317}, {32.276, 41.085, 36.082}, {24.865, 16.864, 40.439}, {27.276, 17.204, 42.096}, {28.865, 17.612, 39.866}, {31.165, 16.030, 39.600}, {31.114, 18.134, 41.626}, {32.724, 18.336, 39.234}, {36.586, 19.071, 38.595}, {20.094, 20.279, 41.174}, {22.378, 18.397, 40.824}, {22.445, 20.467, 42.751}, {24.897, 19.055, 42.394}, {23.952, 20.835, 40.391}, {26.420, 19.196, 40.091}, {26.315, 21.439, 42.042}, {28.648, 19.647, 41.784}, {28.071, 21.674, 39.717}, {30.385, 19.976, 39.621}, {30.266, 22.126, 41.459}, {32.691, 20.448, 41.159}, {31.930, 22.365, 39.164}, {34.208, 20.818, 38.864}, {34.261, 22.887, 40.839}, {36.637, 21.241, 40.554}, {36.001, 23.085, 38.458}, {38.389, 21.457, 38.118}, {38.253, 23.651, 40.026}, {39.970, 23.918, 37.691}, {20.169, 22.307, 43.075}, {19.312, 24.094, 41.002}, {21.726, 22.493, 40.686}, {21.568, 24.789, 42.648}, {23.947, 22.993, 42.348}, {23.361, 24.907, 40.400}, {25.532, 23.198, 39.993}, {25.584, 25.330, 41.980}, {27.972, 23.793, 41.660}, {27.220, 25.710, 39.653}, {29.558, 24.064, 39.364}, {29.522, 26.164, 41.378}, {31.722, 24.540, 41.085}, {31.156, 26.487, 38.880}, {33.477, 24.686, 38.784}, {33.415, 26.946, 40.572}, {35.811, 25.326, 40.359}, {35.143, 27.128, 38.260}, {37.571, 25.480, 38.111}, {37.482, 27.664, 39.988}, {39.916, 25.997, 39.825}, {41.574, 26.258, 37.478}, {16.923, 25.570, 41.235}, {19.086, 26.349, 42.993}, {18.517, 27.988, 40.779}, {20.922, 26.445, 40.588}, {20.750, 28.629, 42.481}, {23.133, 27.067, 42.298}, {22.533, 28.866, 40.212}, {24.873, 27.193, 39.901}, {24.871, 29.371, 41.890}, {27.151, 27.758, 41.579}, {26.424, 29.613, 39.534}, {28.760, 27.934, 39.219}, {28.737, 30.070, 41.151}, {31.065, 28.503, 40.913}, {30.352, 30.331, 38.826}, {32.728, 28.760, 38.485}, {32.687, 30.703, 40.505}, {35.162, 29.214, 40.342}, {34.446, 31.060, 38.243}, {36.750, 29.463, 37.971}, {36.758, 31.591, 39.941}, {39.225, 27.939, 37.764}, {38.358, 31.891, 37.519}, {40.904, 30.303, 37.370}, {18.383, 30.152, 42.782}, {17.601, 32.108, 40.790}, {20.065, 30.386, 40.366}, {20.058, 32.427, 42.358}, {22.526, 31.009, 41.964}, {21.740, 32.901, 40.066}, {24.227, 31.164, 39.713}, {24.137, 33.374, 41.561}, {26.384, 31.692, 41.454}, {25.635, 33.592, 39.383}, {27.947, 31.994, 39.164}, {27.957, 34.094, 41.099}, {30.274, 32.456, 40.849}, {29.705, 34.399, 38.767}, {31.997, 32.763, 38.657}, {31.981, 34.869, 40.517}, {34.390, 33.152, 40.283}, {33.680, 34.997, 38.177}, {35.997, 33.448, 37.890}, {37.632, 37.870, 39.347}, {38.316, 33.843, 39.612}, {37.568, 35.663, 37.613}, {19.454, 34.591, 40.216}, {21.777, 34.927, 41.981}, {20.908, 36.993, 40.018}, {23.312, 35.320, 39.608}, {24.253, 34.081, 45.458}, {25.514, 35.819, 41.227}, {25.008, 37.670, 39.218}, {27.346, 35.904, 38.994}, {27.282, 38.094, 40.957}, {29.453, 36.490, 40.759}, {28.930, 38.323, 38.661}, {31.286, 36.805, 38.533}, {30.459, 41.012, 38.273}, {33.557, 37.247, 40.207}, {32.765, 39.228, 38.108}, {35.162, 37.401, 37.845}, {26.393, 40.013, 38.945}, {28.587, 21.718, 43.714}, {30.855, 20.210, 43.314}, {32.575, 22.700, 43.005}, {36.717, 23.426, 42.414}, {22.562, 22.208, 45.225}, {26.397, 23.505, 43.963}, {26.300, 25.640, 45.921}, {28.439, 23.732, 45.930}, {27.844, 25.976, 43.647}, {30.228, 24.235, 43.438}, {30.189, 26.276, 45.369}, {34.217, 27.125, 44.697}, {31.893, 26.609, 42.999}, {34.099, 25.051, 42.635}, {35.846, 29.545, 44.293}, {35.792, 27.322, 42.325}, {23.965, 25.051, 44.340}, {21.466, 26.934, 44.589}, {25.379, 27.482, 43.976}, {24.112, 27.566, 46.797}, {27.592, 28.172, 45.636}, {27.115, 29.892, 43.553}, {29.529, 28.280, 43.308}, {29.433, 30.438, 45.156}, {31.755, 28.683, 44.987}, {31.052, 30.704, 42.957}, {33.465, 29.051, 42.693}, {33.542, 31.189, 44.696}, {35.104, 31.354, 42.234}, {37.579, 29.755, 42.045}, {23.072, 29.197, 44.264}, {24.855, 31.529, 43.788}, {27.200, 32.058, 45.529}, {26.332, 33.917, 43.403}, {28.655, 32.286, 43.143}, {30.955, 32.865, 44.933}, {30.340, 34.687, 42.787}, {32.738, 32.862, 42.523}, {36.032, 35.459, 39.925}, {27.782, 36.249, 43.060}};
    const int NAU = 884;

    // already in bohr
    /*
    for (int i=0; i<NAU; ++i) {
        au[i].x *= AMS;
        au[i].y *= AMS;
        au[i].z *= AMS;
    }
    */



    if (1) {

        char a; cin>>a;

        tensor2 T(NAU);

        double v[NAU];

        /*
        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<=i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z)/1000;

                T(i,j) = T(j,i) = -r2*r2*r2*r2*r2*r2;
            }
        }
        */

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                T(i,j) = T(j,i) = 1/sqrt(r2);
            }
            T(i,i) = 1;
        }


        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = NAU*NAU;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = NAU;

        dsyev_ (&JOBZ, &UL, &SIZE, T.c2, &SIZE, v, WORK, &LWORK, &INFO);

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;

        tensor2 W(NAU);

        for (int i=0; i<NAU; ++i) {
            const double w1 = sqrt(0.03);
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);
                double r = sqrt(r2);

                W(i,j) = W(j,i) = erf(w1*r)/r ; // exp(-0.05*r2);
            }
            W(i,i) = 2./sqrt(PI) * w1;
        }

        tensor2 J(NAU);


        for (int i=0; i<NAU; ++i) {
            double ww=0;
            for (int l=0; l<NAU; ++l) {
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*T(i,l)*W(k,l);
            }
            v[i] = ww;
        }

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;

        /*


        for (int i=0; i<NAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*W(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<NAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=T(j,l)*cc[l];
                J(i,j) = jj;
            }
        }

        cout.precision(4);
        cout.setf(ios::fixed);
        cout << endl << endl;
        cout << J << endl;
        */

        char aaa; cin >> aaa;




        // compute product in Fourier
        for (int n=0; n<NAU; ++n) {
            cout << n << " : ";
            for (int i=0; i<NAU; ++i) {
                    double s3=0;
                    for (int k=0; k<NAU; ++k) s3 += T(i,k)*T(i,k)*T(n,k);
                    s3*=sqrt(double(NAU));
                    cout << " " << s3;
            }
            char a; cin>>a;
            cout << endl;
        }

    }

    return 0;





    IncompleteGamma.InitIncompleteGammaTable();


    const double w2 = 0.5*0.131957; // from silver; unconvinced
    const double nu = 2*w2;
    const double ww = nu; // from the ion

    tensor2 S(NAU);
    tensor2 T(NAU);
    tensor2 V(NAU);
    tensor2 C(NAU);
    tensor2 D(NAU);
    tensor2 F(NAU);
    tensor2 J(NAU);

    tensor2 JJ(NAU);
    tensor2 XX(NAU);

    tensor2 W(NAU);
    tensor2 II(NAU);
    tensor2 AA(NAU);




    const double SG1 = sqrt(PI/nu);
    const double SG3 = SG1*SG1*SG1;

    center * cww = new center[NAU*NAU];

    int ij=0;

    for (int i=0; i<NAU; ++i) {
        for (int j=0; j<=i; ++j) {

            double px = 0.5*(au[i].x + au[j].x);
            double py = 0.5*(au[i].y + au[j].y);
            double pz = 0.5*(au[i].z + au[j].z);

            double x = au[i].x - au[j].x;
            double y = au[i].y - au[j].y;
            double z = au[i].z - au[j].z;
            double r2 = (x*x+y*y+z*z);

            double K = exp(-0.5*w2*r2) / SG3;

            // set center
            {
                cww[ij].i = i;
                cww[ij].j = j;
                cww[ij].x = px;
                cww[ij].y = py;
                cww[ij].z = pz;
                cww[ij].w = K;
                cww[ij].r2 = r2;
            }

            ++ij;
        }
    }

    FlashSortC (cww, (NAU*NAU+NAU)/2);

    const double r2max = 2.*10.*log(10.)/w2;

    for (ij=0; ij<(NAU*NAU+NAU)/2; ++ij)
        if (cww[ij].r2>r2max) break;

    const int n2max = ij;

    cout << "N2max: " << n2max << endl;



    S.zeroize();
    T.zeroize();
    J.zeroize();
    V.zeroize();

    const double TG1 = 0.5*sqrt(PI*nu);

    const double th = (ww*nu)/(ww+nu);
    const double sqrth = sqrt(th);

    for (int ij=0; ij<n2max; ++ij) {

        double K = cww[ij].w;

        const int i=cww[ij].i;
        const int j=cww[ij].j;

        S(i,j) = S(j,i) = K*SG3; // don't normalize
        T(i,j) = T(j,i) = K*TG1*SG1*SG1*(3.-(0.5*nu)*cww[ij].r2); // don't normalize

        J(i,j) = J(j,i) = K;
        //-2.18388   -2.15613   -2.15814   -2.15723   -1.497   -1.4241   -1.54363
        //51.599 50.5832 50.6381  50.6017 31.2078 29.6027 32.2546

        // background potential contribution
        // each ion contributes one unit charge

        double vv=0;
        for (int k=0; k<NAU; ++k) {
            double xx = cww[ij].x - au[k].x;
            double yy = cww[ij].y - au[k].y;
            double zz = cww[ij].z - au[k].z;

            double rr = xx*xx+yy*yy+zz*zz;

            vv-=IncompleteGamma.calcF2(th*rr);
        }

        V(i,j) = V(j,i) = K * (PI/nu)*sqrt(PI/nu) *  2*sqrth/sqrt(PI) * vv;
    }


    for (int k=0; k<NAU; ++k) cout << V(k,k) << "   "; cout << endl;

    /*
    int i4 = 0;

    for (int ij=0; ij<n2max; ++ij) {
        for (int kl=0; kl<n2max; ++kl) {
            if (cww[ij].r2+cww[kl].r2 > r2max) break;

            ++i4;
        }
    }

    cout << "I4: " << i4 << endl;
    */

    //return 0;


    double w[NAU];

    F = T; // initial guess

    // check triple product
    {
        // compute eigensystem
        {
            int info, lwork;
            double wkopt;
            double * work;

            //int itype = 1; // A v = l B v
            int n = S.n;
            int itype = 1;

            C = F;
            D = S;

            lwork = -1;
            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w, &wkopt, &lwork, &info );
            lwork = (int)wkopt;
            work = new double[lwork];

            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w,   work, &lwork, &info );

            if (info>0) throw info; //meaningless error code

            delete[] work;
        }

        // need to declare a tensor3

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<=i; ++j) {
                for (int k=0; k<=j; ++k) {
                }
            }
        }

    }








    while(true) {
        // compute eigensystem
        {
            int info, lwork;
            double wkopt;
            double * work;

            //int itype = 1; // A v = l B v
            int n = S.n;
            int itype = 1;

            C = F;
            D = S;

            lwork = -1;
            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w, &wkopt, &lwork, &info );
            lwork = (int)wkopt;
            work = new double[lwork];

            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w,   work, &lwork, &info );

            if (info>0) throw info; //meaningless error code

            delete[] work;
        }

        // print evs
        for (int k=0; k<NAU; ++k) cout << k << " " << w[k]<< endl;

        // build density matrix using Fermi-Dirac
        const double beta = 1000.; // inverse temp. (in au)

        cout << "beta: " << beta << endl;
        FDD(D, C, NAU/2, w, NAU, beta);


        for (int i=0; i<NAU; ++i) D(i,i)*=0.5; // so later we can use the triangle


        // check charge consistency
        {
            double qsum=0;

            for (int i=0; i<NAU; ++i)
                for (int j=0; j<=i; ++j)
                    qsum += D(i,j)*J(i,j);

            cout << "charge sum: " << qsum*SG3 << endl;
        }


        // assemble Fock

        F = T;
        //F += V;

        const double sqrtw = (PI*PI*PI)/(nu*nu*nu) * 2/sqrt(PI) * sqrt(.5*nu);


        cout << "Coulomb" << endl;
        JJ.zeroize();

        /*

        #pragma omp parallel for schedule(dynamic)
        for (int ij=0; ij<n2max; ++ij) {
            double sum = 0;

            for (int kl=0; kl<n2max; ++kl) {
                if (cww[ij].r2+cww[kl].r2 > r2max) break;

                double rx = cww[ij].x-cww[kl].x;
                double ry = cww[ij].y-cww[kl].y;
                double rz = cww[ij].z-cww[kl].z;

                double r2 = rx*rx+ry*ry+rz*rz;

                const int k = cww[kl].i;
                const int l = cww[kl].j;

                sum += D(k,l) * cww[kl].w* IncompleteGamma.calcF2(0.5*nu*r2);
            }
            const int i = cww[ij].i;
            const int j = cww[ij].j;
            JJ(i,j) += 2*2*cww[ij].w * sqrtw * sum;
        }

        F += JJ;
        */


        /*

        for (int i =0; i<NAU; ++i) {
            for (int j =0; j<NAU; ++j) {
                cout << JJ(i,j) << "   ";
            }
            cout << endl;
        }
        cout << endl;
        cout << endl;
        */

        cout << "Exchange" << endl;
        XX.zeroize();

        for (int i=0; i<NAU; ++i) D(i,i)*=2;

        // substracts exchage
        #pragma omp parallel for schedule(dynamic)
        for (int k=0; k<NAU; ++k) {
            for (int l=0; l<=k; ++l) {

                double sum = 0;

                double px = (au[k].x - au[l].x);
                double py = (au[k].y - au[l].y);
                double pz = (au[k].z - au[l].z);

                for (int i=0; i<NAU; ++i) {
                    if(J(i,k)<1e-10) continue;

                    double qx = px + au[i].x;
                    double qy = py + au[i].y;
                    double qz = pz + au[i].z;

                    for (int j=0; j<NAU; ++j) {

                        if(J(i,k)*J(j,l)<1e-12) continue;

                        double rx = qx - au[j].x;
                        double ry = qy - au[j].y;
                        double rz = qz - au[j].z;

                        double R2 = 0.25* (rx*rx+ry*ry+rz*rz);

                        sum += D(i,j) * (J(i,k)*J(j,l))  * IncompleteGamma.calcF2(.5*nu*R2);
                    }
                }
                XX(k,l) -= sqrtw *sum;
            }
        }

        /*
        // this should work if we account for symmetry
        #pragma omp parallel for schedule(dynamic)
        for (int ij=0; ij<n2max; ++ij) {
            double sum = 0;

            for (int kl=0; kl<n2max; ++kl) {
                if (cww[ij].r2+cww[kl].r2 > r2max) break;

                double rx = cww[ij].x-cww[kl].x;
                double ry = cww[ij].y-cww[kl].y;
                double rz = cww[ij].z-cww[kl].z;

                double r2 = rx*rx+ry*ry+rz*rz;

                const int i = cww[ij].i;
                const int j = cww[ij].j;
                const int k = cww[kl].i;
                const int l = cww[kl].j;

                double ii = sqrtw * cww[ij].w * cww[kl].w* IncompleteGamma.calcF2(0.5*nu*r2);

                XX(i,k) -= 2* ii * D(j,l);
                XX(i,l) -= 2* ii * D(j,k);
                XX(j,k) -= 2* ii * D(i,l);
                XX(j,l) -= 2* ii * D(i,k);
            }
        }
        */



        F += XX;

        /*
        for (int i =0; i<NAU; ++i) {
            for (int j =0; j<NAU; ++j) {
                cout << XX(i,j) << "   ";
            }
            cout << endl;
        }
        cout << endl;
        cout << endl;
        */


        cout << "Reform Fock matrix" << endl;

        for (int i=0; i<NAU; ++i)
            for (int j=0; j<=i; ++j)
                F(j,i) = F(i,j);
    }



    /*
    cout.precision(16);

    for (int i=0; i<NAU; ++i) cout << w[i] << ","; //cout << i << "   " <<w[i] << endl;

    // now compute a few diagonal

    (AA | II)  is much more substantial than (ia|ia) :
    (AA | II)  is of the same order always (two spread densities repelling)
    (AA | II)  reoughly depends on the size of the system

        int i= 254; //NAU/2-37;
    int a = 657; //NAU/2 +12;
    energy: 0.04218367535352032
A fock: 0.3788914117966032
diagonal energy: 0.1678987374153858

    int i= NAU/2-37;
    int a = NAU/2 +12;
energy: 0.03845997170256591
A fock: 0.04592285513884481
diagonal energy: 0.1574262890304308

    int i= NAU/4-37;
    int a = 3*NAU/4 +12;
energy: 0.0414706970925063
A fock: 0.465775286952472
diagonal energy: 0.1221114545421553

    int i = NAU/2-1;
    int a = NAU/2;
    aaii
    energy: 0.0379157390962336
A fock: 0.001537201252099774
diagonal energy: 0.1673628307033906
aiai
energy: 0.0007232736280711949
A fock: 0.001537201252099774

*/

    int i = NAU/2-1;
    int a = NAU/2;

    for (int p=0; p<NAU; ++p)
        cout << p << " " << T(i,p) << "   " << T(a,p) << endl;

    int c0,c5,c6,c7,c10,c15;
    c0=c5=c6=c7=c10=c15=0;


    for (int p=0; p<NAU; ++p) {
        for (int q=0; q<NAU; ++q) {
            W(p,q) = T(i,p)*T(a,q) * J(p,q);
            II(p,q) = T(i,p)*T(i,q) * J(p,q);
            AA(p,q) = T(a,p)*T(a,q) * J(p,q);
            if (fabs(W(p,q))>1) ++c0;
            if (fabs(W(p,q))>1e-7) ++c7;
            if (fabs(W(p,q))>1e-6) ++c6;
            if (fabs(W(p,q))>1e-5) ++c5;
            if (fabs(W(p,q))>1e-10) ++c10;
            if (fabs(W(p,q))>1e-15) ++c15;
        }
    }
cout << endl;
    cout << c0 << endl;
    cout << c5 << endl;
    cout << c6 << endl;
    cout << c7 << endl;
    cout << c10 << endl;
    cout << c15 << endl;

    double jia = 0;

    double f00 = 1;
    double kkk = 0.5*sqrt(PI);

    for (int p=0; p<NAU; ++p)
        for (int q=0; q<p; ++q) {
            W(p,q) += W(q,p);
            II(p,q) += II(q,p);
            AA(p,q) += AA(q,p);
        }

    for (int p=0; p<NAU; ++p) {
        for (int q=0; q<=p; ++q) {
            if (fabs(W(p,q))<1e-10) continue;
            double pqx = 0.5* (au[p].x + au[q].x);
            double pqy = 0.5* (au[p].y + au[q].y);
            double pqz = 0.5* (au[p].z + au[q].z);
            for (int r=0; r<NAU; ++r) {
                for (int s=0; s<=r; ++s) {
                    if (fabs(W(p,q)*W(r,s))<1e-14) continue;
                    double rsx = 0.5*(au[r].x + au[s].x);
                    double rsy = 0.5*(au[r].y + au[s].y);
                    double rsz = 0.5*(au[r].z + au[s].z);

                    double x=rsx-pqx;
                    double y=rsy-pqy;
                    double z=rsz-pqz;

                    double wr = sqrt(w2*(x*x+y*y+z*z));

                    if (wr>0) jia += (W(p,q)*W(r,s))* kkk*erf(wr) / wr;
                    else     jia += (W(p,q)*W(r,s))*f00;
                }
            }
        }
    }

    jia *= 2*PI*PI*sqrt(PI)/(2*w2*2*w2*sqrt(4*w2));

    cout << "energy: " << jia << endl;
    cout << "A fock: " << w[a]-w[i] << endl;

    double jj = 0;

    for (int p=0; p<NAU; ++p) {
        for (int r=0; r<NAU; ++r) {
            double x=(au[r].x-au[p].x);
            double y=(au[r].y-au[p].y);
            double z=(au[r].z-au[p].z);

            double wr = sqrt(w2*(x*x+y*y+z*z));

            if (wr>0) jj += (W(p,p)*W(r,r))* kkk*erf(wr) / wr;
            else     jj += (W(p,p)*W(r,r))*f00;
        }
    }

    jj *= 2*PI*PI*sqrt(PI)/(2*w2*2*w2*sqrt(4*w2));


    cout << "diagonal energy: " <<   jj << endl;

    /*
        for (int i=0; i<NAU; ++i) {
        for (int j=0; j<NAU; ++j) {
            cout << W(i,j) << " ";
        }
        cout << endl;
    }
    cout << endl;
    cout << endl;
    */


    return 0;
}


// hardware
// ========

class HWbuffer {
    /*
    char * pool;
    size_t size;
    size_t used;

    HWbuffer() {
        pool = NULL;
        size = 0;
        used = 0;
    }
   ~HWbuffer();{
        delete[] pool;
    }
    */

    HWbuffer();
   ~HWbuffer();

    template<class T> T *  get (size_t N); // get a pointer to a chunk of memory
    template<class T> void put (T * p);    //
};

// hardware definitions, which can be useful for
// selecting algorithms, parameters, etc.


/*
struct hardwareModel {
    int nNodes;
    nodeModel * node;
    // network model?
};

struct nodeModel {
    int    nCPUs;
    size_t memory;
    int    nGPUs;
    cpuModel * cpu;
    gpuModel * gpu;
};

struct cpuModel {
    size_t L1size;
    size_t L2size;
    size_t L3size;
    // vector model, etc.
};

struct gpuModel {
    int    nStreams;
    size_t memory;
};


// the more general picture:
struct memory;    // which can be a cache, a RAM, etc.
struct bus;       // which can be an internal bus or a network
struct processor; // which can be a cpu core, a stream processor, etc.


*/
// tensors
// =======

// this is an interface; implementation is uncoupled, and may be multithreaded, GPU, MPI or all of the above
class Tensor {
    // access(es) : there may be multiple ways of indexing the tensor, coupled to the struct through different adaptors
    // iterator
    // basic operations
};

// index pattern that is inherited by a tensor
class TensorStructure {

};

// recursive indices if needed; indices can be collapsed to another index structure
class IndexStructure {

};

// evaluation
// ==========

// everything that changes the internal state of an object must be handled by the object itself

// a step in the calculation

class Unit {
    // list of input state
    // list of dependencies

};

// a kind of unit
class Solver {
};

// data that must be generated (if necessary)
class Data {
};

// other
// =====

// the wavefunction of a system; must also contain orbital information, etc.

class Wavefunction {

};


// what? how?
// what (what) <- what

// phyics     "real-world" physics concepts;                             atom, molecule, properties, etc.
//     translation
// model      physico-mathematical methods & data assocated;             basis set, method, etc.
//     translation
// math       actual mathematical operations needed to solve the model;  solver, iteerations, etc.
//     translation
// hardware   actual low level operations needed to solve the problem



// RRs
// ===

// specify them through some language (PROLOG?)

/*
g(n,m) := n*g(n-1,m)+g(n,m-2)             // some RR

h(0,0,0)   := g(0,2,3)                    // remapping of a function to some other
h(i+1,j,k) := j*h(i,j,k) + AB*h(i,j,k-1)  // dummy arguments

h(0:3,0:2,0:5) // target
*/


// unrelated program
// =================

// U: basis w: occupation N: dimension of basis R: rank (number of vectors)
void FindRelevant (tensor2 & U, double * w, int N, int R) {

    // compute the SVD of the (extended) basis

    tensor2 W(R,R);

    W.zeroize();

    for (int r=0; r<R; ++r) {
        for (int s=0; s<R; ++s) {
            double ss  = 0;

            for (int i=0; i<N; ++i)
                ss += U(r,i)*U(s,i);

            W(r,s) = ss*ss;
        }
    }

    char a;

    //cout << W << endl << endl;
    //cin>>a;

    double e[max(R,N)];

    {
        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = R*R;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = R;

        dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, e, WORK, &LWORK, &INFO);

        delete[] WORK;
    }

    cout << "SVD^2  = ";  for (int i=0; i<R; ++i) cout << e[i] << " "; cout << endl << endl;

    //cout << W << endl << endl;
    //cin>>a;

    /*

    tensor2 U2(N,N);

    // diagonalize each slice:
    for (int r=0; r<R; ++r) {

        // create the matrix
        U2.zeroize();

        for (int s=0; s<R; ++s) {
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    U2(i,j) += W(r,s) * U(s,i) * U(s,j);
        }

        // diagonalize it
        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int LWORK = N*N;  // size of working space
            double * WORK = new double[LWORK];   // nothing
            int SIZE = N;

            dsyev_ (&JOBZ, &UL, &SIZE, U2.c2, &SIZE, e, WORK, &LWORK, &INFO);

            delete[] WORK;
        }

        //cout << " slice " << r << "  ";  for (int i=0; i<N; ++i) cout << e[i] << " "; cout << endl;
        //char a; cin>>a;
    }
    */

}



// extracts the SVD'd slices and diagonalizes them, returning the basis
// and an appropriate value for R
void IguessBase(tensor2 & RR, tensor2 & LL, double * ll, const tensor2 * TT, int N, int K, int & R, const double DTHRESH = 1e-2) {

    // compute the SVD of the (extended) basis
    // can be sped up to N^3

    tensor2 SS(K,K);
    double ss[K];

    SS.zeroize();

    for (int r=0; r<K; ++r) {
        for (int s=0; s<K; ++s) {
            double ss  = 0;

            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    ss += TT[r](i,j)*TT[s](i,j);

            SS(r,s) = -ss;
        }
    }



    {
        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = K*K;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = K;

        dsyev_ (&JOBZ, &UL, &SIZE, SS.c2, &SIZE, ss, WORK, &LWORK, &INFO);

        delete[] WORK;
    }

    //cout << "SVD^2  = ";  for (int i=0; i<K; ++i) cout << -ss[i] << " "; cout << endl << endl;
    //for (int i=0; i<K; ++i) ss[i] = ss[i]>0?-sqrt(ss[i]):0;


    tensor2 MM(N,N);
    double mm[N];

    // diagonalize each slice:
    for (int r=0; r<K; ++r) {

        // create the matrix
        MM.zeroize();

        for (int s=0; s<K; ++s) {
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    MM(i,j) += SS(r,s) * TT[s](i,j);
        }

        // diagonalize it
        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int LWORK = N*N;  // size of working space
            double * WORK = new double[LWORK];   // nothing
            int SIZE = N;

            dsyev_ (&JOBZ, &UL, &SIZE, MM.c2, &SIZE, mm, WORK, &LWORK, &INFO);

            delete[] WORK;
        }


        // copy back
        for (int n=0; n<N; ++n) {
            for (int s=0; s<K; ++s) {
                RR(r*N+n,s) = SS(s,r);
            }

            for (int j=0; j<N; ++j) {
                LL(r*N+n,j) = MM(n,j);
            }
            ll[r*N+n] = -mm[n];
        }
    }

    // awful sort (can be done faster using mergesort, but whatevs)
    for (int n=0; n<N*K; ++n) {
        int l=n;
        for (int m=n+1; m<N*K; ++m) {
            if (fabs(ll[m])>fabs(ll[l])) l=m;
        }

        swap(ll[n],ll[l]);

        for (int i=0; i<N; ++i) {
            swap(LL(n,i),LL(l,i));
        }
    }


    // determine a good value for R
    {
        double f2=0;
        for (int n=0; n<N*K; ++n) f2+=ll[n]*ll[n];

        double r2=0;
        for (int n=0; n<N*K; ++n) {
            r2+=ll[n]*ll[n];
            if ((f2-r2)<f2*DTHRESH) {
                R=n;
                break;
            }
        }
    }

    cout.precision(5);
    for (int n=0; n<R; ++n) cout << ll[n] << " "; cout << endl;
    cout << N << " " << K << " " << R << " " << R/N << endl;
    cout << endl;
    for (int n=0; n<N*K; n+=N) cout << n << " : " << ll[n] << " "; cout << endl;
    cout << endl;
    cout.precision(15);
}

// diagonalizes a set of matrices and sorts their eigenpairs in decreasing absolut order
// LL: basis (rectangular)
// ll: eigenvalues of the basis (occupation)
// TT: 3-tensor / array of square tensors
// N : dimension of the space
// K : dimension of the fitting basis aka. third index of the tensor
void Rebase(tensor2 & LL, double * ll, const tensor2 * TT, int N, int K) {

    //LL.setsize(K*N,N);
    tensor2 SS(N);

    for (int k=0; k<K; ++k) {

        SS = TT[k];

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int LWORK = N*N;  // size of working space
            double * WORK = new double[LWORK];   // nothing
            int SIZE = N;

            dsyev_ (&JOBZ, &UL, &SIZE, SS.c2, &SIZE, &ll[k*N], WORK, &LWORK, &INFO);

            for (int n=0; n<N; ++n) cout << ll[k*N+n] << " "; cout << endl;
        }

        // copy back
        for (int n=0; n<N; ++n) {
            for (int j=0; j<N; ++j) {
                LL(k*N+n,j) = SS(n,j);
            }
        }
    }

    // awful sort (can be done faster using mergesort, but whatevs)
    for (int n=0; n<N*K; ++n) {
        int l=n;
        for (int m=n+1; m<N*K; ++m) {
            if (fabs(ll[m])>fabs(ll[l])) l=m;
        }

        swap(ll[n],ll[l]);

        for (int i=0; i<N; ++i) {
            swap(LL(n,i),LL(l,i));
        }
    }

}

void BuildOverlap (tensor2 * KK, tensor2 & SS, const tensor2 & LL, double * ll, int N, int R) {

    // generate matrix S
    SS.setsize(R);
    double ss[R];

    for (int r=0; r<R; ++r) {
        for (int s=0; s<R; ++s) {
            double l2=0;
            for (int n=0; n<N; ++n) l2 += LL(r,n)*LL(s,n);

            // the weighted version is for MakeKMatrices
            //SS(r,s) = -l2*l2*ll[r]*ll[s]; // sort them in decreasing order
            SS(r,s) = l2*l2; // this is for aLS
        }
    }
}

void SolveALS1 (const tensor2 * KK, tensor2 & SS, const tensor2 & LL, double * ll, int N, int K, int R) {

    tensor2 TT(K,R);
    tensor2 DD(K,R);

    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    for (int k=0; k<K; ++k) {
        for (int r=0; r<R; ++r) {
            double t2=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    t2 += KK[k](i,j)*LL(r,i)*LL(r,j);

            TT(k,r) = t2; // sort them in decreasing order
        }
    }

    // now we must solve the linear system

    // invert SS
    {
        int *IPIV = new int[R+1];
        int LWORK = R*R;
        double *WORK = new double[LWORK];
        int INFO;

        dgetrf_(&R,&R,SS.c2,&R,IPIV,&INFO);
        dgetri_(&R,SS.c2,&R,IPIV,WORK,&LWORK,&INFO);

        delete IPIV;
        delete WORK;
    }

    // solve the systems
    for (int k=0; k<K; ++k) {
        for (int r=0; r<R; ++r) {
            double dd=0;
            for (int s=0; s<R; ++s) {
                dd+= SS(r,s)*TT(k,s);
            }
            DD(k,r) = dd;
        }
    }

    // check residual
    double r2 = 0;
    double x2 = 0;
    for (int k=0; k<K; ++k)
        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j) {
                double xx=KK[k](i,j);
                x2 += xx*xx;
                for (int r=0; r<R; ++r) xx -= DD(k,r) * LL(r,i) * LL(r,j);
                r2 += xx*xx;
            }

    cout << "|X|^2 = " << x2 << endl;
    cout << "|R|^2 = " << r2 << endl;

}

void MakeKMatrices(tensor2 * KK, tensor2 & SS, tensor2 & LL, double * ll, int N, int R) {

    // generate matrix S
    double ss[R];

    // diagonalize
    {
        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = R*R;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = R;

        dsyev_ (&JOBZ, &UL, &SIZE, SS.c2, &SIZE, ss, WORK, &LWORK, &INFO);

        for (int n=0; n<R; ++n) ss[n] = -ss[n];

        for (int n=0; n<R; ++n) cout << ss[n] << " "; cout << endl;
    }





    // generate the K matrices
    for (int r=0; r<R; ++r) {


        KK[r].zeroize();

        for (int s=0; s<R; ++s) {
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    KK[r](i,j) += SS(r,s) * ll[s] * LL(s,i)*LL(s,j);
        }

        KK[r] *= sqrt(ss[r]);
    }


    double kk[N];

    // diagonalize the K matrices
    for (int r=0; r<R; ++r) {

        // diagonalize it
        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int LWORK = N*N;  // size of working space
            double * WORK = new double[LWORK];   // nothing
            int SIZE = N;

            dsyev_ (&JOBZ, &UL, &SIZE, KK[r].c2, &SIZE, kk, WORK, &LWORK, &INFO);

            delete[] WORK;
        }


        cout << " K matrix n. " << r << "  ";  for (int i=0; i<N; ++i) if (fabs(kk[i])>1e-4) cout << -kk[i] << " "; cout << endl;


        // copy back (only first?)
        {
            for (int j=0; j<N; ++j) {
                LL(r,j) = KK[r](0,j);
            }
            ll[r] = -kk[0];
        }
    }



    // awful sort (can be done faster using mergesort, but whatevs)
    for (int n=0; n<R; ++n) {
        int l=n;
        for (int m=n+1; m<R; ++m) {
            if (fabs(ll[m])>fabs(ll[l])) l=m;
        }

        swap(ll[n],ll[l]);

        for (int i=0; i<N; ++i) {
            swap(LL(n,i),LL(l,i));
        }
    }
}

void RebuildTensor (tensor2 * TT, const tensor2 & LL, const double * ll, const tensor2 & SS, const double * ss, int N, int K, int R) {

/*
    // generate matrix S
    S.setsize(R);
    double w[R];

    for (int r=0; r<R; ++r) {
        for (int s=0; s<R; ++s) {
            double ss=0;
            for (int n=0; n<N; ++n) ss += L(r,n)*L(s,n);

            S(r,s) = ss*ss*c[r]*c[s];
        }
    }

    // diagonalize
    {
        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = R*R;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = R;

        dsyev_ (&JOBZ, &UL, &SIZE, S.c2, &SIZE, w, WORK, &LWORK, &INFO);

        for (int n=0; n<R; ++n) cout << w[n] << " "; cout << endl;
    }
*/
}





// ***************************
// ***************************






const double PRPR = 1.e-9;

void ContractIJ (tensor2 & TT, const tensor2 & RR, const tensor2 & LL, const double * ll, const tensor2 & UU, const tensor2 & VV, int N, int K, int R) {
/*
    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    for (int k=0; k<K; ++k) {
        for (int r=0; r<R; ++r) {
            double t2=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    t2 += XX[k](i,j)*UU(r,i)*VV(r,j);

            TT(r,k) = t2;
        }
    }
    */
    TT.zeroize();

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int h=0; h<N*K; ++h) {
            if (fabs(ll[h])<PRPR) continue;

            double lu=0, lv=0, rw=0;

            for (int i=0; i<N; ++i)
                lu += LL(h,i)*UU(r,i);
            for (int j=0; j<N; ++j)
                lv += LL(h,j)*VV(r,j);

            double tp = lu*lv*ll[h];

            for (int k=0; k<K; ++k)
                TT(r,k) += tp*RR(h,k);
        }
    }
}

void ContractIK (tensor2 & TT, const tensor2 & RR, const tensor2 & LL, const double * ll, const tensor2 & UU, const tensor2 & WW, int N, int K, int R) {
/*
    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    for (int j=0; j<N; ++j)
        for (int r=0; r<R; ++r) {
            double t2=0;
            for (int i=0; i<N; ++i)
                for (int k=0; k<K; ++k) {
                    t2 += XX[k](i,j)*UU(r,i)*WW(r,k);

            TT(r,j) = t2;
        }
    }
    */

    TT.zeroize();

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int h=0; h<N*K; ++h) {
            if (fabs(ll[h])<PRPR) continue;

            double lu=0, lv=0, rw=0;

            for (int i=0; i<N; ++i)
                lu += LL(h,i)*UU(r,i);
            for (int k=0; k<K; ++k)
                rw += RR(h,k)*WW(r,k);

            double tp = lu*rw*ll[h];

            for (int j=0; j<N; ++j)
                TT(r,j) += tp*LL(h,j);
        }
    }
}




void ContractIJ (tensor2 & TT, const tensor2 * XX, const tensor2 & UU, const tensor2 & VV, int N, int K, int R) {
    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int k=0; k<K; ++k) {
            double t2=0;
            for (int i=0; i<N; ++i) {
                double xv=0;
                for (int j=0; j<N; ++j)
                    xv += XX[k](i,j)*VV(r,j);
                t2+=UU(r,i)*xv;
            }

            TT(r,k) = t2;
        }
    }
}

void ContractXXW (tensor2 & TT, const tensor2 * XX, const tensor3 & X, const double * vv, const tensor2 & W, const tensor2 & UU, const tensor2 & VV, int N, int M, int R) {

    tensor2 P(R,M);

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int n=0; n<M; ++n) {
            double uvx=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    uvx += UU(r,i)*VV(r,j)*X(n,i,j);
            P(r,n)=uvx/vv[n];
        }
    }

    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int k=0; k<M; ++k) {

            double tt=0;

            for (int n=0; n<M; ++n)
                tt+=P(r,n)*W(n,k);

            TT(r,k) = tt;
        }
    }
}


void ContractIK (tensor2 & TT, const tensor2 * XX, const tensor2 & UU, const tensor2 & WW, int N, int K, int R) {
    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    /*
    for (int j=0; j<N; ++j)
        for (int r=0; r<R; ++r) {
            double t2=0;
            for (int i=0; i<N; ++i)
                for (int k=0; k<K; ++k) {
                    t2 += XX[k](i,j)*UU(r,i)*WW(r,k);

            TT(r,j) = t2;
        }
    }
    */

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int j=0; j<N; ++j) {
            double t2=0;
            for (int k=0; k<K; ++k) {
                double s2=0;
                for (int i=0; i<N; ++i)
                    s2 += XX[k](i,j)*UU(r,i);
                t2+=s2*WW(r,k);
            }

            TT(r,j) = t2;
        }
    }

}

void ContractRD (tensor2 & TT, const tensor2 * ZZ, const tensor2 & XX, int N, int K, int R) {
    // this could be build in N^3 the same as S,
    //as long as we keep track of which basis belongs to what
    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int k=0; k<K; ++k) {
            double t2[4]={0,0,0,0};
            for (int i=0; i<N; ++i) {
                double xv=0;
                for (int j=0; j<N; ++j)
                    xv += ZZ[k](i,j)*XX(4*r,j); // because of symmetry, it don't matter
                for (int h=0; h<4;++h)
                    t2[h]+=XX(4*r+h,i)*xv;
            }

            TT(4*r  ,k) =   t2[0];
            TT(4*r+1,k) = 2*t2[1];
            TT(4*r+2,k) = 2*t2[2];
            TT(4*r+3,k) = 2*t2[3];
        }
    }
}

void BuildS2 (tensor2 & SS, double * ss, const tensor2 & UU, const tensor2 & VV, int N, int M, int R) {

    //void EV (tensor2 &, double *); // defined later

    // generate matrix S
    for (int r=0; r<R; ++r) {
        for (int s=0; s<R; ++s) {
            double u2=0;
            for (int n=0; n<N; ++n) u2 += UU(r,n)*UU(s,n);
            double v2=0;
            for (int m=0; m<M; ++m) v2 += VV(r,m)*VV(s,m);
            SS(r,s) = u2*v2;
        }
    }

    //find clusters
    /*
    cout << "SS clusters: " << endl;
    for (int r=0; r<R; ++r) {
        cout << r << " : ";
        for (int s=0; s<R; ++s) {
            if (-SS(r,s)>0.9) cout << s << " ";
        }
        cout << endl;
    }
    cout << endl;
    */


    /*
    cout << "SS sum: " << endl;

    for (int r=0; r<R; ++r) {
        double s2=0;
        for (int s=0; s<R; ++s) s2+=SS(r,s);
        //cout << -SS(r,r) << " "; cout << endl;
        cout << -s2 << " ";
    }
    cout << endl;
    cout << "SS diag: " << endl;
    for (int r=0; r<R; ++r) cout << -SS(r,r) << " "; cout << endl;

    cout << "SS ready" << endl;
    */

    // invert S2
    if (0) {
        int *IPIV = new int[R+1];
        int LWORK = R*R;
        double *WORK = new double[LWORK];
        int INFO;

        dgetrf_(&R,&R,SS.c2,&R,IPIV,&INFO);
        cout << "dgetrf ready" << endl;
        dgetri_(&R,SS.c2,&R,IPIV,WORK,&LWORK,&INFO);
        cout << "dgetri ready" << endl;

        delete IPIV;
        delete WORK;
    }

    //double ss[R];
    EV(SS,ss);
    //for (int r=0; r<R; ++r) ss[r] = ss[r];

    /*
    for (int r=0; r<R; ++r) ss[r] = 1./sqrt(ss[r]);

    for (int r=0; r<R; ++r)
        for (int s=0; s<R; ++s)
            SS(r,s)*=ss[r];
    */
}

// build S2 from the Xr basis and its spatial derivatives
void BuildS2RD (tensor2 & SS, double * ss, const tensor2 & XX, int N, int M, int R) {

    // generate matrix S
    for (int r=0; r<R; ++r) {
        for (int s=0; s<R; ++s) {
            double s2[4][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};

            for (int i=0; i<4; ++i)
                for (int j=0; j<4; ++j)
                    for (int n=0; n<N; ++n)
                        s2[i][j] += XX(4*r+i,n)*XX(4*s+j,n);

            SS(4*r,4*s) = -s2[0][0]*s2[0][0];
            for (int i=1; i<4; ++i)
                SS(4*r+i,4*s) = -2*s2[0][0]*s2[i][0];
            for (int j=1; j<4; ++j)
                SS(4*r,4*s+j) = -2*s2[0][0]*s2[0][j];
            for (int i=1; i<4; ++i)
                for (int j=1; j<4; ++j)
                    SS(4*r+i,4*s+j) = -2*(s2[0][0]*s2[i][j]+s2[0][j]*s2[i][0]);

        }
    }

    cout << "SS ready" << endl;

    //void EV (tensor2 &, double *); // defined later
    EV(SS,ss);
    for (int r=0; r<4*R; ++r) ss[r] = -ss[r];
}


// the idea is to project on Xij, which should be a complete space for NxN within the problem
void BuildS2W (tensor2 & SS, double * ss, const tensor3 & X, const tensor2 & UU, const tensor2 & VV, int N, int M, int R) {

    tensor2 P(R,M);

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int n=0; n<M; ++n) {
            double uvx=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    uvx += UU(r,i)*VV(r,j)*X(n,i,j); // this will have at most rank n
            P(r,n)=uvx;
        }
    }

    #pragma omp parallel for
    // generate matrix S
    for (int r=0; r<R; ++r) {
        for (int s=0; s<R; ++s) {
            double p2=0;
            for (int n=0; n<M; ++n) p2 += P(r,n)*P(s,n);
            SS(r,s) = -p2;
        }
    }



    cout << "SS sum: " << endl;

    for (int r=0; r<R; ++r) {
        double s2=0;
        for (int s=0; s<R; ++s) s2+=SS(r,s);
        //cout << -SS(r,r) << " "; cout << endl;
        cout << -s2 << " ";
    }
    cout << endl;
    cout << "SS diag: " << endl;
    for (int r=0; r<R; ++r) cout << -SS(r,r) << " "; cout << endl;

    cout << "SS ready" << endl;

    // invert S2
    if (0) {
        int *IPIV = new int[R+1];
        int LWORK = R*R;
        double *WORK = new double[LWORK];
        int INFO;

        dgetrf_(&R,&R,SS.c2,&R,IPIV,&INFO);
        cout << "dgetrf ready" << endl;
        dgetri_(&R,SS.c2,&R,IPIV,WORK,&LWORK,&INFO);
        cout << "dgetri ready" << endl;

        delete IPIV;
        delete WORK;
    }

    //void EV (tensor2 &, double *); // defined later
    EV(SS,ss);
    for (int r=0; r<R; ++r) ss[r] = -ss[r];
}


void SolveALS (double * nn, tensor2 & TT, const tensor2 & SS, const double * ss, int K, int R) {
    /*
    // solve the systems
    #pragma omp parallel for
    for (int k=0; k<K; ++k) {
        double DD[R];

        for (int s=0; s<R; ++s)
            DD[s] = TT(s,k);

        for (int r=0; r<R; ++r) {
            double dd=0;
            for (int s=0; s<R; ++s) {
                dd+= SS(r,s)*DD[s];
            }
            TT(r,k) = dd;
        }


        for (int s=0; s<R; ++s)
            DD[s] = TT(s,k);

        for (int r=0; r<R; ++r) {
            double dd=0;
            for (int s=0; s<R; ++s) {
                dd+= SS(s,r)*DD[s];
            }
            TT(r,k) = dd;
        }
    }*/

    int S;
    for (S=0; S<R;++S)
        if (ss[S]<=01.e-16 * ss[0]) break;
    cout << "using " << S << " eigenvecs out of " << R << endl;

    #pragma omp parallel for
    for (int k=0; k<K; ++k) {
        double DD[S];

        for (int r=0; r<S; ++r) {
            double dd=0;
            for (int s=0; s<R; ++s) {
                dd+= SS(r,s)*TT(s,k);
            }
            DD[r] = dd;
        }

        for (int s=0; s<S; ++s)
            DD[s] /= ss[s];

        for (int r=0; r<R; ++r) {
            double dd=0;
            for (int s=0; s<S; ++s) {
                dd+= SS(s,r)*DD[s];
            }
            TT(r,k) = dd;
        }
    }


    // normalize
    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        double n2=0;
        for (int k=0; k<K; ++k) n2 += TT(r,k)*TT(r,k);
        n2 = sqrt(n2);
        nn[r] = n2;
        n2 = 1./n2;
        for (int k=0; k<K; ++k) TT(r,k)*=n2;
    }

}




void SolveALSW (double * nn, tensor2 & TT, const tensor2 & SS, const double * ss, int K, int R) {

/*

    tensor2 P(R,M);

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int n=0; n<M; ++n) {
            double uvx=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    uvx += UU(r,i)*VV(r,j)*X(n,i,j);
            P(r,n)=uvx;
        }
    }



    int S;
    for (S=0; S<R;++S)
        if (ss[S]<=01.e-16 * ss[0]) break;
    cout << "using " << S << " eigenvecs out of " << R << endl;

    #pragma omp parallel for
    for (int k=0; k<K; ++k) {
        double DD[S];

        for (int r=0; r<S; ++r) {
            double dd=0;
            for (int s=0; s<R; ++s) {
                dd+= SS(r,s)*TT(s,k);
            }
            DD[r] = dd;
        }

        for (int s=0; s<S; ++s)
            DD[s] /= ss[s];

        for (int r=0; r<R; ++r) {
            double dd=0;
            for (int s=0; s<S; ++s) {
                dd+= SS(s,r)*DD[s];
            }
            TT(r,k) = dd;
        }
    }


    // normalize
    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        double n2=0;
        for (int k=0; k<K; ++k) n2 += TT(r,k)*TT(r,k);
        n2 = sqrt(n2);
        nn[r] = n2;
        n2 = 1./n2;
        for (int k=0; k<K; ++k) TT(r,k)*=n2;
    }
*/
}




double CheckNorm(const tensor2 * KK, const double * nn, const tensor2 & UU, const tensor2 & VV, const tensor2 & WW, int N, int K, int R) {

    // check residual
    double r2 = 0;
    double x2 = 0;

    #pragma omp parallel for
    for (int k=0; k<K; ++k) {
        tensor2 T(N);
        T = KK[k];

        double rr=0, xx=0;

        for (int i=0; i<N; ++i) {
            for (int j=0; j<i; ++j) {
                xx+=T(i,j)*T(i,j);
            }
            xx+=0.5*T(i,i)*T(i,i);
        }

        for (int r=0; r<R; ++r) {
            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {
                    T(i,j) -= (nn[r]*WW(r,k))*UU(r,i)*VV(r,j);
                }
            }
        }

        for (int i=0; i<N; ++i) {
            for (int j=0; j<i; ++j) {
                rr+=T(i,j)*T(i,j);
            }
            rr+=0.5*T(i,i)*T(i,i);
        }

        #pragma omp atomic
        x2+=xx;

        #pragma omp atomic
        r2+=rr;

        /*
        for (int i=0; i<N; ++i) {
            for (int j=0; j<i; ++j) {
                double xx=KK[k](i,j);
                x2 += xx*xx;
                for (int r=0; r<R; ++r) xx -= nn[r] * WW(r,k) * UU(r,i) * VV(r,j);
                r2 += xx*xx;
            }
        }
        */
    }

    x2*=2;
    r2*=2;

    cout << "|X|^2 = " << x2 << endl;
    cout << "|R|^2 = " << r2 << endl;

    return r2/x2;

}

void CheckNorm(const tensor2 * KK, const tensor2 & RR, const tensor2 & LL, const double * ll,  const double * nn, const tensor2 & UU, const tensor2 & VV, const tensor2 & WW, int N, int K, int R) {

    // compute Frobenius for the original tensor
    double x2 = 0;
    #pragma omp parallel for
    for (int k=0; k<K; ++k) {
        double ss=0;
        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j) {
                double xx=KK[k](i,j);
                ss += xx*xx;
            }
        #pragma omp atomic
        x2+=ss;
    }

    cout << "|X|^2 = " << x2 << endl;

    // compute Frobenius for the decomposed tensor
    double r2 = 0;
        // generate matrix S
    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        double s2=0;
        for (int s=0; s<R; ++s) {
            double u2=0;
            for (int n=0; n<N; ++n) u2 += UU(r,n)*UU(s,n);
            double v2=0;
            for (int m=0; m<N; ++m) v2 += VV(r,m)*VV(s,m);
            double w2=0;
            for (int m=0; m<K; ++m) w2 += WW(r,m)*WW(s,m);
            s2+= nn[s]*u2*v2*w2;
        }
        #pragma omp atomic
        r2+= nn[r]*s2;
    }

    cout << "|R|^2 = " << r2 << endl;

    // compute Frobenius cross-product
    double rx = 0;

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        double sx=0;
        for (int s=0; s<N*K; ++s) {
            if (fabs(ll[s])<PRPR) continue;
            double u2=0;
            for (int n=0; n<N; ++n) u2 += UU(r,n)*LL(s,n);
            double v2=0;
            for (int m=0; m<N; ++m) v2 += VV(r,m)*LL(s,m);
            double w2=0;
            for (int m=0; m<K; ++m) w2 += WW(r,m)*RR(s,m);
            sx+= ll[s]*u2*v2*w2;
        }
        #pragma omp atomic
        rx+= nn[r]*sx;
    }

    cout << "|AR|^2 = " << x2+r2-2*rx << endl;
}


void CheckNormRD(const tensor2 * KK, const double * nn, const tensor2 & XX, const tensor2 & WW, int N, int K, int R) {

    // check residual
    double r2 = 0;
    double x2 = 0;

    #pragma omp parallel for
    for (int k=0; k<K; ++k) {
        tensor2 T(N);
        T = KK[k];

        double rr=0, xx=0;

        for (int i=0; i<N; ++i) {
            for (int j=0; j<N; ++j) {
                xx+=T(i,j)*T(i,j);
            }
            //xx+=0.5*T(i,i)*T(i,i);
        }

        for (int r=0; r<R; ++r) {
            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    T(i,j) -= (nn[4*r  ]*WW(4*r,  k)) * XX(4*r,i)*XX(4*r,j);
                    for (int h=1; h<4;++h)
                        T(i,j) -= (nn[4*r+h]*WW(4*r+h,k)) * (XX(4*r,i)*XX(4*r+h,j)+XX(4*r+h,i)*XX(4*r,j));
                }
            }
        }

        for (int i=0; i<N; ++i) {
            for (int j=0; j<N; ++j) {
                rr+=T(i,j)*T(i,j);
            }
            //rr+=0.5*T(i,i)*T(i,i);
        }

        #pragma omp atomic
        x2+=xx;

        #pragma omp atomic
        r2+=rr;
    }

    //x2*=2;
    //r2*=2;

    cout << "|X|^2 = " << x2 << endl;
    cout << "|R|^2 = " << r2 << endl;

}


void CheckNormW(const tensor3 & X, const double * vv, const tensor2 & W,   const double * nn, const tensor2 & XX, const tensor2 & WW, int N, int M, int R) {


    tensor2 P(R,M);

    #pragma omp parallel for
    for (int r=0; r<R; ++r) {
        for (int n=0; n<M; ++n) {
            double uvx=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    uvx += XX(r,i)*XX(r,j)*X(n,i,j);
            P(r,n)=uvx/vv[n];
        }
    }

    double ss=0;
    tensor2 Q(M,M);
    for (int n=0; n<M; ++n) {
        for (int k=0; k<M; ++k) {
            double ww=0;
            for (int r=0; r<R; ++r)
                ww+=WW(r,k)*P(r,n);
            //ss+=ww*ww;
            Q(k,n) = ww;
         }
    }

    tensor2 II(M,M);
    for (int k=0; k<M; ++k) {
        for (int l=0; l<M; ++l) {
            for (int n=0; n<M; ++n)
                ss+=Q(k,n)*Q(l,n);
            II(k,l)=ss;
        }
    }

    cout << "resolution: " << endl << II << endl;



    //cout << "|X X^-1| = " << ss << endl;
}




void TMT (const tensor2 & T, tensor2 & M) {

    const int N = M.n;
    tensor2 H(N);

    // transform i
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            double ww=0;
            for (int k=0; k<N; ++k) ww+=T(i,k)*M(k,j);
            H(i,j) = ww;
        }
    }

    // transform j
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            double ww=0;
            for (int k=0; k<N; ++k) ww+=T(j,k)*H(i,k);
            M(i,j) = ww;
        }
    }

}


void pause() {cout << "paused" << endl; char a; cin >> a;}

// we need to at least have 2x (8x?) as many R-points than K functions (Nyquist frequency)
// to be able to reproduce the eigendensities exactly w/o aliasing / bandlimiting artifacts
// https://en.wikipedia.org/wiki/Nyquist_rate

// als, for gods sake, impose Q+D conditions



double Distance2 (const point & P, const point & Q) {
    double x = P.x - Q.x;
    double y = P.y - Q.y;
    double z = P.z - Q.z;

    return (x*x+y*y+z*z);
}

double MinDistance2(const point * grid, int N, const point & P) {
    const double R2max=1e24;
    double r2 = R2max;
    for (int i=0; i<N; ++i) r2=min(r2, Distance2(grid[i], P));
    return r2;
}

inline double Igamma(double K, const point & P, const point & Q) {

    double x = P.x - Q.x;
    double y = P.y - Q.y;
    double z = P.z - Q.z;

    double r2 = (x*x+y*y+z*z);

    return IncompleteGamma.calcF2(K*r2); // PS.c2[i]*PS.c2[j] // the difference is not worth the effort
}

inline point MidPoint(const point & P, const point & Q) {
    point R;
    R.x = 0.5*(P.x + Q.x);
    R.y = 0.5*(P.y + Q.y);
    R.z = 0.5*(P.z + Q.z);
    return R;
}

inline double ERI (int i, int j, int k, int l,  const point * centers, const tensor2 & SS, double KK) {

    point P,Q;

    P = MidPoint(centers[i],centers[j]);
    Q = MidPoint(centers[k],centers[l]);

    return SS(i,j)*SS(k,l)*Igamma(KK,P,Q);
}

// this is missing a constant (the result of the last 1-center integral)
inline double S4int (int i, int j, int k, int l,  const point * centers, const tensor2 & SS, double kw) {

    point P,Q;

    P = MidPoint(centers[i],centers[j]);
    Q = MidPoint(centers[k],centers[l]);

    // ( Pi/(4k) ) ^ (3/2)
    return SS(i,j)*SS(k,l)*exp(-kw*Distance2(P,Q));
}


// the goal is to eliminate the need for LOCAL
// variables in the innermost quajects during execution

class Quaject {
    //uint8_t vconst, vinout, vloc; // const vars going in, vars going out and local

  public:
    bool      operator()(const double  * vars) const; // attempt to evaluate the function
    Quaject * operator()(const Quaject * vars) const; // attempt to specialize the function
};

class Qdouble : public Quaject {
    uint16_t op;
    double val;
};

class Qbranch : public Quaject {
    uint16_t op;
    Quaject * qfalse;
    Quaject * qtrue;
};

// three-direction code (as simple as can be)
// vconst = 2; vout = 1; vloc = 0;
class Q3dir : public Quaject {
    uint16_t ret;
    uint16_t op1;
    uint16_t op2;
    uint8_t OPCODE;
    uint8_t pad[1];
};

// this may call a func, in which case the vars need to be remapped
class Qstatement : public Quaject {
    uint8_t  nin, nout;
    Quaject * qnext;
};

// a block of code w/o branching
class Qblock : public Quaject {
    uint16_t nloc;
    uint8_t  nin, nout; // in variables are constant; out variables may be used for input purposes (inout)
    int Ninstruct;
    Q3dir * qroot;    // first
};

// a function, starting with a root node
class Qfunc : public Quaject {
    uint16_t nloc;
    uint8_t  nin, nout; // in variables are constant; out variables may be used for input purposes (inout)
    Quaject * qroot;    // first


};



// keep a multiscale representation of the function
// i.e. use redundant representations in which quajects
// are expanded recursively at different degrees
// this may expose more patterns to the optimizer
void Parse() {

    // iterate over statements
    {
        // first identify tokens
        // identify and count variables in the statement
        // translate each variable with its ID from the function dictionary

        // ex.
        string chain = "a=3*b+h-5";

        // first identify tokens
        // a = 3 * b + h - 5

        // identify and count variables in the statement
        // out: a    in: b,h   const: 3,5

        // translate each variable with its ID from the function dictionary

    }

}


// use spectral clustering to separate possible clusters in v
void ClusterStuff (const point * v, int N, int K) {

    //const double mu = 4.; // try to make it "hard", so that only similar distances reach a value close to 1
    const double mu4 = 0.06125;

    // generate weight matrix
    tensor2 S(N);
    double  s[N];

    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            // sum and difference vectors
            point xp, xm;
            xp.x = v[i].x+v[j].x;
            xp.y = v[i].y+v[j].y;
            xp.z = v[i].z+v[j].z;
            xm.x = v[i].x-v[j].x;
            xm.y = v[i].y-v[j].y;
            xm.z = v[i].z-v[j].z;

            double rp, rm;
            rp = xp.x*xp.x + xp.y*xp.y + xp.z*xp.z;
            rm = xm.x*xm.x + xm.y*xm.y + xm.z*xm.z;

            //S(i,j) = exp(-mu*rp) + exp(-mu*rm);


            double r2i = v[i].x*v[i].x + v[i].y*v[i].y + v[i].z*v[i].z;
            double r2j = v[j].x*v[j].x + v[j].y*v[j].y + v[j].z*v[j].z;
            double rij = v[i].x*v[j].x + v[i].y*v[j].y + v[i].z*v[j].z;

            S(i,j) = exp(-mu4*(r2i*r2i+r2j*r2j-2*rij*rij));
        }
    }

    // scale
    //for (int i=0; i<N; ++i) s[i] = 1./sqrt(S(i,i));
    for (int i=0; i<N; ++i) {
        double ss=0;
        for (int j=0; j<N; ++j)
            ss+=S(i,j);
        s[i] = 1./sqrt(ss);
    }


    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            S(i,j) *= -s[i]*s[j];
        }
        S(i,i)+=1;
    }

    // diagonalize
    EV(S,s);

    //K=5;

    // cluster the values
    for (int i=0; i<K; ++i) cout << -s[i] << " "; cout << endl;

    for (int i=0; i<N; ++i) {
        cout << i << " : ";
        for (int k=0; k<K; ++k) {
            cout << S(i,k) << " ";
        }
        cout << endl;
    }


    // this is ONE way of doing this
    // we can also start pairing similar spaces by similarity and build a recursive tree

    // in any case, we need to store the sum of outer prods, because the sign becomes nonsense otherwise

    tensor2 CS(N,K);
    CS.zeroize();

    tensor2 CS2[N];

    int NE[N];
    for (int i=0; i<N; ++i) NE[i]=0;

    int NK=0;
    for (int i=0; i<N; ++i) {

        double u2=0;
        for (int k=0; k<K; ++k)
            u2+=S(i,k)*S(i,k);

        double ss=0;
        int    jj=-1;


        for (int j=0; j<NK; ++j) {
            double v2=0;
            for (int k=0; k<K; ++k)
                v2+=CS(j,k)*CS(j,k);
            double uv=0;
            for (int k=0; k<K; ++k)
                uv+=S(i,k)*CS(j,k);
            double rr = (uv*uv)/(u2*v2);

            // best fit so far
            if (rr>ss) {
                ss=rr;
                jj = j;
            }
        }

        // add to cluster
        if (ss>0.25) {
            for (int k=0; k<K; ++k)
                for (int l=0; l<K; ++l)
                    CS2[jj](k,l) += S(i,k)*S(i,l);

            ++NE[jj];
            //for (int k=0; k<K; ++k)
            //    CS(jj,k)+=S(i,k);
        }
        // new cluster
        else {
            CS2[NK].setsize(K);
            for (int k=0; k<K; ++k)
                for (int l=0; l<K; ++l)
                    CS2[NK](k,l) = S(i,k)*S(i,l);

            //for (int k=0; k<K; ++k)
            //    CS(NK,k)=S(i,k);
            NE[NK] =1;
            jj=NK;
            ++NK;
        }

        // now find best rank-1 approximation
        tensor2 T(K);
        double t[K];
        T = CS2[jj];
        T*=-1;
        EV(T,t);

        // copy first eigenvector
        for (int k=0; k<K; ++k)
            CS(jj,k) = CS2[jj](0,k);

    }

    for (int j=0; j<NK; ++j) {
        cout << "cluster " << j << "  :: " << NE[j] << endl;
        for (int k=0; k<K; ++k) cout << " " << CS(j,k); cout << endl;
    }

}


// cut the graph in two
int SpectralCut (point * C, int N, double R2) {

    if (N==0) return 0;
    if (N==1) return 1;

    const int M = 10; // number of eigenvecs


    tensor2 S(N);
    double  s[N];

    const double Kw = (0.5/R2)*4;

    // create similarity matrix
    {
        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                double r2 = Distance2(C[n], C[m]);
                S(n,m) = exp(-Kw*r2);
            }
        }
        // normalize rows/columns (stochastic)
        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            double s2=0;
            for (int k=0; k<N; ++k) s2+=S(n,k);
            s[n] = 1./sqrt(s2);
        }

        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                S(n,m) *= s[n]*s[m];
            }
        }
    }

    // obtain the M largest eigenpairs
    tensor2 V(N,M);
    double v[M];

    int M2 = EVX (S, s, V);

    for (int n=0; n<M2; ++n) v[n] = s[n]; // copy

    cout.precision(16);
    for (int n=0; n<M2; ++n) cout  << v[n] << ", " ; cout << endl;


    // create 2^3 clusters
    int npoints[2][2][2] = {0,0,0,0,0,0,0,0};
    int * np3 = &(npoints[0][0][0]);
    double cuts[3];

    for (int k=0; k<3; ++k) {
        cuts[k] = 0;
        for (int n=0; n<N; ++n) cuts[k]+=V(n, 1+k);
        cuts[k]/=double(N);
    }

    for (int n=0; n<N; ++n) {
        int i,j,k;
        i = (V(n,1)<cuts[0])?0:1;
        j = (V(n,2)<cuts[1])?0:1;
        k = (V(n,3)<cuts[2])?0:1;

        const int ijk = 4*i+2*j+k;

        ++np3[ijk]; // increment
    }

    cout << "cuts: " << cuts[0] << " " << cuts[1] << " " << cuts[2] << endl;
    cout << endl;

    for (int k=0; k<8; ++k) cout << k << " " << np3[k] << endl;
    pause();


    // store the index set of each cluster
    int * idx = new int[N];
    int * NC[8]; {
        NC[0] = idx;
        for (int k=1; k<8; ++k)
            NC[k] = NC[k-1] + np3[k-1];
    }

    for (int k=0; k<8; ++k) np3[k]=0;

    for (int n=0; n<N; ++n) {
        int i,j,k;
        i = (V(n,1)<cuts[0])?0:1;
        j = (V(n,2)<cuts[1])?0:1;
        k = (V(n,3)<cuts[2])?0:1;

        const int ijk = 4*i+2*j+k;
        const int m   = np3[ijk];

        NC[ijk][m] = n; // store the index

        ++np3[ijk]; // increment
    }


    // total dissimilitude tensor
    // ==========================

    // has to be regenerated
    // create similarity matrix
    {
        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                double r2 = Distance2(C[n], C[m]);
                S(n,m) = exp(-Kw*r2);
            }
        }
        // normalize rows/columns (stochastic)
        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            double s2=0;
            for (int k=0; k<N; ++k) s2+=S(n,k);
            s[n] = 1./sqrt(s2);
        }

        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                S(n,m) *= s[n]*s[m];
            }
        }
    }

    tensor2 D2(8);

    for (int p=0; p<8; ++p) {
        for (int q=0; q<8; ++q) {
            double sum=0;
            const int KP = np3[p];
            const int KQ = np3[q];

            for (int i=0; i<KP; ++i) {
                for (int j=0; j<KQ; ++j) {
                    const int n = NC[p][i];
                    const int m = NC[q][j];

                    sum += S(n,m);
                }
            }

            D2(p,q) = sum;
        }
    }

    cout << D2 << endl << endl;

    for (int p=0; p<8; ++p) {
        for (int q=0; q<8; ++q) {
            const int KP = np3[p];
            const int KQ = np3[q];

            D2(p,q) /= sqrt(double(KP*KQ));
        }
    }

    cout << D2 << endl << endl;

    pause();



    /*
    for (int k=1; k<M2; ++k) {

        // copy the second eigenvector to s for sorting
        for (int n=0; n<N; ++n) s[n] = V(n, k);
        FlashSort<double,point> (s, C, N);
        for (int n=0; n<N; ++n) cout  << s[n] << ", " ; cout << endl;

        // count >0 and <0
        double cut; {
            cut = 0;
            for (int n=0; n<N; ++n) cut+=s[n];
            cut/=double(N);
        }

        int nn; {
            nn=0;
            for (int n=0; n<N; ++n) if (s[n]<cut) ++nn;
        }

        cout << "evec " << k << " : " << v[k] << "     cut = " << cut << " ;;  " << N << " = " << nn << " + " << N-nn << endl;

        pause();
    }
    */

}


#include <vector>

struct Centroid {
    point  C;
    double r2;

    void Set(const vector<point> & points) {

        const int N = points.size();
        if (N==0) return;

        C.x=C.y=C.z=0;

        for (int n=0; n<N; ++n) {
            C.x += points[n].x;
            C.y += points[n].y;
            C.z += points[n].x;
        }

        double iN = 1./double(N);
        C.x*=iN;
        C.y*=iN;
        C.z*=iN;

        r2=0;

        //for (it=points.begin(); it!=points.end(); ++it) {
        for (int n=0; n<N; ++n) {
            r2=max(r2,Distance2(points[n],C));
        }

    }
};

struct Cluster {
    Centroid C;
    vector<point> keys;

    void Center() {
        C.Set(keys);
    }

    void Add(const point & P) {
        keys.push_back(P);
    }

    int Size() const {
        return keys.size();
    }

    int SpectralCut(Cluster * D) const {

        const int N = Size();

        if (N==0) return 0;
        if (N==1) {
            D[0].Add(keys[0]);
            D[0].Center();
            return 1;
        }
        if (N==2) {
            D[0].Add(keys[0]);
            D[0].Center();
            return 1;
        }


        tensor2 S(N);
        double  s[N];

        const double Kw = 1./C.r2;

        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                double r2 = Distance2(keys[n], keys[m]);
                S(n,m) = exp(-Kw*r2);
            }
        }
        // normalize rows/columns (stochastic)
        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            double s2=0;
            for (int k=0; k<N; ++k) s2+=S(n,k);
            s[n] = 1./sqrt(s2);
        }

        #pragma omp parallel for
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                S(n,m) *= s[n]*s[m];
            }
        }

        const int K = 3;

        tensor2 V(N,K);
        EVX(S,s,V);

        for (int n=0; n<K; ++n) cout  << s[n] << ", " ;
        cout << endl;

        // use the second ev to cut
        double cut=0;
        for (int n=0; n<N; ++n) cut+=V(n,1);
        cut/=double(N);


        for (int n=0; n<N; ++n) {
            if (V(n,1)>cut) D[0].Add(keys[n]);
            else            D[1].Add(keys[n]);
        }

        D[0].Center();
        D[1].Center();

        return 2;
    }
};

// maybe check for individual elements
double Distance2 (const Cluster & C, const point & Q) {
    double x = C.C.C.x - Q.x;
    double y = C.C.C.y - Q.y;
    double z = C.C.C.z - Q.z;

    return (x*x+y*y+z*z); // - C.C.r2;
}

int Nearest(const Cluster * grid, int N, const point & P) {
    int k=0;
    for (int i=0; i<N; ++i) {
        if (Distance2(grid[i], P) < Distance2(grid[k], P)) k=i;
    }
    return k;
}

/*
void ClusterSet(const Cluster & C) {

    const int N = C.Size();
    if (N<2) return;

    tensor2 S(N);
    double  s[N];

    const double K = 1./C.C.r2;
    {
        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                double r2 = Distance2(C.keys[n], C.keys[m]);
                S(n,m) = exp(-K*r2);
            }
        }
        EV(S,s);

        for (int n=0; n<N; ++n) cout  << s[n] << ", " ;
        cout << endl;
    }
}
*/

void ClusterAll (const point * C, int N) {

    if (0) {
        const int M = N*(N+1)/2;
        point * G1 = new point[M];

        // copy original grid
        for (int i=0; i<N; ++i) G1[i] = C[i];

        // generate midpoints and add them to the grid
        int m=N;
        for (int i=0; i<N; ++i) {
            for (int j=0; j<i; ++j) {
                G1[m] = MidPoint(C[i], C[j]);
                ++m;
            }
        }

        const double R2 = (2.88*2.88*AU*AU);


        const int M1 = SpectralCut (G1, M, R2);
        const int M2 = M-M1;

        cout << "cut: " << M1 << " " << M2 << endl;
        pause();
    }

    else {

        Cluster * G = new Cluster[N];

        // first pass: add all points to closest center
        {
            for (int i=0; i<N; ++i) G[i].Add(C[i]);
            for (int i=0; i<N; ++i) G[i].Center();

            for (int i=0; i<N; ++i) {
                for (int j=0; j<i; ++j) {
                    point P;
                    P = MidPoint(C[i], C[j]);
                    int k = Nearest(G,N,P); // nearest cluster
                    G[k].Add(P);
                }
            }

            for (int i=0; i<N; ++i) G[i].Center();
        }

        while (1) {
            // now divide the clusters
            // using spectralcut
            Cluster * G2 = new Cluster[2*N];
            int M=0;

            for (int i=0; i<N; ++i) {
                cout << "cluster " << i << " :: " << G[i].Size() << " elements" << endl;
                M += G[i].SpectralCut(G2+M);
            }
            pause();

            for (int i=0; i<M; ++i) {
                cout << "cluster " << i << " :: " << G2[i].Size() << " elements" << endl;
            }
            pause();


            // now merge similar clusters (if applicable)
            // we need a better algorithm capable of merging multiple clusters
            // just make a distance matrix for the clusters

            tensor2 D2(M);
            for (int i=0; i<M; ++i)
                for (int j=0; j<M; ++j)
                    D2(i,j) = Distance2(G2[i].C.C, G2[j].C.C);



            int cmerge[M];
            for (int i=0; i<M; ++i) cmerge[i]=i;

            int MM=0;

            for (int i=0; i<M; ++i) {

                if (G2[i].Size()==0) cmerge[i]=-1; // empty Cluster; mark for deletion
                if (cmerge[i]==-1) continue;       // don't merge the same pair twice

                D2(i,i) = 1e24;
                int n=0;
                for (int j=0; j<M; ++j) if (D2(i,j)<D2(i,n)) n=j;
                D2(i,i) = 0;
                double R2 = D2(i,n);

                /*

                point P1,P2,Q;
                Q = G2[i].C.C;

                int n1 = Nearest(G2    ,   i,   Q);
                int n2 = Nearest(G2+i+1, M-i-1, Q) +i+1;

                //cout << "    " << 0 <<":" << i << " " << i+1 << ":" << M-i-1 << "    " << n1 << " "  << n2 << endl;

                if (i>0)   P1 = G2[n1].C.C;
                if (i<M-1) P2 = G2[n2].C.C;

                double r21=1e24, r22=1e24;
                if (i>0)   r21 = Distance2(P1,Q);
                if (i<M-1) r22 = Distance2(P2,Q);

                int n = (r22<r21)?n2:n1;
                double r2 = min(r22,r21);
                */

                const int I = G2[i].Size();
                const int J = G2[n].Size();
                double R2IJ = R2 * double(I*J) / double(I+J);

                double R = sqrt(R2)/(2.88*AU);
                cout << "cluster " << i << " :: " << n << "  R = " << R << "   D2IJ = " << R2IJ << "  Fij = "<<sqrt(R2IJ/M) << endl;

                if (R2IJ<4.0) {
                    cmerge[i]=n;
                    cmerge[n]=-1;
                }

                ++MM;
            }
            pause();

            delete[] G;


            G = new Cluster[MM];

            int mm=0;
            for (int m=0; m<M; ++m) {
                if (cmerge[m]==-1) continue;

                // first copy original
                const int N = G2[m].Size();
                for (int i=0; i<N; ++i) G[mm].Add(G2[m].keys[i]);

                // merge the other cluster
                if (cmerge[m]!=m) {
                    int n = cmerge[m];
                    const int N = G2[n].Size();
                    for (int i=0; i<N; ++i) G[mm].Add(G2[n].keys[i]);
                }

                G[mm].Center();

                ++mm;
            }

            N = mm;

            delete[] G2;
        }

    }

}


// this is terrible; let's reimplement the whole damn thing
void HalveGrid1 (const point * G0, const int N, point *& G1, int & M) {

    // list K-th closest neighbours to each point K=14 for Au (+ itself)
    const int K = 128;
    const double R2max=1e24; // an impossibly large value for testing distances against

    tensor2 R2(N,K+1);
    tensor2 I2(N,K+1);


    for (int i=0; i<N; ++i) {
        I2(i,0) = 0; // how many neighbours so far?
        int nn = 0;

        R2(i,0) = R2max;
        I2(i,0) = -1;

        for (int j=0; j<N; ++j) {
            double x = G0[i].x - G0[j].x;
            double y = G0[i].y - G0[j].y;
            double z = G0[i].z - G0[j].z;
            double r2 = (x*x+y*y+z*z);

            // anything goes
            //if (r2<R2(i,nn))
            {
                // push the item to the dummy slot
                R2(i,nn+1) = r2;
                I2(i,nn+1) = j;

                // now do selection sort;
                for (int k=nn; k>=0; --k)
                    if (R2(i,k)>R2(i,k+1)) {
                        swap(R2(i,k),R2(i,k+1));
                        swap(I2(i,k),I2(i,k+1));
                    }
                    else
                        break;

                // update number of elements
                if (nn<K) ++nn;
            }

        }
    }

    for (int i=0; i<N; ++i) {
        cout << i << "  ";
        for (int k=0; (I2(i,0)!=-1) && (k<K); ++k)
            cout << sqrt(R2(i,k))/(AU*2.88) << " ";
        cout << endl;
    }

    // cluster the typical distances
    // we typically need 3~5 clusters
    G1 = new point[(N*K+N)/2];
    int NK=0;

    /*
    for (int i=0; i<N; ++i) {
        for (int k=0; k<K; ++k) {
            int j=I2[i][k];
            if (j==-1) continue; // last element
            if (j>=i) continue; // self-point or duplicated
            G1[NK].x = G0[i].x - G0[j].x;
            G1[NK].y = G0[i].y - G0[j].y;
            G1[NK].z = G0[i].z - G0[j].z;
            ++NK;
        }
    }

    Cluster(G1, NK, 4); // K = number of clusters
    */

    double r2min,r2max;
    r2min=r2max=R2(0,1); // first neighbour not being itself
    for (int i=1; i<N; ++i) {
        r2min = min(r2min, R2(i,1));
        r2max = max(r2max, R2(i,1));
    }

    r2max = 1.7* r2min; // completely ad-hoc; the idea is that only closest neighbours should apply, but also second neighbours should be discarded, which usually begin at sqrt(3)
    r2min *= 0.125*0.125;   // anything closer to a quarter of what's considered the minimum distance can be thought of as the same point, really
    r2min *= 0.25*0.25;

    //cout << "r2min ; " <<r2min << endl;

    for (int k=0; k<K; ++k) {
        for (int i=0; i<N; ++i) {
            int j=I2(i,k);
            if (j==-1) continue; // last element
            if (j>i) continue; // self-point or duplicated
            //if (R2(i,k) > r2max) continue; // too far away

            G1[NK].x = 0.5*(G0[i].x + G0[j].x);
            G1[NK].y = 0.5*(G0[i].y + G0[j].y);
            G1[NK].z = 0.5*(G0[i].z + G0[j].z);

            //cout << MinDistance2(G1,NK, G1[NK]) << " ";

            if (MinDistance2(G1,NK, G1[NK]) < r2min) continue; // the point has been computed, but will be overwritten

            ++NK;
        }
        //cout << endl;
        //pause();
    }

    M = NK;


    // take each adjacent edge and generate a new grid point in between;
    // avoid duplicated & be careful of boundaries


}

void HalveGrid (const point * G0, const int N, point *& G1, int & M) {

    M = N*(N+1)/2;

    G1 = new point[M];
    double r2[M];

    // copy original grid
    for (int i=0; i<N; ++i) {
        G1[i] = G0[i];
        r2[i] = 0; // the original grid has 0 distance to itself
    }

    // generate midpoints and add them to the grid
    int m=N;
    for (int i=0; i<N; ++i) {
        for (int j=0; j<i; ++j) {
            G1[m] = MidPoint(G0[i], G0[j]);
            r2[m] = MinDistance2(G0,N, G1[m]);
            ++m;
        }
    }

    for (int i=N; i<M; ++i) {
        int k=i;
        for (int j=i+1; j<M; ++j) if (r2[j]>r2[k]) k=j;
        swap(G1[i],G1[k]);
        swap(r2[i],r2[k]);
        for (int j=i+1; j<M; ++j)
            r2[j]=min(r2[j],Distance2(G1[i],G1[j]));
    }

    pause();
    for (int i=0; i<M; ++i) {
        //if (i%20==0) cout << i << " :  ";
        cout << sqrt(r2[i])/(AU*2.88) << ", ";
        //if ((i+1)%20==0) cout << endl;
    }
    cout << endl;
    pause();

    /*
    // flashsort
    for (int i=N; i<M; ++i) r2[m]=-r2[m];  // use negative because FS sorts in ascending order
    FlashSort<double,point>(r2,G1+N, M-N); // sort only the midpoints
    for (int i=N; i<M; ++i) r2[m]=-r2[m];

    // for every point added, update the rest of the list
    bool mark[M];
    for (int i=0; i<M; ++i) mark[i] = false;

    for (int i=N; i<M; ++i) {
        // is this point reliable?
        if (mark[i]==true) {

        }
        else {
            // check which points have to be sorted again
            for (int j=i+1; j<M; ++j) {
                double r22 = Distance2(G1[i],G1[j]);
                if (r22<r2[j]) {
                    r2[j]=r22;
                    mark[j] = true;
                }
            }
        }
    }
    */


}



void EvalAux   (const point * G0, const int N, const point * G1, const int M, const double mu, tensor2 & Y) {

    // now evaluate the basis on all selected grid points
    #pragma omp parallel for
    for (int i=0; i<N; ++i) {
        for (int r=0; r<M; ++r) {
            double x = G0[i].x - G1[r].x;
            double y = G0[i].y - G1[r].y;
            double z = G0[i].z - G1[r].z;
            double r2 = (x*x+y*y+z*z);
            double g  = exp(-mu*r2);
            Y(i,r  ) = g; // don't normalize ?
        }
    }

    // calculate normalization

    // iterate over all site pairs; evaluate Gaussian Kernel
}


void OptimizeGrid (const int M, const int R, const tensor2 & RR) {
    // construct P and Q matrices from outer product of RR
    // matrix in the spanned and residual subspace

    // compute GEV

    // compute derivatives

    // line search optimal points

    // update grid points
}


void AuxGrid (const point * G0, const int N, double alpha,   point *& G1, int & M) {

    // start with the original grid

    // compute all pairs; find out which center is closer

    // if alpha Ar^2 < some quite small thresh, add func to the centre

    // otherwise, add centre to the new grid

    // iterate; figure out if mergers are worth the addition of new fnctions





}



struct vec3 {
    double x,y,z;
};

vec3 normal(const point & P, const point & Q) {
    vec3 N;
    N.x = Q.x-P.x;
    N.y = Q.y-P.y;
    N.z = Q.z-P.z;
    double n2=N.x*N.x+N.y*N.y+N.z*N.z;
    double in = 1./sqrt(n2);
    N.x*=in;
    N.y*=in;
    N.z*=in;
    return N;
}

vec3 cross(const vec3 & u, const vec3 & v) {
    vec3 w;
    w.z = u.x*v.y - u.y*v.x;
    w.x = u.y*v.z - u.z*v.y;
    w.y = u.z*v.x - u.x*v.z;
    return w;
}

double dot (const vec3 & u, const point & P) {
    return u.x*P.x + u.y*P.y + u.z*P.z;
}

point solve3x3 (const vec3 & u, const vec3 & v, const vec3 & w, double b1, double b2, double b3) {
    point P;

    return P;
}

struct Vvertex {
    point O;
};

struct Vedge {
    point O;
    vec3  N; // normal

    Vvertex * vertices[2];
    int Nvertices;
};

struct Vface {
    point O; // origin
    vec3  N; // normal

    Vedge * edges[16];
    int Nedges;
};

struct Vcell {
    point O; // origin
    Vface * faces[16];
    int Nfaces;
};

// origin, list of closest neighbours
void MakeCell (const point & O, const point * P, int N) {

    Vcell cell;
    cell.O = O;

    Vface faces[N]; // at most

    for (int n=0; n<N; ++n) {
        faces[n].O = MidPoint(O,P[n]);
        faces[n].N = normal(O,P[n]);
    }

    // take the planes in threes
    // =========================

    for (int n=0; n<N; ++n) {
        // compute intersections
        for (int m=0; m<n; ++m) {
            // find line equation
            vec3 w;
            point C;

            vec3 & u = faces[n].N;
            vec3 & v = faces[m].N;
            point & O1 = faces[n].O;
            point & O2 = faces[m].O;

            // the line equation is given by C + a*w
            w = cross(u,v);
            C = solve3x3(u,v,w, dot(u,O1),dot(v,O2),dot(w,O1));





        }

    }
};


void Quadrature(const point * centers, int N, double alpha) {

    //const int NQ=8;
    //double HQ[] = {0.2734810461381524521583, 0.8229514491446558925825, 1.380258539198880796372, 1.951787990916253977435, 2.546202157847481362159, 3.176999161979956026814, 3.869447904860122698719, 4.688738939305818364688};
    const int NQ=16;
    double HQ[] = {0.1948407415693993267087,0.584978765435932448467,0.9765004635896828384847,1.370376410952871838162,1.767654109463201604628,2.169499183606112173306,2.577249537732317454031,2.992490825002374206285,3.417167492818570735874,3.853755485471444643888,4.30554795335119844526,4.777164503502596393036,5.27555098651588012782,5.81222594951591383277,6.40949814926966041217,7.1258139098307275728};

    const int NL=26;
    const double u2=1./sqrt(2);
    const double u3=1./sqrt(3);

    point LQ[] = {
                  {1,0,0},{0,1,0},{0,0,1},
                  {u2,u2,0},{u2,-u2,0},{u2,0,u2},{u2,0,-u2}, {0,u2,u2},{0,u2,-u2},
                  {u3,u3,u3},{u3,u3,-u3},{u3,-u3,u3},{-u3,u3,u3}
                  };

    const int R=NQ*NL;


    // the hermite x's are actually frequencies
    // they must be multiplied times 2sqrt(alpha)
    const double w0 = 2*sqrt(alpha);

    // Lebedev 6-point seems simple enough, although 26 points is still quite simple;
    tensor2 X(N,R);


    for (int i=0; i<N; ++i) {
        for (int n=0; n<NQ; ++n) {
            // they should be complex exponentials for the transform;
            // however, since the Lebedev grids are symmetric wrt. inversion,
            // it's simpler to store the components separately
            double w = w0*HQ[n];
            for (int l=0; l<NL; l+=2) {
                double dot = LQ[l/2].x*centers[i].x + LQ[l/2].y*centers[i].y + LQ[l/2].z*centers[i].z;
                int r = NL*n+l;
                X(i,r  ) = cos(w*dot);
                X(i,r+1) = sin(w*dot);
            }
        }
    }

    // let's see how well can they decompose the typical thing.
    // for that we need the quadrature weights, which we must find

    tensor2 K(N);
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            K(i,j) = exp(-alpha*Distance2(centers[i],centers[j]));
        }
    }

    double w[R]; // weights

    // we do it with a single step of ALS
    {
        double t[R];

        for (int r=0; r<R; ++r) {
            double ss=0;
            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    ss+=X(i,r)*X(j,r)*K(i,j);
                }
            }
            t[r] = ss;
        }

        tensor2 S(R);

        for (int r=0; r<R; ++r) {
            for (int s=0; s<R; ++s) {
                double s2=0;
                for (int i=0; i<N; ++i) s2+=X(i,r)*X(i,s);
                S(r,s) = s2*s2;
            }
        }

        INV(S);

        for (int r=0; r<R; ++r) {
            double ww=0;
            for (int s=0; s<R; ++s) ww+=S(r,s)*t[s];
            w[r]=ww;
        }
    }

    cout << "weights: " << endl;
    for (int r=0; r<R; ++r) {
        cout << w[r] << "  ";
        if ((r+1)%NL==0) cout << endl;
    }
    cout << endl;
    pause();

    double k2=0;
    double e2=0;
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            double Kij=0;
            for (int r=0; r<R;++r) Kij+=w[r]*X(i,r)*X(j,r);
            cout << i << " " << j << " :  " << K(i,j) << " " << Kij << endl;
            Kij -= K(i,j);
            e2 += Kij*Kij;
            k2 += K(i,j)*K(i,j);
        }
        pause();
    }
    cout << "|K|^2 = " << k2 << endl;
    cout << "|A|^2 = " << e2 << endl;

}


double Overlap (const double * w1, const double * k1, int K1, int L1,
                const double * w2, const double * k2, int K2, int L2,
                const vec3 & AR, double R2) {

    int L12=L1+L2;

    /*

    for (int i=0; i<K1; ++i) {
        for (int j=0; j<K2; ++j) {
            double k12 = (k1[i]*k2[j])/(k1[i]+k2[j]);
            double II = exp(-k12*R2) * k12*sqrt(k12);
            for (ii=0; ii<L12; ++ii) II*=k12;
        }
    }
    */

}

void TestOverlaps(const point * centers, int N) {

    const double SS[] = {51355337.8000, 11629474.1000, 3252136.26000, 998349.722000,
                            331601.135000, 117200.254000, 43712.5627000, 17030.0473000,
                            6865.21544000, 2852.80422000, 1222.37730000, 536.362090000,
                            244.356338000, 115.700795000, 56.5098052000, 28.2525743000,
                            13.8174786000, 6.6571070200, 3.1274164000, 1.3295761100,
                            0.5150078800 , 0.2060031500, 0.0824012600, 0.0329605000};
    const double PP[] = {15536900.5000, 2743047.34000, 620746.976000, 163862.987000,
                            48746.5884000, 16094.1196000 , 5855.00631000, 2324.10281000,
                            991.471022000, 446.694052000, 209.958884000, 101.721194000,
                            49.2517408000, 24.6008091000, 12.1504196000, 5.8701713100,
                            2.7494301200, 1.1723005800, 0.4481452800, 0.1792581100, 0.0717032400};
    const double DD[] = {6865.21544000, 2852.80422000, 1222.37730000, 536.362090000,
                            244.356338000, 115.700795000, 56.5098052000, 28.2525743000,
                            13.8174786000, 6.6571070200, 3.1274164000, 1.3295761100,
                            0.5318304400, 0.2127321800, 0.0850928700};
    const double AU6S[] = {-0.00001172, -0.00001649, -0.00004265, -0.00008137,
                            -0.00016899, -0.00032914, -0.00065738, -0.00130094, -0.00259623,
                            -0.00472624, -0.00652372, -0.00014498, 0.02668614,
                            0.05146750, -0.03076784, -0.16803076, -0.00252018, 0.35422510,
                            0.05311836, -0.52884798, -0.25561699, 0.40226568, 0.58565103,
                            0.28390712};
    const double AU6P[] = {0.00000145, 0.00000404, 0.00001209, 0.00003475, 0.00010236,
                            0.00030906, 0.00093795, 0.00279296, 0.00751383, 0.01637917,
                            0.02106969, -0.00752819, -0.08453870, -0.06200140, 0.13559267,
                            0.23244567, -0.15873600, -0.55245312, 0.21108823, 0.69452405,
                            0.28554616};
    const double AU5D[] = {0.00007863, 0.00023136, 0.00125245, 0.00514949, 0.01841212,
                            0.04455252, 0.06464063, -0.00024011, -0.14529316, -0.19550859,
                            0.07406019, 0.44532784, 0.44750820, 0.22142067, 0.05503681};
    const int KS = 24;
    const int KP = 21;
    const int KD = 15;

    double N6S[KS], N6P[KP], N5D[KD];

    for (int k=0; k<KS; ++k) N6S[k] = AU6S[k] * pow(SS[k],0.75) * pow(2./PI,0.75);
    for (int k=0; k<KP; ++k) N6P[k] = AU6P[k] * pow(PP[k],1.25) * pow(2./PI,0.75) * 2;
    for (int k=0; k<KD; ++k) N5D[k] = AU5D[k] * pow(DD[k],1.75) * pow(2./PI,0.75) * 4;

    tensor2 Ssp(4*N);

    for (int n=0; n<N; ++n) {
        for (int m=0; m<N; ++m) {
            double r2 = Distance2(centers[n],centers[m]);

        }
    }

}


void TestClustering(const point * centers, int N) {

    tensor2 S(N);
    double  s[N];

    for (int k=-15; k<16; ++k) {

        double K = pow(2,0.5*double(k));

        for (int n=0; n<N; ++n) {
            for (int m=0; m<N; ++m) {
                double r2 = Distance2(centers[n],centers[m]);
                S(n,m) = exp(-K*r2);
            }
        }
        //cout << K << ": ";
        EV(S,s);

        cout << log(K) << ": {";
        for (int n=0; n<N; ++n) cout  << log(s[n]) << ", " ;
        cout << endl;
    }


}

int main() {

    // coordinates for the gold NP in AU

    //point au[] = {{40.00000,  40.00000,  40.00000},{40.00000,  42.03148,  42.03148},{42.03148,  40.00000,  42.03148},{42.03148,  42.03148,  40.00000},{35.93839,  33.90112,  33.90112},{40.00000,  33.89970,  33.89970},{44.06161,  33.90112,  33.90112},{33.90112,  35.93839,  33.90112},{35.93646,  35.93646,  31.86571},{35.93678,  37.96931,  33.90408},{37.96931,  35.93678,  33.90408},{37.96930,  37.96930,  31.86646},{40.00000,  35.93251,  31.86554},{40.00000,  37.96886,  33.90434},{42.03069,  35.93678,  33.90408},{42.03070,  37.96930,  31.86646},{44.06354,  35.93646,  31.86571},{44.06322,  37.96931,  33.90408},{46.09888,  35.93839,  33.90112},{33.89970,  40.00000,  33.89970},{35.93251,  40.00000,  31.86554},{35.93678,  42.03069,  33.90408},{37.96886,  40.00000,  33.90434},{37.96930,  42.03070,  31.86646},{40.00000,  40.00000,  31.86619},{40.00000,  42.03114,  33.90434},{42.03114,  40.00000,  33.90434},{42.03070,  42.03070,  31.86646},{44.06749,  40.00000,  31.86554},{44.06322,  42.03069,  33.90408},{46.10030,  40.00000,  33.89970},{33.90112,  44.06161,  33.90112},{35.93646,  44.06354,  31.86571},{35.93839,  46.09888,  33.90112},{37.96931,  44.06322,  33.90408},{40.00000,  44.06749,  31.86554},{40.00000,  46.10030,  33.89970},{42.03069,  44.06322,  33.90408},{44.06354,  44.06354,  31.86571},{44.06161,  46.09888,  33.90112},{46.09888,  44.06161,  33.90112},{33.90112,  33.90112,  35.93839},{35.93646,  31.86571,  35.93646},{35.93678,  33.90408,  37.96931},{37.96930,  31.86646,  37.96930},{37.96931,  33.90408,  35.93678},{40.00000,  31.86554,  35.93251},{40.00000,  33.90434,  37.96886},{42.03070,  31.86646,  37.96930},{42.03069,  33.90408,  35.93678},{44.06354,  31.86571,  35.93646},{44.06322,  33.90408,  37.96931},{46.09888,  33.90112,  35.93839},{31.86571,  35.93646,  35.93646},{31.86646,  37.96930,  37.96930},{33.90408,  35.93678,  37.96931},{33.90408,  37.96931,  35.93678},{35.93692,  35.93692,  35.93692},{35.93692,  37.96861,  37.96861},{37.96861,  35.93692,  37.96861},{37.96861,  37.96861,  35.93692},{40.00000,  35.93697,  35.93697},{40.00000,  37.96852,  37.96852},{42.03139,  35.93692,  37.96861},{42.03139,  37.96861,  35.93692},{44.06308,  35.93692,  35.93692},{44.06308,  37.96861,  37.96861},{46.09592,  35.93678,  37.96931},{46.09592,  37.96931,  35.93678},{48.13429,  35.93646,  35.93646},{48.13354,  37.96930,  37.96930},{31.86554,  40.00000,  35.93251},{31.86646,  42.03070,  37.96930},{33.90434,  40.00000,  37.96886},{33.90408,  42.03069,  35.93678},{35.93697,  40.00000,  35.93697},{35.93692,  42.03139,  37.96861},{37.96852,  40.00000,  37.96852},{37.96861,  42.03139,  35.93692},{40.00000,  40.00000,  35.93687},{40.00000,  42.03148,  37.96852},{42.03148,  40.00000,  37.96852},{42.03139,  42.03139,  35.93692},{44.06303,  40.00000,  35.93697},{44.06308,  42.03139,  37.96861},{46.09566,  40.00000,  37.96886},{46.09592,  42.03069,  35.93678},{48.13446,  40.00000,  35.93251},{48.13354,  42.03070,  37.96930},{31.86571,  44.06354,  35.93646},{33.90408,  44.06322,  37.96931},{33.90112,  46.09888,  35.93839},{35.93692,  44.06308,  35.93692},{35.93678,  46.09592,  37.96931},{37.96861,  44.06308,  37.96861},{37.96931,  46.09592,  35.93678},{40.00000,  44.06303,  35.93697},{40.00000,  46.09566,  37.96886},{42.03139,  44.06308,  37.96861},{42.03069,  46.09592,  35.93678},{44.06308,  44.06308,  35.93692},{44.06322,  46.09592,  37.96931},{46.09592,  44.06322,  37.96931},{46.09888,  46.09888,  35.93839},{48.13429,  44.06354,  35.93646},{35.93646,  48.13429,  35.93646},{37.96930,  48.13354,  37.96930},{40.00000,  48.13446,  35.93251},{42.03070,  48.13354,  37.96930},{44.06354,  48.13429,  35.93646},{33.89970,  33.89970,  40.00000},{35.93251,  31.86554,  40.00000},{35.93678,  33.90408,  42.03069},{37.96930,  31.86646,  42.03070},{37.96886,  33.90434,  40.00000},{40.00000,  31.86619,  40.00000},{40.00000,  33.90434,  42.03114},{42.03070,  31.86646,  42.03070},{42.03114,  33.90434,  40.00000},{44.06749,  31.86554,  40.00000},{44.06322,  33.90408,  42.03069},{46.10030,  33.89970,  40.00000},{31.86554,  35.93251,  40.00000},{31.86646,  37.96930,  42.03070},{33.90408,  35.93678,  42.03069},{33.90434,  37.96886,  40.00000},{35.93697,  35.93697,  40.00000},{35.93692,  37.96861,  42.03139},{37.96861,  35.93692,  42.03139},{37.96852,  37.96852,  40.00000},{40.00000,  35.93687,  40.00000},{40.00000,  37.96852,  42.03148},{42.03139,  35.93692,  42.03139},{42.03148,  37.96852,  40.00000},{44.06303,  35.93697,  40.00000},{44.06308,  37.96861,  42.03139},{46.09592,  35.93678,  42.03069},{46.09566,  37.96886,  40.00000},{48.13446,  35.93251,  40.00000},{48.13354,  37.96930,  42.03070},{31.86619,  40.00000,  40.00000},{31.86646,  42.03070,  42.03070},{33.90434,  40.00000,  42.03114},{33.90434,  42.03114,  40.00000},{35.93687,  40.00000,  40.00000},{35.93692,  42.03139,  42.03139},{37.96852,  40.00000,  42.03148},{37.96852,  42.03148,  40.00000},{44.06313,  40.00000,  40.00000},{44.06308,  42.03139,  42.03139},{46.09566,  40.00000,  42.03114},{46.09566,  42.03114,  40.00000},{48.13381,  40.00000,  40.00000},{48.13354,  42.03070,  42.03070},{31.86554,  44.06749,  40.00000},{33.90408,  44.06322,  42.03069},{33.89970,  46.10030,  40.00000},{35.93697,  44.06303,  40.00000},{35.93678,  46.09592,  42.03069},{37.96861,  44.06308,  42.03139},{37.96886,  46.09566,  40.00000},{40.00000,  44.06313,  40.00000},{40.00000,  46.09566,  42.03114},{42.03139,  44.06308,  42.03139},{42.03114,  46.09566,  40.00000},{44.06303,  44.06303,  40.00000},{44.06322,  46.09592,  42.03069},{46.09592,  44.06322,  42.03069},{46.10030,  46.10030,  40.00000},{48.13446,  44.06749,  40.00000},{35.93251,  48.13446,  40.00000},{37.96930,  48.13354,  42.03070},{40.00000,  48.13381,  40.00000},{42.03070,  48.13354,  42.03070},{44.06749,  48.13446,  40.00000},{33.90112,  33.90112,  44.06161},{35.93646,  31.86571,  44.06354},{35.93839,  33.90112,  46.09888},{37.96931,  33.90408,  44.06322},{40.00000,  31.86554,  44.06749},{40.00000,  33.89970,  46.10030},{42.03069,  33.90408,  44.06322},{44.06354,  31.86571,  44.06354},{44.06161,  33.90112,  46.09888},{46.09888,  33.90112,  44.06161},{31.86571,  35.93646,  44.06354},{33.90112,  35.93839,  46.09888},{33.90408,  37.96931,  44.06322},{35.93692,  35.93692,  44.06308},{35.93678,  37.96931,  46.09592},{37.96931,  35.93678,  46.09592},{37.96861,  37.96861,  44.06308},{40.00000,  35.93697,  44.06303},{40.00000,  37.96886,  46.09566},{42.03069,  35.93678,  46.09592},{42.03139,  37.96861,  44.06308},{44.06308,  35.93692,  44.06308},{44.06322,  37.96931,  46.09592},{46.09888,  35.93839,  46.09888},{46.09592,  37.96931,  44.06322},{48.13429,  35.93646,  44.06354},{31.86554,  40.00000,  44.06749},{33.89970,  40.00000,  46.10030},{33.90408,  42.03069,  44.06322},{35.93697,  40.00000,  44.06303},{35.93678,  42.03069,  46.09592},{37.96886,  40.00000,  46.09566},{37.96861,  42.03139,  44.06308},{40.00000,  40.00000,  44.06313},{40.00000,  42.03114,  46.09566},{42.03114,  40.00000,  46.09566},{42.03139,  42.03139,  44.06308},{44.06303,  40.00000,  44.06303},{44.06322,  42.03069,  46.09592},{46.10030,  40.00000,  46.10030},{46.09592,  42.03069,  44.06322},{48.13446,  40.00000,  44.06749},{31.86571,  44.06354,  44.06354},{33.90112,  44.06161,  46.09888},{33.90112,  46.09888,  44.06161},{35.93692,  44.06308,  44.06308},{35.93839,  46.09888,  46.09888},{37.96931,  44.06322,  46.09592},{37.96931,  46.09592,  44.06322},{40.00000,  44.06303,  44.06303},{40.00000,  46.10030,  46.10030},{42.03069,  44.06322,  46.09592},{42.03069,  46.09592,  44.06322},{44.06308,  44.06308,  44.06308},{44.06161,  46.09888,  46.09888},{46.09888,  44.06161,  46.09888},{46.09888,  46.09888,  44.06161},{48.13429,  44.06354,  44.06354},{35.93646,  48.13429,  44.06354},{40.00000,  48.13446,  44.06749},{44.06354,  48.13429,  44.06354},{35.93646,  35.93646,  48.13429},{37.96930,  37.96930,  48.13354},{40.00000,  35.93251,  48.13446},{42.03070,  37.96930,  48.13354},{44.06354,  35.93646,  48.13429},{35.93251,  40.00000,  48.13446},{37.96930,  42.03070,  48.13354},{40.00000,  40.00000,  48.13381},{42.03070,  42.03070,  48.13354},{44.06749,  40.00000,  48.13446},{35.93646,  44.06354,  48.13429},{40.00000,  44.06749,  48.13446}};
    //const int N = 248;
    point au[] = { {10.701780,   47.113024,    4.680968},{11.596179,   47.143333,    7.555041},{11.047156,   49.569405,    6.147035},{ 8.888161,   47.780349,    6.795032},{ 9.830161,   45.104299,    6.502047},{12.566127,   45.232284,    5.679045},{13.301999,   48.009501,    5.406641},{12.030194,   49.580400,    8.932070},{ 9.840217,   47.887333,    9.610079},{10.687213,   45.164278,    9.339068},{ 8.929109,   49.254387,    4.344035},{ 7.999755,   46.279376,    4.615939},{10.367111,   44.460244,    3.891016},{12.934957,   46.394562,    3.096706},{11.876086,   49.308374,    3.399016},{ 9.320190,   50.375420,    8.228049},{ 7.964203,   45.912292,    8.666061},{11.589006,   43.045428,    7.473970},{15.203844,   45.980356,    6.370990},{13.791824,   50.404565,    6.823687},{ 9.883830,   51.861442,    2.886829},{ 5.829098,   48.342378,    4.205046},{ 7.378080,   43.285225,    3.695038},{12.845613,   43.609144,    1.896898},{14.114904,   48.943209,    1.510658},{11.578842,   52.045682,    7.609744},{ 7.199213,   48.659373,    8.969050},{ 8.876184,   43.237231,    8.456077},{14.348939,   43.260094,    6.764733},{15.980998,   48.714582,    6.242719},{ 9.377119,   51.752480,    5.867056},{ 7.168141,   49.911416,    6.457063},{ 6.152165,   47.029310,    6.959032},{ 7.154154,   44.307244,    6.435053},{ 9.482151,   42.413182,    5.822036},{12.486873,   42.654492,    4.773865},{14.775951,   44.404396,    4.195756},{15.491875,   47.302093,    3.869900},{14.655822,   50.221314,    4.237918},{12.161122,   51.736482,    4.845039},{ 9.760068,   52.741455,    9.672785},{ 7.564229,   51.112439,   10.234065},{ 6.284257,   46.771304,   10.816069},{ 7.051241,   44.050226,   10.481066},{10.586218,   41.183188,    9.325062},{12.030194,   54.760535,    8.932070},{ 4.966220,   50.458435,    9.060054},{16.345803,   41.255383,    6.396001},{15.296142,   52.950478,    5.428043},{ 4.866187,   44.713247,    8.683068},{11.151151,   40.060119,    5.812032},{18.636813,   48.821624,    5.126876},{13.358390,   47.113024,   13.183958},{12.463990,   47.143333,   10.309884},{13.013013,   49.569405,   11.717890},{15.172008,   47.780349,   11.069894},{14.230008,   45.104299,   11.362879},{11.494043,   45.232284,   12.185880},{10.758170,   48.009501,   12.458284},{14.219952,   47.887333,    8.254846},{13.372956,   45.164278,    8.525857},{15.131060,   49.254387,   13.520890},{16.060415,   46.279376,   13.248986},{13.693058,   44.460244,   13.973909},{11.125212,   46.394562,   14.768219},{12.184083,   49.308374,   14.465909},{14.739979,   50.375420,    9.636877},{16.095966,   45.912292,    9.198864},{12.471163,   43.045428,   10.390955},{ 8.856325,   45.980356,   11.493936},{10.268345,   50.404565,   11.041238},{14.176339,   51.861442,   14.978096},{18.231071,   48.342378,   13.659879},{16.682089,   43.285225,   14.169887},{11.214556,   43.609144,   15.968028},{ 9.945266,   48.943209,   16.354267},{12.481327,   52.045682,   10.255182},{16.860957,   48.659373,    8.895875},{15.183985,   43.237231,    9.408849},{ 9.711230,   43.260094,   11.100193},{ 8.079171,   48.714582,   11.622206},{14.683051,   51.752480,   11.997869},{16.892028,   49.911416,   11.407862},{17.908004,   47.029310,   10.905894},{16.906015,   44.307244,   11.429872},{14.578018,   42.413182,   12.042889},{11.573297,   42.654492,   13.091060},{ 9.284218,   44.404396,   13.669169},{ 8.568294,   47.302093,   13.995025},{ 9.404347,   50.221314,   13.627008},{11.899047,   51.736482,   13.019886},{14.300101,   52.741455,    8.192140},{16.495940,   51.112439,    7.630860},{17.775912,   46.771304,    7.048856},{17.008928,   44.050226,    7.383859},{13.473952,   41.183188,    8.539863},{19.093949,   50.458435,    8.804871},{ 7.714366,   41.255383,   11.468925},{ 8.764028,   52.950478,   12.436882},{19.193982,   44.713247,    9.181857},{12.909018,   40.060119,   12.052893},{ 5.423356,   48.821624,   12.738049} };
    const int N = 102; //51; //

    const double Rmax = 10.5; //3.5

    const double kw   = 0.03; // primitive coefficient
    const double Kww = kw*kw/(kw+kw); //

    // auxilliary
    //const int M = N; // dimension of auxilliary basis
    const double K2   = 2*kw; // density Gaussian exponenu
    const double K22  = K2*K2/(K2+K2); // for erf
    const double K11 = sqrt(K22);
    const double KV  = K11;
    const double GN3D  = pow(K2/PI,0.75);


    // convert the positions to BOHR
    for (int n=0; n<N; ++n) {
        au[n].x *= AU;
        au[n].y *= AU;
        au[n].z *= AU;
    }


    // center
    {
        point C;
        C.x=C.y=C.z=0;

        for (int n=0; n<N; ++n) {
            C.x+=au[n].x;
            C.y+=au[n].y;
            C.z+=au[n].z;
        }

        C.x/=double(N);
        C.y/=double(N);
        C.z/=double(N);

        for (int n=0; n<N; ++n) {
            au[n].x-=C.x;
            au[n].y-=C.y;
            au[n].z-=C.z;
        }
    }


    ClusterAll(au, N);
    pause();
    TestClustering(au,N);
    pause();

    if (0) {
        double r2=0;
        for (int n=0; n<N; ++n) {
            for (int m=0; m<n; ++m) {
                r2=max(r2,Distance2(au[n],au[m]));
            }
        }
        cout << "max  r2 = " << r2 << endl;
        cout << "kw * r2 = " << kw*r2 << endl;
        cout << "|r'| = " << sqrt(kw*r2) << endl;
        return 0;
    }

    // test a simple quadrature
    // Quadrature(au,N, Kww);

    IncompleteGamma.InitIncompleteGammaTable();

    // nested half-grids obtained
    // from the original one
    const point * G0 = au; // the original grid (au positions)
    point * G1, * G2, * G3;
    int N1, N2, N3;
    HalveGrid (G0,N,  G1,N1);
    pause();
    //HalveGrid (G1,N1, G2,N2);
    //HalveGrid (G2,N2, G3,N3);

    //
    cout << "N0 = "  << N  << endl; // original grid
    cout << "N1 = "  << N1 << endl; // aux basis grid
    cout << "N2 = "  << N2 << endl; //
    cout << "N3 = "  << N3 << endl; //
    pause();



    // compute S & T  && find (Laplacian) basis
    // ========================================

    tensor2 SS(N), TT(N), LL(N), UU(N);
    double ww[N*N]; // this is being reused a zillion times. be careful

    for (int i=0; i<N; ++i) {
        for (int j=0; j<=i; ++j) {

            double x = au[i].x - au[j].x;
            double y = au[i].y - au[j].y;
            double z = au[i].z - au[j].z;
            double r2 = (x*x+y*y+z*z);

            SS(j,i)  = exp(-Kww*r2);
            TT(j,i) = 0.5*(6*Kww-4*Kww*Kww*r2)*exp(-Kww*r2);

            SS(i,j) = SS(j,i);
            TT(i,j) = TT(j,i);
        }
    }

    // try to figure out what's going on
    LL=SS;
    EV(LL,ww);

    //LL=TT; UU=SS;
    //GEV(LL,UU,ww); // sorted in increasing order
    //for (int i=0; i<N; ++i) cout << ww[i] << " "; cout << endl; pause();

    tensor2 LLT(N);

    // transpose basis
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            LLT(j,i) = LL(i,j);
        }
    }


    // 2017/01/19
    // try CD+ALS factorization
    // 2017/01/28
    // the main source for numerical error seems to be that:
    // 1) the matrix is (as we expect), very ill-conditioned
    // 2) the CD can't go beyond 16 decimal places of numerical accuracy (double precission)
    // 3) the contamination to the quadrature from discarded eigenfunctions scales as sqrt(condition number)
    // to deal with this we need:
    // 1) better precision in critical calculations (gamma, for one)
    // 2) a spectral transformation on M
    //
    {
        const double CDTHRESH = 1e-14; //1e-12;
        const double DIATHRESH = 1e-14; //1e-12;
        const double ALSTHRESH = 1e-20; //10^-10 ^2
        const int MAXM = min(32,N) * N;


        // DO THE MODIFIED CD IN O(N^4)
        // ============================

        tensor2 * ZZ = new tensor2[MAXM];
        point * Rgrid = new point[MAXM];

        //#pragma omp parallel for schedule(dynamic)
        for (int ii=0; ii<N; ++ii)
            for (int jj=0; jj<N; ++jj)
                ww[ii*N+jj] = S4int (ii,jj, ii,jj, au,SS,kw); //ERI (ii,jj, ii,jj, au,SS,K22);

        // compute sum of ev's
        double dd=0; for (int ij=0; ij<N*N; ++ij) dd+=ww[ij];

        cout << "Matrix sum of ev's = " << dd << endl;


        int M=0;

        double tr=0; // count the trace norm of the reconstructed matrix

        while (M<MAXM) {

            // count whether the remaining trace (sum of leftover ev's) is worth the effort
            if (dd-tr < CDTHRESH*tr) break;

            // find largest value in diagonal
            int ij=0;
            for (int kl=0; kl<N*N; ++kl) if (ww[kl]>ww[ij]) ij=kl;


            int ii = ij/N;
            int jj = ij%N;

            cout << "it = " << M << "   largest element = " << ii << ","<<jj << "   value = " << ww[ij] << "   ";

            // store the centre
            Rgrid[M] = MidPoint(au[ii], au[jj]);

            // initialize the new tensor
            ZZ[M].setsize(N);


            // now compute the full column of integrals
            #pragma omp parallel for
            for (int kk=0; kk<N; ++kk) {
                for (int ll=0; ll<=kk; ++ll) {
                    ZZ[M](kk,ll) = S4int (ii,jj, kk,ll, au,SS,kw); //ERI (ii,jj, kk,ll, au,SS,K22);
                }
            }

            // update the column by removing all previous components
            for (int m=0; m<M; ++m) {
                double w = ZZ[m](ii,jj);

                #pragma omp parallel for
                for (int kk=0; kk<N; ++kk)
                    for (int ll=0; ll<=kk; ++ll)
                        ZZ[M](kk,ll) -= w * ZZ[m](kk,ll);
            }

            // fill in the transposed
            for (int kk=0; kk<N; ++kk)
                for (int ll=0; ll<kk; ++ll)
                    ZZ[M](ll,kk) = ZZ[M](kk,ll);

            // scale the vector
            double iw = 1./sqrt(ZZ[M](ii,jj));
            ZZ[M] *= iw;

            // count the trace contribution to the matrix
            // which coincides with the vector norm^2
            double n2=0;
            for (int kk=0; kk<N; ++kk)
                for (int ll=0; ll<N; ++ll)
                    n2 += ZZ[M](kk,ll) * ZZ[M](kk,ll);
            tr+=n2;

            cout << "norm^2 = " << n2 << "   " << "trace = " << dd-tr << endl;


            // update diagonal
            for (int kk=0; kk<N; ++kk)
                for (int ll=0; ll<N; ++ll)
                    ww[kk*N+ll] -= ZZ[M](kk,ll)*ZZ[M](kk,ll);

            // incremet rank
            ++M;
        }

        cout << "N = " << N << endl;
        cout << "M = " << M << endl;

        if (M==MAXM) cout << "Cholesky failed to decompose matrix within prescribed number of elements" << endl;

        const int R = M;
        //Rgrid = G1; // use the points that better span everything

        // diagonalize from CD
        int M2 = M;
        double s[M];

        // alternative diagonalization
        // ===========================
        {
            tensor2 T(M,N*N);
            tensor2 P(M,N*N);

            for (int mm=0; mm<M; ++mm)
                for (int ii=0; ii<N; ++ii)
                    for (int jj=0; jj<N; ++jj)
                        T(mm,ii*N+jj) = ZZ[mm](ii,jj);

            CD2EV (T, P);

            for (int mm=0; mm<M; ++mm)
                for (int ii=0; ii<N; ++ii)
                    for (int jj=0; jj<N; ++jj)
                        ZZ[mm](ii,jj) = P(mm,ii*N+jj);

            /*
            tensor2 P(M,N*N);
            MPPI2 (T, P);

            // test
            //for (int mm=0; mm<M; ++mm)
                for (int nn=0; nn<M; ++nn) {
                    double s=0;
                    for (int ij=0; ij<N*N; ++ij) s+=T(nn,ij)*P(nn,ij);
                    cout << "  " << s;
                }
                cout << endl;
            */
        }

        pause();

        if (0) {
            tensor2 S(M);
            #pragma omp parallel for schedule(dynamic)
            for (int m=0; m<M; ++m) {
                for (int n=0; n<=m; ++n) {
                    double s=0;
                    for (int ii=0; ii<N; ++ii)
                        for (int jj=0; jj<ii; ++jj)
                            s+= ZZ[m](ii,jj)*ZZ[n](ii,jj);
                    s*=2;
                    for (int ii=0; ii<N; ++ii)
                        s+= ZZ[m](ii,ii)*ZZ[n](ii,ii);

                    S(n,m) = S(m,n) = s;
                }
            }

            EV(S,s, true);

            for (M2=0;M2<M;++M2) if (s[M2]<DIATHRESH*s[0]) break; //if (s[M2]<=0.) break; /
            cout <<"M2 = " << M2 << endl;

            tensor3 V(N,N,M);

            cout << "transposing" << endl;

            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {
                    for (int m=0; m<M; ++m) {
                        V(i,j,m) = ZZ[m](i,j);
                    }
                }
            }

            cout << "rotating" << endl;
            #pragma omp parallel for schedule(dynamic)
            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {
                    double s[M];
                    for (int m=0; m<M; ++m) s[m] = 0;

                    for (int m=0; m<M; ++m) {
                        for (int n=0; n<M; ++n)
                            s[m] += S(m,n)*V(i,j,n);
                    }
                    for (int m=0; m<M; ++m)
                        V(i,j,m) = s[m];
                }
            }

            cout << "transposing back" << endl;
            for (int m=0; m<M; ++m) {
                for (int i=0; i<N; ++i) {
                    for (int j=0; j<=i; ++j) {
                        ZZ[m](i,j) = ZZ[m](j,i) = V(i,j,m);
                    }
                }
            }

            //pause();
        }

        // this was an unsuccessful test of
        // band-limited minimal norm reconstruction
        // 2017/01/26
        if (0) {
            const int M = M2;
            //Rgrid = G1;
            //const int R = M2/2;

            /*
            tensor3 RR(N,N,R);

            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {
                    point P;
                    P = MidPoint(au[i],au[j]);

                    double K = exp(-Kww*Distance2(au[i],au[j]));

                    for (int r=0; r<R; ++r) {
                        RR(j,i, r) = RR(i,j, r) = K * exp(-K2*Distance2(P,Rgrid[r]));
                    }

                }
            }
            */

            tensor2 Y(R,M);

            #pragma omp parallel for
            for (int r=0; r<R; ++r) {
                tensor2 RR(N);

                for (int i=0; i<N; ++i) {
                    for (int j=0; j<=i; ++j) {
                        point P;
                        P = MidPoint(au[i],au[j]);

                        double K = exp(-Kww*Distance2(au[i],au[j]));

                        RR(j,i) = RR(i,j) = K * exp(-K2*Distance2(P,Rgrid[r]));
                    }
                }

                for (int m=0; m<M; ++m) {
                    double sum=0;

                    for (int i=0; i<N; ++i) {
                        for (int j=0; j<N; ++j) {
                            sum += ZZ[m](i,j) * RR(i,j);
                        }
                    }
                    Y(r,m) = sum /s[m];
                }
            }

            tensor2 Z(R,M2);

            MPPI (Y, Z, 1e-12);

            /*

            for (int m=0; m<M2; ++m) {
                double is=1./sqrt(s[m]);
                for (int r=0; r<R; ++r) {
                    Z(r,m) *= is;
                }
            }
            */

            // now just contract it with some solution to check how well it works
            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {
                    double c[R]; {
                        point P;
                        P = MidPoint(au[i],au[j]);
                        double K = exp(-Kww*Distance2(au[i],au[j]));
                        for (int r=0; r<R; ++r) c[r] = K * exp(-K2*Distance2(P,Rgrid[r]));
                    }

                    double w[M]; {
                        for (int m=0; m<M; ++m) {
                            double ww=0;
                            for (int r=0; r<R; ++r) ww+=c[r]*Z(r,m);
                            w[m] = ww;
                        }
                    }

                    cout << "testing pair: " << i << " " << j << endl;
                    // now we can print both
                    for (int m=0; m<M; ++m) {
                        double z = ZZ[m](i,j) * (1./sqrt(s[m]));
                         cout << m << " : " << z << "     " << w[m] << "   " << z-w[m] << endl;
                    }
                    pause();


                }
            }

        }


        // NOW WE FIT THE ALS
        // ==================
        //Rgrid = G1; //?

        const int K = M2;

        tensor2 XX(R,N); {
            // now evaluate the basis on all selected grid points
            tensor2 BB(N,R); {
                EvalAux(au,N, Rgrid,R,  kw, BB);
                BB*=GN3D; // normalization
            }

            // transpose
            for (int n=0; n<N; ++n)
                for (int r=0; r<R; ++r)
                    XX(r,n) = BB(n,r);

            // normalize
            for (int r=0; r<R; ++r) {
                double s2=0;
                for (int n=0; n<N; ++n) s2 += XX(r,n)*XX(r,n);
                double is = 1./sqrt(s2);
                for (int n=0; n<N; ++n) XX(r,n)*=is;
            }
        }


        tensor2 WW(R,K);
        double  nn[R];

        // solve ALS for constant XX
        if (0) {
            tensor2 SS(R);
            double  ss[R];
            ContractIJ (WW, ZZ,XX,XX,N,K,R);
            BuildS2   (SS, ss, XX,XX, N,N,R);
            SolveALS (nn, WW, SS, ss, K,R);
        }
        // attempt to solve the system directly by computing the pseudoinverse
        // seems to work just fine! it even improves on previous results
        else {
            tensor2 YY(R,N*N);

            for (int rr=0; rr<R; ++rr)
                for (int ii=0; ii<N; ++ii)
                    for (int jj=0; jj<N; ++jj)
                        YY(rr,ii*N+jj) = XX(rr,ii)*XX(rr,jj);

            tensor2 YYI(R,N*N);

            MPPI2(YY,YYI);

            for (int rr=0; rr<R; ++rr)
                for (int mm=0; mm<K; ++mm) {
                    double sum=0;
                    for (int ii=0; ii<N; ++ii)
                        for (int jj=0; jj<N; ++jj)
                            sum+=YYI(rr,ii*N+jj)*ZZ[mm](ii,jj);
                    WW(rr,mm) = sum;
                }

            // normalize & stone the norm
            for (int rr=0; rr<R; ++rr) {
                double w2=0;
                for (int mm=0; mm<K; ++mm) w2+=WW(rr,mm)*WW(rr,mm);
                w2 = sqrt(w2);
                nn[rr] = w2;

                double iw2 = 1./w2;
                for (int mm=0; mm<K; ++mm) WW(rr,mm)*=iw2;
            }
        }

        // check the norm of the approx. decomp.
        CheckNorm(ZZ, nn, XX,XX, WW, N,K,R);

        // ALS
        // ===

        if (0) {
            int K = 100; // begin with something

            tensor2 SS(R);
            double  ss[R];

            tensor2 UU(R,N);
            tensor2 VV(R,N);

            // copy it
            for (int r=0;r<R;++r)
                for (int n=0;n<N;++n)
                    VV(r,n) = UU(r,n) = XX(r,n);

            int it=0;
            double norm;

            while (true) {
                ++it;
                cout << "step " << it << endl;

                // changed function arguments
                ContractIK (UU,   ZZ,VV,WW,N,K,R);
                BuildS2   (SS, ss, VV, WW, N,K,R);
                SolveALS (nn, UU, SS, ss, N,R);

                norm = CheckNorm(ZZ, nn, UU, VV, WW, N,K,R);
                if (norm<ALSTHRESH) K=min(2*K,M);

                ContractIK (VV,   ZZ,UU,WW,N,K,R);
                BuildS2   (SS, ss, UU, WW, N,K,R);
                SolveALS (nn, VV, SS, ss, N,R);

                norm = CheckNorm(ZZ, nn, UU, VV, WW, N,K,R);
                if (norm<ALSTHRESH) K=min(2*K,M);

                ContractIJ (WW,   ZZ,UU,VV,N,K,R);
                BuildS2   (SS, ss, UU, VV, N,N,R);
                SolveALS (nn, WW, SS, ss, K,R);

                norm = CheckNorm(ZZ, nn, UU, VV, WW, N,K,R);
                if (norm<ALSTHRESH) K=min(2*K,M);
            }
        }



        int it=0;
        while(0) {
            ++it;
            cout << "iteration: " << it << endl;

            // pair similar elements
            int paired[R];

            if (0) {
                tensor2 S(R);
                for (int r=0; r<R; ++r) {
                    for (int s=0; s<R; ++s) {
                        double ss=0;
                        for (int i=0; i<N; ++i) ss+=XX(r,i)*XX(s,i);
                        S(r,s) = ss;
                    }
                }

                int NN=7;

                int    C2[R][NN+1];
                double H2[R][NN+1];

                for (int r=0; r<R; ++r) {
                    int nn = 0;

                    H2[r][0] =  -1; // less than absolute minimum
                    C2[r][0] =  -1;

                    for (int s=0; s<R; ++s) {
                        // push the item to the dummy slot
                        H2[r][nn+1] = S(r,s)*S(r,s);
                        C2[r][nn+1] = s;

                        // now do selection sort;
                        for (int k=nn; k>=0; --k)
                            if (H2[r][k]<H2[r][k+1]) {
                                swap(H2[r][k],H2[r][k+1]);
                                swap(C2[r][k],C2[r][k+1]);
                            }
                            else
                                break;

                        // update number of elements
                        if (nn<NN) ++nn;
                    }
                }

                // start pairing until there is a clash
                for (int r=0; r<R; ++r) paired[r] = r;

                for (int r=0; r<R; ++r) {
                    if (paired[r]!=r) continue; // if paired, don't do anything

                    int s=r; // default (can't be paired with itself)
                    for (int nn=1; nn<NN; ++nn) {
                        s=C2[r][nn];
                        if (paired[s]==s) break; // unpaired
                    }

                    paired[r]=s;
                    paired[s]=r;
                }
            }


            // solve DDD (dictionary-decomposed diagonalization)
            {

                tensor3 D(N,N,R);
                tensor3 V(N,N,R);

                cout << "transposing" << endl;

                for (int i=0; i<N; ++i) {
                    for (int j=0; j<=i; ++j) {
                        for (int m=0; m<K; ++m) {
                            D(i,j,m) = ZZ[m](i,j);
                        }
                    }
                }

                cout << "building inner product" << endl;

                #pragma omp parallel for schedule(dynamic)
                for (int i=0; i<N; ++i) {
                    for (int j=0; j<=i; ++j) {

                        for (int r=0; r<R; ++r) {
                            double v=0;
                            for (int m=0; m<K; ++m) v+=WW(r,m)*D(i,j,m);
                            V(i,j,r) = v;
                        }

                    }
                }

                cout << "building dictionary overlap" << endl;

                tensor2 SS(R);

                for (int s=0; s<R; ++s) {
                    for (int r=0; r<R; ++r) {
                        double v=0;
                        for (int m=0; m<K; ++m) {
                            v+=WW(s,m) * WW(r,m);
                        }
                        SS(s,r) = v;
                    }
                }

                cout << "inverting overlap" << endl;

                INV(SS);

                cout << "solving dictionary decomposition" << endl;


                #pragma omp parallel for schedule(dynamic)
                for (int i=0; i<N; ++i) {
                    for (int j=0; j<=i; ++j) {

                        for (int r=0; r<R; ++r) {
                            double d=0;
                            for (int s=0; s<R; ++s) d+=V(i,j,s)*SS(r,s);
                            D(i,j,r) = d;
                        }
                    }
                }

                cout << "finding principal components" << endl; // reduce this from O(N^4) to O(N^3)

                /*
                for (int r=0; r<R; ++r) {
                    tensor2 T(N);
                    for (int i=0; i<N; ++i)
                        for (int j=0; j<=i; ++j)
                            T(i,j) = T(j,i) = D(i,j,r);

                    tensor2 S(N);
                    for (int i=0; i<N; ++i) {
                        for (int j=0; j<N; ++j) {
                            double ss=0;
                            for (int r=0; r<R; ++r) ss+=XX(r,i)*XX(r,j);
                            S(i,j) = ss;
                        }
                    }


                    double t[N];
                    cout << "component " << r << " ::  ";
                    GEV(T,S,t);
                    pause();

                    // copy new X:
                    for (int i=0; i<N; ++i) XX(r,i) = T(0,i);
                }
                */

                // now find PCs from GEV
                /*
                for (int r=0; r<R; ++r) {
                    int s = paired[r];

                    if (s<r) continue; // skip

                    // do regular
                    if (s==r) {
                        tensor2 T(N);
                        for (int i=0; i<N; ++i)
                            for (int j=0; j<=i; ++j)
                                T(i,j) = T(j,i) = D(i,j,r);

                        double t[N];
                        EV(T,t);
                        for (int i=0; i<N; ++i) XX(r,i) = T(0,i); // copy new X
                    }

                    else {

                        tensor2 T1(N), T2(N);

                        for (int i=0; i<N; ++i)
                            for (int j=0; j<=i; ++j)
                                T1(i,j) = T1(j,i) = D(i,j,r);

                        for (int i=0; i<N; ++i)
                            for (int j=0; j<=i; ++j)
                                T2(i,j) = T2(j,i) = D(i,j,s);

                        // do GEV and pick two largest components



                        double t[N];
                        GEV(T1,T2,t);

                        // copy new X:
                        for (int i=0; i<N; ++i) XX(r,i) = T(0,i);
                    }
                }
                */


                // now find PCs
                for (int r=0; r<R; ++r) {
                    tensor2 T(N);
                    for (int i=0; i<N; ++i)
                        for (int j=0; j<=i; ++j)
                            T(i,j) = T(j,i) = D(i,j,r);

                    double t[N];
                    cout << "component " << r << " ::  " << nn[r] << "  ::   ";
                    EV(T,t,true);
                    pause();

                    // copy new X:
                    for (int i=0; i<N; ++i) XX(r,i) = T(0,i);
                }


            }



            // solve ALS for constant XX
            {
                tensor2 SS(R);
                double  ss[R];
                ContractIJ (WW, ZZ,XX,XX,N,K,R);
                BuildS2   (SS, ss, XX,XX, N,N,R);
                SolveALS (nn, WW, SS, ss, K,R);
            }

            // check whether solution makes sense
            CheckNorm(ZZ, nn, XX,XX, WW, N,K,R);

        }

        // NOW GENERATE THE FINAL DECOMPOSITION
        // ====================================
        tensor2 YY(R,R);
        for (int r=0; r<R; ++r) {
            for (int s=0; s<R; ++s) {
                double yy=0;
                for (int k=0; k<K; ++k) yy+=WW(r,k)*WW(s,k);
                YY(r,s) = yy*nn[r]*nn[s];
            }
        }

        // now the full ERI decomposition should be:
        // Wijkl = XXir XXjr YYrs XXsk XXsl

        // testing ;;
        cout.precision(16);

            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {

                /*
                    double IJ[R];

                    for (int r=0; r<R; ++r) {
                        double s=0,t=0;
                        for (int a=0; a<N; ++a) s+=LL(i,a)*XX(r,a);
                        for (int a=0; a<N; ++a) t+=LL(j,a)*XX(r,a);
                        IJ[r] = s*t;
                    }
                    */

                    for (int k=0; k<=i; ++k) {
                        for (int l=0; l<=k; ++l) {

                        /*
                            double KL[R];

                            for (int r=0; r<R; ++r) {
                                double s=0,t=0;
                                for (int a=0; a<N; ++a) s+=LL(k,a)*XX(r,a);
                                for (int a=0; a<N; ++a) t+=LL(l,a)*XX(r,a);
                                KL[r] = s*t;
                            }
                            */

                            double Wijkl3;

                            {
                                Wijkl3=0;
                                for (int m=0; m<M; ++m) {
                                    Wijkl3 += ZZ[m](i,j)*ZZ[m](k,l);
                                }
                            }

                            double Wijkl2;

                            {
                                Wijkl2=0;

                                /*
                                //#pragma omp parallel for
                                for (int r=0; r<R; ++r) {
                                    for (int s=0; s<R; ++s) {
                                        Wijkl2+=YY(r,s)*IJ[r]*KL[s];
                                    }
                                }
                                */
                                for (int r=0; r<R; ++r) {
                                    for (int s=0; s<R; ++s) {
                                        Wijkl2+=XX(r,i)*XX(r,j)*YY(r,s)*XX(s,k)*XX(s,l);
                                    }
                                }
                            }


                            double Wijkl;
                            Wijkl=ERI (i,j, k,l, au,SS,K22);

                            /*
                            {
                                Wijkl=0;
                                //#pragma omp parallel for
                                for (int ii=0; ii<N; ++ii) {
                                    for (int jj=0; jj<N; ++jj) {

                                        double ss=0;
                                        for (int kk=0; kk<N; ++kk) {
                                            for (int ll=0; ll<N; ++ll) {
                                                ss+=LL(k,kk)*LL(l,ll) * ERI (ii,jj, kk,ll, au,SS,K22);
                                            }
                                        }
                                        ss *= LL(i,ii)*LL(j,jj)*SS(ii,jj);
                                        #pragma omp atomic
                                        Wijkl+=ss;
                                    }
                                }
                            }
                            */

                            //cout << "(" << i << " " << j << "|" << k << " " << l << ")  =  " << Wijkl << " " << Wijkl2 << " " << Wijkl3 << "     " << Wijkl-Wijkl2 << " "  << Wijkl-Wijkl3 << endl;
                            cout << "(" << i << " " << j << "|" << k << " " << l << ")  =  " << Wijkl2 << " " << Wijkl3 << "     " << Wijkl2-Wijkl3 <<  endl;

                        }
                    }

                    pause();

                }
            }






        // what can be done now?
        // =====================
        // 1) test integrals with LL
        // 2) try to improve XX using the dictionary learning technique
        // 3) go for the N^3 algorithm
        // 4) optimize the points
        // 5) diagonalize ZZ? (to split everything better)

    }
    return 0;


    // select the grids
    // ================
    int MM = N1; point * MGrid = G1; // for the auxilliary densities
    int R  = N1; point * RGrid = G1; // for the real-space evaluation grid


    // compute W & find basis
    // ======================
    tensor2 WW(MM), CC(MM);
    int M = MM; // M is just the rank of W: i.e. how many funcs are required to reproduce the density

    {
        #pragma omp parallel for schedule(dynamic)
        for (int i=0; i<MM; ++i) {
            for (int j=0; j<=i; ++j) {
                WW(i,j)=WW(j,i) = -Igamma(K22, MGrid[i], MGrid[j]);
            }
        }

        CC=WW;
        EV(CC, ww);

        for (int k=0; k<MM; ++k) ww[k]=-ww[k];

        // find the dimension of the basis
        for (int k=0; k<MM; ++k)
            if (ww[k]<0) {
                M = k;
                break;
            }

        // scale the eigendensities
        /*
        for (int n=0; n<M; ++n) {
            double w=1./sqrt(ww[n]);
            for (int i=0; i<MM; ++i) CC(n,i) *= w;
        }
        */
    }

    tensor2 VV(MM,R); // eigendensities evaluated at the grid points
    {
        tensor2 YY(MM,R);

        EvalAux(MGrid,MM, RGrid,R, K2, YY);
        YY *= GN3D*GN3D; // density normalization

        for (int n=0; n<MM; ++n) {
            for (int r=0; r<R; ++r) {
                double yy = 0;
                for (int i=0; i<MM; ++i) yy+=CC(n,i)*YY(i,r  );
                VV(n,r) = yy;
            }
        }
    }

    /*
    cout << "M is " << M << endl;
    cout << VV << endl;
    pause();
    */


    // compute T_k; transform slices to harmonic basis imnmediately
    // ============================================================

    tensor3 X(MM,  N,N);

    #pragma omp parallel for
    for (int k=0; k<MM; ++k) {
        if (k%20==0) cout << k<< "  ";

        tensor2 VV(N);

        for (int i=0; i<N; ++i) {
            for (int j=0; j<=i; ++j) {

                point P;

                P.x = 0.5*(au[i].x + au[j].x);
                P.y = 0.5*(au[i].y + au[j].y);
                P.z = 0.5*(au[i].z + au[j].z);

                VV(j,i)= VV(i,j)=SS(i,j)*Igamma(K22, P, MGrid[k]);
            }
        }

        // transform i,j indices to Laplacian basis
        TMT (LL, VV);

        // store in X
        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j)
                X(k,  i,j) = VV(i,j);
    }
    cout << endl;


    // maybe later
    //INV(WW);


    // generate the LC slices
    // ======================

    tensor2 Z[MM];
    for (int k=0; k<MM; ++k) Z[k].setsize(N);
    for (int k=0; k<MM; ++k) Z[k].zeroize();

    const int BB=32; // blocking factor

    #pragma omp parallel for
    for (int kk=0; kk<M; kk+=BB) {
        for (int ll=0; ll<MM; ll+=BB) {// block on l or else it's extremely slow
            for (int ij=0; ij<N*N; ij+=BB) {
                const int maxi2=min(N*N, ij+BB);
                const int maxkk=min(M,   kk+BB);
                const int maxll=min(MM,  ll+BB);


                for (int k=kk; k<maxkk; ++k) {
                    for (int i2=ij; i2<maxi2; ++i2) {
                        double zz=0;
                        for (int l=ll; l<maxll; ++l) zz+=CC(k,l) * X(l,0,i2);
                        Z[k].c2[i2] += zz/ww[k];
                    }
                }

            }
        }
    }
    cout << "rotation done" << endl;

    // diagonalize W and see how many components it has
    if (0) {
        int N2 = (N+N*N)/2;
        tensor2 WW(N*N);
        tensor2 TT(N2);

        cout << 0 << endl;

        {
            #pragma omp parallel for schedule(dynamic)
            for (int ii=0; ii<N; ++ii) {
                for (int jj=0; jj<N; ++jj) {

                    for (int kk=0; kk<N; ++kk) {
                        for (int ll=0; ll<N; ++ll) {
                            WW(ii*N+jj,kk*N+ll) = -ERI(ii,jj,kk,ll, au, SS, K22);
                        }
                    }
                }
            }

        }
        cout << 1 << endl;

        // to transform this right we need a O N5 procedure
        {
            tensor2 LT(N2,N*N);

            int ij=0;
            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {

                    #pragma omp parallel for schedule(dynamic)
                    for (int kl=0; kl<N*N; ++kl) {
                        double sum=0;

                        for (int ii=0; ii<N; ++ii) {
                            for (int jj=0; jj<N; ++jj) {
                                sum+=LL(i,ii)*LL(j,jj)*WW(ii*N+jj, kl);
                            }
                        }

                        LT(ij,kl) = sum;
                    }

                    ++ij;
                }
            }

            cout << 2 << endl;

            ij=0;
            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {

                    #pragma omp parallel for schedule(dynamic)
                    for (int kl=0; kl<N2; ++kl) {
                        double sum=0;

                        for (int ii=0; ii<N; ++ii) {
                            for (int jj=0; jj<N; ++jj) {
                                sum+=LL(i,ii)*LL(j,jj)*LT(kl,ii*N+jj);
                            }
                        }

                        TT(kl,ij) = sum;
                    }

                    ++ij;
                }
            }


        }

        cout << 3 << endl;


        double tt[N2];
        EV(TT,tt);
        pause();


    }



    // 2017-01-16
    // "fit" the density
    {

        tensor2 XX(R,N);

        {
            cout << "const R = " << R << endl;

            // now evaluate the basis on all selected grid points
            tensor2 BB(N,R);

            EvalAux(G0,N, RGrid,R,  kw, BB);
            BB*=GN3D; // normalization


            // transform BB to the laplacian basis
            #pragma omp parallel for
            for (int r=0; r<R; ++r) {
                for (int i=0; i<N; ++i) {
                    double ww=0;
                    for (int k=0; k<N; ++k) ww+=LL(i,k)*BB(k,r);
                    XX(r,i) = ww; //BB(i,r); //ww; /ww;
                }
            }

            // pseudoinvert
            cout << "N:  " << N << endl;
            cout << "M:  " << M << endl;
            cout << "MM: " << MM << endl;
            cout << "R:  " << R << endl;

            cout << "here" << endl;


            // detect outliers
            /*
            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {
                    double p[R];
                    for (int r=0; r<R; ++r) p[r] = XX(r,i)* XX(r,j);

                    for (int m=0; m<M; ++m) {
                        for (int r=0; r<R; ++r)
                            p[r] -= Z[m](i,j) * VV(m,r);
                    }

                    //norm
                    double n2=0;
                    for (int r=0; r<R; ++r) n2+=p[r]*p[r];

                    cout << "norm^2 = " << n2 << endl;

                }
            }
            */



            tensor2 RR(MM,R);
            MPPI(VV, RR); // pseudoinverse


            // let's try fitting (ij) in AO

            cout.precision(16);

            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {

                    double p[R];
                    for (int r=0; r<R; ++r) p[r] = XX(r,i)* XX(r,j);

                    for (int m=0; m<M; ++m) {
                        for (int r=0; r<R; ++r)
                            p[r] -= Z[m](i,j) * VV(m,r);
                    }

                    //norm
                    double n2=0;
                    for (int r=0; r<R; ++r) n2+=p[r]*p[r];

                    cout << "fitting norm^2 = " << n2 << endl;

                    //for (int r=0; r<R; ++r) cout << p[r]*p[r] << " "; cout << endl;

                    // quadrature of the densities
                    // from the grid expansion
                    double f[MM];
                    for (int m=0; m<MM; ++m) {
                        double gg=0;
                        for (int r=0; r<R; ++r)
                            gg+=RR(m,r) * XX(r,i)* XX(r,j); // *BB(i,r)*BB(j,r);
                        f[m]=gg;//Z[m](i,j);/
                    }
                    //for (int m=0; m<MM; ++m) cout << f[m] << " "; cout << endl << endl;
                    //for (int m=0; m<MM; ++m) cout << Z[m](i,j) << " "; cout  << endl << endl;

                    double e2=0;
                    for (int r=0; r<R; ++r) {
                        double rr = XX(r,i)* XX(r,j); // BB(i,r)*BB(j,r);
                        for (int m=0; m<MM; ++m) rr-= f[m]*VV(m,r);
                        e2+=rr*rr;
                    }
                    cout<< "residual norm : " << e2 << endl;

                    point P;
                    P.x = 0.5*(au[i].x + au[j].x);
                    P.y = 0.5*(au[i].y + au[j].y);
                    P.z = 0.5*(au[i].z + au[j].z);


                    for (int k=0; k<=i; ++k) {
                        for (int l=0; l<=k; ++l) {

                        // quadrature of the densities
                        // from the grid expansion
                            double g[MM];
                            for (int m=0; m<MM; ++m) {
                                double gg=0;
                                for (int r=0; r<R; ++r)
                                    gg+=RR(m,r)* XX(r,k)* XX(r,l); //BB(k,r)*BB(l,r);
                                g[m]= gg;//Z[m](k,l);//
                            }

                            point Q;

                            Q.x = 0.5*(au[k].x + au[l].x);
                            Q.y = 0.5*(au[k].y + au[l].y);
                            Q.z = 0.5*(au[k].z + au[l].z);

                            double Wijkl = SS(i,j)*SS(k,l)*Igamma(K22,P,Q);
                            double Wijkl2=0;
                            double Wijkl3=0;
                            for (int m=0; m<M; ++m) Wijkl2 += Z[m](i,j)*Z[m](k,l) * ww[m]; // * (ww[m]*ww[m]);
                            for (int m=0; m<M; ++m) Wijkl3 += f[m]*g[m] * ww[m]; // (ww[m]*ww[m]);


                            double WW;

                            {
                                WW=0;
                                //#pragma omp parallel for
                                for (int ii=0; ii<N; ++ii) {
                                    for (int jj=0; jj<N; ++jj) {
                                        P.x = 0.5*(au[ii].x + au[jj].x);
                                        P.y = 0.5*(au[ii].y + au[jj].y);
                                        P.z = 0.5*(au[ii].z + au[jj].z);
                                        double ss=0;
                                        for (int kk=0; kk<N; ++kk) {
                                            for (int ll=0; ll<N; ++ll) {
                                                Q.x = 0.5*(au[kk].x + au[ll].x);
                                                Q.y = 0.5*(au[kk].y + au[ll].y);
                                                Q.z = 0.5*(au[kk].z + au[ll].z);
                                                ss+=LL(k,kk)*LL(l,ll)*SS(kk,ll)*Igamma(K22,P,Q);
                                            }
                                            //WW+=LL(i,ii)*LL(j,jj)*SS(ii,jj)* LL(k,kk)*LL(l,kk)*SS(kk,kk)*Igamma(K22,P,au[kk]);
                                        }
                                        ss *= LL(i,ii)*LL(j,jj)*SS(ii,jj);
                                        #pragma omp atomic
                                        WW+=ss;
                                    }
                                    /*
                                    {
                                        for (int kk=0; kk<N; ++kk) {
                                            for (int ll=0; ll<N; ++ll) {
                                                Q.x = 0.5*(au[kk].x + au[ll].x);
                                                Q.y = 0.5*(au[kk].y + au[ll].y);
                                                Q.z = 0.5*(au[kk].z + au[ll].z);
                                                WW+=0.5*LL(i,ii)*LL(j,ii)*SS(ii,ii)* LL(k,kk)*LL(l,ll)*SS(kk,ll)*Igamma(K22,au[ii],Q);
                                            }
                                            WW+=0.25*LL(i,ii)*LL(j,ii)*SS(ii,ii)* LL(k,kk)*LL(l,kk)*SS(kk,kk)*Igamma(K22,au[ii],au[kk]);
                                        }
                                    }
                                    */
                                }
                                //WW*=4;
                            }
                            Wijkl=WW;

                            //Wijkl=Wijkl2;
                            //Wijkl2=Wijkl3;

                            cout << "(" << i << " " << j << "|" << k << " " << l << ")  =  " << Wijkl << " " << Wijkl2 << " " << Wijkl3 << "     " << Wijkl-Wijkl2 << " "  << Wijkl-Wijkl3 << endl;

                        }
                    }

                    pause();

                }
            }

            /*
            // normalize XX
            for (int r=0; r<R; ++r) {
                double ww=0;
                for (int i=0; i<N; ++i) ww+=XX(r,i)*XX(r,i);
                double ss=1./sqrt(ww);
                for (int i=0; i<N; ++i) XX(r,i)*=ss;
            }

            // now attempt to fit Z
            // ====================

            tensor2 S2(R);
            double s2[R];
            BuildS2 (S2,s2, XX,XX, N,N, R);
            //BuildS2W (S2,s2,  X,vv,W,  XX,XX, N,N, R);

            tensor2 VV(R,M);
            ContractIJ (VV, Z, XX,XX, N,M,R);
            //ContractXXW (VV, Z, X,vv,W,  XX,XX, N,M,R);

            double FF[R];
            SolveALS (FF, VV, S2,s2, M,R);

            CheckNorm(Z, FF, XX,XX, VV, N,M,R);
            //CheckNormW(X,vv,W, FF,XX,VV, N,M,R);
            */

        }

        // now do ALS
        if (0) {
            //const int N = N;
            const int K = M;

            tensor2 RR(K*N,K);
            double  ll[N*K];

            tensor2 SS(R);
            double  ss[R];

            tensor2 UU(R,N);
            tensor2 VV(R,N);
            tensor2 WW(R,K);
            double  nn[R];


            // copy it
            for (int r=0;r<R;++r)
                for (int n=0;n<N;++n)
                    VV(r,n) = UU(r,n) = XX(r,n);

            int it=0;

            while (true) {
                ++it;
                cout << "step " << it << endl;

                // changed function arguments

                ContractIJ (WW,   Z,UU,VV,N,K,R);
                //ContractIJ (WW, RR,LL,ll,   UU,VV,N,K,R);
                BuildS2   (SS, ss, UU, VV, N,N,R);
                SolveALS (nn, WW, SS, ss, K,R);

                CheckNorm(Z, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);

                ContractIK (UU,   Z,VV,WW,N,K,R);
                //ContractIK (UU, RR,LL,ll,  VV,WW,N,K,R);
                BuildS2   (SS, ss, VV, WW, N,K,R);
                SolveALS (nn, UU, SS, ss, N,R);

                CheckNorm(Z, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);

                ContractIK (VV,   Z,UU,WW,N,K,R);
                //ContractIK (VV, RR,LL,ll, UU,WW,N,K,R);
                BuildS2   (SS, ss, UU, WW, N,K,R);
                SolveALS (nn, VV, SS, ss, N,R);

                CheckNorm(Z, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);
            }
        }


    }


    return 0;
}




int main_RKHS() {

    //point au[] = {{40.00000,  40.00000,  40.00000},{40.00000,  42.03148,  42.03148},{42.03148,  40.00000,  42.03148},{42.03148,  42.03148,  40.00000},{35.93839,  33.90112,  33.90112},{40.00000,  33.89970,  33.89970},{44.06161,  33.90112,  33.90112},{33.90112,  35.93839,  33.90112},{35.93646,  35.93646,  31.86571},{35.93678,  37.96931,  33.90408},{37.96931,  35.93678,  33.90408},{37.96930,  37.96930,  31.86646},{40.00000,  35.93251,  31.86554},{40.00000,  37.96886,  33.90434},{42.03069,  35.93678,  33.90408},{42.03070,  37.96930,  31.86646},{44.06354,  35.93646,  31.86571},{44.06322,  37.96931,  33.90408},{46.09888,  35.93839,  33.90112},{33.89970,  40.00000,  33.89970},{35.93251,  40.00000,  31.86554},{35.93678,  42.03069,  33.90408},{37.96886,  40.00000,  33.90434},{37.96930,  42.03070,  31.86646},{40.00000,  40.00000,  31.86619},{40.00000,  42.03114,  33.90434},{42.03114,  40.00000,  33.90434},{42.03070,  42.03070,  31.86646},{44.06749,  40.00000,  31.86554},{44.06322,  42.03069,  33.90408},{46.10030,  40.00000,  33.89970},{33.90112,  44.06161,  33.90112},{35.93646,  44.06354,  31.86571},{35.93839,  46.09888,  33.90112},{37.96931,  44.06322,  33.90408},{40.00000,  44.06749,  31.86554},{40.00000,  46.10030,  33.89970},{42.03069,  44.06322,  33.90408},{44.06354,  44.06354,  31.86571},{44.06161,  46.09888,  33.90112},{46.09888,  44.06161,  33.90112},{33.90112,  33.90112,  35.93839},{35.93646,  31.86571,  35.93646},{35.93678,  33.90408,  37.96931},{37.96930,  31.86646,  37.96930},{37.96931,  33.90408,  35.93678},{40.00000,  31.86554,  35.93251},{40.00000,  33.90434,  37.96886},{42.03070,  31.86646,  37.96930},{42.03069,  33.90408,  35.93678},{44.06354,  31.86571,  35.93646},{44.06322,  33.90408,  37.96931},{46.09888,  33.90112,  35.93839},{31.86571,  35.93646,  35.93646},{31.86646,  37.96930,  37.96930},{33.90408,  35.93678,  37.96931},{33.90408,  37.96931,  35.93678},{35.93692,  35.93692,  35.93692},{35.93692,  37.96861,  37.96861},{37.96861,  35.93692,  37.96861},{37.96861,  37.96861,  35.93692},{40.00000,  35.93697,  35.93697},{40.00000,  37.96852,  37.96852},{42.03139,  35.93692,  37.96861},{42.03139,  37.96861,  35.93692},{44.06308,  35.93692,  35.93692},{44.06308,  37.96861,  37.96861},{46.09592,  35.93678,  37.96931},{46.09592,  37.96931,  35.93678},{48.13429,  35.93646,  35.93646},{48.13354,  37.96930,  37.96930},{31.86554,  40.00000,  35.93251},{31.86646,  42.03070,  37.96930},{33.90434,  40.00000,  37.96886},{33.90408,  42.03069,  35.93678},{35.93697,  40.00000,  35.93697},{35.93692,  42.03139,  37.96861},{37.96852,  40.00000,  37.96852},{37.96861,  42.03139,  35.93692},{40.00000,  40.00000,  35.93687},{40.00000,  42.03148,  37.96852},{42.03148,  40.00000,  37.96852},{42.03139,  42.03139,  35.93692},{44.06303,  40.00000,  35.93697},{44.06308,  42.03139,  37.96861},{46.09566,  40.00000,  37.96886},{46.09592,  42.03069,  35.93678},{48.13446,  40.00000,  35.93251},{48.13354,  42.03070,  37.96930},{31.86571,  44.06354,  35.93646},{33.90408,  44.06322,  37.96931},{33.90112,  46.09888,  35.93839},{35.93692,  44.06308,  35.93692},{35.93678,  46.09592,  37.96931},{37.96861,  44.06308,  37.96861},{37.96931,  46.09592,  35.93678},{40.00000,  44.06303,  35.93697},{40.00000,  46.09566,  37.96886},{42.03139,  44.06308,  37.96861},{42.03069,  46.09592,  35.93678},{44.06308,  44.06308,  35.93692},{44.06322,  46.09592,  37.96931},{46.09592,  44.06322,  37.96931},{46.09888,  46.09888,  35.93839},{48.13429,  44.06354,  35.93646},{35.93646,  48.13429,  35.93646},{37.96930,  48.13354,  37.96930},{40.00000,  48.13446,  35.93251},{42.03070,  48.13354,  37.96930},{44.06354,  48.13429,  35.93646},{33.89970,  33.89970,  40.00000},{35.93251,  31.86554,  40.00000},{35.93678,  33.90408,  42.03069},{37.96930,  31.86646,  42.03070},{37.96886,  33.90434,  40.00000},{40.00000,  31.86619,  40.00000},{40.00000,  33.90434,  42.03114},{42.03070,  31.86646,  42.03070},{42.03114,  33.90434,  40.00000},{44.06749,  31.86554,  40.00000},{44.06322,  33.90408,  42.03069},{46.10030,  33.89970,  40.00000},{31.86554,  35.93251,  40.00000},{31.86646,  37.96930,  42.03070},{33.90408,  35.93678,  42.03069},{33.90434,  37.96886,  40.00000},{35.93697,  35.93697,  40.00000},{35.93692,  37.96861,  42.03139},{37.96861,  35.93692,  42.03139},{37.96852,  37.96852,  40.00000},{40.00000,  35.93687,  40.00000},{40.00000,  37.96852,  42.03148},{42.03139,  35.93692,  42.03139},{42.03148,  37.96852,  40.00000},{44.06303,  35.93697,  40.00000},{44.06308,  37.96861,  42.03139},{46.09592,  35.93678,  42.03069},{46.09566,  37.96886,  40.00000},{48.13446,  35.93251,  40.00000},{48.13354,  37.96930,  42.03070},{31.86619,  40.00000,  40.00000},{31.86646,  42.03070,  42.03070},{33.90434,  40.00000,  42.03114},{33.90434,  42.03114,  40.00000},{35.93687,  40.00000,  40.00000},{35.93692,  42.03139,  42.03139},{37.96852,  40.00000,  42.03148},{37.96852,  42.03148,  40.00000},{44.06313,  40.00000,  40.00000},{44.06308,  42.03139,  42.03139},{46.09566,  40.00000,  42.03114},{46.09566,  42.03114,  40.00000},{48.13381,  40.00000,  40.00000},{48.13354,  42.03070,  42.03070},{31.86554,  44.06749,  40.00000},{33.90408,  44.06322,  42.03069},{33.89970,  46.10030,  40.00000},{35.93697,  44.06303,  40.00000},{35.93678,  46.09592,  42.03069},{37.96861,  44.06308,  42.03139},{37.96886,  46.09566,  40.00000},{40.00000,  44.06313,  40.00000},{40.00000,  46.09566,  42.03114},{42.03139,  44.06308,  42.03139},{42.03114,  46.09566,  40.00000},{44.06303,  44.06303,  40.00000},{44.06322,  46.09592,  42.03069},{46.09592,  44.06322,  42.03069},{46.10030,  46.10030,  40.00000},{48.13446,  44.06749,  40.00000},{35.93251,  48.13446,  40.00000},{37.96930,  48.13354,  42.03070},{40.00000,  48.13381,  40.00000},{42.03070,  48.13354,  42.03070},{44.06749,  48.13446,  40.00000},{33.90112,  33.90112,  44.06161},{35.93646,  31.86571,  44.06354},{35.93839,  33.90112,  46.09888},{37.96931,  33.90408,  44.06322},{40.00000,  31.86554,  44.06749},{40.00000,  33.89970,  46.10030},{42.03069,  33.90408,  44.06322},{44.06354,  31.86571,  44.06354},{44.06161,  33.90112,  46.09888},{46.09888,  33.90112,  44.06161},{31.86571,  35.93646,  44.06354},{33.90112,  35.93839,  46.09888},{33.90408,  37.96931,  44.06322},{35.93692,  35.93692,  44.06308},{35.93678,  37.96931,  46.09592},{37.96931,  35.93678,  46.09592},{37.96861,  37.96861,  44.06308},{40.00000,  35.93697,  44.06303},{40.00000,  37.96886,  46.09566},{42.03069,  35.93678,  46.09592},{42.03139,  37.96861,  44.06308},{44.06308,  35.93692,  44.06308},{44.06322,  37.96931,  46.09592},{46.09888,  35.93839,  46.09888},{46.09592,  37.96931,  44.06322},{48.13429,  35.93646,  44.06354},{31.86554,  40.00000,  44.06749},{33.89970,  40.00000,  46.10030},{33.90408,  42.03069,  44.06322},{35.93697,  40.00000,  44.06303},{35.93678,  42.03069,  46.09592},{37.96886,  40.00000,  46.09566},{37.96861,  42.03139,  44.06308},{40.00000,  40.00000,  44.06313},{40.00000,  42.03114,  46.09566},{42.03114,  40.00000,  46.09566},{42.03139,  42.03139,  44.06308},{44.06303,  40.00000,  44.06303},{44.06322,  42.03069,  46.09592},{46.10030,  40.00000,  46.10030},{46.09592,  42.03069,  44.06322},{48.13446,  40.00000,  44.06749},{31.86571,  44.06354,  44.06354},{33.90112,  44.06161,  46.09888},{33.90112,  46.09888,  44.06161},{35.93692,  44.06308,  44.06308},{35.93839,  46.09888,  46.09888},{37.96931,  44.06322,  46.09592},{37.96931,  46.09592,  44.06322},{40.00000,  44.06303,  44.06303},{40.00000,  46.10030,  46.10030},{42.03069,  44.06322,  46.09592},{42.03069,  46.09592,  44.06322},{44.06308,  44.06308,  44.06308},{44.06161,  46.09888,  46.09888},{46.09888,  44.06161,  46.09888},{46.09888,  46.09888,  44.06161},{48.13429,  44.06354,  44.06354},{35.93646,  48.13429,  44.06354},{40.00000,  48.13446,  44.06749},{44.06354,  48.13429,  44.06354},{35.93646,  35.93646,  48.13429},{37.96930,  37.96930,  48.13354},{40.00000,  35.93251,  48.13446},{42.03070,  37.96930,  48.13354},{44.06354,  35.93646,  48.13429},{35.93251,  40.00000,  48.13446},{37.96930,  42.03070,  48.13354},{40.00000,  40.00000,  48.13381},{42.03070,  42.03070,  48.13354},{44.06749,  40.00000,  48.13446},{35.93646,  44.06354,  48.13429},{40.00000,  44.06749,  48.13446}};
    //const int N = 248;
    point au[] = { {10.701780,   47.113024,    4.680968},{11.596179,   47.143333,    7.555041},{11.047156,   49.569405,    6.147035},{ 8.888161,   47.780349,    6.795032},{ 9.830161,   45.104299,    6.502047},{12.566127,   45.232284,    5.679045},{13.301999,   48.009501,    5.406641},{12.030194,   49.580400,    8.932070},{ 9.840217,   47.887333,    9.610079},{10.687213,   45.164278,    9.339068},{ 8.929109,   49.254387,    4.344035},{ 7.999755,   46.279376,    4.615939},{10.367111,   44.460244,    3.891016},{12.934957,   46.394562,    3.096706},{11.876086,   49.308374,    3.399016},{ 9.320190,   50.375420,    8.228049},{ 7.964203,   45.912292,    8.666061},{11.589006,   43.045428,    7.473970},{15.203844,   45.980356,    6.370990},{13.791824,   50.404565,    6.823687},{ 9.883830,   51.861442,    2.886829},{ 5.829098,   48.342378,    4.205046},{ 7.378080,   43.285225,    3.695038},{12.845613,   43.609144,    1.896898},{14.114904,   48.943209,    1.510658},{11.578842,   52.045682,    7.609744},{ 7.199213,   48.659373,    8.969050},{ 8.876184,   43.237231,    8.456077},{14.348939,   43.260094,    6.764733},{15.980998,   48.714582,    6.242719},{ 9.377119,   51.752480,    5.867056},{ 7.168141,   49.911416,    6.457063},{ 6.152165,   47.029310,    6.959032},{ 7.154154,   44.307244,    6.435053},{ 9.482151,   42.413182,    5.822036},{12.486873,   42.654492,    4.773865},{14.775951,   44.404396,    4.195756},{15.491875,   47.302093,    3.869900},{14.655822,   50.221314,    4.237918},{12.161122,   51.736482,    4.845039},{ 9.760068,   52.741455,    9.672785},{ 7.564229,   51.112439,   10.234065},{ 6.284257,   46.771304,   10.816069},{ 7.051241,   44.050226,   10.481066},{10.586218,   41.183188,    9.325062},{12.030194,   54.760535,    8.932070},{ 4.966220,   50.458435,    9.060054},{16.345803,   41.255383,    6.396001},{15.296142,   52.950478,    5.428043},{ 4.866187,   44.713247,    8.683068},{11.151151,   40.060119,    5.812032},{18.636813,   48.821624,    5.126876},{13.358390,   47.113024,   13.183958},{12.463990,   47.143333,   10.309884},{13.013013,   49.569405,   11.717890},{15.172008,   47.780349,   11.069894},{14.230008,   45.104299,   11.362879},{11.494043,   45.232284,   12.185880},{10.758170,   48.009501,   12.458284},{14.219952,   47.887333,    8.254846},{13.372956,   45.164278,    8.525857},{15.131060,   49.254387,   13.520890},{16.060415,   46.279376,   13.248986},{13.693058,   44.460244,   13.973909},{11.125212,   46.394562,   14.768219},{12.184083,   49.308374,   14.465909},{14.739979,   50.375420,    9.636877},{16.095966,   45.912292,    9.198864},{12.471163,   43.045428,   10.390955},{ 8.856325,   45.980356,   11.493936},{10.268345,   50.404565,   11.041238},{14.176339,   51.861442,   14.978096},{18.231071,   48.342378,   13.659879},{16.682089,   43.285225,   14.169887},{11.214556,   43.609144,   15.968028},{ 9.945266,   48.943209,   16.354267},{12.481327,   52.045682,   10.255182},{16.860957,   48.659373,    8.895875},{15.183985,   43.237231,    9.408849},{ 9.711230,   43.260094,   11.100193},{ 8.079171,   48.714582,   11.622206},{14.683051,   51.752480,   11.997869},{16.892028,   49.911416,   11.407862},{17.908004,   47.029310,   10.905894},{16.906015,   44.307244,   11.429872},{14.578018,   42.413182,   12.042889},{11.573297,   42.654492,   13.091060},{ 9.284218,   44.404396,   13.669169},{ 8.568294,   47.302093,   13.995025},{ 9.404347,   50.221314,   13.627008},{11.899047,   51.736482,   13.019886},{14.300101,   52.741455,    8.192140},{16.495940,   51.112439,    7.630860},{17.775912,   46.771304,    7.048856},{17.008928,   44.050226,    7.383859},{13.473952,   41.183188,    8.539863},{19.093949,   50.458435,    8.804871},{ 7.714366,   41.255383,   11.468925},{ 8.764028,   52.950478,   12.436882},{19.193982,   44.713247,    9.181857},{12.909018,   40.060119,   12.052893},{ 5.423356,   48.821624,   12.738049} };
    const int N = 51; //102;

    const double Rmax = 10.5; //3.5

    const double kw   = 0.03; // primitive coefficient
    const double Kww = kw*kw/(kw+kw); //

    // auxilliary
    //const int M = N; // dimension of auxilliary basis
    const double K2   = 2*kw; // density Gaussian exponenu
    const double K22  = K2*K2/(K2+K2); // for erf
    const double K11 = sqrt(K22);
    const double KV  = K11;
    const double GN3D  = pow(K2/PI,0.75);

    IncompleteGamma.InitIncompleteGammaTable();



    const point * G0 = au; // the original grid (au positions)
    point * G1, * G2;
    int N1, N2;
    HalveGrid (G0,N,  G1,N1);
    HalveGrid (G1,N1, G2,N2);

    //
    cout << "N0 = "  << N  << endl; // original grid
    cout << "N1 = "  << N1 << endl; // aux basis grid
    cout << "N2 = "  << N2 << endl; //
    pause();



    // compute S & T  && find (Laplacian) basis
    // ========================================

    tensor2 SS(N), TT(N), LL(N), UU(N);
    double ww[N*N]; // this is being reused a zillion times. be careful

    for (int i=0; i<N; ++i) {
        for (int j=0; j<=i; ++j) {

            double x = au[i].x - au[j].x;
            double y = au[i].y - au[j].y;
            double z = au[i].z - au[j].z;
            double r2 = (x*x+y*y+z*z);

            SS(j,i)  = exp(-Kww*r2);
            TT(j,i) = 0.5*(6*Kww-4*Kww*Kww*r2)*exp(-Kww*r2);

            SS(i,j) = SS(j,i);
            TT(i,j) = TT(j,i);
        }
    }

    LL=TT; UU=SS;

    GEV(LL,UU,ww); // sorted in increasing order

    tensor2 LLT(N);

    // transpose basis
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            LLT(j,i) = LL(i,j);
        }
    }


    // compute grid
    // ============
    const int RMAX = min(32,N)/2;

    // generate a bunch of points
    tensor2 PS(N), PX(N), PY(N), PZ(N);
    PS.zeroize();

    for (int i=0; i<N; ++i) {
        for (int j=i+1; j<N; ++j) PS(i,j) = 0; // skip symmetric
        for (int j=0; j<=i; ++j) {
            double x = au[i].x - au[j].x;
            double y = au[i].y - au[j].y;
            double z = au[i].z - au[j].z;
            double r2 = (x*x+y*y+z*z);
            PS(i,j) = SS(i,j);
            PX(i,j) = 0.5*(au[i].x + au[j].x);
            PY(i,j) = 0.5*(au[i].y + au[j].y);
            PZ(i,j) = 0.5*(au[i].z + au[j].z);
        }
    }

    // sort
    for (int p=0; p<N*N; ++p) {
        int r=p;
        for (int q=p+1; q<N*N; ++q) {
            if (PS.c2[q]>PS.c2[r]) r=q;
        }
        swap(PS.c2[p],PS.c2[r]);
        swap(PX.c2[p],PX.c2[r]);
        swap(PY.c2[p],PY.c2[r]);
        swap(PZ.c2[p],PZ.c2[r]);
    }

    // now sort by maximum distance to included grid point
    // (which should actually be quite close to first neighbour's products)

    //for (int p=0; p<N; ++p) {
    //    PS.c2[p]=1;
    //}

    /*
    for (int p=N; p<N*RMAX; ++p) {
        double xp=PX.c2[p];
        double yp=PY.c2[p];
        double zp=PZ.c2[p];

        double minr2=1e18;

        // search all centers
        for (int q=0; q<N; ++q) {
            double xq=PX.c2[q];
            double yq=PY.c2[q];
            double zq=PZ.c2[q];
            double x=xq-xp;
            double y=yq-yp;
            double z=zq-zp;
            minr2=min(minr2,x*x+y*y+z*z);
        }
        PS.c2[p]=minr2;
    }
    */

    // the first N centers go directly to the grid

    int R=(N*N+N)/2;

    /*

    for (int p=N; p<N*RMAX; ++p) {

        double minr2=1e16;

        // find point farthest from previous points
        int r=p;
        double xp=PX.c2[p];
        double yp=PY.c2[p];
        double zp=PZ.c2[p];

        for (int q=0; q<p; ++q) {
            double xq=PX.c2[q];
            double yq=PY.c2[q];
            double zq=PZ.c2[q];
            double x=xq-xp;
            double y=yq-yp;
            double z=zq-zp;

            if (x*x+y*y+z*z<minr2) {
                r=q;
                minr2=x*x+y*y+z*z;
            }
        }

        // don't pick centers that are near overlapping
        // R~8N because the product grid has double the frequency
        // (except for boundary effects)
        if (minr2<1e-3) {
            R=p;
            break;
        }

        swap(PS.c2[p],PS.c2[r]);
        swap(PX.c2[p],PX.c2[r]);
        swap(PY.c2[p],PY.c2[r]);
        swap(PZ.c2[p],PZ.c2[r]);
    }
    */


    /*
    for (int p=N; p<N*RMAX; ++p) {

        // find point farthest from previous points
        int r=p;
        for (int q=p+1; q<N*RMAX; ++q) {
            if (PS.c2[q]>PS.c2[r]) r=q;
        }

        // don't pick centers that are near overlapping
        // R~8N because the product grid has double the frequency
        // (except for boundary effects)
        if (PS.c2[r]<1e-3) {
            R=p;
            break;
        }

        swap(PS.c2[p],PS.c2[r]);
        swap(PX.c2[p],PX.c2[r]);
        swap(PY.c2[p],PY.c2[r]);
        swap(PZ.c2[p],PZ.c2[r]);

        // now update remaining
        double xp=PX.c2[p];
        double yp=PY.c2[p];
        double zp=PZ.c2[p];

        for (int q=p+1; q<N*RMAX; ++q) {
            double xq=PX.c2[q];
            double yq=PY.c2[q];
            double zq=PZ.c2[q];
            double x=xq-xp;
            double y=yq-yp;
            double z=zq-zp;
            PS.c2[q]=min(PS.c2[q],x*x+y*y+z*z);
        }
    }
    */




    //pause();
    cout << "R : " << R << endl;

    int MM=R;

    for (int r=0; r<R; ++r) {
        cout << " " << PS.c2[r];
        if (PS.c2[r]<exp(-Kww*Rmax*Rmax)) {
            MM = r;
            cout << "MM is " << r << endl;
            break;
        }
    }

    R = MM;

    pause();

    /*
    for (int r=N; r<R; ++r) {
        if (PS.c2[r]<3.5) {
            MM = r;
            cout << "MM is " << r << endl;
            break;
        }
    }
    */

    // now compute the actual weight of the given density (which is related to the distance)
    point * G = new point[MM];
    for (int r=0; r<MM; ++r) {
        G[r].x = PX.c2[r];
        G[r].y = PY.c2[r];
        G[r].z = PZ.c2[r];
    }


    // compute W & find basis
    // ======================
    //int M = RM;

    tensor2 WW(MM), CC(MM);

    #pragma omp parallel for schedule(dynamic)
    for (int i=0; i<MM; ++i) {
        for (int j=0; j<=i; ++j) {

            WW(i,j)=WW(j,i) = -Igamma(K22, G[i], G[j]);

            /*
            //double x = au[i].x - au[j].x;
            //double y = au[i].y - au[j].y;
            //double z = au[i].z - au[j].z;
            double x = G[i].x - G[j].x;
            double y = G[i].y - G[j].y;
            double z = G[i].z - G[j].z;

            double r2 = (x*x+y*y+z*z);

            WW(j,i)= -IncompleteGamma.calcF2(K22*r2); // PS.c2[i]*PS.c2[j] // the difference is not worth the effort
            WW(i,j)=WW(j,i);
            */
        }
    }

    int M;




    // try something quick
    if (0) {
        CC = WW;

        // shift
        for (int i=0; i<MM; ++i) CC(i,i)-=-1e-10;

        //OK, this doesn't seem improvable (numerical error in diagonalization remains the same despite some attempts at improving it)
        /*
        tensor2 W2(MM);
        W2=WW;

        EV(WW, ww); // sorted in decreasing order
        for (int k=0; k<MM; ++k) cout << ww[k] << " "; cout << endl;

        TMT(WW,W2);
        for (int k=0; k<MM; ++k) cout << W2(k,k) << " "; cout << endl;
        //cout << W2 << endl;
        //pause();

        // linear transform for T
        for (int k=0; k<MM; ++k) {
            ww[k] = 1./sqrt(W2(k,k));
            for (int i=0; i<MM; ++i)
                WW(k,i) *= ww[k];
        }

        // preconditioning of W2 (try to recover the exact W^-1/2)
        for (int i=0; i<MM; ++i) {
            for (int j=0; j<MM; ++j) {
                W2(i,j) *= ww[i]*ww[j];
            }
        }

        EV(W2, ww);
        for (int k=0; k<MM; ++k) cout << ww[k] << " "; cout << endl;
        */

        EV(WW, ww);

        for (int k=0; k<MM; ++k) cout << ww[k] << " "; cout << endl;

        // unshift
        for (int k=0; k<MM; ++k) ww[k]+=1e-10;

        M=MM; // dimension of the auxiliary basis


        const double wmin = 0; //-ww[0]*1e-12;

        for (int k=0; k<MM; ++k) {
            ww[k]=-ww[k];

            // should be positive and above a certain thershold
            if (ww[k]<=wmin) {
                M=k;
                break;
            }

            ww[k] = 1./sqrt(ww[k]);
            for (int i=0; i<MM; ++i)
                CC(k,i) *= ww[k];
        }

    }

    cout << "here" << endl;

    M = MM;

    tensor2 VV(M,R);
    {
        tensor2 YY(MM,R);

        CC=WW;
        EV(CC, ww);
        for (int k=0; k<MM; ++k) ww[k]=-ww[k];




        // now evaluate the basis on all selected grid points
        /*
        #pragma omp parallel for
        for (int i=0; i<MM; ++i) {
            for (int r=0; r<R; ++r) {
                double x = G[i].x - PX.c2[r];//PR2[r].x; //
                double y = G[i].y - PY.c2[r];//PR2[r].y;
                double z = G[i].z - PZ.c2[r];//PR2[r].z; //
                double r2 = (x*x+y*y+z*z);
                double g  = exp(-K2*r2);
                YY(i,r  ) = GN3D*GN3D* g; // don't normalize ?
            }
        }
        */

        EvalAux(G,MM, G2,R, K2, YY);

        YY *= GN3D*GN3D;

        for (int n=0; n<M; ++n) {
            for (int r=0; r<R; ++r) {
                double yy = 0;
                for (int i=0; i<MM; ++i) yy+=CC(n,i)*YY(i,r  );
                VV(n,r) = yy;
            }
        }

    }



    cout << "M is " << M << endl;


    // compute T_k; transform slices to harmonic basis imnmediately
    // ============================================================

    tensor3 X(MM,  N,N);

    #pragma omp parallel for
    for (int k=0; k<MM; ++k) {
        if (k%20==0) cout << k<< "  ";

        tensor2 VV(N);

        for (int i=0; i<N; ++i) {
            for (int j=0; j<=i; ++j) {

                double px = 0.5*(au[i].x + au[j].x);
                double py = 0.5*(au[i].y + au[j].y);
                double pz = 0.5*(au[i].z + au[j].z);

                //double xx = px - au[k].x;
                //double yy = py - au[k].y;
                //double zz = pz - au[k].z;
                double xx = px - G[k].x;
                double yy = py - G[k].y;
                double zz = pz - G[k].z;

                double r2 = xx*xx+yy*yy+zz*zz;

                VV(i,j)=SS(i,j)*IncompleteGamma.calcF2(K22*r2); // LOL
                VV(j,i)= VV(i,j);
            }
        }

        // transform i,j indices to Laplacian basis
        TMT (LL, VV);
        // not now

        // store in X
        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j)
                X(k,  i,j) = VV(i,j);
    }
    cout << endl;



    INV(WW);


    // generate the LC slices
    // ======================

    //tensor3 Z(M,  N,N);
    tensor2 Z[MM];
    for (int k=0; k<MM; ++k) Z[k].setsize(N);
    for (int k=0; k<MM; ++k) Z[k].zeroize();

    const int BB=32; // blocking factor

    #pragma omp parallel for
    for (int kk=0; kk<MM; kk+=BB) {
        for (int ll=0; ll<MM; ll+=BB) {// block on l or else it's extremely slow
            for (int ij=0; ij<N*N; ij+=BB) {
                const int maxi2=min(N*N, ij+BB);
                const int maxkk=min(M,   kk+BB);
                const int maxll=min(MM,  ll+BB);


                for (int k=kk; k<maxkk; ++k) {
                    for (int i2=ij; i2<maxi2; ++i2) {
                        double ww=0;
                        for (int l=ll; l<maxll; ++l) ww+=CC(k,l) * X(l,0,i2);
                        //Z(k,i,j) = ww;
                        Z[k].c2[i2] += ww;
                        //Z(k,i,j) = X(k,i,j);
                    }
                }

            }
        }
    }
    cout << "rotation 1 done" << endl;

    /*
    #pragma omp parallel for
    for (int kk=0; kk<M; kk+=BB) {
        for (int ll=0; ll<MM; ll+=BB) {// block on l or else it's extremely slow
            for (int ij=0; ij<N*N; ij+=BB) {
                const int maxi2=min(N*N, ij+BB);
                const int maxkk=min(M,   kk+BB);
                const int maxll=min(MM,  ll+BB);


                for (int k=kk; k<maxkk; ++k) {
                    for (int i2=ij; i2<maxi2; ++i2) {
                        double ww=0;
                        for (int l=ll; l<maxll; ++l) ww+=CC(k,l) * Z[l].c2[i2]; // X(l,0,i2);
                        X(k,0,i2) += ww;
                    }
                }

            }
        }
    }
    cout << "rotation 2 done" << endl;
    */


    /*
    // the other half (painfully slow otherwise)
    for (int k=0; k<M; ++k) {
        for (int i=0; i<N; ++i) {
            for (int j=0; j<i; ++j) {
                Z[k](j,i) = Z[k](i,j);
            }
        }
    }
    */


    // compute SVD
    tensor2 W(M);
    double vv[M]; // singular values

    if (0) {
        #pragma omp parallel for schedule(dynamic)
        for (int k=0; k<M; ++k) {
            for (int l=0; l<=k; ++l) {
                double ss  = 0;

                for (int i=0; i<N; ++i)
                    for (int j=0; j<i; ++j)
                        ss += Z[k](i,j)*Z[l](i,j);
                ss*=2;
                for (int i=0; i<N; ++i) ss+=Z[k](i,i)*Z[l](i,i);

                W(k,l) = W(l,k) = -ss;
            }
        }

        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = M*M;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = M;

        dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, vv, WORK, &LWORK, &INFO);

        cout << "SVD^2  = ";  for (int i=0; i<M; ++i) cout << -vv[i] << " "; cout << endl << endl;

        cout << "SVD    = ";  for (int i=0; i<M; ++i) cout << sqrt(-vv[i]) << " "; cout << endl << endl;

        cout << "rotating SVD";

        #pragma omp parallel for schedule(dynamic)
        for (int i=0; i<N; ++i)
            for (int j=0; j<=i; ++j) {
                double ww[M];
                for (int k=0; k<M; ++k) {
                    double ss = 0;
                    for (int l=0; l<M; ++l) {
                        ss+=W(k,l)*Z[l](i,j);
                    }
                    ww[k] = ss;
                }
                for (int k=0; k<M; ++k) X(k, i,j) = ww[k];
            }


        for (int k=0; k<M; ++k)
            for (int i=0; i<N; ++i)
                for (int j=0; j<i; ++j)
                    X(k, j,i) = X(k, i,j);

        for (int k=0; k<M; ++k) {
            double ss=0;
            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    ss+=X(k, i,j)*X(k, i,j);
            ss=sqrt(ss);
            vv[k]=ss; // store the norm
            ss=1./ss;

            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    X(k, i,j)*=ss;
        }

        cout << "SVD    = ";  for (int i=0; i<M; ++i) cout << vv[i] << " "; cout << endl << endl;

    }

    /*
    // transform the basis back to AO
    #pragma omp parallel for
    for (int k=0; k<M; ++k) {
        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j)
                Z[k](i,j) = X(k,  i,j);
        TMT(LLT,Z[k]);
        TMT(SS,Z[k]);
    }
    */

    // check it really is unitary
    if (0) {
        #pragma omp parallel for schedule(dynamic)
        for (int k=0; k<M; ++k) {
            for (int l=0; l<=k; ++l) {
                double ss  = 0;

                for (int i=0; i<N; ++i)
                    for (int j=0; j<i; ++j)
                        ss += X(k,i,j)*X(l,i,j);
                ss*=2;
                for (int i=0; i<N; ++i) ss+=X(k,i,i)*X(l,i,i);

                W(k,l) = W(l,k) = ss;
            }
        }

        cout.precision(4);
        cout << W << endl;
        pause();
    }


    // check the error in the fitting basis
    // ====================================
    // around 10^-9 to 10^-12 seems reasonably for most integrals
    if (0) {
        // transposed LL basis
        tensor2 LLT(N);
        for (int i=0; i<N; ++i) {
            for (int j=0; j<N; ++j) {
                LLT(i,j) = LL(j,i);
            }
        }

        #pragma omp parallel for
        for (int k=0; k<M; ++k) {
            tensor2 K(N);

            for (int i=0; i<N; ++i)
                for (int j=0; j<N; ++j)
                    K(i,j) = X(k,  i,j);

            // K' = S LL+ Z LL++ S+
            TMT (LLT, K);
            TMT (SS,  K);

            Z[k] = K;

            // copy back to X for the time being
        }

        cout.precision(16);

        for (int i=0; i<N; ++i) {
            for (int j=0; j<=i; ++j) {
                double px = 0.5*(au[i].x + au[j].x);
                double py = 0.5*(au[i].y + au[j].y);
                double pz = 0.5*(au[i].z + au[j].z);

                for (int k=0; k<=i; ++k) {
                    for (int l=0; l<=k; ++l) {

                        double qx = 0.5*(au[k].x + au[l].x);
                        double qy = 0.5*(au[k].y + au[l].y);
                        double qz = 0.5*(au[k].z + au[l].z);
                        double x= px-qx;
                        double y= py-qy;
                        double z= pz-qz;
                        double Wijkl = SS(i,j)*SS(k,l)*IncompleteGamma.calcF2(K22*(x*x+y*y+z*z));
                        double Wijkl2=0;
                        for (int m=0; m<M;++m) Wijkl2 += vv[m]*vv[m]*Z[m](i,j)*Z[m](k,l);

                        cout << "(" << i << " " << j << "|" << k << " " << l << ")  =  " << Wijkl << " " << Wijkl2 << "   " << Wijkl-Wijkl2 << "  " << 1-Wijkl2/Wijkl<< endl;

                    }
                }

                pause();

            }
        }

    }




    // 2016-12-14
    // find the clusters & cluster basis of the diagonalized basis
    if (0) {
        tensor3 Z(M,  N,N); // just so it compiles

        tensor2 T(N);

        tensor2 B(N*M,N);
        double bb[N*M];


        for (int k=0; k<M; ++k) {
            cout << "slice " << k << endl;
            T = Z(k);
            EV(T,ww);
            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    B(k*N+i,j) = T(i,j);
                }
            }
            for (int i=0; i<N; ++i) bb[k*N+i]=ww[i];
        }

        // build giant cluster distance matrix
        tensor2 SS(N*M,N*M);
        #pragma omp parallel for schedule(dynamic)
        for (int n=0; n<N*M; ++n) {
            for (int m=0; m<n; ++m) {
                double s=0;
                for (int k=0; k<N;++k) s+=B(n,k)*B(m,k);
                SS(n,m) = SS(m,n) = -s*s; //*bb[n]*bb[m];
            }
            {
                double s=0;
                for (int k=0; k<N;++k) s+=B(n,k)*B(n,k);
                SS(n,n) = -s*s; //*bb[n]*bb[n];
            }
        }

        cout << "constructing SS done" << endl;

        double ss[N*M];

        EV(SS,ss);
        for (int n=0; n<N*M; ++n) ss[n]=-ss[n];
        cout << "diagonalizeing SS done" << endl;

        /*
        int nmax;
        for (nmax=0; nmax<N*M; ++nmax)
            if (fabs(ss[nmax])<1e-15*double(N*M))
                break;

        cout << " nmax is : " << nmax << "   out of " << N*M << endl;
        */


        tensor2 T2(N);

        // now pick a few Bs
        for (int n=0; n<N*M; ++n) {

            //cout << "coefs: " << endl;
            //for (int k=0; k<N*M; ++k) cout << SS(n,k) << " "; cout  << endl;

            T.zeroize();

            for (int k=0; k<N*M; ++k) {
                double ss=-SS(n,k); //*bb[k];
                for (int i=0; i<N; ++i) {
                    for (int j=0; j<N; ++j) {
                        T(i,j) += ss*B(k,i)*B(k,j);
                    }
                }
            }

            // square the matrix
            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    double ss=0;
                    for (int k=0; k<N; ++k) ss+=T(i,k)*T(j,k);
                    T2(i,j)=-ss;
                }
            }


            //cout << "basis " << n << endl;
            //if (n==0) cout << endl << T << endl << endl;
            EV(T2, ww);
            //if (n==0) cout << endl << T << endl << endl;

            //pause();

            if (n%10==9) pause();
        }

    }


    // 2016-12-19
    // "fit" the density
    {




            /*
            // generate a bunch of points
            //tensor2 PS2(R), PX2(R), PY2(R), PZ2(R);
            point  PR2[64*R];
            double PS2[64*R];

            //PS2.zeroize();

            int ij=0;

            for (int i=0; i<R; ++i) {
                //for (int j=i+1; j<R; ++j) PS2(i,j) = 1e6; // skip symmetric
                for (int j=0; j<=i; ++j) {
                    double x = PX.c2[i] - PX.c2[j];
                    double y = PY.c2[i] - PY.c2[j];
                    double z = PZ.c2[i] - PZ.c2[j];
                    double r2 = (x*x+y*y+z*z);
                    PS2[ij+j] = r2;
                    PR2[ij+j].x = 0.5*(PX.c2[i] + PX.c2[j]);
                    PR2[ij+j].y = 0.5*(PY.c2[i] + PY.c2[j]);
                    PR2[ij+j].z = 0.5*(PZ.c2[i] + PZ.c2[j]);
                }

                // mini sort (will discard everything after some number)
                int jmax = min(i, 64);
                for (int j=0; j<jmax; ++j) {
                    int r=j;
                    for (int k=j+1; k<=i; ++k)
                        if (PS2[ij+k]<PS2[ij+r]) r=k;
                    swap(PS2[ij+j],PS2[ij+r]);
                    swap(PR2[ij+j],PR2[ij+r]);
                }

                ij+=jmax;
            }

            const int MAXR2=ij;

            // sort
            for (int p=0; p<MAXR2; ++p) {
                int r=p;
                for (int q=p+1; q<MAXR2; ++q) {
                    if (PS2[q]<PS2[r]) r=q;
                }
                swap(PS2[p],PS2[r]);
                swap(PR2[p],PR2[r]);
            }


            // now sort by largest distance to previous set
            for (int p=0; p<R; ++p) {
                PS2[p]=0;
            }

            for (int p=R; p<MAXR2; ++p) {
                double xp=PR2[p].x;
                double yp=PR2[p].y;
                double zp=PR2[p].z;

                double minr2=1e18;

                // search all centers
                for (int q=0; q<R; ++q) {
                    double xq=PR2[q].x;
                    double yq=PR2[q].y;
                    double zq=PR2[q].z;
                    double x=xq-xp;
                    double y=yq-yp;
                    double z=zq-zp;
                    minr2=min(minr2,x*x+y*y+z*z);
                }
                PS2[p]=minr2;
            }


            int R2;
            for (int p=R; p<MAXR2; ++p) {

                // find point farthest from previous points
                int r=p;
                for (int q=p+1; q<MAXR2; ++q) {
                    if (PS2[q]>PS2[r]) r=q;
                }

                if (PS2[r]<1e-3) {
                    R2 = p;
                    break;
                }

                swap(PS2[p],PS2[r]);
                swap(PR2[p],PR2[r]);

                // now update remaining
                double xp=PR2[p].x;
                double yp=PR2[p].y;
                double zp=PR2[p].z;

                for (int q=p+1; q<MAXR2; ++q) {
                    double xq=PR2[q].x;
                    double yq=PR2[q].y;
                    double zq=PR2[q].z;
                    double x=xq-xp;
                    double y=yq-yp;
                    double z=zq-zp;
                    PS2[q]=min(PS2[q],x*x+y*y+z*z);
                }
            }


            cout << "R2 is : " << R2 << endl;


            pause();

            R = R2;
            */

            // "halving" the grid again doubles the frequency and increases
            // the number of grid points by 2^3


        // without derivatives:
        // 1691evecs / 1691
        // X^2 = 448.133
        // R^2 = 0.290916

        // with derivatives:
        // 2613 evecs / 6764
        // X^2 = 448.133
        // R^2 = 145.027

        // just a quick test
        //R=N;

        tensor2 XX(R,N);

        //tensor2 YY(M,R);

        {
            cout << "const R = " << R << endl;

            // now evaluate the basis on all selected grid points
            tensor2 BB(N,R);

            for (int i=0; i<N; ++i) {
                for (int r=0; r<R; ++r) {
                    double x = au[i].x - PX.c2[r];//PR2[r].x; //
                    double y = au[i].y - PY.c2[r];//PR2[r].y;
                    double z = au[i].z - PZ.c2[r];//PR2[r].z; //
                    double r2 = (x*x+y*y+z*z);
                    double g  = exp(-kw*r2);
                    BB(i,r  ) = GN3D * g; // don't normalize ?
                }
            }

            // transform BB to the laplacian basis
            #pragma omp parallel for
            for (int r=0; r<R; ++r) {
                for (int i=0; i<N; ++i) {
                    double ww=0;
                    for (int k=0; k<N; ++k) ww+=LL(i,k)*BB(k,r);
                    XX(r,i) = ww;
                }
            }

            // evaluate densities at grid positions
            /*
            #pragma omp parallel for
            for (int k=0; k<M; ++k) {
                for (int r=0; r<R; ++r) {
                    double yy=0;
                    for (int i=0; i<N; ++i)
                        for (int j=0; j<N; ++j)
                            //yy += Z[k](i,j) * BB(i,r) * BB(j,r);
                            yy += X(k, i,j) * XX(r,i) * XX(r,j); // X(k, ij) diagonalized the Coulomb supermatrix W
                    YY(k,r) = yy*vv[k]*vv[k]; //*vv[k]; // * (vv[k]); // yes, this belongs here; eventually it is cancelled with the diagonal later on
                }
            }
            */

            cout << "N:  " << N << endl;
            cout << "M:  " << M << endl;
            cout << "MM: " << MM << endl;
            cout << "R:  " << R << endl;

            cout << "here" << endl;


            tensor2 RR(M,R);

            //MPPI(YY, RR); // pseudoinverse
            MPPI(VV, RR); // pseudoinverse


            // let's try fitting (ij) in AO

            cout.precision(16);

            for (int i=0; i<N; ++i) {
                for (int j=0; j<=i; ++j) {

                    // quadrature of the densities
                    // from the grid expansion
                    double f[M];
                    for (int m=0; m<M; ++m) {
                        double gg=0;
                        for (int r=0; r<R; ++r)
                            gg+=RR(m,r) * XX(r,i)* XX(r,j); // *BB(i,r)*BB(j,r);
                        f[m]=gg;//Z[m](i,j);/
                    }
                    for (int m=0; m<M; ++m) cout << f[m] << " "; cout << endl << endl;
                    //for (int m=0; m<M; ++m) cout << Z[m](i,j) << " "; cout  << endl << endl;
                    for (int m=0; m<M; ++m) cout << X(m,i,j) << " "; cout  << endl << endl;

                    double e2=0;
                    for (int r=0; r<R; ++r) {
                        double rr = XX(r,i)* XX(r,j); // BB(i,r)*BB(j,r);
                        for (int m=0; m<M; ++m) rr-= f[m]*VV(m,r);
                        e2+=rr*rr;
                    }
                    cout<< "residual norm : " << e2 << endl;


                    double px = 0.5*(au[i].x + au[j].x);
                    double py = 0.5*(au[i].y + au[j].y);
                    double pz = 0.5*(au[i].z + au[j].z);


                    for (int k=0; k<=i; ++k) {
                        for (int l=0; l<=k; ++l) {

                        // quadrature of the densities
                        // from the grid expansion
                            double g[M];
                            for (int m=0; m<M; ++m) {
                                double gg=0;
                                for (int r=0; r<R; ++r)
                                    gg+=RR(m,r)* XX(r,k)* XX(r,l); //BB(k,r)*BB(l,r);
                                g[m]= gg;//Z[m](k,l);//
                            }

                            double qx = 0.5*(au[k].x + au[l].x);
                            double qy = 0.5*(au[k].y + au[l].y);
                            double qz = 0.5*(au[k].z + au[l].z);
                            double x= px-qx;
                            double y= py-qy;
                            double z= pz-qz;
                            double Wijkl = SS(i,j)*SS(k,l)*IncompleteGamma.calcF2(K22*(x*x+y*y+z*z));
                            double Wijkl2=0;
                            double Wijkl3=0;
                            for (int m=0; m<M;++m) Wijkl2 += Z[m](i,j)*Z[m](k,l) / ww[m]; // * (ww[m]*ww[m]);
                            for (int m=0; m<M;++m) Wijkl3 += f[m]*g[m] * ww[m]; // (ww[m]*ww[m]);

                            Wijkl=Wijkl2;
                            Wijkl2=Wijkl3;

                            cout << "(" << i << " " << j << "|" << k << " " << l << ")  =  " << Wijkl << " " << Wijkl2 << "   " << Wijkl-Wijkl2 << "  " << 1-Wijkl2/Wijkl<< endl;

                        }
                    }

                    pause();

                }
            }








            // normalize XX
            for (int r=0; r<R; ++r) {
                double ww=0;
                for (int i=0; i<N; ++i) ww+=XX(r,i)*XX(r,i);
                double ss=1./sqrt(ww);
                for (int i=0; i<N; ++i) XX(r,i)*=ss;
            }

            // now attempt to fit Z
            // ====================

            tensor2 S2(R);
            double s2[R];
            BuildS2 (S2,s2, XX,XX, N,N, R);
            //BuildS2W (S2,s2,  X,vv,W,  XX,XX, N,N, R);

            tensor2 VV(R,M);
            ContractIJ (VV, Z, XX,XX, N,M,R);
            //ContractXXW (VV, Z, X,vv,W,  XX,XX, N,M,R);

            double FF[R];
            SolveALS (FF, VV, S2,s2, M,R);

            CheckNorm(Z, FF, XX,XX, VV, N,M,R);
            //CheckNormW(X,vv,W, FF,XX,VV, N,M,R);

        }

        // now do ALS
        {
            //const int N = N;
            const int K = M;

            tensor2 RR(K*N,K);
            double  ll[N*K];

            tensor2 SS(R);
            double  ss[R];

            tensor2 UU(R,N);
            tensor2 VV(R,N);
            tensor2 WW(R,K);
            double  nn[R];


            // copy it
            for (int r=0;r<R;++r)
                for (int n=0;n<N;++n)
                    VV(r,n) = UU(r,n) = XX(r,n);

            int it=0;

            while (true) {
                ++it;
                cout << "step " << it << endl;

                // changed function arguments

                ContractIJ (WW,   Z,UU,VV,N,K,R);
                //ContractIJ (WW, RR,LL,ll,   UU,VV,N,K,R);
                BuildS2   (SS, ss, UU, VV, N,N,R);
                SolveALS (nn, WW, SS, ss, K,R);

                CheckNorm(Z, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);

                ContractIK (UU,   Z,VV,WW,N,K,R);
                //ContractIK (UU, RR,LL,ll,  VV,WW,N,K,R);
                BuildS2   (SS, ss, VV, WW, N,K,R);
                SolveALS (nn, UU, SS, ss, N,R);

                CheckNorm(Z, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);

                ContractIK (VV,   Z,UU,WW,N,K,R);
                //ContractIK (VV, RR,LL,ll, UU,WW,N,K,R);
                BuildS2   (SS, ss, UU, WW, N,K,R);
                SolveALS (nn, VV, SS, ss, N,R);

                CheckNorm(Z, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);
            }




        }


        {
            // one for the point and 3 for its derivatives
            const int RR = 4*R;

            //const int R = n*N;
            cout << "const R  = " << R << endl;
            cout << "const RR = " << RR << endl;

            // this works now
            //cout << "distances: " << endl;
            //for (int p=0; p<R; ++p) cout << PS.c2[p] << " "; cout << endl << endl;

            // now evaluate the basis on all salected points
            tensor2 BB(N,RR);
            tensor2 XX(RR,N);

            for (int i=0; i<N; ++i) {
                for (int r=0; r<R; ++r) {
                    double x = au[i].x - PX.c2[r];//PR2[r].x; //
                    double y = au[i].y - PY.c2[r];//PR2[r].y;
                    double z = au[i].z - PZ.c2[r];//PR2[r].z; //
                    double r2 = (x*x+y*y+z*z);
                    double g  = exp(-kw*r2);
                    BB(i,4*r  ) = g; // don't normalize
                    BB(i,4*r+1) = 2*kw*x*g; // don't normalize
                    BB(i,4*r+2) = 2*kw*y*g; // don't normalize
                    BB(i,4*r+3) = 2*kw*z*g; // don't normalize
                }
            }

            // transform BB to the laplacian basis
            #pragma omp parallel for
            for (int r=0; r<RR; ++r) {
                for (int i=0; i<N; ++i) {
                    double ww=0;
                    for (int k=0; k<N; ++k) ww+=LL(i,k)*BB(k,r);
                    XX(r,i) = ww;
                }
            }

            // normalize XX
            // I think it doesnt make sense
            /*
            for (int r=0; r<RR; ++r) {
                double ww=0;
                for (int i=0; i<N; ++i) ww+=XX(r,i)*XX(r,i);
                double ss=1./sqrt(ww);
                for (int i=0; i<N; ++i) XX(r,i)*=ss;
            }
            */


            // now attempt to fit Z
            // ====================

            tensor2 S2(RR);
            double s2[RR];
            //BuildS2 (S2,s2, XX,XX, N,N, RR);
            BuildS2RD(S2,s2, XX, N,N, R); // only R

            tensor2 VV(RR,M);
            //ContractIJ (VV, Z, XX,XX, N,M,RR);
            ContractRD (VV, Z, XX,    N,M,R);

            double FF[RR];
            SolveALS (FF, VV, S2,s2, M,RR);

            CheckNormRD(Z, FF, XX, VV, N,M,R);
        }
    }

    // we only have to make sure that the eigendensities are fit correctly
    // if we fit R densities (R~8N) and adjust the points, we can set an extra 3R DOF,
    // which means we can have a HIGHER order integration using the same number of points

    //
    // regardless, we have now an excellent initial guess for the ALS iteration
    //





/*
    tensor2 VV(N);
    for (int k=0; k<M; ++k) {
        for (int i=0; i<N; ++i)
            for (int j=0; j<N; ++j)
                VV(i,j)=X(0,  i,j);
        cout << " SLICE = "  <<k << endl;

        cout << VV << endl;
        pause();
    }

    */

    // check the properties of the slices (sparse? convolution structure?)
    // ===================================================================

    cout.precision(3);

    /*
    // slice norms
    for (int i=0; i<M; ++i) {
        if (i%10==0) pause();
        cout << "potential: " << i << " :    ";
        for (int r=0;  r<N;    ++r) cout << Z(i,r,r) << "  "; cout<<endl;
    }

    pause();

    // slice norms
    for (int i=0; i<N; ++i) {
        cout << "function: " << i << " :    ";
        for (int r=0;  r<M;    ++r) cout << Z(r,i,i) << "  "; cout<<endl;
    }

    pause();

    // slice norms
    for (int k=0; k<M; ++k) {
        double ww=0,tr=0;
        for (int ij=0; ij<N*N; ++ij) ww+=Z(k,ij)*Z(k,ij);
        for (int i=0;  i<N;    ++i) tr += Z(k,i,i)*Z(k,i,i);
        cout << "slice " << k << " :    norm^2 = " << ww << "  d^2 =  " << tr <<endl;
    }

    pause();


    // find most occupied entries
    for (int k=0; k<1; ++k) {
        cout << k << " slice" << endl;
        for (int i=0; i<N; ++i) {

            cout << "  " << i << "-th row :  ";

            double z2=0;
            for (int j=0; j<N; ++j) z2+=Z(k,i,j)*Z(k,i,j);

            // sort
            double zz=0;
            for (int n=0; n<N; ++n) {
                double l = n;
                for (int m=n+1; m<N; ++m) if (fabs(Z(k,i,m))>fabs(Z(k,i,l))) l=m;
                swap(Z(k,i,n),Z(k,i,l));
                zz+=Z(k,i,n)*Z(k,i,n);
                if ( (z2-zz)<1.e-6*z2) break;
                cout << Z(k,i,n) << "  ";
            }
            cout << endl << endl;

        }
        cout << endl;
    }
    */


    pause();


    // print first slice
    tensor2 T(N);
    //T0 = Z(0);
    /*
    for (int k=0; k<M; ++k) {
        cout << "slice " << k << endl;
        T = Z(k);
        cout << T << endl << endl;
        EV(T,ww);
        pause();
    }
    */

    // what's left is not so hard



    return 0;
}

int mainALS() {

    // already in bohr
    //point au[] = {{27.645, 27.554, 31.144}, {27.495, 29.559, 33.208}, {29.803, 28.000, 32.892}, {29.281, 29.869, 30.789}, {21.984, 23.877, 17.795}, {26.615, 20.414, 17.047}, {25.708, 24.593, 17.184}, {28.103, 22.904, 16.788}, {29.716, 25.476, 16.435}, {19.619, 25.541, 17.475}, {23.484, 26.248, 17.350}, {24.966, 28.724, 17.082}, {27.272, 27.026, 16.860}, {29.121, 29.425, 16.375}, {22.454, 30.607, 17.252}, {26.543, 30.993, 16.641}, {23.876, 16.164, 21.999}, {27.942, 16.894, 21.453}, {31.939, 17.453, 20.640}, {19.306, 19.469, 22.678}, {21.629, 17.755, 22.250}, {20.971, 19.760, 20.375}, {23.315, 20.087, 21.970}, {25.655, 18.524, 21.816}, {24.959, 20.363, 19.548}, {27.311, 18.607, 19.289}, {27.251, 20.866, 21.333}, {29.638, 19.220, 21.023}, {28.834, 21.082, 19.033}, {31.184, 21.735, 20.705}, {33.599, 19.889, 20.325}, {32.772, 21.886, 17.949}, {35.126, 22.348, 20.002}, {17.036, 21.184, 22.881}, {18.642, 23.388, 22.353}, {20.933, 21.878, 22.241}, {20.239, 23.694, 20.068}, {22.714, 22.059, 19.957}, {22.545, 24.154, 21.833}, {24.926, 22.481, 21.526}, {24.266, 24.420, 19.567}, {26.523, 22.786, 19.249}, {26.323, 24.939, 21.276}, {28.699, 23.363, 20.842}, {28.060, 25.108, 18.801}, {30.420, 23.458, 18.564}, {30.299, 25.580, 20.423}, {32.769, 24.126, 20.215}, {32.036, 26.055, 18.040}, {34.214, 26.537, 19.870}, {36.640, 24.835, 19.619}, {16.424, 25.082, 22.658}, {17.842, 25.402, 20.254}, {17.898, 27.472, 22.315}, {20.081, 25.927, 22.065}, {19.421, 27.804, 19.829}, {21.849, 26.003, 19.670}, {21.776, 28.307, 21.593}, {24.050, 26.551, 21.356}, {23.289, 28.500, 19.274}, {25.714, 26.817, 19.152}, {25.669, 28.968, 21.097}, {27.925, 27.314, 20.804}, {27.291, 29.277, 18.765}, {29.595, 27.581, 18.470}, {29.588, 29.675, 20.385}, {31.876, 28.110, 20.063}, {31.284, 30.014, 18.051}, {33.737, 28.382, 17.859}, {33.482, 30.512, 19.893}, {35.777, 29.036, 19.517}, {14.970, 27.421, 21.843}, {16.541, 30.017, 21.573}, {19.414, 29.950, 21.953}, {21.044, 30.162, 19.691}, {20.946, 32.348, 21.550}, {23.379, 30.574, 21.384}, {22.750, 32.503, 19.359}, {24.966, 30.898, 19.080}, {24.941, 32.980, 20.931}, {27.324, 31.335, 20.802}, {26.517, 33.217, 18.688}, {28.822, 31.611, 18.349}, {28.855, 33.581, 20.383}, {31.161, 32.046, 20.078}, {30.433, 33.977, 17.910}, {32.635, 34.401, 19.687}, {35.138, 32.821, 19.378}, {18.163, 32.481, 21.229}, {20.014, 32.692, 18.507}, {22.606, 34.602, 21.179}, {24.125, 34.907, 18.877}, {24.176, 37.063, 20.811}, {26.402, 35.300, 20.555}, {27.928, 37.695, 20.033}, {30.302, 35.980, 19.962}, {20.646, 15.672, 26.717}, {23.034, 13.981, 26.539}, {22.290, 15.852, 24.489}, {24.647, 16.471, 26.064}, {27.011, 14.753, 25.791}, {26.287, 16.634, 23.810}, {28.740, 14.950, 23.418}, {28.643, 17.060, 25.433}, {30.992, 15.417, 25.152}, {30.436, 17.281, 22.974}, {32.579, 17.825, 24.732}, {32.680, 15.572, 22.853}, {34.231, 18.067, 22.464}, {36.495, 18.474, 24.163}, {14.655, 21.383, 24.677}, {18.342, 17.345, 27.099}, {17.498, 19.227, 24.925}, {19.995, 17.580, 24.677}, {19.856, 19.654, 26.589}, {22.216, 18.015, 26.346}, {21.594, 19.952, 24.289}, {23.896, 18.309, 24.028}, {23.895, 20.477, 25.961}, {26.259, 18.777, 25.675}, {25.549, 20.668, 23.647}, {27.975, 18.983, 23.390}, {27.787, 21.264, 25.314}, {30.267, 19.453, 24.979}, {29.506, 21.444, 22.959}, {31.899, 19.644, 22.650}, {31.780, 21.888, 24.579}, {34.197, 20.211, 24.346}, {33.450, 22.155, 22.356}, {35.759, 20.539, 22.054}, {35.748, 22.666, 23.987}, {38.041, 21.011, 23.767}, {37.393, 22.996, 21.737}, {39.100, 25.322, 21.511}, {15.196, 23.214, 27.062}, {17.485, 21.354, 26.832}, {17.010, 23.279, 24.801}, {19.296, 21.579, 24.617}, {19.158, 23.752, 26.497}, {21.592, 22.017, 26.306}, {20.803, 23.997, 24.211}, {23.283, 22.307, 23.825}, {23.193, 24.422, 25.805}, {25.522, 22.807, 25.586}, {24.803, 24.708, 23.547}, {27.107, 23.034, 23.230}, {27.023, 25.182, 25.316}, {29.439, 23.522, 24.943}, {28.684, 25.487, 22.920}, {31.039, 23.824, 22.560}, {30.921, 25.930, 24.547}, {33.405, 24.238, 24.226}, {32.559, 26.162, 22.225}, {35.040, 24.609, 21.990}, {34.878, 26.751, 23.929}, {37.341, 25.138, 23.731}, {36.530, 26.984, 21.588}, {38.833, 27.487, 23.275}, {14.577, 24.888, 24.753}, {14.501, 26.937, 26.914}, {16.812, 25.336, 26.667}, {16.211, 27.186, 24.484}, {18.591, 25.663, 24.409}, {18.464, 27.761, 26.308}, {20.789, 26.058, 26.068}, {20.142, 28.097, 24.021}, {22.317, 26.342, 23.763}, {22.358, 28.485, 25.691}, {24.621, 26.893, 25.504}, {24.029, 28.585, 23.357}, {26.346, 27.125, 23.196}, {26.300, 29.134, 25.204}, {28.697, 27.546, 24.907}, {27.984, 29.389, 22.846}, {30.356, 27.810, 22.490}, {30.211, 29.980, 24.514}, {32.581, 28.281, 24.281}, {31.827, 30.177, 22.196}, {34.156, 28.546, 21.811}, {34.166, 30.711, 23.819}, {36.468, 28.966, 23.602}, {35.796, 30.932, 21.571}, {38.103, 29.302, 21.202}, {38.082, 31.425, 23.303}, {13.139, 29.450, 26.370}, {16.149, 29.276, 26.518}, {14.943, 29.798, 23.964}, {17.731, 29.588, 24.188}, {17.686, 31.708, 26.143}, {20.041, 30.109, 25.904}, {19.325, 31.993, 23.828}, {21.796, 30.429, 23.659}, {21.628, 32.434, 25.634}, {24.009, 30.757, 25.435}, {23.351, 32.745, 23.295}, {25.687, 31.053, 23.120}, {25.546, 33.194, 24.969}, {27.871, 31.552, 24.678}, {27.193, 33.461, 22.709}, {29.505, 31.870, 22.367}, {29.462, 33.885, 24.456}, {31.780, 32.215, 24.092}, {31.107, 34.145, 22.081}, {33.410, 32.676, 21.824}, {33.414, 34.753, 23.746}, {35.705, 33.080, 23.479}, {35.071, 34.848, 21.392}, {37.432, 35.637, 23.214}, {14.829, 31.722, 26.061}, {16.520, 32.117, 23.565}, {16.256, 34.282, 25.560}, {19.182, 33.951, 25.814}, {19.763, 34.911, 20.596}, {20.933, 34.375, 23.552}, {20.716, 36.447, 25.547}, {23.166, 34.785, 25.313}, {22.440, 36.603, 23.211}, {24.830, 35.069, 22.961}, {24.723, 37.182, 24.924}, {27.085, 35.603, 24.642}, {26.373, 37.407, 22.640}, {28.720, 35.767, 22.277}, {28.728, 37.939, 24.276}, {31.035, 36.248, 23.981}, {30.163, 38.179, 21.805}, {32.638, 36.468, 21.515}, {32.555, 38.726, 23.489}, {34.886, 37.034, 23.184}, {17.957, 34.682, 23.302}, {22.444, 38.828, 25.123}, {24.078, 39.155, 22.829}, {26.306, 39.552, 24.517}, {30.157, 40.478, 23.736}, {26.105, 12.825, 30.167}, {27.750, 12.928, 27.746}, {30.051, 13.365, 29.488}, {30.268, 11.527, 24.361}, {33.956, 14.181, 28.760}, {19.153, 15.212, 29.149}, {21.255, 16.036, 30.724}, {23.590, 14.316, 30.554}, {22.954, 16.232, 28.475}, {25.334, 14.634, 28.106}, {25.297, 16.703, 30.118}, {27.716, 15.008, 29.831}, {26.933, 16.975, 27.771}, {29.319, 15.254, 27.488}, {29.306, 17.372, 29.504}, {31.541, 15.794, 29.264}, {30.932, 17.608, 27.103}, {33.243, 16.130, 26.806}, {33.131, 18.106, 28.812}, {35.464, 16.601, 28.543}, {34.859, 18.440, 26.544}, {37.071, 18.894, 28.112}, {39.688, 23.355, 23.439}, {16.416, 19.263, 31.240}, {18.905, 17.556, 30.942}, {18.245, 19.492, 29.004}, {20.642, 17.757, 28.727}, {20.409, 19.916, 30.588}, {22.967, 18.446, 30.288}, {22.193, 20.133, 28.255}, {24.611, 18.586, 28.000}, {24.535, 20.699, 30.041}, {26.960, 18.939, 29.766}, {26.162, 20.904, 27.671}, {28.583, 19.258, 27.286}, {28.408, 21.359, 29.365}, {30.832, 19.764, 29.091}, {30.146, 21.556, 26.994}, {32.464, 20.013, 26.687}, {32.389, 22.169, 28.684}, {34.775, 20.632, 28.438}, {34.079, 22.460, 26.239}, {36.361, 20.862, 26.077}, {36.360, 22.910, 28.029}, {38.670, 21.327, 27.838}, {38.017, 23.167, 25.576}, {40.186, 23.754, 27.418}, {14.109, 20.811, 31.464}, {13.445, 22.906, 29.390}, {15.786, 21.221, 29.088}, {15.760, 23.329, 31.143}, {18.068, 21.618, 30.935}, {17.513, 23.476, 28.782}, {19.765, 21.804, 28.549}, {19.788, 23.925, 30.611}, {22.091, 22.230, 30.196}, {21.436, 24.164, 28.264}, {23.777, 22.544, 27.886}, {23.672, 24.705, 29.867}, {26.042, 23.051, 29.556}, {25.364, 24.968, 27.527}, {27.751, 23.227, 27.276}, {27.655, 25.376, 29.313}, {30.010, 23.785, 29.075}, {29.268, 25.633, 26.912}, {31.668, 24.032, 26.654}, {31.598, 26.137, 28.631}, {33.953, 24.440, 28.293}, {33.333, 26.452, 26.348}, {35.626, 24.842, 25.947}, {35.582, 26.874, 28.056}, {37.943, 25.282, 27.677}, {37.164, 27.186, 25.709}, {39.654, 25.613, 25.393}, {39.558, 27.667, 27.314}, {42.032, 26.010, 26.924}, {41.197, 27.914, 24.864}, {13.422, 24.952, 31.362}, {12.845, 26.646, 29.250}, {15.229, 25.297, 28.975}, {15.068, 27.275, 31.103}, {17.441, 25.632, 30.857}, {16.929, 27.523, 28.791}, {19.147, 25.885, 28.408}, {19.114, 27.948, 30.431}, {21.303, 26.324, 30.179}, {20.706, 28.210, 28.045}, {23.137, 26.523, 27.766}, {23.013, 28.746, 29.792}, {25.344, 27.040, 29.470}, {24.542, 29.012, 27.438}, {26.952, 27.285, 27.223}, {26.871, 29.386, 29.178}, {29.266, 27.894, 28.802}, {28.578, 29.636, 26.797}, {30.937, 28.007, 26.526}, {30.899, 30.090, 28.515}, {33.214, 28.528, 28.221}, {32.455, 30.402, 26.163}, {34.881, 28.774, 25.976}, {34.790, 30.864, 27.976}, {37.117, 29.288, 27.641}, {36.306, 31.077, 25.575}, {38.835, 29.566, 25.187}, {38.788, 31.538, 27.068}, {41.166, 30.029, 26.837}, {40.514, 29.928, 22.852}, {12.629, 28.951, 31.015}, {14.549, 29.035, 28.957}, {12.950, 31.496, 28.635}, {16.671, 29.589, 30.726}, {15.918, 31.440, 28.546}, {18.288, 29.844, 28.266}, {18.212, 31.907, 30.272}, {20.606, 30.339, 30.088}, {19.819, 32.193, 27.836}, {22.216, 30.586, 27.755}, {21.973, 32.682, 29.623}, {24.471, 31.048, 29.387}, {23.905, 32.834, 27.344}, {26.309, 31.377, 27.119}, {26.192, 33.291, 29.039}, {28.578, 31.826, 28.859}, {27.795, 33.731, 26.631}, {30.178, 32.079, 26.587}, {30.171, 34.223, 28.404}, {32.480, 32.511, 28.246}, {31.762, 34.436, 26.061}, {34.029, 32.782, 25.814}, {34.095, 34.899, 27.721}, {36.399, 33.201, 27.392}, {35.710, 35.280, 25.388}, {37.856, 33.406, 25.134}, {37.963, 35.756, 27.018}, {40.317, 36.056, 28.687}, {15.789, 33.497, 30.576}, {14.518, 33.956, 28.097}, {17.426, 33.756, 28.208}, {17.356, 35.941, 30.111}, {19.632, 34.315, 29.971}, {19.121, 36.121, 27.842}, {21.370, 34.493, 27.488}, {21.322, 36.702, 29.597}, {23.687, 35.039, 29.231}, {23.064, 36.899, 27.254}, {25.495, 35.251, 26.999}, {25.388, 37.459, 28.867}, {27.723, 35.781, 28.656}, {27.030, 37.723, 26.626}, {29.404, 36.060, 26.321}, {29.303, 38.272, 28.290}, {31.683, 36.613, 27.910}, {30.948, 38.449, 25.809}, {33.379, 36.842, 25.637}, {33.267, 39.059, 27.542}, {35.512, 37.273, 27.426}, {34.947, 39.198, 25.132}, {18.902, 38.373, 29.860}, {20.703, 38.551, 27.515}, {20.397, 40.641, 29.489}, {22.981, 39.041, 29.249}, {22.446, 40.948, 27.154}, {24.635, 39.331, 26.917}, {24.519, 41.527, 28.954}, {26.941, 39.986, 28.534}, {26.253, 41.842, 26.470}, {28.545, 40.164, 26.224}, {28.630, 42.193, 28.005}, {30.867, 40.584, 27.828}, {26.718, 12.902, 34.388}, {28.461, 13.117, 31.903}, {30.715, 13.525, 33.634}, {32.347, 13.885, 31.350}, {34.973, 14.259, 33.000}, {19.562, 15.690, 33.016}, {24.607, 12.262, 32.546}, {21.869, 16.215, 34.802}, {24.173, 14.520, 34.449}, {23.618, 16.394, 32.296}, {26.009, 14.740, 32.254}, {25.811, 16.908, 34.144}, {28.235, 15.224, 33.968}, {27.545, 17.101, 31.800}, {29.908, 15.472, 31.578}, {29.830, 17.636, 33.562}, {32.320, 15.978, 33.303}, {31.585, 17.902, 31.264}, {33.936, 16.230, 30.897}, {33.819, 18.452, 33.011}, {36.194, 16.875, 32.540}, {35.403, 18.677, 30.456}, {37.839, 17.115, 30.111}, {37.743, 19.246, 32.237}, {39.590, 19.509, 29.914}, {17.326, 17.399, 33.272}, {17.133, 19.398, 35.418}, {19.518, 17.812, 35.031}, {18.783, 19.646, 33.021}, {21.290, 18.090, 32.679}, {21.207, 20.076, 34.696}, {23.511, 18.587, 34.390}, {22.694, 20.477, 32.303}, {25.057, 18.839, 32.120}, {25.083, 20.980, 34.043}, {27.372, 19.288, 33.699}, {26.694, 21.156, 31.680}, {29.145, 19.519, 31.463}, {29.101, 21.676, 33.412}, {31.377, 19.973, 33.118}, {30.723, 21.910, 31.076}, {33.109, 20.229, 30.788}, {33.072, 22.341, 32.738}, {35.376, 20.788, 32.497}, {34.726, 22.665, 30.372}, {37.043, 21.040, 30.113}, {37.022, 23.109, 32.121}, {39.375, 21.491, 31.873}, {38.678, 23.410, 29.828}, {41.146, 22.035, 29.500}, {40.913, 24.010, 31.513}, {14.894, 21.015, 35.594}, {11.840, 22.231, 31.781}, {16.364, 21.267, 33.230}, {16.453, 23.408, 35.225}, {18.713, 21.825, 34.964}, {18.102, 23.718, 32.873}, {20.402, 21.910, 32.644}, {20.400, 24.058, 34.568}, {22.734, 22.555, 34.300}, {22.005, 24.379, 32.210}, {24.349, 22.839, 31.951}, {24.409, 24.896, 33.949}, {26.725, 23.228, 33.623}, {25.970, 25.081, 31.535}, {28.340, 23.496, 31.374}, {28.242, 25.702, 33.229}, {30.558, 23.960, 33.002}, {29.984, 25.847, 30.944}, {32.346, 24.250, 30.650}, {32.285, 26.314, 32.546}, {34.636, 24.768, 32.444}, {33.900, 26.647, 30.208}, {36.292, 25.031, 30.035}, {36.260, 27.155, 32.089}, {38.488, 25.484, 31.748}, {37.815, 27.391, 29.664}, {40.249, 25.847, 29.397}, {40.137, 27.913, 31.282}, {42.622, 26.266, 31.109}, {41.728, 28.125, 29.038}, {14.044, 22.969, 33.477}, {13.233, 27.070, 33.297}, {15.642, 25.299, 33.137}, {15.651, 27.418, 35.101}, {18.119, 25.786, 34.879}, {17.256, 27.662, 32.887}, {19.679, 26.088, 32.571}, {19.625, 28.204, 34.471}, {21.922, 26.535, 34.167}, {21.285, 28.472, 32.106}, {23.608, 26.676, 31.782}, {23.637, 28.822, 33.769}, {25.881, 27.184, 33.562}, {25.229, 29.060, 31.501}, {31.542, 28.277, 30.582}, {31.518, 30.374, 32.502}, {33.890, 28.776, 32.306}, {33.197, 30.649, 30.254}, {35.531, 29.024, 29.978}, {35.465, 31.139, 31.849}, {37.791, 29.572, 31.609}, {37.125, 31.452, 29.462}, {39.347, 29.774, 29.292}, {39.377, 31.887, 31.192}, {41.700, 30.252, 30.909}, {40.971, 32.187, 28.653}, {13.092, 28.995, 35.341}, {14.852, 29.463, 32.976}, {14.841, 31.254, 35.061}, {17.227, 29.758, 34.809}, {16.596, 31.617, 32.697}, {18.950, 30.068, 32.409}, {18.797, 32.108, 34.425}, {21.237, 30.496, 34.051}, {20.485, 32.457, 32.037}, {22.851, 30.815, 31.720}, {22.819, 32.978, 33.656}, {25.077, 31.298, 33.417}, {24.501, 33.129, 31.238}, {26.775, 31.445, 31.090}, {26.782, 33.655, 32.995}, {29.266, 31.986, 32.758}, {28.523, 33.983, 30.742}, {30.847, 32.288, 30.535}, {30.864, 34.503, 32.416}, {33.100, 32.806, 32.090}, {32.391, 34.793, 30.040}, {34.751, 33.100, 29.744}, {34.641, 35.220, 31.748}, {37.128, 33.535, 31.494}, {36.397, 35.437, 29.468}, {38.666, 33.813, 29.057}, {38.799, 35.874, 31.049}, {41.135, 34.131, 30.843}, {13.969, 33.093, 32.783}, {16.419, 33.713, 34.625}, {15.558, 35.531, 32.613}, {18.010, 33.976, 32.332}, {17.980, 36.138, 34.283}, {20.355, 34.508, 33.913}, {19.645, 36.341, 31.937}, {22.122, 34.809, 31.626}, {21.952, 36.901, 33.617}, {24.459, 35.189, 33.360}, {23.735, 37.082, 31.314}, {26.036, 35.499, 30.897}, {26.082, 37.653, 32.919}, {28.366, 36.123, 32.613}, {27.645, 37.913, 30.524}, {30.087, 36.393, 30.344}, {29.961, 38.524, 32.229}, {32.348, 36.841, 31.997}, {31.599, 38.704, 29.894}, {33.960, 37.098, 29.665}, {33.873, 39.274, 31.638}, {36.157, 37.538, 31.344}, {35.611, 39.495, 29.279}, {37.963, 37.766, 28.979}, {17.112, 38.013, 32.059}, {19.516, 38.573, 33.892}, {21.164, 38.677, 31.566}, {21.187, 40.870, 33.643}, {23.619, 39.209, 33.198}, {22.795, 41.096, 31.142}, {25.325, 39.524, 30.877}, {25.167, 41.687, 32.782}, {27.648, 40.101, 32.464}, {26.913, 42.004, 30.491}, {29.175, 40.335, 30.276}, {29.268, 42.515, 32.150}, {31.592, 40.793, 31.929}, {33.510, 41.363, 29.544}, {29.032, 13.376, 35.951}, {33.116, 13.981, 35.278}, {19.884, 15.768, 37.032}, {22.506, 16.279, 38.853}, {22.400, 14.029, 36.966}, {24.182, 16.515, 36.481}, {26.512, 14.933, 36.249}, {26.442, 17.155, 38.245}, {28.897, 15.584, 37.952}, {28.057, 17.378, 35.787}, {30.673, 15.714, 35.623}, {30.496, 17.928, 37.502}, {32.844, 16.226, 37.338}, {32.175, 18.216, 35.229}, {34.588, 16.563, 34.952}, {34.360, 18.649, 37.011}, {37.151, 14.923, 34.846}, {36.233, 18.970, 34.545}, {38.576, 19.383, 36.216}, {40.340, 19.567, 33.762}, {17.715, 21.662, 41.299}, {19.967, 18.070, 39.290}, {19.512, 19.955, 37.013}, {21.778, 18.136, 36.833}, {21.784, 20.307, 38.722}, {24.094, 18.712, 38.409}, {23.382, 20.617, 36.337}, {25.788, 19.127, 36.031}, {25.746, 21.127, 38.067}, {28.140, 19.487, 37.754}, {27.413, 21.425, 35.726}, {29.706, 19.816, 35.459}, {29.682, 21.957, 37.455}, {32.011, 20.307, 37.147}, {31.323, 22.159, 35.089}, {33.752, 20.586, 34.904}, {33.571, 22.743, 36.896}, {36.033, 20.990, 36.540}, {35.375, 22.852, 34.394}, {37.865, 21.343, 34.185}, {37.757, 23.346, 36.163}, {40.114, 21.742, 35.803}, {39.317, 23.692, 33.807}, {41.689, 24.304, 35.499}, {14.719, 23.120, 37.496}, {17.108, 21.519, 37.416}, {17.007, 23.653, 39.200}, {19.424, 22.081, 38.998}, {18.726, 23.902, 36.906}, {21.055, 22.270, 36.597}, {21.051, 24.326, 38.570}, {23.319, 22.783, 38.304}, {22.625, 24.623, 36.260}, {24.920, 22.949, 36.031}, {24.862, 25.061, 37.978}, {27.268, 23.489, 37.700}, {26.517, 25.292, 35.545}, {28.912, 23.775, 35.335}, {28.901, 25.882, 37.263}, {31.247, 24.238, 36.961}, {30.625, 26.112, 34.849}, {32.900, 24.491, 34.649}, {32.882, 26.607, 36.598}, {35.212, 24.976, 36.402}, {34.577, 26.875, 34.377}, {36.995, 25.152, 34.108}, {36.817, 27.385, 35.978}, {39.241, 25.781, 35.727}, {38.587, 27.677, 33.705}, {40.913, 26.179, 33.431}, {40.802, 28.173, 35.420}, {42.621, 28.543, 32.924}, {14.039, 25.091, 35.376}, {16.395, 25.612, 37.122}, {16.283, 27.639, 39.118}, {18.603, 25.865, 38.892}, {18.047, 27.943, 36.776}, {20.334, 26.257, 36.654}, {20.286, 28.376, 38.468}, {22.584, 26.767, 38.212}, {21.865, 28.652, 36.102}, {24.236, 26.968, 35.947}, {24.207, 29.041, 37.755}, {26.549, 27.346, 37.495}, {25.837, 29.403, 35.513}, {28.180, 27.710, 35.156}, {28.144, 29.865, 37.136}, {30.476, 28.168, 36.798}, {29.873, 30.183, 34.791}, {32.194, 28.515, 34.622}, {32.182, 30.708, 36.653}, {34.501, 29.075, 36.258}, {33.778, 30.895, 34.202}, {36.162, 29.266, 33.899}, {36.055, 31.392, 35.829}, {38.435, 29.775, 35.640}, {37.727, 31.660, 33.578}, {40.153, 30.010, 33.223}, {39.959, 32.119, 35.196}, {41.824, 32.222, 32.975}, {15.499, 29.378, 37.030}, {15.301, 31.433, 38.949}, {17.798, 30.013, 38.766}, {17.046, 31.859, 36.717}, {19.525, 30.300, 36.459}, {19.399, 32.392, 38.442}, {21.800, 30.849, 38.104}, {21.124, 32.687, 35.976}, {23.516, 30.955, 35.767}, {23.328, 32.993, 37.724}, {25.788, 31.496, 37.431}, {25.150, 33.336, 35.335}, {27.536, 31.760, 34.963}, {27.369, 33.753, 37.059}, {29.859, 32.300, 36.762}, {29.104, 34.148, 34.675}, {31.552, 32.527, 34.431}, {31.411, 34.679, 36.370}, {33.709, 33.009, 36.196}, {33.014, 34.947, 34.150}, {35.346, 33.361, 33.795}, {35.317, 35.345, 35.843}, {37.664, 33.782, 35.435}, {37.032, 35.741, 33.393}, {39.403, 33.939, 33.156}, {39.959, 34.310, 37.157}, {17.006, 33.975, 38.772}, {15.850, 38.445, 35.033}, {18.748, 34.175, 36.375}, {18.620, 36.343, 38.221}, {21.092, 34.817, 38.007}, {20.244, 36.624, 35.991}, {22.733, 35.043, 35.626}, {22.766, 37.136, 37.600}, {25.053, 35.528, 37.356}, {24.342, 37.434, 35.206}, {26.758, 35.709, 35.000}, {26.705, 37.941, 36.907}, {29.003, 36.194, 36.610}, {28.246, 38.173, 34.599}, {30.718, 36.665, 34.312}, {30.602, 38.639, 36.422}, {33.051, 37.058, 36.170}, {32.268, 39.014, 34.053}, {34.571, 37.337, 33.716}, {34.607, 39.491, 35.809}, {36.850, 37.680, 35.532}, {33.989, 41.237, 33.685}, {21.843, 39.829, 38.471}, {21.913, 39.018, 35.704}, {24.217, 39.588, 37.148}, {22.075, 41.856, 36.306}, {25.934, 39.867, 34.817}, {28.231, 40.285, 36.539}, {27.559, 42.226, 34.453}, {30.043, 40.545, 34.317}, {32.276, 41.085, 36.082}, {24.865, 16.864, 40.439}, {27.276, 17.204, 42.096}, {28.865, 17.612, 39.866}, {31.165, 16.030, 39.600}, {31.114, 18.134, 41.626}, {32.724, 18.336, 39.234}, {36.586, 19.071, 38.595}, {20.094, 20.279, 41.174}, {22.378, 18.397, 40.824}, {22.445, 20.467, 42.751}, {24.897, 19.055, 42.394}, {23.952, 20.835, 40.391}, {26.420, 19.196, 40.091}, {26.315, 21.439, 42.042}, {28.648, 19.647, 41.784}, {28.071, 21.674, 39.717}, {30.385, 19.976, 39.621}, {30.266, 22.126, 41.459}, {32.691, 20.448, 41.159}, {31.930, 22.365, 39.164}, {34.208, 20.818, 38.864}, {34.261, 22.887, 40.839}, {36.637, 21.241, 40.554}, {36.001, 23.085, 38.458}, {38.389, 21.457, 38.118}, {38.253, 23.651, 40.026}, {39.970, 23.918, 37.691}, {20.169, 22.307, 43.075}, {19.312, 24.094, 41.002}, {21.726, 22.493, 40.686}, {21.568, 24.789, 42.648}, {23.947, 22.993, 42.348}, {23.361, 24.907, 40.400}, {25.532, 23.198, 39.993}, {25.584, 25.330, 41.980}, {27.972, 23.793, 41.660}, {27.220, 25.710, 39.653}, {29.558, 24.064, 39.364}, {29.522, 26.164, 41.378}, {31.722, 24.540, 41.085}, {31.156, 26.487, 38.880}, {33.477, 24.686, 38.784}, {33.415, 26.946, 40.572}, {35.811, 25.326, 40.359}, {35.143, 27.128, 38.260}, {37.571, 25.480, 38.111}, {37.482, 27.664, 39.988}, {39.916, 25.997, 39.825}, {41.574, 26.258, 37.478}, {16.923, 25.570, 41.235}, {19.086, 26.349, 42.993}, {18.517, 27.988, 40.779}, {20.922, 26.445, 40.588}, {20.750, 28.629, 42.481}, {23.133, 27.067, 42.298}, {22.533, 28.866, 40.212}, {24.873, 27.193, 39.901}, {24.871, 29.371, 41.890}, {27.151, 27.758, 41.579}, {26.424, 29.613, 39.534}, {28.760, 27.934, 39.219}, {28.737, 30.070, 41.151}, {31.065, 28.503, 40.913}, {30.352, 30.331, 38.826}, {32.728, 28.760, 38.485}, {32.687, 30.703, 40.505}, {35.162, 29.214, 40.342}, {34.446, 31.060, 38.243}, {36.750, 29.463, 37.971}, {36.758, 31.591, 39.941}, {39.225, 27.939, 37.764}, {38.358, 31.891, 37.519}, {40.904, 30.303, 37.370}, {18.383, 30.152, 42.782}, {17.601, 32.108, 40.790}, {20.065, 30.386, 40.366}, {20.058, 32.427, 42.358}, {22.526, 31.009, 41.964}, {21.740, 32.901, 40.066}, {24.227, 31.164, 39.713}, {24.137, 33.374, 41.561}, {26.384, 31.692, 41.454}, {25.635, 33.592, 39.383}, {27.947, 31.994, 39.164}, {27.957, 34.094, 41.099}, {30.274, 32.456, 40.849}, {29.705, 34.399, 38.767}, {31.997, 32.763, 38.657}, {31.981, 34.869, 40.517}, {34.390, 33.152, 40.283}, {33.680, 34.997, 38.177}, {35.997, 33.448, 37.890}, {37.632, 37.870, 39.347}, {38.316, 33.843, 39.612}, {37.568, 35.663, 37.613}, {19.454, 34.591, 40.216}, {21.777, 34.927, 41.981}, {20.908, 36.993, 40.018}, {23.312, 35.320, 39.608}, {24.253, 34.081, 45.458}, {25.514, 35.819, 41.227}, {25.008, 37.670, 39.218}, {27.346, 35.904, 38.994}, {27.282, 38.094, 40.957}, {29.453, 36.490, 40.759}, {28.930, 38.323, 38.661}, {31.286, 36.805, 38.533}, {30.459, 41.012, 38.273}, {33.557, 37.247, 40.207}, {32.765, 39.228, 38.108}, {35.162, 37.401, 37.845}, {26.393, 40.013, 38.945}, {28.587, 21.718, 43.714}, {30.855, 20.210, 43.314}, {32.575, 22.700, 43.005}, {36.717, 23.426, 42.414}, {22.562, 22.208, 45.225}, {26.397, 23.505, 43.963}, {26.300, 25.640, 45.921}, {28.439, 23.732, 45.930}, {27.844, 25.976, 43.647}, {30.228, 24.235, 43.438}, {30.189, 26.276, 45.369}, {34.217, 27.125, 44.697}, {31.893, 26.609, 42.999}, {34.099, 25.051, 42.635}, {35.846, 29.545, 44.293}, {35.792, 27.322, 42.325}, {23.965, 25.051, 44.340}, {21.466, 26.934, 44.589}, {25.379, 27.482, 43.976}, {24.112, 27.566, 46.797}, {27.592, 28.172, 45.636}, {27.115, 29.892, 43.553}, {29.529, 28.280, 43.308}, {29.433, 30.438, 45.156}, {31.755, 28.683, 44.987}, {31.052, 30.704, 42.957}, {33.465, 29.051, 42.693}, {33.542, 31.189, 44.696}, {35.104, 31.354, 42.234}, {37.579, 29.755, 42.045}, {23.072, 29.197, 44.264}, {24.855, 31.529, 43.788}, {27.200, 32.058, 45.529}, {26.332, 33.917, 43.403}, {28.655, 32.286, 43.143}, {30.955, 32.865, 44.933}, {30.340, 34.687, 42.787}, {32.738, 32.862, 42.523}, {36.032, 35.459, 39.925}, {27.782, 36.249, 43.060}};
    //const int NAU = 884;

    point au[] = {{40.00000,  40.00000,  40.00000},{40.00000,  42.03148,  42.03148},{42.03148,  40.00000,  42.03148},{42.03148,  42.03148,  40.00000},{35.93839,  33.90112,  33.90112},{40.00000,  33.89970,  33.89970},{44.06161,  33.90112,  33.90112},{33.90112,  35.93839,  33.90112},{35.93646,  35.93646,  31.86571},{35.93678,  37.96931,  33.90408},{37.96931,  35.93678,  33.90408},{37.96930,  37.96930,  31.86646},{40.00000,  35.93251,  31.86554},{40.00000,  37.96886,  33.90434},{42.03069,  35.93678,  33.90408},{42.03070,  37.96930,  31.86646},{44.06354,  35.93646,  31.86571},{44.06322,  37.96931,  33.90408},{46.09888,  35.93839,  33.90112},{33.89970,  40.00000,  33.89970},{35.93251,  40.00000,  31.86554},{35.93678,  42.03069,  33.90408},{37.96886,  40.00000,  33.90434},{37.96930,  42.03070,  31.86646},{40.00000,  40.00000,  31.86619},{40.00000,  42.03114,  33.90434},{42.03114,  40.00000,  33.90434},{42.03070,  42.03070,  31.86646},{44.06749,  40.00000,  31.86554},{44.06322,  42.03069,  33.90408},{46.10030,  40.00000,  33.89970},{33.90112,  44.06161,  33.90112},{35.93646,  44.06354,  31.86571},{35.93839,  46.09888,  33.90112},{37.96931,  44.06322,  33.90408},{40.00000,  44.06749,  31.86554},{40.00000,  46.10030,  33.89970},{42.03069,  44.06322,  33.90408},{44.06354,  44.06354,  31.86571},{44.06161,  46.09888,  33.90112},{46.09888,  44.06161,  33.90112},{33.90112,  33.90112,  35.93839},{35.93646,  31.86571,  35.93646},{35.93678,  33.90408,  37.96931},{37.96930,  31.86646,  37.96930},{37.96931,  33.90408,  35.93678},{40.00000,  31.86554,  35.93251},{40.00000,  33.90434,  37.96886},{42.03070,  31.86646,  37.96930},{42.03069,  33.90408,  35.93678},{44.06354,  31.86571,  35.93646},{44.06322,  33.90408,  37.96931},{46.09888,  33.90112,  35.93839},{31.86571,  35.93646,  35.93646},{31.86646,  37.96930,  37.96930},{33.90408,  35.93678,  37.96931},{33.90408,  37.96931,  35.93678},{35.93692,  35.93692,  35.93692},{35.93692,  37.96861,  37.96861},{37.96861,  35.93692,  37.96861},{37.96861,  37.96861,  35.93692},{40.00000,  35.93697,  35.93697},{40.00000,  37.96852,  37.96852},{42.03139,  35.93692,  37.96861},{42.03139,  37.96861,  35.93692},{44.06308,  35.93692,  35.93692},{44.06308,  37.96861,  37.96861},{46.09592,  35.93678,  37.96931},{46.09592,  37.96931,  35.93678},{48.13429,  35.93646,  35.93646},{48.13354,  37.96930,  37.96930},{31.86554,  40.00000,  35.93251},{31.86646,  42.03070,  37.96930},{33.90434,  40.00000,  37.96886},{33.90408,  42.03069,  35.93678},{35.93697,  40.00000,  35.93697},{35.93692,  42.03139,  37.96861},{37.96852,  40.00000,  37.96852},{37.96861,  42.03139,  35.93692},{40.00000,  40.00000,  35.93687},{40.00000,  42.03148,  37.96852},{42.03148,  40.00000,  37.96852},{42.03139,  42.03139,  35.93692},{44.06303,  40.00000,  35.93697},{44.06308,  42.03139,  37.96861},{46.09566,  40.00000,  37.96886},{46.09592,  42.03069,  35.93678},{48.13446,  40.00000,  35.93251},{48.13354,  42.03070,  37.96930},{31.86571,  44.06354,  35.93646},{33.90408,  44.06322,  37.96931},{33.90112,  46.09888,  35.93839},{35.93692,  44.06308,  35.93692},{35.93678,  46.09592,  37.96931},{37.96861,  44.06308,  37.96861},{37.96931,  46.09592,  35.93678},{40.00000,  44.06303,  35.93697},{40.00000,  46.09566,  37.96886},{42.03139,  44.06308,  37.96861},{42.03069,  46.09592,  35.93678},{44.06308,  44.06308,  35.93692},{44.06322,  46.09592,  37.96931},{46.09592,  44.06322,  37.96931},{46.09888,  46.09888,  35.93839},{48.13429,  44.06354,  35.93646},{35.93646,  48.13429,  35.93646},{37.96930,  48.13354,  37.96930},{40.00000,  48.13446,  35.93251},{42.03070,  48.13354,  37.96930},{44.06354,  48.13429,  35.93646},{33.89970,  33.89970,  40.00000},{35.93251,  31.86554,  40.00000},{35.93678,  33.90408,  42.03069},{37.96930,  31.86646,  42.03070},{37.96886,  33.90434,  40.00000},{40.00000,  31.86619,  40.00000},{40.00000,  33.90434,  42.03114},{42.03070,  31.86646,  42.03070},{42.03114,  33.90434,  40.00000},{44.06749,  31.86554,  40.00000},{44.06322,  33.90408,  42.03069},{46.10030,  33.89970,  40.00000},{31.86554,  35.93251,  40.00000},{31.86646,  37.96930,  42.03070},{33.90408,  35.93678,  42.03069},{33.90434,  37.96886,  40.00000},{35.93697,  35.93697,  40.00000},{35.93692,  37.96861,  42.03139},{37.96861,  35.93692,  42.03139},{37.96852,  37.96852,  40.00000},{40.00000,  35.93687,  40.00000},{40.00000,  37.96852,  42.03148},{42.03139,  35.93692,  42.03139},{42.03148,  37.96852,  40.00000},{44.06303,  35.93697,  40.00000},{44.06308,  37.96861,  42.03139},{46.09592,  35.93678,  42.03069},{46.09566,  37.96886,  40.00000},{48.13446,  35.93251,  40.00000},{48.13354,  37.96930,  42.03070},{31.86619,  40.00000,  40.00000},{31.86646,  42.03070,  42.03070},{33.90434,  40.00000,  42.03114},{33.90434,  42.03114,  40.00000},{35.93687,  40.00000,  40.00000},{35.93692,  42.03139,  42.03139},{37.96852,  40.00000,  42.03148},{37.96852,  42.03148,  40.00000},{44.06313,  40.00000,  40.00000},{44.06308,  42.03139,  42.03139},{46.09566,  40.00000,  42.03114},{46.09566,  42.03114,  40.00000},{48.13381,  40.00000,  40.00000},{48.13354,  42.03070,  42.03070},{31.86554,  44.06749,  40.00000},{33.90408,  44.06322,  42.03069},{33.89970,  46.10030,  40.00000},{35.93697,  44.06303,  40.00000},{35.93678,  46.09592,  42.03069},{37.96861,  44.06308,  42.03139},{37.96886,  46.09566,  40.00000},{40.00000,  44.06313,  40.00000},{40.00000,  46.09566,  42.03114},{42.03139,  44.06308,  42.03139},{42.03114,  46.09566,  40.00000},{44.06303,  44.06303,  40.00000},{44.06322,  46.09592,  42.03069},{46.09592,  44.06322,  42.03069},{46.10030,  46.10030,  40.00000},{48.13446,  44.06749,  40.00000},{35.93251,  48.13446,  40.00000},{37.96930,  48.13354,  42.03070},{40.00000,  48.13381,  40.00000},{42.03070,  48.13354,  42.03070},{44.06749,  48.13446,  40.00000},{33.90112,  33.90112,  44.06161},{35.93646,  31.86571,  44.06354},{35.93839,  33.90112,  46.09888},{37.96931,  33.90408,  44.06322},{40.00000,  31.86554,  44.06749},{40.00000,  33.89970,  46.10030},{42.03069,  33.90408,  44.06322},{44.06354,  31.86571,  44.06354},{44.06161,  33.90112,  46.09888},{46.09888,  33.90112,  44.06161},{31.86571,  35.93646,  44.06354},{33.90112,  35.93839,  46.09888},{33.90408,  37.96931,  44.06322},{35.93692,  35.93692,  44.06308},{35.93678,  37.96931,  46.09592},{37.96931,  35.93678,  46.09592},{37.96861,  37.96861,  44.06308},{40.00000,  35.93697,  44.06303},{40.00000,  37.96886,  46.09566},{42.03069,  35.93678,  46.09592},{42.03139,  37.96861,  44.06308},{44.06308,  35.93692,  44.06308},{44.06322,  37.96931,  46.09592},{46.09888,  35.93839,  46.09888},{46.09592,  37.96931,  44.06322},{48.13429,  35.93646,  44.06354},{31.86554,  40.00000,  44.06749},{33.89970,  40.00000,  46.10030},{33.90408,  42.03069,  44.06322},{35.93697,  40.00000,  44.06303},{35.93678,  42.03069,  46.09592},{37.96886,  40.00000,  46.09566},{37.96861,  42.03139,  44.06308},{40.00000,  40.00000,  44.06313},{40.00000,  42.03114,  46.09566},{42.03114,  40.00000,  46.09566},{42.03139,  42.03139,  44.06308},{44.06303,  40.00000,  44.06303},{44.06322,  42.03069,  46.09592},{46.10030,  40.00000,  46.10030},{46.09592,  42.03069,  44.06322},{48.13446,  40.00000,  44.06749},{31.86571,  44.06354,  44.06354},{33.90112,  44.06161,  46.09888},{33.90112,  46.09888,  44.06161},{35.93692,  44.06308,  44.06308},{35.93839,  46.09888,  46.09888},{37.96931,  44.06322,  46.09592},{37.96931,  46.09592,  44.06322},{40.00000,  44.06303,  44.06303},{40.00000,  46.10030,  46.10030},{42.03069,  44.06322,  46.09592},{42.03069,  46.09592,  44.06322},{44.06308,  44.06308,  44.06308},{44.06161,  46.09888,  46.09888},{46.09888,  44.06161,  46.09888},{46.09888,  46.09888,  44.06161},{48.13429,  44.06354,  44.06354},{35.93646,  48.13429,  44.06354},{40.00000,  48.13446,  44.06749},{44.06354,  48.13429,  44.06354},{35.93646,  35.93646,  48.13429},{37.96930,  37.96930,  48.13354},{40.00000,  35.93251,  48.13446},{42.03070,  37.96930,  48.13354},{44.06354,  35.93646,  48.13429},{35.93251,  40.00000,  48.13446},{37.96930,  42.03070,  48.13354},{40.00000,  40.00000,  48.13381},{42.03070,  42.03070,  48.13354},{44.06749,  40.00000,  48.13446},{35.93646,  44.06354,  48.13429},{40.00000,  44.06749,  48.13446}};
    const int NAU = 248;

    const double w2 = 0.03;
    const double nu = 2*w2;
    const double ww = nu;



    const double kw   = 0.03; // primitive coefficient
    const double Kww = kw*kw/(kw+kw); //
    const double K2   = 2*kw; // density
    const double K22  = K2*K2/(K2+K2);

    const double K3w  = kw*kw/(kw+kw+K2);
    const double K32  = kw*K2/(kw+kw+K2);



    // test other own T3 decomposition
    // ===============================

    if (1) {

        // find out real rank of the basis
        tensor2 Sd(NAU), Sdd(NAU);

        {char a; cin>>a;}

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                Sd(i,j) = Sd(j,i) = -exp(-Kww*r2);
            }
            Sd(i,i) = -1;
        }

        Sdd = Sd;

        double vd[NAU];

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int SIZE = NAU;
            int LWORK = SIZE*SIZE;  // size of working space
            double * WORK = new double[LWORK];   // nothing

            dsyev_ (&JOBZ, &UL, &SIZE, Sd.c2, &SIZE, vd, WORK, &LWORK, &INFO);
            for (int i=0; i<SIZE; ++i) vd[i] = -vd[i];
            for (int i=0; i<SIZE; ++i) cout << vd[i] << " "; cout << endl << endl;
        }

        int MAU=NAU; // for (MAU=0; MAU<NAU; ++MAU) if (vd[MAU]<vd[0]*1e-8) break;

        for (int i=0; i<MAU; ++i) vd[i] = 1./sqrt(vd[i]);

        tensor2 Ld(MAU,NAU);

        // this probably has lower rank
        for (int k=0; k<MAU; ++k) {
            for (int q=0; q<NAU; ++q) {
                Ld(k,q)=vd[k]*Sd(k,q);
            }
        }

        cout << "MAU, NAU : " << MAU << " " << NAU << endl;



        IncompleteGamma.InitIncompleteGammaTable();


        //tensor3 X(NAU,  MAU,MAU); // 884*884*884*4*8
        tensor2 XX[NAU];
        for (int k=0; k<NAU; ++k) XX[k].setsize(MAU);

        const double K11 = sqrt(K22);
        const double KV  = K11;

        #pragma omp parallel for
        for (int k=0; k<NAU; ++k) {
            if (k%10==0) cout << k<<endl;

            for (int i=0; i<NAU; ++i) {
                for (int j=0; j<=i; ++j) {

                    double px = 0.5*(au[i].x + au[j].x);
                    double py = 0.5*(au[i].y + au[j].y);
                    double pz = 0.5*(au[i].z + au[j].z);

                    double K = -Sdd(i,j);

                    double xx = px - au[k].x;
                    double yy = py - au[k].y;
                    double zz = pz - au[k].z;

                    double r2 = xx*xx+yy*yy+zz*zz;

                    //double rr = sqrt(xx*xx+yy*yy+zz*zz);
                    //V(i,j)=V(j,i)=K * erf(K11 * rr)/rr;

                    XX[k](i,j)=XX[k](j,i)= K * KV * IncompleteGamma.calcF2(K22*r2);
                }
            }
        }

        cout << "done"<< endl;

        //{char a; cin>>a;}

        {
            const int N = MAU;
            const int K = NAU;
            int R;

            tensor2 RR(K*N,K);
            tensor2 LL(K*N,N);
            double  ll[N*K];

            // generates a reasonable initial set for the ij 1-rank basis
            IguessBase(RR,LL,ll, XX, N,K,R);
            cout << "done"<< endl;

            tensor2 SS(R);

            tensor2 UU(R,N);
            tensor2 VV(R,N);
            tensor2 WW(R,K);
            double  nn[R];

            // copy it
            for (int r=0;r<R;++r)
                for (int n=0;n<N;++n)
                    VV(r,n) = UU(r,n) = LL(r,n);

            int it=0;

            while (true) {
                ++it;
                cout << "step " << it << endl;

                // changed function arguments
                /*
                ContractIJ (WW,   XX,UU,VV,N,K,R);
                //ContractIJ (WW, RR,LL,ll,   UU,VV,N,K,R);
                BuildS2   (SS, UU, VV, N,N,R);
                SolveALS (nn, WW, SS, K,R);

                CheckNorm(XX, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);

                ContractIK (UU,   XX,VV,WW,N,K,R);
                //ContractIK (UU, RR,LL,ll,  VV,WW,N,K,R);
                BuildS2   (SS, VV, WW, N,K,R);
                SolveALS (nn, UU, SS, N,R);

                CheckNorm(XX, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);

                ContractIK (VV,   XX,UU,WW,N,K,R);
                //ContractIK (VV, RR,LL,ll, UU,WW,N,K,R);
                BuildS2   (SS, UU, WW, N,K,R);
                SolveALS (nn, VV, SS, N,R);

                CheckNorm(XX, nn, UU, VV, WW, N,K,R);
                //CheckNorm(XX, RR,LL,ll, nn,UU,VV, WW, N,K,R);
                */
            }




        }

        /*


        tensor2 LL(MAU*NAU,MAU);
        double  ll[MAU*NAU];
        int R; // = 12*MAU;

        // generates a reasonable initial set for the ij 1-rank basis
        IguessBase(LL,ll, XX, MAU,NAU, R);
        cout << "done"<< endl;

        tensor2 SS(R);
        BuildOverlap (XX, SS, LL, ll, MAU, R);
        SolveALS1(XX, SS, LL, ll, MAU, NAU, R);
        cout << "done"<< endl;

        //{char a; cin>>a;}
        cout << "find linear dependencies"<< endl;
        MakeKMatrices(XX, SS, LL, ll, MAU, R);
        cout << "done"<< endl;
        //{char a; cin>>a;}
        */

        /*
        cout << "rebasing"<< endl;
        Rebase(LL,ll, XX,MAU,NAU);
        cout << "done"<< endl;
        {char a; cin>>a;}
        */


        return 0;
    }


    // test PARAFAC / TUCKER3

    // iteration 2395 ~10N -> Frobenius^2 = 0.001283

    if (1) {

        // find out real rank of the basis
        tensor2 Sd(NAU), Sdd(NAU);

        {char a; cin>>a;}

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                Sd(i,j) = Sd(j,i) = -exp(-Kww*r2);
            }
            Sd(i,i) = -1;
        }

        Sdd = Sd;

        double vd[NAU];

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int SIZE = NAU;
            int LWORK = SIZE*SIZE;  // size of working space
            double * WORK = new double[LWORK];   // nothing

            dsyev_ (&JOBZ, &UL, &SIZE, Sd.c2, &SIZE, vd, WORK, &LWORK, &INFO);
            for (int i=0; i<SIZE; ++i) vd[i] = -vd[i];
            for (int i=0; i<SIZE; ++i) cout << vd[i] << " "; cout << endl << endl;
        }

        int MAU=NAU; // for (MAU=0; MAU<NAU; ++MAU) if (vd[MAU]<vd[0]*1e-8) break;

        for (int i=0; i<MAU; ++i) vd[i] = 1./sqrt(vd[i]);

        tensor2 Ld(MAU,NAU);

        // this probably has lower rank
        for (int k=0; k<MAU; ++k) {
            for (int q=0; q<NAU; ++q) {
                Ld(k,q)=vd[k]*Sd(k,q);
            }
        }

        cout << "MAU, NAU : " << MAU << " " << NAU << endl;



        IncompleteGamma.InitIncompleteGammaTable();


        tensor3 X(NAU,  MAU,MAU); // 884*884*884*4*8

        const double K11 = sqrt(K22);
        const double KV  = K11;

        #pragma omp parallel for
        for (int k=0; k<NAU; ++k) {
            if (k%10==0) cout << k<<endl;

            tensor2 V(NAU,NAU);

            for (int i=0; i<NAU; ++i) {
                for (int j=0; j<=i; ++j) {

                    double px = 0.5*(au[i].x + au[j].x);
                    double py = 0.5*(au[i].y + au[j].y);
                    double pz = 0.5*(au[i].z + au[j].z);

                    double K = -Sdd(i,j);

                    double xx = px - au[k].x;
                    double yy = py - au[k].y;
                    double zz = pz - au[k].z;

                    double r2 = xx*xx+yy*yy+zz*zz;

                    //double rr = sqrt(xx*xx+yy*yy+zz*zz);
                    //V(i,j)=V(j,i)=K * erf(K11 * rr)/rr;

                    V(i,j)=V(j,i)= K * KV * IncompleteGamma.calcF2(K22*r2);
                }
            }

            /*
            // transform
            for (int i=0; i<MAU; ++i) {
                double cc[NAU];
                for (int l=0; l<NAU; ++l) {
                    double ww=0;
                    for (int k=0; k<NAU; ++k) ww+=Ld(i,k)*V(k,l);
                    cc[l] = ww;
                }
                for (int j=0; j<MAU; ++j) {
                    double jj=0;
                    for (int l=0; l<NAU; ++l) jj+=Ld(j,l)*cc[l];
                    X(k,  i,j) = jj;
                }
            }
            */

            for (int i=0; i<MAU; ++i)
                for (int j=0; j<MAU; ++j)
                    X(k,  i,j) = V(i,j);
        }

        cout << "done"<< endl;

        {char a; cin>>a;}

        const bool ComputeSVD = true;
        if (ComputeSVD) {
            tensor2 W(NAU,NAU);

            for (int k=0; k<NAU; ++k) {
                for (int l=0; l<=k; ++l) {
                    double ss  = 0;

                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<i; ++j)
                            ss += X(k,  i,j)*X(l,  i,j);
                    ss*=2;
                    for (int i=0; i<MAU; ++i) ss+=X(k,  i,i)*X(l,  i,i);

                    W(k,l) = W(l,k) = -ss;
                }
            }

            double e[NAU];

                char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
                char UL   = 'U'; // matrix storage pàtern
                int INFO;        // chack status
                int LWORK = NAU*NAU;  // size of working space
                double * WORK = new double[LWORK];   // nothing
                int SIZE = NAU;

                dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, e, WORK, &LWORK, &INFO);

            cout << "SVD^2  = ";  for (int i=0; i<NAU; ++i) cout << -e[i] << " "; cout << endl << endl;

            cout << "rotating SVD";

            for (int i=0; i<MAU; ++i)
                for (int j=0; j<=i; ++j) {
                    double ww[NAU];
                    for (int k=0; k<NAU; ++k) {
                        double ss = 0;
                        for (int l=0; l<NAU; ++l) {
                            ss+=W(k,l)*X(l,i,j);
                        }
                        ww[k] = ss;
                    }
                   for (int k=0; k<NAU; ++k) X(k, i,j) = ww[k];
            }

            for (int k=0; k<NAU; ++k)
                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<=i; ++j)
                        X(k, j,i) = X(k, i,j);



            {char a; cin>>a;}

        }


        // test eigenvalues
        // ================

        {
            // this is surprisingly fast; even if we end up using some HOSVD,
            // the regular rank decomposition may speed up the computationChe

            tensor2 E(NAU,NAU);
            double e[NAU];

            for (int k=0; k<NAU; ++k) {

                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<MAU; ++j)
                        E(i,j) = -X(k,  i,j);

                char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
                char UL   = 'U'; // matrix storage pàtern
                int INFO;        // chack status
                int LWORK = NAU*NAU;  // size of working space
                double * WORK = new double[LWORK];   // nothing
                int SIZE = NAU;

                dsyev_ (&JOBZ, &UL, &SIZE, E.c2, &SIZE, e, WORK, &LWORK, &INFO);

                cout << k << " :: "; for (int i=0; i<NAU; ++i) cout << -e[i] << " "; cout << endl << endl;
            }

            return 0;
        }


    }


    return 0;
}



int mainnn() {

    // already in bohr
    //point au[] = {{27.645, 27.554, 31.144}, {27.495, 29.559, 33.208}, {29.803, 28.000, 32.892}, {29.281, 29.869, 30.789}, {21.984, 23.877, 17.795}, {26.615, 20.414, 17.047}, {25.708, 24.593, 17.184}, {28.103, 22.904, 16.788}, {29.716, 25.476, 16.435}, {19.619, 25.541, 17.475}, {23.484, 26.248, 17.350}, {24.966, 28.724, 17.082}, {27.272, 27.026, 16.860}, {29.121, 29.425, 16.375}, {22.454, 30.607, 17.252}, {26.543, 30.993, 16.641}, {23.876, 16.164, 21.999}, {27.942, 16.894, 21.453}, {31.939, 17.453, 20.640}, {19.306, 19.469, 22.678}, {21.629, 17.755, 22.250}, {20.971, 19.760, 20.375}, {23.315, 20.087, 21.970}, {25.655, 18.524, 21.816}, {24.959, 20.363, 19.548}, {27.311, 18.607, 19.289}, {27.251, 20.866, 21.333}, {29.638, 19.220, 21.023}, {28.834, 21.082, 19.033}, {31.184, 21.735, 20.705}, {33.599, 19.889, 20.325}, {32.772, 21.886, 17.949}, {35.126, 22.348, 20.002}, {17.036, 21.184, 22.881}, {18.642, 23.388, 22.353}, {20.933, 21.878, 22.241}, {20.239, 23.694, 20.068}, {22.714, 22.059, 19.957}, {22.545, 24.154, 21.833}, {24.926, 22.481, 21.526}, {24.266, 24.420, 19.567}, {26.523, 22.786, 19.249}, {26.323, 24.939, 21.276}, {28.699, 23.363, 20.842}, {28.060, 25.108, 18.801}, {30.420, 23.458, 18.564}, {30.299, 25.580, 20.423}, {32.769, 24.126, 20.215}, {32.036, 26.055, 18.040}, {34.214, 26.537, 19.870}, {36.640, 24.835, 19.619}, {16.424, 25.082, 22.658}, {17.842, 25.402, 20.254}, {17.898, 27.472, 22.315}, {20.081, 25.927, 22.065}, {19.421, 27.804, 19.829}, {21.849, 26.003, 19.670}, {21.776, 28.307, 21.593}, {24.050, 26.551, 21.356}, {23.289, 28.500, 19.274}, {25.714, 26.817, 19.152}, {25.669, 28.968, 21.097}, {27.925, 27.314, 20.804}, {27.291, 29.277, 18.765}, {29.595, 27.581, 18.470}, {29.588, 29.675, 20.385}, {31.876, 28.110, 20.063}, {31.284, 30.014, 18.051}, {33.737, 28.382, 17.859}, {33.482, 30.512, 19.893}, {35.777, 29.036, 19.517}, {14.970, 27.421, 21.843}, {16.541, 30.017, 21.573}, {19.414, 29.950, 21.953}, {21.044, 30.162, 19.691}, {20.946, 32.348, 21.550}, {23.379, 30.574, 21.384}, {22.750, 32.503, 19.359}, {24.966, 30.898, 19.080}, {24.941, 32.980, 20.931}, {27.324, 31.335, 20.802}, {26.517, 33.217, 18.688}, {28.822, 31.611, 18.349}, {28.855, 33.581, 20.383}, {31.161, 32.046, 20.078}, {30.433, 33.977, 17.910}, {32.635, 34.401, 19.687}, {35.138, 32.821, 19.378}, {18.163, 32.481, 21.229}, {20.014, 32.692, 18.507}, {22.606, 34.602, 21.179}, {24.125, 34.907, 18.877}, {24.176, 37.063, 20.811}, {26.402, 35.300, 20.555}, {27.928, 37.695, 20.033}, {30.302, 35.980, 19.962}, {20.646, 15.672, 26.717}, {23.034, 13.981, 26.539}, {22.290, 15.852, 24.489}, {24.647, 16.471, 26.064}, {27.011, 14.753, 25.791}, {26.287, 16.634, 23.810}, {28.740, 14.950, 23.418}, {28.643, 17.060, 25.433}, {30.992, 15.417, 25.152}, {30.436, 17.281, 22.974}, {32.579, 17.825, 24.732}, {32.680, 15.572, 22.853}, {34.231, 18.067, 22.464}, {36.495, 18.474, 24.163}, {14.655, 21.383, 24.677}, {18.342, 17.345, 27.099}, {17.498, 19.227, 24.925}, {19.995, 17.580, 24.677}, {19.856, 19.654, 26.589}, {22.216, 18.015, 26.346}, {21.594, 19.952, 24.289}, {23.896, 18.309, 24.028}, {23.895, 20.477, 25.961}, {26.259, 18.777, 25.675}, {25.549, 20.668, 23.647}, {27.975, 18.983, 23.390}, {27.787, 21.264, 25.314}, {30.267, 19.453, 24.979}, {29.506, 21.444, 22.959}, {31.899, 19.644, 22.650}, {31.780, 21.888, 24.579}, {34.197, 20.211, 24.346}, {33.450, 22.155, 22.356}, {35.759, 20.539, 22.054}, {35.748, 22.666, 23.987}, {38.041, 21.011, 23.767}, {37.393, 22.996, 21.737}, {39.100, 25.322, 21.511}, {15.196, 23.214, 27.062}, {17.485, 21.354, 26.832}, {17.010, 23.279, 24.801}, {19.296, 21.579, 24.617}, {19.158, 23.752, 26.497}, {21.592, 22.017, 26.306}, {20.803, 23.997, 24.211}, {23.283, 22.307, 23.825}, {23.193, 24.422, 25.805}, {25.522, 22.807, 25.586}, {24.803, 24.708, 23.547}, {27.107, 23.034, 23.230}, {27.023, 25.182, 25.316}, {29.439, 23.522, 24.943}, {28.684, 25.487, 22.920}, {31.039, 23.824, 22.560}, {30.921, 25.930, 24.547}, {33.405, 24.238, 24.226}, {32.559, 26.162, 22.225}, {35.040, 24.609, 21.990}, {34.878, 26.751, 23.929}, {37.341, 25.138, 23.731}, {36.530, 26.984, 21.588}, {38.833, 27.487, 23.275}, {14.577, 24.888, 24.753}, {14.501, 26.937, 26.914}, {16.812, 25.336, 26.667}, {16.211, 27.186, 24.484}, {18.591, 25.663, 24.409}, {18.464, 27.761, 26.308}, {20.789, 26.058, 26.068}, {20.142, 28.097, 24.021}, {22.317, 26.342, 23.763}, {22.358, 28.485, 25.691}, {24.621, 26.893, 25.504}, {24.029, 28.585, 23.357}, {26.346, 27.125, 23.196}, {26.300, 29.134, 25.204}, {28.697, 27.546, 24.907}, {27.984, 29.389, 22.846}, {30.356, 27.810, 22.490}, {30.211, 29.980, 24.514}, {32.581, 28.281, 24.281}, {31.827, 30.177, 22.196}, {34.156, 28.546, 21.811}, {34.166, 30.711, 23.819}, {36.468, 28.966, 23.602}, {35.796, 30.932, 21.571}, {38.103, 29.302, 21.202}, {38.082, 31.425, 23.303}, {13.139, 29.450, 26.370}, {16.149, 29.276, 26.518}, {14.943, 29.798, 23.964}, {17.731, 29.588, 24.188}, {17.686, 31.708, 26.143}, {20.041, 30.109, 25.904}, {19.325, 31.993, 23.828}, {21.796, 30.429, 23.659}, {21.628, 32.434, 25.634}, {24.009, 30.757, 25.435}, {23.351, 32.745, 23.295}, {25.687, 31.053, 23.120}, {25.546, 33.194, 24.969}, {27.871, 31.552, 24.678}, {27.193, 33.461, 22.709}, {29.505, 31.870, 22.367}, {29.462, 33.885, 24.456}, {31.780, 32.215, 24.092}, {31.107, 34.145, 22.081}, {33.410, 32.676, 21.824}, {33.414, 34.753, 23.746}, {35.705, 33.080, 23.479}, {35.071, 34.848, 21.392}, {37.432, 35.637, 23.214}, {14.829, 31.722, 26.061}, {16.520, 32.117, 23.565}, {16.256, 34.282, 25.560}, {19.182, 33.951, 25.814}, {19.763, 34.911, 20.596}, {20.933, 34.375, 23.552}, {20.716, 36.447, 25.547}, {23.166, 34.785, 25.313}, {22.440, 36.603, 23.211}, {24.830, 35.069, 22.961}, {24.723, 37.182, 24.924}, {27.085, 35.603, 24.642}, {26.373, 37.407, 22.640}, {28.720, 35.767, 22.277}, {28.728, 37.939, 24.276}, {31.035, 36.248, 23.981}, {30.163, 38.179, 21.805}, {32.638, 36.468, 21.515}, {32.555, 38.726, 23.489}, {34.886, 37.034, 23.184}, {17.957, 34.682, 23.302}, {22.444, 38.828, 25.123}, {24.078, 39.155, 22.829}, {26.306, 39.552, 24.517}, {30.157, 40.478, 23.736}, {26.105, 12.825, 30.167}, {27.750, 12.928, 27.746}, {30.051, 13.365, 29.488}, {30.268, 11.527, 24.361}, {33.956, 14.181, 28.760}, {19.153, 15.212, 29.149}, {21.255, 16.036, 30.724}, {23.590, 14.316, 30.554}, {22.954, 16.232, 28.475}, {25.334, 14.634, 28.106}, {25.297, 16.703, 30.118}, {27.716, 15.008, 29.831}, {26.933, 16.975, 27.771}, {29.319, 15.254, 27.488}, {29.306, 17.372, 29.504}, {31.541, 15.794, 29.264}, {30.932, 17.608, 27.103}, {33.243, 16.130, 26.806}, {33.131, 18.106, 28.812}, {35.464, 16.601, 28.543}, {34.859, 18.440, 26.544}, {37.071, 18.894, 28.112}, {39.688, 23.355, 23.439}, {16.416, 19.263, 31.240}, {18.905, 17.556, 30.942}, {18.245, 19.492, 29.004}, {20.642, 17.757, 28.727}, {20.409, 19.916, 30.588}, {22.967, 18.446, 30.288}, {22.193, 20.133, 28.255}, {24.611, 18.586, 28.000}, {24.535, 20.699, 30.041}, {26.960, 18.939, 29.766}, {26.162, 20.904, 27.671}, {28.583, 19.258, 27.286}, {28.408, 21.359, 29.365}, {30.832, 19.764, 29.091}, {30.146, 21.556, 26.994}, {32.464, 20.013, 26.687}, {32.389, 22.169, 28.684}, {34.775, 20.632, 28.438}, {34.079, 22.460, 26.239}, {36.361, 20.862, 26.077}, {36.360, 22.910, 28.029}, {38.670, 21.327, 27.838}, {38.017, 23.167, 25.576}, {40.186, 23.754, 27.418}, {14.109, 20.811, 31.464}, {13.445, 22.906, 29.390}, {15.786, 21.221, 29.088}, {15.760, 23.329, 31.143}, {18.068, 21.618, 30.935}, {17.513, 23.476, 28.782}, {19.765, 21.804, 28.549}, {19.788, 23.925, 30.611}, {22.091, 22.230, 30.196}, {21.436, 24.164, 28.264}, {23.777, 22.544, 27.886}, {23.672, 24.705, 29.867}, {26.042, 23.051, 29.556}, {25.364, 24.968, 27.527}, {27.751, 23.227, 27.276}, {27.655, 25.376, 29.313}, {30.010, 23.785, 29.075}, {29.268, 25.633, 26.912}, {31.668, 24.032, 26.654}, {31.598, 26.137, 28.631}, {33.953, 24.440, 28.293}, {33.333, 26.452, 26.348}, {35.626, 24.842, 25.947}, {35.582, 26.874, 28.056}, {37.943, 25.282, 27.677}, {37.164, 27.186, 25.709}, {39.654, 25.613, 25.393}, {39.558, 27.667, 27.314}, {42.032, 26.010, 26.924}, {41.197, 27.914, 24.864}, {13.422, 24.952, 31.362}, {12.845, 26.646, 29.250}, {15.229, 25.297, 28.975}, {15.068, 27.275, 31.103}, {17.441, 25.632, 30.857}, {16.929, 27.523, 28.791}, {19.147, 25.885, 28.408}, {19.114, 27.948, 30.431}, {21.303, 26.324, 30.179}, {20.706, 28.210, 28.045}, {23.137, 26.523, 27.766}, {23.013, 28.746, 29.792}, {25.344, 27.040, 29.470}, {24.542, 29.012, 27.438}, {26.952, 27.285, 27.223}, {26.871, 29.386, 29.178}, {29.266, 27.894, 28.802}, {28.578, 29.636, 26.797}, {30.937, 28.007, 26.526}, {30.899, 30.090, 28.515}, {33.214, 28.528, 28.221}, {32.455, 30.402, 26.163}, {34.881, 28.774, 25.976}, {34.790, 30.864, 27.976}, {37.117, 29.288, 27.641}, {36.306, 31.077, 25.575}, {38.835, 29.566, 25.187}, {38.788, 31.538, 27.068}, {41.166, 30.029, 26.837}, {40.514, 29.928, 22.852}, {12.629, 28.951, 31.015}, {14.549, 29.035, 28.957}, {12.950, 31.496, 28.635}, {16.671, 29.589, 30.726}, {15.918, 31.440, 28.546}, {18.288, 29.844, 28.266}, {18.212, 31.907, 30.272}, {20.606, 30.339, 30.088}, {19.819, 32.193, 27.836}, {22.216, 30.586, 27.755}, {21.973, 32.682, 29.623}, {24.471, 31.048, 29.387}, {23.905, 32.834, 27.344}, {26.309, 31.377, 27.119}, {26.192, 33.291, 29.039}, {28.578, 31.826, 28.859}, {27.795, 33.731, 26.631}, {30.178, 32.079, 26.587}, {30.171, 34.223, 28.404}, {32.480, 32.511, 28.246}, {31.762, 34.436, 26.061}, {34.029, 32.782, 25.814}, {34.095, 34.899, 27.721}, {36.399, 33.201, 27.392}, {35.710, 35.280, 25.388}, {37.856, 33.406, 25.134}, {37.963, 35.756, 27.018}, {40.317, 36.056, 28.687}, {15.789, 33.497, 30.576}, {14.518, 33.956, 28.097}, {17.426, 33.756, 28.208}, {17.356, 35.941, 30.111}, {19.632, 34.315, 29.971}, {19.121, 36.121, 27.842}, {21.370, 34.493, 27.488}, {21.322, 36.702, 29.597}, {23.687, 35.039, 29.231}, {23.064, 36.899, 27.254}, {25.495, 35.251, 26.999}, {25.388, 37.459, 28.867}, {27.723, 35.781, 28.656}, {27.030, 37.723, 26.626}, {29.404, 36.060, 26.321}, {29.303, 38.272, 28.290}, {31.683, 36.613, 27.910}, {30.948, 38.449, 25.809}, {33.379, 36.842, 25.637}, {33.267, 39.059, 27.542}, {35.512, 37.273, 27.426}, {34.947, 39.198, 25.132}, {18.902, 38.373, 29.860}, {20.703, 38.551, 27.515}, {20.397, 40.641, 29.489}, {22.981, 39.041, 29.249}, {22.446, 40.948, 27.154}, {24.635, 39.331, 26.917}, {24.519, 41.527, 28.954}, {26.941, 39.986, 28.534}, {26.253, 41.842, 26.470}, {28.545, 40.164, 26.224}, {28.630, 42.193, 28.005}, {30.867, 40.584, 27.828}, {26.718, 12.902, 34.388}, {28.461, 13.117, 31.903}, {30.715, 13.525, 33.634}, {32.347, 13.885, 31.350}, {34.973, 14.259, 33.000}, {19.562, 15.690, 33.016}, {24.607, 12.262, 32.546}, {21.869, 16.215, 34.802}, {24.173, 14.520, 34.449}, {23.618, 16.394, 32.296}, {26.009, 14.740, 32.254}, {25.811, 16.908, 34.144}, {28.235, 15.224, 33.968}, {27.545, 17.101, 31.800}, {29.908, 15.472, 31.578}, {29.830, 17.636, 33.562}, {32.320, 15.978, 33.303}, {31.585, 17.902, 31.264}, {33.936, 16.230, 30.897}, {33.819, 18.452, 33.011}, {36.194, 16.875, 32.540}, {35.403, 18.677, 30.456}, {37.839, 17.115, 30.111}, {37.743, 19.246, 32.237}, {39.590, 19.509, 29.914}, {17.326, 17.399, 33.272}, {17.133, 19.398, 35.418}, {19.518, 17.812, 35.031}, {18.783, 19.646, 33.021}, {21.290, 18.090, 32.679}, {21.207, 20.076, 34.696}, {23.511, 18.587, 34.390}, {22.694, 20.477, 32.303}, {25.057, 18.839, 32.120}, {25.083, 20.980, 34.043}, {27.372, 19.288, 33.699}, {26.694, 21.156, 31.680}, {29.145, 19.519, 31.463}, {29.101, 21.676, 33.412}, {31.377, 19.973, 33.118}, {30.723, 21.910, 31.076}, {33.109, 20.229, 30.788}, {33.072, 22.341, 32.738}, {35.376, 20.788, 32.497}, {34.726, 22.665, 30.372}, {37.043, 21.040, 30.113}, {37.022, 23.109, 32.121}, {39.375, 21.491, 31.873}, {38.678, 23.410, 29.828}, {41.146, 22.035, 29.500}, {40.913, 24.010, 31.513}, {14.894, 21.015, 35.594}, {11.840, 22.231, 31.781}, {16.364, 21.267, 33.230}, {16.453, 23.408, 35.225}, {18.713, 21.825, 34.964}, {18.102, 23.718, 32.873}, {20.402, 21.910, 32.644}, {20.400, 24.058, 34.568}, {22.734, 22.555, 34.300}, {22.005, 24.379, 32.210}, {24.349, 22.839, 31.951}, {24.409, 24.896, 33.949}, {26.725, 23.228, 33.623}, {25.970, 25.081, 31.535}, {28.340, 23.496, 31.374}, {28.242, 25.702, 33.229}, {30.558, 23.960, 33.002}, {29.984, 25.847, 30.944}, {32.346, 24.250, 30.650}, {32.285, 26.314, 32.546}, {34.636, 24.768, 32.444}, {33.900, 26.647, 30.208}, {36.292, 25.031, 30.035}, {36.260, 27.155, 32.089}, {38.488, 25.484, 31.748}, {37.815, 27.391, 29.664}, {40.249, 25.847, 29.397}, {40.137, 27.913, 31.282}, {42.622, 26.266, 31.109}, {41.728, 28.125, 29.038}, {14.044, 22.969, 33.477}, {13.233, 27.070, 33.297}, {15.642, 25.299, 33.137}, {15.651, 27.418, 35.101}, {18.119, 25.786, 34.879}, {17.256, 27.662, 32.887}, {19.679, 26.088, 32.571}, {19.625, 28.204, 34.471}, {21.922, 26.535, 34.167}, {21.285, 28.472, 32.106}, {23.608, 26.676, 31.782}, {23.637, 28.822, 33.769}, {25.881, 27.184, 33.562}, {25.229, 29.060, 31.501}, {31.542, 28.277, 30.582}, {31.518, 30.374, 32.502}, {33.890, 28.776, 32.306}, {33.197, 30.649, 30.254}, {35.531, 29.024, 29.978}, {35.465, 31.139, 31.849}, {37.791, 29.572, 31.609}, {37.125, 31.452, 29.462}, {39.347, 29.774, 29.292}, {39.377, 31.887, 31.192}, {41.700, 30.252, 30.909}, {40.971, 32.187, 28.653}, {13.092, 28.995, 35.341}, {14.852, 29.463, 32.976}, {14.841, 31.254, 35.061}, {17.227, 29.758, 34.809}, {16.596, 31.617, 32.697}, {18.950, 30.068, 32.409}, {18.797, 32.108, 34.425}, {21.237, 30.496, 34.051}, {20.485, 32.457, 32.037}, {22.851, 30.815, 31.720}, {22.819, 32.978, 33.656}, {25.077, 31.298, 33.417}, {24.501, 33.129, 31.238}, {26.775, 31.445, 31.090}, {26.782, 33.655, 32.995}, {29.266, 31.986, 32.758}, {28.523, 33.983, 30.742}, {30.847, 32.288, 30.535}, {30.864, 34.503, 32.416}, {33.100, 32.806, 32.090}, {32.391, 34.793, 30.040}, {34.751, 33.100, 29.744}, {34.641, 35.220, 31.748}, {37.128, 33.535, 31.494}, {36.397, 35.437, 29.468}, {38.666, 33.813, 29.057}, {38.799, 35.874, 31.049}, {41.135, 34.131, 30.843}, {13.969, 33.093, 32.783}, {16.419, 33.713, 34.625}, {15.558, 35.531, 32.613}, {18.010, 33.976, 32.332}, {17.980, 36.138, 34.283}, {20.355, 34.508, 33.913}, {19.645, 36.341, 31.937}, {22.122, 34.809, 31.626}, {21.952, 36.901, 33.617}, {24.459, 35.189, 33.360}, {23.735, 37.082, 31.314}, {26.036, 35.499, 30.897}, {26.082, 37.653, 32.919}, {28.366, 36.123, 32.613}, {27.645, 37.913, 30.524}, {30.087, 36.393, 30.344}, {29.961, 38.524, 32.229}, {32.348, 36.841, 31.997}, {31.599, 38.704, 29.894}, {33.960, 37.098, 29.665}, {33.873, 39.274, 31.638}, {36.157, 37.538, 31.344}, {35.611, 39.495, 29.279}, {37.963, 37.766, 28.979}, {17.112, 38.013, 32.059}, {19.516, 38.573, 33.892}, {21.164, 38.677, 31.566}, {21.187, 40.870, 33.643}, {23.619, 39.209, 33.198}, {22.795, 41.096, 31.142}, {25.325, 39.524, 30.877}, {25.167, 41.687, 32.782}, {27.648, 40.101, 32.464}, {26.913, 42.004, 30.491}, {29.175, 40.335, 30.276}, {29.268, 42.515, 32.150}, {31.592, 40.793, 31.929}, {33.510, 41.363, 29.544}, {29.032, 13.376, 35.951}, {33.116, 13.981, 35.278}, {19.884, 15.768, 37.032}, {22.506, 16.279, 38.853}, {22.400, 14.029, 36.966}, {24.182, 16.515, 36.481}, {26.512, 14.933, 36.249}, {26.442, 17.155, 38.245}, {28.897, 15.584, 37.952}, {28.057, 17.378, 35.787}, {30.673, 15.714, 35.623}, {30.496, 17.928, 37.502}, {32.844, 16.226, 37.338}, {32.175, 18.216, 35.229}, {34.588, 16.563, 34.952}, {34.360, 18.649, 37.011}, {37.151, 14.923, 34.846}, {36.233, 18.970, 34.545}, {38.576, 19.383, 36.216}, {40.340, 19.567, 33.762}, {17.715, 21.662, 41.299}, {19.967, 18.070, 39.290}, {19.512, 19.955, 37.013}, {21.778, 18.136, 36.833}, {21.784, 20.307, 38.722}, {24.094, 18.712, 38.409}, {23.382, 20.617, 36.337}, {25.788, 19.127, 36.031}, {25.746, 21.127, 38.067}, {28.140, 19.487, 37.754}, {27.413, 21.425, 35.726}, {29.706, 19.816, 35.459}, {29.682, 21.957, 37.455}, {32.011, 20.307, 37.147}, {31.323, 22.159, 35.089}, {33.752, 20.586, 34.904}, {33.571, 22.743, 36.896}, {36.033, 20.990, 36.540}, {35.375, 22.852, 34.394}, {37.865, 21.343, 34.185}, {37.757, 23.346, 36.163}, {40.114, 21.742, 35.803}, {39.317, 23.692, 33.807}, {41.689, 24.304, 35.499}, {14.719, 23.120, 37.496}, {17.108, 21.519, 37.416}, {17.007, 23.653, 39.200}, {19.424, 22.081, 38.998}, {18.726, 23.902, 36.906}, {21.055, 22.270, 36.597}, {21.051, 24.326, 38.570}, {23.319, 22.783, 38.304}, {22.625, 24.623, 36.260}, {24.920, 22.949, 36.031}, {24.862, 25.061, 37.978}, {27.268, 23.489, 37.700}, {26.517, 25.292, 35.545}, {28.912, 23.775, 35.335}, {28.901, 25.882, 37.263}, {31.247, 24.238, 36.961}, {30.625, 26.112, 34.849}, {32.900, 24.491, 34.649}, {32.882, 26.607, 36.598}, {35.212, 24.976, 36.402}, {34.577, 26.875, 34.377}, {36.995, 25.152, 34.108}, {36.817, 27.385, 35.978}, {39.241, 25.781, 35.727}, {38.587, 27.677, 33.705}, {40.913, 26.179, 33.431}, {40.802, 28.173, 35.420}, {42.621, 28.543, 32.924}, {14.039, 25.091, 35.376}, {16.395, 25.612, 37.122}, {16.283, 27.639, 39.118}, {18.603, 25.865, 38.892}, {18.047, 27.943, 36.776}, {20.334, 26.257, 36.654}, {20.286, 28.376, 38.468}, {22.584, 26.767, 38.212}, {21.865, 28.652, 36.102}, {24.236, 26.968, 35.947}, {24.207, 29.041, 37.755}, {26.549, 27.346, 37.495}, {25.837, 29.403, 35.513}, {28.180, 27.710, 35.156}, {28.144, 29.865, 37.136}, {30.476, 28.168, 36.798}, {29.873, 30.183, 34.791}, {32.194, 28.515, 34.622}, {32.182, 30.708, 36.653}, {34.501, 29.075, 36.258}, {33.778, 30.895, 34.202}, {36.162, 29.266, 33.899}, {36.055, 31.392, 35.829}, {38.435, 29.775, 35.640}, {37.727, 31.660, 33.578}, {40.153, 30.010, 33.223}, {39.959, 32.119, 35.196}, {41.824, 32.222, 32.975}, {15.499, 29.378, 37.030}, {15.301, 31.433, 38.949}, {17.798, 30.013, 38.766}, {17.046, 31.859, 36.717}, {19.525, 30.300, 36.459}, {19.399, 32.392, 38.442}, {21.800, 30.849, 38.104}, {21.124, 32.687, 35.976}, {23.516, 30.955, 35.767}, {23.328, 32.993, 37.724}, {25.788, 31.496, 37.431}, {25.150, 33.336, 35.335}, {27.536, 31.760, 34.963}, {27.369, 33.753, 37.059}, {29.859, 32.300, 36.762}, {29.104, 34.148, 34.675}, {31.552, 32.527, 34.431}, {31.411, 34.679, 36.370}, {33.709, 33.009, 36.196}, {33.014, 34.947, 34.150}, {35.346, 33.361, 33.795}, {35.317, 35.345, 35.843}, {37.664, 33.782, 35.435}, {37.032, 35.741, 33.393}, {39.403, 33.939, 33.156}, {39.959, 34.310, 37.157}, {17.006, 33.975, 38.772}, {15.850, 38.445, 35.033}, {18.748, 34.175, 36.375}, {18.620, 36.343, 38.221}, {21.092, 34.817, 38.007}, {20.244, 36.624, 35.991}, {22.733, 35.043, 35.626}, {22.766, 37.136, 37.600}, {25.053, 35.528, 37.356}, {24.342, 37.434, 35.206}, {26.758, 35.709, 35.000}, {26.705, 37.941, 36.907}, {29.003, 36.194, 36.610}, {28.246, 38.173, 34.599}, {30.718, 36.665, 34.312}, {30.602, 38.639, 36.422}, {33.051, 37.058, 36.170}, {32.268, 39.014, 34.053}, {34.571, 37.337, 33.716}, {34.607, 39.491, 35.809}, {36.850, 37.680, 35.532}, {33.989, 41.237, 33.685}, {21.843, 39.829, 38.471}, {21.913, 39.018, 35.704}, {24.217, 39.588, 37.148}, {22.075, 41.856, 36.306}, {25.934, 39.867, 34.817}, {28.231, 40.285, 36.539}, {27.559, 42.226, 34.453}, {30.043, 40.545, 34.317}, {32.276, 41.085, 36.082}, {24.865, 16.864, 40.439}, {27.276, 17.204, 42.096}, {28.865, 17.612, 39.866}, {31.165, 16.030, 39.600}, {31.114, 18.134, 41.626}, {32.724, 18.336, 39.234}, {36.586, 19.071, 38.595}, {20.094, 20.279, 41.174}, {22.378, 18.397, 40.824}, {22.445, 20.467, 42.751}, {24.897, 19.055, 42.394}, {23.952, 20.835, 40.391}, {26.420, 19.196, 40.091}, {26.315, 21.439, 42.042}, {28.648, 19.647, 41.784}, {28.071, 21.674, 39.717}, {30.385, 19.976, 39.621}, {30.266, 22.126, 41.459}, {32.691, 20.448, 41.159}, {31.930, 22.365, 39.164}, {34.208, 20.818, 38.864}, {34.261, 22.887, 40.839}, {36.637, 21.241, 40.554}, {36.001, 23.085, 38.458}, {38.389, 21.457, 38.118}, {38.253, 23.651, 40.026}, {39.970, 23.918, 37.691}, {20.169, 22.307, 43.075}, {19.312, 24.094, 41.002}, {21.726, 22.493, 40.686}, {21.568, 24.789, 42.648}, {23.947, 22.993, 42.348}, {23.361, 24.907, 40.400}, {25.532, 23.198, 39.993}, {25.584, 25.330, 41.980}, {27.972, 23.793, 41.660}, {27.220, 25.710, 39.653}, {29.558, 24.064, 39.364}, {29.522, 26.164, 41.378}, {31.722, 24.540, 41.085}, {31.156, 26.487, 38.880}, {33.477, 24.686, 38.784}, {33.415, 26.946, 40.572}, {35.811, 25.326, 40.359}, {35.143, 27.128, 38.260}, {37.571, 25.480, 38.111}, {37.482, 27.664, 39.988}, {39.916, 25.997, 39.825}, {41.574, 26.258, 37.478}, {16.923, 25.570, 41.235}, {19.086, 26.349, 42.993}, {18.517, 27.988, 40.779}, {20.922, 26.445, 40.588}, {20.750, 28.629, 42.481}, {23.133, 27.067, 42.298}, {22.533, 28.866, 40.212}, {24.873, 27.193, 39.901}, {24.871, 29.371, 41.890}, {27.151, 27.758, 41.579}, {26.424, 29.613, 39.534}, {28.760, 27.934, 39.219}, {28.737, 30.070, 41.151}, {31.065, 28.503, 40.913}, {30.352, 30.331, 38.826}, {32.728, 28.760, 38.485}, {32.687, 30.703, 40.505}, {35.162, 29.214, 40.342}, {34.446, 31.060, 38.243}, {36.750, 29.463, 37.971}, {36.758, 31.591, 39.941}, {39.225, 27.939, 37.764}, {38.358, 31.891, 37.519}, {40.904, 30.303, 37.370}, {18.383, 30.152, 42.782}, {17.601, 32.108, 40.790}, {20.065, 30.386, 40.366}, {20.058, 32.427, 42.358}, {22.526, 31.009, 41.964}, {21.740, 32.901, 40.066}, {24.227, 31.164, 39.713}, {24.137, 33.374, 41.561}, {26.384, 31.692, 41.454}, {25.635, 33.592, 39.383}, {27.947, 31.994, 39.164}, {27.957, 34.094, 41.099}, {30.274, 32.456, 40.849}, {29.705, 34.399, 38.767}, {31.997, 32.763, 38.657}, {31.981, 34.869, 40.517}, {34.390, 33.152, 40.283}, {33.680, 34.997, 38.177}, {35.997, 33.448, 37.890}, {37.632, 37.870, 39.347}, {38.316, 33.843, 39.612}, {37.568, 35.663, 37.613}, {19.454, 34.591, 40.216}, {21.777, 34.927, 41.981}, {20.908, 36.993, 40.018}, {23.312, 35.320, 39.608}, {24.253, 34.081, 45.458}, {25.514, 35.819, 41.227}, {25.008, 37.670, 39.218}, {27.346, 35.904, 38.994}, {27.282, 38.094, 40.957}, {29.453, 36.490, 40.759}, {28.930, 38.323, 38.661}, {31.286, 36.805, 38.533}, {30.459, 41.012, 38.273}, {33.557, 37.247, 40.207}, {32.765, 39.228, 38.108}, {35.162, 37.401, 37.845}, {26.393, 40.013, 38.945}, {28.587, 21.718, 43.714}, {30.855, 20.210, 43.314}, {32.575, 22.700, 43.005}, {36.717, 23.426, 42.414}, {22.562, 22.208, 45.225}, {26.397, 23.505, 43.963}, {26.300, 25.640, 45.921}, {28.439, 23.732, 45.930}, {27.844, 25.976, 43.647}, {30.228, 24.235, 43.438}, {30.189, 26.276, 45.369}, {34.217, 27.125, 44.697}, {31.893, 26.609, 42.999}, {34.099, 25.051, 42.635}, {35.846, 29.545, 44.293}, {35.792, 27.322, 42.325}, {23.965, 25.051, 44.340}, {21.466, 26.934, 44.589}, {25.379, 27.482, 43.976}, {24.112, 27.566, 46.797}, {27.592, 28.172, 45.636}, {27.115, 29.892, 43.553}, {29.529, 28.280, 43.308}, {29.433, 30.438, 45.156}, {31.755, 28.683, 44.987}, {31.052, 30.704, 42.957}, {33.465, 29.051, 42.693}, {33.542, 31.189, 44.696}, {35.104, 31.354, 42.234}, {37.579, 29.755, 42.045}, {23.072, 29.197, 44.264}, {24.855, 31.529, 43.788}, {27.200, 32.058, 45.529}, {26.332, 33.917, 43.403}, {28.655, 32.286, 43.143}, {30.955, 32.865, 44.933}, {30.340, 34.687, 42.787}, {32.738, 32.862, 42.523}, {36.032, 35.459, 39.925}, {27.782, 36.249, 43.060}};
    //const int NAU = 884;

    point au[] = {{40.00000,  40.00000,  40.00000},{40.00000,  42.03148,  42.03148},{42.03148,  40.00000,  42.03148},{42.03148,  42.03148,  40.00000},{35.93839,  33.90112,  33.90112},{40.00000,  33.89970,  33.89970},{44.06161,  33.90112,  33.90112},{33.90112,  35.93839,  33.90112},{35.93646,  35.93646,  31.86571},{35.93678,  37.96931,  33.90408},{37.96931,  35.93678,  33.90408},{37.96930,  37.96930,  31.86646},{40.00000,  35.93251,  31.86554},{40.00000,  37.96886,  33.90434},{42.03069,  35.93678,  33.90408},{42.03070,  37.96930,  31.86646},{44.06354,  35.93646,  31.86571},{44.06322,  37.96931,  33.90408},{46.09888,  35.93839,  33.90112},{33.89970,  40.00000,  33.89970},{35.93251,  40.00000,  31.86554},{35.93678,  42.03069,  33.90408},{37.96886,  40.00000,  33.90434},{37.96930,  42.03070,  31.86646},{40.00000,  40.00000,  31.86619},{40.00000,  42.03114,  33.90434},{42.03114,  40.00000,  33.90434},{42.03070,  42.03070,  31.86646},{44.06749,  40.00000,  31.86554},{44.06322,  42.03069,  33.90408},{46.10030,  40.00000,  33.89970},{33.90112,  44.06161,  33.90112},{35.93646,  44.06354,  31.86571},{35.93839,  46.09888,  33.90112},{37.96931,  44.06322,  33.90408},{40.00000,  44.06749,  31.86554},{40.00000,  46.10030,  33.89970},{42.03069,  44.06322,  33.90408},{44.06354,  44.06354,  31.86571},{44.06161,  46.09888,  33.90112},{46.09888,  44.06161,  33.90112},{33.90112,  33.90112,  35.93839},{35.93646,  31.86571,  35.93646},{35.93678,  33.90408,  37.96931},{37.96930,  31.86646,  37.96930},{37.96931,  33.90408,  35.93678},{40.00000,  31.86554,  35.93251},{40.00000,  33.90434,  37.96886},{42.03070,  31.86646,  37.96930},{42.03069,  33.90408,  35.93678},{44.06354,  31.86571,  35.93646},{44.06322,  33.90408,  37.96931},{46.09888,  33.90112,  35.93839},{31.86571,  35.93646,  35.93646},{31.86646,  37.96930,  37.96930},{33.90408,  35.93678,  37.96931},{33.90408,  37.96931,  35.93678},{35.93692,  35.93692,  35.93692},{35.93692,  37.96861,  37.96861},{37.96861,  35.93692,  37.96861},{37.96861,  37.96861,  35.93692},{40.00000,  35.93697,  35.93697},{40.00000,  37.96852,  37.96852},{42.03139,  35.93692,  37.96861},{42.03139,  37.96861,  35.93692},{44.06308,  35.93692,  35.93692},{44.06308,  37.96861,  37.96861},{46.09592,  35.93678,  37.96931},{46.09592,  37.96931,  35.93678},{48.13429,  35.93646,  35.93646},{48.13354,  37.96930,  37.96930},{31.86554,  40.00000,  35.93251},{31.86646,  42.03070,  37.96930},{33.90434,  40.00000,  37.96886},{33.90408,  42.03069,  35.93678},{35.93697,  40.00000,  35.93697},{35.93692,  42.03139,  37.96861},{37.96852,  40.00000,  37.96852},{37.96861,  42.03139,  35.93692},{40.00000,  40.00000,  35.93687},{40.00000,  42.03148,  37.96852},{42.03148,  40.00000,  37.96852},{42.03139,  42.03139,  35.93692},{44.06303,  40.00000,  35.93697},{44.06308,  42.03139,  37.96861},{46.09566,  40.00000,  37.96886},{46.09592,  42.03069,  35.93678},{48.13446,  40.00000,  35.93251},{48.13354,  42.03070,  37.96930},{31.86571,  44.06354,  35.93646},{33.90408,  44.06322,  37.96931},{33.90112,  46.09888,  35.93839},{35.93692,  44.06308,  35.93692},{35.93678,  46.09592,  37.96931},{37.96861,  44.06308,  37.96861},{37.96931,  46.09592,  35.93678},{40.00000,  44.06303,  35.93697},{40.00000,  46.09566,  37.96886},{42.03139,  44.06308,  37.96861},{42.03069,  46.09592,  35.93678},{44.06308,  44.06308,  35.93692},{44.06322,  46.09592,  37.96931},{46.09592,  44.06322,  37.96931},{46.09888,  46.09888,  35.93839},{48.13429,  44.06354,  35.93646},{35.93646,  48.13429,  35.93646},{37.96930,  48.13354,  37.96930},{40.00000,  48.13446,  35.93251},{42.03070,  48.13354,  37.96930},{44.06354,  48.13429,  35.93646},{33.89970,  33.89970,  40.00000},{35.93251,  31.86554,  40.00000},{35.93678,  33.90408,  42.03069},{37.96930,  31.86646,  42.03070},{37.96886,  33.90434,  40.00000},{40.00000,  31.86619,  40.00000},{40.00000,  33.90434,  42.03114},{42.03070,  31.86646,  42.03070},{42.03114,  33.90434,  40.00000},{44.06749,  31.86554,  40.00000},{44.06322,  33.90408,  42.03069},{46.10030,  33.89970,  40.00000},{31.86554,  35.93251,  40.00000},{31.86646,  37.96930,  42.03070},{33.90408,  35.93678,  42.03069},{33.90434,  37.96886,  40.00000},{35.93697,  35.93697,  40.00000},{35.93692,  37.96861,  42.03139},{37.96861,  35.93692,  42.03139},{37.96852,  37.96852,  40.00000},{40.00000,  35.93687,  40.00000},{40.00000,  37.96852,  42.03148},{42.03139,  35.93692,  42.03139},{42.03148,  37.96852,  40.00000},{44.06303,  35.93697,  40.00000},{44.06308,  37.96861,  42.03139},{46.09592,  35.93678,  42.03069},{46.09566,  37.96886,  40.00000},{48.13446,  35.93251,  40.00000},{48.13354,  37.96930,  42.03070},{31.86619,  40.00000,  40.00000},{31.86646,  42.03070,  42.03070},{33.90434,  40.00000,  42.03114},{33.90434,  42.03114,  40.00000},{35.93687,  40.00000,  40.00000},{35.93692,  42.03139,  42.03139},{37.96852,  40.00000,  42.03148},{37.96852,  42.03148,  40.00000},{44.06313,  40.00000,  40.00000},{44.06308,  42.03139,  42.03139},{46.09566,  40.00000,  42.03114},{46.09566,  42.03114,  40.00000},{48.13381,  40.00000,  40.00000},{48.13354,  42.03070,  42.03070},{31.86554,  44.06749,  40.00000},{33.90408,  44.06322,  42.03069},{33.89970,  46.10030,  40.00000},{35.93697,  44.06303,  40.00000},{35.93678,  46.09592,  42.03069},{37.96861,  44.06308,  42.03139},{37.96886,  46.09566,  40.00000},{40.00000,  44.06313,  40.00000},{40.00000,  46.09566,  42.03114},{42.03139,  44.06308,  42.03139},{42.03114,  46.09566,  40.00000},{44.06303,  44.06303,  40.00000},{44.06322,  46.09592,  42.03069},{46.09592,  44.06322,  42.03069},{46.10030,  46.10030,  40.00000},{48.13446,  44.06749,  40.00000},{35.93251,  48.13446,  40.00000},{37.96930,  48.13354,  42.03070},{40.00000,  48.13381,  40.00000},{42.03070,  48.13354,  42.03070},{44.06749,  48.13446,  40.00000},{33.90112,  33.90112,  44.06161},{35.93646,  31.86571,  44.06354},{35.93839,  33.90112,  46.09888},{37.96931,  33.90408,  44.06322},{40.00000,  31.86554,  44.06749},{40.00000,  33.89970,  46.10030},{42.03069,  33.90408,  44.06322},{44.06354,  31.86571,  44.06354},{44.06161,  33.90112,  46.09888},{46.09888,  33.90112,  44.06161},{31.86571,  35.93646,  44.06354},{33.90112,  35.93839,  46.09888},{33.90408,  37.96931,  44.06322},{35.93692,  35.93692,  44.06308},{35.93678,  37.96931,  46.09592},{37.96931,  35.93678,  46.09592},{37.96861,  37.96861,  44.06308},{40.00000,  35.93697,  44.06303},{40.00000,  37.96886,  46.09566},{42.03069,  35.93678,  46.09592},{42.03139,  37.96861,  44.06308},{44.06308,  35.93692,  44.06308},{44.06322,  37.96931,  46.09592},{46.09888,  35.93839,  46.09888},{46.09592,  37.96931,  44.06322},{48.13429,  35.93646,  44.06354},{31.86554,  40.00000,  44.06749},{33.89970,  40.00000,  46.10030},{33.90408,  42.03069,  44.06322},{35.93697,  40.00000,  44.06303},{35.93678,  42.03069,  46.09592},{37.96886,  40.00000,  46.09566},{37.96861,  42.03139,  44.06308},{40.00000,  40.00000,  44.06313},{40.00000,  42.03114,  46.09566},{42.03114,  40.00000,  46.09566},{42.03139,  42.03139,  44.06308},{44.06303,  40.00000,  44.06303},{44.06322,  42.03069,  46.09592},{46.10030,  40.00000,  46.10030},{46.09592,  42.03069,  44.06322},{48.13446,  40.00000,  44.06749},{31.86571,  44.06354,  44.06354},{33.90112,  44.06161,  46.09888},{33.90112,  46.09888,  44.06161},{35.93692,  44.06308,  44.06308},{35.93839,  46.09888,  46.09888},{37.96931,  44.06322,  46.09592},{37.96931,  46.09592,  44.06322},{40.00000,  44.06303,  44.06303},{40.00000,  46.10030,  46.10030},{42.03069,  44.06322,  46.09592},{42.03069,  46.09592,  44.06322},{44.06308,  44.06308,  44.06308},{44.06161,  46.09888,  46.09888},{46.09888,  44.06161,  46.09888},{46.09888,  46.09888,  44.06161},{48.13429,  44.06354,  44.06354},{35.93646,  48.13429,  44.06354},{40.00000,  48.13446,  44.06749},{44.06354,  48.13429,  44.06354},{35.93646,  35.93646,  48.13429},{37.96930,  37.96930,  48.13354},{40.00000,  35.93251,  48.13446},{42.03070,  37.96930,  48.13354},{44.06354,  35.93646,  48.13429},{35.93251,  40.00000,  48.13446},{37.96930,  42.03070,  48.13354},{40.00000,  40.00000,  48.13381},{42.03070,  42.03070,  48.13354},{44.06749,  40.00000,  48.13446},{35.93646,  44.06354,  48.13429},{40.00000,  44.06749,  48.13446}};
    const int NAU = 248;

    const double w2 = 0.03;
    const double nu = 2*w2;
    const double ww = nu;



    const double kw   = 0.03; // primitive coefficient
    const double Kww = kw*kw/(kw+kw); //
    const double K2   = 2*kw; // density
    const double K22  = K2*K2/(K2+K2);

    const double K3w  = kw*kw/(kw+kw+K2);
    const double K32  = kw*K2/(kw+kw+K2);



    // TEST SVD IDEA

    // it certainly works; the "issue" is that the coupling
    // between subspaces has high rank

    if (0) {
        double kc = 1.;
        double kd = 0.03;

        tensor2 Sc(NAU), Sd(NAU), Tcd(NAU);

        tensor2 Scc(NAU), Sdd(NAU);

        {char a; cin>>a;}


        double sss = sqrt(4*kc*kd)/(kc+kd);
        double s3 = sqrt(sss)*sss;

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                Sc(i,j) = Sc(j,i) = -exp(-0.5*kc*r2);
                Sd(i,j) = Sd(j,i) = -exp(-0.5*kd*r2);

                Tcd(i,j) = Tcd(j,i) = exp(-(kc*kd/(kc+kd))*r2) * s3;
            }

            Sc(i,i) = Sd(i,i) = -1;
            Tcd(i,i) = s3;
        }

        Scc = Sc;
        Sdd = Sd;

        double vc[NAU], vd[NAU];

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int SIZE = NAU;
            int LWORK = SIZE*SIZE;  // size of working space
            double * WORK = new double[LWORK];   // nothing


            dsyev_ (&JOBZ, &UL, &SIZE, Sc.c2, &SIZE, vc, WORK, &LWORK, &INFO);
            for (int i=0; i<SIZE; ++i) vc[i] = -vc[i];
            for (int i=0; i<SIZE; ++i) cout << vc[i] << " "; cout << endl << endl;

            dsyev_ (&JOBZ, &UL, &SIZE, Sd.c2, &SIZE, vd, WORK, &LWORK, &INFO);
            for (int i=0; i<SIZE; ++i) vd[i] = -vd[i];
            for (int i=0; i<SIZE; ++i) cout << vd[i] << " "; cout << endl << endl;
        }

        int MAU; for (MAU=0; MAU<NAU; ++MAU) if (vd[MAU]<vd[0]*1e-8) break;

        for (int i=0; i<NAU; ++i) vc[i] = 1./sqrt(vc[i]);
        for (int i=0; i<MAU; ++i) vd[i] = 1./sqrt(vd[i]);

        tensor2 Lc(NAU), Ld(MAU,NAU), T2(NAU,MAU);

        // this is symmetric; no problem
        for (int p=0; p<NAU; ++p) {
            for (int q=0; q<NAU; ++q) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=vc[k]*Sc(k,p)*Sc(k,q);
                Lc(p,q)= ww;
            }
        }

        // this probably has lower rank
        for (int k=0; k<MAU; ++k) {
            for (int q=0; q<NAU; ++q) {
                Ld(k,q)=vd[k]*Sd(k,q);
            }
        }

        // transform Tcd
        for (int i=0; i<NAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=Lc(i,k)*Tcd(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<MAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=Ld(j,l)*cc[l];
                T2(i,j) = jj;
            }
        }

        /*
        tensor2 SDD(MAU);

        // transform Tcd
        for (int i=0; i<MAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=Ld(i,k)*Sdd(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<MAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=Ld(j,l)*cc[l];
                SDD(i,j) = jj;
            }
        }

        cout << endl;
        cout << SDD << endl << endl;
        */



        double ss[NAU];

        const int nau = NAU;
        const int mau = MAU;

        tensor2 Lc2(NAU);

        {
            int NAU = nau;
            int MAU = mau;

            tensor2 U(NAU), V(NAU);

            char all[] = "A";
            int lwork = -1;
            double wokpt;
            double * work;
            int info;

            dgesvd_( all, all, &MAU, &NAU, T2.c2, &MAU, ss, U.c2, &MAU, V.c2, &NAU,  &wokpt, &lwork, &info);

            if (info > 0) return 33;
            lwork = int(wokpt);
            work = new double[max(1,lwork)];

            dgesvd_( all, all, &MAU, &NAU, T2.c2, &MAU, ss, U.c2, &MAU, V.c2, &NAU,  work, &lwork, &info);


            for (int p=0; p<NAU; ++p) {
                for (int q=0; q<NAU; ++q) {
                    double ww=0;
                    for (int k=0; k<NAU; ++k) ww+=V(p,k)*Lc(k,q);
                    Lc2(p,q)= ww;
                }
            }

            delete[] work;
        }

        for (int i=0; i<MAU; ++i) cout << ss[i] << " "; cout << endl << endl;


        {char aaa; cin >> aaa;}

        // this is correct
        /*
        tensor2 SCC(NAU);

        // transform Tcd
        for (int i=0; i<NAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=Lc2(i,k)*Scc(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<NAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=Lc2(j,l)*cc[l];
                SCC(i,j) = jj;
            }
        }

        for (int i=0; i<NAU; ++i) cout << SCC(i,i) << " "; cout << endl << endl;

        {char aaa; cin >> aaa;}
        */

        return 0;
    }


    // test own T3 decomposition
    // =========================

    if (0) {

            // find out real rank of the basis
        tensor2 Sd(NAU), Sdd(NAU);

        {char a; cin>>a;}

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                Sd(i,j) = Sd(j,i) = -exp(-Kww*r2);
            }
            Sd(i,i) = -1;
        }

        Sdd = Sd;

        IncompleteGamma.InitIncompleteGammaTable();

        const int MAU = NAU;

        tensor3 X(NAU,  MAU,MAU); // 884*884*884*4*8

        const double K11 = sqrt(K22);
        const double KV  = K11;

        #pragma omp parallel for
        for (int k=0; k<NAU; ++k) {
            if (k%10==0) cout << k<<endl;

            tensor2 V(NAU,NAU);

            for (int i=0; i<NAU; ++i) {
                for (int j=0; j<=i; ++j) {

                    double px = 0.5*(au[i].x + au[j].x);
                    double py = 0.5*(au[i].y + au[j].y);
                    double pz = 0.5*(au[i].z + au[j].z);

                    double K = -Sdd(i,j);

                    double xx = px - au[k].x;
                    double yy = py - au[k].y;
                    double zz = pz - au[k].z;

                    double r2 = xx*xx+yy*yy+zz*zz;


                    V(i,j)=V(j,i)= K * KV * IncompleteGamma.calcF2(K22*r2);
                }
            }

            for (int i=0; i<MAU; ++i)
                for (int j=0; j<MAU; ++j)
                    X(k,  i,j) = V(i,j);
        }

        cout << "done"<< endl;

        {char a; cin>>a;}


        const bool ComputeT3 = true;

        if (ComputeT3) {

            tensor2 U(NAU*MAU, MAU); // to store the computed basis
            int nu=0;

            // iterate over the T slices
            // =========================
            for (int k=0; k<NAU; ++k) {

                // copy the k-th slice to a matrix W
                tensor2 W(MAU,MAU);

                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<MAU; ++j)
                        W(i,j) = X(k,  i,j);

                {
                    double wf2=0;
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            wf2+=W(i,j)*W(i,j);
                    cout << "X slice " << k << " Frobenius^2 : " << wf2 << endl;
                }

                double w[MAU];

                {
                    char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
                    char UL   = 'U'; // matrix storage pàtern
                    int INFO;        // chack status
                    int LWORK = MAU*MAU;  // size of working space
                    double * WORK = new double[LWORK];   // nothing
                    int SIZE = MAU;

                    dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, w, WORK, &LWORK, &INFO);
                }

                //for (int i=0; i<MAU; ++i) cout << w[i] << " "; cout << endl;

                // copy the basis to U
                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<MAU; ++j)
                        U(nu+i,j) = W(i,j);
                nu += MAU;


                FindRelevant(U, NULL, MAU, nu);

                /*

                // substract the contribution to every other slice
                for (int l=k+1; l<NAU; ++l) {
                    // compute the contribution of each eigenvector i (they can be computed at once since they are orthogonal)
                    double vv[MAU];
                    for (int i=0; i<MAU; ++i) {
                        double tww=0;
                        for (int n=0; n<MAU; ++n)
                            for (int m=0; m<MAU; ++m)
                                tww+=X(l,m,n)*W(i,n)*W(i,m);
                        vv[i] = tww;
                    }

                    // update the matrix
                    for (int i=0; i<MAU; ++i) {
                        double tww=0;
                        for (int n=0; n<MAU; ++n)
                            for (int m=0; m<MAU; ++m)
                                X(l,m,n)-=vv[i]*W(i,n)*W(i,m);
                    }
                }
                */
                {char a; cin>>a;}

            }

            {char a; cin>>a;}

        }

        return 0;
    }

    // test other own T3 decomposition
    // ===============================

    if (1) {

        // find out real rank of the basis
        tensor2 Sd(NAU), Sdd(NAU);

        {char a; cin>>a;}

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                Sd(i,j) = Sd(j,i) = -exp(-Kww*r2);
            }
            Sd(i,i) = -1;
        }

        Sdd = Sd;

        double vd[NAU];

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int SIZE = NAU;
            int LWORK = SIZE*SIZE;  // size of working space
            double * WORK = new double[LWORK];   // nothing

            dsyev_ (&JOBZ, &UL, &SIZE, Sd.c2, &SIZE, vd, WORK, &LWORK, &INFO);
            for (int i=0; i<SIZE; ++i) vd[i] = -vd[i];
            for (int i=0; i<SIZE; ++i) cout << vd[i] << " "; cout << endl << endl;
        }

        int MAU=NAU; // for (MAU=0; MAU<NAU; ++MAU) if (vd[MAU]<vd[0]*1e-8) break;

        for (int i=0; i<MAU; ++i) vd[i] = 1./sqrt(vd[i]);

        tensor2 Ld(MAU,NAU);

        // this probably has lower rank
        for (int k=0; k<MAU; ++k) {
            for (int q=0; q<NAU; ++q) {
                Ld(k,q)=vd[k]*Sd(k,q);
            }
        }

        cout << "MAU, NAU : " << MAU << " " << NAU << endl;



        IncompleteGamma.InitIncompleteGammaTable();


        tensor3 X(NAU,  MAU,MAU); // 884*884*884*4*8

        const double K11 = sqrt(K22);
        const double KV  = K11;

        #pragma omp parallel for
        for (int k=0; k<NAU; ++k) {
            if (k%10==0) cout << k<<endl;

            tensor2 V(NAU,NAU);

            for (int i=0; i<NAU; ++i) {
                for (int j=0; j<=i; ++j) {

                    double px = 0.5*(au[i].x + au[j].x);
                    double py = 0.5*(au[i].y + au[j].y);
                    double pz = 0.5*(au[i].z + au[j].z);

                    double K = -Sdd(i,j);

                    double xx = px - au[k].x;
                    double yy = py - au[k].y;
                    double zz = pz - au[k].z;

                    double r2 = xx*xx+yy*yy+zz*zz;

                    //double rr = sqrt(xx*xx+yy*yy+zz*zz);
                    //V(i,j)=V(j,i)=K * erf(K11 * rr)/rr;

                    V(i,j)=V(j,i)= K * KV * IncompleteGamma.calcF2(K22*r2);
                }
            }

            for (int i=0; i<MAU; ++i)
                for (int j=0; j<MAU; ++j)
                    X(k,  i,j) = V(i,j);
        }

        cout << "done"<< endl;

        {char a; cin>>a;}



        // PARAFAC, or SOMETHING

        // rand
        unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
        MTRand_int32 irand(init, length); // 32-bit int generator
        // this is an example of initializing by an array
        // you may use MTRand(seed) with any 32bit integer
        // as a seed for a simpler initialization
        MTRand drand; // double in [0, 1) generator, already init


        cout.precision(16);
        int it = 0;

        // max rank & total rank
        const int MAXR = 32*MAU;
        int R=0;

        // the factorized basis
        tensor2 UU(MAXR, MAU); // U
        tensor2 DD(MAXR, NAU);

        // save a copy
        tensor3 Y(NAU,MAU,MAU);
        for (int k=0; k<NAU; ++k)
            for (int i=0; i<MAU; ++i)
                for (int j=0; j<MAU; ++j)
                    Y(k,i,j) = X(k,i,j);

        while (it<32) {
            ++it;

            cout << "Iteration : " << it << "    "; //<< endl;

            {
                double s2 = 0;

                for (int k=0; k<NAU; ++k)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            s2 += X(k,i,j)*X(k,i,j);

                cout << "Frobenius : " << s2; // << endl;
            }


            // =================================================================
            if (1) {
                // generate a random vector
                double vv[NAU];
                double uu[MAU];
                tensor2 SS(MAU);

                // generate random vector
                for (int i=0; i<NAU; ++i) vv[i] = drand();

                // normalize vv
                {
                    double s2=0;
                    for (int i=0; i<NAU; ++i) s2 += vv[i]*vv[i];
                    s2 = 1./sqrt(s2);
                    for (int i=0; i<NAU; ++i) vv[i]*=s2;
                }


                double sigma  = 0;
                double lambda = 0;

                // maximum number of iterations just in case
                for (int n=0; n<128; ++n) {

                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j) {
                            double ss = 0;
                            for (int k=0; k<NAU; ++k) ss+=X(k,i,j)*vv[k];
                            SS(i,j) = ss;
                        }

                    // symmetrize (increase numerical stability)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<i; ++j) {
                            double ss = SS(i,j) + SS(j,i);
                            ss *= 0.5;
                            SS(i,j) = SS(j,i) = ss;
                        }


                    {
                        /* Locals */
                        int NSELECT = 1;
                        int n = MAU, il, iu, m, lda = MAU, ldz = MAU, info, lwork;
                        double abstol, vl, vu;
                        double wkopt;
                        double* work;
                        /* Local arrays */
                   /* iwork dimension should be at least 5*n */
                        int iwork[5*MAU], ifail[MAU];
                        double w[MAU];


                        /* Executable statements */
                        //cout << ( " DSYEVX Example Program Results\n" );
                        /* Negative abstol means using the default value */
                        abstol = -1.0;
                        /* Set il, iu to compute NSELECT smallest eigenvalues */
                        il = iu = MAU;

                        /* Query and allocate the optimal workspace */
                        lwork = -1;
                        dsyevx_( "Vectors", "Indices", "Upper", &n, SS.c2, &lda, &vl, &vu, &il, &iu,
                                        &abstol, &m, w, uu, &ldz, &wkopt, &lwork, iwork, ifail, &info );
                        lwork = (int)wkopt;
                        work = new double[lwork];
                        /* Solve eigenproblem */
                        dsyevx_( "Vectors", "Indices", "Upper", &n, SS.c2, &lda, &vl, &vu, &il, &iu,
                                        &abstol, &m, w, uu, &ldz, work, &lwork, iwork, ifail, &info );
                        delete[] work;

                        lambda = w[0];
                    }

                    cout << " lambda : " << lambda << endl;


                    // now the other side:

                    for (int k=0; k<NAU; ++k) {
                        double ss = 0;
                        for (int i=0; i<MAU; ++i)
                            for (int j=0; j<MAU; ++j)
                                ss+=X(k,i,j)*uu[i]*uu[j];
                         vv[k] = ss;
                    }

                    // normalize vv
                    {
                        double s2=0;
                        for (int i=0; i<NAU; ++i) s2 += vv[i]*vv[i];
                        sigma = sqrt(s2);
                        s2 = 1./sigma;
                        for (int i=0; i<NAU; ++i) vv[i]*=s2;
                    }

                    cout << " sigma = " << sigma << endl;

                    if (fabs(sigma-lambda)<sigma*1e-8) break; // should learn when something is not going to converge further and skip anyways
                }

                // save uu
                for (int i=0; i<MAU; ++i) UU(R,i) = uu[i];
                for (int i=0; i<NAU; ++i) DD(R,i) = lambda*vv[i];

                ++R;


                // update the tensor
                for (int k=0; k<NAU; ++k)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            X(k,i,j) -= lambda*vv[k]*uu[i]*uu[j];
            }





        }



        tensor2 SS(R,R);
        tensor2 II(R,NAU);

        tensor2 MM(MAU,MAU);
        double uu[MAU];

        while (1) {

            // fit expansion to best frobenius given U
            // =======================================

            for (int r=0; r<R; ++r)
                for (int s=0; s<R; ++s) {
                    double ss = 0;
                    for (int i=0; i<MAU; ++i)
                        ss += UU(r,i)*UU(s,i);
                    SS(r,s) = ss*ss;
                }

            // invert SS
            {
                int *IPIV = new int[R+1];
                int LWORK = R*R;
                double *WORK = new double[LWORK];
                int INFO;

                dgetrf_(&R,&R,SS.c2,&R,IPIV,&INFO);
                dgetri_(&R,SS.c2,&R,IPIV,WORK,&LWORK,&INFO);

                delete IPIV;
                delete WORK;
            }

            for (int r=0; r<R; ++r) {
                for (int k=0; k<NAU; ++k) {
                    double s2=0;

                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            s2 += Y(k,i,j)*UU(r,i)*UU(r,j);
                    II(r,k) = s2;
                }
            }

            for (int k=0; k<NAU; ++k) {
                for (int r=0; r<R; ++r) {
                    double dd=0;
                    for (int s=0; s<R; ++s) {
                        dd+= SS(r,s)*II(s,k);
                    }
                    DD(r,k) = dd;
                }
            }

            X.zeroize();
            for (int k=0; k<NAU; ++k)
                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<MAU; ++j)
                        X(k,i,j) = Y(k,i,j);
            for (int r=0; r<R; ++r) {
                for (int k=0; k<NAU; ++k) {
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            X(k,i,j) -= DD(r,k)*UU(r,i)*UU(r,j);
                }
            }

            {
                double s2 = 0;

                for (int k=0; k<NAU; ++k)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j) {
                            s2 += X(k,i,j)*X(k,i,j);
                        }

                cout << "Final Frobenius : " << s2 << endl;
            }

            // now adjust the basis U
            // ======================
            for (int r=0; r<R; ++r) {

                /*

                for (int i=0; i<MAU; ++i) uu[i] = 0;

                // build M and do power iteration all at once
                for (int k=0; k<NAU; ++k) {
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            uu[i]+=DD(r,k)*UU(r,j)*X(k,i,j);
                            //uu[i]+=DD(r,k)*UU(r,i)*(X(k,i,j)+DD(r,k)*UU(r,j));
                }


                double dd2=0;
                for (int k=0; k<NAU; ++k) dd2+=DD(r,k)*DD(r,k);
                for (int i=0; i<MAU; ++i) uu[i]+=dd2*UU(r,i);


                // rescale
                {
                    double u2=0;
                    for (int i=0; i<MAU; ++i) u2 += uu[i]*uu[i];
                    u2 = 1./sqrt(u2);
                    for (int i=0; i<MAU; ++i) uu[i] *=u2;
                }
                */


                MM.zeroize();
                for (int k=0; k<NAU; ++k) {
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            MM(i,j) += DD(r,k)*X(k,i,j);
                }


                double dd2=0;
                for (int k=0; k<NAU; ++k) dd2+=DD(r,k)*DD(r,k);
                dd2*=0.5;

                for (int i=0; i<MAU; ++i) {
                    //MM(i,i) = dd2;
                    for (int j=0; j<MAU; ++j)
                        MM(i,j) += dd2*UU(r,i)*UU(r,j);
                }


                // compute largest eigenvector
                {

                    int NSELECT = 1;
                    int n = MAU, il, iu, m, lda = MAU, ldz = MAU, info, lwork;
                    double abstol, vl, vu;
                    double wkopt;
                    double* work;


                    int iwork[5*MAU], ifail[MAU];
                    double w[MAU];

                    abstol = -1.0;
                    il = iu = MAU;

                    lwork = -1;
                    dsyevx_( "Vectors", "Indices", "Upper", &n, MM.c2, &lda, &vl, &vu, &il, &iu,
                                    &abstol, &m, w, uu, &ldz, &wkopt, &lwork, iwork, ifail, &info );
                    lwork = (int)wkopt;
                    work = new double[lwork];
                    dsyevx_( "Vectors", "Indices", "Upper", &n, MM.c2, &lda, &vl, &vu, &il, &iu,
                                    &abstol, &m, w, uu, &ldz, work, &lwork, iwork, ifail, &info );
                    delete[] work;
                }


                // copy to basis
                for (int i=0; i<MAU; ++i) UU(r,i) = uu[i];
            }


        }


        return 0;
    }






    // test PARAFAC / TUCKER3

    // iteration 2395 ~10N -> Frobenius^2 = 0.001283

    if (1) {

        // find out real rank of the basis
        tensor2 Sd(NAU), Sdd(NAU);

        {char a; cin>>a;}

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                Sd(i,j) = Sd(j,i) = -exp(-Kww*r2);
            }
            Sd(i,i) = -1;
        }

        Sdd = Sd;

        double vd[NAU];

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int SIZE = NAU;
            int LWORK = SIZE*SIZE;  // size of working space
            double * WORK = new double[LWORK];   // nothing

            dsyev_ (&JOBZ, &UL, &SIZE, Sd.c2, &SIZE, vd, WORK, &LWORK, &INFO);
            for (int i=0; i<SIZE; ++i) vd[i] = -vd[i];
            for (int i=0; i<SIZE; ++i) cout << vd[i] << " "; cout << endl << endl;
        }

        int MAU=NAU; // for (MAU=0; MAU<NAU; ++MAU) if (vd[MAU]<vd[0]*1e-8) break;

        for (int i=0; i<MAU; ++i) vd[i] = 1./sqrt(vd[i]);

        tensor2 Ld(MAU,NAU);

        // this probably has lower rank
        for (int k=0; k<MAU; ++k) {
            for (int q=0; q<NAU; ++q) {
                Ld(k,q)=vd[k]*Sd(k,q);
            }
        }

        cout << "MAU, NAU : " << MAU << " " << NAU << endl;



        IncompleteGamma.InitIncompleteGammaTable();


        tensor3 X(NAU,  MAU,MAU); // 884*884*884*4*8

        const double K11 = sqrt(K22);
        const double KV  = K11;

        #pragma omp parallel for
        for (int k=0; k<NAU; ++k) {
            if (k%10==0) cout << k<<endl;

            tensor2 V(NAU,NAU);

            for (int i=0; i<NAU; ++i) {
                for (int j=0; j<=i; ++j) {

                    double px = 0.5*(au[i].x + au[j].x);
                    double py = 0.5*(au[i].y + au[j].y);
                    double pz = 0.5*(au[i].z + au[j].z);

                    double K = -Sdd(i,j);

                    double xx = px - au[k].x;
                    double yy = py - au[k].y;
                    double zz = pz - au[k].z;

                    double r2 = xx*xx+yy*yy+zz*zz;

                    //double rr = sqrt(xx*xx+yy*yy+zz*zz);
                    //V(i,j)=V(j,i)=K * erf(K11 * rr)/rr;

                    V(i,j)=V(j,i)= K * KV * IncompleteGamma.calcF2(K22*r2);
                }
            }

            /*
            // transform
            for (int i=0; i<MAU; ++i) {
                double cc[NAU];
                for (int l=0; l<NAU; ++l) {
                    double ww=0;
                    for (int k=0; k<NAU; ++k) ww+=Ld(i,k)*V(k,l);
                    cc[l] = ww;
                }
                for (int j=0; j<MAU; ++j) {
                    double jj=0;
                    for (int l=0; l<NAU; ++l) jj+=Ld(j,l)*cc[l];
                    X(k,  i,j) = jj;
                }
            }
            */

            for (int i=0; i<MAU; ++i)
                for (int j=0; j<MAU; ++j)
                    X(k,  i,j) = V(i,j);
        }

        cout << "done"<< endl;

        {char a; cin>>a;}

        const bool ComputeSVD = true;
        if (ComputeSVD) {
            tensor2 W(NAU,NAU);

            for (int k=0; k<NAU; ++k) {
                for (int l=0; l<=k; ++l) {
                    double ss  = 0;

                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<i; ++j)
                            ss += X(k,  i,j)*X(l,  i,j);
                    ss*=2;
                    for (int i=0; i<MAU; ++i) ss+=X(k,  i,i)*X(l,  i,i);

                    W(k,l) = W(l,k) = -ss;
                }
            }

            double e[NAU];

                char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
                char UL   = 'U'; // matrix storage pàtern
                int INFO;        // chack status
                int LWORK = NAU*NAU;  // size of working space
                double * WORK = new double[LWORK];   // nothing
                int SIZE = NAU;

                dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, e, WORK, &LWORK, &INFO);

            cout << "SVD^2  = ";  for (int i=0; i<NAU; ++i) cout << -e[i] << " "; cout << endl << endl;

            cout << "rotating SVD";

            for (int i=0; i<MAU; ++i)
                for (int j=0; j<=i; ++j) {
                    double ww[NAU];
                    for (int k=0; k<NAU; ++k) {
                        double ss = 0;
                        for (int l=0; l<NAU; ++l) {
                            ss+=W(k,l)*X(l,i,j);
                        }
                        ww[k] = ss;
                    }
                   for (int k=0; k<NAU; ++k) X(k, i,j) = ww[k];
            }

            for (int k=0; k<NAU; ++k)
                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<=i; ++j)
                        X(k, j,i) = X(k, i,j);



            {char a; cin>>a;}

        }


        // test eigenvalues
        // ================

        {
            // this is surprisingly fast; even if we end up using some HOSVD,
            // the regular rank decomposition may speed up the computationChe

            tensor2 E(NAU,NAU);
            double e[NAU];

            for (int k=0; k<NAU; ++k) {

                for (int i=0; i<MAU; ++i)
                    for (int j=0; j<MAU; ++j)
                        E(i,j) = -X(k,  i,j);

                char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
                char UL   = 'U'; // matrix storage pàtern
                int INFO;        // chack status
                int LWORK = NAU*NAU;  // size of working space
                double * WORK = new double[LWORK];   // nothing
                int SIZE = NAU;

                dsyev_ (&JOBZ, &UL, &SIZE, E.c2, &SIZE, e, WORK, &LWORK, &INFO);

                cout << k << " :: "; for (int i=0; i<NAU; ++i) cout << -e[i] << " "; cout << endl << endl;
            }

            return 0;
        }



        // PARAFAC, or SOMETHING

        // rand
        unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
        MTRand_int32 irand(init, length); // 32-bit int generator
        // this is an example of initializing by an array
        // you may use MTRand(seed) with any 32bit integer
        // as a seed for a simpler initialization
        MTRand drand; // double in [0, 1) generator, already init


        cout.precision(16);
        int it = 0;

        while (true) {
            ++it;

            cout << "Iteration : " << it << "    "; //<< endl;

            {
                double s2 = 0;

                for (int k=0; k<NAU; ++k)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            s2 += X(k,i,j)*X(k,i,j);
                cout << "Frobenius : " << s2; // << endl;
            }

            // naive algorithm : computes the largest PC with an SVD-like power
            // iteration and then the largest eigenpair
            // =================================================================
            if (0) {
                // generate a random vector
                double vv[NAU]; for (int i=0; i<NAU; ++i) vv[i] = drand();
                tensor2 SS(MAU);

                // normalize vv
                {
                    double s2=0;
                    for (int i=0; i<NAU; ++i) s2 += vv[i]*vv[i];
                    s2 = 1./sqrt(s2);
                    for (int i=0; i<NAU; ++i) vv[i]*=s2;
                }

                double sigma = 0;

                // maximum number of iterations just in case
                for (int n=0; n<12; ++n) {

                    double sv, ss;

                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j) {
                            double ss = 0;
                            for (int k=0; k<NAU; ++k) ss+=X(k,i,j)*vv[k];
                            SS(i,j) = ss;
                        }

                    // symmetrize (increase numerical stability)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<i; ++j) {
                            double ss = SS(i,j) + SS(j,i);
                            ss *= 0.5;
                            SS(i,j) = SS(j,i) = ss;
                        }

                    // normalize SS
                    {
                        double s2=0;
                        for (int ij=0; ij<MAU*MAU; ++ij) s2 += SS.c2[ij]*SS.c2[ij];
                        ss = sqrt(s2);
                        s2 = 1./ss;
                        SS*=s2;
                    }

                    for (int k=0; k<NAU; ++k) {
                        double ss = 0;
                        for (int i=0; i<MAU; ++i)
                            for (int j=0; j<MAU; ++j)
                                ss+=X(k,i,j)*SS(i,j);
                         vv[k] = ss;
                    }

                    // normalize vv
                    {
                        double s2=0;
                        for (int i=0; i<NAU; ++i) s2 += vv[i]*vv[i];
                        ss = sqrt(s2);
                        s2 = 1./ss;
                        for (int i=0; i<NAU; ++i) vv[i]*=s2;
                    }

                    if (fabs(sigma-ss)<ss*1e-14) break; // should learn when something is not going to converge further and skip anyways

                    sigma = ss;
                }

                cout << " sigma = " << sigma; // << endl;



                //cout << "SS matrix: " << endl;
                //cout << SS << endl << endl;


                //extract highest eigenvector

                /*

                double uu[MAU]; for (int i=0; i<MAU; ++i) uu[i] = drand();
                double lambda=0;
                cout << " lambda = ";

                while (true) {
                    double ev;

                    double tt[MAU];

                    for (int i=0; i<MAU; ++i) {
                        double ss = 0;
                        for (int j=0; j<MAU; ++j) ss+=SS(i,j)*uu[j];
                        tt[i] = ss;
                    }

                    // normalize uu
                    {
                        double s2=0;
                        for (int i=0; i<MAU; ++i) s2 += tt[i]*tt[i];
                        ev = sqrt(s2);
                        s2 = 1./ev;
                        for (int i=0; i<MAU; ++i) uu[i]=tt[i]*s2;
                    }
                    cout << ev << " ";

                    //{char a; cin>>a;}

                    if (fabs(lambda-ev)<ev*1e-12) break;

                    lambda = ev;
                }
                cout << endl;
                */

                double uu[MAU];
                double lambda=0;

                {
                    /* Locals */
                    int NSELECT = 1;
                    int n = MAU, il, iu, m, lda = MAU, ldz = MAU, info, lwork;
                    double abstol, vl, vu;
                    double wkopt;
                    double* work;
                    /* Local arrays */
               /* iwork dimension should be at least 5*n */
                    int iwork[5*MAU], ifail[MAU];
                    double w[MAU];


                    /* Executable statements */
                    //cout << ( " DSYEVX Example Program Results\n" );
                    /* Negative abstol means using the default value */
                    abstol = -1.0;
                    /* Set il, iu to compute NSELECT smallest eigenvalues */
                    il = iu = MAU;

                    /* Query and allocate the optimal workspace */
                    lwork = -1;
                    dsyevx_( "Vectors", "Indices", "Upper", &n, SS.c2, &lda, &vl, &vu, &il, &iu,
                                    &abstol, &m, w, uu, &ldz, &wkopt, &lwork, iwork, ifail, &info );
                    lwork = (int)wkopt;
                    work = new double[lwork];
                    /* Solve eigenproblem */
                    dsyevx_( "Vectors", "Indices", "Upper", &n, SS.c2, &lda, &vl, &vu, &il, &iu,
                                    &abstol, &m, w, uu, &ldz, work, &lwork, iwork, ifail, &info );
                    delete[] work;

                    lambda = w[0];
                }

                cout << " lambda : " << lambda << endl;



                for (int k=0; k<NAU; ++k)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            X(k,i,j) -= (sigma*lambda)*vv[k]*uu[i]*uu[j];
            }


            else {

                // generate a random vector
                double vv[NAU];
                double uu[MAU];
                tensor2 SS(MAU);


                // generate random vector
                for (int i=0; i<NAU; ++i) vv[i] = drand();

                // normalize vv
                {
                    double s2=0;
                    for (int i=0; i<NAU; ++i) s2 += vv[i]*vv[i];
                    s2 = 1./sqrt(s2);
                    for (int i=0; i<NAU; ++i) vv[i]*=s2;
                }


                double sigma  = 0;
                double lambda = 0;

                // maximum number of iterations just in case
                for (int n=0; n<128; ++n) {

                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j) {
                            double ss = 0;
                            for (int k=0; k<NAU; ++k) ss+=X(k,i,j)*vv[k];
                            SS(i,j) = ss;
                        }

                    // symmetrize (increase numerical stability)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<i; ++j) {
                            double ss = SS(i,j) + SS(j,i);
                            ss *= 0.5;
                            SS(i,j) = SS(j,i) = ss;
                        }


                    {
                        /* Locals */
                        int NSELECT = 1;
                        int n = MAU, il, iu, m, lda = MAU, ldz = MAU, info, lwork;
                        double abstol, vl, vu;
                        double wkopt;
                        double* work;
                        /* Local arrays */
                   /* iwork dimension should be at least 5*n */
                        int iwork[5*MAU], ifail[MAU];
                        double w[MAU];


                        /* Executable statements */
                        //cout << ( " DSYEVX Example Program Results\n" );
                        /* Negative abstol means using the default value */
                        abstol = -1.0;
                        /* Set il, iu to compute NSELECT smallest eigenvalues */
                        il = iu = MAU;

                        /* Query and allocate the optimal workspace */
                        lwork = -1;
                        dsyevx_( "Vectors", "Indices", "Upper", &n, SS.c2, &lda, &vl, &vu, &il, &iu,
                                        &abstol, &m, w, uu, &ldz, &wkopt, &lwork, iwork, ifail, &info );
                        lwork = (int)wkopt;
                        work = new double[lwork];
                        /* Solve eigenproblem */
                        dsyevx_( "Vectors", "Indices", "Upper", &n, SS.c2, &lda, &vl, &vu, &il, &iu,
                                        &abstol, &m, w, uu, &ldz, work, &lwork, iwork, ifail, &info );
                        delete[] work;

                        lambda = w[0];
                    }

                    cout << " lambda : " << lambda << endl;


                    // now the other side:

                    for (int k=0; k<NAU; ++k) {
                        double ss = 0;
                        for (int i=0; i<MAU; ++i)
                            for (int j=0; j<MAU; ++j)
                                ss+=X(k,i,j)*uu[i]*uu[j];
                         vv[k] = ss;
                    }

                    // normalize vv
                    {
                        double s2=0;
                        for (int i=0; i<NAU; ++i) s2 += vv[i]*vv[i];
                        sigma = sqrt(s2);
                        s2 = 1./sigma;
                        for (int i=0; i<NAU; ++i) vv[i]*=s2;
                    }

                    cout << " sigma = " << sigma << endl;

                    if (fabs(sigma-lambda)<sigma*1e-14) break; // should learn when something is not going to converge further and skip anyways
                }

                // update the tensor
                for (int k=0; k<NAU; ++k)
                    for (int i=0; i<MAU; ++i)
                        for (int j=0; j<MAU; ++j)
                            X(k,i,j) -= lambda*vv[k]*uu[i]*uu[j];
            }



        }

        return 0;
    }


    // CENTER THE SYSTEM;

    if (1) {

        // center
        {
            point O;

            O.x=O.y=O.z=0;

            for (int i=0; i<NAU; ++i) {
                O.x += au[i].x;
                O.y += au[i].y;
                O.z += au[i].z;
            }

            O.x/=NAU; O.y/=NAU; O.z/=NAU;

            for (int i=0; i<NAU; ++i) {
                au[i].x -= O.x;
                au[i].y -= O.y;
                au[i].z -= O.z;
            }
        }

        {
            double dxy, dxz, dyz, dw2, d00, r2;
            dxy=dxz=dyz=dw2=d00=r2=0;

            for (int i=0; i<NAU; ++i) {
                double x = au[i].x;
                double y = au[i].y;
                double z = au[i].z;
                dxy+=x*y;
                dxz+=x*z;
                dyz+=y*z;
                dw2+=x*x-y*y;
                d00+=z*z-0.5*(x*x+y*y);
                r2 +=z*z+x*x+y*y;
            }

            cout << r2 << endl;
            cout << dxy << " " << dxz << " " << dyz << " " << dw2 << " " << d00 << endl;
        }

        {char a; cin>>a;}


        tensor2 T(NAU+1);

        double v[NAU+1];

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                //T(i,j) = T(j,i) = r2; //-1/sqrt(r2);
                T(i,j) =  x;
                T(j,i) = -x;
            }
            T(i,i) = 0; //-1;

            T(NAU,i) =  1;
            T(i,NAU) = -1;
        }
        T(NAU,NAU) = 0;




        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int SIZE = NAU+1;
        int LWORK = SIZE*SIZE;  // size of working space
        double * WORK = new double[LWORK];   // nothing


        dsyev_ (&JOBZ, &UL, &SIZE, T.c2, &SIZE, v, WORK, &LWORK, &INFO);

        //for (int i=0; i<NAU; ++i) v[i]=-v[i]-1.;
        for (int i=0; i<SIZE; ++i) cout << v[i] << " "; cout << endl << endl;

        for (int i=0; i<SIZE; ++i) cout << T(i,NAU) << " "; cout << endl << endl;

        {char aaa; cin >> aaa;}

        tensor2 W(NAU);

        for (int i=0; i<NAU; ++i) {
            const double w1 = sqrt(0.03);
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);
                double r = sqrt(r2);

                W(i,j) = W(j,i) = erf(w1*r)/r ; // exp(-0.05*r2);
            }
            W(i,i) = 2./sqrt(PI) * w1;
        }

        tensor2 J(NAU);


        for (int i=0; i<NAU; ++i) {
            double ww=0;
            for (int l=0; l<NAU; ++l) {
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*T(i,l)*W(k,l);
            }
            v[i] = ww;
        }

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;


        dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, v, WORK, &LWORK, &INFO);

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;

        /*


        for (int i=0; i<NAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*W(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<NAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=T(j,l)*cc[l];
                J(i,j) = jj;
            }
        }

        cout.precision(4);
        cout.setf(ios::fixed);
        cout << endl << endl;
        cout << J << endl;
        */

        {char aaa; cin >> aaa;}




        // compute product in Fourier
        for (int n=0; n<NAU; ++n) {
            cout << n << " : ";
            for (int i=0; i<NAU; ++i) {
                    double s3=0;
                    for (int k=0; k<NAU; ++k) s3 += T(i,k)*T(i,k)*T(n,k);
                    s3*=sqrt(double(NAU));
                    cout << " " << s3;
            }
            char a; cin>>a;
            cout << endl;
        }


        return 0;

    }



    if (0) {

        char a; cin>>a;

        tensor2 T(NAU);
        double v[NAU];
        tensor2 W(NAU);

        for (int i=0; i<NAU; ++i) {
            const double w1 = sqrt(0.03);
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);
                double r = sqrt(r2);

                W(i,j) = W(j,i) = -erf(w1*r)/r ; // exp(-0.05*r2);
            }
            W(i,i) = -2./sqrt(PI) * w1;
        }

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int LWORK = NAU*NAU;  // size of working space
            double * WORK = new double[LWORK];   // nothing
            int SIZE = NAU;

            dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, v, WORK, &LWORK, &INFO);

            for (int i=0; i<NAU; ++i) v[i]=-v[i];
            for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;
        }


        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                T(i,j) = T(j,i) = 1/sqrt(r2);
            }
            T(i,i) = 1;
        }

        int NC = 1 + 3 + 5;

        tensor2 T2(NC);

        for (int i=0; i<NC; ++i) {
            for (int j=0; j<NC; ++j) {
                double ww=0;
                for (int l=0; l<NAU; ++l)
                    for (int k=0; k<NAU; ++k) ww+=W(i,k)*W(j,l)*T(k,l);
                T2(i,j) = ww;
            }
        }

        cout << T2 << endl << endl;


        {char aaa; cin >> aaa;}

        tensor2 S(NAU);

        for (int i=0; i<NAU; ++i) {
            const double w1 = sqrt(0.03);
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                S(i,j) = S(j,i) = exp(-w1*w1*r2) ; // exp(-0.05*r2);
            }
            S(i,i) = 1;
        }

        {
            char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
            char UL   = 'U'; // matrix storage pàtern
            int INFO;        // chack status
            int LWORK = NAU*NAU;  // size of working space
            double * WORK = new double[LWORK];   // nothing
            int SIZE = NAU;

            dsyev_ (&JOBZ, &UL, &SIZE, S.c2, &SIZE, v, WORK, &LWORK, &INFO);

            for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;
        }

        {char aaa; cin >> aaa;}


        tensor2 J(NAU);


        for (int i=0; i<NAU; ++i) {
            double ww=0;
            for (int l=0; l<NAU; ++l) {
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*T(i,l)*W(k,l);
            }
            v[i] = ww;
        }

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;


        //dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, v, WORK, &LWORK, &INFO);

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;

        /*


        for (int i=0; i<NAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*W(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<NAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=T(j,l)*cc[l];
                J(i,j) = jj;
            }
        }

        cout.precision(4);
        cout.setf(ios::fixed);
        cout << endl << endl;
        cout << J << endl;
        */

        {char aaa; cin >> aaa;}




        // compute product in Fourier
        for (int n=0; n<NAU; ++n) {
            cout << n << " : ";
            for (int i=0; i<NAU; ++i) {
                    double s3=0;
                    for (int k=0; k<NAU; ++k) s3 += T(i,k)*T(i,k)*T(n,k);
                    s3*=sqrt(double(NAU));
                    cout << " " << s3;
            }
            char a; cin>>a;
            cout << endl;
        }


        return 0;
    }

    if (0) {

        char a; cin>>a;

        tensor2 T(NAU+1);

        double v[NAU+1];

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);

                //T(i,j) = T(j,i) = r2; //-1/sqrt(r2);
                T(i,j) =  x;
                T(j,i) = -x;
            }
            T(i,i) = 0; //-1;

            T(NAU,i) =  1;
            T(i,NAU) = -1;
        }
        T(NAU,NAU) = 0;




        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int SIZE = NAU+1;
        int LWORK = SIZE*SIZE;  // size of working space
        double * WORK = new double[LWORK];   // nothing


        dsyev_ (&JOBZ, &UL, &SIZE, T.c2, &SIZE, v, WORK, &LWORK, &INFO);

        //for (int i=0; i<NAU; ++i) v[i]=-v[i]-1.;
        for (int i=0; i<SIZE; ++i) cout << v[i] << " "; cout << endl << endl;

        for (int i=0; i<SIZE; ++i) cout << T(i,NAU) << " "; cout << endl << endl;

        {char aaa; cin >> aaa;}

        tensor2 W(NAU);

        for (int i=0; i<NAU; ++i) {
            const double w1 = sqrt(0.03);
            for (int j=0; j<i; ++j) {

                double x = au[i].x - au[j].x;
                double y = au[i].y - au[j].y;
                double z = au[i].z - au[j].z;
                double r2 = (x*x+y*y+z*z);
                double r = sqrt(r2);

                W(i,j) = W(j,i) = erf(w1*r)/r ; // exp(-0.05*r2);
            }
            W(i,i) = 2./sqrt(PI) * w1;
        }

        tensor2 J(NAU);


        for (int i=0; i<NAU; ++i) {
            double ww=0;
            for (int l=0; l<NAU; ++l) {
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*T(i,l)*W(k,l);
            }
            v[i] = ww;
        }

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;


        dsyev_ (&JOBZ, &UL, &SIZE, W.c2, &SIZE, v, WORK, &LWORK, &INFO);

        for (int i=0; i<NAU; ++i) cout << v[i] << " "; cout << endl;

        /*


        for (int i=0; i<NAU; ++i) {
            double cc[NAU];
            for (int l=0; l<NAU; ++l) {
                double ww=0;
                for (int k=0; k<NAU; ++k) ww+=T(i,k)*W(k,l);
                cc[l] = ww;
            }
            for (int j=0; j<NAU; ++j) {
                double jj=0;
                for (int l=0; l<NAU; ++l) jj+=T(j,l)*cc[l];
                J(i,j) = jj;
            }
        }

        cout.precision(4);
        cout.setf(ios::fixed);
        cout << endl << endl;
        cout << J << endl;
        */

        {char aaa; cin >> aaa;}




        // compute product in Fourier
        for (int n=0; n<NAU; ++n) {
            cout << n << " : ";
            for (int i=0; i<NAU; ++i) {
                    double s3=0;
                    for (int k=0; k<NAU; ++k) s3 += T(i,k)*T(i,k)*T(n,k);
                    s3*=sqrt(double(NAU));
                    cout << " " << s3;
            }
            char a; cin>>a;
            cout << endl;
        }


        return 0;

    }




    IncompleteGamma.InitIncompleteGammaTable();

    {char aaa; cin >> aaa;}


    // overlap, kinetic energy, orthogonal basis
    tensor2 S(NAU);
    tensor2 T(NAU);
    tensor2 C(NAU);
    tensor2 D(NAU);

    // DF primitives' overlap and J interaction
    tensor2 S2(NAU);
    tensor2 V2(NAU);

    // tensors for V3 decomposition
    tensor2 G(NAU);
    tensor2 H(NAU);

    const double SG1 = sqrt(PI/(2*kw));
    const double SG3 = SG1*SG1*SG1;
    const double TG1 = 0.5*sqrt(PI*nu);

    const double SG22  = sqrt(0.5*K2/PI);
    const double K11   = sqrt(K22);


    const double GG1 = sqrt(2*kw*K2/(PI*(2*kw+K2)));
    const double GG3 = GG1*GG1*GG1;

    for (int i=0; i<NAU; ++i) {
        for (int j=0; j<=i; ++j) {

            double x = au[i].x - au[j].x;
            double y = au[i].y - au[j].y;
            double z = au[i].z - au[j].z;
            double r2 = (x*x+y*y+z*z);
            double r1 = sqrt(r2);

            S(i,j) = S(j,i)  = exp(-Kww*r2);
            T(i,j) = T(j,i) = 0.5*(6*Kww-4*Kww*Kww*r2)*exp(-Kww*r2);

            S2(i,j) = S2(j,i) = exp(-K22*r2) * SG22 * SG22 * SG22; // the bump is normalized to unity

            // negative so the eigenvalues appear sorted in descending magnitude
            V2(i,j) = V2(j,i) = -erf(K11*r1)/r1;
            G(i,j) = G(j,i) = -exp(-K3w*r2)*GG1;
            H(i,j) = H(j,i) = -exp(-K32*r2)*GG1;
        }
        V2(i,i) = -2/sqrt(PI) * K11;
    }

    double gg[NAU];
    double hh[NAU];

    const double TH = 1e-4;
    int NG = NAU;
    int NH = NAU;

    {
        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = NAU*NAU;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = NAU;

        dsyev_ (&JOBZ, &UL, &SIZE, G.c2, &SIZE, gg, WORK, &LWORK, &INFO);

        for (int i=0; i<NAU; ++i) gg[i] = -gg[i];
        double trg = 0;

        for (NG=0; NG<NAU; ++NG) {
            trg += gg[NG];
            if (gg[NG]<trg*TH) break;
        }


        cout << "G : " << NG << " : "; for (int i=0; i<NG; ++i) cout << gg[i] << " "; cout << endl << endl;

        delete[] WORK;
    }

    {
        char JOBZ = 'V' ; //'V'; // compute eigenvalues and eigenvectors
        char UL   = 'U'; // matrix storage pàtern
        int INFO;        // chack status
        int LWORK = NAU*NAU;  // size of working space
        double * WORK = new double[LWORK];   // nothing
        int SIZE = NAU;

        dsyev_ (&JOBZ, &UL, &SIZE, H.c2, &SIZE, hh, WORK, &LWORK, &INFO);
        for (int i=0; i<NAU; ++i) hh[i] = -hh[i];

        double trh = 0;

        for (NH=0; NH<NAU; ++NH) {
            trh += hh[NH];
            if (hh[NH]<trh*TH) break;
        }

        cout << "H : " << NH << " : "; for (int i=0; i<NH; ++i) cout << hh[i] << " "; cout << endl << endl;

        delete[] WORK;
    }

    {
        const double hg0=gg[0]*hh[0]*hh[0];
        const double th = hg0 * 1.e-8;

        int n3 = 0;

        for (int i=0; i<NAU; ++i) {
            for (int j=0; j<NAU; ++j) {
                for (int k=0; k<NAU; ++k) {
                    if (gg[i]*hh[j]*hh[k] > th) ++n3;
                }
            }
        }

        cout << "number of triples : " << n3 << endl;
    }



    double w[NAU];
    double vv[NAU];

    // compute eigensystem
    {
        int info, lwork;
        double wkopt;
        double * work;

        //int itype = 1; // A v = l B v
        int n = S.n;
        int itype = 1;

        C = T;
        D = S;

        lwork = -1;
        dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w, &wkopt, &lwork, &info );
        lwork = (int)wkopt;
        work = new double[lwork];

        dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w,   work, &lwork, &info );

        if (info>0) throw info; //meaningless error code

        delete[] work;

        cout << "T : "; for (int i=0; i<NAU; ++i) cout << w[i] << " "; cout << endl << endl;
    }

    {
        int info, lwork;
        double wkopt;
        double * work;

        int n = S2.n;
        int itype = 1;

        lwork = -1;
        dsygv_ (&itype, "V", "L", &n, V2.c2, &n, S2.c2, &n, vv, &wkopt, &lwork, &info );
        lwork = (int)wkopt;
        work = new double[lwork];

        dsygv_ (&itype, "V", "L", &n, V2.c2, &n, S2.c2, &n, vv,   work, &lwork, &info );
        for (int i=0; i<NAU; ++i) vv[i] = -vv[i];

        if (info>0) throw info; //meaningless error code

        delete[] work;

        cout << "V2: "; for (int i=0; i<NAU; ++i) cout << vv[i] << " "; cout << endl << endl;
    }


    tensor3 X(NAU,NG,NH);
    tensor3 Y(NAU,NH,NH);

    cout << "computing X :" << endl;

    for (int n=0; n<NAU; ++n) {
        for (int m=0; m<NG; ++m) {
            for (int l=0; l<NH; ++l) {
                double x3=0;
                for (int k=0; k<NAU; ++k) x3 += C(n,k) * G(m,k) * H(l,k);
                X(n,m,l) = x3* sqrt(gg[m]);
            }
        }
    }

    cout << "done" << endl;

    // this needs something more
    cout << "computing Y :" << endl;
    for (int n=0; n<NAU; ++n) {
        for (int m=0; m<NH; ++m) {
            for (int l=0; l<NH; ++l) {
                double y3=0;
                for (int k=0; k<NAU; ++k) y3 += V2(n,k) * H(m,k) * H(l,k);
                Y(n,m,l) = y3* sqrt(vv[n]) * hh[m] * hh[l];
            }
        }
    }
    cout << "done" << endl;


    // compute the norms
    tensor2 X2(NG,NH);


    for (int m=0; m<NG; ++m) {
        for (int l=0; l<NH; ++l) {
            double x2=0;

            for (int n=0; n<NAU; ++n) {
                x2 += X(n,m,l)*X(n,m,l);
            }
            X2(m,l) = x2;
        }
    }

    cout << "X norms:" << endl;
    cout << X2 << endl;


    return 0;


    {char aaa; cin >> aaa;}




    tensor2 V(NAU);
    tensor2 F(NAU);
    tensor2 J(NAU);

    tensor2 JJ(NAU);
    tensor2 XX(NAU);

    tensor2 W(NAU);
    tensor2 II(NAU);
    tensor2 AA(NAU);








    F = T; // initial guess

    // check triple product
    {
        // compute eigensystem
        {
            int info, lwork;
            double wkopt;
            double * work;

            //int itype = 1; // A v = l B v
            int n = S.n;
            int itype = 1;

            C = F;
            D = S;

            lwork = -1;
            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w, &wkopt, &lwork, &info );
            lwork = (int)wkopt;
            work = new double[lwork];

            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w,   work, &lwork, &info );

            if (info>0) throw info; //meaningless error code

            delete[] work;
        }


    }








    while(true) {
        // compute eigensystem
        {
            int info, lwork;
            double wkopt;
            double * work;

            //int itype = 1; // A v = l B v
            int n = S.n;
            int itype = 1;

            C = F;
            D = S;

            lwork = -1;
            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w, &wkopt, &lwork, &info );
            lwork = (int)wkopt;
            work = new double[lwork];

            dsygv_ (&itype, "V", "L", &n, C.c2, &n, D.c2, &n, w,   work, &lwork, &info );

            if (info>0) throw info; //meaningless error code

            delete[] work;
        }

        // print evs
        for (int k=0; k<NAU; ++k) cout << k << " " << w[k]<< endl;

        // build density matrix using Fermi-Dirac
        const double beta = 1000.; // inverse temp. (in au)

        cout << "beta: " << beta << endl;
        FDD(D, C, NAU/2, w, NAU, beta);


        for (int i=0; i<NAU; ++i) D(i,i)*=0.5; // so later we can use the triangle


        // check charge consistency
        {
            double qsum=0;

            for (int i=0; i<NAU; ++i)
                for (int j=0; j<=i; ++j)
                    qsum += D(i,j)*J(i,j);

            cout << "charge sum: " << qsum*SG3 << endl;
        }


        // assemble Fock

        F = T;
        //F += V;

        const double sqrtw = (PI*PI*PI)/(nu*nu*nu) * 2/sqrt(PI) * sqrt(.5*nu);


        cout << "Coulomb" << endl;
        JJ.zeroize();

        /*

        #pragma omp parallel for schedule(dynamic)
        for (int ij=0; ij<n2max; ++ij) {
            double sum = 0;

            for (int kl=0; kl<n2max; ++kl) {
                if (cww[ij].r2+cww[kl].r2 > r2max) break;

                double rx = cww[ij].x-cww[kl].x;
                double ry = cww[ij].y-cww[kl].y;
                double rz = cww[ij].z-cww[kl].z;

                double r2 = rx*rx+ry*ry+rz*rz;

                const int k = cww[kl].i;
                const int l = cww[kl].j;

                sum += D(k,l) * cww[kl].w* IncompleteGamma.calcF2(0.5*nu*r2);
            }
            const int i = cww[ij].i;
            const int j = cww[ij].j;
            JJ(i,j) += 2*2*cww[ij].w * sqrtw * sum;
        }

        F += JJ;
        */


        /*

        for (int i =0; i<NAU; ++i) {
            for (int j =0; j<NAU; ++j) {
                cout << JJ(i,j) << "   ";
            }
            cout << endl;
        }
        cout << endl;
        cout << endl;
        */

        cout << "Exchange" << endl;
        XX.zeroize();

        for (int i=0; i<NAU; ++i) D(i,i)*=2;

        // substracts exchage
        #pragma omp parallel for schedule(dynamic)
        for (int k=0; k<NAU; ++k) {
            for (int l=0; l<=k; ++l) {

                double sum = 0;

                double px = (au[k].x - au[l].x);
                double py = (au[k].y - au[l].y);
                double pz = (au[k].z - au[l].z);

                for (int i=0; i<NAU; ++i) {
                    if(J(i,k)<1e-10) continue;

                    double qx = px + au[i].x;
                    double qy = py + au[i].y;
                    double qz = pz + au[i].z;

                    for (int j=0; j<NAU; ++j) {

                        if(J(i,k)*J(j,l)<1e-12) continue;

                        double rx = qx - au[j].x;
                        double ry = qy - au[j].y;
                        double rz = qz - au[j].z;

                        double R2 = 0.25* (rx*rx+ry*ry+rz*rz);

                        sum += D(i,j) * (J(i,k)*J(j,l))  * IncompleteGamma.calcF2(.5*nu*R2);
                    }
                }
                XX(k,l) -= sqrtw *sum;
            }
        }

        /*
        // this should work if we account for symmetry
        #pragma omp parallel for schedule(dynamic)
        for (int ij=0; ij<n2max; ++ij) {
            double sum = 0;

            for (int kl=0; kl<n2max; ++kl) {
                if (cww[ij].r2+cww[kl].r2 > r2max) break;

                double rx = cww[ij].x-cww[kl].x;
                double ry = cww[ij].y-cww[kl].y;
                double rz = cww[ij].z-cww[kl].z;

                double r2 = rx*rx+ry*ry+rz*rz;

                const int i = cww[ij].i;
                const int j = cww[ij].j;
                const int k = cww[kl].i;
                const int l = cww[kl].j;

                double ii = sqrtw * cww[ij].w * cww[kl].w* IncompleteGamma.calcF2(0.5*nu*r2);

                XX(i,k) -= 2* ii * D(j,l);
                XX(i,l) -= 2* ii * D(j,k);
                XX(j,k) -= 2* ii * D(i,l);
                XX(j,l) -= 2* ii * D(i,k);
            }
        }
        */



        F += XX;

        /*
        for (int i =0; i<NAU; ++i) {
            for (int j =0; j<NAU; ++j) {
                cout << XX(i,j) << "   ";
            }
            cout << endl;
        }
        cout << endl;
        cout << endl;
        */


        cout << "Reform Fock matrix" << endl;

        for (int i=0; i<NAU; ++i)
            for (int j=0; j<=i; ++j)
                F(j,i) = F(i,j);
    }



    /*
    cout.precision(16);

    for (int i=0; i<NAU; ++i) cout << w[i] << ","; //cout << i << "   " <<w[i] << endl;

    // now compute a few diagonal

    (AA | II)  is much more substantial than (ia|ia) :
    (AA | II)  is of the same order always (two spread densities repelling)
    (AA | II)  reoughly depends on the size of the system

        int i= 254; //NAU/2-37;
    int a = 657; //NAU/2 +12;
    energy: 0.04218367535352032
A fock: 0.3788914117966032
diagonal energy: 0.1678987374153858

    int i= NAU/2-37;
    int a = NAU/2 +12;
energy: 0.03845997170256591
A fock: 0.04592285513884481
diagonal energy: 0.1574262890304308

    int i= NAU/4-37;
    int a = 3*NAU/4 +12;
energy: 0.0414706970925063
A fock: 0.465775286952472
diagonal energy: 0.1221114545421553

    int i = NAU/2-1;
    int a = NAU/2;
    aaii
    energy: 0.0379157390962336
A fock: 0.001537201252099774
diagonal energy: 0.1673628307033906
aiai
energy: 0.0007232736280711949
A fock: 0.001537201252099774

*/

    int i = NAU/2-1;
    int a = NAU/2;

    for (int p=0; p<NAU; ++p)
        cout << p << " " << T(i,p) << "   " << T(a,p) << endl;

    int c0,c5,c6,c7,c10,c15;
    c0=c5=c6=c7=c10=c15=0;


    for (int p=0; p<NAU; ++p) {
        for (int q=0; q<NAU; ++q) {
            W(p,q) = T(i,p)*T(a,q) * J(p,q);
            II(p,q) = T(i,p)*T(i,q) * J(p,q);
            AA(p,q) = T(a,p)*T(a,q) * J(p,q);
            if (fabs(W(p,q))>1) ++c0;
            if (fabs(W(p,q))>1e-7) ++c7;
            if (fabs(W(p,q))>1e-6) ++c6;
            if (fabs(W(p,q))>1e-5) ++c5;
            if (fabs(W(p,q))>1e-10) ++c10;
            if (fabs(W(p,q))>1e-15) ++c15;
        }
    }
    cout << endl;
    cout << c0 << endl;
    cout << c5 << endl;
    cout << c6 << endl;
    cout << c7 << endl;
    cout << c10 << endl;
    cout << c15 << endl;

    double jia = 0;

    double f00 = 1;
    double kkk = 0.5*sqrt(PI);

    for (int p=0; p<NAU; ++p)
        for (int q=0; q<p; ++q) {
            W(p,q) += W(q,p);
            II(p,q) += II(q,p);
            AA(p,q) += AA(q,p);
        }

    for (int p=0; p<NAU; ++p) {
        for (int q=0; q<=p; ++q) {
            if (fabs(W(p,q))<1e-10) continue;
            double pqx = 0.5* (au[p].x + au[q].x);
            double pqy = 0.5* (au[p].y + au[q].y);
            double pqz = 0.5* (au[p].z + au[q].z);
            for (int r=0; r<NAU; ++r) {
                for (int s=0; s<=r; ++s) {
                    if (fabs(W(p,q)*W(r,s))<1e-14) continue;
                    double rsx = 0.5*(au[r].x + au[s].x);
                    double rsy = 0.5*(au[r].y + au[s].y);
                    double rsz = 0.5*(au[r].z + au[s].z);

                    double x=rsx-pqx;
                    double y=rsy-pqy;
                    double z=rsz-pqz;

                    double wr = sqrt(w2*(x*x+y*y+z*z));

                    if (wr>0) jia += (W(p,q)*W(r,s))* kkk*erf(wr) / wr;
                    else     jia += (W(p,q)*W(r,s))*f00;
                }
            }
        }
    }

    jia *= 2*PI*PI*sqrt(PI)/(2*w2*2*w2*sqrt(4*w2));

    cout << "energy: " << jia << endl;
    cout << "A fock: " << w[a]-w[i] << endl;

    double jj = 0;

    for (int p=0; p<NAU; ++p) {
        for (int r=0; r<NAU; ++r) {
            double x=(au[r].x-au[p].x);
            double y=(au[r].y-au[p].y);
            double z=(au[r].z-au[p].z);

            double wr = sqrt(w2*(x*x+y*y+z*z));

            if (wr>0) jj += (W(p,p)*W(r,r))* kkk*erf(wr) / wr;
            else     jj += (W(p,p)*W(r,r))*f00;
        }
    }

    jj *= 2*PI*PI*sqrt(PI)/(2*w2*2*w2*sqrt(4*w2));


    cout << "diagonal energy: " <<   jj << endl;

    /*
        for (int i=0; i<NAU; ++i) {
        for (int j=0; j<NAU; ++j) {
            cout << W(i,j) << " ";
        }
        cout << endl;
    }
    cout << endl;
    cout << endl;
    */


    return 0;
}

