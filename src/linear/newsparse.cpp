#include "newsparse.hpp"
#include "tensors.hpp"
#include "math/angular.hpp"
using namespace LibAngular;

//THIS CLASS IS UBER CONFUSING FOR THE USER
//TAKE THE TIME TO ENCAPSULATE IT PROPERLY AND FIX THE CODE
//ALSO: ADD SPARSICITY
//*********************************************************

/*

r2tensor<uint32_t> sparsetensorpattern::a2pos; //posicion del bloque para cada atomo

uint16_t * sparsetensorpattern::alen = NULL; //length of the atom block
uint16_t * sparsetensorpattern::nfs = NULL;  //number of functions per atom type

//second level, GC function blocks
r2tensor< r2tensor<uint32_t> > sparsetensorpattern::f2pos;
r2tensor<uint16_t> sparsetensorpattern::flen; //length of the function block
r2tensor<uint8_t> sparsetensorpattern::js;
r2tensor<uint8_t> sparsetensorpattern::ms;

//third level, individual function blocks
r2tensor< r2tensor<r2tensor<uint32_t> > > sparsetensorpattern::j2pos;


//one has to store which 'element' every atom is so to avoid wasting too much space
uint16_t sparsetensorpattern::nids;
uint16_t * sparsetensorpattern::ids = NULL;

uint32_t sparsetensorpattern::natoms;
uint32_t sparsetensorpattern::nbasis;
uint32_t sparsetensorpattern::ntotal;
uint32_t sparsetensorpattern::ntotal2;


//first level, atom blocks
uint32_t * sparsetensorpattern::a1pos = NULL; //posiciones en el array lineal para cada atomo

//second level, GC function blocks
r2tensor<uint32_t> sparsetensorpattern::f1pos; //posiciones en el array lineal para cada atomo

//third level, individual function blocks
r2tensor<r2tensor<uint32_t> > sparsetensorpattern::j1pos;
*/

// offset of the atom-atom block from the beginning of the array
uint32_t sparsetensorpattern::GetOffset(uint32_t at1, uint32_t at2) const {
    return a2pos(at1, at2);
}

// offset starting from the beginning of the atom-atom block
uint32_t sparsetensorpattern::GetOffset(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2) const {
    uint16_t id1 = ids[at1];
    uint16_t id2 = ids[at2];
    return f2pos(id1,id2)(nf1,nf2);
}

/*
uint32_t sparsetensorpattern::GetOffset(uint32_t at1, uint32_t at2, uint16_t nf1, uint16_t nf2, uint16_t nj1, uint16_t nj2) {
    uint16_t id1 = ids[at1];
    uint16_t id2 = ids[at2];

    return f2pos(id1,id2)(nf1,nf2);
}
*/

// offset of the atom block from the beginning of the array
uint32_t sparsetensorpattern::GetOffset1(uint32_t at1) const {
    return a1pos[at1];
}

// offset of the atom block from the beginning of the array
uint32_t sparsetensorpattern::GetOffset1(uint32_t at1, uint16_t nf1) const {
    uint16_t id1 = ids[at1];
    return f1pos(id1,nf1);
}

uint32_t sparsetensorpattern::GetLen1(uint32_t at1) const {
    uint16_t id1 = ids[at1];
    return alen[id1];
}


static inline uint32_t Tpos (uint32_t M1, uint32_t M2, uint32_t v1, uint32_t v2) {
    return v2*M1 + v1;
}


Sparse & Sparse::GetSymmetric(const tensor2 & rhs) {

    if (values==NULL) return *this; //Set();

    //foreach atom
    for (uint32_t ati=0; ati<P->natoms; ++ati) {
        for (uint32_t atj=0; atj<P->natoms; ++atj) {

            uint16_t id1   = P->ids[ati];
            uint16_t id2   = P->ids[atj];

            double * offset1 = values + P->a2pos[ati][atj];

            uint32_t p1 = P->a1pos[ati];
            uint32_t q1 = P->a1pos[atj];

            for (int b1=0; b1<P->nfs[id1]; ++b1) {
                for (int b2=0; b2<P->nfs[id2]; ++b2) {
                    double * offset2 = offset1 + P->f2pos[id1][id2][b1][b2];

                    uint32_t p2 = p1 + P->f1pos[id1][b1];
                    uint32_t q2 = q1 + P->f1pos[id2][b2];

                    uint32_t mj1 = P->js[id1][b1];
                    uint32_t mj2 = P->js[id2][b2];
                    uint32_t mm1 = P->ms[id1][b1];
                    uint32_t mm2 = P->ms[id2][b2];

                    for (uint8_t j1=0; j1<mj1; ++j1) {
                        for (uint8_t j2=0; j2<mj2; ++j2) {
                            double * offset3 = offset2  +  (j1*mj2+j2)*(mm1*mm2);

                            uint32_t p3 = p2 + j1*mm1;
                            uint32_t q3 = q2 + j2*mm2;


                            for (uint8_t m1=0; m1<mm1; ++m1) {
                                for (uint8_t m2=0; m2<mm2; ++m2) {
                                    uint32_t Ap = Tpos(mm1,mm2, m1,m2);

                                    uint32_t p4 = p3 + m1;
                                    uint32_t q4 = q3 + m2;

                                    (*this)(ati,atj, b1,b2, j1,j2)[Ap] = 0.5*(rhs(p4, q4) + rhs(q4,p4));
                                }
                            }

                        }
                    }

                }
            }

        }
    }

    return *this;
}

Sparse & Sparse::GetAntiSymmetric(const tensor2 & rhs) {

    if (values==NULL) return *this; //Set();

    //foreach atom
    for (uint32_t ati=0; ati<P->natoms; ++ati) {
        for (uint32_t atj=0; atj<P->natoms; ++atj) {

            uint16_t id1   = P->ids[ati];
            uint16_t id2   = P->ids[atj];

            double * offset1 = values + P->a2pos[ati][atj];

            uint32_t p1 = P->a1pos[ati];
            uint32_t q1 = P->a1pos[atj];

            for (int b1=0; b1<P->nfs[id1]; ++b1) {
                for (int b2=0; b2<P->nfs[id2]; ++b2) {
                    double * offset2 = offset1 + P->f2pos[id1][id2][b1][b2];

                    uint32_t p2 = p1 + P->f1pos[id1][b1];
                    uint32_t q2 = q1 + P->f1pos[id2][b2];

                    uint32_t mj1 = P->js[id1][b1];
                    uint32_t mj2 = P->js[id2][b2];
                    uint32_t mm1 = P->ms[id1][b1];
                    uint32_t mm2 = P->ms[id2][b2];

                    for (uint8_t j1=0; j1<mj1; ++j1) {
                        for (uint8_t j2=0; j2<mj2; ++j2) {
                            double * offset3 = offset2  +  (j1*mj2+j2)*(mm1*mm2);

                            uint32_t p3 = p2 + j1*mm1;
                            uint32_t q3 = q2 + j2*mm2;


                            for (uint8_t m1=0; m1<mm1; ++m1) {
                                for (uint8_t m2=0; m2<mm2; ++m2) {
                                    uint32_t Ap = Tpos(mm1,mm2, m1,m2);

                                    uint32_t p4 = p3 + m1;
                                    uint32_t q4 = q3 + m2;

                                    (*this)(ati,atj, b1,b2, j1,j2)[Ap] = 0.5*(rhs(p4, q4) - rhs(q4,p4));
                                }
                            }

                        }
                    }

                }
            }

        }
    }

    return *this;
}


Sparse & Sparse::operator=(const tensor2 & rhs) {

    if (values==NULL) return *this; //Set(*rhs.P);

    //foreach atom
    for (uint32_t ati=0; ati<P->natoms; ++ati) {
        for (uint32_t atj=0; atj<P->natoms; ++atj) {

            uint16_t id1   = P->ids[ati];
            uint16_t id2   = P->ids[atj];

            double * offset1 = values + P->a2pos[ati][atj];

            uint32_t p1 = P->a1pos[ati];
            uint32_t q1 = P->a1pos[atj];

            for (int b1=0; b1<P->nfs[id1]; ++b1) {
                for (int b2=0; b2<P->nfs[id2]; ++b2) {
                    double * offset2 = offset1 + P->f2pos[id1][id2][b1][b2];

                    uint32_t p2 = p1 + P->f1pos[id1][b1];
                    uint32_t q2 = q1 + P->f1pos[id2][b2];

                    uint32_t mj1 = P->js[id1][b1];
                    uint32_t mj2 = P->js[id2][b2];
                    uint32_t mm1 = P->ms[id1][b1];
                    uint32_t mm2 = P->ms[id2][b2];

                    for (uint8_t j1=0; j1<mj1; ++j1) {
                        for (uint8_t j2=0; j2<mj2; ++j2) {
                            double * offset3 = offset2  +  (j1*mj2+j2)*(mm1*mm2);

                            uint32_t p3 = p2 + j1*mm1;
                            uint32_t q3 = q2 + j2*mm2;


                            for (uint8_t m1=0; m1<mm1; ++m1) {
                                for (uint8_t m2=0; m2<mm2; ++m2) {
                                    uint32_t Ap = Tpos(mm1,mm2, m1,m2);

                                    uint32_t p4 = p3 + m1;
                                    uint32_t q4 = q3 + m2;

                                    (*this)(ati,atj, b1,b2, j1,j2)[Ap] = rhs(p4, q4);
                                }
                            }

                        }
                    }

                }
            }


        }
    }

    return *this;
}

Sparse & Sparse::operator=(const Sparse & rhs) {
    if (values==NULL) Set(*rhs.P);
    for (int i=0; i<P->ntotal2; ++i) values[i] = rhs.values[i];
    return *this;
}

Sparse & Sparse::operator+=(const Sparse & rhs) {
    if (values==NULL) {Set(*rhs.P); zeroize();}
    for (int i=0; i<P->ntotal2; ++i) values[i] += rhs.values[i];
    return *this;
}

Sparse & Sparse::operator-=(const Sparse & rhs) {
    if (values==NULL) {Set(*rhs.P); zeroize();}
    for (int i=0; i<P->ntotal2; ++i) values[i] -= rhs.values[i];
    return *this;
}

Sparse & Sparse::operator*=(double rhs) {
    if (values==NULL) return *this;
    for (int i=0; i<P->ntotal2; ++i) values[i] *= rhs;
    return *this;
}

Sparse & Sparse::operator/=(double rhs) {
    if (values==NULL) return *this;
    double irhs = 1./rhs;
    for (int i=0; i<P->ntotal2; ++i) values[i] *= irhs;
    return *this;
}


//wont work
/*
void TestSMT () {
    r1tensor<r1tensor<int> > A,B;
    r2tensor<r2tensor<int> > C;
    C = A * B;
}
*/

/*
Sparse & Sparse::operator=(const tensor & rhs) {
    if (values==NULL) Set();

    zeroize();


    //foreach atom
    for (uint32_t ati=0; ati<natoms; ++ati) {
        for (uint32_t atj=0; atj<natoms; ++atj) {

            uint16_t id1   = ids[ati];
            uint16_t id2   = ids[atj];

            double * offset1 = values + a2pos[ati][atj];

            const tensor2 * block = rhs(ati,atj);

            if (block==NULL) continue;


            for (int b1=0; b1<nfs[id1]; ++b1) {
                for (int b2=0; b2<nfs[id2]; ++b2) {
                    double * offset2 = offset1 + f2pos[id1][id2][b1][b2];

                    uint32_t p2 = f1pos[id1][b1];
                    uint32_t q2 = f1pos[id2][b2];

                    uint32_t mj1 = js[id1][b1];
                    uint32_t mj2 = js[id2][b2];
                    uint32_t mm1 = ms[id1][b1];
                    uint32_t mm2 = ms[id2][b2];

                    for (uint8_t j1=0; j1<mj1; ++j1) {
                        for (uint8_t j2=0; j2<mj2; ++j2) {
                            double * offset3 = offset2  +  (j1*mj2+j2)*(mm1*mm2);

                            uint32_t p3 = p2 + j1*mm1;
                            uint32_t q3 = q2 + j2*mm2;


                            for (uint8_t m1=0; m1<mm1; ++m1) {
                                for (uint8_t m2=0; m2<mm2; ++m2) {
                                    uint32_t Ap = Tpos(mm1,mm2, m1,m2);

                                    uint32_t p4 = p3 + m1;
                                    uint32_t q4 = q3 + m2;

                                    (*this)(ati,atj, b1,b2, j1,j2)[Ap] = (*block)(p4, q4);
                                }
                            }

                        }
                    }

                }
            }


        }
    }

    return *this;
}
*/
