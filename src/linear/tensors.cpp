#include <cmath>
#include "eigen.hpp"
#include "tensors.hpp"
#include "newtensors.hpp"
#include "newsparse.hpp"
#include "low/MPIwrap.hpp"
#include "low/chrono.hpp"

#ifdef _OPENMP
    #include <omp.h>
#else
    #define omp_get_thread_num() 0
    #define omp_get_max_threads() 1
#endif

using namespace std;

tensor2::tensor2() {
    n=m=0; ldm=0;
    c=NULL; c2=NULL;
}

tensor2::tensor2(int64_t p) {
    n=m=0; ldm=0;
    c=NULL; c2=NULL;
    setsize(p);
    flags |= FLAG_SQUARE;
}

tensor2::tensor2(int64_t p, int64_t q) {
    n=m=0; ldm=0;
    c=NULL; c2=NULL;
    setsize(p,q);
}

tensor2::tensor2(int64_t p, double * v) {
    setsize(p,p,v);
    flags |= FLAG_DUMMY;
    flags |= FLAG_SQUARE;
}

tensor2::tensor2(int64_t p, int64_t q, double * v) {
    setsize(p,q,v);
    flags |= FLAG_DUMMY;
}

// this does not work, whereas initializing from a const tensor does.
/*
tensor2::tensor2(int64_t p, int64_t q, const double * v) {
    //setsize(p,q,v);

    n=p;
    ldm=m=q;
    c2=NULL;
    c=new double*[n];
    for (int64_t i=0;i<n;++i) c[i] = v + i*m;

    flags |= FLAG_CONST;
    flags |= FLAG_DUMMY;
}
*/

tensor2::tensor2(int64_t p, const tensor2 & T, int64_t i0) {
    n=p; m=T.m;
    ldm=T.ldm;
    c2=NULL;
    c=T.c+i0;
    flags |= FLAG_CONST;
    flags |= FLAG_DUMMY;
}

tensor2::~tensor2() {
    if ((flags & FLAG_CONST)==0) delete[] c; // c is owned by some other tensor
    if ((flags & FLAG_DUMMY)==0) delete[] c2;
}

tensor2::tensor2(const tensor2& TensorIn) {
    n=m=0; ldm=0;
    c=NULL; c2=NULL;
    *this = TensorIn;
}

void tensor2::Transpose() {

    if (n==m) {
        for (int64_t p=0; p<n; ++p) {
            for (int64_t q=0; q<p; ++q) {
                std::swap(c[p][q], c[q][p]);
            }
        }
    }

    else {
        tensor2 R(m,n);

        for (int64_t p=0; p<n; ++p)
            for (int64_t q=0; q<m; ++q)
                R.c[q][p] = c[p][q];

        double * p = c[0];

        {
            std::swap (  n,   m );
            ldm = m;

            delete[] c;
            c = new double*[n];

            for (int64_t i=0;i<n;++i)
                c[i] = p + i*m;
        }

        (*this) = R;


        /*

        std::swap (  n,   m );
        //std::swap (ldn, ldm );
        ldm = m;

        // changed this due to
        // bizarre memory bug,
        // possibly unrelated

        delete[] c2;
        c2 = R.c2;
        R.c2 = NULL;

        delete[] c;
        c = R.c;
        R.c = NULL;

        // R's deestructor will actually destroy the old c & c2
        */
    }
}

void tensor2::setsize(int64_t p) {
    setsize(p, p);
    flags |= FLAG_SQUARE;
}

void tensor2::setsize(int64_t p, int64_t q) {
    if (n==p && m==q) return;

    if (n!=0 || m!=0) {
        delete[] c2;
	    delete[] c;
    }

	n = p;
	ldm = m = q;

	c  = new double*[n];
    c2 = new double[n*m];

	for (int64_t i=0;i<n;++i)
		c[i] = c2 + i*m;

    flags = 0;
}

void tensor2::setsize(int64_t p, int64_t q, double * v) {
    n=p;
    ldm=m=q;
    c2=NULL;
    c=new double*[n];
    for (int64_t i=0;i<n;++i) c[i] = v + i*m;
}

/*
void tensor2::setsize(int64_t p, int64_t q, const double * v) {
    n=p;
    ldm=m=q;
    c2=NULL;
    c=new const double*[n];
    for (int64_t i=0;i<n;++i) c[i] = v + i*m;
}
*/


void tensor2::setsize(const tensor2 & R) {
    setsize(R.n,R.m);
}

void tensor2::clear() {
	delete[] c2;
	delete[] c;
	c2=NULL;
	c =NULL;
	n=0;
	ldm=m=0;
}

tensor2 & tensor2::operator=(const symtensor2 & st) {

    setsize(st.n);

    // copy the values
    for (int64_t i=0;i<n;++i)
        for (int64_t j=0;j<n;++j)
            c[i][j] = st(i,j);

    return *this;
}

double tensor2::trace() const {
    double r = 0;
    for (int i=0; i<min(n,m); ++i) r += c[i][i];
    return r;
}


static inline uint32_t Tpos (uint32_t M1, uint32_t M2, uint32_t v1, uint32_t v2) {
    return v2*M1 + v1;
}

tensor2 & tensor2::operator=(const Sparse & rhs) {

    const sparsetensorpattern & stp = *rhs.P;

    if (n!=stp.ntotal) {
        if (n>0)
            clear();
        setsize(stp.ntotal);
    }

    if (rhs.values==NULL) {
        return *this;
    }

    //foreach atom
    for (uint32_t ati=0; ati<stp.natoms; ++ati) {
        for (uint32_t atj=0; atj<stp.natoms; ++atj) {

            uint16_t id1   = stp.ids[ati];
            uint16_t id2   = stp.ids[atj];

            double * offset1 = rhs.values + stp.a2pos[ati][atj];

            uint32_t p1 = stp.a1pos[ati];
            uint32_t q1 = stp.a1pos[atj];


            for (int b1=0; b1<stp.nfs[id1]; ++b1) {
                for (int b2=0; b2<stp.nfs[id2]; ++b2) {
                    double * offset2 = offset1 + stp.f2pos[id1][id2][b1][b2];

                    uint32_t p2 = p1 + stp.f1pos[id1][b1];
                    uint32_t q2 = q1 + stp.f1pos[id2][b2];

                    uint32_t mj1 = stp.js[id1][b1];
                    uint32_t mj2 = stp.js[id2][b2];
                    uint32_t mm1 = stp.ms[id1][b1];
                    uint32_t mm2 = stp.ms[id2][b2];

                    for (uint8_t j1=0; j1<mj1; ++j1) {
                        for (uint8_t j2=0; j2<mj2; ++j2) {
                            double * offset3 = offset2  +  (j1*mj2+j2)*(mm1*mm2);

                            uint32_t p3 = p2 + j1*mm1;
                            uint32_t q3 = q2 + j2*mm2;


                            for (uint8_t m1=0; m1<mm1; ++m1) {
                                for (uint8_t m2=0; m2<mm2; ++m2) {
                                    uint32_t Ap = Tpos(mm1,mm2, m1,m2);

                                    uint32_t p4 = p3 + m1;
                                    uint32_t q4 = q3 + m2;

                                    (*this)(p4, q4) = rhs(ati,atj, b1,b2, j1,j2)[Ap];
                                }
                            }

                        }
                    }

                }
            }

        }
    }

    return *this;
}

// copy a section of the tensor specified by the index arrays
tensor2 tensor2::operator() (const int * i, const int * j, int N, int M) const {
    tensor2 R(N,M);

    for (int n=0; n<N; ++n) {
        for (int m=0; m<M; ++m) {
            R(n,m) = (*this)(i[n],j[m]);
        }
    }

    return R;
}

std::ostream & operator<<(std::ostream & os, const tensor2 & T) {
    for (int i=0; i<T.n; ++i) {
        for (int j=0; j<T.m; ++j) {
            os << T(i,j) << " ";
        }
        os << std::endl << std::endl;
    }
    return os;
}

symtensor2::symtensor2 () {
    n=n2=0;
    c=NULL;
    c2=NULL;
}

symtensor2::symtensor2 (int64_t m) {
    n=n2=0;
    c=NULL;
    c2=NULL;
    setsize(m);
}

// recast an array as a symtensor
symtensor2::symtensor2 (int64_t m, double * v) {

	n = m;
	n2 = (n*n+n)/2;

	c2 = NULL;
	c  = new double*[n];

	// assign pointers
	double * p = v;

	for (uint64_t i=0; i<n; ++i) {
		c[i] = p;
		p += i+1;
	}
}

symtensor2::~symtensor2 () {
    delete[] c;
    delete[] c2;
}

void symtensor2::setsize(int m) {
    if (n==m) return;

    if (n!=0) {
        delete[] c2;
        delete[] c;
    }

	n = m;
	n2 = (n*n+n)/2;

	c2 = new double[n2];
	c  = new double*[n];

	// assign pointers
	double * p = c2;

	for (uint64_t i=0; i<n; ++i) {
		c[i] = p;
		p += i+1;
	}
}

void symtensor2::clear() {
	delete[] c;
	delete[] c2;
	c2 = NULL;
	c  = NULL;
	n  = 0;
	n2 = 0;
}

symtensor2 & symtensor2::operator=(const symtensor2 & st) {
    if (this == &st) return *this;

    setsize(st.n);

    // make a hard copy
    for (uint64_t i=0;i<n2;++i) c[0][i] = st.c[0][i];

    return *this;
}

symtensor2 & symtensor2::operator=(const tensor2 & st) {

    setsize(st.n);

    // copy the values
    for (uint64_t i=0;i<n;++i)
        for (uint64_t j=0; j<=i; ++j)
            c[i][j] = st.c[i][j];

    return *this;
}

symtensor2 & symtensor2::operator+=(const symtensor2 & st) {
    for (uint64_t i=0;i<n2;++i) c[0][i] += st.c[0][i];
    return *this;
}

symtensor2 & symtensor2::operator-=(const symtensor2 & st) {
    for (uint64_t i=0;i<n2;++i) c[0][i] -= st.c[0][i];
    return *this;
}

symtensor2 & symtensor2::operator=(double s) {
    zeroize();
    for (uint64_t i=0;i<n;++i)
        (*this)(i,i) = s;
    return *this;
}

symtensor2 & symtensor2::operator+=(double s) {
    for (uint64_t i=0;i<n;++i)
        (*this)(i,i) += s;
    return *this;
}

symtensor2 & symtensor2::operator*=(double s) {
    for (uint64_t i=0;i<n2;++i) c[0][i] *= s;
    return *this;
}

//this can't 'be right
const symtensor2 symtensor2::operator*(const symtensor2 & rhs) const {
    symtensor2 result;

    result.setsize(n);

    //#pragma omp parallel for schedule(dynamic)
    for (uint64_t i=0; i<n; ++i) {
        for (uint64_t j=0; j<=i; ++j) {
            double sum = 0;
            for (uint64_t k=0; k<n; ++k)
                sum += (*this)(i,k)*rhs(k,j);
            result(i,j) = sum;
        }
    }

    return result;
}

std::ostream & operator<<(std::ostream & os, const symtensor2 & T) {
    for (int i=0; i<T.n; ++i) {
        for (int j=0; j<T.n; ++j) {
            os << T(i,j) << " ";
        }
        os << std::endl << std::endl;
    }
    return os;
}

//
//  A PARALLEL TENSOR CLASS SHOULD ENCAPSULATE THIS BEHAVIOUR
//

void Tensor2Broadcast(tensor2 & T) {

  #ifdef C_MPI
    NodeGroup.Sync();
    long long int n=T.n, m=T.m;
    NodeGroup.Broadcast (n);
    NodeGroup.Broadcast (m);
    T.setsize(n,m); // this is ignored if T is already set to n,m; initialize otherwise
    NodeGroup.Broadcast (T.c[0], n*m);
    NodeGroup.Sync();
  #endif
}

void Tensor2Broadcast(symtensor2 & T) {

  #ifdef C_MPI
    NodeGroup.Sync();
    int n=T.n;
    NodeGroup.Broadcast (n);
    T.setsize(n); // this is ignored if T is already set to n; initialize otherwise
    NodeGroup.Broadcast (T.c[0], T.n2);
    NodeGroup.Sync();
  #endif
}

void Tensor2Reduce(tensor2 & T) {

    NodeGroup.AllReduce(T.c[0], T.n*T.m); // in-place reduce-scatter
}

void Tensor2Reduce(symtensor2 & T) {

     NodeGroup.AllReduce(T.c[0], T.n2); // in-place reduce-scatter
}
