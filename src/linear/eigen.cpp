/*
    eigen.cpp:     Wrappers for several dense linear algebra routines
*/

#include <iostream>
#include <string>
#include <cstring>
#include <cmath>

#include "eigen.hpp"
#include "tensors.hpp"
#include "low/sort.hpp"
#include "low/strings.hpp"
#include "fiesta/fiesta.hpp"

using namespace std;
using namespace Fiesta;

//
//  LAPACK-like matrix operations
//

// symmetric eigenvalue
extern "C" void dsyev_(                   const char * jobz, const char * uplo, const int * n, double * a, const int * lda,                              double * w, double * work, const int * lwork, int * info);
// generalized symmetric eigenvalue
extern "C" void dsygv_(const int * itype, const char * jobz, const char * uplo, const int * n, double * a, const int * lda, double * b, const int * ldb, double * w, double * work, const int * lwork, int * info);
// basic Cholesky
extern "C" void dpotrf_(char * uplo, int * n, double * a, int * lda, int * info);
// Cholesky with pivoting
extern "C" void dpstrf_ ( char * UL, int * N, double * A, int * LDA, int * piv,
                        int * rank, double * tol, double * work, int * info);
// singular value decomposition
extern "C" void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
                    int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
                    double* work, int* lwork, int* info );
// triangular matrix inversion
extern "C" void dtrtri_ ( char * UL, char * DI, int * N, double * A, int * LDA, int * info);

// DSPTRF computes the factorization of a real symmetric matrix A stored in packed format using
// the Bunch-Kaufman diagonal pivoting method
extern "C" void dsptrf_( const char* uplo, const int* n, double* ap, int* ipiv, int* info );

// DSPTRS solves a system of linear equations Ax = b with a real symmetric matrix A stored in
// packed format using the factorization computed by DSPTRF
extern "C" void dsptrs_( const char* uplo, const int* n, const int* nrhs, const double* ap,
                         const int* ipiv, double* b, const int* ldb, int* info );

// same as DSPTRF, unpacked
extern "C" void dsytrf_( const char* uplo, const int* n, double* ap, int * lda, int* ipiv, double * work, const int * lwork, int* info );

// same as DSPTRS, unpacked
extern "C" void dsytrs_( const char* uplo, const int* n, const int* nrhs, const double* ap, int * lda,
                         const int* ipiv, double* b, const int* ldb, int* info );

// DAXPY constant times a vector plus a vector
extern "C" void daxpy_ ( const int* n, const double* alpha, const double* x, const int* incx,
                         double* y, const int* incy );

// DDOT forms the dot product of two vectors.
extern "C" double ddot_( const int* n, const double* x, const int* incx, const double* y, const int* incy );

// nonsymmetric EV
extern "C" double dgeev_( const char*  JOBVL, const char* JOBVR, int * N, double * A, int * LDA, double * WR, double * WI, double * VL, int * LDVL, double * VR, int * LDVR, double * WORK, int * LWORK, int * INFO );

// LU decomposition with partial pivoting
extern "C" double dgetrf_(int * m, int * n, double * A, int * lda, int * ipiv, int * info);

// invert a matrix factorized using LU
extern "C" double dgetri_(int * n, double * A, int * lda, int * ipiv, double *work, int * lwork, int * info);


void DiagonalizeV  (tensor2 & A, double * w, bool descending)  {

    // used for low rank decomposition
    // of SDP matrices
    if (descending) A *= -1.;

    {
        // on input, A contains the projected hamiltonian
        int info, lwork;
        double wkopt;
        double * work;

        int n = A.n;

        const char JOBZ = 'V';// compute eigenvalues and eigenvectors
        const char UL   = 'L'; // matrix storage p�tern

        lwork = -1;
        dsyev_( &JOBZ, &UL, &n, A.c[0], &n, w, &wkopt, &lwork, &info );
        lwork = (int)wkopt;
        work = new double[lwork];

        dsyev_( &JOBZ, &UL, &n, A.c[0], &n, w,   work, &lwork, &info );

        if (info>0) {
            cout << endl << "Warning; "  << info << " eigenpairs failed to converge in dsyev_" << endl;
            //throw 1412; //meaningless error code
        }

        delete[] work;
        // on output, A contains the transposed matrix of orthogonal vectors
    }

    // adjust the eigenvalues back
    if (descending) {
        tensor2 v (1, A.n, w);
        v *= -1.;
    }
}

void DiagonalizeE  (const tensor2 & B, double * w, bool descending)  {

    tensor2 A;
    A = B;

    if (descending) A *= -1.;

    {
        // on input, A contains the projected hamiltonian
        int info, lwork;
        double wkopt;
        double * work;

        int n = A.n;

        const char JOBZ = 'N'; // compute eigenvalues only
        const char UL   = 'L'; // matrix storage p�tern

        lwork = -1;
        dsyev_( &JOBZ, &UL, &n, A.c[0], &n, w, &wkopt, &lwork, &info );
        lwork = (int)wkopt;
        work = new double[lwork];

        dsyev_( &JOBZ, &UL, &n, A.c[0], &n, w,   work, &lwork, &info );

        if (info>0) {
            cout << endl << "Warning; "  << info << " eigenpairs failed to converge in dsyev_" << endl;
        }

        delete[] work;
    }

    // adjust the eigenvalues back
    if (descending) {
        tensor2 v (1, A.n, w);
        v *= -1.;
    }
}

void DiagonalizeGV (tensor2 & A, tensor2 & B, double * w, int itype)  {

    int info, lwork;
    double wkopt;
    double * work;

    //int itype = 1; // A v = l B v
    int n = A.n;

    const char JOBZ = 'V'; // compute eigenvalues and eigenvectors
    const char UL   = 'L'; // matrix storage p�tern

    lwork = -1;
    dsygv_ (&itype, &JOBZ, &UL, &n, A.c[0], &n, B.c[0], &n, w, &wkopt, &lwork, &info );
    lwork = (int)wkopt;
    work = new double[lwork];

    dsygv_ (&itype, &JOBZ, &UL, &n, A.c[0], &n, B.c[0], &n, w,   work, &lwork, &info );

    if (info>0) throw info; //meaningless error code

    delete[] work;
}


void DiagonalizeNE  (const tensor2 & A, double * wr, double * wi)  {


    tensor2 W;
    W = A;

    {
        //on input, A contains the projected hamiltonian
        int info, lwork;
        double wkopt;
        double * work;

        int n = A.n;

        const char JOBV = 'N';// do not compute eigenvectors

        lwork = -1;
        dgeev_( &JOBV, &JOBV, &n, W[0], &n, wr, wi, NULL, &n, NULL, &n, &wkopt, &lwork, &info); // query for optimal space
        lwork = (int)wkopt;
        work = new double[lwork];

        dgeev_( &JOBV, &JOBV, &n, W[0], &n, wr, wi, NULL, &n, NULL, &n, work, &lwork, &info); // compute

        if (info>0) {
            cout << endl << "Warning; "  << info << " eigenpairs failed to converge in dgeev_" << endl;
            //throw 1412; //meaningless error code
        }

        delete[] work;
        //on output, A contains the transposed matrix of orthogonal vectors
    }

}


void Cholesky(tensor2 & A) {

    int info;

    char UL   = 'L'; // matrix storage p�tern

    int n = A.n;

    dpotrf_ (&UL, &n, A.c[0], &n, &info);

    int nn = info>0?info-1:A.n;

    // MAKE THE TRIANGLE
    for (int i=0; i<nn; ++i) {
        for (int j=0; j<i; ++j) {
            A(i,j) = 0;
        }
    }

    // ZERO THE REDUNDANT BASIS
    for (int i=nn; i<A.n; ++i) {
        for (int j=0; j<A.n; ++j) {
            A(i,j) = 0;
        }
    }

    if (info>0) cout << "Cholesky info: " << info << endl;
}

int Cholesky(tensor2 & A, int * p, double tol) {

    int info;

    char UL   = 'L'; // matrix storage p�tern

    int n = A.n;

    int r; // rank

    double * work = new double[2*n];

    //dpotrf_ (&UL, &n, A.c[0], &n, &info);
    //int nn = info>0?info-1:A.n;

    dpstrf_ (&UL, &n, A.c[0], &n, p, &r, &tol, work, &info );

    int nn = r; //info>0?info-1:A.n;


    // MAKE THE TRIANGLE
    for (int i=0; i<A.n; ++i) {
        for (int j=0; j<i; ++j) {
            A(i,j) = 0;
        }
    }

    // ZERO THE REDUNDANT BASIS
    /*
    for (int i=nn; i<A.n; ++i) {
        for (int j=0; j<A.n; ++j) {
            A(i,j) = 0;
        }
    }
    */

    // convert FORTRAN array indexing to C++
    for (int n=0; n<r; ++n) --p[n];

    if (info>0) cout << "Cholesky info: " << info << endl;

    delete[] work;

    return r; // rank of decomposition
}

// invert the Cholesky decomposition
void InvChol (tensor2 & L) {

    char low[] = "L";
    char non[] = "N";

    int info;
    int n = L.n;

    dtrtri_ (low, non, &n, L.c[0],  &n, &info);

    if (info>0) cout << "Inverse Cholesky info: " << info << endl;
}

// solve a set of linear equations, and save result in b
// Note: A is destroyed after SolveLinear
void SolveLinear(char uplo, int N, double * A, double * b) {

    int * piv = new int [N];
    int info = 0, nhrs = 1;

    dsptrf_ (&uplo, &N, A, piv, &info);

    if (info != 0) {
        throw Exception ("Error in SolveLinear: dsptrf failed! Info=" + to_string(info));
    }

    dsptrs_ (&uplo, &N, &nhrs, A, piv, b, &N, &info);

    if (info != 0) {
        throw Exception ("Error in SolveLinear: dsptrs failed! Info=" + to_string(info));
    }

    delete[] piv;
}

void SolveLinear(tensor2 & A, double * b) {

    int N = A.n;
    char uplo = 'u';

    int piv[N];
    int info = 0, nhrs = 1;

    int lwork = -1;
    double wkopt;
    double * work;


    dsytrf_ (&uplo, &N, A.c[0], &N, piv, &wkopt, &lwork, &info); // first call queries dimension of work space

    lwork = (int)wkopt;
    work = new double[lwork];

    dsytrf_ (&uplo, &N, A.c[0], &N, piv, work, &lwork, &info);

    delete[] work;

    if (info != 0) {
        throw Exception ("Error in SolveLinear: dsptrf failed! Info=" + to_string(info));
    }

    dsytrs_ (&uplo, &N, &nhrs, A.c[0], &N, piv, b, &N, &info);

    if (info != 0) {
        throw Exception ("Error in SolveLinear: dsptrs failed! Info=" + to_string(info));
    }
}

void InvertLU (tensor2 & A) {

    int n = A.n;
    int m = A.m;

    int info;

    int ipiv[n];

    // LU decomposition with partial pivoting
    dgetrf_(&n,&n,A[0], &n, ipiv, &info);

    if (info != 0) {
        throw Exception ("Error in InverseLU: dgetrf failed! Info=" + to_string(info));
    }

    int lwork = -1;
    double wkopt;
    double * work;

    dgetri_(&n, A[0], &n, ipiv, &wkopt, &lwork, &info);

    if (info != 0) {
        throw Exception ("Error in InverseLU: querying dgetri failed! Info=" + to_string(info));
    }

    lwork = (int)wkopt;
    work = new double[lwork];

    dgetri_(&n, A[0], &n, ipiv, work, &lwork, &info);

    delete[] work;

    if (info != 0) {
        throw Exception ("Error in InverseLU: dgetri failed! Info=" + to_string(info));
    }
}


double DotProduct (const tensor2 & A, const tensor2 & B) {

    if (A.m != B.m || A.n != B.n) {
        cout << "Error in DotProduct: sizes of matrices do not match!" << endl;
        exit(1);
    }

    int size = A.m * A.n;
    int incx = 1, incy = 1;

    return ddot_( &size, A.c[0], &incx, B.c[0], &incy );
}

//
//    substituto all SVDs for dgesdd_ which is much faster, and especially with deflation
//

// smarter? SVD
int SVD (const tensor2 & T, tensor2 & U, tensor2 & V, double * ss) {

    const int N = T.n;
    const int M = T.m;
    const int R = min(N,M);

    char all[]   = "A"; // return all vectors
    char sing[]  = "S"; // return only the first R vectors
    char over[]  = "O"; // overwrite T with the first R vectors

    int lwork = -1;
    double wokpt;
    double * work;
    int info;

    int NV = M;
    int NO = N;

    // determine which dimension is larger and reuse memory in either U or V
    // hasn't been tested yet
    {
        // make a copy of T
        tensor2 A(N,M);
        A = T;

        dgesvd_( all, all, &NV, &NO, A.c[0], &NV, ss, V.c[0], &NV, U.c[0], &NO,  &wokpt, &lwork, &info);

        if (info!=0) cout << "SVD1 info = " << info << endl;

        lwork = int(wokpt);
        work = new double[max(1,lwork)];

        dgesvd_( all, all, &NV, &NO, A.c[0], &NV, ss, V.c[0], &NV, U.c[0], &NO,  work, &lwork, &info);
    }

    if (info!=0) cout<< "SVD2 info = " << info << endl;

    delete[] work;

    return R;
}

// principal component analysis (SVD)
int PCA (const tensor2 & T, double * ss) {
    const int N = T.n;
    const int M = T.m;
    const int R = min(N,M);

    tensor2 U(N), V(M);

    SVD(T,U,V,ss);

    return R;
}

// SVD for thin arrays; careful, as it might
int SVD (tensor2 & T, tensor2 & U, double * ss) {

    const int N = T.n;
    const int M = T.m;
    const int R = min(N,M);

    char all[]   = "A"; // return all vectors
    char sing[]  = "S"; // return only the first R vectors
    char over[]  = "O"; // overwrite T with the first R vectors

    int lwork = -1;
    double wokpt;
    double * work;
    int info;

    // this is expected
    {
        int NV = M;
        int NO = N;

        dgesvd_( over, sing, &NV, &NO, T.c[0], &NV, ss, NULL, &NV, U.c[0], &NO,  &wokpt, &lwork, &info);

        if (info!=0) cout << "SVD1 info = " << info << endl;

        lwork = int(wokpt);
        work = new double[max(1,lwork)];

        dgesvd_( over, sing, &NV, &NO, T.c[0], &NV, ss, NULL, &NV, U.c[0], &NO,  work, &lwork, &info);

        if (info!=0) cout<< "SVD2 info = " << info << endl;
    }


    delete[] work;

    return R;
}

// inverse or MP pseudoinverse a symmetric matrix,
// should precondition the diagonal
void Invert (tensor2 & W) {

    const int N = W.n;

    tensor2 A = W;

    double a[N];
    DiagonalizeV(A,a);

    for (int n=0; n<N; ++n) a[n] = 1./a[n];

    WeightedInnerProductT (W, A, a);
}

// Moore-Penrose pseudoinverse
void PseudoInverse (tensor2 & T) {

    const int N = T.n;
    const int M = T.m;
    const int K = min(N,M);

    tensor2 U(N), V(M);
    U.zeroize();
    V.zeroize();

    double s[K];

    SVD (T, U,V, s);
    U.Transpose();
    U.n = V.n = K;

    // invert s
    for (int k=0; k<K; ++k) if (s[k]!=0.) s[k] =  1./s[k];

    for (int k=0; k<K; ++k) cblas_dscal (M, s[k], V[k], 1);

    TensorProductNTN (T, U, V);
}

int ALSstep () {

}

int KhatriRao (tensor2 & AB, const tensor2 & A, const tensor2 & B) {

    if (A.n!=B.n) {
        cout << "Bad dimensions in Khatri-Rao product! "  << A.n << " " << B.n << endl;
        return -1;
    }

    const int N = A.n;
    const int I = A.m;
    const int J = B.m;

    AB.setsize(N,I*J);

    // this is not very efficient, but there is little to be gained anyway
    for (int n=0; n<N; ++n) {
        for (int i=0; i<I; ++i) {
            for (int j=0; j<J; ++j) {
                AB(n,i*J+j) = A(n,i)*B(n,j);
            }
        }
    }

    return 0;

}

tensor2 Normalize(tensor2 & A) {
    int N = A.n;
    int M = A.m;

    tensor2 r(1,N); r.zeroize();

    for (int n=0; n<N; ++n) {
        tensor2 v(1,M,A[n]);
        r(0,n) = v.frobenius();
        r(0,n) = sqrt(r(0,n));
        v *= 1./r(0,n);
    }

    return r;
}


int SolveALS (tensor2 & U, tensor2 & V, tensor2 & W, tensor2 & dd, tensor3 & TT, const int MAXIT, double thresh=1.e-6) {

    const int I = TT.m;
    const int J = TT.l;
          int K = TT.n; // this is usually the shortest index

          int R = U.n; // should check all matrices have same rank



    double TF2 = TT.T.frobenius();

    cout << "initial norm^2 = " << TF2 << endl;


    // do one ALS step to fill in W & dd
    {
        tensor2 S(R), s(1,R), UV (R,I*J);

        KhatriRao (UV,U,V);

        SVD(UV,S,s[0]);
        //cout << s(0,0) / s(0,R-1) << "   ";

        tensor2 PP(R,K);

        TensorProductNNT (PP , UV,TT.T);

        for (int r=0; r<R; ++r) s(0,r) = 1./s(0,r);

        DiagonalScaling(s[0], PP);

        TensorProduct(W,S,PP);

        dd = Normalize(W);
    }

    // ALS iteration
    // ==============
    int it=0;
    double RF2, RF0=TF2;

    while(it<MAXIT) {

        // FIXME : check for condition number in SVD and remove near linear dependencies (and potential border ranks)

        // test norm
        {
            tensor2 RR; // residual

            tensor2 UV (R,I*J);
            KhatriRao (UV,U,V);

            WeightedInnerProductT(RR, W,UV, dd[0]);

            RR-=TT.T;

            RF2 = RR.frobenius();

            //cout << "residual norm^2 = " << RF2 << "   relative error^2 = " << RF2/TF2 << "  RMS = " << sqrt(RF2/TF2) << endl;
        }

        if (RF0-RF2 < thresh*thresh*TF2) break;
        RF0 = RF2;


        ++it;
        //cout << "ALS cycle : "  << it << "    ";

        // other two modes missing (require shuffling)

        // W U
        {
            tensor2 S(R), s(1,R), WU (R,K*I);

            KhatriRao (WU,W,U);

            SVD(WU,S,s[0]);
            //cout << s(0,0) / s(0,R-1) << "   ";

            tensor2 PP(R,J);

            const tensor2 TTP(K*I, J, TT.T[0]); // recast shape (may be incorrect)
            TensorProduct    (PP , WU, TTP);

            for (int r=0; r<R; ++r) s(0,r) = 1./s(0,r);

            DiagonalScaling(s[0], PP);

            TensorProduct(V,S,PP);

            dd = Normalize(V);
        }

        // W V
        {
            tensor2 S(R), s(1,R), WV (R,K*J);

            KhatriRao (WV,W,V);

            SVD(WV,S,s[0]);
            //out << s(0,0) / s(0,R-1) << "   ";

            tensor2 PP(R,I);

            for (int k=0; k<K; ++k) TT.R[k].Transpose();

            const tensor2 TTP(K*J, I, TT.T[0]); // recast shape (may be incorrect)
            TensorProduct    (PP , WV, TTP);

            for (int k=0; k<K; ++k) TT.R[k].Transpose();

            for (int r=0; r<R; ++r) s(0,r) = 1./s(0,r);

            DiagonalScaling(s[0], PP);

            TensorProduct(U,S,PP);;

            dd = Normalize(U);
        }

        // u v
        {
            tensor2 S(R), s(1,R), UV (R,I*J);

            KhatriRao (UV,U,V);

            SVD(UV,S,s[0]);
            //cout << s(0,0) / s(0,R-1) << "   ";

            tensor2 PP(R,K);

            TensorProductNNT (PP , UV,TT.T);

            for (int r=0; r<R; ++r) s(0,r) = 1./s(0,r);

            DiagonalScaling(s[0], PP);

            TensorProduct(W,S,PP);

            dd = Normalize(W);
        }
    }


    // test norm
    {
        tensor2 RR; // residual

        tensor2 UV (R,I*J);
        KhatriRao (UV,U,V);

        WeightedInnerProductT(RR, W,UV, dd[0]);

        RR-=TT.T;

        double RF2 = RR.frobenius();

        cout << "final norm^2 = " << RF2 << "   relative error^2 = " << RF2/TF2 << "  RMS = " << sqrt(RF2/TF2) << endl;
    }

    return 0;
}

int CPdeflation (tensor2 & U, tensor2 & V, tensor2 & W, tensor2 & dd, tensor3 & TT) {

    const int I = TT.m;
    const int J = TT.l;
          int K = TT.n; // this is usually the shortest index

          int R = U.n; // should check all matrices have same rank

    double TF2 = TT.T.frobenius();

    //U.zeroize();
    //V.zeroize();
    W.zeroize();
    dd.zeroize();

    tensor3 RR;
    RR = TT;

    // iterate component by component
    for (int r=0; r<R; ++r) {

        cout << "solving rank = "  << r << endl;

        // start with random-ish vectors
        tensor2 u(1,I,U[r]);
        tensor2 v(1,J,V[r]);
        tensor2 w(1,K,W[r]);
        tensor2 d(1,1,dd[0]+r);

        //u(0,0) = 1.;
        //v(0,0) = 1.;
        //w(0,0) = 1.;


        SolveALS(u,v,w, d, RR, 1000);

        // update TT
        for (int i=0; i<I; ++i)
            for (int j=0; j<J; ++j)
                for (int k=0; k<K; ++k)
                    RR(k,i,j) -= d(0,0) *  u(0,i) * v(0,j) * w(0,k);

        cout << "redidual tensor frobenius : " << RR.T.frobenius() << endl;
    }

}


int CPdecomp (tensor2 & U, tensor2 & V, const tensor3 & T, const double thresh) {

    const int MAXIT = 5;

    // the main idea is to get the factors approximately
    // right from a low resolution version of the tensor,
    // then incrementally increase the resolution & correct the factors
    // (similar to how adversarial image generation works)
    // ==================================================


    const int I = T.m;
    const int J = T.l;
          int K = T.n; // this is usually the shortest index


    // compress the tensor (high values will
    // cluster towards 0,0,0 corner)

    tensor3 TT;

    double TF2;

    // only one mode
    {
        tensor2 c(1,K), W(K);

        TensorProductNNT (W,T.T,T.T);

        TF2 = W.trace();

        DiagonalizeV(W,c[0],true);

        Transpose(W);

        cout << "old K : " << K << endl;

        // discard quasi-degenerate modes
        // this should count discarded Frobenius norm instead
        for (int k=0; k<K; ++k) {
            if (c[0][k]<thresh*TF2) {
                K = k;
                break;
            }
        }

        cout << "new K : " << K << endl;


        W.n = K; // resize; discard

        // rotate slices

        TT.Set(K,I,J);
        TensorProduct(TT.T, W,T.T);
    }

    cout << "tensor norm^2 = " << TF2 << endl;


    int R = K * min(I,J); // much more than necessary

    //tensor2 U(R,I), V(R,J);
    U.setsize(R,I);
    V.setsize(R,J);
    tensor2 dd(1,R);
    tensor2 W(R,K);



    //int R = HOSVD(T); // rank of decomposition

    // RANK SELECTION + INITIAL GUESS

    {
        tensor2 F2 = TT * TT; // norm squared of k-mode tensor fibers


        struct idxpair {
            int i,j;
        };

        idxpair * i2 = new idxpair[I*J];

        for (int i=0; i<I; ++i) for (int j=0; j<J; ++j) i2[i*J+j].i = i;
        for (int i=0; i<I; ++i) for (int j=0; j<J; ++j) i2[i*J+j].j = j;

        F2 *= -1; // reverse order
        FlashSort(F2[0], i2, I*J);
        F2 *= -1;

        for (int ij=0; ij<I*J; ++ij) {
            if (F2[0][ij]<TF2*thresh) {
                cout << "fiber R = " << ij << endl;
                break;
            }
            //cout << "Fiber " << i2[ij].i << " , " << i2[ij].j << "  ::   " << F2[0][ij] << endl;
        }

    }




    // populate U,V with initial guess
    {
        int RR=0;


        for (int k=0; k<K; ++k) {

            int NM = min(I,J);

            tensor2 tt(1,NM, dd[0]+RR);
            //tensor2 UU(I,I,U[RR]),VV(J,J,V[RR]); // did not work as expected
            tensor2 UU(I),VV(J);

            SVD (TT.R[k],UU,VV,tt[0]);
            UU.Transpose(); // in-place transpose


            // cuts off lowest PC's

            for (int n=0; n<NM; ++n) {
                if (tt[0][n]*tt[0][n]<thresh*TF2) {
                    NM = n;
                    break;
                }
            }


            tt.m = UU.n = VV.n = NM;
            tensor2 UUU(NM,I,U[RR]),VVV(NM,J,V[RR]);
            UUU = UU;
            VVV = VV;

            //cout << "SVD " << k << " : " << tt;


            RR += NM;

            /*
            // outproject u x v from remaining slabs
            for (int l=k+1; l<K2; ++l) {
                tensor2 P,Q;

                TensorProduct    (P, U, TT[l]);
                TensorProductNNT (Q, P,V);

                for (int n=0; n<NM; ++n) tt(0,n) = Q(n,n);

                tensor2 W;
                WeightedInnerProductT (W, U,V, tt[0]);

                TT[l] -= W;
            }
            */
        }

        cout << "old R : " << R << endl;

        // logically resize
        U.n = V.n = W.n = dd.m = R = RR;

        cout << "new R : " << R << endl;
    }



    // greedy algorithm AKA iterative CP deflation
    {
        CPdeflation (U, V, W, dd, TT);
    }




    if (0) {
        tensor2 S,T, s(1,R);
        TensorProductNNT(S,U,U);
        TensorProductNNT(T,V,V);

        S *= T; // hadamard product

        T = S;  // copy
        DiagonalScaling(dd[0],T);
        DiagonalScaling(T,dd[0]);


        DiagonalizeE(S,s[0], true);
        cout << "double SVD basis: " << s << endl;

        DiagonalizeE(T,s[0], true);
        cout << "weighted basis: " << s << endl;

        DiagonalizeGV(T,S,s[0]);
        cout << "pencil basis: " << s << endl;
    }




    cout.precision(12);


    cout << "R / K = " << double(R)/double(K) << endl;

    SolveALS(U,V,W,dd, TT, MAXIT);




    //cout << "  occupancies : " << dd;


    // alternatively:
    // ==============

    // select a subset of (relevant) indices from each mode

    // perform a CP-ALS / BCD on the sub-tensor

    // extend the vectors' indices to the full dimensions of each mode; minimize the residual

    // loop (add more indices & increase rank of decomposition)


    // alternatively:
    // ==============

    // compute norms^2 for each fiber over K

    // select largest i,j pairs

    // set these as the initial guess for ALS


    // also: cluster the fibers






    return 0;
}

// FIXME: it is more computationally efficient to compute the inner product for each
//        mode over the two other modes and diagonalize the resulting symmetric matrix

int HOSVD (const tensor3 & T, const double thresh) {

    // need to fix again the SVD shape problem for indices of very low dimension

    const int K = T.n; // this is usually the shortest index

    const int N = T.m;
    const int M = T.l;


    int N2 = min(N,M*K);
    int M2 = min(M,N*K);
    int K2 = min(K,N*M);

    cout << "initial tensor : " << N << " x " << M << " x " << K << endl;

    tensor2 A(N,M*K), a(1,N2), U(N2);
    U.zeroize();

    for (int n=0; n<N; ++n) {
        for (int m=0; m<M; ++m) {
            for (int k=0; k<K; ++k) {
                A(n,m*K+k) = T[k](n,m);
            }
        }
    }

    TensorProductNNT(U, A,A);

    const double TF2 = U.trace(); // frobenius norm squared

    DiagonalizeV(U,a[0],true);

    cout << "HOSVD mode 1 : " << a << endl;

    tensor2 B(M,N*K), b(1,M2), V(M2);
    V.zeroize();

    for (int m=0; m<M; ++m) {
        for (int n=0; n<N; ++n) {
            for (int k=0; k<K; ++k) {
                B(m,n*K+k) = T[k](n,m);
            }
        }
    }

    TensorProductNNT (V,B,B);

    DiagonalizeV(V,b[0],true);

    cout << "HOSVD mode 2 : " << b << endl;


    tensor2 C(K,N*M), c(1,K2), W(K2);
    W.zeroize();

    for (int k=0; k<K; ++k) {
        for (int n=0; n<N; ++n) {
            for (int m=0; m<M; ++m) {
                C(k,n*M+m) = T[k](n,m);
            }
        }
    }

    TensorProductNNT (W,C,C);

    DiagonalizeV(W,c[0],true);

    cout << "HOSVD mode 3 : " << c << endl;


    // discard quasi-degenerate modes
    // this should count discarded Frobenius norm instead
    for (int k=0; k<K2; ++k) {
        if (c[0][k]<thresh*TF2) {
            K2 = k;
            break;
        }
    }
    for (int k=0; k<M2; ++k) {
        if (b[0][k]<thresh*TF2) {
            M2 = k;
            break;
        }
    }
    for (int k=0; k<N2; ++k) {
        if (a[0][k]<thresh*TF2) {
            N2 = k;
            break;
        }
    }

    Transpose(U);
    Transpose(V);
    Transpose(W);

    U.n = N2;
    V.n = M2;
    W.n = K2;

    // rotate slices
    tensor3 TT(K2, N2,M2);


    for (int k=0; k<K; ++k) {

        tensor2 P,Q;
        TensorProduct    (P, U, T[k]);
        TensorProductNNT (Q, P,   V);

        for (int l=0; l<K2; ++l)
            TT[l].FMA ( Q, W(l,k));

    }

    //for (int k=0; k<K2; ++k)  cout << "  " << TT[k].frobenius();
    //cout << endl;

    cout << "core tensor : " << N2 << " x " << M2 << " x " << K2 << endl;

    cout << "fist slice : " << TT[0] << endl << endl;

    //for (int k=0; k<K2; ++k) cout << TT[k] << endl << endl;

    // count elements
    {
        int ne = 0;

        for (int k=0; k<K2; ++k)
            for (int n=0; n<N2; ++n)
                for (int m=0; m<M2; ++m)
                    if (TT[k](n,m)*TT[k](n,m) > thresh*TF2) ++ne;

        cout << " number of elements larger than " << thresh*TF2 << " :  " << ne << " / " << K2*N2*M2 << endl;
    }


    for (int k=0; k<K2; ++k) {
        tensor2 tt(1,min(N2,M2));
        PCA(TT[k],tt[0]);
        cout << "SVD " << k << " : " << tt;
    }

    tensor2 F(N2,M2);
    F.zeroize();

    for (int k=0; k<K2; ++k) {

        for (int m=0; m<M2; ++m) {
            for (int n=0; n<N2; ++n) {
                F(n,m) += TT[k](n,m)*TT[k](n,m);
            }
        }
    }

    cout << "fibers : " << endl << F << endl << endl;


    // test for lates use as initial guess with CP / ALS
    {
        const int NM = min(N2,M2);
        for (int k=0; k<K2; ++k) {

            tensor2 tt(1,NM);
            tensor2 U(N2),V(M2);

            SVD (TT[k],U,V,tt[0]);
            U.Transpose();
            U.n = V.n = NM;

            cout << "SVD " << k << " : " << tt;

            // outproject u x v from remaining slabs
            for (int l=k+1; l<K2; ++l) {
                tensor2 P,Q;

                TensorProduct    (P, U, TT[l]);
                TensorProductNNT (Q, P,V);

                for (int n=0; n<NM; ++n) tt(0,n) = Q(n,n);

                tensor2 W;
                WeightedInnerProductT (W, U,V, tt[0]);

                TT[l] -= W;
            }

        }
    }


    // old method using SVD

    /*

    // first do the HOSVD

    tensor2 A(N,M*K), a(1,N2), U(N2);
    U.zeroize();

    for (int n=0; n<N; ++n) {
        for (int m=0; m<M; ++m) {
            for (int k=0; k<K; ++k) {
                A(n,m*K+k) = T[k](n,m);
            }
        }
    }

    SVD(A,U,a[0]);

    cout << "HOSVD mode 1 : " << a << endl;



    tensor2 B(M,N*K), b(1,M2), V(M2);
    V.zeroize();

    for (int m=0; m<M; ++m) {
        for (int n=0; n<N; ++n) {
            for (int k=0; k<K; ++k) {
                B(m,n*K+k) = T[k](n,m);
            }
        }
    }

    SVD (B,V,b[0]);

    cout << "HOSVD mode 2 : " << b << endl;



    tensor2 C(K,N*M), c(1,K2), W(K2);
    W.zeroize();

    for (int k=0; k<K; ++k) {
        for (int n=0; n<N; ++n) {
            for (int m=0; m<M; ++m) {
                C(k,n*M+m) = T[k](n,m);
            }
        }
    }

    SVD (C,W,c[0]);

    cout << "HOSVD mode 3 : " << c << endl;


    // discard quasi-degenerate modes
    // >>> do the same with the other modes
    for (int k=0; k<K2; ++k) {
        const double thresh = 1.e-6;
        if (c[0][k]<thresh*c[0][0]) {
            K2 = k;
            break;
        }
    }


    // rotate slices
    tensor3 TT(K2, N2,M2);


    for (int k=0; k<K; ++k) {

        tensor2 P,Q;
        TensorProductNTN    (P, U, T[k]);
        TensorProduct       (Q, P,   V);

        for (int l=0; l<K2; ++l)
            TT[l].FMA ( Q, W(k,l));

    }

    //for (int k=0; k<K2; ++k)  cout << "  " << TT[k].frobenius();
    //cout << endl;


    cout << "core tensor : " << N2 << " x " << M2 << " x " << K2 << endl;

    //for (int k=0; k<K2; ++k) cout << TT[k] << endl << endl;

    // count elements
    {
        const double thresh = 1.e-5;
        int ne = 0;

        for (int k=0; k<K2; ++k)
            for (int n=0; n<N2; ++n)
                for (int m=0; m<M2; ++m)
                    if (fabs(TT[k](n,m))>thresh) ++ne;

        cout << " number of elements larger than " << thresh << " :  " << ne << " / " << K2*N2*M2 << endl;
    }


    for (int k=0; k<K2; ++k) {
        tensor2 tt(1,min(N2,M2));
        PCA(TT[k],tt[0]);
        cout << "SVD " << k << " : " << tt;
    }
    */


    int R = K2* min(N2,M2); // we need to do better than this by skipping low SVDs, etc.

    return R;
}



/*
    BLAS-like matrix operations
*/


// CBLAS inclusion is handled by the makefile
// we really need to put things in order a bit

//producto de tensores de rango 2
void TensorProduct(tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha) {

    // mismatched shapes ?
    if ((A.m != B.n)) {
        cout << " bad contraction in TensorProduct! " << A.n << " " << A.m << "   " << B.n << " " << B.m << endl;
        return;
    }

    const int I = A.n;
    const int J = B.m;
    const int K = A.m;

    R.setsize(I,J); // reset R shape if needed

    const int LDA = A.m;
    const int LDB = B.m;
    const int LDC = R.m;

    if ((I==0)||(J==0)||(K==0)) return;

    //R.zeroize(); // setsize() does not initialize the tensor; dgemm might end up multiplying 0.*NaN pr 0*Infty if the memory is not initialized

    if (alpha==0) cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, I, J, K,   1.0, A.c[0], LDA,  B.c[0], LDB, 0.0, R.c[0], LDC);
    else          cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, I, J, K, alpha, A.c[0], LDA,  B.c[0], LDB, 1.0, R.c[0], LDC);
}

void TensorProductNTN(tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha) {

    // mismatched shapes ?
    if ((A.n != B.n)) {
        cout << " bad contraction in TensorProductNTN! " << A.n << " " << A.m << "   " << B.n << " " << B.m << endl;
        return;
    }

    const int I = A.m;
    const int J = B.m;
    const int K = A.n;

    R.setsize(I,J); // reset R shape if needed

    const int LDA = A.m;
    const int LDB = B.m;
    const int LDC = R.m;

    if ((I==0)||(J==0)||(K==0)) return;

    //R.zeroize();

    if (alpha==0) cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, I, J, K,   1.0, A.c[0], LDA,  B.c[0], LDB, 0.0, R.c[0], LDC);
    else          cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, I, J, K, alpha, A.c[0], LDA,  B.c[0], LDB, 1.0, R.c[0], LDC);
}

//producto de tensores de rango 2
void TensorProductNNT(tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha) {

    // mismatched shapes ?
    if ((A.m != B.m)) {
        cout << " bad contraction in TensorProductNNT! " << A.n << " " << A.m << "   " << B.n << " " << B.m << endl;
        return;
    }

    const int I = A.n;
    const int J = B.n;
    const int K = A.m;

    R.setsize(I,J); // reset R shape if needed

    const int LDA = A.m;
    const int LDB = B.m;
    const int LDC = R.m;

    if ((I==0)||(J==0)||(K==0)) return;

    //R.zeroize();

    if (alpha==0) cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, I, J, K,   1.0, A.c[0], LDA,  B.c[0], LDB, 0.0, R.c[0], LDC);
    else          cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, I, J, K, alpha, A.c[0], LDA,  B.c[0], LDB, 1.0, R.c[0], LDC);
}

void TensorProductNTT(tensor2 & R, const tensor2 & A, const tensor2 & B, double alpha) {

    // mismatched shapes ?
    if ((A.n != B.m)) {
        cout << " bad contraction in TensorProductNTT! " << A.n << " " << A.m << "   " << B.n << " " << B.m << endl;
        return;
    }

    const int I = A.m;
    const int J = B.n;
    const int K = A.n;

    R.setsize(I,J); // reset R shape if needed

    const int LDA = A.m;
    const int LDB = B.m;
    const int LDC = R.m;

    if ((I==0)||(J==0)||(K==0)) return;

    //R.zeroize();

    if (alpha==0) cblas_dgemm(CblasRowMajor, CblasTrans, CblasTrans, I, J, K,   1.0, A.c[0], LDA,  B.c[0], LDB, 0.0, R.c[0], LDC);
    else          cblas_dgemm(CblasRowMajor, CblasTrans, CblasTrans, I, J, K, alpha, A.c[0], LDA,  B.c[0], LDB, 1.0, R.c[0], LDC);
}


tensor2 Transpose (const tensor2 & A) {

    const int BS = 8;
    const int N = A.m;
    const int M = A.n;

    tensor2 T(N,M);

    // used blocked algorithm
    for (int nn=0; nn<N; nn+=BS) {
        for (int mm=0; mm<M; mm+=BS) {

            const int NB = min (nn+BS, N);
            const int MB = min (mm+BS, M);

            for (int n=nn; n<NB; ++n)
                for (int m=mm; m<MB; ++m)
                    T(n,m) = A(m,n);
        }
    }

    // check destructor & copy constructors
    return T;
}

// computes W = A D A+   (where D is a diagonal matrix)
void WeightedInnerProduct (tensor2 & W, const tensor2 & A, const double * d) {
    const int N = A.n;
    const int M = A.m;

    tensor2 DAt;
    DAt = Transpose(A);

    // multiply with diagonal
    for (int m=0; m<M; ++m) cblas_dscal (N, d[m], DAt[m], 1);

    TensorProduct (W, A, DAt);
}

// computes W = A+ D A   (where D is a diagonal matrix)
void WeightedInnerProductT (tensor2 & W, const tensor2 & A, const double * d) {
    const int N = A.n;
    const int M = A.m;

    tensor2 DA;
    DA = A;

    // multiply with diagonal
    for (int n=0; n<N; ++n) cblas_dscal (M, d[n], DA[n], 1);

    TensorProductNTN (W, A, DA);
}


// computes W = A D B+   (where D is a diagonal matrix)
void WeightedInnerProduct (tensor2 & W, const tensor2 & A, const tensor2 & B, const double * d) {
    const int N = A.n;
    const int K = A.m; // == B.m
    const int M = B.n;

    tensor2 DBt;
    DBt = Transpose(B);

    // multiply with diagonal
    for (int k=0; k<K; ++k) cblas_dscal (M, d[k], DBt[k], 1);

    TensorProduct (W, A, DBt);
}

// computes W = A+ D B   (where D is a diagonal matrix)
void WeightedInnerProductT  (tensor2 & W, const tensor2 & A, const tensor2 & B, const double * d) {
    const int N = A.m;
    const int K = A.n; // == B.n
    const int M = B.m;

    tensor2 DB;
    DB = B;

    // multiply with diagonal
    for (int k=0; k<K; ++k) cblas_dscal (M, d[k], DB[k], 1);

    TensorProductNTN (W, A, DB);
}



// A <- d * A
void DiagonalScaling (const double * d, tensor2 & A) {

    const int N = A.n;
    const int M = A.m;

    // multiply with diagonal
    for (int n=0; n<N; ++n) cblas_dscal (M, d[n], A[n], 1);
}

// A <- A * d
void DiagonalScaling (tensor2 & A, const double * d) {

    const int N = A.n;
    const int M = A.m;

    // multiply with diagonal
    // this access pattern is quite inefficient compared to the transposed one
    for (int m=0; m<M; ++m) cblas_dscal (N, d[m], &A[0][m], M);
}

// tensor primitives
// ==================

// recast 2-index tensors as linear arrays

tensor1::tensor1(tensor2 & T) {
    c = T.c[0];
    n = T.n * T.m;
}

tensor1::tensor1(symtensor2 & T) {
    c = T.c[0];
    n = T.n2;
}

tensor1::tensor1(const tensor2 & T) {
    c = T.c[0];
    n = T.n * T.m;
}

tensor1::tensor1(const symtensor2 & T) {
    c = T.c[0];
    n = T.n2;
}

static const uint64_t MAXINT = (1<<30); // 2^30 < 2^31 - 1 (maximum representable 32-bit integer)

void tensor1::zeroize() {
    if (isunset()) {
        cout << "tensor1 is not initialized!" << endl;
        return;
    }
    memset( (void*)c, 0, n*sizeof(double) );
}

tensor1 & tensor1::operator+=(const tensor1 & s) {
    return FMA(s, +1.);
}

tensor1 & tensor1::operator-=(const tensor1 & s) {
    return FMA(s, -1.);
}

tensor1 & tensor1::operator*=(double s) {

    if (isunset()) {
        cout << "tensor1 is not initialized!" << endl;
        return *this;
    }

    // for large dimensions, do it in batches
    {
        double * p=c;
        uint64_t d = n;

        while (d>MAXINT) {
            cblas_dscal (int(MAXINT), s, p, 1);
            p+= MAXINT;
            d-= MAXINT;
        }
        cblas_dscal (int(d), s, p, 1);
    }

    return *this;
}

tensor1 & tensor1::FMA (const tensor1 & s, double a) {

    if (isunset()) {
        cout << "tensor1 is not initialized!" << endl;
        return *this;
    }

    if (s.isunset()) {
        cout << "tensor1 is not initialized!" << endl;
        return *this;
    }

    if (n!=s.n) {
        // issue an error
        cout << "incompatible dimension computing dot product" << endl;
        return *this;
    }

    // for large dimensions, do it in batches
    {
        double * p=c, *q=s.c;
        uint64_t d = n;

        while (d>MAXINT) {
            cblas_daxpy (int(MAXINT), a, q, 1, p, 1);
            q+= MAXINT;
            p+= MAXINT;
            d-= MAXINT;
        }
        cblas_daxpy (int(d), a, q, 1, p, 1);
    }

    return *this;
}

double tensor1::operator*(const tensor1 & s) const {

    if (isunset()) {
        cout << "tensor1 is not initialized!" << endl;
        return 0;
    }

    if (s.isunset()) {
        cout << "tensor1 is not initialized!" << endl;
        return 0;
    }

    if (n!=s.n) {
        // issue an error
        cout << "incompatible dimension computing dot product" << endl;
        return 0;
    }

    double r=0;

    // for large dimensions, do it in batches
    {
        double * p=c, *q=s.c;
        uint64_t d = n;

        while (d>MAXINT) {
            r += cblas_ddot (int(MAXINT), p,1, q,1);
            q+= MAXINT;
            p+= MAXINT;
            d-= MAXINT;
        }
        r += cblas_ddot (int(d), p,1, q,1);
    }

    return r;
}

double tensor1::norm() const {
    return (*this)*(*this);
}

/*
    FIXME : what if D=n*m overflows?
*/

// use dcopy (probably implemented with memcpy)
tensor2 & tensor2::operator=(const tensor2 & st) {

    if (this == &st) return *this; // self-assignment

    {
        setsize(st.n, st.m);
        const int D = n*m;
        cblas_dcopy(D, st.c[0],1, c[0],1);
    }

    return *this;
}

double tensor2::operator*(const tensor2 & s) const {

    if ( (n!=s.n) || (m!=s.m) ) {
        cout << "incompatible shape computing Kronecker matrix dot product" << endl;
        return 0;
    }

    const tensor1 A(*this);
    const tensor1 B(s);

    return A*B;
}

double Contract(const tensor2 & A, const tensor2 & B) {
    return A*B;
}

tensor2 & tensor2::operator*=(double s) {

    tensor1 A(*this);
    A*=s;

    return *this;
}

tensor2 & tensor2::operator*=(const tensor2 & s) {
    if ( (n!=s.n) || (m!=s.m) ) {
        cout << "incompatible shape computing matrix direct product" << endl;
        return *this;
    }

    // just elementwise product
    for (int ij=0; ij<n*m; ++ij) c[0][ij] *= s[0][ij];

    return *this;
}


tensor2 & tensor2::FMA (const tensor2 & s, double a) {

    if ( (n!=s.n) || (m!=s.m) ) {
        cout << "incompatible shape calculating daxpy" << endl;
        return *this;
    }

    tensor1       A(*this);
    const tensor1 B(s);
    A.FMA(B,a);

    return *this;
}

tensor2 & tensor2::operator+=(const tensor2 & s) {
    return FMA(s, +1.);
}

tensor2 & tensor2::operator-=(const tensor2 & s) {
    return FMA(s, -1.);
}

double tensor2::frobenius() const {
    return (*this)*(*this); //  DotProduct(*this, *this);
}

void tensor2::zeroize() {
    tensor1 A(*this);
    A.zeroize();
}

void tensor2::Symmetrize() {
    if (n==m)
    for (int p=0; p<n; ++p) {
        for (int q=0; q<=p; ++q) {
            double s = c[p][q]+c[q][p];
            c[p][q] = s;
            c[q][p] = s;
        }
    }
}

void tensor2::AntiSymmetrize() {
    if (n==m)
    for (int p=0; p<n; ++p) {
        for (int q=0; q<=p; ++q) {
            double a = c[p][q]-c[q][p];
            c[p][q] =  a;
            c[q][p] = -a;
        }
    }
}

symtensor2 & symtensor2::FMA (const symtensor2 & s, double a) {

    if (n!=s.n) {
        cout << "incompatible shape calculating daxpy" << endl;
        return *this;
    }

          tensor1 A(*this);
    const tensor1 B(s);

    A.FMA(B,a);

    return *this;
}

void symtensor2::zeroize() {
    tensor1 A(*this);
    A.zeroize();
}
