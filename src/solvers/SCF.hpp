#ifndef __SCF__
#define __SCF__

#include <string>

class DFTgrid;
class Echidna;
class tensor2;
class symtensor;
class rAtom;

enum class eMethod      {RHF, UHF, ROHF, OEHF, RMP2, UMP2, RKS, UKS, ROKS, RHFRSP, RCMM, BSO};

// a solver for R/U HF/KS using different methods
class SCFsolver {

    // basic data
    // ==========

    int nalpha, nbeta; // number of a,b electrons
    int ncore,  nval;  // core and valence electrons
    eMethod open;


    bool usexc = false; // for DFT
    double fxc = 1; // fraction of XC for hybrid functionals

    const DFTgrid * dft = NULL;
          Echidna * efs = NULL;

    int NAO, NMO;


    // default configuration
    // =====================
    double DIISthresh    = 1e-8; //0.001;
    double DIISmaxratio  = 10;
    double DIIScondition = 1e-6;

    int maxSCFc = 50;

    int MAXDIIS  = maxSCFc+1;
    int MAXEDIIS =        12;


    double CSthresh  = 1.e-15;  // cauchy-schwarz
    double SCFthresh = 5.e-8;   // convergence of [F,D]

    double CDthresh  = 0;       // cholesky decomposition threshold (default: no CD)

    // tensors
    // =======
    tensor2 SS, HH; // overlap, core Hamiltonian in OAO basis

    // BBB is (S_AO)^(-1/2) in nAOxnMO dimension
    tensor2 SSB, BBB; // block-orthonormalized overlap, block preconditioner

    // for GetFock/GetCoulomb/GetExchange
    // BBB_2 is (S_AO)^1/2 in nAOxnMO dimension
    tensor2 BBB_2;

    // one-electron integrals
    tensor2 DX, DY, DZ;
    tensor2 PX, PY, PZ;
    tensor2 LX, LY, LZ;

    // must change the nomenclature
    tensor2 * FFa = NULL, * DDa = NULL;
    tensor2 * FFb = NULL, * DDb = NULL;

    tensor2 * JJa = NULL, * JJb = NULL;
    tensor2 * KKa = NULL, * KKb = NULL;

    tensor2 * XXa = NULL, * XXb = NULL;

    // MO coefficients
    tensor2 C, Cb;


    //int ND;


    double * ea = NULL;
    double * eb = NULL;

    double iEsmear = 0;

    // energies
    double Enuc, E1=0, E2=0, E2j, E2k, E2xc, Eel, Et;
    double X2=-1; // unset

    double Et_prev, delta_Et, delta_G;

    // does not save by default
    std::string save = "";


    bool init = false;
    int nSCF = 0; // number of SCF iterations

    static const int MAXM = 120; // probably much fewer than that

    struct Step {
        tensor2 *Da, *Fa, *Ja, *Ka, *Xa;
        tensor2 *Db, *Fb, *Jb, *Kb, *Xb;

        double E1a, E2a, E1b, E2b, Eel, X2; // must be saved

        Step();
        void Clear();
       ~Step();
    };

    // history of previous iterations
    // ==============================

    Step hist[MAXM];

    tensor2 sX2, sKD, sJD;
    double sE1[MAXM];

    // keep track of relevant EDIIS indices
    int iEDIIS[MAXM];
    int nEDIIS = 0;

  private:

    void    SolveSecular (int N=1);
    void    ARHstep();

    void    CalcEnergies();
    void    CalcCommuters();
    void    Save(const string & xtra, tensor2 * T);
    void    NewStep  (int N=1);
    void    SaveStep ();
    void    LoadStep ();

  public:

    SCFsolver  ();
    ~SCFsolver ();

    void    ReadDensityFromTensor (tensor2* DensityPointer);
    void    ReadDensity  (string InpDens);
    void    ZeroDensity  (int N=1);
    void    AssembleFock (int N=1);
    void    AssembleFock (double CSthresh, int N=1); // assemble a Fock / KS matrix

    void    SetSystem    (const symtensor & Sp, const symtensor & Hp, int na, int nb, double enuc, eMethod method=eMethod::RHF); // set Hcore, S, nelec, etc.
    void    SetSolvers   (Echidna * efssolver, const DFTgrid * grid); // this will keep
    void    SetParams    (const string & savefile, double DMprecision, double HFprecision, int maxSCFc); // set convergence, thresholds, number of cycles, etc.
    void    SetDIIS      (double thresh, double maxratio, double condition, int NDIIS, int NEDIIS);  // set convergence, threshold, etc.
    void    SetIguess    (const symtensor & Fp); // sets an initial guess for the system
    void    SetCD        (double thresh);        // do CD of the two-electron tensor

    void    SetDipoleInts (tensor2& TX, tensor2& TY, tensor2& TZ);
    void    SetNablaInts  (tensor2& TX, tensor2& TY, tensor2& TZ);
    void    SetAngMomInts (tensor2& TX, tensor2& TY, tensor2& TZ);

    // MCSCF is diabled since it is not used in Fiesta
    //void    RunMCSCF     (); // self-explanatory

    void    RunSCF       (); // self-explanatory
    void    RunSAP       (int core=0, int fill=0); // for SAP initial guess

    void    RunBasisOptimizer (const rAtom *  Atoms, uint32_t nAtoms);

    // getters
    tensor2 GetMOs       (bool beta=false) const;
    tensor2 GetDensity   (bool beta=false) const;
    tensor2 GetCoulomb   (bool beta=false) const; // this returns the J matrix in the local basis
    tensor2 GetExchange  (bool beta=false) const; // this returns the K matrix in the local basis
    tensor2 GetFock      (bool beta=false) const; // this returns the Fock in the local basis
    tensor2 GetFockMO    (bool beta=false) const; // this returns Fock in MO basis
    const double  * GetEnergies  (bool beta=false) const; // constant pointer
    double          GetSCFenergy () const; // retrieve converged? energy
    double          GetTotalEnergy () const;
    const int       GetNumOcc (bool beta=false) const;
    const double    GetCSthresh() const;

    const tensor2& GetDX () const { return DX; }
    const tensor2& GetDY () const { return DY; }
    const tensor2& GetDZ () const { return DZ; }

    const tensor2& GetPX () const { return PX; }
    const tensor2& GetPY () const { return PY; }
    const tensor2& GetPZ () const { return PZ; }

    const tensor2& GetLX () const { return LX; }
    const tensor2& GetLY () const { return LY; }
    const tensor2& GetLZ () const { return LZ; }

    void            Clear        (); // delete all information from some previous run

};

#endif
