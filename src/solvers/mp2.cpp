
/**
    unfinished MP2 solver
*/

#include "molecule/edata.hpp"
#include "linear/tensors.hpp"
#include "linear/eigen.hpp"

#include "fiesta/fiesta.hpp"
using namespace Fiesta;

#include <string>
#include <iostream>
using namespace std;

// LAPACK ROUTINES
extern "C" {
    void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
                    int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
                    double* work, int* lwork, int* info );
    void dgesv_ ( int* n, int* nrhs, double* a, int* lda, int* ipiv,
                    double* b, int* ldb, int* info );
}



void MolElData::DoRMP2 (double & EMP2) {

    const double Tthresh = 1.e-40; // CS threshold for integrals

    string save = "";
    double Ehf = 0;
    const int n2 = nBFs*nBFs;
    const int N=nBFs, N2=n2;

    const double * En;

    Messenger << "Initializing RMP2" << endl;
    {
        En = SCF.GetEnergies();
        C  = SCF.GetMOs(); // this must be transposed
    }


    for (int n=0; n<nBFs; ++n) Messenger << En[n] << " "; Messenger << endl;
    D2 = Dp;

    Messenger << "dim   = " << nBFs << endl;
    Messenger << "dim^2 = " << n2 << endl;


    //EchidnaSolver.Cholesky(1.e-20); // this is the discard for the individual CS ones
    //EchidnaSolver.InitCholesky (1.e-8); // this is now the CD threshold for decomposition

    //return;


    Messenger << "Get full bielectronic tensor" << endl;
    tensor4 W4(nBFs);
    EchidnaSolver.GetFullTensor(W4, Tthresh);

    // MP2 energies in O(N^5) using RI
    if (0) {
        const int NO = npairs;
        const int NV = nBFs - NO;
        const int NOV = NO*NV;
        const int NN  = nBFs*nBFs;

        double * es = new double[NOV];
        tensor2 ED(NOV);

        {
            for (int i=0; i<NO; ++i)
                for (int a=0; a<NV; ++a)
                    es[a*NO+i] = En[NO+a] - En[i];

            for (int ia=0; ia<NOV; ++ia)
                for (int jb=0; jb<NOV; ++jb)
                    ED(ia,jb) = 1./(es[ia]+es[jb]);

            // find decomposition of the energy denominators
            Messenger << "diagonalizing 1" << endl;
            DiagonalizeV (ED, es);

            // print first few vectors
            for (int n=0; n<6; ++n) {
                int nn = NOV-1-n;
                Messenger << n << " : " << es[nn] << endl;

                for (int a=0; a<NV; ++a) {
                    for (int i=0; i<NO; ++i) {
                        Messenger << " " << ED(nn, a*NO+i);
                    }
                    Messenger << endl;
                }
                Messenger << endl;
            }


            int m = NO, n = NV, lda = m, ldu = m, ldvt = n, info, lwork;
            double wkopt;
            double* work;
            /* Local arrays */
            double s[n], u[ldu*m], vt[ldvt*n], a[n*m];

            lwork = -1;
            dgesvd_( "A", "A", &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, &wkopt, &lwork, &info);
            lwork = (int)wkopt;
            work = new double[lwork];

            Messenger.precision(12);

            for (int k=0; k<16; ++k) {
                for (int ia=0; ia<NOV; ++ia) a[ia] = ED(NOV-1-k,ia);
                dgesvd_ ("A", "A", &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work, &lwork, &info);
                Messenger << k << " :: " << es[NOV-1-k] << " :: "; for (int n=0; n<NO; ++n) Messenger << " " << s[n]; Messenger << endl;
            }
        }


        /* this is just a test for the less-naive O(N^3~N^4) algorithm */
        {
            // IRL this will be sparse
            tensor4 PP[16]; // shouldn't be more than 16
            tensor4 PW[16];
            tensor4 PX[16];


            // this is the trickiest loop to accelerate
            // the Laplace-transformed variant has no such issues, given that the indices i and a are
            // decoupled and can be treated independently, but the Cholesky-decomposed denominators
            // couple them.
            // however, this is close in structure to LT, so there MAY be a way to find a numerical approximation
            // indeed, the SVDs of the eigenvectors corresponding to the largest contributions contain themselves
            // few relevant terms. It IS possible to compute this in O(N^3) given some predefined numerical threshold
            // also, expressing the result in terms of pseudodensity matrices' product may be helpful later on
            int lpp=0;
            for (int l=0; (l<16)&&(es[NOV-1-l]>1e-16); ++l) {
                const int sss = NOV-1-l;
                PP[l].Set(nBFs);

                for (int p=0; p<nBFs; ++p) {
                    for (int q=0; q<nBFs; ++q) {
                        // check CS
                        for (int r=0; r<nBFs; ++r) {
                            for (int s=0; s<nBFs; ++s) {
                                double sum = 0;
                                for (int i=0; i<NO; ++i) {
                                    for (int a=0; a<NV; ++a) {
                                        const int ai = a*NO+i;
                                        sum += ED(sss, ai) * C(i,p) * C(NO+a,q) * C(i,r) * C(NO+a,s); // this is ON5, but it can be done in ON4 or less
                                    }
                                }
                                // pq is always an AO product
                                PP[l](p,q,r,s) = sum;
                            }
                        }
                    }
                }
                lpp = l+1;
            }

            Messenger << "lpp " << lpp << endl;

            // this looks like the limiting step, O N^4 if p and q are independent (like in exchange) and
            // O N^3 if pq are later contracted with a density (like in J-like)
            // perhaps this can be accelerated with CD- / RI-

            // can be made cubic with a CFMM-like trick
            #pragma omp parallel for
            for (int l=0; l<lpp; ++l) {
                PW[l].Set(nBFs);

                for (int p=0; p<nBFs; ++p) {
                    for (int q=0; q<nBFs; ++q) {
                        for (int r=0; r<nBFs; ++r) {
                            for (int s=0; s<nBFs; ++s) {
                                double sum = 0;
                                for (int t=0; t<nBFs; ++t) {
                                    for (int u=0; u<nBFs; ++u) {
                                        sum += PP[l](p,q,t,u)* W4(t,u,r,s); // this is O(N^4) for (tu) and (rs) local AO products
                                    }
                                }
                                PW[l](p,q,r,s) = sum;
                            }
                        }
                    }
                }
            }

            // contract 2 PW's and sum over l
            double E2J = 0;

            // O N^2
            for (int l=0; l<lpp; ++l) {

                double E2l = 0;

                for (int p=0; p<nBFs; ++p) {
                    for (int q=0; q<nBFs; ++q) {
                        for (int r=0; r<nBFs; ++r) {
                            for (int s=0; s<nBFs; ++s) {
                                E2l += PW[l](p,q,r,s) * PW[l](r,s,p,q);
                            }
                        }
                    }
                }

                const int sss = NOV-1-l;
                E2J += es[sss] * E2l;
                Messenger << l << " " << es[sss] << " " <<  E2l << endl;
            }

            E2J *= -2; Messenger << "E2J : " << E2J << endl;


            // O N^3 -> loop over rw and vs first
            #pragma omp parallel for
            for (int l=0; l<lpp; ++l) {
                const int sss = NOV-1-l;
                PX[l].Set(nBFs);

                for (int r=0; r<nBFs; ++r) {
                    for (int s=0; s<nBFs; ++s) {

                        // (tu)
                        for (int t=0; t<nBFs; ++t) {
                            for (int u=0; u<nBFs; ++u) {
                                double sumvw = 0;

                                // (vs)
                                for (int v=0; v<nBFs; ++v) {
                                    // (rw)
                                    for (int w=0; w<nBFs; ++w) {
                                        sumvw += PP[l](t,u,v,w) * W4(r,w,v,s);
                                    }
                                }

                                PX[l](t,u, r,s) = sumvw;
                            }
                        }
                    }
                }
            }



            // do the Xcontribution
            double E2X = 0;

            // O N^3 -> just a simple integration
            #pragma omp parallel for
            for (int l=0; l<lpp; ++l) {
                const int sss = NOV-1-l;

                double sum = 0;

                for (int r=0; r<nBFs; ++r) {
                    for (int s=0; s<nBFs; ++s) {

                        // (tu)
                        for (int t=0; t<nBFs; ++t) {
                            for (int u=0; u<nBFs; ++u) {
                                sum += PW[l](r,s,t,u) * PX[l](t,u, r,s);
                            }
                        }
                    }
                }

                #pragma omp atomic
                E2X += es[sss] * sum;
            }

            Messenger << "E2X : " << E2X << endl;
        }


        // this should use AO representation and return
        // only the largest eigenvalues
        Messenger << "diagonalizing 2" << endl;
        double * ww = new double[NN];
        DiagonalizeV (W4.T, ww);


        tensor4 W5(nBFs);

        {
            Messenger << "Transforming " << endl;
            W5.T.zeroize();

            for (int cd=0; cd<NN; ++cd) {
                if (ww[cd]<1e-16) continue;

                for (int i=0; i<nBFs; ++i) {
                    for (int j=0; j<nBFs; ++j) {

                        double sum=0;
                        for (int a=0; a<nBFs; ++a) {
                            for (int b=0; b<nBFs; ++b) {
                                sum += C(i,a) * C(j,b) * W4.T(cd, a*nBFs+b);
                            }
                        }
                        W5.T(cd, i*nBFs+j) = sum;
                    }
                }
            }
        }


        Messenger << "computing" << endl;
        double Emp2j = 0;
        double Emp2x = 0;

        for (int ss=0; ss<NOV; ++ss)  {
            const int sss = NOV-1-ss;
            if (es[sss]<1e-16) continue;

            for (int nn=0; nn<NN; ++nn) {
                const int nnn = NN-1-nn;
                if (es[sss]*ww[nnn]<1e-16) continue;

                for (int mm=0; mm<NN; ++mm) {
                    const int mmm = NN-1-mm;
                    if (es[sss]*ww[nnn]*ww[mmm]<1e-16) continue;


                    // J-like
                    double TP = 0;

                    for (int i=0; i<npairs; ++i) {
                        for (int a=npairs; a<nBFs; ++a) {
                            const int ia = i*nBFs + a;
                            const int ai = (a-NO)*NO+i;
                            TP += W5.T(nnn,ia) * W5.T(mmm,ia) * ED(sss, ai);
                        }
                    }

                    Emp2j -= ww[nnn] * ww[mmm] * es[sss] * TP * TP;

                    // X-like
                    double Ex = 0;

                    for (int i=0; i<npairs; ++i) {
                        for (int j=0; j<npairs; ++j) {
                            double Xa = 0;
                            for (int a=npairs; a<nBFs; ++a) {
                                const int ia = i*nBFs + a;
                                const int ja = j*nBFs + a;
                                const int ai = (a-NO)*NO+i;
                                Xa += W5.T(nnn,ia) * W5.T(mmm,ja) * ED(sss, ai);
                            }
                            double Xb = 0;
                            for (int b=npairs; b<nBFs; ++b) {
                                const int jb = j*nBFs + b;
                                const int ib = i*nBFs + b;
                                const int bj = (b-NO)*NO+j;
                                Xb += W5.T(nnn,jb) * W5.T(mmm,ib) * ED(sss, bj);
                            }
                            Ex += Xa * Xb;
                        }
                    }

                    Emp2x -= ww[nnn] * ww[mmm] * es[sss] * Ex;
                }
            }
        }

        double Emp2 = 2*Emp2j-Emp2x;

        Messenger << "HF   energy: " << Ehf << endl;

        Messenger << "MP2j energy: " << 2*Emp2j << endl;
        Messenger << "MP2x energy: " << -Emp2x << endl;
        Messenger << "MP2  energy: " << Emp2 << endl;

        Messenger << "     energy: " << ENuc+Ehf+Emp2 << endl;

        return;
    }

    // naive RI algorithm
    if (0) {
        const int NO = npairs;
        const int NV = nBFs - NO;
        const int NOV = NO*NV;
        const int NN  = nBFs*nBFs;

        double * es = new double[NOV];

        for (int i=0; i<NO; ++i)
            for (int a=0; a<NV; ++a)
                es[a*NO+i] = En[NO+a] - En[i];

        tensor2 ED(NOV);

        for (int ia=0; ia<NOV; ++ia) {
            for (int jb=0; jb<NOV; ++jb) {
                ED(ia,jb) = 1./(es[ia]+es[jb]);
            }
        }

        Messenger << "diagonalizing 1" << endl;
        DiagonalizeV (ED, es);


        Messenger << "diagonalizing 2" << endl;

        // diagonalize
        double * ww = new double[NN];
        DiagonalizeV (W4.T, ww);

        Messenger << "transform" << endl;


        // first transformation
        // second transformation
        tensor2 WT(NN,NOV);
        WT.zeroize();

        for (int i=0; i<NO; ++i)  {
            for (int a=0; a<NV; ++a) {

                #pragma omp parallel for
                for (int nn=0; nn<NN; ++nn)  {
                    if (ww[NN-1-nn]<1.e-16) continue;

                    double sum = 0;

                    for (int p=0; p<nBFs; ++p)  {
                        for (int q=0; q<nBFs; ++q)  {
                            sum += C(i,p) * C(NO+a,q) * W4.T(NN-1-nn, p*nBFs+q);
                        }
                    }
                    const int ia = a*NO+i;
                    WT(nn,ia) = sum;
                }
            }
        }

        Messenger << "evaluate" << endl;


        double Emp2j = 0;
        double Emp2x = 0;


        // loops over quadrature points
        #pragma omp parallel for
        for (int ss=0; ss<NOV; ++ss)  {

            // loops over eigendensities
            for (int nn=0; nn<NN; ++nn)  {
                Messenger << ss << ":" << nn << "  "; //Messenger.flush();

                for (int mm=0; mm<NN; ++mm)  {

                    double p4 = es[NOV-1-ss]*ww[NN-1-nn]*ww[NN-1-mm];
                    if (p4<1.e-16) continue;

                    double s4j = 0;
                    double s4x = 0;

                    for (int i=0; i<NO; ++i) {
                        for (int a=0; a<NV; ++a) {
                            for (int j=0; j<NO; ++j) {
                                for (int b=0; b<NV; ++b) {
                                    const int ia = a*NO+i;
                                    const int ib = b*NO+i;
                                    const int ja = a*NO+j;
                                    const int jb = b*NO+j;

                                    s4j += WT(nn,ia)*WT(nn,jb) * WT(mm,ia)*WT(mm,jb) *  ED(ss,ia) * ED(ss,jb);
                                    s4x += WT(nn,ia)*WT(nn,jb) * WT(mm,ib)*WT(mm,ja) *  ED(ss,ia) * ED(ss,jb);
                                }
                            }
                        }
                    }

                    #pragma omp atomic
                    Emp2j += p4*s4j;
                    #pragma omp atomic
                    Emp2x += p4*s4x;
                }
            }
        }


        double Emp2 = 2*Emp2j-Emp2x;

        Messenger << "   HF   energy: " << Ehf << endl;

        Messenger << "RI-MP2j energy: " << 2*Emp2j << endl;
        Messenger << "RI-MP2x energy: " << -Emp2x << endl;
        Messenger << "RI-MP2  energy: " << Emp2 << endl;

        Messenger << "        energy: " << ENuc+Ehf+Emp2 << endl;



        delete[] ww;
        delete[] es;



    return;
    }

    // transform and compute MP2 energies
    if (0) {

        tensor4 W5(nBFs); //, W6(nBFs), W7(nBFs), W8(nBFs);

        Messenger << "Transforming 1/4" << endl;

        #pragma omp parallel for
        for (int b=0; b<nBFs; ++b) {
            Messenger << b << " "; //Messenger.flush();
            for (int c=0; c<nBFs; ++c) {
                for (int d=0; d<nBFs; ++d) {
                    for (int i=0; i<nBFs; ++i) {
                        double sum=0;
                        for (int a=0; a<nBFs; ++a) {
                            sum += C(i,a) * W4(a,b,c,d);
                        }
                        W5 (i,b,c,d) = sum;
                    }
                }
            }
        }

        Messenger << "Transforming 2/4" << endl;

        #pragma omp parallel for
        for (int i=0; i<nBFs; ++i) {
            Messenger << i << " "; //Messenger.flush();
            for (int c=0; c<nBFs; ++c) {
                for (int d=0; d<nBFs; ++d) {
                    for (int j=0; j<nBFs; ++j) {
                        double sum=0;
                        for (int b=0; b<nBFs; ++b) {
                            sum += C(j,b) * W5(i,b,c,d);
                        }
                        W4 (i,j,c,d) = sum;
                    }
                }
            }
        }

        Messenger << "Transforming 3/4" << endl;

        #pragma omp parallel for
        for (int i=0; i<nBFs; ++i) {
            Messenger << i << " "; //Messenger.flush();
            for (int j=0; j<nBFs; ++j) {
                for (int d=0; d<nBFs; ++d) {
                    for (int k=0; k<nBFs; ++k) {
                        double sum=0;
                        for (int c=0; c<nBFs; ++c) {
                            sum += C(k,c) * W4(i,j,c,d);
                        }
                        W5 (i,j,k,d) = sum;
                    }
                }
            }
        }

        Messenger << "Transforming 4/4" << endl;

        #pragma omp parallel for
        for (int i=0; i<nBFs; ++i) {
            Messenger << i << " "; //Messenger.flush();
            for (int j=0; j<nBFs; ++j) {
                for (int k=0; k<nBFs; ++k) {
                    for (int l=0; l<nBFs; ++l) {
                        double sum=0;
                        for (int d=0; d<nBFs; ++d) {
                            sum += C(l,d) * W5(i,j,k,d);
                        }
                        W4 (i,j,k,l) = sum;
                    }
                }
            }
        }


        // print
        double Emp2j = 0;
        double Emp2x = 0;

        for (int i=0; i<npairs; ++i) {
            for (int j=0; j<npairs; ++j) {
                for (int a=npairs; a<nBFs; ++a) {
                    for (int b=npairs; b<nBFs; ++b) {
                        Emp2j += (W4(i,a,j,b) * W4(i,a,j,b)) / (En[i]-En[a] + En[j]-En[b]);
                        Emp2x += (W4(i,a,j,b) * W4(i,b,j,a)) / (En[i]-En[a] + En[j]-En[b]);
                    }
                }
            }
        }

        double Emp2 = 2*Emp2j-Emp2x;

        Messenger << "HF   energy: " << Ehf << endl;

        Messenger << "MP2j energy: " << 2*Emp2j << endl;
        Messenger << "MP2x energy: " << -Emp2x << endl;
        Messenger << "MP2  energy: " << Emp2 << endl;

        Messenger << "     energy: " << ENuc+Ehf+Emp2 << endl;

        return;
    }



    // check exchange spectum
    if (0) {
        tensor4 X4(nBFs);

        for (int d=0; d<nBFs; ++d) {
            for (int c=0; c<nBFs; ++c) {
                for (int b=0; b<nBFs; ++b) {
                    for (int a=0; a<nBFs; ++a) {
                        X4(a,c,b,d) = W4(a,b,c,d);
                    }
                }
            }
        }

        double * x = new double[nBFs*nBFs];

        Messenger << "diagonalizing eXchange-like" << endl;

        DiagonalizeV (X4.T, x);
        Messenger.precision(10);
        for (int i=0; i<n2; ++i) Messenger << x[n2-1-i] << "   "; Messenger << endl;

        for (int i=0; i<n2; ++i) {
            Messenger << i << " " << x[n2-1-i] << ": ";
             for (int j=0; j<n2; ++j) Messenger << X4.T(n2-1-i, j) << " "; Messenger << endl << endl;
        }

        delete[] x;
        return;
    }

    // test HF energy
    if (0) {
        F2 = Hp;

        for (int d=0; d<nBFs; ++d) {
            for (int c=0; c<nBFs; ++c) {
                double Jsum=0;

                for (int b=0; b<nBFs; ++b) {
                    for (int a=0; a<nBFs; ++a) {
                        Jsum += D2(a,b) * W4(a,b,c,d);
                    }
                }
                F2(c,d) += 2*Jsum;
            }
        }

        for (int d=0; d<nBFs; ++d) {
            for (int b=0; b<nBFs; ++b) {
                double Xsum=0;

                for (int c=0; c<nBFs; ++c) {
                    for (int a=0; a<nBFs; ++a) {
                        Xsum += D2(a,c) * W4(a,b,c,d);
                    }
                }
                F2(b,d) -= Xsum;
            }
        }

        double * e = new double[nBFs];
        tensor2 S2;
        S2 = Sp;
        DiagonalizeGV (F2, S2, e);

        for (int n=0; n<nBFs; ++n) Messenger << e[n] << " "; Messenger << endl;

        delete[] e;

        return;
    }

    // test the Cholesky routine
    if (0) {
        Messenger << W4.T << endl << endl << endl;
        Messenger << "Compute Cholesky" << endl;

        tensor2 W2;
        W2 = W4.T;
        Cholesky (W2);


        Messenger << W2 << endl << endl;

        tensor2 W2T;

        W2T = W2; W2T.Transpose();

        tensor2 A;
        // this no longer computes the product; use TensorProduct
        //A = W2T*W2;

        Messenger << A << endl << endl;
    }

/*
        > make diagonal atom basis
        > rotate simple atoms in the supermatrix
        > project atoms basis off bonds (use DF to minimize residual; project out)
        > make diagonal bond basis
        > rotate atoms pairs in the supermatrix
*/

    /*
    if (0) {

        int nAtoms = sparsetensorpattern::natoms;
        Messenger << "n atoms : " << nAtoms << endl;

        Messenger.precision(16);

        int nn3 = (nBFs*nBFs+nBFs)/2;

        // compress tensor4 by roughly 4
        tensor2 W3(nn3);
        W3.zeroize();

        int os12 = 0;
        for (int a1=0; a1<nAtoms; ++a1) {
            for (int a2=0; a2<=a1; ++a2) {

                const int im = sparsetensorpattern::GetLen1(a1);
                const int jm = sparsetensorpattern::GetLen1(a2);
                const int i0 = sparsetensorpattern::GetOffset1(a1);
                const int j0 = sparsetensorpattern::GetOffset1(a2);
                const int ij0 = (a1==a2)?(im*im+im)/2:im*jm;


                int os34 = 0;
                for (int b1=0; b1<nAtoms; ++b1) {
                    for (int b2=0; b2<=b1; ++b2) {

                        // extract atom^2 superblock
                        // =========================


                        const int km = sparsetensorpattern::GetLen1(b1);
                        const int lm = sparsetensorpattern::GetLen1(b2);
                        const int k0 = sparsetensorpattern::GetOffset1(b1);
                        const int l0 = sparsetensorpattern::GetOffset1(b2);
                        const int kl0 = (b1==b2)?(km*km+km)/2:km*lm;


                        // extract tensor
                        tensor2 T(ij0, kl0);

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<((a1==a2)?i+1:jm); ++j) {
                                int ij = (a1==a2)?(i*i+i)/2+j:i*jm+j;
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        T (ij, kl) = W4(i0+i,j0+j, k0+k,l0+l);
                                    }
                                }
                            }
                        }

                        // store in tensor
                        for (int ij=0; ij<ij0; ++ij) {
                            for (int kl=0; kl<kl0; ++kl) {
                                W3(os12+ij,os34+kl) = T(ij,kl);
                            }
                        }

                        os34 += kl0;
                    }
                }

                os12 += ij0;
            }
        }

        // diagonalize
        {
            Messenger << "diagonalizing W3 tensor" << endl;
            double * w = new double[nn3];
            DiagonalizeE (W3, w);
            Messenger.precision(16);

            double sum = 0;
            for (int i=1;i<=min(2048,nn3); ++i) {
                sum += w[nn3-i];
                Messenger << i << " : " << w[nn3-i] << "     "; Messenger << endl;
            }
        }

        return;
    }
    */

    // compress the 2e tensor
    // and diagonalize it
    /*
    if (0) {
        int nAtoms = sparsetensorpattern::natoms;
        Messenger << "n atoms : " << nAtoms << endl;

        Messenger.precision(16);

        int nn3 = (nBFs*nBFs+nBFs)/2;

        // compress tensor4 by roughly 4
        tensor2 W3(nn3);
        W3.zeroize();

        int os12 = 0;
        for (int a1=0; a1<nAtoms; ++a1) {
            for (int a2=0; a2<=a1; ++a2) {

                const int im = sparsetensorpattern::GetLen1(a1);
                const int jm = sparsetensorpattern::GetLen1(a2);
                const int i0 = sparsetensorpattern::GetOffset1(a1);
                const int j0 = sparsetensorpattern::GetOffset1(a2);
                const int ij0 = (a1==a2)?(im*im+im)/2:im*jm;


                int os34 = 0;
                for (int b1=0; b1<nAtoms; ++b1) {
                    for (int b2=0; b2<=b1; ++b2) {

                        // extract atom^2 superblock
                        // =========================


                        const int km = sparsetensorpattern::GetLen1(b1);
                        const int lm = sparsetensorpattern::GetLen1(b2);
                        const int k0 = sparsetensorpattern::GetOffset1(b1);
                        const int l0 = sparsetensorpattern::GetOffset1(b2);
                        const int kl0 = (b1==b2)?(km*km+km)/2:km*lm;


                        // extract tensor
                        tensor2 T(ij0, kl0);

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<((a1==a2)?i+1:jm); ++j) {
                                int ij = (a1==a2)?(i*i+i)/2+j:i*jm+j;
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        T (ij, kl) = W4(i0+i,j0+j, k0+k,l0+l);
                                    }
                                }
                            }
                        }

                        // store in tensor
                        for (int ij=0; ij<ij0; ++ij) {
                            for (int kl=0; kl<kl0; ++kl) {
                                W3(os12+ij,os34+kl) = T(ij,kl);
                            }
                        }

                        os34 += kl0;
                    }
                }

                os12 += ij0;
            }
        }

        // diagonalize
        {
            Messenger << "diagonalizing W3 tensor" << endl;
            double * w = new double[nn3];
            DiagonalizeE (W3, w);
            Messenger.precision(16);

            double sum = 0;
            for (int i=1;i<=min(2048,nn3); ++i) {
                sum += w[nn3-i];
                Messenger << i << " : " << w[nn3-i] << "     "; Messenger << endl;
            }
        }

        return;
    }
    */

    /*
        MAKE DIAGONAL ATOM BASIS CUTOFF BEFORE THIS, SINCE OTHERWISE WE OBTAIN A SINGULAR H MATRIX
    */


/*
        > make diagonal atom basis
        > rotate simple atoms in the supermatrix
        > project atoms basis off bonds (use DF to minimize residual; project out)
        > make diagonal bond basis
        > rotate atoms pairs in the supermatrix
*/



    // compute a reduced basis for 2e
    /*
    if (0) {
        int nAtoms = sparsetensorpattern::natoms;
        Messenger << "n atoms : " << nAtoms << endl;

        Messenger.precision(16);

        const double CDthresh = 1e-10;
        r2tensor<tensor2> DB;
        DB.set(nAtoms,nAtoms);
        int nn2 = 0;


        // fit bond product density with atomic basis and compute residual
        for (int a1=0; a1<nAtoms; ++a1) {
            for (int a2=0; a2<a1; ++a2) {
                Messenger << a1 << " " << a2 << endl;

                // extract atom^2 superblock
                // =========================

                const int len1 = sparsetensorpattern::GetLen1(a1);
                const int len2 = sparsetensorpattern::GetLen1(a2);

                const int len11 = (len1*len1+len1)/2;
                const int len22 = (len2*len2+len2)/2;

                const int i0 = sparsetensorpattern::GetOffset1(a1);
                const int j0 = sparsetensorpattern::GetOffset1(a2);

                const int len12 = len1*len2;
                const int len1122 = len11+len22;

                tensor2 H, C, R;

                H.setsize (len11+len22);
                C.setsize (len11+len22, len12);
                R.setsize (len11+len22, len12);

                tensor2 HH, HS, SH, SS;
                HH.setsize(len1122);
                HS.setsize(len1122, len12);
                SH.setsize(len12, len1122);
                SS.setsize(len12);


                // copy atom 1 block
                for (int i=0; i<len1; ++i) {
                    for (int j=0; j<=i; ++j) {
                        for (int k=0; k<len1; ++k) {
                            for (int l=0; l<=k; ++l) {
                                int ij = (i*i+i)/2+j;
                                int kl = (k*k+k)/2+l;

                                HH(ij,kl) = H (ij, kl) = W4(i0+i,i0+j, i0+k,i0+l);
                            }
                        }
                    }
                }

                // copy atom 2 block
                for (int i=0; i<len2; ++i) {
                    for (int j=0; j<=i; ++j) {
                        for (int k=0; k<len2; ++k) {
                            for (int l=0; l<=k; ++l) {
                                int ij = len11 + (i*i+i)/2+j;
                                int kl = len11 + (k*k+k)/2+l;

                                HH(ij,kl) = H (ij, kl) = W4(j0+i,j0+j, j0+k,j0+l);
                            }
                        }
                    }
                }

                // cross terms
                for (int i=0; i<len1; ++i) {
                    for (int j=0; j<=i; ++j) {
                        for (int k=0; k<len2; ++k) {
                            for (int l=0; l<=k; ++l) {
                                int ij =         (i*i+i)/2+j;
                                int kl = len11 + (k*k+k)/2+l;

                                HH(ij,kl) = HH(kl,ij) = H (ij, kl) = H (kl,ij) = W4(i0+i,i0+j, j0+k,j0+l);
                            }
                        }
                    }
                }

                // vector coefficients: atom 1
                for (int i=0; i<len1; ++i) {
                    for (int j=0; j<=i; ++j) {
                        for (int k=0; k<len1; ++k) {
                            for (int l=0; l<len2; ++l) {
                                int ij = (i*i+i)/2+j;
                                int kl = k*len2+l;

                                HS(ij,kl) = SH(kl,ij) = C (ij, kl) = W4(i0+i,i0+j, i0+k,j0+l);
                            }
                        }
                    }
                }

                // vector coefficients: atom 2
                for (int i=0; i<len2; ++i) {
                    for (int j=0; j<=i; ++j) {
                        for (int k=0; k<len1; ++k) {
                            for (int l=0; l<len2; ++l) {
                                int ij = len11 + (i*i+i)/2+j;
                                int kl = k*len2+l;

                                HS(ij,kl) = SH(kl,ij) = C (ij, kl) = W4(j0+i,j0+j, i0+k,j0+l);
                            }
                        }
                    }
                }

                // bond 12
                for (int i=0; i<len1; ++i) {
                    for (int j=0; j<len2; ++j) {
                        for (int k=0; k<len1; ++k) {
                            for (int l=0; l<len2; ++l) {
                                int ij = i*len2+j;
                                int kl = k*len2+l;

                                SS(ij,kl) = W4(i0+i,j0+j, i0+k,j0+l);
                            }
                        }
                    }
                }


                // invert matrix H
                // ===============
                // OBS! the matrix is often close to singular
                {
                    Messenger << "before" << endl;
                    Messenger << H << endl;

                    int info;
                    int * ipiv = new int[H.n];
                    dgetrf_(&H.n, &H.n, H.c2, &H.n, ipiv, &info); // LU decomposition

                    int lwork = -1;
                    double * work = NULL;
                    double work1 = 0;
                    dgetri_(&H.n, H.c2, &H.n, ipiv, &work1, &lwork, &info); // get info about optimal work
                    lwork = (int) work1;

                    work = new double[lwork];
                    dgetri_(&H.n, H.c2, &H.n, ipiv, work, &lwork, &info); // invert the matrix from decomposition

                    if (info!=0) {
                        Messenger << "error inverting H:  " << info << endl;
                    }

                    delete[] work;
                    delete[] ipiv;

                    Messenger << "after:" << endl;
                    Messenger << H << endl;
                }


                // solve R = H^-1 * C'
                // ===================
                {
                    for (int i=0; i<len1122; ++i) {
                        for (int k=0; k<len12; ++k) {
                            double sum = 0;
                            for (int j=0; j<len1122; ++j) {
                                sum += H(i,j)*C(j,k);
                            }
                            R(i,k) = sum;
                        }
                    }
                }

                // save the matrix R

                // linear combination of rows
                // ==========================
                for (int b1=0; b1<nAtoms; ++b1) {
                    for (int b2=0; b2<=b1; ++b2) {

                        const int im = sparsetensorpattern::GetLen1(a1);
                        const int jm = sparsetensorpattern::GetLen1(a2);
                        const int km = sparsetensorpattern::GetLen1(b1);
                        const int lm = sparsetensorpattern::GetLen1(b2);

                        const int i0 = sparsetensorpattern::GetOffset1(a1);
                        const int j0 = sparsetensorpattern::GetOffset1(a2);
                        const int k0 = sparsetensorpattern::GetOffset1(b1);
                        const int l0 = sparsetensorpattern::GetOffset1(b2);


                        const int len34 = (b1==b2)?(km*km+km)/2:km*lm;

                        // extract tensors
                        // ===============
                        tensor2 TA  (len11, len34);
                        tensor2 TB  (len22, len34);
                        tensor2 TAB (len12, len34);

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<=i; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = (i*i+i)/2+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        TA (ij, kl) = W4(i0+i,i0+j, k0+k,l0+l);
                                    }
                                }
                            }
                        }

                        for (int i=0; i<jm; ++i) {
                            for (int j=0; j<=i; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = (i*i+i)/2+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        TB (ij, kl) = W4(j0+i,j0+j, k0+k,l0+l);
                                    }
                                }
                            }
                        }

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<jm; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = i*jm+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        TAB (ij, kl) = W4(i0+i,j0+j, k0+k,l0+l);
                                    }
                                }
                            }
                        }

                        // make linear combination
                        for (int i=0; i<len12; ++i) {
                            for (int k=0; k<len34; ++k) {
                                double sum = 0;
                                for (int j=0; j<len11; ++j)
                                    sum += R(j,i)*TA(j,k);
                                for (int j=0; j<len22; ++j)
                                    sum += R(len11+j,i)*TB(j,k);
                                TAB(i,k) -= sum;
                            }
                        }

                        // leave the tensor where it was
                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<jm; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = i*jm+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        W4(i0+i,j0+j, k0+k,l0+l) = TAB (ij, kl);
                                    }
                                }
                            }
                        }


                    }
                }


                // linear combination of cols
                // ==========================
                for (int b1=0; b1<nAtoms; ++b1) {
                    for (int b2=0; b2<=b1; ++b2) {

                        const int im = sparsetensorpattern::GetLen1(a1);
                        const int jm = sparsetensorpattern::GetLen1(a2);
                        const int km = sparsetensorpattern::GetLen1(b1);
                        const int lm = sparsetensorpattern::GetLen1(b2);

                        const int i0 = sparsetensorpattern::GetOffset1(a1);
                        const int j0 = sparsetensorpattern::GetOffset1(a2);
                        const int k0 = sparsetensorpattern::GetOffset1(b1);
                        const int l0 = sparsetensorpattern::GetOffset1(b2);


                        const int len34 = (b1==b2)?(km*km+km)/2:km*lm;

                        // extract tensors
                        // ===============
                        tensor2 TA  (len11, len34);
                        tensor2 TB  (len22, len34);
                        tensor2 TAB (len12, len34);

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<=i; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = (i*i+i)/2+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        TA (ij, kl) = W4(k0+k,l0+l, i0+i,i0+j);
                                    }
                                }
                            }
                        }

                        for (int i=0; i<jm; ++i) {
                            for (int j=0; j<=i; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = (i*i+i)/2+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        TB (ij, kl) = W4(k0+k,l0+l, j0+i,j0+j);
                                    }
                                }
                            }
                        }

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<jm; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = i*jm+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        TAB (ij, kl) = W4(k0+k,l0+l,  i0+i,j0+j);
                                    }
                                }
                            }
                        }

                        // make linear combination
                        for (int i=0; i<len12; ++i) {
                            for (int k=0; k<len34; ++k) {
                                double sum = 0;
                                for (int j=0; j<len11; ++j)
                                    sum += R(j,i)*TA(j,k);
                                for (int j=0; j<len22; ++j)
                                    sum += R(len11+j,i)*TB(j,k);
                                TAB(i,k) -= sum;
                            }
                        }

                        // leave the tensor where it was
                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<jm; ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = i*jm+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        W4(k0+k,l0+l, i0+i,j0+j) = TAB (ij, kl);
                                    }
                                }
                            }
                        }

                    }
                }





                // update atom blocks
                // ==================
                {
                    // SS block
                    for (int ij=0; ij<len12; ++ij) {
                        for (int kl=0; kl<len12; ++kl) {

                            double sum = 0;

                            for (int p=0; p<len1122; ++p) sum += R(p,ij) * HS(p,kl);
                            for (int p=0; p<len1122; ++p) sum += R(p,kl) * SH(ij,p);

                            SS(ij,kl) -= sum;

                            sum = 0;

                            for (int p=0; p<len1122; ++p)
                                for (int q=0; q<len1122; ++q)
                                    sum += R(p,ij) * R(q,kl) * HH(p,q);

                            SS(ij,kl) += sum;
                        }
                    }
                    // HS blocks
                    for (int ij=0; ij<len12; ++ij) {
                        for (int kl=0; kl<len1122; ++kl) {

                            double sum = 0;

                            for (int p=0; p<len1122; ++p)                              sum += R(      p,ij) * AA(p,kl);

                            SH(ij,kl) -= sum;
                            HS(kl,ij) -= sum;
                        }
                    }

                    // actually the whole block row and column needs to be updated
                    // ===========================================================
                }

            }
        }



        // generate diagonal product basis
        for (int a1=0; a1<nAtoms; ++a1) {
            for (int a2=0; a2<=a1; ++a2) {

                // extract atom^2 superblock
                // =========================

                const int len1 = sparsetensorpattern::GetLen1(a1);
                const int len2 = sparsetensorpattern::GetLen1(a2);

                const int i0 = sparsetensorpattern::GetOffset1(a1);
                const int j0 = sparsetensorpattern::GetOffset1(a2);

                tensor2 & Wa12 = DB(a1,a2);

                int len12;

                if (a1==a2) len12 = (len1*len1+len1)/2;
                else        len12 = len1*len2;

                Wa12.setsize(len12);


                if (a1==a2) {
                    for (int i=0; i<len1; ++i) {
                        for (int j=0; j<=i; ++j) {
                            for (int k=0; k<len1; ++k) {
                                for (int l=0; l<=k; ++l) {
                                    Wa12 ((i*i+i)/2+j, (k*k+k)/2+l) = W4(i0+i,j0+j, i0+k,j0+l);
                                }
                            }
                        }
                    }
                }
                else {
                    for (int i=0; i<len1; ++i) {
                        for (int j=0; j<len2; ++j) {
                            for (int k=0; k<len1; ++k) {
                                for (int l=0; l<len2; ++l) {
                                    Wa12 (i*len2+j, k*len2+l) = W4(i0+i,j0+j, i0+k,j0+l);
                                }
                            }
                        }
                    }
                }



                // compute eigensystem
                // ====================

                double * w = new double[len12];

                DiagonalizeV(Wa12, w);

                for (int i=0; i<len12/2; ++i) {
                    for (int j=0; j<len12; ++j) {
                        swap(Wa12(i,j), Wa12(len12-1-i,j));
                    }
                    swap(w[i],w[len12-1-i]);
                }

                int nev; for (nev=0; nev<len12; ++nev) if (w[nev]<CDthresh) break;

                // zero negligible contributions
                for (int i=nev; i<len12; ++i) {
                    for (int j=0; j<len12; ++j)
                        Wa12(i,j) = 0;
                    w[i]=0;
                }

                Wa12.n = nev;

                // increment total number of functions
                nn2 += nev;

                delete[] w;
            }
        }


        Messenger << "dim   = " << nBFs << endl;
        Messenger << "dim^2 = " << n2 << endl;
        Messenger << "dimr  = " << nn2 << endl;

        //compressed tensor
        tensor2 W2(nn2);

        int os12 = 0;
        // loop over all atom pairs
        for (int a1=0; a1<nAtoms; ++a1) {
            for (int a2=0; a2<=a1; ++a2) {

                int os34 = 0;

                // loop over all atom pairs
                for (int b1=0; b1<nAtoms; ++b1) {
                    for (int b2=0; b2<=b1; ++b2) {

                        // extract atom^2 superblock
                        // =========================

                        const int im = sparsetensorpattern::GetLen1(a1);
                        const int jm = sparsetensorpattern::GetLen1(a2);
                        const int km = sparsetensorpattern::GetLen1(b1);
                        const int lm = sparsetensorpattern::GetLen1(b2);

                        const int i0 = sparsetensorpattern::GetOffset1(a1);
                        const int j0 = sparsetensorpattern::GetOffset1(a2);
                        const int k0 = sparsetensorpattern::GetOffset1(b1);
                        const int l0 = sparsetensorpattern::GetOffset1(b2);

                        const int ij0 = (a1==a2)?(im*im+im)/2:im*jm;
                        const int ijf = DB(a1,a2).n;
                        const int kl0 = (b1==b2)?(km*km+km)/2:km*lm;
                        const int klf = DB(b1,b2).n;

                        tensor2 & WL = DB(a1,a2);
                        tensor2 & WR = DB(b1,b2);

                        // extract tensor
                        tensor2 B0(ij0, kl0);

                        //Messenger << a1 << " " << a2 << " " << b1 << " " << b2 << endl;

                        for (int i=0; i<im; ++i) {
                            for (int j=0; j<((a1==a2)?i+1:jm); ++j) {
                                for (int k=0; k<km; ++k) {
                                    for (int l=0; l<((b1==b2)?k+1:lm); ++l) {
                                        int ij = (a1==a2)?(i*i+i)/2+j:i*jm+j;
                                        int kl = (b1==b2)?(k*k+k)/2+l:k*lm+l;
                                        B0 (ij, kl) = W4(i0+i,j0+j, k0+k,l0+l);
                                    }
                                }
                            }
                        }

                        //Messenger << a1 << " " << a2 << " " << b1 << " " << b2 << endl;

                        // left-multiply
                        tensor2 B1(ijf, kl0);

                        for (int p=0; p<ijf; ++p) {
                            for (int q=0; q<kl0; ++q) {
                                double sum = 0;
                                for (int r=0; r<ij0; ++r)
                                    sum += B0(r,q) * WL(p,r);
                                B1(p,q) = sum;
                            }
                        }

                        //Messenger << a1 << " " << a2 << " " << b1 << " " << b2 << endl;

                        // right-multiply
                        tensor2 B2(ijf, klf);

                        for (int p=0; p<ijf; ++p) {
                            for (int q=0; q<klf; ++q) {
                                double sum = 0;
                                for (int r=0; r<kl0; ++r)
                                    sum += B1(p,r) * WR(q,r);
                                B2(p,q) = sum;
                            }
                        }

                        //Messenger << a1 << " " << a2 << " " << b1 << " " << b2 << endl;

                        // store in tensor
                        for (int ij=0; ij<ijf; ++ij) {
                            for (int kl=0; kl<klf; ++kl) {
                                W2(os12+ij,os34+kl) = B2(ij,kl);
                            }
                        }

                        //Messenger << a1 << " " << a2 << " " << b1 << " " << b2 << endl;

                        os34 += DB(b1,b2).n;
                    }
                }

                os12 += DB(a1,a2).n;
            }
        }

        //Messenger << W2 << endl;
        //WriteTensor("W2.txt", W2);



        Messenger << "diagonalizing compressed tensor" << endl;
        double * w = new double[nn2];
        DiagonalizeE (W2, w);
        Messenger.precision(16);

        double sum = 0;
        for (int i=1;i<=min(2048,nn2); ++i) {
            sum += w[nn2-i];
            Messenger << i << " : " << w[nn2-i] << "     "; Messenger << endl;
        }

    }
    */

    // check semidefinite positiveness
    if (0) {

        // W4.T.zeroize();
        // double w2 = 1; //0.25*0.25*0.25;
        // EchidnaSolver.GetFullTensor(W4, Tthresh, w2);



        int n2h =  (n2+nBFs)/2;

        tensor2 WW( n2h );
        double * w = new double[n2h];

        for (int i=0; i<nBFs; ++i) {
            for (int j=0; j<=i; ++j) {
                for (int k=0; k<nBFs; ++k) {
                    for (int l=0; l<=k; ++l) {
                        WW((i*i+i)/2+j, (k*k+k)/2+l) = -W4 (i,j,k,l);
                    }
                }
            }
        }

        Messenger << "diagonalizing" << endl;

        DiagonalizeV (WW, w);
        Messenger.precision(16);

        // correct sign
        for (int i=0; i<n2h; ++i) w[i] = -w[i];
        // print a few
        for (int i=0;i<n2h; i+=nBFs) Messenger << i << " : " << w[i] << "     "; Messenger << endl;

        // number of spanning vectors
        int ii;
        double sum = 0;
        for (ii=n2h/2;ii>=0; --ii) {
            sum += w[ii]*w[ii];
            if (sum>1e-8) break;
        }
        sum = sqrt(sum);
        Messenger << ii << " : " << w[ii] << "  " << sum << endl;



        for (int i=0; i<ii; ++i) {
            double v2 = 0;
            double i2 = i;

            for (int j=i; j<n2h; ++j) {
                double sum = 0;
                for (int k=i; k<ii; ++k) {
                    sum += WW(k,j)*WW(k,j);
                }
                if (sum>v2) {
                    v2 = sum;
                    i2 = j;
                }
            }
            Messenger << i << " " << sqrt(v2);

            // swap the vecs
            for (int k=0; k<ii; ++k) swap(WW(k,i),WW(k,i2));

            // apply a household transformation
            double v[ii];

            for (int k=0;k<ii;++k) v[k] = 0;
            for (int k=i;k<ii;++k) v[k] = WW(k,i);

            double s2=0; for (int k=0;k<ii;++k) s2 += v[k]*v[k]; // norm
            double s = sqrt(s2);
            double iH;

            if (v[i]>0) {
                iH = 1./(s2 + s*v[i]);
                v[i] += s;
            }
            else {
                iH = 1./(s2 - s*v[i]);
                v[i] -= s;
            }

            for (int j=i; j<n2h; ++j) {
                // inner product
                double s=0; for (int k=0;k<ii;++k) s += v[k]*WW(k,j);
                // outprojection
                for (int k=0;k<ii;++k) WW(k,j) -= s*iH*v[k];
            }

            Messenger << " " << WW(i,i) << endl;
        }

        /*
        // sort according to occupation
        for (int i=0; i<n2h; ++i) {
            double fi = f[i];
            int ipiv = i;
            for (int j=i+1; j<n2h; ++j) {
                if (f[j]>fi) {
                    fi = f[j];
                    ipiv = j;
                }
            }
            swap(f[i],f[ipiv]);
            for (int k=0; k<ii; ++k) swap(WW(k,i),WW(k,ipiv));
        }


        tensor2 SS(ii);
        SS.zeroize();

        for (int k=0; k<n2h; ++k)
            for (int i=0; i<ii; ++i)
                for (int j=0; j<ii; ++j)
                    SS(i,j) -= WW(i,k)*WW(j,k);

        DiagonalizeV (SS, w);
        // correct sign
        for (int i=0; i<ii; ++i) w[i] = -w[i];
        for (int i=0; i<ii; ++i) Messenger << w[i] << " "; Messenger << endl;

        // rotate the subspace according to we
        for (int i=0; i<n2h; ++i) {
            double t[ii];
            for (int j=0; j<ii; ++j) {
                double sum = 0;
                for (int k=0; k<ii; ++k) {
                    sum += SS(k,j)*WW(j,i);
                }
                t[j] = sum;
            }

            for (int j=0; j<ii; ++j) WW(j,i) = t[j];
        }

        //WW.Transpose();
        */

        //WW.Transpose();

        // actually, the maximally linearly independent set should be at the beginning
        int jj = n2h - ii;
        // construct
        {
            int info;
            int lda = n2h;
            int ldb = n2h;
            int ipiv[ii];
            dgesv_( &ii, &jj, WW.c2, &lda, ipiv, WW.c2+ii, &ldb, &info);
        }

        {
            int m = ii, n = ii, lda = n2h, ldu = m, ldvt = n, info, lwork;
            double wkopt;
            double* work;

            double s[n], u[ldu*m], vt[ldvt*n];

            lwork = -1;
            dgesvd_( "A", "A", &m, &n, WW.c2+ii, &lda, s, u, &ldu, vt, &ldvt, &wkopt, &lwork, &info);
            lwork = (int)wkopt;
            work = new double[lwork];

            dgesvd_ ("A", "A", &m, &n, WW.c2+ii, &lda, s, u, &ldu, vt, &ldvt, work, &lwork, &info);

            Messenger << "SVD" << endl;
            for (int i=0; i<ii; ++i) Messenger << s[i] << " "; Messenger << endl;
        }


        /*
        // build overlap
        tensor2 SS(n2h);
        SS.zeroize();

        for (int k=0; k<ii; ++k)
            for (int i=0; i<n2h; ++i)
                for (int j=0; j<n2h; ++j)
                    SS(i,j) += WW(k,i)*WW(k,j);

        // Cholesky + pivot
        // compute localized basis
        int ncd=0;
        for (int i=0; i<n2h; ++i) {
            double ss = SS(i,i);
            int ipiv = i;
            for (int j=i+1; j<n2h; ++j) {
                if (SS(j,j)>ss) {
                    ipiv = j;
                    ss = SS(j,j);
                }
            }

            if (ss<=0) break;

            // swap
            for (int j=0; j<n2h; ++j) swap(SS(i,j),SS(ipiv,j));
            for (int j=0; j<n2h; ++j) swap(SS(j,i),SS(j,ipiv));

            // norm
            double t = 1./sqrt(SS(i,i));

            // fill mem with Cholesky vectors
            for (int j=i;   j<n2h; ++j) SS(i,j) *= t;
            for (int j=i+1; j<n2h; ++j) SS(j,i) = 0;

            // update remaining matrix
            for (int j=i+1; j<n2h; ++j)
                for (int k=i+1; k<n2h; ++k)
                    SS(j,k) -= SS(i,k)*SS(i,j);

            ncd = i;
        }

        Messenger << endl << endl;


        for (int i=0;i<ncd; i++) Messenger << i << " : " << SS(i,i) << "     "; Messenger << endl;

        Messenger.precision(5);
        for (int i=0; i<ncd; ++i) {
            for (int k=0; k<n2h; ++k)
                Messenger << " " << SS(i,k);
            Messenger << endl;
        }
        Messenger << endl;
        Messenger << endl;
        */

        /*
        DiagonalizeV (SS, w);
        // correct sign
        for (int i=0; i<n2h; ++i) w[i] = -w[i];

        Messenger << "S evs" << endl;
        for (int i=0;i<n2h; i+=nBFs) Messenger << i << " : " << w[i] << "     "; Messenger << endl;
        */
        delete[] w;
    }

    // try SR operators
    if (0) {
        int n2h =  (n2+nBFs)/2;

        tensor2 WW( n2h );
        double * w = new double[n2h];

        double w2 = 1;

        for (int oo=0; oo<6; ++oo) {

            EchidnaSolver.GetFullTensor(W4, Tthresh, w2);

            for (int i=0; i<nBFs; ++i) {
                for (int j=0; j<=i; ++j) {
                    for (int k=0; k<nBFs; ++k) {
                        for (int l=0; l<=k; ++l) {
                            WW((i*i+i)/2+j, (k*k+k)/2+l) = W4 (i,j,k,l);
                        }
                    }
                }
            }

            Messenger << "diagonalizing with w2=" << w2 << endl;

            DiagonalizeE (WW, w);
            Messenger.precision(16);
            //for (int i=0; i<n2; ++i) Messenger << w[i] << "   "; Messenger << endl;

            double sum = 0;
            //for (int i=1;i<=min(2048,n2h); ++i) {
            for (int i=1;i<=n2h; i+=nBFs) {
                //sum += w[n2h-i];
                Messenger << i << " : " << w[n2h-i] << "     "; Messenger << endl;
            }

            int ii;

            for (ii=n2h/2; ii<n2h; ++ii) {
                sum += w[ii]*w[ii];
                if (sum>1e-8) break;
            }
            sum = sqrt(sum);

            Messenger << n2h-ii-1 << " : " << w[ii] << "  " << sum << endl;

            w2*=0.25;
        }

        delete[] w;
    }



    // CD - MP2 with O(N^3) Cholesky
    // =============================

    if (1) {
        // 2017/01/19
        // try CD+ALS factorization
        // 2017/01/28
        // the main source for numerical error seems to be that:
        // 1) the matrix is (as we expect), very ill-conditioned
        // 2) the CD can't go beyond 16 decimal places of numerical accuracy (double precission)
        // 3) the contamination to the quadrature from discarded eigenfunctions scales as sqrt(condition number)
        // to deal with this we need:
        // 1) better precision in critical calculations (gamma, for one)
        // 2) a spectral transformation on M
        //

        // store the diagonal
        double * ww = new double[N2];
        for (int ij=0; ij<N2; ++ij) ww[ij] = W4.T(ij,ij);


        {
            const double CDTHRESH = 1e-12; //1e-14;
            const double DIATHRESH = 1e-12; //1e-14;
            const double ALSTHRESH = 1e-20; //10^-10 ^2
            const int MAXM = N2; //min(32,N) * N;


            // DO THE MODIFIED CD IN O(N^4)
            // ============================

            tensor2 * ZZ = new tensor2[MAXM];

            // compute sum of ev's
            double dd=0; for (int ij=0; ij<N2; ++ij) dd+=ww[ij];

            Messenger << "Matrix sum of ev's = " << dd << endl;


            int M=0;

            double tr=0; // count the trace norm of the reconstructed matrix

            while (M<MAXM) {

                // count whether the remaining trace (sum of leftover ev's) is worth the effort
                if (dd-tr < CDTHRESH*dd) break;

                // find largest value in diagonal
                int ij=0;
                for (int kl=0; kl<N2; ++kl) if (ww[kl]>ww[ij]) ij=kl;

                int ii = ij/N;
                int jj = ij%N;

                Messenger << "it = " << M << "   largest element = " << ii << ","<<jj << "   value = " << ww[ij] << "   ";

                // store the centre
                //Rgrid[M] = MidPoint(au[ii], au[jj]);

                // initialize the new tensor
                ZZ[M].setsize(N);


                // now compute the full column of integrals
                //#pragma omp parallel for
                for (int kk=0; kk<N; ++kk) {
                    for (int ll=0; ll<=kk; ++ll) {
                        ZZ[M](kk,ll) = W4(ii,jj, kk,ll); // S4int (ii,jj, kk,ll, au,SS,kw); //ERI (ii,jj, kk,ll, au,SS,K22);
                    }
                }

                // update the column by removing all previous components
                for (int m=0; m<M; ++m) {
                    double w = ZZ[m](ii,jj);

                    #pragma omp parallel for
                    for (int kk=0; kk<N; ++kk)
                        for (int ll=0; ll<=kk; ++ll)
                            ZZ[M](kk,ll) -= w * ZZ[m](kk,ll);
                }

                // fill in the transposed
                for (int kk=0; kk<N; ++kk)
                    for (int ll=0; ll<kk; ++ll)
                        ZZ[M](ll,kk) = ZZ[M](kk,ll);

                // scale the vector
                double iw = 1./sqrt(ZZ[M](ii,jj));
                ZZ[M] *= iw;

                // count the trace contribution to the matrix
                // which coincides with the vector norm^2
                double n2=0;
                for (int kk=0; kk<N; ++kk)
                    for (int ll=0; ll<N; ++ll)
                        n2 += ZZ[M](kk,ll) * ZZ[M](kk,ll);
                tr+=n2;

                Messenger << "norm^2 = " << n2 << "   " << "trace = " << dd-tr << endl;


                // update diagonal
                for (int kk=0; kk<N; ++kk)
                    for (int ll=0; ll<N; ++ll)
                        ww[kk*N+ll] -= ZZ[M](kk,ll)*ZZ[M](kk,ll);

                // incremet rank
                ++M;
            }

            Messenger << "N  = " << N << endl;
            Messenger << "N2 = " << N2 << endl;
            Messenger << "M  = " << M << endl;

            if (M==MAXM) Messenger << "Cholesky failed to decompose matrix within prescribed number of elements" << endl;


            // now transform to MO (O/V) basis
            // ===============================

            const int nN = C.n;    // NMO
            const int nA = C.m;    // NAO
            const int nO = npairs;
            const int nV = nN - nO;

            // set two dummy tensors to map the occupied
            // and virtual orbitals of C
            tensor2 CO (nO,nA,   C[ 0] );
            tensor2 CV (nV,nA,   C[nO] );


            tensor2 * RR = new tensor2[M];

            for (int m=0; m<M; ++m) {
                tensor2 T; // temp
                TensorProduct    (T, CO, ZZ[m]);
                TensorProductNNT (RR[m], T, CV);
            }

            // rebuild the tensor (in MO basis)
            // ================================
            W4.T.zeroize();

            for (int i=0; i<nO; ++i) {
                for (int a=0; a<nV; ++a) {
                    for (int j=0; j<nO; ++j) {
                        for (int b=0; b<nV; ++b) {
                            double ww=0;
                            for (int m=0; m<M; ++m) ww += RR[m](i,a) * RR[m](j,b);
                            //Messenger << i << " " << a << " " << j << " " << b << "   " << ww << "    " << (En[nO+a]-En[i] + En[nO+b]-En[j]) << endl;
                            W4(i,a, j,b) = ww;
                        }
                    }
                }
            }


            // print
            double Emp2j = 0;
            double Emp2x = 0;

            for (int i=0; i<nO; ++i) {
                for (int j=0; j<nO; ++j) {
                    for (int a=0; a<nV; ++a) {
                        for (int b=0; b<nV; ++b) {
                            Emp2j -= (W4(i,a,j,b) * W4(i,a,j,b)) / (En[nO+a]-En[i] + En[nO+b]-En[j]);
                            Emp2x -= (W4(i,a,j,b) * W4(i,b,j,a)) / (En[nO+a]-En[i] + En[nO+b]-En[j]);
                        }
                    }
                }
            }

            double Emp2 = 2*Emp2j-Emp2x;

            Messenger << "MP2j energy: " << 2*Emp2j << endl;
            Messenger << "MP2x energy: " << -Emp2x << endl;
            Messenger << "MP2  energy: " << Emp2 << endl;




            // what can be done now?
            // =====================
            // 1) test integrals with LL
            // 2) try to improve XX using the dictionary learning technique
            // 3) go for the N^3 algorithm
            // 4) optimize the points
            // 5) diagonalize ZZ? (to split everything better)

        }




        return;

    }


}

