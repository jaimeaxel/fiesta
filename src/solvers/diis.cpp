#include <cmath>
#include <cstring>
#include <cstdlib>
#include <vector>

#include "linear/eigen.hpp"
#include "linear/tensors.hpp"
#include "diis.hpp"

using namespace std;

DIIS::DIIS() :
    min_dim (1), max_dim (8), dim (0), ind (0), bb (NULL)
{
    init();
}

DIIS::DIIS(int min_vecs, int max_vecs) :
    min_dim (min_vecs), max_dim (max_vecs), dim (0), ind (0), bb (NULL)
{
    init();
}

DIIS::~DIIS()
{
    delete[] bb;
    bb = NULL;
}

void DIIS::init()
{
    Fa.resize(max_dim);
    Fb.resize(max_dim);
    Ga.resize(max_dim);
    Gb.resize(max_dim);

    B.setsize(max_dim+1, max_dim+1);
    B.zeroize();

    bb = new double [max_dim+1];
    memset(bb, 0, (max_dim+1)*sizeof(double));
}

void DIIS::update(tensor2* FF, tensor2* GG, int N)
{
    // increment dim until max_dim
    if (dim < max_dim) {
        dim = ind + 1;

        for (int i = 0; i < max_dim+1; i++) {
            B[i][dim] = -1.0;
            B[dim][i] = -1.0;
        }
        B[dim][dim] = 0.0;

        bb[dim-1] =  0.0;
        bb[dim]   = -1.0;
    }

    // update F and G from input
    Fa[ind] = FF[0];
    Ga[ind] = GG[0];
    if (N > 1) {
        Fb[ind] = FF[1];
        Gb[ind] = GG[1];
    }

    // update corresponding row/col in B matrix
    for (int row = 0; row < dim; row++) {
        for (int col = row; col < dim; col++) {
            if (ind != row && ind != col) continue;

            B[row][col] = DotProduct (Ga[row], Ga[col]);

            if (N > 1) {
                B[row][col] += DotProduct (Gb[row], Gb[col]);
                B[row][col] *= 0.5;
            }

            if (row != col) B[col][row] = B[row][col];
        }
    }

    // move current index
    ind += 1;
    if (ind == max_dim) { ind = 0; }

    // apply DIIS update
    if (dim >= min_dim) {
        double* tmp_B  = new double [(dim+1)*(dim+2)/2];
        double* tmp_bb = new double [dim+1];

        // column-majored; low-triangular
        for (int row = 0, ind = 0; row < dim+1; row++) {
            for (int col = row; col < dim+1; col++, ind++) {
                tmp_B[ind] = B[row][col];
            }
        }

        memset(tmp_bb, 0, dim*sizeof(double));
        tmp_bb[dim] = -1.0;
        
        SolveLinear('L', dim+1, tmp_B, tmp_bb);

        tensor2 tmp_F;

        // update Fock of alpha spin
        FF[0].zeroize();
        for (int i = 0; i < dim; i++) {
            tmp_F = Fa[i];
            tmp_F *= tmp_bb[i];
            FF[0] += tmp_F;
        }

        if (N > 1) {
            // update Fock of beta spin
            FF[1].zeroize();
            for (int i = 0; i < dim; i++) {
                tmp_F = Fb[i];
                tmp_F *= tmp_bb[i];
                FF[1] += tmp_F;
            }
        }

        delete[] tmp_B;
        delete[] tmp_bb;
        tmp_B  = NULL;
        tmp_bb = NULL;
    }
}
