#ifndef __RSP__
#define __RSP__

#include <string>

// data structure for storing response vectors in parallel
struct multivec;


struct RSPparams {

    enum class RSPmode {NONE, REAL, IMAG, CPP};
    enum class RSPgrad {NONE, DIP, BED};

    RSPmode mode = RSPmode::NONE;
    RSPgrad grad = RSPgrad::NONE;


    // input data
    int      NW = 0;    // number of frequencies
    double * ww = NULL; // frequencies
    double   gg = 0;    // default gamma

    // default thresholds and solver internals
    double Dthresh = 1.0E-12;    // CS Fock threshold
    double Cthresh = 1.0E-5;     // convergence threshold
    double Sthresh = 1.0E-6;     // SVD threshold

    double CDthresh = 0;         // for Cholesky Decomposition

    int NT  = 32;                // default maximum number of cycles
    int MD  =  0;                // maximum subspace dim (MD==0 uses NT x max vecs per cycle)
    int NA  =  0;                // dimension of active subspace (NA <= 0 substracts vectors from the total virtual subspace)
    int MV  =  0;                // maximum number of trial vectors per cycle (MV==0 ignores this)

    std::string savepath = "";


    // setter functions
    int SetGamma        (double g);
    int SetFreqs        (double w0, double wf, int N);
    int SetFreqs        (const double * w, int N);
    int SetCSthresh     (double th);
    int SetConvergence  (double th);
    int SetSVDthresh    (double th);
    int SetMaxRSPcycles (int N);
    int SetMaxSubspace  (int N);
    int SetMaxNumVecs   (int N);
    int SetActive       (int N);
    int SetCDthreshold  (double th);

    int SetSavePath     (const std::string & s);

    // set the mode
    int SetReal () {mode = RSPmode::REAL; return 0;}
    int SetImag () {mode = RSPmode::IMAG; return 0;}
    int SetCPP  () {mode = RSPmode::CPP;  return 0;}

   ~RSPparams();
};

#endif
