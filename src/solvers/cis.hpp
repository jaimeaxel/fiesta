#ifndef _CIS_HPP_
#define _CIS_HPP_

#include <vector>
#include "linear/tensors.hpp"
#include "solvers/SCF.hpp"

class CISsolver
{
  private:
    SCFsolver* scf;  // SCF handle
    Echidna* efs;    // EFS handle
    DFTgrid* dft;    // DFT handle

    tensor2 C;   // MO coef matrix
    tensor2 D;   // density matrix
    tensor2 F;   // Fock matrix
    std::vector<double> eps; // oribtal energies

    int nmo; // number of MOs
    int nao; // number of AOs

    tensor2 Cocc; // MO coef (occupied)
    tensor2 Cvir; // MO coef (virtual)
    int nocc; // number of occupied orbitals
    int nvir; // number of virtual orbitals

    int nstates  = 5;   // number of requested states
    int nguess   = 10;  // number of guess vectors
    int max_dim  = 50;  // maximum dimension of subspace
    int max_iter = 100; // maximum number of iterations

    double CISconv    = 1.0e-6; // CIS convergence threshold
    double CISnormtol = 1.0e-6; // threshold for adding vector
    bool   CISdebug   = false;  // double check CIS result

    std::vector<double>  E_cis;   // excitation energies
    std::vector<tensor2> CI_vecs; // CI vectors

    tensor2 elec_trans_dipole;  // electric transition dipole
    tensor2 velo_trans_dipole;  // velocity transition dipole
    tensor2 magn_trans_dipole;  // magnetic transition dipole

    tensor2 LE_LE; // LE-LE couplings
    tensor2 LE_CT; // LE-CT couplings
    tensor2 CT_CT; // CT-CT couplings

    tensor2 GetSigma(const tensor2& B);

  public:
    CISsolver();
    CISsolver(SCFsolver* SCF, Echidna* EFS, DFTgrid* DFT);
    ~CISsolver();

    void InitCIS(SCFsolver* SCF, Echidna* EFS, DFTgrid* DFT);
    void RunCIS();
    void RunDimerCIS(tensor2* density_pointer, int dimer_ct_nocc, int dimer_ct_nvir);

    void ComputeTransDipoles();
    void ComputeTransDipoles(std::vector<tensor2>& b_vecs);

    void set_nstates(int n);
    void set_conv(double thre);

    const std::vector<double>&  get_E_cis()   const { return E_cis;   }
    const std::vector<tensor2>& get_CI_vecs() const { return CI_vecs; }

    const tensor2& get_elec_trans_dipole() const { return elec_trans_dipole; }
    const tensor2& get_velo_trans_dipole() const { return velo_trans_dipole; }
    const tensor2& get_magn_trans_dipole() const { return magn_trans_dipole; }

    const tensor2& get_LE_LE() const { return LE_LE; }
    const tensor2& get_LE_CT() const { return LE_CT; }
    const tensor2& get_CT_CT() const { return CT_CT; }
};

#endif
