#include <cmath>
#include <cstring>
#include <cassert>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "linear/eigen.hpp"
#include "linear/tensors.hpp"
#include "solvers/SCF.hpp"
#include "solvers/cis.hpp"

using namespace Fiesta;

struct Excitation {
    int occ;
    int vir;
    double delta_e;

    Excitation() : occ(0), vir(0), delta_e(0.0) {}
    Excitation(int i, int a, double e) : occ(i), vir(a), delta_e(e) {}
    ~Excitation() {}
};

bool CompareExcitation(Excitation a, Excitation b) {
    return a.delta_e < b.delta_e;
}

CISsolver::CISsolver()
{
    scf = NULL;
    efs = NULL;
    dft = NULL;
    nmo = nao = 0;
    nocc = nvir = 0;
}

CISsolver::CISsolver(SCFsolver* SCF, Echidna* EFS, DFTgrid* DFT)
{
    InitCIS(SCF, EFS, DFT);
}

CISsolver::~CISsolver() {}

void CISsolver::InitCIS(SCFsolver* SCF, Echidna* EFS, DFTgrid* DFT)
{
    scf = SCF;
    efs = EFS;
    dft = DFT;

    C = scf->GetMOs();
    D = scf->GetDensity();
    F = scf->GetFock();

    nmo = C.n;
    nao = C.m;

    eps.resize(nmo);
    memcpy(eps.data(), scf->GetEnergies(), sizeof(double)*nmo);

    nocc = scf->GetNumOcc();
    nvir = nmo - nocc;

    Cocc.setsize(nocc, nao);
    Cvir.setsize(nvir, nao);
    memcpy(Cocc.c2, C.c2,            sizeof(double)*nocc*nao);
    memcpy(Cvir.c2, C.c2 + nocc*nao, sizeof(double)*nvir*nao);

    nguess  = std::min(nguess,  nocc*nvir);
    nstates = std::min(nstates, nocc*nvir);
}

void CISsolver::set_nstates(int n)
{
    nstates = n;
    nguess = nstates * 4;

    nstates = std::min(nstates, nocc*nvir);
    nguess  = std::min(nguess,  nocc*nvir);

    max_dim = nguess * 5;
}

void CISsolver::set_conv(double thre)
{
    CISconv = thre;
}

tensor2 CISsolver::GetSigma(const tensor2& B)
{
    // T = (Cocc)' * B * Cvir
    tensor2 T;
    tensor2 tmp;
    TensorProduct(tmp, B, Cvir);
    TensorProductNTN(T, Cocc, tmp);

    tensor2* JJ = new tensor2;
    tensor2* KK = new tensor2;
    tensor2* AA = new tensor2;
    JJ->setsize(T.n);
    KK->setsize(T.n);
    AA->setsize(T.n);
    JJ->zeroize();
    KK->zeroize();
    AA->zeroize();

    // Messenger status needs to be handled with care.
    // If it is already disabled at this point, we are likely doing google test.
    // In other words, if msg_active is false, we do not touch the Messenger.
    bool msg_active = Messenger.IsEnabled();

    if (msg_active) { Messenger.Disable(); }

    efs->Contraction(JJ, KK, AA, &T, 1, scf->GetCSthresh(), scf->GetCSthresh());

    if (msg_active) { Messenger.Enable(); }

    JJ[0] *= 2.0;

    double fxc = 1.0;
    if (dft != NULL && dft->IsInit()) {
        fxc = dft->GetHFK();
    }

    if (fxc > 0.0) {
        KK[0] += AA[0];
        JJ[0].FMA(KK[0], -fxc);
    }

    if (dft != NULL && dft->IsInit()) {
        dft->CalcXCrsp(JJ, &T, 1, D);
    }

    // sigma = Cocc * S * (Cvir)'
    tensor2 sigma;
    TensorProductNNT(tmp, JJ[0], Cvir);
    TensorProduct(sigma, Cocc, tmp);

    for (int i = 0; i < nocc; i++) {
        for (int a = 0; a < nvir; a++) {
            sigma[i][a] += B[i][a] * (eps[nocc + a] - eps[i]);
        }
    }

    delete JJ;
    delete KK;
    delete AA;

    return sigma;
}

void CISsolver::RunCIS()
{
    // sort occ->vir excitations
    // Abar is preconditioner
    std::vector<Excitation> excitations;
    tensor2 Abar (nocc, nvir);
    for (int i = 0, ia = 0; i < nocc; i++) {
        for (int a = 0; a < nvir; a++, ia++) {
            double delta_e = eps[nocc + a] - eps[i];
            excitations.push_back(Excitation(i, a, delta_e));
            Abar.c2[ia] = delta_e;
        }
    }
    std::sort(excitations.begin(), excitations.end(), CompareExcitation);

    // initial guess vectors
    std::vector<tensor2> Bs;
    for (int i = 0; i < nguess; i++) {
        tensor2 B (nocc, nvir);
        B.zeroize();
        B[excitations[i].occ][excitations[i].vir] = 1.0;
        Bs.push_back(B);
    }

    tensor2 G (max_dim, max_dim);
    int G_dim = 0;

    std::vector<tensor2> Ss;

    // start iterations
    int iter = 0;
    while (true) {

        Messenger << "CIS iteration: " << iter+1
                  << "    subspace size:" << Bs.size() << std::endl;

        Messenger.Push();

        // form G matrix: G_ij = sigma_i * b_j
        // collect sigma vectors in Ss
        for (int i = G_dim; i < Bs.size(); i++) {
            tensor2 S;
            S = GetSigma(Bs[i]);
            Ss.push_back(S);
            for (int j = 0; j <= i; j++) {
                double Gij = DotProduct(S, Bs[j]);
                G[i][j] = Gij;
                if (j != i) { G[j][i] = Gij; }
            }
        }
        G_dim = Bs.size();

        // form sub G matrix and diagonalize
        tensor2 evec (G_dim, G_dim);
        for (int i = 0; i < G_dim; i++) {
            memcpy(evec.c[i], G.c[i], sizeof(double)*G_dim);
        }
        std::vector<double> eval (G_dim);
        DiagonalizeV(evec, eval.data());

        // get eigenvalues and residuals, and check convergence
        std::vector<double> Es;
        std::vector<tensor2> Rs;
        bool converged = true;
        for (int k = 0; k < nstates; k++) {

            double E = eval[k];
            Es.push_back(E);

            tensor2 R (nocc, nvir);
            R.zeroize();
            for (int ind = 0; ind < Bs.size(); ind++) {
                tensor2& B = Bs[ind];
                tensor2& S = Ss[ind];
                double a_ik = evec[k][ind];
                R.FMA(S, a_ik);
                R.FMA(B, -a_ik*E);
            }
            Rs.push_back(R);

            // check convergence
            double R_norm = sqrt(DotProduct(R,R));
            if (R_norm > CISconv) {
                converged = false;
            }

            Messenger << "Root " << (k+1) << " " << E << "   Residual=" << R_norm
                      << (R_norm <= CISconv ? "   converged" : "") << std::endl;
        }
        Messenger << std::endl;

        Messenger.Pop();

        Messenger << std::endl;

        // save converged results
        if (converged) {
            E_cis.resize(nstates);
            memcpy(E_cis.data(), eval.data(), sizeof(double)*nstates);
            CI_vecs.resize(nstates);
            for (int k = 0; k < nstates; k++) {
                CI_vecs[k].setsize(nocc, nvir);
                CI_vecs[k].zeroize();
                for (int ind = 0; ind < Bs.size(); ind++) {
                    tensor2& B = Bs[ind];
                    double a_ik = evec[k][ind];
                    CI_vecs[k].FMA(B, a_ik);
                }
            }
            break;
        }

        // add new guess vectors
        for (int k = 0; k < Es.size(); k++) {
            double E = Es[k];
            tensor2& R = Rs[k];

            // build correction vectors
            tensor2 D (nocc, nvir);
            for (int elem = 0; elem < nocc*nvir; elem++) {
                D.c2[elem] = R.c2[elem] / (-Abar.c2[elem] + E);
            }
            double D_norm = sqrt(DotProduct(D,D));
            D *= 1.0/D_norm;

            // orthogonalization against the existing guess vectors
            for (int m = 0; m < Bs.size(); m++) {
                tensor2& B = Bs[m];
                double proj = DotProduct(D,B);
                D.FMA(B, -proj);
            }
            D_norm = sqrt(DotProduct(D,D));

            // check norm and add vector
            if (D_norm > CISnormtol) {
                D *= 1.0/D_norm;
                Bs.push_back(D);
            }
        }

        // subspace collapse if necessary
        if (Bs.size() > max_dim) {
            std::vector<tensor2> new_Bs;
            for (int k = 0; k < nguess; k++) {
                tensor2 new_B (nocc, nvir);
                new_B.zeroize();
                for (int ind = 0; ind < G_dim; ind++) {
                    double a_ik = evec[k][ind];
                    new_B.FMA(Bs[ind], a_ik);
                }
                new_Bs.push_back(new_B);
            }
            Bs.resize(0);
            for (int k = 0; k < new_Bs.size(); k++) {
                Bs.push_back(new_Bs[k]);
            }
            Ss.resize(0);
            G_dim = 0;
        }

        iter ++;

        if (iter > max_iter) {
            throw("Error: CIS failed to converge!");
        }
    }

    Messenger << "CIS converged!" << std::endl << std::endl;

    if (CISdebug) {
        Messenger << "Checking eigenvalues and eigenvectors..." << std::endl;
        for (int k = 0; k < nstates; k++) {
            tensor2& ci_vec = CI_vecs[k];
            double ci_norm = DotProduct(ci_vec, ci_vec);

            tensor2 sigma;
            sigma = GetSigma(ci_vec);
            double ci_eval = DotProduct(sigma, ci_vec);

            Messenger << "Root " << k+1 << "  Norm=" << ci_norm
                      << "  Error=" << fabs(ci_eval - E_cis[k]) << std::endl;
        }
        Messenger << std::endl;
    }

    for (int k = 0; k < nstates; k++) {
        Messenger << "CIS Excitation Energy " << k+1 << ":  " << E_cis[k] << std::endl;
    }
    Messenger << std::endl;

    ComputeTransDipoles();
}

void CISsolver::ComputeTransDipoles()
{
    ComputeTransDipoles(CI_vecs);
}

void CISsolver::ComputeTransDipoles(std::vector<tensor2>& b_vecs)
{
    int n = b_vecs.size();

    elec_trans_dipole.setsize(n, 3);
    velo_trans_dipole.setsize(n, 3);
    magn_trans_dipole.setsize(n, 3);

    tensor2 T, prod;

    // dipole operator 1e ints
    const tensor2& DX = scf->GetDX();
    const tensor2& DY = scf->GetDY();
    const tensor2& DZ = scf->GetDZ();

    // nabla operator 1e ints
    const tensor2& PX = scf->GetPX();
    const tensor2& PY = scf->GetPY();
    const tensor2& PZ = scf->GetPZ();

    // angular momentum operator 1e ints
    const tensor2& LX = scf->GetLX();
    const tensor2& LY = scf->GetLY();
    const tensor2& LZ = scf->GetLZ();

    for (int k = 0; k < n; k++) {

        const tensor2& CI_vec = b_vecs[k];

        // OV_MO to AO transformation
        TensorProduct(prod, CI_vec, Cvir);
        TensorProductNTN(T, Cocc, prod);

        elec_trans_dipole[k][0] = sqrt(2.0) * Contract(T, DX);
        elec_trans_dipole[k][1] = sqrt(2.0) * Contract(T, DY);
        elec_trans_dipole[k][2] = sqrt(2.0) * Contract(T, DZ);

        velo_trans_dipole[k][0] = sqrt(2.0) * Contract(T, PX);
        velo_trans_dipole[k][1] = sqrt(2.0) * Contract(T, PY);
        velo_trans_dipole[k][2] = sqrt(2.0) * Contract(T, PZ);

        magn_trans_dipole[k][0] = sqrt(2.0) * Contract(T, LX);
        magn_trans_dipole[k][1] = sqrt(2.0) * Contract(T, LY);
        magn_trans_dipole[k][2] = sqrt(2.0) * Contract(T, LZ);
    }

    for (int k = 0; k < n; k++) {
        Messenger << "Electric Transition Dipole Moment " << k+1 << ": "
                  << elec_trans_dipole[k][0] << " "
                  << elec_trans_dipole[k][1] << " "
                  << elec_trans_dipole[k][2] << std::endl;
    }
    Messenger << std::endl;

    for (int k = 0; k < n; k++) {
        Messenger << "Velocity Transition Dipole Moment " << k+1 << ": "
                  << velo_trans_dipole[k][0] << " "
                  << velo_trans_dipole[k][1] << " "
                  << velo_trans_dipole[k][2] << std::endl;
    }
    Messenger << std::endl;

    for (int k = 0; k < n; k++) {
        Messenger << "Magnetic Transition Dipole Moment " << k+1 << ": "
                  << magn_trans_dipole[k][0] << " "
                  << magn_trans_dipole[k][1] << " "
                  << magn_trans_dipole[k][2] << std::endl;
    }
    Messenger << std::endl;

    if (E_cis.size() > 0 && E_cis.size() == n) {

        for (int k = 0; k < n; k++) {

            double exc_ene = E_cis[k];

            double dip_str = elec_trans_dipole[k][0] * elec_trans_dipole[k][0] +
                             elec_trans_dipole[k][1] * elec_trans_dipole[k][1] +
                             elec_trans_dipole[k][2] * elec_trans_dipole[k][2];

            double osc_str = 2.0 / 3.0 * dip_str * exc_ene;

            double rot_str_len = -(elec_trans_dipole[k][0] * magn_trans_dipole[k][0] +
                                   elec_trans_dipole[k][1] * magn_trans_dipole[k][1] +
                                   elec_trans_dipole[k][2] * magn_trans_dipole[k][2]);

            double rot_str_vel =  (velo_trans_dipole[k][0] * magn_trans_dipole[k][0] +
                                   velo_trans_dipole[k][1] * magn_trans_dipole[k][1] +
                                   velo_trans_dipole[k][2] * magn_trans_dipole[k][2]) / exc_ene;

            Messenger << "Oscillator/Rotational strength " << k+1 << ":"
                      << "   f=   " << osc_str
                      << "   R=   " << rot_str_vel
                      << std::endl;
        }
        Messenger << std::endl;
    }
}

void CISsolver::RunDimerCIS(tensor2* density_pointer, int dimer_ct_nocc, int dimer_ct_nvir)
{
    tensor2& CI_vecs_A = density_pointer[3];
    tensor2& CI_vecs_B = density_pointer[4];

    tensor2& dummy_Cocc_A = density_pointer[5];
    tensor2& dummy_Cocc_B = density_pointer[6];

    tensor2& dummy_indices_AB = density_pointer[7];

    int index_A = dummy_indices_AB.n;
    int index_B = dummy_indices_AB.m;

    int nocc_A = dummy_Cocc_A.n;
    int nvir_A = dummy_Cocc_A.m;
    int nocc_B = dummy_Cocc_B.n;
    int nvir_B = dummy_Cocc_B.m;

    int nocc = nocc_A + nocc_B;
    int nvir = nvir_A + nvir_B;

    int nmo  = C.n;
    int nao  = C.m;

    assert(nmo == nocc + nvir);

    // ==> LE states <==

    //            vir(A)       vir(B)
    //        +------------+------------+
    // occ(A) |     A      |            |
    //        +------------+------------+
    // occ(B) |            |     B      |
    //        +------------+------------+

    int nstates_A = CI_vecs_A.n;
    int nstates_B = CI_vecs_B.n;

    std::vector<tensor2> bvecs_LE_A;
    std::vector<tensor2> bvecs_LE_B;

    std::vector<std::string> name_LE_A;
    std::vector<std::string> name_LE_B;

    // LE(A)
    for (int sA = 0; sA < nstates_A; sA++) {
        // LE CI vector in monomer A
        tensor2 bvec_A (nocc_A, nvir_A);
        memcpy(bvec_A[0], CI_vecs_A[sA], sizeof(double)*nocc_A*nvir_A);

        // LE CI vector in dimer AB
        tensor2 bvec_1 (nocc, nvir);
        bvec_1.zeroize();
        for (int i = 0; i < nocc_A; i++) {
            memcpy(bvec_1[i], bvec_A[i], sizeof(double)*nvir_A);
        }
        bvecs_LE_A.push_back(bvec_1);

        stringstream ss;
        ss << (index_A+1) << "e(" << (sA+1) << ")" << (index_B+1) << "g";
        name_LE_A.push_back(ss.str());
    }

    // LE(B)
    for (int sB = 0; sB < nstates_B; sB++) {
        // LE CI vector in monomer B
        tensor2 bvec_B (nocc_B, nvir_B);
        memcpy(bvec_B[0], CI_vecs_B[sB], sizeof(double)*nocc_B*nvir_B);

        // LE CI vector in dimer AB
        tensor2 bvec_2 (nocc, nvir);
        bvec_2.zeroize();
        for (int i = 0; i < nocc_B; i++) {
            memcpy(bvec_2[i + nocc_A] + nvir_A, bvec_B[i], sizeof(double)*nvir_B);
        }
        bvecs_LE_B.push_back(bvec_2);

        stringstream ss;
        ss << (index_A+1) << "g" << (index_B+1) << "e(" << (sB+1) << ")";
        name_LE_B.push_back(ss.str());
    }

    // ==> CT states <==

    //            vir(A)       vir(B)
    //        +------------+------------+
    // occ(A) |            |     AB     |
    //        +------------+------------+
    // occ(B) |     BA     |            |
    //        +------------+------------+

    std::vector<tensor2> bvecs_CT;
    std::vector<std::string> name_CT;

    // CT(A->B)
    for (int i = 0; i < dimer_ct_nocc; i++) {
    for (int a = 0; a < dimer_ct_nvir; a++) {
        tensor2 bvec_12 (nocc, nvir);
        bvec_12.zeroize();

        // hA->lB
        bvec_12[nocc_A-1 - i][nvir_A + a] = 1.0;
        bvecs_CT.push_back(bvec_12);

        stringstream ss;
        ss << (index_A+1) << "+(H" << i << ")" << (index_B+1) << "-(L" << a << ")";
        name_CT.push_back(ss.str());
    }}

    // CT(B->A)
    for (int i = 0; i < dimer_ct_nocc; i++) {
    for (int a = 0; a < dimer_ct_nvir; a++) {
        tensor2 bvec_21 (nocc, nvir);
        bvec_21.zeroize();

        // hB->lA
        bvec_21[nocc-1 - i][a] = 1.0;
        bvecs_CT.push_back(bvec_21);

        stringstream ss;
        ss << (index_A+1) << "-(L" << a << ")" << (index_B+1) << "+(H" << i << ")";
        name_CT.push_back(ss.str());
    }}

    // CT transition dipoles
    Messenger << "=== Transition Dipoles ===" << std::endl << std::endl;
    ComputeTransDipoles(bvecs_CT);

    // ==> 1e Fock contribution <==

    tensor2 F_MO;
    tensor2 tmp;
    TensorProductNNT(tmp, F, C);
    TensorProduct(F_MO, C, tmp);

    tensor2 F_occ (nocc, nocc);
    tensor2 F_vir (nvir, nvir);

    for (int i = 0; i < nocc; i++) {
        memcpy(F_occ[i], F_MO[i], sizeof(double)*nocc);
    }
    for (int a = 0; a < nvir; a++) {
        memcpy(F_vir[a], F_MO[a + nocc] + nocc, sizeof(double)*nvir);
    }

    // ==> LE-LE and LE-CT couplings <==

    Messenger << "=== Couplings and CT Energies ===" << std::endl << std::endl;

    std::vector<tensor2> sigma_LE_A;
    std::vector<tensor2> sigma_CT;

    // sigma for LE(A) states
    for (int s1 = 0; s1 < bvecs_LE_A.size(); s1++) {
        const tensor2& bvec_1 = bvecs_LE_A[s1];
        // 2e contrib.
        tensor2 sigma_1 = GetSigma(bvec_1);
        // 1e contrib.
        tensor2 ov_occ, ov_vir;
        TensorProduct(ov_occ, F_occ, bvec_1);
        TensorProduct(ov_vir, bvec_1, F_vir);
        sigma_1.FMA(ov_occ, -1.0);
        sigma_1.FMA(ov_vir,  1.0);
        // full sigma
        sigma_LE_A.push_back(sigma_1);
    }

    // sigma for CT states
    for (int ct = 0; ct < bvecs_CT.size(); ct++) {
        const tensor2& bvec_12 = bvecs_CT[ct];
        // 2e contrib.
        tensor2 sigma_12 = GetSigma(bvec_12);
        // 1e contrib.
        tensor2 ov_occ, ov_vir;
        TensorProduct(ov_occ, F_occ, bvec_12);
        TensorProduct(ov_vir, bvec_12, F_vir);
        sigma_12.FMA(ov_occ, -1.0);
        sigma_12.FMA(ov_vir,  1.0);
        // full sigma
        sigma_CT.push_back(sigma_12);
    }

    // LE-LE couplings
    LE_LE.setsize(bvecs_LE_A.size(), bvecs_LE_B.size());
    LE_LE.zeroize();

    for (int sA = 0; sA < bvecs_LE_A.size(); sA++) {
        const tensor2& sigma_1 = sigma_LE_A[sA];

        for (int sB = 0; sB < bvecs_LE_B.size(); sB++) {
            const tensor2& bvec_2 = bvecs_LE_B[sB];
            double coupling = DotProduct(sigma_1, bvec_2);

            Messenger << "LE-LE coupling:  "
                      << name_LE_A[sA] << "  "
                      << name_LE_B[sB] << "  "
                      << coupling << std::endl;

            LE_LE[sA][sB] = coupling;
        }
        Messenger << std::endl;
    }

    // LE-CT couplings
    LE_CT.setsize(bvecs_LE_A.size() + bvecs_LE_B.size(), bvecs_CT.size());
    LE_CT.zeroize();

    for (int sA = 0; sA < bvecs_LE_A.size(); sA++) {
        const tensor2& bvec_1 = bvecs_LE_A[sA];

        for (int ct = 0; ct < bvecs_CT.size(); ct++) {
            const tensor2& sigma_12 = sigma_CT[ct];
            double coupling = DotProduct(bvec_1, sigma_12);

            Messenger << "LE-CT coupling:  "
                      << name_LE_A[sA] << "  "
                      << name_CT[ct]   << "  "
                      << coupling << std::endl;

            LE_CT[sA][ct] = coupling;
        }
        Messenger << std::endl;
    }

    for (int sB = 0; sB < bvecs_LE_B.size(); sB++) {
        const tensor2& bvec_2 = bvecs_LE_B[sB];

        for (int ct = 0; ct < bvecs_CT.size(); ct++) {
            const tensor2& sigma_12 = sigma_CT[ct];
            double coupling = DotProduct(bvec_2, sigma_12);

            Messenger << "LE-CT coupling:  "
                      << name_LE_B[sB] << "  "
                      << name_CT[ct]   << "  "
                      << coupling << std::endl;

            LE_CT[sB + bvecs_LE_A.size()][ct] = coupling;
        }
        Messenger << std::endl;
    }

    // CT-CT couplings
    CT_CT.setsize(bvecs_CT.size(), bvecs_CT.size());
    CT_CT.zeroize();

    for (int ct1 = 0; ct1 < bvecs_CT.size(); ct1++) {
        const tensor2& sigma_12 = sigma_CT[ct1];

        for (int ct2 = ct1+1; ct2 < bvecs_CT.size(); ct2++) {
            const tensor2& bvec_12 = bvecs_CT[ct2];

            double coupling = DotProduct(sigma_12, bvec_12);
            Messenger << "CT-CT coupling:  "
                      << name_CT[ct1] << "  "
                      << name_CT[ct2] << "  "
                      << coupling << std::endl;

            CT_CT[ct1][ct2] = coupling;
            CT_CT[ct2][ct1] = coupling;
        }
    }
    Messenger << std::endl;

    // CT excitation energies
    for (int ct = 0; ct < bvecs_CT.size(); ct++) {
        const tensor2& sigma_12 = sigma_CT[ct];
        const tensor2& bvec_12  = bvecs_CT[ct];

        double energy = DotProduct(sigma_12, bvec_12);
        Messenger << "CT energy:  "
                  << name_CT[ct] << "  "
                  << energy << std::endl;

        CT_CT[ct][ct] = energy;
    }
    Messenger << std::endl;
}
