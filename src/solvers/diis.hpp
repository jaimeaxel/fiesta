#ifndef __DIIS__
#define __DIIS__

#include <vector>

#include "linear/tensors.hpp"

using namespace std;

class DIIS {
  private:
    int min_dim, max_dim;
    int dim, ind;
    vector<tensor2> Fa, Fb, Ga, Gb;
    tensor2 B;
    double* bb;

    void init();

  public:
    DIIS();
    DIIS(int min_vecs, int max_vecs);
    ~DIIS();

    void update (tensor2* FF, tensor2* GG, int N);
};

#endif
