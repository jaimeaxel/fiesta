/*
    SCF.cpp

    self-consistent field solver class

    TODO:
    + ADMA: pick lowest energy density in history as reference for the calculation (only affects DFT)
*/

#include <cmath>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"

#include "low/chrono.hpp"
#include "low/io.hpp"
#include "low/strings.hpp"
#include "low/MPIwrap.hpp"
#include "basis/shellpair.hpp"

#include "linear/sparse.hpp"
#include "linear/tensors.hpp"
#include "linear/eigen.hpp"

#include "orbitals/OAObasis.hpp"
#include "orbitals/DMsolve.hpp"

#include "molecule/edata.hpp"
#include "solvers/SCF.hpp"
#include "solvers/diis.hpp"

using namespace std;
using namespace Fiesta;

SCFsolver::SCFsolver() : nalpha(0), nbeta(0), ncore(0), nval(0), open(eMethod::RHF), NAO(0), NMO(0) {}
SCFsolver::~SCFsolver() {}

SCFsolver::Step::Step() {
    Da = Fa = Ja = Ka = Xa = NULL;
    Db = Fb = Jb = Kb = Xb = NULL;
    E1a = E2a = E1b = E2b = Eel = X2 = 0;
}

void SCFsolver::Step::Clear() {

    delete[] Da, Fa, Ja, Ka, Xa;
    Da = Fa = Ja = Ka = Xa = NULL;
    Db = Fb = Jb = Kb = Xb = NULL;

    E1a = E2a = E1b = E2b = Eel = X2 = 0;
}

SCFsolver::Step::~Step() {
    Clear();
}

/*
    =================================================
    AUXILLIARY FUNCTIONS AND ROUTINES; MOVE ELSEWHERE
    =================================================
*/

double SolveDIIS (double & d, double * cc, const tensor2 & WDIIS, const int N);

double Trace (const tensor2 & Di, const tensor2 & Dj, const tensor2 & S) {
    tensor2 DSi, DSj;
    TensorProduct(DSi, Di, S);
    TensorProduct(DSj, Dj, S);
    DSj.Transpose();
    return Contract (DSi,DSj);
}

double Dist2 (const tensor2 & Di, const tensor2 & Dj, const tensor2 & S) {
    tensor2 AD;
    AD  = Di;
    AD -= Dj;
    return Trace (AD,AD,S);
}

void Solve (const tensor2 & H, double * g) {

    const int N = H.n;

    tensor2 A = H;
    double a[N];
    DiagonalizeV(A,a);

    Messenger << "gradient     : "; for (int n=0; n<N; ++n) Messenger << g[n] << " "; Messenger << endl;
    Messenger << "eigenvalues  : "; for (int n=0; n<N; ++n) Messenger << a[n] << " "; Messenger << endl;

    double s[N];
    for (int n=0; n<N; ++n) {
        s[n] = 0;
        if (fabs(a[n]) < 1e-10) continue;
        for (int m=0; m<N; ++m) s[n] += A(n,m)*g[m];
    }

    Messenger << "gradient''   : "; for (int n=0; n<N; ++n) Messenger << s[n] << " "; Messenger << endl;

    for (int n=0; n<N; ++n) {
        if (fabs(a[n]) < 1e-10) continue;
        s[n] /= a[n];
    }

    Messenger << "step''       : "; for (int n=0; n<N; ++n) Messenger << s[n] << " "; Messenger << endl;

    for (int n=0; n<N; ++n) {
        g[n] = 0;
        for (int m=0; m<N; ++m) g[n] += A(m,n)*s[m];
    }
    Messenger << "step         : "; for (int n=0; n<N; ++n) Messenger << g[n] << " "; Messenger << endl;
}

void Solve (double * x, const tensor2 & H, const double * g) {

    const int N = H.n;
    for (int n=0; n<N; ++n) x[n] = -g[n]; // copy gradient
    Solve(H,x);
}

inline double dot(const double * u, const double * v, int N) {
    double s = 0;
    for (int n=0; n<N; ++n) s+=u[n]*v[n];
    return s;
}

void Project (tensor2 & A, double * v) {

    const int N=A.n;

    // normalize v
    {
        double s = 0;
        for (int n=0; n<N; ++n) s+=v[n]*v[n];
        double is = 1./sqrt(s);
        for (int n=0; n<N; ++n) v[n]*=is;
    }

    double p[N];
    for (int n=0; n<N; ++n) {
        p[n]=0;
        for (int m=0; m<N; ++m) p[n]+=A(n,m)*v[m];
    }

    double a; {
        a = 0;
        for (int n=0; n<N; ++n) a+=v[n]*p[n];
    }

    // projects equation out of matrix
    for (int n=0; n<N; ++n)
        for (int m=0; m<N; ++m)
            A(n,m) += a*v[n]*v[m] - p[n]*v[m] - p[m]*v[n];
}

int norm (double * v, int N) {
    double s = dot(v,v,N);
    if (s<1e-16) return 1;
    double is = 1./sqrt(s);
    for (int n=0; n<N; ++n) v[n]*=is;
    return 0;
}

// for hyperplane equations
int norm (double * v, double & r, int N) {
    double s = dot(v,v,N);
    if (s<1e-16) return 1;
    double is = 1./sqrt(s);
    for (int n=0; n<N; ++n) v[n]*=is;
    r *= is;
    return 0;
}

void Project (double * h, double * v, int N) {
    double a = dot(h,v,N);
    for (int n=0; n<N; ++n) h[n]-=a*v[n];
}

void Project (double * h, double & h0, const double * v, const double & v0, int N) {
    double a = dot(h,v,N);
    for (int n=0; n<N; ++n) h[n]-=a*v[n];
    h0 -= a*v0;
}

/*
    minimize:
        h c + c* H c
    subject to the restrains:
        R c + r >= 0
    and the constrains:
        M c + m = 0

    R & r must define a convex set or convergence is not guaranteed
    the rows of R must be normalized
*/
double SolveRestrained (double * cc,  const double * hh, const tensor2 & HH,
                                      const double * rr, const tensor2 & RR,
                                      const double * mm, const tensor2 & MM
                        ) {

    Messenger.precision(8);

    const int NN = HH.n; // number of variables
    const int NR = RR.n; // number of restraints
    const int NM = MM.n; // number of constrains

    if (NR+NM<NN) {
        Messenger << "error: underdetermined system in SolveConstrained" << endl;
        return 0.;
    }

    const int MAXIT = 5*NN; // no reason

    bool active[NR]; // active set
    for (int r=0; r<NR; ++r) active[r] = false;

    // we need:
    // ========

    // 1. the restrain eqs projected on the constrained subspace
    // =========================================================
    // project restrains on constrained subspace
    tensor2 RRP; RRP = RR; // projected restrains
    double rrp[NR]; for (int r=0; r<NR; ++r) rrp[r] = rr[r];

    for (int r=0; r<NR; ++r) {
        for (int m=0; m<NM; ++m)
            Project (RRP.c[r],rrp[r], MM.c[m],mm[m], NN);

        norm(RRP.c[r],rrp[r],NN); // remember to normalize after projecting things out
    }

    // 2. given K active restrains, the K normals projected on the K-1 complement subspace
    // ===================================================================================

    Messenger.Disable();
    for (int k=0; k<MAXIT; ++k) {

        // find the potentially active set by using all restraints
        // being approximately satisfied (will be pruned later on)
        int NA; {
            NA = 0;
            for (int r=0; r<NR; ++r) {
                double x=rrp[r];
                for (int n=0; n<NN; ++n) x+=RRP(r,n)*cc[n]; // angle between restraint normal and gradient
                active[r] = (fabs(x)<1e-8);
                if (active[r]) ++NA;
            }
        }

        //Messenger << "cc ="; for (int n=0; n<NN; ++n) Messenger << " " << cc[n]; Messenger << endl << endl;

        // compute gradient
        double g[NN]; {
            for (int n=0; n<NN; ++n) g[n]=hh[n];
            for (int n=0; n<NN; ++n)
                for (int m=0; m<NN; ++m)
                      g[n]+= 2*HH(n,m)*cc[m];
        }

        //Messenger << "gg ="; for (int n=0; n<NN; ++n) Messenger << " " << g[n]; Messenger << endl << endl;

        // general direction, projection onto restrained subspace
        double uu[NN];
        for (int n=0; n<NN; ++n) uu[n] = -g[n];
        //for (int p=0; p<NM; ++p) Project (uu, RRP.c[p], NN); // project constrains


        // check wether some of the restrains can be removed
        // from the list due to the gradient pushing away from them
        // this is tricky because when multiple restrains are simultaneously satisfied,
        // the normals can partially push against another restrain
        if (NA>0) {
            //Messenger << "initial NA : " << NA << endl;
            tensor2 SS(NA);
            double up[NA];

            // indices to active set
            int idx[NA]; {
                int p=0;

                for (int r=0; r<NR; ++r) {
                    if (active[r]) {
                        idx[p] = r;
                        ++p;
                    }
                }
            }


            double uur[NN]; {
                tensor2 M(NA+NM, NN);

                for (int m=0; m<NM; ++m)
                    for (int n=0; n<NN; ++n) M   (m,n) = MM(m,n);
                for (int p=0; p<NA; ++p)
                    for (int n=0; n<NN; ++n) M(NM+p,n) = RR(idx[p],n);

                tensor2 U(NA+NM), V(NN);
                double s[NN];

                SVD(M,U,V,s);

                // now project the V components from uu
                for (int n=0; n<NN; ++n) uur[n] = uu[n];
                for (int m=0; m<NA+NM; ++m) Project(uur, V.c[m], NN);
                for (int n=0; n<NN; ++n) uur[n] = uu[n]-uur[n];
            }

            // inner product
            for (int p=0; p<NA; ++p) {
                for (int q=0; q<NA; ++q) {
                    int i = idx[p];
                    int j = idx[q];

                    SS(p,q) = dot(RRP.c[i],RRP.c[j],NN);
                }
            }

            for (int p=0; p<NA; ++p) {
                int i = idx[p];
                up[p] = dot(RRP.c[i],uur,NN);
            }

            Solve(SS,up); // solve linear system

            // take the restrain out of the active set
            for (int n=0; n<NA; ++n)
                if (up[n]>=0) {
                    active[idx[n]] = false;
                    --NA;
                }
        }

        // make a list of the active set
        // =============================
        const int NC = NM+NA;

        tensor2 PP(NC, NN);
        double pp[NC];

        {
            // copy pure restraints
            for (int m=0; m<NM; ++m) {
                for (int n=0; n<NN; ++n) PP(m,n) = MM(m,n);
                pp[m] = mm[m];
            }

            int p=NM;
            // add constraints
            for (int r=0; r<NR; ++r) {
                if (active[r]) {
                    for (int n=0; n<NN; ++n) PP(p,n) = RR(r,n);
                    pp[p] = rr[r];
                    ++p;
                }
            }

            // orthonormalize
            for (int p=0; p<NC; ++p) {
                norm(PP.c[p],pp[p],NN); // normalize the thing

                for (int q=p+1; q<NC; ++q)
                    Project (PP.c[q],pp[q], PP.c[p],pp[p], NN);
            }
        }

        // take this out from the search direction
        for (int p=0; p<NC; ++p) Project (uu , PP.c[p], NN); // direction outprojected constrains, restrains

        // project the restraint set restrains
        tensor2 RRP; RRP = RR; // projected restrains
        double rrp[NR]; for (int r=0; r<NR; ++r) rrp[r] = rr[r];

        for (int r=0; r<NR; ++r) {
            if (active[r]) continue; // already in use

            for (int p=0; p<NC; ++p)
                Project (RRP.c[r],rrp[r], PP.c[p],pp[p], NN);

            int err = norm(RRP.c[r],rrp[r],NN); // remember to normalize after projecting things out
            if (err) {} // constraint s collinear/coplanar; do not check
        }

        // uu is now (minus) the projected gradient;
        // which is also the direction of search

        // normalize
        {
            double u2=0;
            for (int n=0; n<NN; ++n) u2+=uu[n]*uu[n];

            if (u2<1e-12) {
                Messenger << "gradient converged!" << endl;
                break;
            }

            double nn = 1./sqrt(u2);
            for (int n=0; n<NN; ++n) uu[n]*=nn;
        }

        // now do a line search (following the gradient)
        double f,b; {
            f=0; b=0;
            for (int n=0; n<NN; ++n) f+=uu[n]*g[n];
            for (int n=0; n<NN; ++n)
                for (int m=0; m<NN; ++m)
                    b += 2*HH(n,m)*uu[n]*uu[m];
        }

        // step; place where the projected gradient vanishes
        double a;

        if      (     b <0.)           a = double(NN); // for negative curvatures, just follow the direction of the gradient
        else if (fabs(b)<1e-5*fabs(b)) a = double(NN); // if too small curvature (either way), let the restraints handle the case
        else                           a = -0.5 * f/b;

        // first restraint to cross; none at the moment
        int m=-1;

        // now find which (unused) restraint is crossed first
        for (int r=0; r<NR; ++r) {
            if (active[r]) continue;

            double v=0;
            for (int n=0; n<NN; ++n) v+=RRP(r,n)*uu[n]; // angle between restraint normal and gradient

            double x=rrp[r];
            for (int n=0; n<NN; ++n) x+=RRP(r,n)*cc[n]; // angle between restraint normal and gradient

            // x + d * v = 0;
            double d = -x/v;

            if (fabs(v)<1e-5*fabs(x)) continue; // the vector may well be orthogonal to the restraint normal

            // if it is ahead
            if (d>0) {
                // if it crosses before any previous solution
                if (d<a) {
                    a = d; // new minimum
                    m=r; // remember which restraint
                }
            }
            //Messenger << "m = " << m << " a = " << a << endl;
        }

        for (int n=0; n<NN; ++n) cc[n] += a*uu[n]; // move forward

    }
    Messenger.Enable();

    // evaluate function
    double f;
    {
        f=0;
        for (int n=0; n<NN; ++n)
            f+=cc[n]*hh[n];
        for (int n=0; n<NN; ++n)
            for (int m=0; m<NN; ++m)
                f += HH(n,m)*cc[m]*cc[n];
    }

    return f;
}


double SolveRestrained2 (double * cc,  const double * hh, const tensor2 & HH,
                                      const double * rr, const tensor2 & RR,
                                      const double * mm, const tensor2 & MM
                        ) {

    Messenger.precision(8);

    const int NN = HH.n; // number of variables
    const int NR = RR.n; // number of restraints
    const int NM = MM.n; // number of constrains

    if (NR+NM<NN) {
        Messenger << "error: underdetermined system in SolveConstrained" << endl;
        return 0.;
    }

    const int MAXIT = 5*NN; // no reason


    bool active[NR]; // active set
    for (int r=0; r<NR; ++r) active[r] = false;


    // we need:
    // ========

    // 1. the restrain eqs projected on the constrained subspace
    // =========================================================
    // project restrains on constrained subspace


    // 2. given K active restrains, the K normals projected on the K-1 complement subspace
    // ===================================================================================


    for (int k=0; k<MAXIT; ++k) {

        Messenger << "cc ="; for (int n=0; n<NN; ++n) Messenger << " " << cc[n]; Messenger << endl << endl;

        // compute gradient
        double g[NN]; {
            for (int n=0; n<NN; ++n) g[n]=hh[n];
            for (int n=0; n<NN; ++n)
                for (int m=0; m<NN; ++m)
                      g[n]+= 2*HH(n,m)*cc[m];
        }

        Messenger << "gg ="; for (int n=0; n<NN; ++n) Messenger << " " << g[n]; Messenger << endl << endl;

        // general direction, projection onto restrained subspace
        double uu[NN], uur[NN];
        for (int n=0; n<NN; ++n) uu[n] = uur[n] = -g[n];


        // make a list of the active set
        // =============================
        int NC; {
            NC = NM; // number of constraints on the system
            for (int r=0; r<NR; ++r) if (active[r]) ++NC;
        }

        tensor2 PP(NC, NN);
        double pp[NC];

        {
            // copy pure restraints
            for (int m=0; m<NM; ++m) {
                for (int n=0; n<NN; ++n) PP(m,n) = MM(m,n);
                pp[m] = mm[m];
            }

            int p=NM;
            // add constraints
            for (int r=0; r<NR; ++r) {
                if (active[r]) {
                    for (int n=0; n<NN; ++n) PP(p,n) = RR(r,n);
                    pp[p] = rr[r];
                    ++p;
                }
            }

            // orthonormalize
            for (int p=0; p<NC; ++p) {
                norm(PP.c[p],pp[p],NN); // normalize the thing

                for (int q=p+1; q<NC; ++q)
                    Project (PP.c[q],pp[q], PP.c[p],pp[p], NN);
            }
        }

        // take this out from the search direction
        for (int p=0; p<NC; ++p) Project (uu , PP.c[p], NN); // direction outprojected constrains, restrains
        for (int p=0; p<NM; ++p) Project (uur, PP.c[p], NN); // direction, outprojected restrains
        for (int n=0; n<NN; ++n) uur[n] -= uu[n]; // restraint subspace

        Messenger << "u  ="; for (int n=0; n<NN; ++n) Messenger << " " << uu[n]; Messenger << endl << endl;
        Messenger << "up ="; for (int n=0; n<NN; ++n) Messenger << " " << uur[n]; Messenger << endl << endl;

        // project the restraint set restrains
        tensor2 RRP; RRP = RR; // projected restrains
        double rrp[NR]; for (int r=0; r<NR; ++r) rrp[r] = rr[r];

        for (int r=0; r<NR; ++r) {
            if (active[r]) continue; // already in use

            for (int p=0; p<NC; ++p)
                Project (RRP.c[r],rrp[r], PP.c[p],pp[p], NN);

            int err = norm(RRP.c[r],rrp[r],NN); // remember to normalize after projecting things out
            if (err) {} // constraint s collinear/coplanar; do not check
        }

        // uu is now (minus) the projected gradient;
        // which is also the direction of search

        // normalize
        {
            double u2=0;
            for (int n=0; n<NN; ++n) u2+=uu[n]*uu[n];

            if (u2<1e-16) {
                Messenger << "gradient converged!" << endl;
                break;
            }

            double nn = 1./sqrt(u2);
            for (int n=0; n<NN; ++n) uu[n]*=nn;
        }

        Messenger << "u ="; for (int n=0; n<NN; ++n) Messenger << " " << uu[n]; Messenger << endl << endl;

        // now do a line search (following the gradient)
        double f,b; {
            f=0; b=0;
            for (int n=0; n<NN; ++n) f+=uu[n]*g[n];
            for (int n=0; n<NN; ++n)
                for (int m=0; m<NN; ++m)
                    b += 2*HH(n,m)*uu[n]*uu[m];
        }

        // step; place where the projected gradient vanishes
        double a = -0.5 * f/b;
        if (fabs(b)<1e-5) a = double(NN); // if too small curvature (either way), let the restraints handle the case
        if (     b <0.)   a = double(NN); // for negative curvatures, just follow the direction of the gradient

        // first restraint to cross; none at the moment
        int m=-1;
        Messenger << "step     : " << a << endl;

        // now find which (unused) restraint is crossed first
        for (int r=0; r<NR; ++r) {
            if (active[r]) continue;

            double v=0;
            for (int n=0; n<NN; ++n) v+=RRP(r,n)*uu[n]; // angle between restraint normal and gradient

            double x=rrp[r];
            for (int n=0; n<NN; ++n) x+=RRP(r,n)*cc[n]; // angle between restraint normal and gradient

            // x + d * v = 0;
            double d = -x/v;

            if (fabs(v)<1e-5) continue; // the vector may well be orthogonal to the restraint normal

            // if it is ahead
            if (d>0) {
                // if it crosses before any previous solution
                if (d<a) {
                    a = d; // new minimum
                    m=r; // remember which restraint
                }
            }
        }

        for (int n=0; n<NN; ++n) cc[n] += a*uu[n]; // move forward
        for (int r=0; r<NR; ++r) {
            //if (active[r]) continue;

            double x=rrp[r];
            for (int n=0; n<NN; ++n) x+=RRP(r,n)*cc[n]; // angle between restraint normal and gradient

            //Messenger << "r = " << r << " x = " << x << " rr " << rrp[r] << endl;

            active[r] = (fabs(x)<1e-5);
        }

        Messenger << "m = " << m << " a = " << a << endl;

        Messenger << "cc ="; for (int n=0; n<NN; ++n) Messenger << " " << cc[n]; Messenger << endl << endl;

        double ff,ss, tt;
        {
            ff=0;
            for (int n=0; n<NN; ++n)
                ff+=cc[n]*hh[n];
            for (int n=0; n<NN; ++n)
                for (int m=0; m<NN; ++m)
                    ff += HH(n,m)*cc[m]*cc[n];
            ss=0;
            for (int n=0; n<NN; ++n) ss +=cc[n];
            tt=0;
            for (int n=0; n<NN; ++n) tt +=uu[n];
        }

        Messenger << "NC   = " << NC << endl;
        for (int r=0; r<NR; ++r) if (active[r]) Messenger << "  " << r; Messenger << endl;
        Messenger << "fmin = " << ff << "  cc sum = " << ss  << "  uu sum = " << tt << endl;

        char aa; cin >> aa;
    }


    // evaluate function
    double f;
    {
        f=0;
        for (int n=0; n<NN; ++n)
            f+=cc[n]*hh[n];
        for (int n=0; n<NN; ++n)
            for (int m=0; m<NN; ++m)
                f += HH(n,m)*cc[m]*cc[n];
    }

    return f;
}

double SolveSAP        (double * cc,  const double * hh, const tensor2 & HH, double Q) {

    const int N = HH.n;
    const int M = 2*N; // twice the restraints

    // restraints (occupation)
    double  rr[M];
    tensor2 RR(M, N);
    RR.zeroize();

    for (int n=0; n<N; ++n) {
        RR(2*n  , n) =  1; rr[2*n  ] = 0;  //  c >= 0
        RR(2*n+1, n) = -1; rr[2*n+1] = 1;  // -c + 1 >= 0
    }

    // constraints (charge)
    double  mm[1];
    tensor2 MM(1, N);
    MM.zeroize();

    for (int n=0; n<N; ++n) {
        MM(0,n) =  1;
        mm[0]   = -Q;  //  c >= 0
    }


    // (the RRs are already normalized, but this will be helpful for the general case)
    for (int m=0; m<M; ++m) norm (RR.c[m], rr[m], N);

    // now the constrains
    for (int m=0; m<1; ++m) norm (MM.c[m], mm[m], N);

    // wild guess
    for (int n=0; n<N; ++n) cc[n] = Q / double(N); // Q<N, so all restrains are satisfied

    // solve
    return SolveRestrained (cc, hh,HH, rr,RR, mm,MM);
}

double SolveDIIS (double & d, double * cc, const tensor2 & WDIIS, const int N) {

    tensor2 A = WDIIS;
    double a[N];
    DiagonalizeV(A,a);

    double s[N];

    for (int n=0; n<N; ++n) {
        s[n] = 0;
        for (int m=0; m<N; ++m) s[n] += A(m,n);

        // improved stability
        if (s[n]<0) {
            s[n]=-s[n];
            for (int m=0; m<N; ++m) A(m,n)=-A(m,n);
        }
    }

    Messenger << "eigenvalues  : "; for (int n=0; n<N; ++n) Messenger << a[n] << " "; Messenger << endl;
    Messenger << "eigenvector S: "; for (int n=0; n<N; ++n) Messenger << s[n] << " "; Messenger << endl;

    // lagrangian
    //double d;
    {
        d = 0;
        for (int n=0; n<N; ++n) d += s[n]*s[n]*(1./a[n]);
        d = -1./d;
    }

    // rhs solutions
    for (int n=0; n<N; ++n) s[n] = -d * (1./a[n]) * s[n];

    for (int n=0; n<N; ++n) {
        cc[n] = 0;
        for (int m=0; m<N; ++m) cc[n] += A(n,m)*s[m];
    }

    double ss=0; for (int n=0; n<N; ++n) ss+=cc[n];

    return ss; // Lagrangian
}

// XX: rotation generator
// en: orbital energies
// h : maximum permisible norm of XX
double Shiftguess (const tensor2 & RR, const double * en, double h) {

    const int NMO = RR.n;

    double x2=0;

    for (int i=0; i<NMO; ++i) {
        for (int j=0; j<NMO; ++j) {
            double x = RR(i,j) / (en[i]+en[j]);
            x2 += x*x;
        }
    }

    if (x2<h*h) return 0; // no shift needed; perhaps


    double AEmax = en[NMO-1]+en[0]; // might want to check this, along with the rest of the algorithm
    double f = sqrt(x2/(h*h)); // fraction

    // -AEmax*(f-1) = mu


    // estimate
    double mu0 = 0;
    double x20 = x2;

    // high estimate of the system's shift
    double muh = 2* (-AEmax*(f-1)); // twice, since we are about to halve it; notice this is negative
    double x2h = 0;

    double mu;

    // there' s a much better way to do this by
    // interpolating a polynomial in the region (since there are no poles)
    // which arguably has better convergence
    while (1) {
        mu = 0.5*(mu0+muh);

        double x2=0;
        for (int i=0; i<NMO; ++i) {
            for (int j=0; j<NMO; ++j) {
                double x = RR(i,j) / (en[i]+en[j]-mu);
                x2 += x*x;
            }
        }

        //Messenger << mu << " " << x2 << endl;

        if (x2>h*h) {
            mu0 = mu;
            x20 = x2;
        }
        else {
            muh = mu;
            x2h = x2;
        }

        if (fabs(muh-mu0)<1e-6) break;
    }

    return 0.5*(mu0+muh);
}


// proects the O-V part of a matrix in ~MO coordinates
void Project (tensor2 & P, const tensor2 & RO, const tensor2 & RV, const tensor2 & M) {
    tensor2 OM;
    TensorProduct    (OM, RO,  M);
    TensorProductNNT ( P, OM, RV);
}

void UnProject (tensor2 & M, const tensor2 & RO, const tensor2 & RV, const tensor2 & P) {
    tensor2 OP;
    TensorProductNTN (OP, RO,  P);
    TensorProduct    ( M, OP, RV);
    M.AntiSymmetrize();
}

void DmatProduct(tensor2 & D, const tensor2 & A, const tensor2 & S, const tensor2 & B) {
    tensor2 SB;
    TensorProduct(SB,S,B);
    TensorProduct(D, A,SB);
}

// a0 + a1 z + a2 z*z + a3 z*z*z = 0
double SolveCubic(double a0, double a1, double a2, double a3) {

    // scale coefficients
    a0/=a3;
    a1/=a3;
    a2/=a3;

    double Q = (3*a1-a2*a2)/9;
    double R = (9*a2*a1-27*a0-2*a2*a2*a2)/54;

    double D2 = Q*Q*Q + R*R;

    double z1,z2,z3;

    // three solutions in the reals
    if (D2<0) {
        double T = sqrt(R*R - D2); // modulo
        double phi = atan2(sqrt(-D2),R);

        double STr = 2*cbrt(T)*cos(phi/3);
        double STi = 2*cbrt(T)*sin(phi/3);
        z1 = -a2/3 + STr;
        z2 = -a2/3 - STr/2 + sqrt(3)*STi/2;
        z3 = -a2/3 - STr/2 - sqrt(3)*STi/2;
    }
    // only one real solution
    else {
        double S = cbrt(R + sqrt(D2));
        double T = cbrt(R - sqrt(D2));

        return -(a2/3 + S + T);
    }

    // find closest solution
    if ( fabs(z3)<fabs(z1) ) swap(z1,z3);
    if ( fabs(z2)<fabs(z1) ) swap(z1,z2);

    return z1;
}

double EvalQuartic(double & E0, double & E1,double & E2,double & E3,double & E4, double z, const double f[2], const double G[2][2]) {
    double s = sin(z);     // this is  ~x  in the limit
    double c = cos(z);   // this is ~x^2 in the limit
    double d = 1-c;

    // e + s + (c + s^2) + 2*s*c + c^2

    double fs = f[0]; // aprox. x^1
    double fc = f[1]; // aprox. x^2
    double gss = G[0][0]; // aprox. x^2
    double gsc = G[0][1]+G[1][0]; // aprox. x^3
    double gcc = G[1][1]; // aprox. x^4


    E0 = fs*s    + fc*(1-c) + gss * s*s           + gsc * (s-s*c)        + gcc * (-2*c+ c*c+1);
    E1 = fs*c    + fc*s     + gss*2*s*c           + gsc * (c-2*c*c+1)    + gcc *  (2*s-2*s*c);
    E2 = fs*(-s) + fc*c     + gss*2*(2*c*c-1)     + gsc * (-s+4*c*s)     + gcc *  (2*c-4*c*c+2);
    E3 = fs*(-c) + fc*(-s)  + gss*2*(-4*s*c)      + gsc * (-c+8*c*c-4)   + gcc * (-2*s+8*c*s);
    E4 = fs*s    + fc*(-c)  + gss*(-8)*(2*c*c-1)  + gsc * (s-16*c*s)     + gcc * (-2*c+16*c*c-8);

    return E0;
}

double NewtonRaphson(double z,   const double f[2], const double G[2][2]) {

    // Newton-Raphson
    while (1) {

        // get z in (-PI,PI) range
        z += 3*PI;
        z = fmod(z,2*PI);
        z -= PI;

        double E0,E1,E2,E3,E4;
        EvalQuartic(E0,E1,E2,E3,E4, z,f,G);

        double Az;

        if (fabs(E2)>0.1*fabs(E1)) {
            // Newton-Raphson
            double Az1 = -E1/E2;
            Az = Az1;
        }
        else if  (fabs(E4)>1e-12)
        {
            // solve a cubic, which is guaranteed to have a solution in the reals
            double Az3 = SolveCubic(E1,E2,E3/2,E4/6);
            Az = Az3;
        }
        else {
            Messenger << "bad step in Newton-Raphson!" << endl;
            Az = 0.000001;
        }

        z += Az;
        if (fabs(Az)<1e-10) break;
    }

    return z;
}

double MinimizeQuartic(const double f[2], const double G[2][2]) {

    // the function may contain 2 minimums and 2 maximums, so better try and find them all
    const int K = 8;
    double z[K];
    double E[K];

    for (int k=0; k<K; ++k) z[k] = 2*PI*(double(2*k+1)/double(2*K) - 0.5);

    for (int k=0; k<K; ++k) {
        double d,h,t,q;
        z[k] = NewtonRaphson (z[k], f,G);
        EvalQuartic(E[k],d,h,t,q, z[k], f,G);

        // insertion sort
        for (int l=k; l>0; --l) {
            if (E[l]<E[l-1]) {
                swap(E[l],E[l-1]);
                swap(z[l],z[l-1]);
            }
        }
    }

    return z[0];
}

// the laziest possible solution
void MinimizeQuartic (double * phi, const double * fs, const double * fc, const tensor2 & Gss, const tensor2 & Gsc, const tensor2 & Gcs, const tensor2 & Gcc, int K) {

    double f[2], G[2][2];
    double E[K];

    for (int k=0; k<K; ++k) phi[k] = 0; // i guess

    while(1) {

        for (int k=0; k<K; ++k) {
            f[0] = fs[k];
            f[1] = fc[k];
            G[0][0] = Gss(k,k);
            G[0][1] = Gsc(k,k);
            G[1][0] = Gcs(k,k);
            G[1][1] = Gcc(k,k);

            // add the contribution from other rotations
            // ()
            for (int l=0; l<K; ++l) {
                if (l==k) continue;
                double s =   sin(phi[l]);
                double c = 1-cos(phi[l]);

                f[0] += 2*s* Gss(k,l);
                f[0] += 2*c* Gsc(k,l);
                f[1] += 2*s* Gcs(k,l);
                f[1] += 2*c* Gcc(k,l);
            }

            phi[k] = MinimizeQuartic(f,G);

            double d,h,t,q;
            EvalQuartic(E[k],d,h,t,q, phi[k], f,G);
        }

        Messenger << "angles1 :  ";
        for (int k=0; k<K; ++k) Messenger << phi[k] << "   ";
        Messenger << endl;

        double s0[K], c0[K], s1[K], c1[K], s2[K], c2[K];

        // transform
        for (int k=0; k<K; ++k) {
            double sk = sin(phi[k]);
            double ck = cos(phi[k]);

            // position
            s0[k] =   sk;
            c0[k] = 1-ck;

            // first derivative
            s1[k] =   ck;
            c1[k] =   sk;

            // second derivative
            s2[k] =  -sk;
            c2[k] =   ck;
        }


        // evaluate energy
        // ===============
        double E1=0, E2=0;

        for (int k=0; k<K; ++k) {
            E1 += fs[k]*s0[k];
            E1 += fc[k]*c0[k];
        }
        for (int k=0; k<K; ++k) {
            for (int l=0; l<K; ++l) {
                E2 += Gss(k,l) * s0[k]*s0[l];
                E2 += Gsc(k,l) * s0[k]*c0[l];
                E2 += Gcs(k,l) * c0[k]*s0[l];
                E2 += Gcc(k,l) * c0[k]*c0[l];
            }
        }

        Messenger << "Energy = " << E1+E2  << endl;

        // evaluate gradient
        // =================
        double gs[K], gc[K];

        // f + 2*G()
        for (int k=0; k<K; ++k) {
            gs[k] = fs[k];
            for (int l=0; l<K; ++l) gs[k] += 2*Gss(k,l) *s0[l];
            for (int l=0; l<K; ++l) gs[k] += 2*Gsc(k,l) *c0[l];
            gc[k] = fc[k];
            for (int l=0; l<K; ++l) gc[k] += 2*Gcs(k,l) *s0[l];
            for (int l=0; l<K; ++l) gc[k] += 2*Gcc(k,l) *c0[l];
        }

        // contract gradient
        double gg[K];
        for (int k=0; k<K; ++k) gg[k] = s1[k]*gs[k] + c1[k]*gc[k];


        Messenger << "Gradient :  ";
        for (int k=0; k<K; ++k) Messenger << gg[k] << "   ";
        Messenger << endl;

        // check for convergence
        double gg2; {
            gg2=0;
            for (int k=0; k<K; ++k) gg2 += gg[k]*gg[k];
        }
        if (gg2 < 1e-20) break;


        // evaluate hessian
        // ================
        tensor2 HH(K);

        for (int k=0; k<K; ++k)
            for (int l=0; l<K; ++l)
                HH(k,l) = 2*( s1[k]*s1[l]* Gss(k,l) +
                              s1[k]*c1[l]* Gsc(k,l) +
                              c1[k]*s1[l]* Gcs(k,l) +
                              c1[k]*c1[l]* Gcc(k,l)
                             );

        for (int k=0; k<K; ++k) HH(k,k) += s2[k]*gs[k] + c2[k]*gc[k];


        // solve the linear system
        // =======================
        double Aphi[K];
        Solve (Aphi, HH, gg);

        for (int k=0; k<K; ++k) phi[k] -= 0.1*gg[k]; //Aphi[k];

        Messenger << "angles2 :  ";
        for (int k=0; k<K; ++k) Messenger << phi[k] << "   ";
        Messenger << endl;

        //char a; cin >> a;
    }
}

// find the rotation generator X that transforms D0 into D1, i.e. D1 = e^-X D0 e^X
void RotationGenerator (tensor2 & XX, const tensor2 & D0, const tensor2 & DD, const tensor2 & SS) {

    const int NMO = DD.n;
    const int NO  = round(Contract(DD,SS));
    const int NV  = NMO-NO;

    // check other matrix also


    // project Doo, Dvv
    // ================
    Messenger << "project Doo, Dvv, Dov" << endl;

    tensor2 H; // Hessian
    tensor2 G; // Gradient
    {
        // H =   Dvv - Doo =  D0 - D0 D - D D0
        // G = - Dov + Dvo =       D0 D - D D0
        tensor2 D0P;
        DmatProduct    (D0P, D0, SS, DD);

        H  = D0P;
        H.Symmetrize();
        H *= -1;
        H += D0;

        G  = D0P;
        G.AntiSymmetrize();
    }


    // diagonalize Dvv-Doo
    // ===================
    Messenger << "diagonalize Dvv-Doo Hessian" << endl;
    double en[NMO];
    double eo[NO];
    double ev[NV];

    tensor2 RO, RV, R; // occuped and virtual orbital bases
    {
        // we need the inverse of SS
        tensor2 ISS;
        ISS = SS;
        Invert(ISS);

        // project oo
        tensor2 D0P,P0P;
        DmatProduct    ( D0P, D0, SS, DD);
        DmatProduct    ( P0P, DD, SS, D0P);
        R = P0P;
        //R*=-1; // sort the energies closest to the Fermi level first
        {
            tensor2 IS;
            IS = ISS;
            DiagonalizeGV (R, IS, en);
        }

        static const double ETHRESH = 1e-6;

        RO.setsize(NO,NMO);
        {
            int k=0;
            for (int i=0; i<NO; ++i) {
                // find next vector
                while(fabs(en[k])<ETHRESH) ++k;
                for (int j=0; j<NMO; ++j) RO(i,j) = R(k,j);
                eo[i] = en[k];
                ++k;
            }
        }

        //Messenger.precision(8);
        //Messenger << "oo : "; for (int i=0; i<NO; ++i) Messenger << eo[i] << " "; Messenger << endl;
        //Messenger.precision(16);


        tensor2 ID;
        ID = ISS;
        ID -= DD;

        //Messenger << "vv : "<< endl;

        DmatProduct    (D0P, D0, SS, ID);
        DmatProduct    (P0P, ID, SS, D0P);
        R = P0P;

        //Messenger << "vv : "<< endl;

        {
            tensor2 IS;
            IS = ISS;
            DiagonalizeGV (R, IS, en);
        }

        //Messenger << "vv : "<< endl;

        RV.setsize(NV,NMO);
        {
            int k=0;
            for (int i=0; i<NV; ++i) {
                // find next vector
                while(fabs(en[k])<ETHRESH) ++k;
                for (int j=0; j<NMO; ++j) RV(i,j) = R(k,j);
                ev[i] = en[k];
                ++k;
            }
        }

        //Messenger.precision(8);
        //Messenger << "vv : ";for (int i=0; i<NV; ++i) Messenger << ev[i] << " "; Messenger << endl;
        //Messenger.precision(16);

        R = H;
        {
            tensor2 IS;
            IS = ISS;
            DiagonalizeGV (R, IS, en);
        }
    }

    // project the OV rotation generators MO basis
    tensor2 YY;
    {
        Project(YY,RO,RV,G);

        for (int o=0; o<NO; ++o)
            for (int v=0; v<NV; ++v)
                YY(o,v) /= eo[o]+ev[v];
    }

    UnProject(XX,RO,RV,YY);

}

/*
    ======================================
    SCF SOLVER MAIN FUNCTIONS AND ROUTINES
    ======================================
*/

// ===========================================================================
// The Journal of Chemical Physics 137, 054110054110 (2012); 10.1063/1.4740249
// ===========================================================================

void SCFsolver::ARHstep() {

    if (NodeGroup.IsMaster()) {

        const double htrust = 1.0; // maximum radius of the trust region
        const int NO = nalpha;
        const int NV = NMO-NO;


        tensor2 AD[nSCF];
        tensor2 AF[nSCF];
        tensor2 AJ[nSCF];
        tensor2 AK[nSCF];
        tensor2 & DD = AD[nSCF-1];
        tensor2 & FF = AF[nSCF-1];
        tensor2 & JJ = AJ[nSCF-1];
        tensor2 & KK = AK[nSCF-1];

        // transform Fn and Dn to the MO basis
        // ===================================
        //Messenger << "transform Fn and Dn to the MO basis" << endl;
        
        const tensor2 & DD0 = *(hist[nSCF-1].Da);
        DD = DD0;
        
        for (int j=0; j<nSCF-1; ++j) {
            const tensor2 & Dj = *(hist[j].Da);
            AD[j] = Dj;
            AD[j] -= DD;
        }
        
        const tensor2 & FF0 = *(hist[nSCF-1].Fa);
        FF = FF0;
        
        const tensor2 & JJ0 = *(hist[nSCF-1].Ja);
        JJ = JJ0;
        
        const tensor2 & KK0 = *(hist[nSCF-1].Ka);
        KK = KK0;

        for (int j=0; j<nSCF-1; ++j) {
            const tensor2 & Fj = *(hist[j].Fa);
            AF[j] = Fj;
            AF[j] -= FF;
        }
        
        for (int j=0; j<nSCF-1; ++j) {
            const tensor2 & Jj = *(hist[j].Ja);
            AJ[j] = Jj;
            AJ[j] -= JJ;
        }
        
        for (int j=0; j<nSCF-1; ++j) {
            const tensor2 & Kj = *(hist[j].Ka);
            AK[j] = Kj;
            AK[j] -= KK;
        }

        // this must be precomputed
        // ========================
        tensor2 TT (nSCF-1);
        for (int i=0; i<nSCF-1; ++i)
            for (int j=0; j<nSCF-1; ++j)
                TT(i,j) = Contract(AD[i],AD[j]);

        tensor2 TTJ(nSCF-1);
        for (int i=0; i<nSCF-1; ++i)
            for (int j=0; j<nSCF-1; ++j)
                TTJ(i,j) = Contract(AD[i],AJ[j]);

        tensor2 TTK(nSCF-1);
        for (int i=0; i<nSCF-1; ++i)
            for (int j=0; j<nSCF-1; ++j)
                TTK(i,j) = Contract(AD[i],AK[j]);

        // project Foo, Fvv
        // ================
        //Messenger << "project Foo, Fvv" << endl;

        // H =   Fvv - Foo =  F - F D - D F
        // G = - Fov + Fvo =      F D - D F
        tensor2 FD;
        TensorProduct(FD, FF, DD);
        FD.Symmetrize();

        tensor2 FH; // Hessian
        FH  = FF;
        FH -= FD;

        // diagonalize Fvv-Foo
        // ===================
        //Messenger << "diagonalize Fvv-Foo Hessian" << endl;
        double en[NAO];
        double eo[NO];
        double ev[NV];

        tensor2 RO, RV, R; // occuped and virtual orbital bases
        {
            tensor2 FD,DFD;
            // project onto D
            TensorProduct    ( FD, FF, DD);
            TensorProduct    (DFD, DD, FD);

            R = DFD;
            R*=-1; // sort the energies closest to the Fermi level first

            DiagonalizeV (R, en); // FH = C+ e C

            static const double ETHRESH = 1e-6;

            RO.setsize(NO,NMO);
            {
                int k=0;
                for (int i=0; i<NO; ++i) {
                    // find next vector
                    while(fabs(en[k])<ETHRESH) ++k;
                    for (int j=0; j<NMO; ++j) RO(i,j) = R(k,j);
                    eo[i] = en[k];
                    ++k;
                }
            }

            //Messenger.precision(8);
            //Messenger << "oo : "; for (int i=0; i<NO; ++i) Messenger << -eo[i] << " "; Messenger << endl;
            //Messenger.precision(16);

            tensor2 ID;
            ID = DD;
            ID *= -1;
            for (int i=0; i<NMO; ++i) ID(i,i) += 1.;

            TensorProduct    ( FD, FF, ID);
            TensorProduct    (DFD, ID, FD);
            R = DFD;

            //R *=-1.;

            DiagonalizeV (R, en); // FH = C+ e C
            //for (int i=0; i<NMO; ++i) en[i] = -en[i];

            RV.setsize(NV,NMO);
            {
                int k=0;
                for (int i=0; i<NV; ++i) {
                    // find next vector
                    while(fabs(en[k])<ETHRESH) ++k;
                    for (int j=0; j<NMO; ++j) RV(i,j) = R(k,j);
                    ev[i] = en[k];
                    ++k;
                }
            }

            //Messenger.precision(8);
            //Messenger << "vv : ";for (int i=0; i<NV; ++i) Messenger << ev[i] << " "; Messenger << endl;
            //Messenger.precision(16);


            R = FH;
            DiagonalizeV (R, en); // FH = C+ e C
        }


        tensor2 AX[nSCF];
        tensor2 & XX = AX[nSCF-1]; // gradient (rhs)

        tensor2 AY[nSCF];
        tensor2 AZ[nSCF];

        // difference in gradient
        // AF DD - DD AF = (I-D) AF D - D AF (I-D)
        {
            // AF = 2 AJ + AK
            for (int j=0; j<nSCF; ++j) {
                TensorProduct    (AX[j], AF[j], DD);
                AX[j].AntiSymmetrize();
            }

            for (int j=0; j<nSCF; ++j) {
                TensorProduct    (AY[j], AJ[j], DD);
                AY[j].AntiSymmetrize();
            }

            for (int j=0; j<nSCF; ++j) {
                TensorProduct    (AZ[j], AK[j], DD);
                AZ[j].AntiSymmetrize();
            }
        }


        // transform G = -Fov+Fvo to the diagonal basis
        // ===========================================
        //Messenger << "transform gradients to diagonal basis" << endl;
        // includes XX, which is the last element
        for (int j=0; j<nSCF; ++j) {
            tensor2 GR;
            TensorProductNNT (GR, AX[j], R);
            TensorProduct    (AX[j], R, GR);
        }
        for (int j=0; j<nSCF; ++j) {
            tensor2 GR;
            TensorProductNNT (GR, AY[j], R);
            TensorProduct    (AY[j], R, GR);
        }
        for (int j=0; j<nSCF; ++j) {
            tensor2 GR;
            TensorProductNNT (GR, AZ[j], R);
            TensorProduct    (AZ[j], R, GR);
        }

        // we should use the MO basis throughout

        //double shift = 0; // works, but we should find a way of
        double shift =  Shiftguess (XX, en, htrust);
        if (shift!=0) Messenger << "Hessian shift : " << shift << endl;


        // now make Xij = Gij/(Ei+Ej)
        // ==========================

        //Messenger << "solve approximate ARH system" << endl;
        // includes XX, which is the last element
        for (int k=0; k<nSCF; ++k)
            for (int i=0; i<NMO; ++i)
                for (int j=0; j<NMO; ++j) {
                    AX[k](i,j) /= (en[i]+en[j]-shift);
                    AY[k](i,j) /= (en[i]+en[j]-shift);
                    AZ[k](i,j) /= (en[i]+en[j]-shift);
                }


        // transform X back
        // ================
        //Messenger << "transform X" << endl;
        // includes XX, which is the last element
        for (int k=0; k<nSCF; ++k) {
            tensor2 XR;
            TensorProduct    (XR, AX[k], R);
            TensorProductNTN (AX[k], R, XR);
        }
        for (int k=0; k<nSCF; ++k) {
            tensor2 YR;
            TensorProduct    (YR, AY[k], R);
            TensorProductNTN (AY[k], R, YR);
        }
        for (int k=0; k<nSCF; ++k) {
            tensor2 ZR;
            TensorProduct    (ZR, AZ[k], R);
            TensorProductNTN (AZ[k], R, ZR);
        }


        tensor2 X;
        double cc[2*nSCF]; // ARH system solution

        if (nSCF>1) {
            tensor2 DDX(2*nSCF-2);
            double  ddx[2*nSCF-2];

            {
                tensor2 DX;
                TensorProduct    (DX, DD, XX);
                DX.Symmetrize();  // (D* X + X+ D*)

                // tr ( (Dk-D*) [D*,X] )
                for (int k=0; k<nSCF-1; ++k) ddx[k]        = Contract(AJ[k],DX);
                for (int k=0; k<nSCF-1; ++k) ddx[k+nSCF-1] = Contract(AK[k],DX);
            }

            for (int j=0; j<nSCF-1; ++j) {
                tensor2 DAY;
                TensorProduct    (DAY, DD, AY[j]);
                DAY.Symmetrize();  // (D* X + X+ D*)
                tensor2 DAZ;
                TensorProduct    (DAZ, DD, AZ[j]);
                DAZ.Symmetrize();  // (D* X + X+ D*)

                for (int k=0; k<nSCF-1; ++k) {
                    DDX (k       , j       ) = Contract(AJ[k], DAY);
                    DDX (k       , j+nSCF-1) = Contract(AJ[k], DAZ);
                    DDX (k+nSCF-1, j       ) = Contract(AK[k], DAY);
                    DDX (k+nSCF-1, j+nSCF-1) = Contract(AK[k], DAZ);
                }
            }
            //for (int i=0; i<2*nSCF-2; ++i) Messenger << ddx[i] << "  "; Messenger << endl;
            // DDX.Symmetrize(); DDX*=0.5; // whether or not it should be symmetrized depends on the formulation

            // now solve system
            // ================

            tensor2 M(2*nSCF-2);
            M.zeroize();

            // (T - K)^-1
            for (int j=0; j<nSCF-1; ++j) {
                for (int k=0; k<nSCF-1; ++k) {
                    M (j       , k       ) = TTJ(j,k);
                    M (j+nSCF-1, k+nSCF-1) = TTK(j,k);
                }
            }

            M -= DDX; // this is symmetric except perhaps in DFT


            tensor2 IM;
            IM = M;
            Invert(IM);


            for (int i=0; i<2*nSCF-2; ++i) {
                double d=0;
                for (int j=0; j<2*nSCF-2; ++j) d += IM(i,j) * ddx[j];
                cc[i] = d; // p[i] * d; // remember the preconditioning ?
            }

            // {char a; cin >> a;}
            //Messenger << "DARH solution: ";
            //Messenger.precision(8);
            //for (int i=0; i<2*nSCF-2; ++i) Messenger << cc[i] << "  "; Messenger << endl;
            //Messenger.precision(16);

            {
                X = XX;
                for (int j=0; j<nSCF-1; ++j) X.FMA ( AY[j], cc[j       ] );
                for (int j=0; j<nSCF-1; ++j) X.FMA ( AZ[j], cc[j+nSCF-1] );
            }

        }
        // nSCF<2
        else {
            X = XX;
        }


        // check order of contributions
        // ============================
        {
            const int N=4;
            tensor2 DXn[N+1];

            DXn[0] = DD;
            for (int n=0; n<N; ++n) {
                TensorProduct(DXn[n+1],DXn[n],X);
                DXn[n+1].Symmetrize();
                DXn[n+1] *= 1./double(n+1);
            }

            tensor2 H0 = HH;

            for (int n=0; n<=N; ++n) {
                double e1n = Contract(H0, DXn[n]);
                double s1n = Contract(DD, DXn[n]);
                double e2n = Contract(FF, DXn[n]); // - 0.5*e1n;

                //Messenger << n << "-order 1e- energy : " << e1n << "  2e- energy: " << e2n << endl;
            }

            for (int n=0; n<=N; ++n) {

                double e2n =0;

                for (int j=0; j<nSCF-1; ++j) e2n += cc[j] * Contract(AF[j], DXn[n]);

                //Messenger << n << "-order 2e- energy : " << e2n << endl;
            }

        }


        // control the step size (trust region)
        // ====================================
        double x2 = Contract(X,X);
        //Messenger << "|X|^2 = " << x2 << endl;
        if (x2>htrust*htrust) {
            double f = htrust / sqrt(x2);
            X *= f;
            x2 *= f*f;
            //Messenger << "new |X|^2 = " << x2 << endl;
        }

        // compute rotation: R=exp(X)
        // ==========================
        //Messenger << "compute rotation: R=exp(X)" << endl;
        {
            // alternatively, we could use SVD

            const int K = 10;
            const double fK = pow(2.,-K);

            X *= fK; // 2^10
            for (int i=0; i<NMO; ++i) X(i,i) += 1.; // I + f X

            tensor2 X2n;

            for (int k=0; k<K; ++k) {
                TensorProduct(X2n,X,X); // X^2^(n+1) = X^2^n * X^2^n
                X = X2n;
            }
        }

        // apply rotation
        // Dn+1 = R+ Dn R
        // ==============
        //Messenger << "apply rotation" << endl;
        {
            // D' = exp(-X) D exp(+X)
            tensor2 DX;
            TensorProduct    (DX, DD, X);
            TensorProductNTN (DD, X, DX);
        }

        //Messenger << "purify new D matrix (in MO basis)" << endl;

        {
            // check N
            const double MPTHRESH = 1e-13;

            //Messenger << "number of electrons = ";
            for (int it=0; it<5; ++it) {
                double ne = DD.trace();
                //Messenger << "  " << ne;
                if (fabs(ne-double(nalpha))<MPTHRESH) break;
                McWeenyPurification(DD);
            }
            //Messenger << endl;
        }


        // transform back to AO basis
        // ==========================
        //Messenger << "transform back to AO basis" << endl;
        {
            // D = L D' L+
            (*DDa) = DD;
        }


        // purify the new density matrix using McWeeny
        // ===========================================
        //Messenger << "purify new D matrix (in AO basis)" << endl;

        {
            // check N
            const double MPTHRESH = 1e-13;

            //Messenger << "number of electrons = ";
            for (int it=0; it<5; ++it) {
                double ne = Contract(*DDa, SS);
                //Messenger << "  " << ne;
                if (fabs(ne-double(nalpha))<MPTHRESH*double(nalpha)) break;
                McWeenyPurification(*DDa, SS);
            }
            //Messenger << endl;
        }

    }

    Tensor2Broadcast(*DDa);
}

void SCFsolver::ZeroDensity(int N) {
    for (int n=0; n<N; ++n) DDa[n].zeroize();
}

void SCFsolver::ReadDensityFromTensor(tensor2* DensityPointer) {

    const tensor2& DD = DensityPointer[0];
    AO2basis (*DDa, DD, BBB_2);

    // we should allow UHF/ROHF for equal number of alpha/beta electrons
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);

    if (open_shell) {
        const tensor2& DD = DensityPointer[1];
        AO2basis (*DDb, DD, BBB_2);
    }

    // Note: this only happens in exciton dimer calculations.
    // C is in AO basis, we do not transform to OAO basis.
    tensor2& CC = DensityPointer[2];
    TensorProduct(C, CC, BBB_2);
}

void SCFsolver::ReadDensity(string InpDens) {
    int nao = BBB.n;

    ifstream inp_dens (InpDens);
    if (! inp_dens.is_open()) {
        throw Exception ("Error: cannot open file " + InpDens);
    }

    tensor2 DD (nao);
    for (int mu = 0; mu < nao; mu++) {
        string line;
        if (! getline(inp_dens, line).eof()) {
            stringstream ss (line);
            for (int nu = 0; nu < nao; nu++) {
                ss >> DD[mu][nu];
            }
        } else {
            throw Exception ("Error while reading file " + InpDens);
        }
    }
    AO2basis (*DDa, DD, BBB_2);

    // we should allow UHF/ROHF for equal number of alpha/beta electrons
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);

    if (open_shell) {
        DD.zeroize();
        for (int mu = 0; mu < nao; mu++) {
            string line;
            if (! getline(inp_dens, line).eof()) {
                stringstream ss (line);
                for (int nu = 0; nu < nao; nu++) {
                    ss >> DD[mu][nu];
                }
            } else {
                throw Exception ("Error while reading file " + InpDens);
            }
        }
        AO2basis (*DDb, DD, BBB_2);
    }

    inp_dens.close();
}

// assemble a Fock / KS matrix
void SCFsolver::AssembleFock (int N) {
    AssembleFock (CSthresh, N);
}

void SCFsolver::AssembleFock (double CSthresh, int N) {

     /*
         WARNING: the option of computing multiple contractions is only for the SAP solver,
                  so it wont work with DFT; moreover, the "trick" is extremely expensive and will be replaced soon
     */

    // we should allow UHF/ROHF for equal number of alpha/beta electrons
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);

    if (nalpha==nbeta && !open_shell) {

        tensor2 DD[N], JJ[N], KK[N];

        for (int n=0; n<N; ++n) DD[n].setsize(BBB.n);
        for (int n=0; n<N; ++n) JJ[n].setsize(BBB.n);
        for (int n=0; n<N; ++n) KK[n].setsize(BBB.n);

        // basis set change may generate numerical noise in ill-conditioned systems;
        // better stick to ONE version of the density matrix
        if (NodeGroup.IsMaster()) {
            for (int n=0; n<N; ++n) basis2AO (DD[n], DDa[n], BBB);
        }
        for (int n=0; n<N; ++n) Tensor2Broadcast(DD[n]);


        E2j=0, E2xc=0, E2k=0;

        {
            //Messenger << "Computing Coulomb and HF exchange contributions" << endl;

            //Messenger.Push(); {

                if (CDthresh==0.) efs->Contraction (JJ, KK, NULL, DD, N, CSthresh, CSthresh);
                else {
                    efs->ContractionCholesky (JJ, DD, N, CDthresh);
                    efs->Contraction         (NULL, KK, NULL, DD, N, CSthresh, CSthresh);
                }

                // F = H + 2J - K
                for (int n=0; n<N; ++n) JJ[n] *=    2;
                for (int n=0; n<N; ++n) KK[n] *= -fxc;

            //} Messenger.Pop();

            // contributions to the energy
            E2j = Contract(*DD,*JJ);
            E2k = Contract(*DD,*KK);
        }


        if (usexc) {

            //Messenger << "Computing Exchange-correlation contributions" << endl;

            //Messenger.Push(); {
                E2xc = dft->CalcXCscf(*KK, *DD);    // compute DFT contribution to XC & update matrix
            //} Messenger.Pop();
        }

        // only saves a few
        Save("D", DD);
        Save("J", JJ);
        Save("K", KK);


        // (vide supra)
        // basis set change may generate numerical noise in ill-conditioned systems;
        // better stick to ONE version of the density matrix
        if (NodeGroup.IsMaster()) {

            // transform to OAO basis
            for (int n=0; n<N; ++n) AO2basis (JJa[n], JJ[n], BBB);
            for (int n=0; n<N; ++n) AO2basis (KKa[n], KK[n], BBB);
        }
        for (int n=0; n<N; ++n) Tensor2Broadcast(JJa[n]);
        for (int n=0; n<N; ++n) Tensor2Broadcast(KKa[n]);


        // assemble the Fock / KS proper
        for (int n=0; n<N; ++n) {
            FFa[n]  = HH;     // set to core hamiltonian
            FFa[n] += JJa[n]; // +2J
            FFa[n] += KKa[n]; // -K
        }

        // compute the energies for the current cycle
        {
            E1 = 2*Contract ( HH , *DDa);
            E2 = E2j + E2k + E2xc;

            Eel = E1 + E2;
            Et  = Enuc + Eel;
        }
    }

    // open-shell
    else {

        E2j=0, E2xc=0, E2k=0;

        const int N=2;

        tensor2 DD[N], JJ[N], KK[N];
        tensor2 D_tot, J_tot;

        for (int n=0; n<N; ++n) DD[n].setsize(BBB.n);
        for (int n=0; n<N; ++n) JJ[n].setsize(BBB.n);
        for (int n=0; n<N; ++n) KK[n].setsize(BBB.n);

        // (vide supra)
        if (NodeGroup.IsMaster()) {
            for (int n=0; n<N; ++n) basis2AO (DD[n], DDa[n], BBB);
        }
        for (int n=0; n<N; ++n) Tensor2Broadcast(DD[n]);

        //Messenger << "Computing Coulomb and HF exchange contributions" << endl;

        //Messenger.Push(); {

            efs->Contraction(JJ, KK, NULL, DD, N, CSthresh, CSthresh);

            KK[0] *= -fxc;
            KK[1] *= -fxc;

            J_tot =  JJ[0];
            J_tot += JJ[1];

            D_tot =  DD[0];
            D_tot += DD[1];

            E2j = 0.5 * Contract(D_tot, J_tot);
            E2k = 0.5 * (Contract(DD[0], KK[0]) + Contract(DD[1], KK[1]));

        //} Messenger.Pop();

        if (usexc) {

            //Messenger << "Computing Exchange-correlation contributions" << endl;

            //Messenger.Push(); {
                // Note: need open-shell dft implementation for CalcXCscf!
                throw Exception("Error: open-shell DFT not yet implemented!");
                //E2xc += 0.5 * dft->CalcXCscf(KK[0], DD[0]);
                //E2xc += 0.5 * dft->CalcXCscf(KK[1], DD[1]);
            //} Messenger.Pop();
        }

        Save("D", DD);
        Save("J", JJ);
        Save("K", KK);

        // (vide supra)
        if (NodeGroup.IsMaster()) {

            // transform to OAO basis
            for (int n=0; n<N; ++n) AO2basis (JJa[n], JJ[n], BBB);
            for (int n=0; n<N; ++n) AO2basis (KKa[n], KK[n], BBB);
        }
        for (int n=0; n<N; ++n) Tensor2Broadcast(JJa[n]);
        for (int n=0; n<N; ++n) Tensor2Broadcast(KKa[n]);


        // assemble the Fock matrices
        // Fa = H + Ja + Jb - Ka
        // Fb = H + Ja + Jb - Kb

        J_tot =  *JJa;
        J_tot += *JJb;

        *FFa = HH;
        *FFa += J_tot;
        *FFb = *FFa;

        *FFa += *KKa;
        *FFb += *KKb;

        // compute the energies
        // Eel = 0.5 * Tr[Da(H+Fa) + Db(H+Fb)]
        {
            D_tot =  *DDa;
            D_tot += *DDb;

            E1 = Contract(HH, D_tot);
            E2 = E2j + E2k + E2xc;

            Eel = E1 + E2;
            Et  = Enuc + Eel;
        }

        // This is only useful for exciton model dimer calculation
        if (C.m == 0 && C.n == 0) {
            C.setsize(BBB.n, (*FFa).n);
            C.zeroize();
        }

        // add ROHF constraints (for RH):
        if (open==eMethod::ROHF || open==eMethod::ROKS) {

            //                       docc        socc        virt
            //            docc  [ 0.5*(Fa+Fb)     Fb      0.5*(Fa+Fb) ]
            // F_{eff} =  socc  [     Fb      0.5*(Fa+Fb)     Fa      ]
            //            virt  [ 0.5*(Fa+Fb)     Fa      0.5*(Fa+Fb) ]

            // Fock: OAO -> MO
            tensor2 tmp;

            TensorProduct    (tmp, C, *FFa);
            TensorProductNNT (*FFa, tmp, C);

            TensorProduct    (tmp, C, *FFb);
            TensorProductNNT (*FFb, tmp, C);

            // build effective Fock for ROHF/ROKS
            tensor2 F_eff;
            tensor2 & Fa = *FFa;
            tensor2 & Fb = *FFb;

            F_eff =  Fa;
            F_eff += Fb;
            F_eff *= 0.5;

            for (int i = 0; i < nbeta; i++) {
                for (int k = nbeta; k < nalpha; k++) {
                    F_eff[i][k] = Fb[i][k];
                    F_eff[k][i] = Fb[k][i];
                }
            }

            for (int i = nalpha; i < NMO; i++) {
                for (int k = nbeta; k < nalpha; k++) {
                    F_eff[i][k] = Fa[i][k];
                    F_eff[k][i] = Fa[k][i];
                }
            }

            // effective Fock: MO -> OAO
            TensorProductNTN (tmp, C, F_eff);
            TensorProduct    (F_eff, tmp, C);

            // update Fock
            *FFa = F_eff;
            *FFb = F_eff;

        }

    }

}

void SCFsolver::SolveSecular(int N) {

    if (N>1) {

        delete DDa;

        DDa = new tensor2[N];

        //Messenger << "Assembling density matrices" << endl;
        //Messenger.Push(); {

            for (int n=0; n<N; ++n) Tensor2Broadcast(DDa[n]);

        //} Messenger.Pop();

    }
    else {
        // compute the density matrix from Fock / KS
        //Messenger << "Assembling density matrix" << endl;
        //Messenger.Push(); {

            if (NodeGroup.IsMaster()) {

                if (nalpha==nbeta) {

                    // alpha electrons or electron pairs
                    CalcDmat(*DDa, C, ea, *FFa, nalpha, 0);

                }

                // open-shell

                else {

                    CalcDmat(*DDa, C, ea, *FFa, nalpha, 0);
                    if (open==eMethod::UHF || open==eMethod::UKS) {
                        // UHF/UKS: form beta density from beta Fock
                        CalcDmat(*DDb, Cb, eb, *FFb, nbeta, 0);
                    } else {
                        // ROHF/ROKS: form beta density from C
                        tensor2 Cb_occ (nbeta, NAO, C[0]);
                        TensorProductNTN (*DDb, Cb_occ, Cb_occ);
                    }
                }
            }

            Tensor2Broadcast(C);

            Tensor2Broadcast(*DDa);
            NodeGroup.Broadcast (ea, NMO);

            if (nalpha!=nbeta) {
                Tensor2Broadcast(*DDb);
                if (open==eMethod::UHF || open==eMethod::UKS) {
                    Tensor2Broadcast(Cb);
                    NodeGroup.Broadcast (eb, NMO);
                }
            }

        //} Messenger.Pop();

    }
}

static bool abs_compare(double a, double b)
{
    return (std::abs(a) < std::abs(b));
}

void SCFsolver::CalcCommuters() {

    if (nalpha==nbeta) {
        // avoid potential numerical divergence once again
        if (NodeGroup.IsMaster()) {
            CalcCommuter (*XXa, *FFa, *DDa);
        }

        Tensor2Broadcast(*XXa);
        X2 = 2*Contract(*XXa,*XXa);

        int size_a = (*XXa).n * (*XXa).m;
        double* ptr_max = std::max_element((*XXa).c2, (*XXa).c2 + size_a, abs_compare);
        delta_G = fabs(*ptr_max);
    }
    else {
        if (NodeGroup.IsMaster()) {
            CalcCommuter (*XXa, *FFa, *DDa);
            CalcCommuter (*XXb, *FFb, *DDb);
        }

        Tensor2Broadcast(*XXa);
        Tensor2Broadcast(*XXb);
        X2 = Contract(*XXa,*XXa) + Contract(*XXb,*XXb);

        int size_a = (*XXa).n * (*XXa).m;
        int size_b = (*XXb).n * (*XXb).m;
        double* ptr_max_a = std::max_element((*XXa).c2, (*XXa).c2 + size_a, abs_compare);
        double* ptr_max_b = std::max_element((*XXb).c2, (*XXb).c2 + size_b, abs_compare);
        delta_G = std::max(fabs(*ptr_max_a), fabs(*ptr_max_b));
    }
}


// save any given matrix using a basic format
void SCFsolver::Save(const std::string & xtra, tensor2 * T) {

    if (save=="") return;

    if (NodeGroup.IsMaster()) {
        string file = save+"_"+xtra+"_"+std::to_string(nSCF);
        WriteTensor(file+".txt", *T);

        symtensor2 F;
        F = *T;
        WriteTensorBin(file+".bin", F);
    }
}


void SCFsolver::NewStep (int N) {

    // we should allow UHF/ROHF for equal number of alpha/beta electrons
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);

    if (nalpha!=nbeta || open_shell) N*=2;

    {
        DDa = new tensor2[N];
        FFa = new tensor2[N];
        JJa = new tensor2[N];
        KKa = new tensor2[N];
        XXa = new tensor2[N];

        for (int n=0; n<N; ++n){
            DDa[n].setsize(NAO);
            FFa[n].setsize(NAO);
            JJa[n].setsize(NAO);
            KKa[n].setsize(NAO);
            XXa[n].setsize(NAO);
        }

        DDb=FFb=JJb=KKb=XXb=NULL;
    }

    if (nalpha!=nbeta || open_shell) {
        N/=2;
        DDb = DDa+N;
        FFb = FFa+N;
        JJb = JJa+N;
        KKb = KKa+N;
        XXb = XXa+N;
    }

}

// Retrieve FF and XX after NewStep
// Necessary for DIIS iterations
void SCFsolver::LoadStep () {

    if (nSCF == 0) return;

    *FFa = *(hist[nSCF-1].Fa);
    *XXa = *(hist[nSCF-1].Xa);

    if (nalpha!=nbeta) {
        *FFb = *(hist[nSCF-1].Fb);
        *XXb = *(hist[nSCF-1].Xb);
    }

}

void SCFsolver::SaveStep () {

    // save also EDIIS information
    iEDIIS[nEDIIS] = nSCF;
    ++nEDIIS;

    // may or may not need all of them
    // save them all for the moment
    {
        hist[nSCF].Da = DDa;
        hist[nSCF].Fa = FFa;
        hist[nSCF].Ja = JJa;
        hist[nSCF].Ka = KKa;
        hist[nSCF].Xa = XXa;
    }
    if (nalpha!=nbeta) {
        hist[nSCF].Db = DDb;
        hist[nSCF].Fb = FFb;
        hist[nSCF].Jb = JJb; // this must go
        hist[nSCF].Kb = KKb;
        hist[nSCF].Xb = XXb;
    }

    hist[nSCF].E1a = E1;
    hist[nSCF].E1b = 0; //E1b;
    hist[nSCF].E2a = E2;
    hist[nSCF].E2b = 0; //E2b;
    hist[nSCF].Eel = Eel;
    hist[nSCF].X2  = X2;
}


// set Hcore, S, nelec, etc.
void SCFsolver::SetSystem (const symtensor & Sp, const symtensor & Hp, int na, int nb, double enuc, eMethod method) {

    // transform everything to an orthonormal basis on entry or exit;
    // don't get rid of LL yet, as it can be useful in order to 
    // implement eigensystem preconditioning

    {
        Messenger << "Solving orthonormal basis";
        Messenger.Push(); {

            if (NodeGroup.IsMaster())
            {
                tensor2 S;
                S = Sp;

                CalcBaseSqrt(S, BBB); // this could be the preconditioner, but in practice it can be the OAO basis

                TensorProduct(BBB_2, S, BBB); // for transforming back

                AO2basis(SS, S, BBB); // SS == I except for numerical noise

                // comment on the consufsing names:
                //
                // BBB == S^-1/2, dimension nAO x nMO
                // BBB * BBB' == S^-1 in AO
                //
                // BBB_2 == S^1/2, dimension nAO x nMO
                // BBB_2 * BBB_2' == S in AO
                //
                // BBB * BBB_2' == I
                // BBB_2 * BBB' == I
                //
                // AO2basis(SS, S, BBB): SS = BBB' * S * BBB
                // basis2AO(S, SS, BBB): S = BBB_2 * SS * BBB_2'
                //
                // Note that SS is identity matrix. As such there's probably
                // no explicit need to use it in normal SCF.
                //
                // Another thing to mention is that one should not feed an
                // identity matrix to an eigensolver.

                tensor2 & H = S;
                H = Hp;
                AO2basis(HH, H, BBB);
            }

            Tensor2Broadcast(BBB);
            Tensor2Broadcast(BBB_2);
            Tensor2Broadcast(SS);
            Tensor2Broadcast(HH);

            NAO = BBB.n;
            NMO = BBB.m;

        } Messenger.Pop();
        Messenger << endl;
    }


    nalpha = na;
    nbeta  = nb;
    ncore  =  0;
    nval   = nalpha+nbeta;

    open = method; //(method==eMethod::UHF)||(method==eMethod::ROHF)||(method==eMethod::UKS);

    ea = new double[NMO];
    if ( (open==eMethod::UHF) || (open==eMethod::UKS) )
        eb = new double[NMO];
    else
        eb = ea;

    // inner product spaces
    {
        sX2.setsize(MAXM);
        sKD.setsize(MAXM);
        sJD.setsize(MAXM);
        sX2.zeroize();
        sKD.zeroize();
        sJD.zeroize();
    }

    Enuc = enuc;

    init = true;
}

// sets an initial guess for the system
void SCFsolver::SetIguess (const symtensor & Fp) {

    NewStep();

    {
        tensor2 F; F = Fp;
        AO2basis(*FFa, F, BBB);
    }

    // we should allow UHF/ROHF for equal number of alpha/beta electrons
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);

    if (nalpha!=nbeta || open_shell) {
        // NOTE: for the moment DoGuess only gives one Fock matrix Fp
        *FFb = *FFa;
    }

    X2 = -1; // this becomes unset
}



// this will keep
void SCFsolver::SetSolvers (Echidna * efssolver, const DFTgrid * grid) {

    efs = efssolver;

    if (grid->IsInit()) {
        dft   = grid;
        usexc = true;
        fxc   = dft->GetHFK();
    }
    else {
        dft   = NULL;
        usexc = false;
        fxc   = 1;
    }
}

// set convergence, thresholds, number of cycles, etc.
void SCFsolver::SetParams(const string & savefile, double CSth, double SCFth, int maxcycles) {

    save = savefile;

    CSthresh  = CSth;
    SCFthresh = SCFth;

    maxSCFc   = maxcycles;
}

// clears information from previous calls
void SCFsolver::Clear () {

    dft = NULL;
    efs = NULL;

    // default configuration
    // =====================
    DIISthresh    = 1e-8; //0.001;
    DIISmaxratio  = 10;
    DIIScondition = 1e-6;

    maxSCFc = 50;

    MAXDIIS  = maxSCFc+1;
    MAXEDIIS =        12;

    CSthresh  = 1.e-12;  // cauchy-schwarz
    SCFthresh = 5.e-8;   // convergence of [F,D]


    // tensors
    // =======
    SS.clear();
    HH.clear();

    //delete FFa, DDa, FFb, DDb;
    //delete JJa, KKa, XXa;
    //delete JJb, KKb, XXb;

    FFa = DDa = FFb = DDb = NULL;
    JJa = JJb = KKa = KKb = NULL;
    XXa = XXb = NULL;

    delete[] ea; ea=NULL;
    if (open==eMethod::UHF || open==eMethod::UKS) {
        delete[] eb; eb=NULL;
    }

    iEsmear = 0;

    // energies
    E1=0, E2=0, Eel, Et;
    X2=-1; // unset

    // does not save by default
    save = "";

    init = false;
    nSCF = 0; // number of SCF iterations

    // history of previous iterations
    // ==============================

    sX2.clear();
    sKD.clear();
    sJD.clear();

    for (int i=0; i<nEDIIS; ++i) {
        hist[i].Clear();
        iEDIIS[i] = 0;
    }

    nEDIIS = 0;

}


// set convergence, threshold, etc.
void SCFsolver::SetDIIS (double thresh, double maxratio, double condition, int NDIIS, int NEDIIS) {

    DIISthresh    = thresh;
    DIISmaxratio  = maxratio;
    DIIScondition = condition;

    MAXDIIS  = NDIIS;
    MAXEDIIS = NEDIIS;
}


void SCFsolver::SetCD   (double thresh) {
    if (thresh>0.) CDthresh = thresh;
}


void SCFsolver::SetDipoleInts (tensor2& TX, tensor2& TY, tensor2& TZ)
{
    DX = TX;
    DY = TY;
    DZ = TZ;
}

void SCFsolver::SetNablaInts (tensor2& TX, tensor2& TY, tensor2& TZ)
{
    PX = TX;
    PY = TY;
    PZ = TZ;
}

void SCFsolver::SetAngMomInts (tensor2& TX, tensor2& TY, tensor2& TZ)
{
    LX = TX;
    LY = TY;
    LZ = TZ;
}

// self-explanatory
void SCFsolver::RunSCF() {

    // the "initial guess" is stored in *FFa (& b)

    // initialize DIIS
    DIIS* my_diis = NULL;
    my_diis = new DIIS ();

    // test the cholesky basis
    if (CDthresh>0.) efs->InitCholesky(CDthresh);

    Et_prev  = 0.0;

    while(1) {

        Messenger << endl;

        if (nSCF>0) Messenger<<"SCF iteration" << nSCF << endl;
        else        Messenger<<"SCF initial guess" << endl;

        Messenger.Push(); {

            if (nalpha!=nbeta) {
                // DIIS iteration
                if (nSCF>0) { my_diis->update(FFa, XXa, 2); }
                SolveSecular();
            }

            else {
                if ((nSCF>0) && (X2>0. && X2<1.)) {
                    // ARH iteration
                    Messenger << "ARH Step" << endl;
                    ARHstep();
                } else {
                    // DIIS iteration
                    Messenger << "DIIS Step" << endl;
                    if (nSCF>0) { my_diis->update(FFa, XXa, 1); }
                    SolveSecular();
                }
            }

            // build Fock/KS from D
            //Messenger << "Constructing new Fock" << endl;
            AssembleFock  (CSthresh);

            CalcCommuters ();
            SaveStep      ();

            // print energies, convergence, etc.
            //Messenger << "|[F,D]|^2 error   : " << X2 << endl;

          //if (nalpha!=nbeta) {
          //    Messenger << "tr(Da S)          : " << Contract(*DDa, SS) << endl;
          //    Messenger << "tr(Db S)          : " << Contract(*DDb, SS) << endl;
          //} else {
          //    Messenger << "tr(D S)           : " << Contract(*DDa, SS) << endl;
          //}

          //Messenger << "H core    energy  : " << E1 << " hartree" << endl;
          //Messenger << "Coulomb   energy  : " << E2j << " hartree" << endl;
          //Messenger << "HF exch.  energy  : " << E2k << " hartree" << endl;
          //Messenger << "XC        energy  : " << E2xc << " hartree" << endl;
          //Messenger << "SCF e     energy  : " << Eel << " hartree" << endl;

            #ifdef DEBUG
            // This block of code shows how orbital gradient is computed in AO
            // G == (S^-1/2)' (FDS - SDF) S^-1/2
            // TODO: open-shell
            if (nalpha == nbeta) {
                tensor2 D_AO = GetDensity();
                tensor2 F_AO = GetFock();
                tensor2 S_AO;
                TensorProductNNT(S_AO, BBB_2, BBB_2);

                tensor2 prod;
                tensor2 FDS;
                TensorProduct(prod, D_AO, S_AO);
                TensorProduct(FDS, F_AO, prod);

                tensor2 G;
                TensorProduct(prod, FDS, BBB);
                TensorProductNTN(G, BBB, prod);
                G.AntiSymmetrize(); // G = G - G'

                double* ptr_max = std::max_element(G.c2, G.c2 + G.n*G.m, abs_compare);
                double debug_delta_G = fabs(*ptr_max);
                assert(fabs(delta_G - debug_delta_G) < 1.0e-8);
            }
            #endif

            delta_Et = Et - Et_prev;
            Et_prev = Et;

            Messenger << "Total SCF energy  : " << Et  << "   hartree" << endl;
            Messenger << "Delta SCF Energy  : " << delta_Et << endl;
            Messenger << "Orbital Gradient  : " << delta_G << endl;
            Messenger << endl;

        } Messenger.Pop();

        // convergence achieved !
        if ((nSCF>0) && (fabs(delta_Et) < 1.0e-8) && (delta_G < 1.0e-6)) {
            Messenger << endl << "SCF converged!" << endl;
            break;
            // Note: break must be outside Push/Pop pair; otherwise the message plotter will get confused!
            // TODO: figure out why a missing Pop() will cause incomplete output.
        }

        if (nSCF>=maxSCFc) {
            Messenger << "SCF failed to converge after " << nSCF << " cycles" << endl;
            break;
        }

        NewStep();
        ++nSCF;
        LoadStep();
    }

    Messenger << endl;

    // compute the canonical MOs from the converged density;
    // necessary for RSP, MP2 and others
    {
        //Messenger << "Computing canonical orbitals" << endl;
        SolveSecular();
        ++nSCF; // otherwise it overwrittes information if called again
    }

    // print orbital energies
    if (open==eMethod::RHF || open==eMethod::RKS) {
        Messenger.precision(5);
        Messenger << "Orbital energies" << endl;
        for (int j = 0; j < NMO; ++j) {
            if (j % 5 == 0) Messenger << j;
            Messenger << ea[j];
            if (j % 5 == 4) Messenger << endl;
        }
        Messenger << endl << endl;
        Messenger.precision(12);
    }

    else {
        Messenger.precision(5);
        Messenger << "Orbital energies (alpha)" << endl;
        for (int j = 0; j < NMO; ++j) {
            if (j % 5 == 0) Messenger << j;
            Messenger << ea[j];
            if (j % 5 == 4) Messenger << endl;
        }
        Messenger << endl << endl;
        Messenger << "Orbital energies (beta)"  << endl;
        for (int j = 0; j < NMO; ++j) {
            if (j % 5 == 0) Messenger << j;
            Messenger << eb[j];
            if (j % 5 == 4) Messenger << endl;
        }
        Messenger << endl << endl;
        Messenger.precision(12);
    }

    delete my_diis;

}

// SAP initial guess
void SCFsolver::RunSAP(int core, int fill) {
    int nfill;

    ncore = core;
    nval  = nalpha+nbeta-ncore;
    nfill = fill;

    const int K = (nfill-ncore)/2; //nval;
    NewStep (K+1);

    *FFa = HH;

    Messenger << "ncore, nnext : " << ncore << " " << nfill << " : " << K << endl;


    // store density matrix between cycles
    tensor2 DD0(HH);
    DD0.zeroize();


    for (nSCF=0; nSCF<maxSCFc; ++nSCF) {

        Messenger << endl;

        if (nSCF>0) Messenger<<"SAP step number " << nSCF << endl;
        else        Messenger<<"SAP core initial guess" << endl;

        Messenger.Push(); {

            //SolveSecular();         // obtain DM from pseudoFock

            Messenger << "Assembling one-atom density matrices" << endl;

            CalcDmat                  (*DDa, ea, *FFa, ncore/2, 0, C);  // finds core DM
            AssembleCanonicalOrbitals (DDa+1, C, ncore/2, nfill/2);     // assembles each valence D individually

            // build Fock/KS from D
            Messenger << "Constructing new Fock" << endl;
            efs->Contraction(JJa, KKa, NULL, DDa, K+1, CSthresh, CSthresh);

            for (int n=0; n<K+1; ++n) JJa[n] *=  2;
            for (int n=0; n<K+1; ++n) KKa[n] *= -1;

            // core Hamiltonian + one-particle Focks
            FFa[0]  = HH;
            FFa[0] += JJa[0];
            FFa[0] += KKa[0];

            for (int k=0; k<K; ++k) {
                FFa[k+1]  = JJa[k+1];
                //FFa[k+1] += KKa[k+1]; // the solver seems to have issue with negative curvatures
            }

            // add densities (between 0 and 1)
            // solve system with minimum energy
            //CalcEnergies  ();
            Eel = Contract(*DDa, HH) + Contract(*DDa, *FFa);

            double  EE1[K];
            tensor2 EE2(K);

            for (int k=0; k<K; ++k) EE1[k] = 2*Contract(FFa[0],DDa[1+k]); // 2 tr (hc D) + 2 tr (J(D*)D) + 2 tr(J(D) D*)
            for (int k=0; k<K; ++k)
                for (int l=0; l<K; ++l)
                    EE2(k,l) = Contract(FFa[1+l],DDa[1+k]);

            Messenger << "E1 ="; for (int k=0; k<K; ++k) Messenger << " " << EE1[k]; Messenger << endl << endl;
            //Messenger << "E2 =" << EE2 << endl << endl;

            // use the ADIIS solver; we must scale the system
            // it will only solve with a subset of the restraints, but at least its the most important ones
            double nv = 0.5*double(nval);

            double  cc[K]; {
                // solve the system assuming occupancies add up to nval
                // on a second thought, they do need the upper bound

                SolveSAP (cc, EE1, EE2,  nv);

                double s=0; for (int k=0; k<K; ++k) s+=cc[k];
                Messenger << "cc ="; for (int k=0; k<K; ++k) Messenger << " " << cc[k]; Messenger << endl << endl;
                Messenger << "ss =" << s << endl;
            }

            // now assemblo the final DM and Fock
            for (int k=0; k<K; ++k) FFa[0].FMA ( FFa[k+1], cc[k] );
            for (int k=0; k<K; ++k) DDa[0].FMA ( DDa[k+1], cc[k] );

            double DS2 = Dist2 (DDa[0], DD0, SS);

            DD0 = *DDa;


            double AE; {
                AE=0;
                for (int k=0; k<K; ++k) AE += cc[k]*EE1[k];
                for (int k=0; k<K; ++k)
                    for (int l=0; l<K; ++l)
                        AE += cc[k]*cc[l]*EE2(k,l);
            }

            Messenger << "|D-D'|^2         : " << DS2 << endl;
            Messenger << "core     energy  : " << Eel << " hartree" << endl;
            Messenger << "valence  energy  : " << AE  << " hartree" << endl;
            Messenger << "Total SAP energy : " << Eel+AE  << " hartree" << endl;
            Messenger << endl;

            if (DS2 < SCFthresh) break;
            // update inner`product spaces and save matrices
        } Messenger.Pop();
    }

    ++nSCF; // otherwise it overwrittes information if called again
}


// optimize the subspace spanned by the elemental basis set, i.e. find the optimally least useful "atomic orbitals"

// assumes only one type of atom is present

void SCFsolver::RunBasisOptimizer (const rAtom *  atoms, uint32_t natoms) {


    //
    // METHOD 1: find the "system-averaged shell occupation numbers" from tr (D* S) under linear approximation
    //


    //
    // METHOD 2: find the "system-averaged atomic natural orbitals" from tr (D* F*) under linear approximation
    //           (no second or higher order contributions to the perturbation)
    //

    //
    // METHOD 3: find the "system-averaged atomic natural orbitals" from tr (F* S^-1) under linear approximation
    //           and discard the highest average energy contributions
    //

    if (0) {
        // we must diagonalize very block independently (otherwise the transformation is meaningless)

        tensor2 RR(NAO);
        double vv[NAO];

        // build the block-diagonal rotation matrix RR
        // from the atomic blocks of SS
        {

            RR.zeroize();

            int os = 0; // offset


            for (int n=0; n<natoms; ++n) {

                const rAtomPrototype & r = *(atoms[n].rAP); // the prototype

                const int NB = r.basis.N(); // number of basis funcs

                char LL[NB]; // angular momenta


                Messenger << n << " : ";

                for (int b=0; b<NB; ++b) {
                    LL[b] = r.basis[b].l;
                    Messenger << int(LL[b]);
                }
                Messenger << endl;

                // loop for all angular momenta
                for (int l=0; l<=LMAX; ++l) {

                    // counts and indexes the elemental
                    // bases with the given l
                    // ================================
                    int NL=0;
                    int il[NB];

                    int ob=0;

                    for (int b=0; b<NB; ++b) {
                        if (LL[b]==l) {
                            il[NL] = ob;
                            ++NL;
                        }
                        ob += 2*LL[b]+1;
                    }

                    if (NL==0) continue; // no shells of the given l

                    // make a simple atom-block SB
                    // ===========================
                    tensor2 SB (NL);

                    for (int ii=0; ii<NL; ++ii)
                        for (int jj=0; jj<NL; ++jj)
                            SB(ii,jj) = SS ( os + il[ii], os + il[jj] );


                    // diagonalize the block
                    // =====================
                    tensor2 RB; RB = SB; RB*=-1;
                    double ss[NL];

                    DiagonalizeV(RB, ss);

                    // rotate all m's of the shell with the same matrix
                    for (int mm=0; mm<2*l+1; ++mm)
                        for (int ii=0; ii<NL; ++ii)
                            for (int jj=0; jj<NL; ++jj)
                                RR ( os + il[ii] + mm, os + il[jj] + mm ) = RB(ii,jj);

                    // same here
                    for (int mm=0; mm<2*l+1; ++mm)
                        for (int ii=0; ii<NL; ++ii)
                            vv [ os + il[ii] + mm] = -ss [ii];

                    for (int i=0; i<NL; ++i) Messenger << "  " << -ss[i]; Messenger << endl;
                }

                // update offset
                for (int b=0; b<NB; ++b) os += 2*LL[b]+1;
            }

            // this is just asdjaosd
            for (int n=0; n<NAO; ++n) vv[n] = sqrt(vv[n]);

        }

        // build the atomic-orthonormalized DD & SS matrices
        // and compute their product
        tensor2 DS, DF, FI;
        {
            // rotate & print SS
            tensor2 ST, DT, FT, IT;
            {
                tensor2 SC;

                TensorProductNNT (SC,  SS, RR);
                TensorProduct    (ST,  RR, SC);

                for (int n=0; n<NAO; ++n)
                    for (int m=0; m<NAO; ++m)
                        ST(n,m) *= 1./(vv[n] * vv[m]);
            }
            {
                tensor2 FC;

                TensorProductNNT (FC,  *FFa, RR);
                TensorProduct    (FT,  RR, FC);

                for (int n=0; n<NAO; ++n)
                    for (int m=0; m<NAO; ++m)
                        FT(n,m) *= 1./(vv[n] * vv[m]);
            }

            {
                tensor2 DC;

                TensorProductNNT (DC,  *DDa, RR);
                TensorProduct    (DT,  RR, DC);

                for (int n=0; n<NAO; ++n)
                    for (int m=0; m<NAO; ++m)
                        DT(n,m) *= (vv[n] * vv[m]);
            }
            {
                tensor2 SI; SI = SS; Invert(SI); // S^-1

                tensor2 SC;

                TensorProductNNT (SC,  SI, RR);
                TensorProduct    (IT,  RR, SC);

                for (int n=0; n<NAO; ++n)
                    for (int m=0; m<NAO; ++m)
                        IT(n,m) *= (vv[n] * vv[m]);
            }

            TensorProduct    (DS,  DT, ST);
            DS.Symmetrize();

            TensorProduct    (DF,  DT, FT);
            DF.Symmetrize();

            TensorProduct    (FI,  FT, IT);
            FI.Symmetrize();
        }

        Messenger << "trace DS : " << DS.trace() << endl;

        Messenger << "trace FS^-1 : " << FI.trace() << endl;


        // sum the diagonal blocks of DS

        tensor2 MM[LMAX+1]; double * mm[LMAX+1];
        tensor2 EE[LMAX+1]; double * ee[LMAX+1];
        tensor2 FF[LMAX+1]; double * ff[LMAX+1];

        {

            const rAtomPrototype & r = *(atoms[0].rAP); // the prototype
            const int NB = r.basis.N(); // number of basis funcs

            char LL[NB]; // angular momenta

            for (int b=0; b<NB; ++b) LL[b] = r.basis[b].l; // remember L

            int NL[LMAX+1]; // number of basis of L=l
            int il[LMAX+1][NB]; // indices

            for (int l=0; l<=LMAX; ++l) NL[l] = 0; // no funcs


            {
                int ob=0; // basis offset within atom

                for (int b=0; b<NB; ++b) {
                    const int L = LL[b];

                    il[L][NL[L]] = ob;
                    ++NL[L];

                    ob += 2*L+1;
                }
            }



            for (int l=0; l<=LMAX; ++l) {
                MM[l].setsize(NL[l]);
                MM[l].zeroize();

                mm[l] = new double[NL[l]];

                EE[l].setsize(NL[l]);
                EE[l].zeroize();

                ee[l] = new double[NL[l]];

                FF[l].setsize(NL[l]);
                FF[l].zeroize();

                ff[l] = new double[NL[l]];
            }



            int os=0;

            for (int n=0; n<natoms; ++n) {

                for (int l=0; l<=LMAX; ++l) {
                    //const int L = LL[b];

                    if (NL[l]==0) continue; // no shells of the given l

                    // loop over all functions of the
                    for (int ii=0; ii<NL[l]; ++ii)
                        for (int jj=0; jj<NL[l]; ++jj)

                            // sum all 2L+1 diagonal blocks
                            for (int mm=0; mm<2*l+1; ++mm) {
                                MM[l](ii,jj) += DS ( os + il[l][ii] + mm, os + il[l][jj] + mm );
                                EE[l](ii,jj) += DF ( os + il[l][ii] + mm, os + il[l][jj] + mm );
                                FF[l](ii,jj) += FI ( os + il[l][ii] + mm, os + il[l][jj] + mm );
                            }



                }

                for (int b=0; b<NB; ++b) os += 2*LL[b]+1;

            }

        }

        for (int l=0; l<=LMAX; ++l) {

            if (MM[l].n==0) continue;

            Messenger << "Shell block with L = " << l << endl << endl;

            Messenger << "Averaged occupation matrix: " << endl << endl;

            Messenger << MM[l] << endl;

            Messenger << "D1 trace : " << MM[l].trace() << endl << endl;

            DiagonalizeE(MM[l],mm[l]);

            Messenger << "occ evs :  "; for (int i=0; i<MM[l].n; ++i) Messenger << "  " << mm[l][i];

            Messenger << endl << endl << endl;


            Messenger << "Averaged energy perturbation: " << endl << endl;

            Messenger << EE[l] << endl ;

            Messenger << "E1 trace : " << EE[l].trace() << endl << endl;

            DiagonalizeE(EE[l],ee[l]);

            Messenger << "en evs :  "; for (int i=0; i<MM[l].n; ++i) Messenger << "  " << ee[l][i];

            Messenger << endl << endl << endl;


            Messenger << "Averaged fock: " << endl << endl;

            Messenger << FF[l] << endl ;

            Messenger << "F1 trace : " << FF[l].trace() << endl << endl;

            DiagonalizeE(FF[l],ff[l]);

            Messenger << "fock evs :  "; for (int i=0; i<MM[l].n; ++i) Messenger << "  " << ff[l][i];

            Messenger << endl << endl << endl << endl;


            delete[] mm[l];
            delete[] ee[l];
            delete[] ff[l];
        }

    }


    //
    // METHOD 4: find the "system-averaged atomic natural orbitals" from the discarded (virtual) subspace, i.e.
    //           the orbitals whose contribution to response is physically meaningless or at the very least, less important
    //
    //           this is mathematically equivalent to finding the system-averaged natural orbitals of the O+A subspace and
    //           selecting the LC close to 0 for discard
    //
    //           an intuitive justification for this is that plasmons in larger clusters will mostly involve excitations WITHIN
    //           the conduction band and only involve other bands as a second order effect;

    {

        const int no = nalpha+nbeta; // occupied
        const int na = 3*no; // active (for response)

        const int nv = NAO - no - na; // virtual (to be deleted)

        Messenger << "spaces; occupied active virtual : " << no << " " << na << " " << nv << endl;

        // find the density matrix of the occupied + active subspaces   //discarded subspace
        tensor2 DV(NAO); {
            tensor2 FV; FV = *FFa; //FV *= -1;

            tensor2 CV(NAO);

            double ev[NAO];

            CalcDmat(DV, ev, FV, no+na, 0, CV);

            Messenger << "virtual fock evs :  "; for (int i=0; i<NAO; ++i) Messenger << "  " << ev[i]; Messenger << endl << endl;
        }

        const rAtomPrototype & r = *(atoms[0].rAP); // the prototype
        const int NB = r.basis.N(); // number of basis funcs

        char LL[NB]; // angular momenta

        int NL[LMAX+1];     // number of basis of L=l
        int il[LMAX+1][NB]; // indices

        {
            for (int b=0; b<NB; ++b) LL[b] = r.basis[b].l; // remember L
            for (int l=0; l<=LMAX; ++l) NL[l] = 0; // no funcs

            // counts and indexes the elemental
            // bases with the given l
            int ob=0; // basis offset within atom

            for (int b=0; b<NB; ++b) {
                const int L = LL[b];

                il[L][NL[L]] = ob;
                ++NL[L];

                ob += 2*L+1;
            }
        }

        Messenger << " shell angular momenta : "; for (int b=0; b<NB; ++b) Messenger << int(LL[b]); Messenger << endl;


        // diagonalize ONE shell block per angular momentum
        tensor2 BB[LMAX+1]; tensor2 SSS[LMAX+1]; double * bb[LMAX+1];

        {
            // test every possible angular momentum
            for (int l=0; l<=LMAX; ++l) {

                if (NL[l]==0) continue; // no shells of the given l

                // make a simple atom-block SB
                // ===========================
                SSS[l].setsize(NL[l]);

                for (int ii=0; ii<NL[l]; ++ii)
                    for (int jj=0; jj<NL[l]; ++jj)
                        SSS[l](ii,jj) = SS ( 0 + il[l][ii], 0 + il[l][jj] ); // os + il ... , os + il ...


                // diagonalize the block
                // =====================
                BB[l] = SSS[l]; BB[l] *= -1;
                bb[l] = new double[NL[l]];

                DiagonalizeV(BB[l], bb[l]);

                for (int ii=0; ii<NL[l]; ++ii)
                    bb[l] [ii] = sqrt(-bb[l][ii]); // -ss [ii];
            }

        }

        // we must diagonalize very block independently (otherwise the transformation is meaningless)

        tensor2 RR(NAO);
        double vv[NAO];

        // build the block-diagonal rotation matrix RR
        // from the atomic blocks of SS
        {

            RR.zeroize();

            int os = 0; // offset


            for (int n=0; n<natoms; ++n) {

                // loop for all angular momenta
                for (int l=0; l<=LMAX; ++l) {

                    if (NL[l]==0) continue; // no shells of the given l

                    // rotate all m's of the shell with the same matrix
                    for (int mm=0; mm<2*l+1; ++mm)
                        for (int ii=0; ii<NL[l]; ++ii)
                            for (int jj=0; jj<NL[l]; ++jj)
                                RR ( os + il[l][ii] + mm, os + il[l][jj] + mm ) = BB[l](ii,jj); // RB(ii,jj);

                    // same here
                    for (int mm=0; mm<2*l+1; ++mm)
                        for (int ii=0; ii<NL[l]; ++ii)
                            vv [ os + il[l][ii] + mm] = bb[l][ii];
                }

                // update offset
                for (int b=0; b<NB; ++b) os += 2*LL[b]+1;
            }
        }

        // ==================================================
        // build the atomic-orthonormalized DD & SS matrices,
        // compute their product and diagonalize the system average
        // =========================================================
        tensor2 DS;
        {
            // rotate & print SS
            tensor2 ST, DT;
            {
                tensor2 SC;

                TensorProductNNT (SC,  SS, RR);
                TensorProduct    (ST,  RR, SC);

                for (int n=0; n<NAO; ++n)
                    for (int m=0; m<NAO; ++m)
                        ST(n,m) *= 1./(vv[n] * vv[m]);
            }
            {
                tensor2 DC;

                TensorProductNNT (DC,  DV, RR);
                TensorProduct    (DT,  RR, DC);

                for (int n=0; n<NAO; ++n)
                    for (int m=0; m<NAO; ++m)
                        DT(n,m) *= (vv[n] * vv[m]);
            }

            TensorProduct    (DS,  DT, ST);
            DS.Symmetrize();
            DS *= 0.5;
        }

        Messenger << "trace DS : " << DS.trace() << endl;

        // sum the diagonal blocks of DS
        tensor2 NN[LMAX+1]; double * nn[LMAX+1];

        {
            for (int l=0; l<=LMAX; ++l) {
                NN[l].setsize(NL[l]);
                NN[l].zeroize();

                nn[l] = new double[NL[l]];
            }


            int os=0;

            for (int n=0; n<natoms; ++n) {

                for (int l=0; l<=LMAX; ++l) {
                    if (NL[l]==0) continue; // no shells of the given l

                    // loop over all functions of the
                    for (int ii=0; ii<NL[l]; ++ii)
                        for (int jj=0; jj<NL[l]; ++jj)

                            // sum all 2L+1 diagonal blocks
                            for (int mm=0; mm<2*l+1; ++mm)
                                NN[l](ii,jj) += DS ( os + il[l][ii] + mm, os + il[l][jj] + mm );
                }

                for (int b=0; b<NB; ++b) os += 2*LL[b]+1;
            }

        }

        // print
        for (int l=0; l<=LMAX; ++l) {
            if (NL[l]==0) continue;
            Messenger << "Shell block with L = " << l << endl << endl;
            Messenger << "Averaged occupation matrix: " << endl << endl;
            Messenger << NN[l] << endl;
            Messenger << "N1 trace : " << NN[l].trace() << endl << endl;
            DiagonalizeV (NN[l],nn[l]);

            const int N = NL[l];

            for (int n=0; n<N; ++n) {
                int o=n;
                for (int m=n+1; m<N; ++m) if (fabs(nn[l][m]) > fabs(nn[l][o])) o = m;
                if (o==n) continue; // don't bother to swap identical things
                swap ( nn[l][n], nn[l][o] ); // eigenvalues
                for (int m=0; m<N; ++m) swap(NN[l](n,m),NN[l](o,m)); // perhaps indices are reversed
            }

            Messenger << "sorted occupations :  "; for (int i=0; i<NL[l]; ++i) Messenger << "  " << nn[l][i];
            Messenger << endl << endl << endl;
        }

        // ====================================
        // now generate the ANO subspace proper
        // ====================================

        tensor2 ANO[LMAX+1];

        for (int l=0; l<=LMAX; ++l) {
            if (NL[l]==0) continue;

            {
                tensor2 DR;
                DR = BB[l];

                for (int n=0; n<NL[l]; ++n)
                    for (int m=0; m<NL[l]; ++m)
                        DR(n,m) *= (1./bb[l][n]);

                TensorProduct(ANO[l], NN[l], DR);
            }

            // new dimension of the reduced subspace
            int M = 0;

            const double THRESH = 0.001;

            for (int i=0; i<NL[l]; ++i)
                if (fabs(nn[l][i])>THRESH) ++M;


            Messenger << "Shell block with L = " << l << endl << endl;

            Messenger << "admitted  occupations :  "; for (int i=0; i<M; ++i) Messenger << "  " << nn[l][i]; Messenger << endl;
            Messenger << "discarded occupations :  "; for (int i=M; i<NL[l]; ++i) Messenger << "  " << nn[l][i]; Messenger << endl;

            Messenger << "Orthonormalized orbitals: " << endl << endl;

            tensor2 TT; TT = ANO[l]; TT.Transpose(); TT.m = M;
            Messenger << TT << endl << endl;

            // discard the other ones by setting the number of rows accordingly
            ANO[l].n = M;
        }

        // (total) number of projected orbitals
        int NPO; {
            NPO = 0;

            for (int l=0; l<=LMAX; ++l) {
                if (NL[l]==0) continue; // no shells of the given l
                NPO += ANO[l].n * (2*l+1);
            }

            NPO *= natoms; // all atoms are the same
        }

        // diagonal block projectors into the ANO subspace
        tensor2 LP(NAO, NPO); {

            LP.zeroize();

            int os  = 0; // offset
            int osc = 0; // offset for contracted shells

            for (int n=0; n<natoms; ++n) {

                for (int l=0; l<=LMAX; ++l) {
                    if (NL[l]==0) continue; // no shells of the given l

                    // loop over (valid) contracted shells
                    for (int jj=0; jj<ANO[l].n; ++jj) {
                        // loop over AO shells
                        for (int ii=0; ii<ANO[l].m; ++ii)
                            // populate all 2L+1 diagonal entries
                            for (int mm=0; mm<2*l+1; ++mm)
                                LP ( os + il[l][ii] + mm, osc + mm ) = ANO[l](jj,ii);

                        osc += 2*l+1;
                    }
                }

                for (int b=0; b<NB; ++b) os += 2*LL[b]+1;
            }
        }

        Messenger << "Projector matrix ready " << endl << endl;



        // ======================================
        // re-optimize SCF in the projected basis
        // desperately needs convergence acceleration
        // ======================================

        // projected overlap
        tensor2 SP; {
            tensor2 SL;

            TensorProduct    (SL, SS, LP);
            TensorProductNTN (SP, LP, SL);
        }
        // projected fock / KS
        tensor2 FP; {
            tensor2 FL;

            TensorProduct    (FL, *FFa, LP);
            TensorProductNTN (FP,   LP, FL);
        }
        tensor2 ISP; {
            ISP = SP;
            Invert(ISP);
        }
        tensor2 DP;


        for (int it=0; it<10; ++it) {

            Messenger << "Solving secular equation" << endl << endl;
            double ee[NPO];
            tensor2 F2; F2 = FP;
            tensor2 S2; S2 = SP;
            DiagonalizeGV (F2, S2, ee);

            Messenger << "Assembling density matrix" << endl << endl;
            tensor2 & CP = F2;
            //CP.m = nalpha;
            //TensorProductNNT (DP, CP, CP);
            CP.n = nalpha;
            TensorProductNTN (DP, CP, CP);

            Messenger << "Transforming density matrix to the original basis" << endl << endl;
            tensor2 & DL = F2;

            TensorProductNNT (  DL, DP, LP);
            TensorProduct    (*DDa, LP, DL);


            // build Fock/KS from D
            Messenger << "Constructing new Fock" << endl;
            AssembleFock  (CSthresh);

            // transform Fock back
            {
                tensor2 FL;

                TensorProduct    (FL, *FFa, LP);
                TensorProductNTN (FP,   LP, FL);
            }

            // we either use the ANO basis or we'll have to
            // project from FFa
            {
                //CalcCommuters ();

                // tr ( (L+ F D S L - L+ S D F L ) * (L+ F D S L - L+ S D F L ) ) =
                // 2 tr (F D S L L+ F D S L L+) - 2 tr (F D S L L+ S D F L L+) =
                // 2 tr (F D F D) - 2 tr (F D S D F S^-1) = 2 tr (F D F D - F D F S^-1) =
                // -2 tr (F D F (S^-1 - D) )

                tensor2 FD, FI, FF;
                TensorProduct (FD, FP, DP);
                TensorProduct (FI, FP, ISP);
                FI -= FD;
                TensorProduct (FF, FD, FI);

                X2 = FF.trace();
                X2 *= 2; // from the formula
                X2 *= 2; // because of RHF / RKS
            }

            Messenger << "|[F,D]|^2 error   : " << X2 << endl;
            Messenger << "tr(D S)           : " << Contract(DP,SP) << endl;
            Messenger << "SCF e    energy   : " << Eel << " hartree" << endl;
            Messenger << "Total SCF energy  : " << Et  << " hartree" << endl;
            Messenger << endl;

            // convergence achieved !
            if ((nSCF>0) && (X2>=0) && (X2 < SCFthresh)) {
                Messenger << "SCF procedure converged within specfied precision after " << it << " cycles" << endl;
                break;
            }
        }

        // =================================================================
        // now use F to rotate the basis so that it roughly matches energies
        // =================================================================

        tensor2 EE; {
            TensorProduct(EE, FP, DP);
            EE.Symmetrize();
            EE *= 0.5;
        }

        tensor2 SAANO[LMAX+1]; // energy-averaged shells

        for (int l=0; l<=LMAX; ++l) {
            if (NL[l]==0) continue; // no shells of the given l
            SAANO[l].setsize(ANO[l].n);
            SAANO[l].zeroize();
        }

        {
            int os  = 0; // offset

            for (int n=0; n<natoms; ++n) {

                for (int l=0; l<=LMAX; ++l) {
                    if (NL[l]==0) continue; // no shells of the given l

                    // loop over (valid) contracted shells
                    for (int ii=0; ii<ANO[l].n; ++ii) {
                        for (int jj=0; jj<ANO[l].n; ++jj) {
                            // populate all 2L+1 diagonal entries
                            for (int mm=0; mm<2*l+1; ++mm)
                                SAANO[l](ii,jj) += EE ( os + (2*l+1)*ii + mm, os + (2*l+1)*jj + mm );
                        }

                    }

                    os += (2*l+1)*ANO[l].n;
                }
            }
        }

        Messenger << "trace EE : " << EE.trace() << endl;

        // print
        for (int l=0; l<=LMAX; ++l) {
            if (NL[l]==0) continue;
            double ff[NL[l]];
            Messenger << "Shell block with L = " << l << endl << endl;
            Messenger << "Averaged fock matrix: " << endl << endl;
            Messenger << SAANO[l] << endl;
            DiagonalizeV (SAANO[l],ff);
            Messenger << "F block evs :  "; for (int i=0; i<NL[l]; ++i) Messenger << "  " << ff[i]; Messenger << endl << endl;
            Messenger << "Averaged orbitals: " << endl << endl;
            Messenger << SAANO[l] << endl;
            Messenger << endl << endl << endl;
        }

        // now use the rotations on the previous ANO basis

        // ===========
        // free arrays
        // ===========
        for (int l=0; l<=LMAX; ++l) delete[] nn[l];
    }
}

// ===============
//      getters
// ===============

tensor2 SCFsolver::GetDensity  (bool beta) const {
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);
    beta &= open_shell;
    tensor2 & DD = (beta?*DDb:*DDa);
    tensor2 D;
    basis2AO (D, DD, BBB);
    return D;
}

tensor2 SCFsolver::GetMOs      (bool beta) const  {
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);
    beta &= open_shell;
    const tensor2 & C_OAO = (beta?Cb:C);
    tensor2 C_AO;
    TensorProductNNT (C_AO,C_OAO,BBB);
    return C_AO;
}

tensor2 SCFsolver::GetCoulomb  (bool beta) const {
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);
    beta &= open_shell;
    tensor2 & JJ = (beta?*JJb:*JJa);
    tensor2 J;
    basis2AO (J, JJ, BBB_2);
    J *= 0.5;
    return J;
}

tensor2 SCFsolver::GetExchange (bool beta) const {
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);
    beta &= open_shell;
    tensor2 & KK = (beta?*KKb:*KKa);
    tensor2 K;
    basis2AO (K, KK, BBB_2);
    K *= -1.0;
    return K;
}

tensor2 SCFsolver::GetFock     (bool beta) const {
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);
    beta &= open_shell;
    tensor2 & FF = (beta?*FFb:*FFa);
    tensor2 F;
    basis2AO (F, FF, BBB_2);
    return F;
}

tensor2 SCFsolver::GetFockMO   (bool beta) const {
    // C and F in AO basis
    tensor2 C = GetMOs(beta);
    tensor2 F = GetFock(beta);
    // AO to MO transformation
    tensor2 tmp;
    TensorProductNNT(tmp, F, C);
    TensorProduct(F, C, tmp);
    // F in MO
    return F;
}

const double  * SCFsolver::GetEnergies  (bool beta) const {
    bool open_shell = (open==eMethod::UHF  || open==eMethod::UKS ||
                       open==eMethod::ROHF || open==eMethod::ROKS);
    beta &= open_shell;
    if (!beta ) return ea;
    else        return eb;
}

// retrieve converged? energy
double SCFsolver::GetSCFenergy()   const { return Eel; }
double SCFsolver::GetTotalEnergy() const { return Eel + Enuc; }

const int SCFsolver::GetNumOcc(bool beta) const { return beta ? nbeta : nalpha; }
const double     SCFsolver::GetCSthresh() const { return CSthresh; }
