/*
    guess.cpp

    initialize the density or fock / KS matrices to serve as initial guess for SCF
*/

#include <cassert>

#include "molecule/edata.hpp"
#include "linear/eigen.hpp"


#include "fiesta/fiesta.hpp"
using namespace Fiesta;


#include "low/io.hpp"

/*

    these two functions should go in atoms, along with the periodic table in calculate.cpp, etc.

*/

// returns the number of core electrons for a given Z
int Zcore(int Z) {
    const int magic[] = {0,2,10,18,36,54,86,118};

    for (int n=6; n>=0; --n)
        if (Z >= magic[n])
            return magic[n];
}

// returns the number of electrons of the next filled shell for a given Z
int Znext(int Z) {
    const int magic[] = {0,2,10,18,36,54,86,118};

    for (int n=0; n<6; ++n)
        if (Z <= magic[n+1])
            return magic[n+1];
}


std::vector< std::vector<double> > BuildQocc()
{
    std::vector< std::vector<double> > Qocc;

    // dummy atom
    Qocc.push_back(std::vector<double> ());

    // H,He
    Qocc.push_back(std::vector<double> ({ 0.5 }));
    Qocc.push_back(std::vector<double> ({ 1.0 }));

    // Li,Be,B,C,N,O,F,Ne
    Qocc.push_back(std::vector<double> ({ 1.0, 0.5 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 0.375, 0.375, 0.375, 0.375 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 0.500, 0.500, 0.500, 0.500 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 0.625, 0.625, 0.625, 0.625 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 0.750, 0.750, 0.750, 0.750 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 0.875, 0.875, 0.875, 0.875 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.000, 1.000, 1.000, 1.000 }));

    // Na,Mg,Al,Si,P,S,Cl,Ar
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 0.500 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 1.000 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 0.375, 0.375, 0.375, 0.375 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 0.500, 0.500, 0.500, 0.500 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 0.625, 0.625, 0.625, 0.625 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 0.750, 0.750, 0.750, 0.750 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 0.875, 0.875, 0.875, 0.875 }));
    Qocc.push_back(std::vector<double> ({ 1.0, 1.0, 1.0, 1.0, 1.0, 1.000, 1.000, 1.000, 1.000 }));

    return Qocc;
}


void MolElData::DoGuess    (symtensor & Fp, eGuess InitialGuess, eMethod ElectronMethod, const std::string & file, const std::vector<int> nuc_chg) {

    if (ElectronMethod == eMethod::RHF || ElectronMethod == eMethod::RHFRSP || ElectronMethod == eMethod::RMP2 || ElectronMethod == eMethod::RKS || ElectronMethod == eMethod::RCMM || ElectronMethod == eMethod::BSO) {
        double EnHF;

        Messenger << "Restricted SCF initial guess:";
        Messenger.Push(); {

            if (InitialGuess==eGuess::SAVE) {
                if (file == "") {
                    Messenger << "restart initial guess but no savefile specified; using core hamiltonian instead" << endl;
                    Fp = Hp;
                }
                else {
                    Messenger << "reading fock/KS matrix from file" << endl;
                    symtensor2 iF;
                    ReadTensorBin(iF, file);
                    Fp = iF;
                }
            }
            else if (InitialGuess==eGuess::SAP) {
                Messenger << "superposition of atomic potentials" << endl;

                // make a block-diagonal projection of Sp and Hp
                symtensor Sa, Ha;
                Sa.init(TensorPattern);
                Ha.init(TensorPattern);

                for (int i=0; i<Sp.getn(); ++i) {
                    if (Sp(i,i)==NULL) continue; // nearly impossible
                    Sa.touch(i,i);               // initializes the block to 0 but only in case it was not yet initialized
                    for (int ii=0; ii<Sp.pos[i]; ++ii) {
                        for (int jj=0; jj<=ii; ++jj) {
                            Sa(i,i,ii,jj) = Sp(i,i, ii,jj); // D0(i,j,ii,jj);
                        }
                    }
                }

                for (int i=0; i<Hp.getn(); ++i) {
                    if (Hp(i,i)==NULL) continue; // nearly impossible
                    Ha.touch(i,i);               // initializes the block to 0 but only in case it was not yet initialized
                    for (int ii=0; ii<Hp.pos[i]; ++ii) {
                        for (int jj=0; jj<=ii; ++jj) {
                            Ha(i,i,ii,jj) = Hp(i,i, ii,jj); // D0(i,j,ii,jj);
                        }
                    }
                }

                // initialize with the integrals, spin, etc.
                SCF.SetSystem  (Sa, Ha, nalpha, nbeta, ENuc);
                SCF.SetSolvers (&EchidnaSolver, &DFT);
                SCF.SetIguess  (Ha);

                int ncore, nfill; {
                    ncore = nfill = 0;
                    for (int i=0; i<natoms; ++i) ncore += Zcore(atoms[i].rAP->Z);
                    for (int i=0; i<natoms; ++i) nfill += Znext(atoms[i].rAP->Z);
                }

                {
                    // we must set all non-diagonal blocks to 0 in both Sp & Hp
                    const double SCFprecision  =  1.e-8; // for SAP, this becomes the distance between consecutive DMs
                    const double CS2center     = -2;     // this sets the 'local' mode
                    const double GaussOverlap  = 1.e-15;
                    const int maxCycles        = 10;     // more that enough
                    string none = "";

                    SCF.SetParams    (none,  CS2center, SCFprecision, maxCycles);

                    // we must compute the core with full J-K, but since many atoms will have open shells,
                    // we should treat valence orbitals as a continuous density, with no effect on K
                    Messenger << "Solving SAP initial guess";
                    SCF.RunSAP(ncore, nfill);


                    Messenger << "Computing SAP potential";
                    D2 = SCF.GetDensity();

                    //Messenger << "quick test: " << Contract(S,D2) << endl;

                    const double CS3thresh     = -3;     // this sets the 'scatter local' mode

                    tensor2 FF(D2.n);

                    EchidnaSolver.Contraction          (&FF, NULL, NULL, &D2, 1, CS3thresh, CS3thresh, false);

                    // assembles Fock
                    Fp = FF;
                    Fp += Hp; // add core to initial guess
                }


                SCF.Clear(); // erase the information about previous iterations
            }

            else if (InitialGuess==eGuess::SAD) {

                Messenger << "Initial guess from superposition of atomic densities (SAD)" << endl;
                bool msg_active = Messenger.IsEnabled();
                if (msg_active) { Messenger.Disable(); }

                // min-AO basis set for SAD guess
                GTObasis gBasis;
                gBasis.name = "min-cc-pVDZ.bs";

                // number of AO in a shell of angular momentum
                // hard-coded for spherical harmonic
                // 1s, 3p, 5d, 7f, 9g, 11h, 13i
                std::vector<int> nao_shell = std::vector<int> ({ 1, 3, 5, 7, 9, 11, 13 });

                // number of AOs on each atom
                std::vector<int> nao_atom_1;
                std::vector<int> nao_atom_2;

                tensor2 S1, S2;

                // process smaller basis
                {
                    MolElData guess;

                    guess.npairs = npairs;
                    guess.nalpha = nalpha;
                    guess.nbeta  = nbeta;
                    guess.ENuc   = ENuc;

                    guess.AtomInteractions = AtomInteractions;

                    MCPbasis McpBasis;
                    ECPbasis EcpBasis;

                    GTOMCPECPbasis combined_basis (gBasis,McpBasis,EcpBasis);
                    combined_basis.Load();

                    guess.SumBasisFunctions (atoms, natoms, gBasis);

                    for (int i = 0; i < nuc_chg.size(); i++) {
                        int ind = nuc_chg[i];
                        ElementBasis& atom_basis = gBasis.AtomBasis[ind];
                        int nao_atom = 0;
                        for (int s = 0; s < atom_basis.Nshells(); s++) {
                            int angl = atom_basis.shell[s].l;
                            int num  = atom_basis.shell[s].J;
                            nao_atom += nao_shell[angl] * num;
                        }
                        nao_atom_1.push_back(nao_atom);
                    }

                    guess.InitPositions(gBasis);
                    guess.InitTensors(ElectronMethod==eMethod::UHF || ElectronMethod==eMethod::UMP2);

                    guess.InitMcp(McpBasis);
                    guess.InitEcp(EcpBasis);

                    guess.CalcShellPairs(gBasis, -log(1e-15));
                    guess.Start1eGTO();

                    S1 = guess.Sp;
                }

                // process mixed basis
                {
                    gBasis += *basis; // merges the two basis sets

                    MolElData mix;

                    mix.npairs = npairs;
                    mix.nalpha = nalpha;
                    mix.nbeta  = nbeta;

                    mix.AtomInteractions = AtomInteractions;

                    mix.SumBasisFunctions (atoms, natoms, gBasis);

                    for (int i = 0; i < nuc_chg.size(); i++) {
                        int ind = nuc_chg[i];
                        ElementBasis& atom_basis = gBasis.AtomBasis[ind];
                        int nao_atom = 0;
                        for (int s = 0; s < atom_basis.Nshells(); s++) {
                            int angl = atom_basis.shell[s].l;
                            int num  = atom_basis.shell[s].J;
                            nao_atom += nao_shell[angl] * num;
                        }
                        nao_atom_2.push_back(nao_atom);
                    }

                    // get number of AOs per atom in the larger basis
                    for (int i = 0; i < nuc_chg.size(); i++) {
                        nao_atom_2[i] -= nao_atom_1[i];
                    }

                    // MolElData should hold its own copy of Atoms, nAtoms and gBasis,
                    // modified for ECPs, etc.
                    mix.InitPositions(gBasis);
                    mix.InitTensors(ElectronMethod==eMethod::UHF || ElectronMethod==eMethod::UMP2);

                    mix.CalcShellPairs(gBasis, -log(1e-15));
                    mix.Start1eGTO();

                    S2 = mix.Sp;
                }

                // extract overlap S12 and S22

                tensor2 S12, S22;
                S12.setsize(S1.n, S2.n-S1.n);
                S22.setsize(S2.n-S1.n, S2.n-S1.n);

                int natoms = nuc_chg.size();

                std::vector<std::string> ao_mol;
                for (int i = 0; i < natoms; i++) {
                    for (int j = 0; j < nao_atom_1[i]; j++) {
                        ao_mol.push_back("A");
                    }
                    for (int k = 0; k < nao_atom_2[i]; k++) {
                        ao_mol.push_back("B");
                    }
                }

                assert(ao_mol.size() == S2.n);

                std::vector<int> list_1;
                std::vector<int> list_2;
                for (int i = 0; i < ao_mol.size(); i++) {
                    if (ao_mol[i] == "A") { list_1.push_back(i); }
                    if (ao_mol[i] == "B") { list_2.push_back(i); }
                }

                for (int i = 0; i < list_1.size(); i++) {
                for (int j = 0; j < list_2.size(); j++) {
                    S12[i][j] = S2[list_1[i]][list_2[j]];
                }}

                for (int i = 0; i < list_2.size(); i++) {
                for (int j = 0; j < list_2.size(); j++) {
                    S22[i][j] = S2[list_2[i]][list_2[j]];
                }}

                // form C_SAD matrix

                tensor2 C_SAD;
                C_SAD.setsize(S12.m, S12.n);
                C_SAD.zeroize();

                std::vector< std::vector<double> > Qocc = BuildQocc();

                int start_1 = 0;
                int start_2 = 0;
                for (int a = 0; a < natoms; a++) {

                    tensor2 s_12;
                    tensor2 s_22;

                    s_12.setsize(nao_atom_1[a], nao_atom_2[a]);
                    s_22.setsize(nao_atom_2[a], nao_atom_2[a]);

                    for (int i = 0; i < nao_atom_1[a]; i++) {
                    for (int j = 0; j < nao_atom_2[a]; j++) {
                        s_12[i][j] = S12[i + start_1][j + start_2];
                    }}

                    for (int i = 0; i < nao_atom_2[a]; i++) {
                    for (int j = 0; j < nao_atom_2[a]; j++) {
                        s_22[i][j] = S22[i + start_2][j + start_2];
                    }}

                    // A = S12' C1(identity)
                    tensor2 A;
                    tensor2 C1;
                    C1.setsize(s_12.n, s_12.n);
                    C1.zeroize();
                    for (int ind = 0; ind < C1.n; ind++) {
                        C1[ind][ind] = 1.0;
                    }
                    TensorProductNTN(A, s_12, C1);

                    // S22^-1
                    tensor2 evec_22;
                    evec_22 = s_22;
                    double* eval_22 = new double [s_22.n];
                    DiagonalizeV(evec_22, eval_22);
                    free(eval_22);

                    tensor2 prod, diag;
                    TensorProductNNT(prod, s_22, evec_22);
                    TensorProduct   (diag, evec_22, prod);
                    for (int ind = 0; ind < diag.n; ind++) {
                        diag[ind][ind] = 1.0 / diag[ind][ind];
                    }

                    tensor2 s_22_inv;
                    TensorProduct   (prod, diag, evec_22);
                    TensorProductNTN(s_22_inv, evec_22, prod);

                    // M = A' S22^-1 A
                    tensor2 M;
                    TensorProduct   (prod, s_22_inv, A);
                    TensorProductNTN(M, A, prod);

                    // M^-1/2
                    tensor2 evec_M;
                    evec_M = M;
                    double* eval_M = new double [M.n];
                    DiagonalizeV(evec_M, eval_M);
                    free(eval_M);

                    TensorProductNNT(prod, M, evec_M);
                    TensorProduct   (diag, evec_M, prod);
                    for (int ind = 0; ind < diag.n; ind++) {
                        diag[ind][ind] = 1.0 / sqrt(diag[ind][ind]);
                    }

                    tensor2 M_invsqrt;
                    TensorProduct   (prod, diag, evec_M);
                    TensorProductNTN(M_invsqrt, evec_M, prod);

                    // C2 = S22^-1 A M^-1/2

                    tensor2 C2;
                    TensorProduct(prod, A, M_invsqrt);
                    TensorProduct(C2, s_22_inv, prod);

                    // place C2

                    for (int j = 0; j < nao_atom_2[a]; j++) {
                    for (int i = 0; i < nao_atom_1[a]; i++) {
                        C_SAD[j + start_2][i + start_1] = C2[j][i] * sqrt(Qocc[nuc_chg[a]][i]);
                    }}

                    // increment starting AO indices

                    start_1 += nao_atom_1[a];
                    start_2 += nao_atom_2[a];
                }

                // form initial density and Fock matrix

                tensor2 D_SAD;
                TensorProductNNT(D_SAD, C_SAD, C_SAD);

                tensor2 J_SAD, K_SAD;
                J_SAD.setsize(D_SAD.n, D_SAD.m);
                K_SAD.setsize(D_SAD.n, D_SAD.m);
                EchidnaSolver.Contraction(&J_SAD, &K_SAD, NULL, &D_SAD, 1, 1.0e-14, 1.0e-14);
                J_SAD *= 2.0;
                K_SAD *= -1.0;
                J_SAD += K_SAD;

                Fp = J_SAD;
                Fp += Hp;

                if (msg_active) { Messenger.Enable(); }
                Messenger << "SAD guess ready" << endl;
            }

            else if (InitialGuess==eGuess::CORE) {
                Messenger << "core hamiltonian" << endl;
                Fp = Hp;
            }
            else if (InitialGuess==eGuess::KIN) {
                Messenger << "kinetic energy" << endl;
                Fp = Tp;
            }
            else if (InitialGuess==eGuess::DFIT) {

                /*
                if (file == "") {
                    Messenger << "initial guess fitting but no basis set specified; attempting Huzinaga MINI" << endl;
                    DMload = "basis/mini.bs";
                }
                */

                if (file == "") {
                    Messenger << "initial guess fitting but no basis set specified" << endl;
                    throw 12321;
                }


                GTObasis    gBasis;   // gaussian basis set
                gBasis.name = file;
                MolElData guess;

                tensor D0;

                // first we solve the system with a smaller basis
                {
                    // set e- in the electrondata
                    guess.npairs = npairs;
                    guess.nalpha = nalpha;
                    guess.nbeta  = nbeta;

                    guess.ENuc   = ENuc;


                    MCPbasis    McpBasis; // details for the model core potentials
                    ECPbasis    EcpBasis; // details for the effective core potentials


                    guess.AtomInteractions = AtomInteractions;
                    GTOMCPECPbasis combined_basis(gBasis,McpBasis,EcpBasis);
                    combined_basis.Load();

                    guess.SumBasisFunctions (atoms, natoms, gBasis);


                    // MolElData should hold its own copy of Atoms, nAtoms and gBasis,
                    // modified for ECPs, etc.
                    guess.InitPositions (gBasis);
                    guess.InitTensors   (ElectronMethod==eMethod::UHF || ElectronMethod==eMethod::UMP2);

                    guess.InitMcp       (McpBasis);
                    guess.InitEcp       (EcpBasis);

                    Messenger << "Starting computation" << endl;
                    Messenger.Push(); {
                        guess.CalcShellPairs (gBasis, -log(1e-15)); // set a very high logGDO just in case there are ECPs;
                        guess.Start1eGTO     ();
                        guess.Start2eGTO     ();
                    } Messenger.Pop();

                    // now run the actual system
                    {
                        const double SCFprecision = 1.e-4;  // the first iteration with the new basis is typically higher than this
                        const double CSthresh     = 1.e-10;
                        const double CDprecision  = 0;      // no Cholesky (for the moment
                        const enum eGuess   InitialGuess   = eGuess::CORE; //SAP;
                        const enum eMethod  ElectronMethod = eMethod::RHF;
                        const int maxCycles = 50;

                        string none = "";

                        guess.ENuc = ENuc;

                        guess.CalcElectronic (InitialGuess, ElectronMethod, CSthresh, SCFprecision, CDprecision, maxCycles, none, none, nuc_chg); // this should be set in two different places
                    }

                    // retrieve the converged SCF density matrix and translate its format
                    guess.D2 = guess.SCF.GetDensity();

                    D0.init  (guess.TensorPattern);
                    D0.touch (guess.AtomInteractions);
                    D0 = guess.D2;
                }


                tensor G2;

                // then we run an EFS call with a synthetic basis made from the sum of the two
                {
                    gBasis += *basis; // merges the two basis sets

                    MolElData mix;

                    // set e- in the electrondata
                    mix.npairs = npairs;
                    mix.nalpha = nalpha;
                    mix.nbeta  = nbeta;

                    mix.AtomInteractions = AtomInteractions;


                    mix.SumBasisFunctions (atoms, natoms, gBasis);

                    // MolElData should hold its own copy of Atoms, nAtoms and gBasis,
                    // modified for ECPs, etc.
                    mix.InitPositions (gBasis); //mol.InitElectronDataPositions(gBasis);
                    mix.InitTensors   (ElectronMethod==eMethod::UHF || ElectronMethod==eMethod::UMP2); //mol.InitElectronData( ElectronMethod==UHF || ElectronMethod==UMP2);

                    Messenger.Push(); {
                        mix.CalcShellPairs (gBasis, -log(1e-15));
                        mix.Start1eGTO     ();
                        mix.Start2eGTO     ();

                    } Messenger.Pop();

                    // converts D0 to the mixed basis & then computes J&K
                    {
                        tensor DD;

                        DD.init(mix.TensorPattern);
                        DD.zeroize();

                        for (int i=0; i<D0.getn(); ++i) {
                            for (int j=0; j<D0.getn(); ++j) {
                                for (int ii=0; ii<D0.pos[i]; ++ii) {
                                    for (int jj=0; jj<D0.pos[j]; ++jj) {
                                        DD(i,j,ii,jj) = D0(i,j,ii,jj);
                                    }
                                }
                            }
                        }

                        mix.D2 = DD;

                        tensor2 J2(mix.D2);
                        tensor2 K2(mix.D2);

                        // compute 2-electron contributions to Fock
                        const double CSthresh  = 1.e-9; // this has changed and it will compute all integrals by default
                        mix.EchidnaSolver.Contraction          (&J2, &K2, NULL, &mix.D2, 1, -1, CSthresh); // -1 is to skip unwanted integrals

                        J2 *=  2.; K2 *= -1.; J2 += K2;

                        // translate format
                        // mix.Hp = J2;
                        G2.init  (mix.TensorPattern);
                        G2.touch (mix.AtomInteractions);
                        G2.zeroize();
                        G2 = J2;
                    }


                    // assemble Fock
                    Fp = Hp;

                    // converts G from the mixed basis to the system's
                    {
                        for (int i=0; i<Fp.getn(); ++i) {
                            for (int j=0; j<i; ++j) {

                                if (G2(i,j)==NULL) continue; // do not do anything if the block is empty
                                Fp.touch(i,j);               // initializes the block to 0 but only in case it was not yet initialized

                                const int osn = guess.TensorPattern.getpos(i);
                                const int osm = guess.TensorPattern.getpos(j);

                                for (int ii=0; ii<Fp.pos[i]; ++ii) {
                                    for (int jj=0; jj<Fp.pos[j]; ++jj) {
                                        Fp(i,j,ii,jj) += G2(i,j, osn+ii,osm+jj); // D0(i,j,ii,jj);
                                    }
                                }
                            }
                            if (G2(i,i)!=NULL) {

                                Fp.touch(i,i);               // initializes the block to 0 but only in case it was not yet initialized

                                const int osn = guess.TensorPattern.getpos(i);

                                for (int ii=0; ii<Fp.pos[i]; ++ii) {
                                    for (int jj=0; jj<=ii; ++jj) {
                                        Fp(i,i,ii,jj) += G2(i,i, osn+ii,osn+jj); // D0(i,j,ii,jj);
                                    }
                                }
                            }
                        }
                    }

                }

                Messenger << "everything ready " << endl;
            }
            else
                Messenger << "unrecognized initial guess? " << endl;

        } Messenger.Pop();
        Messenger << endl;

    }

    else if (ElectronMethod == eMethod::UHF || ElectronMethod == eMethod::UKS || ElectronMethod == eMethod::ROHF) {

        Messenger << "Unrestricted SCF initial guess : ";

        Messenger.Push(); {
            Messenger << "core hamiltonian" << endl;
            Fp = Hp;
        } Messenger.Pop();

    }

}
