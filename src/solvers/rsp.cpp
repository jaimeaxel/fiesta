/*
    rsp.cpp

    linear response test
*/

#include <cmath>
#include "fiesta/fiesta.hpp"
#include "molecule/edata.hpp"

#include "low/io.hpp"
#include "low/chrono.hpp"

#include "orbitals/DMsolve.hpp"
#include "linear/sparse.hpp"
#include "linear/tensors.hpp"
#include "linear/eigen.hpp"

#include "fiesta/calculate.hpp"
using namespace std;
using namespace Fiesta;

#include "basis/shellpair.hpp"


/*
    parameters for RSP & CPP
*/

#include "rsp.hpp"

int RSPparams::SetGamma (double g) {gg=g; return 0;}

int RSPparams::SetFreqs (double w0, double wf, int N) {

    NW = N+1; // add the endpoint, too
    ww = new double[NW];

    double Aw = (wf-w0)/double(NW-1);

    for (int i=0; i<NW; ++i) ww[i] = w0 + (double(i))*Aw;

    return 0;
}

int RSPparams::SetFreqs (const double * w, int N) {

    NW = N; // add the endpoint, too
    ww = new double[NW];

    for (int i=0; i<NW; ++i) ww[i] = w[i];

    return 0;
}


int RSPparams::SetCSthresh     (double th) {Dthresh=th; return 0;}
int RSPparams::SetConvergence  (double th) {Cthresh=th; return 0;}
int RSPparams::SetSVDthresh    (double th) {Sthresh=th; return 0;}

int RSPparams::SetMaxRSPcycles (int N)     {NT=N;       return 0;}
int RSPparams::SetMaxSubspace  (int N)     {MD=N;       return 0;}

int RSPparams::SetMaxNumVecs   (int N)     {MV=N;       return 0;}

int RSPparams::SetActive       (int N)     {NA=N;       return 0;}

int RSPparams::SetCDthreshold  (double th) {CDthresh=th; return 0;};

int RSPparams::SetSavePath     (const std::string & s) {savepath = s; return 0;};

RSPparams::~RSPparams() {delete[] ww;}



// complex frequencies
void InverseCPP (double * RS, double * RA, double * IS, double * IA, const double * ee, double wr, double wi, const int N) {

    // FIXME !!! BLAS 2
    // same here; whatever can be done is dubious

    # pragma omp parallel for
    for (int n=0; n<N; ++n) {

        // uses U+ E U U^-1 x = U+ r
        //           (+1 -1)
        // with U =
        //           (+1 +1)

        // diagonal elements
        double we1 = ee[n] - wr;
        double we2 = ee[n] + wr;

        // rotate rhs to the U basis, too
        double rr1 =  RS[n] + RA[n];
        double rr2 = -RS[n] + RA[n];
        double ri1 =  IS[n] + IA[n];
        double ri2 = -IS[n] + IA[n];

        // symmetrization scheme
        // of the CPP matrix
        //ri1 *= -1.;
        //ri2 *= -1.;

        // now invert the 2 2x2 R/I matrices
        double iw1 = 1./ (we1*we1 + wi*wi);
        double iw2 = 1./ (we2*we2 + wi*wi);


        double xr1 = (we1*rr1 +  wi*ri1) * iw1;
        double xi1 = ( wi*rr1 - we1*ri1) * iw1;
        double xr2 = (we2*rr2 -  wi*ri2) * iw2;
        double xi2 = (-wi*rr2 - we2*ri2) * iw2;

        // apply U to the result & scale with the missing U U+ normalization of 1/2
        RS[n] = 0.5*( xr1-xr2);
        RA[n] = 0.5*( xr1+xr2);
        IS[n] = 0.5*( xi1-xi2);
        IA[n] = 0.5*( xi1+xi2);


        /*
        double we1 = ee[n] - wr;
        double we2 = ee[n] + wr;

        double rr1 = RS[n] + RA[n];
        double rr2 = RS[n] - RA[n];
        double ri1 = IS[n] + IA[n];
        double ri2 = IS[n] - IA[n];

        double iw1 = 1./ (we1*we1 + wi*wi);
        double iw2 = 1./ (we2*we2 + wi*wi);

        double xr1 = (we1*rr1 +  wi*ri1) * iw1;
        double xi1 = ( wi*rr1 - we1*ri1) * iw1;
        double xr2 = (we2*rr2 -  wi*ri2) * iw2;
        double xi2 = (-wi*rr2 - we2*ri2) * iw2;

        RS[n] = 0.5*(xr1+xr2);
        RA[n] = 0.5*(xr1-xr2);
        IS[n] = 0.5*(xi1+xi2);
        IA[n] = 0.5*(xi1-xi2);
        */
    }

}

// new preconditioner for complex frequencies
void InverseCPP (double * RS, double * RA, double * IS, double * IA, const double * es, const double * ea, double wr, double wi, const int N) {


    // this can be done much simpler by scaling both sides with diag( sqrt(ea/es), sqrt(es/ea) )
    // and applying the usual preconditioning for the geometric mean

    # pragma omp parallel for
    for (int n=0; n<N; ++n) {

        // compute the 4x4 inverse of:
        //  es  -wr    0  wi
        // -wr   ea   wi   0
        //   0   wi  -es  wr
        //  wi    0   wr -ea


        const double s = es[n];
        const double a = ea[n];
        const double w = wr;
        const double g = wi;

        // (w^2 + g^2) ^2 + (es*ea)^2 - 2*es*ea*(w^2-g^2) > 0
        double det = (w*w + g*g) * (w*w + g*g) + (s*a) * ( (s*a) - 2*(w+g)*(w-g) );

        double inv = 1./det;

        // diagonal terms: ds, da, -ds, -da

        double dd = s*a + g*g - w*w;

        double ds = a * dd;
        double da = s * dd;

        // antidiagonal term
        double aa = g * (s*a + g*g + w*w);

        // conjugate block-antidiagonal terms
        double cc = 2*g*w;
        double cs = cc*a;
        double ca = cc*s;

        // conjugate block-diagonal term
        double bb = w * (s*a - g*g - w*w);


        // short-hand rhs values
        const double rs =   RS[n];
        const double ra =   RA[n];
        const double is =   IS[n]; // from matrix symmetrization
        const double ia =   IA[n]; //

        // matrix multiplication
        double xs = ds*rs + bb*ra + cs*is + aa*ia;
        double xa = bb*rs + da*ra + aa*is + ca*ia;

        double ys = cs*rs + aa*ra - ds*is - bb*ia;
        double ya = aa*rs + ca*ra - bb*is - da*ia;

        // overwrite with solution
        RS[n] = xs * inv;
        RA[n] = xa * inv;
        IS[n] = ys * inv;
        IA[n] = ya * inv;
    }

}

// imaginary frequencies
void InverseImag (double * RS, double * IA, const double * Ea, double wi, const int N) {

    // FIXME !!! BLAS 2
    // same here; whatever can be done is dubious

    # pragma omp parallel for
    for (int n=0; n<N; ++n) {

        double we = Ea[n];
        double rr = RS[n];
        double ri = IA[n];

        // symmetrization scheme
        // of the complex matrix
        //ri *= -1;

        double iw = 1./ (we*we + wi*wi);

        double xr = ( we*rr + wi*ri) * iw;
        double xi = ( wi*rr - we*ri) * iw;

        RS[n] = xr;
        IA[n] = xi;
    }

}

// real frequencies
void InverseReal (double * RS, double * RA, const double * Ea, double wr, const int N) {

    // FIXME !!! BLAS 2
    // same here; whatever can be done is dubious
    # pragma omp parallel for
    for (int n=0; n<N; ++n) {
        double we1 = Ea[n] - wr;
        double we2 = Ea[n] + wr;

        double rr1 = RS[n] + RA[n];
        double rr2 = RS[n] - RA[n];

        double xr1 =  (rr1) / (we1);
        double xr2 =  (rr2) / (we2);

        RS[n] = 0.5*(xr1+xr2);
        RA[n] = 0.5*(xr1-xr2);
    }

}


// complex frequencies
void SDPprecond (double * RS, double * RA, double * IS, double * IA, const double * ee, double wr, double wi, const int N, double emin=0.) {


    // FIXME !!! BLAS 2
    // same here; whatever can be done is dubious

    # pragma omp parallel for
    for (int n=0; n<N; ++n) {

        // uses U+ E U U^-1 x = U+ r
        //           (+1 -1)
        // with U =
        //           (+1 +1)

        // diagonal elements
        double we1 = ee[n] - wr;
        double we2 = ee[n] + wr;

        // rotate rhs to the U basis, too
        double rr1 =  RS[n] + RA[n];
        double rr2 = -RS[n] + RA[n];
        double ri1 =  IS[n] + IA[n];
        double ri2 = -IS[n] + IA[n];

        // symmetrization scheme
        //ri1 *= -1.;
        //ri2 *= -1.;

        // diagonalize the two 2x2 systems
        // ===============================
        // scale to a^2+b^2 = 1; this also scales the eigenvalues of the 2x2 systems to 1,-1

        double s1 = sqrt(we1*we1 + wi*wi);
        double s2 = sqrt(we2*we2 + wi*wi);

        double is1 = 1.;
        double is2 = 1.;

        // complementary Yukawa-like potential;
        // smooth out inverses for small energy gaps, since in all likelihood  the error is larger than  the gap itself
        // Integrate[Exp[-s1 t ], {t, 0, 1./emin}]
        if (emin>0.) {
            double ie = 1./emin;
            is1 -= exp (-s1*ie);
            is2 -= exp (-s2*ie);
        }

        is1 /= s1;
        is2 /= s2;



        rr1 *= is1;
        ri1 *= is1;
        rr2 *= is2;
        ri2 *= is2;

        // now, since doing the abs val of the eigenvalues will result in a unitary matrix, we can skip the rotation

        // do the 2x2 unitary rotations back to the original basis
        RS[n] = 0.5 * ( rr1 - rr2);
        RA[n] = 0.5 * ( rr1 + rr2);
        IS[n] = 0.5 * ( ri1 - ri2);
        IA[n] = 0.5 * ( ri1 + ri2);

    }

}

// new preconditioner for complex frequencies; symmetric definite positive
void SDPprecond (double * RS, double * RA, double * IS, double * IA, const double * es, const double * ea, double wr, double wi, const int N) {


    # pragma omp parallel for
    for (int n=0; n<N; ++n) {



        // original matrix & rhs
        //  es  -wr    0  wi
        // -wr   ea   wi   0
        //   0   wi  -es  wr
        //  wi    0   wr -ea

        //
        const double s = es[n]; // >0
        const double a = ea[n]; // >0
        const double w = wr;
        const double g = wi;

        double rs =   RS[n];
        double ra =   RA[n];
        double is =   IS[n]; // from matrix symmetrization
        double ia =   IA[n]; //


        // scale the matrix & rhs
        //  1 -r   0  i
        // -r  1   i  0
        //  0  i  -1  r
        //  i  0   r -1

        const double fs = 1./sqrt(s);
        const double fa = 1./sqrt(a);
        const double r = w * (fs*fa);
        const double i = g * (fs*fa);

        rs *= fs;
        ra *= fa;
        is *= fs;
        ia *= fa;

        // 2x2 rotate matrix & rhs
        //  1-r  0    i    0
        //   0  1+r   0   -i
        //   i   0  -1+r   0
        //   0  -i    0  -1-r

        double r1 =   rs+ra;
        double r2 =   rs-ra;
        double i1 =   is+ia;
        double i2 =   is-ia;


        // diagonalize the 2x2 subsystems; use absolute value of eigenvalue (so they become unitary)
        double f1 = 1./ sqrt( (1-r)*(1-r) + i*i );
        double f2 = 1./ sqrt( (1+r)*(1+r) + i*i );

        r1 *= f1;
        r2 *= f2;
        i1 *= f1;
        i2 *= f2;


        // undo 2x2 rotation
        rs =   r1+r2;
        ra =   r1-r2;
        is =   i1+i2;
        ia =   i1-i2;

        // apply second half of diagonal scaling
        rs *= fs;
        ra *= fa;
        is *= fs;
        ia *= fa;

        // save; there is a missing 1/2 factor from the 2x2 rotations
        RS[n] = 0.5*rs;
        RA[n] = 0.5*ra;
        IS[n] = 0.5*is;
        IA[n] = 0.5*ia;
    }

}


// make inner product for Woodbury preconditioner;
// fixed formula
void WoodburyBlocks (tensor2 & WBE, tensor2 & WBG, const tensor2 & V, const double * es, const double * ea, double wr, double wi, const int NOV) {

    // we need the diagonal of the first and third blocks, equivalent up to a sign
    // we also need the cross-terms, i.e.
    double  dd[NOV];
    double  gg[NOV];


    for (int n=0; n<NOV; ++n) {
        const double s = es[n];
        const double a = ea[n];
        const double w = wr;
        const double g = wi;

        // (w^2 + g^2) ^2 + (es*ea)^2 - 2*es*ea*(w^2-g^2) > 0
        double det = (w*w + g*g) * (w*w + g*g) + (s*a) * ( (s*a) - 2*(w+g)*(w-g) );
        double inv = 1./det;

        dd[n] = a* (s*a + g*g - w*w) * inv;
        gg[n] = 2*g*w*a              * inv;
    }

    // now compute the weigthed inner product
    WeightedInnerProduct (WBE, V, dd);
    WeightedInnerProduct (WBG, V, gg);

    const int M = V.n;

    tensor2 WB(2*M);
    WB.zeroize();

    // add identity
    for (int k=0; k<M; ++k) WBE(k,k) += 1.;

    // block invert (~4x faster than full matrix)
    {
        tensor2 IWE, IEG, GEG;
        IWE = WBE;
        Invert(IWE);

        TensorProduct (IEG, IWE, WBG);
        TensorProduct (GEG, WBG, IEG);

        WBE += GEG; //

        Invert(WBE); // (E + G E^-1 G)^-1
        TensorProduct(WBG, IEG, WBE); // E^-1 G (E + G E^-1 G)^-1
    }

    /*
    for (int i=0; i<M; ++i)
        for (int j=0; j<M; ++j) {
            WB (i,j)     =  WBE (i,j);
            WB (M+i,M+j) = -WBE (i,j);
            WB (M+i,j)   =  WBG (i,j);
            WB (  i,M+j) =  WBG (i,j);
        }


    //InvertLU(WB);
    // invert
    Invert(WB);

    // split result
    for (int i=0; i<M; ++i)
        for (int j=0; j<M; ++j) {
            WBE (i,j) = WB (i,j);
            WBG (i,j) = WB (i,M+j);
        }
    */
}

// Woodbury update to diagonal preconditioner;
// updates the blocks E0S and E0A of a diagonal preconditioner with low rank factorizations, decomposed in U diag(d) U+
// zs & za contain the unpreconditioned residuals in usual R/I
void BlockUpdate (
                  const double * es, const double * ea, const double * wr, const double * wi, int NOV, int NK, // preconditioner
                  const tensor2 & US, const tensor2 & UA, const double * ds, const double * da, int RD,         // decomposed updates, i.e.    US+ ds^-1 US
                  tensor2 & zs, tensor2 & za) {

    // first compute the diagonals of the original preconditioners:
    tensor2 DS(NK,NOV), DA(NK,NOV), CS(NK,NOV), CA(NK,NOV), AA(NK,NOV), BB(NK,NOV);

    // compute the 4x4 inverse of:
    //  es  -wr    0  wi
    // -wr   ea   wi   0
    //   0   wi  -es  wr
    //  wi    0   wr -ea
    // for each equation k and each element of the diagonal;
    // the ivnerse matrix has the structure:
    //  ds  bb   cs   aa
    //  bb  da   aa   ca
    //  cs  aa  -ds  -bb
    //  aa  ca  -bb  -da
    for (int k=0; k<NK; ++k) {
        for (int n=0; n<NOV; ++n) {

            const double s = es[n];
            const double a = ea[n];
            const double w = wr[k];
            const double g = wi[k];

            // (w^2 + g^2) ^2 + (es*ea)^2 - 2*es*ea*(w^2-g^2) > 0
            double det = (w*w + g*g) * (w*w + g*g) + (s*a) * ( (s*a) - 2*(w+g)*(w-g) );

            double inv = 1./det;

            // diagonal terms: ds, da, -ds, -da

            double dd = s*a + g*g - w*w;

            double ds = a * dd;
            double da = s * dd;

            // antidiagonal term
            double aa = g * (s*a + g*g + w*w);

            // conjugate block-antidiagonal terms
            double cc = 2*g*w;
            double cs = cc*a;
            double ca = cc*s;

            // conjugate block-diagonal term
            double bb = w * (s*a - g*g - w*w);

            // store for later consumption
            DS(k,n) = ds * inv;
            DA(k,n) = da * inv;
            CS(k,n) = cs * inv;
            CA(k,n) = ca * inv;
            AA(k,n) = aa * inv;
            BB(k,n) = bb * inv;

            /*
            // short-hand rhs values
            const double rs =   RS[n];
            const double ra =   RA[n];
            const double is =   IS[n]; // from matrix symmetrization
            const double ia =   IA[n]; //

            // overwrite solution with matrix mult.
            RS[n] = ds*rs + bb*ra + cs*is + aa*ia;;
            RA[n] = bb*rs + da*ra + aa*is + ca*ia;
            IS[n] = cs*rs + aa*ra - ds*is - bb*ia;
            IA[n] = aa*rs + ca*ra - bb*is - da*ia;
            */
        }
    }


    // now build the projections
    tensor3 DS3(NK,RD,RD), DA3(NK,RD,RD), CS3(NK,RD,RD), CA3(NK,RD,RD), AA3(NK,RD,RD), BB3(NK,RD,RD);

    for (int k=0; k<NK; ++k) {
        WeightedInnerProduct (DS3[k], US, DS[k]);
        WeightedInnerProduct (DA3[k], UA, DA[k]);
        WeightedInnerProduct (CS3[k], US, CS[k]);
        WeightedInnerProduct (CA3[k], UA, CA[k]);

        WeightedInnerProduct (AA3[k], US, UA, AA[k]);
        WeightedInnerProduct (BB3[k], US, UA, BB[k]);
    }

    // update the diagonal blocks with the inverse diagonals
    for (int k=0; k<NK; ++k) {
        for (int r=0; r<RD; ++r) DS3[k](r,r) += ds[r];
        for (int r=0; r<RD; ++r) DA3[k](r,r) += da[r];
    }

    // generate rhs of Woodbury update
    tensor2 RS, RA; // (2*NK) * RD
    {
        tensor2 ys, ya;
        ys = zs;
        ya = za;

        // precondition the rhs
        for (int k=0; k<NK; ++k) {
            SDPprecond (ys[k], ya[k], ys[k+NK], ya[k+NK], es, ea, wr[k], wi[k], NOV);
        }

        // project zs/za onto the updates' basis
        TensorProductNNT (RS, ys, US);
        TensorProductNNT (RA, ya, UA);
    }


    tensor2 bb(4,RD);
    tensor2 M(4*RD);

    // now set up and solve the linear eqs
    for (int k=0; k<NK; ++k) {

        // right hand side


        // copy; remember to swap IS <-> RA
        for (int r=0; r<RD; ++r) {
            bb(0,r) =  RS(k,r);
            bb(1,r) =  RA(k,r);
            bb(2,r) =  RS(NK+k,r);
            bb(3,r) =  RA(NK+k,r);
        }

        M.zeroize();

        // build the matrix;
        // AA & BB must be transposed in AS blocks

        for (int r=0; r<RD; ++r) {
            for (int s=0; s<RD; ++s) {
                M (0*RD+r,0*RD+s) =  DS3[k](r,s);
                M (0*RD+r,1*RD+s) =  BB3[k](r,s);
                M (0*RD+r,2*RD+s) =  CS3[k](r,s);
                M (0*RD+r,3*RD+s) =  AA3[k](r,s);

                M (1*RD+r,0*RD+s) =  BB3[k](s,r);
                M (1*RD+r,1*RD+s) =  DA3[k](r,s);
                M (1*RD+r,2*RD+s) =  AA3[k](s,r);
                M (1*RD+r,3*RD+s) =  CA3[k](r,s);

                M (2*RD+r,0*RD+s) =  CS3[k](r,s);
                M (2*RD+r,1*RD+s) =  AA3[k](r,s);
                M (2*RD+r,2*RD+s) = -DS3[k](r,s);
                M (2*RD+r,3*RD+s) = -BB3[k](r,s);

                M (3*RD+r,0*RD+s) =  AA3[k](s,r);
                M (3*RD+r,1*RD+s) =  CA3[k](r,s);
                M (3*RD+r,2*RD+s) = -BB3[k](s,r);
                M (3*RD+r,3*RD+s) = -DA3[k](r,s);
            }
        }

        // solve the system
        SolveLinear(M, bb[0]);


        // copy back; remember to swap IS <-> RA
        for (int r=0; r<RD; ++r) {
            RS(k,r) = bb(0,r);
            RA(k,r) = bb(1,r);
            RS(NK+k,r) = bb(2,r);
            RA(NK+k,r) = bb(3,r);
        }

    }

    // now update vector
    TensorProduct (zs,RS,US, -1.);
    TensorProduct (za,RA,UA, -1.);


    // finally, apply E0 preconditioner to the updated system
    for (int k=0; k<NK; ++k) {
        SDPprecond (zs[k], za[k], zs[k+NK], za[k+NK], es, ea, wr[k], wi[k], NOV);
    }


}

// creates the rank decomposed update for the updated preconditioner
void ComputeUpdate (
                    tensor2 & U, double * du, // result
                    const tensor2 & rr, const tensor2 & bb, const double * ee, const double * e0, int RD, int N // rho, basis, eigenvals, diag. E0,
                   ) {

    tensor2 AE(RD);
    WeightedInnerProduct(AE, bb, e0);
    AE *= -1; // it's E - E0

    for (int r=0; r<RD; ++r) AE(r,r) += ee[r];

    tensor2 B, ie(1,RD), ss(1,RD);
    B = rr;

    for (int r=0; r<RD; ++r) ie(0,r) = 1./ ee[r];

    DiagonalScaling(ie[0],B);

    // now we have U as an orthonormal basis and T in between; diagonalize T and rotate U accordingly
    DiagonalizeV(AE,ss[0]);

    // must discard any updates too low to contribute:
    Messenger << "evs of Woodbury update : " << ss << endl;

    TensorProduct(U,AE,B); // rotate

    for (int r=0; r<RD; ++r) du[r] = 1./ss(0,r); // these are inverses


    /*
    tensor2 AE(RD);
    WeightedInnerProduct(AE, bb, e0);
    AE *= -1; // it's E - E0

    for (int r=0; r<RD; ++r) AE(r,r) += ee[r];


    for (int r=0; r<RD; ++r)
        for (int s=0; s<RD; ++s)
            AE(r,s) /= ee[r] * ee[s];

    tensor2 B;
    B = rr;

    tensor2 RR(RD), ss(1,RD);
    SVD (B, RR, ss[0]);

    // now rotate AE with RR
    {
        tensor2 T;
        TensorProduct     (T, AE, RR);
        TensorProductNTN  (AE,RR, T);
    }

    // ... and scale with the singular values
    for (int r=0; r<RD; ++r)
        for (int s=0; s<RD; ++s)
            AE(r,s)*= ss(0,r)*ss(0,s);

    // now we have U as an orthonormal basis and T in between; diagonalize T and rotate U accordingly
    DiagonalizeV(AE,ss[0]);

    // must discard any updates too low to contribute:
    Messenger << "evs of Woodbury update : " << ss << endl;

    TensorProduct(U,AE,B); // rotate


    for (int r=0; r<RD; ++r) du[r] = 1./ss(0,r); // these are inverses
    */


    // OLD METHOD; TOO IMPRECISE

    /*
    tensor2 B(2*RD,N);

    tensor2 B0(RD,N,B[0]);
    tensor2 B1(RD,N,B[RD]);

    B0 = rr; // copy rho vectors to the first half of the space
    B1 = bb; // copy basis to the second half
    DiagonalScaling(B1,e0); // scale with E0


    // find an orthonormal basis from B and the rotation for it
    tensor2 RR(2*RD), ss(1,2*RD);
    SVD (B, RR, ss[0]);


    tensor2 AE(2*RD);

    // now we have to apply the inner rotation to the energy fitting subspace:
    {
        AE.zeroize();

        for (int r=0; r<RD; ++r) AE(r,r) = 1./ee[r]; // first half are the inverse E2 eigenvalues

        // compute the inner product of the basis with E0
        tensor2 E0;
        WeightedInnerProduct(E0, bb, e0);

        // fill the fit (which is negative)
        Invert (E0);

        for (int r=0; r<RD; ++r)
            for (int s=0; s<RD; ++s)
                AE(RD+r,RD+s) = -E0(r,s);
    }

    // now rotate AE with RR
    tensor2 T;
    TensorProduct    (T, AE, RR);
    TensorProductNTN (AE,RR, T);

    // ... and scale with the singular values
    for (int r=0; r<2*RD; ++r)
        for (int s=0; s<2*RD; ++s)
            AE(r,s)*= ss(0,r)*ss(0,s);

    // now we have U as an orthonormal basis and T in between; diagonalize T and rotate U accordingly
    DiagonalizeV(AE,ss[0]);

    // must discard any updates too low to contribute:


    Messenger << "evs of Woodbury update : " << ss << endl;

    TensorProduct(U,AE,B); // rotate

    for (int r=0; r<2*RD; ++r) du[r] = 1./ss(0,r); // these are inverses
    */

}



// for the time being, we know all data is in node 0
// this one is wrong, too, given the 2x2 block structure of the update
void Wprecond (double * RS, double * RA, double * IS, double * IA, const tensor2 & W, const tensor2 & jf, const double * es, const double * ea, double wr, double wi, const int N) {

    // apply diagonal preconditioner
    InverseCPP (RS, RA, IS, IA, es, ea, wr, wi, N);

    // build inner product of symmetric subblocks with the decomposed factors of the J update
    tensor2 rs(N,1,RS), is(N,1,IS), ra(N,1,RA), ia(N,1,IA);
    tensor2 jr, ji;

    TensorProduct (jr, jf, rs); // (M x 1) =  (M x NOV) x (NOVx1)
    TensorProduct (ji, jf, is);

    // now multiply with the Woodbury blocks
    tensor2 vr, vi;

    TensorProduct (vr, W, jr); // (M x 1) = (M x M) x (M x 1)
    TensorProduct (vi, W, ji); // notice this should have opposite sign (from -W)

    // now build the update
    tensor2 rs2(N,1),ra2(N,1),is2(N,1),ia2(N,1);

    TensorProductNTN (rs2,jf, vr); //  (NOV x 1) = (NOV x M) x (M x 1)
    ra2.zeroize();
    TensorProductNTN (is2,jf, vi); // this still has opposite sign; however it is going to be corrected in the preconditioner call
    ia2.zeroize();

    // call the diagonal inverse for the update
    //InverseCPP2 (rs2.c2, ra2.c2, is2.c2, ia2.c2, es, ea, wr, wi, N);
    SDPprecond(rs2.c2, ra2.c2, is2.c2, ia2.c2, es, ea, wr, wi, N);

    // update the solution
    rs -= rs2;
    ra -= ra2;
    is -= is2;
    ia -= ia2;
}

void Wprecond (double * RS, double * RA, double * IS, double * IA, const tensor2 & WE, const tensor2 & WG, const tensor2 & jf, const double * es, const double * ea, double wr, double wi, const int N) {

    // apply diagonal preconditioner
    InverseCPP (RS, RA, IS, IA, es, ea, wr, wi, N);

    // build inner product of symmetric subblocks with the decomposed factors of the J update
    tensor2 rs(N,1,RS), is(N,1,IS), ra(N,1,RA), ia(N,1,IA);
    tensor2 jr, ji;

    TensorProduct (jr, jf, rs); // (M x 1) =  (M x NOV) x (NOVx1)
    TensorProduct (ji, jf, is);

    // now multiply with the Woodbury blocks
    tensor2 vr, vi;

    TensorProduct (vr, WE, jr); // (M x 1) = (M x M) x (M x 1)
    TensorProduct (vi, WE, ji); // notice this should have opposite sign (from -W)
    vi *= -1; // the second block is inverted
    TensorProduct (vi, WG, jr, 1.); // add the cross (imaginary) terms
    TensorProduct (vr, WG, ji, 1.); //

    // now build the update
    tensor2 rs2(N,1),ra2(N,1),is2(N,1),ia2(N,1);

    TensorProductNTN (rs2,jf, vr); //  (NOV x 1) = (NOV x M) x (M x 1)
    ra2.zeroize();
    TensorProductNTN (is2,jf, vi); // this still has opposite sign; however it is going to be corrected in the preconditioner call
    ia2.zeroize();

    // call the diagonal inverse for the update
    InverseCPP (rs2.c2, ra2.c2, is2.c2, ia2.c2, es, ea, wr, wi, N);

    // update the solution
    rs -= rs2;
    ra -= ra2;
    is -= is2;
    ia -= ia2;
}

#include "low/sort.hpp"

void ComputeCondition (
                  const double * es, const double * ea, const double * wr, const double * wi, int NOV, int NK, // preconditioner
                  const tensor2 & BS, const tensor2 & BA, const tensor2 & RS, const tensor2 & RA     // basis set & rhos
                  ) {

    // first compute the diagonals of the original preconditioners:
    tensor2 DS(NK,NOV), DA(NK,NOV), CS(NK,NOV), CA(NK,NOV), AA(NK,NOV), BB(NK,NOV);

    // compute the 4x4 inverse of:
    //  es  -wr    0  wi
    // -wr   ea   wi   0
    //   0   wi  -es  wr
    //  wi    0   wr -ea
    // for each equation k and each element of the diagonal;
    // the ivnerse matrix has the structure:
    //  ds  bb   cs   aa
    //  bb  da   aa   ca
    //  cs  aa  -ds  -bb
    //  aa  ca  -bb  -da
    for (int k=0; k<NK; ++k) {
        for (int n=0; n<NOV; ++n) {

            const double s = es[n];
            const double a = ea[n];
            const double w = wr[k];
            const double g = wi[k];

            // (w^2 + g^2) ^2 + (es*ea)^2 - 2*es*ea*(w^2-g^2) > 0
            double det = (w*w + g*g) * (w*w + g*g) + (s*a) * ( (s*a) - 2*(w+g)*(w-g) );

            double inv = 1./det;

            // diagonal terms: ds, da, -ds, -da

            double dd = s*a + g*g - w*w;

            double ds = a * dd;
            double da = s * dd;

            // antidiagonal term
            double aa = g * (s*a + g*g + w*w);

            // conjugate block-antidiagonal terms
            double cc = 2*g*w;
            double cs = cc*a;
            double ca = cc*s;

            // conjugate block-diagonal term
            double bb = w * (s*a - g*g - w*w);

            // store for later consumption
            DS(k,n) = ds * inv;
            DA(k,n) = da * inv;
            CS(k,n) = cs * inv;
            CA(k,n) = ca * inv;
            AA(k,n) = aa * inv;
            BB(k,n) = bb * inv;

            /*
            // short-hand rhs values
            const double rs =   RS[n];
            const double ra =   RA[n];
            const double is =   IS[n]; // from matrix symmetrization
            const double ia =   IA[n]; //

            // overwrite solution with matrix mult.
            RS[n] = ds*rs + bb*ra + cs*is + aa*ia;;
            RA[n] = bb*rs + da*ra + aa*is + ca*ia;
            IS[n] = cs*rs + aa*ra - ds*is - bb*ia;
            IA[n] = aa*rs + ca*ra - bb*is - da*ia;
            */
        }
    }

    const int RD = BS.n;

    // (E0 |b - |rho>) M^-1
    // where M is the overlap
    // so that we have <b| (E0-wS)^-1 (E-E0) |b> M^-1
    // i.e. project the equations on an orthonormal basis spanned by bs/ba
    tensor2 TS, TA; {

        tensor2 SS, SA, MS,MA;

        SS = BS;
        SA = BA;
        DiagonalScaling(SS, es);
        DiagonalScaling(SA, ea);
        SS -= RS;
        SA -= RA;

        SS *= -1;
        SA *= -1;

        TensorProductNNT(MS, BS, BS);
        TensorProductNNT(MA, BA, BA);
        Invert(MS);
        Invert(MA);

        TensorProduct(TS, MS, SS);
        TensorProduct(TA, MA, SA);
    }




    // now build the projections over <b|  |rho>
    tensor3 DS3(NK,RD,RD), DA3(NK,RD,RD), CS3(NK,RD,RD), CA3(NK,RD,RD), AA3(NK,RD,RD), BB3(NK,RD,RD), AT3(NK,RD,RD), BT3(NK,RD,RD);

    for (int k=0; k<NK; ++k) {
        WeightedInnerProduct (DS3[k], BS, TS, DS[k]);
        WeightedInnerProduct (DA3[k], BA, TA, DA[k]);
        WeightedInnerProduct (CS3[k], BS, TS, CS[k]);
        WeightedInnerProduct (CA3[k], BA, TA, CA[k]);

        WeightedInnerProduct (AA3[k], BS, TA, AA[k]);
        WeightedInnerProduct (BB3[k], BS, TA, BB[k]);
        WeightedInnerProduct (AT3[k], BA, TS, AA[k]);
        WeightedInnerProduct (BT3[k], BA, TS, BB[k]);
    }



    tensor2 M(4*RD);

    // now set up and solve the linear eqs
    for (int k=0; k<NK; ++k) {

        M.zeroize();

        // build the matrix;
        // AA & BB must be transposed in AS blocks


        // also notice the IS/IA diagonal right projectors also change sign

        for (int r=0; r<RD; ++r) {
            for (int s=0; s<RD; ++s) {
                M (0*RD+r,0*RD+s) =  DS3[k](r,s);
                M (0*RD+r,1*RD+s) =  BB3[k](r,s);
                M (0*RD+r,2*RD+s) = -CS3[k](r,s);
                M (0*RD+r,3*RD+s) = -AA3[k](r,s);

                M (1*RD+r,0*RD+s) =  BT3[k](r,s);
                M (1*RD+r,1*RD+s) =  DA3[k](r,s);
                M (1*RD+r,2*RD+s) = -AT3[k](r,s);
                M (1*RD+r,3*RD+s) = -CA3[k](r,s);

                M (2*RD+r,0*RD+s) =  CS3[k](r,s);
                M (2*RD+r,1*RD+s) =  AA3[k](r,s);
                M (2*RD+r,2*RD+s) =  DS3[k](r,s);
                M (2*RD+r,3*RD+s) =  BB3[k](r,s);

                M (3*RD+r,0*RD+s) =  AT3[k](r,s);
                M (3*RD+r,1*RD+s) =  CA3[k](r,s);
                M (3*RD+r,2*RD+s) =  BT3[k](r,s);
                M (3*RD+r,3*RD+s) =  DA3[k](r,s);
            }
        }

        // find the spectrum
        tensor2 m(2,4*RD);

        DiagonalizeNE (M, m[0], m[1]);

        for (int r=0; r<4*RD; ++r) m(0,r) += 1.;

        for (int r=0; r<4*RD; ++r) m(0,r)  = sqrt(m(0,r)*m(0,r)+m(1,r)*m(1,r));

        FlashSort (m[0],4*RD);

        //for (int r=0; r<4*RD; ++r) m(1,r)  = 1./m(0,r);
        //Messenger << "equation " << k << " (E0-wS)^-1 (E-E0) spectrum; |z| & |z|^-1 : " << endl << m << endl;
        Messenger << "frequency " << k << " : |v_min|, |v_max|, condition = " << m[0][0] << " " << m[0][4*RD-1] << "  " << m[0][4*RD-1]/m[0][0] << endl;
    }


}



// data structure for storing response vectors in parallel
struct multivec {

    tensor2 V;


    uint64_t nov; // (total) dimension of array
    uint64_t no, nv;

    uint64_t d; // dimension of array per node == V.m
    uint64_t l; // (local) dimension of array <= d


    multivec() {
        nov = d = l = no = nv = 0;
    }

    // get subrange; start with vector k and end in k+m
    void setsize (multivec & M, uint64_t k, uint64_t m) {
        // sanity check
        //if ((k<0) || (k+m>M.V.n))  throw 120;

        nov = M.nov;
        no = M.no;
        nv = M.nv;
        d = M.d;
        l = M.l;

        V.setsize (m, d, M.V[k]); // make
    }

    void setsize (uint64_t k, uint64_t o, uint64_t v) {

        int N = NodeGroup.GetTotalNodes();
        int R = NodeGroup.GetRank();

        nov = o*v;
        no = o;
        nv = v;
        d = 1 + (nov-1)/N; // ceil(n / NN)
        l = d;

        if (R+1==N) l = nov - R * d; // remainder

        V.setsize(k,d);
        V.zeroize();
    }

    double * setsize (uint64_t k, uint64_t o, uint64_t v, double * p) {

        int N = NodeGroup.GetTotalNodes();
        int R = NodeGroup.GetRank();

        nov = o*v;
        no = o;
        nv = v;
        d = 1 + (nov-1)/N; // ceil(n / NN)
        l = d;

        // for last node:
        if (R+1==N) l = nov - R * d; // remainder

        V.setsize(k,d,  p);

        return p + k*d;
    }

    // inner product: S * A
    // should implement a move constructor
    tensor2 operator*(const multivec & rhs) const {

        tensor2 S;

        TensorProductNNT(S, V, rhs.V);
        Tensor2Reduce(S);

        return S;
    }


    // gather all data (T should already be formatted)
    void get (tensor2 & T, int k) {
        // sanity check
        if ((T.n!=no) || (T.m!=nv)) throw 122;
        if ((k<0)     || (k>=V.n))  throw 123;


        NodeGroup.AllGather (V[k], l, T[0], nov);
    }

    // copy a subvector to memory
    void set (int k, const tensor2 & T) {
        // sanity check
        if ((T.n!=no) || (T.m!=nv)) throw 124;
        if ((k<0)     || (k>=V.n))  throw 125;

        int N = NodeGroup.GetTotalNodes();
        int R = NodeGroup.GetRank();

        // copy subarray to memory
        const double * t = *T + R*d;
        for (uint64_t i=0; i<l; ++i) V[k][i] = t[i];
        for (uint64_t i=l; i<d; ++i) V[k][i] = 0;
    }

    // multiply-sub
    multivec & msub (const tensor2 & S, const multivec & M) {

        TensorProduct (V, S, M.V, -1.);

        return *this;
    }

    // multiply-sub
    multivec & madd (const tensor2 & S, const multivec & M) {

        TensorProduct (V, S, M.V, +1.);

        return *this;
    }

    // matrix-matrix multiply
    multivec &  mul (const tensor2 & S, const multivec & M) {

        TensorProduct (V, S, M.V);

        return *this;
    }

    // in-place rotation or LC of the vectors
    // assumes the physical dimension of V cointains the data despite having a different logical dimension
    // it also may change the (logical) dimension of V depending on R
    multivec &  rot (const tensor2 & R) {

        V.n = R.m; // set logical dimension to match the columns of R;

        tensor2 M;
        TensorProduct (M, R, V);

        // set logical dimension to to M.n to avoid calling the destructor on V during the copy
        // and preserve the rest of the vectors in the leftover buffer
        V.n = M.n;
        V   = M;

        return *this;
    }

    // element-wise addition a[m] += b*c[m]
    multivec &  fma (const multivec & W, const multivec & rhs) {

        for (int n=0; n<V.n; ++n)
            for (int m=0; m<l; ++m)
                V[n][m] += W.V[0][m] * rhs.V[n][m];

        return * this;
    }

    // element-wise addition a[m] += b*c[m]
    multivec &  fma (double s, const multivec & rhs) {

        V.FMA(rhs.V,s);

        return * this;
    }

    // write the norms to the array n2[]
    void norm (double * n2) const {

        // use a proper reducing scheme

        for (int n=0; n<V.n; ++n) {
            double s2=0;
            for (int m=0; m<l; ++m) s2 += V[n][m] * V[n][m];
            n2[n] = s2;
        }

        NodeGroup.AllReduce(n2, V.n);
    }

    // write the norms wrt the weights W[0] to the array n2[]
    void norm (double * n2,  const multivec & W) const {

        // use a proper reducing scheme

        for (int n=0; n<V.n; ++n) {
            double s2=0;
            for (int m=0; m<l; ++m) s2 += V[n][m] * V[n][m] * W.V[0][m];
            n2[n] = s2;
        }

        NodeGroup.AllReduce(n2, V.n);
    }

    tensor2 norm() const {

        tensor2 S(1,V.n);

        norm(S[0]);

        return S;
    }

    // vector-by-vector inner product
    void inner (double * d,  const multivec & rhs) const {

        // use a proper reducing schene

        for (int n=0; n<V.n; ++n) {
            double s2=0;
            for (int m=0; m<l; ++m) s2 += V[n][m] * rhs.V[n][m];
            d[n] = s2;
        }

        NodeGroup.AllReduce(d, V.n);
    }

    tensor2 inner(const multivec & diag) const {

        tensor2 W;
        WeightedInnerProduct (W,V,diag.V[0]);

        Tensor2Reduce(W);

        return W;
    }

    tensor2 inner(const multivec & diag, const multivec & outer) const {

        tensor2 W, U;
        WeightedInnerProduct (W, V,diag.V[0]); // inner product wrt
        Tensor2Reduce(W);

        // if there is a rank > 0 update, add it
        if (outer.V.n>0) {
            TensorProductNNT     (U, V, outer.V);
            Tensor2Reduce(U);
            TensorProductNNT(W,U,U,1.); // add the outer block update
        }

        return W;
    }

    tensor2 inner() const {
        return (*this)*(*this);
    }

    // inner product with another set and wrt a diagonal metric
    tensor2 inner2(const multivec & rhs, const multivec & diag) const {

        tensor2 W;

        WeightedInnerProduct (W, V,rhs.V,diag.V[0]);

        Tensor2Reduce(W);

        return W;
    }


    // scale each vector n with s[n]
    multivec & scale (const double * s) {

        // use dscal
        for (int n=0; n<V.n; ++n)
            for (int m=0; m<l; ++m)
                V[n][m] *= s[n];

        return *this;
    }


    multivec & operator=  (const multivec & rhs) {

        if ((V.n!=rhs.V.n) || (V.m!=rhs.V.m)) return *this; // should give error

        V = rhs.V;

        return *this;
    }

    multivec & operator+= (const multivec & rhs) {

        if ((V.n!=rhs.V.n) || (V.m!=rhs.V.m)) return *this; // should give error

        V += rhs.V;

        return *this;
    }

    multivec & operator-= (const multivec & rhs) {

        if ((V.n!=rhs.V.n) || (V.m!=rhs.V.m)) return *this; // should give error

        V -= rhs.V;

        return *this;
    }

    multivec & operator*= (double s) {

        V *= s;

        return *this;
    }

};

// solves the GEV system
// not fully tested for subspaces with different dimension
void SolvePaired (tensor2 & RS, tensor2 & RA, double * EE, const tensor2 & ES, const tensor2 & EA, const tensor2 & AS, const int RD, double invEcut = 0.) {

    const int N1 = ES.n;
    const int N2 = EA.n;

    const int NN = max(N1,N2);


    RS.setsize(ES.n);
    RA.setsize(EA.n);
    RS.zeroize();
    RA.zeroize();


    tensor2 L1, L2;

    L1 = ES;
    L2 = EA;




    // usual implementation; note this is only executed by master
    {

        tensor2 SS(N1,N2),  TT;
        tensor2 U(N1), V(N2);

        // Cholesky pivots
        int p1[N1], p2[N2];


        {
            const double tol = 1.e-40;

            int R1 = Cholesky(L1, p1, tol);
            int R2 = Cholesky(L2, p2, tol);

            if ( (R1<N1) || (R2<N2) ) Messenger << "CD ranks = " << R1 << " " << R2 << "    " << N1 << " " << N2 << endl;
        }



        {
            // better implementation
            if (1) {
                // invert the CDs
                InvChol(L1);
                InvChol(L2);

                for (int m=0; m<N2; ++m)
                    for (int n=0; n<N1; ++n)
                        SS(m,n) = AS(p2[m],p1[n]);

                TensorProductNTN  (TT, L2, SS);   // La S^-1
                TensorProduct     (SS, TT, L1);  // La S^-1 Ls
            }

            // old one; tested
            else {

                for (int m=0; m<N2; ++m)
                    for (int n=0; n<N1; ++n)
                        SS(m,n) = AS(p2[m],p1[n]);

                //SS = SA;
                PseudoInverse(SS);

                TensorProduct    (TT, L2, SS);   // La S^-1
                TensorProductNNT (SS, TT, L1);  // La S^-1 Ls

                // invert the CDs
                InvChol(L1);
                InvChol(L2);

                PseudoInverse(SS);
            }


            // find the SVD
            SVD (SS, V, U, EE);
            V.Transpose();

            for (int m=0; m<N1; ++m) EE[m] = 1./EE[m]; // energies come from procedure inverted


            {
                double sqe[NN];

                for (int m=0; m<NN; ++m) sqe[m] = sqrt(EE[m]);


                TensorProductNNT (SS,U,L1);
                DiagonalScaling  (sqe, SS);

                for (int m=0; m<N2; ++m)
                    for (int n=0; n<N1; ++n)
                        RS(m,p1[n]) = SS(m,n);

                TensorProductNNT (SS,V,L2);
                DiagonalScaling  (sqe, SS);

                for (int m=0; m<N2; ++m)
                    for (int n=0; n<N1; ++n)
                        RA(m,p2[n]) = SS(m,n);
            }

            // cutoff too low inverse energies
            //int R; for (R=0; R<RD; ++R) if (EE[R]<invEcut) break;
            //RS.n = RA.n = R;
        }

    }


}

// only find eigenvalues of GEV system
void SolvePaired (double * EE, const tensor2 & ES, const tensor2 & EA, const tensor2 & AS) {

    const int N1 = ES.n;
    const int N2 = EA.n;

    const int NN = max(N1,N2);

    tensor2 L1, L2;

    L1 = ES;
    L2 = EA;


    // Cholesky pivots
    int p1[N1], p2[N2];


    const double tol = 1.e-40;

    const int R1 = Cholesky(L1, p1, tol);
    const int R2 = Cholesky(L2, p2, tol);

    if ( (R1<N1) || (R2<N2) ) Messenger << "CD ranks = " << R1 << " " << R2 << "    " << N1 << " " << N2 << endl;

    //if (R1<N1) cout << L1 << endl; if (R2<N2) cout << L2 << endl;
    //for (int i=0; i<N1; ++i) Messenger << p1[i] << " "; Messenger << endl; for (int i=0; i<N2; ++i) Messenger << p2[i] << " "; Messenger << endl;

    tensor2 SS(R2,R1),  TT, I1(R1), I2(R2);

    // copy only the entries we are interested in
    for (int m=0; m<R1; ++m)
        for (int n=0; n<R1; ++n)
            I1(m,n) = L1(m,n);

    for (int m=0; m<R2; ++m)
        for (int n=0; n<R2; ++n)
            I2(m,n) = L2(m,n);


    // invert the CDs
    InvChol(I1);
    InvChol(I2);


    for (int m=0; m<R2; ++m)
        for (int n=0; n<R1; ++n)
            SS(m,n) = AS(p2[m],p1[n]);

    TensorProductNTN  (TT, I2, SS);   // La S^-1
    TensorProduct     (SS, TT, I1);  // La S^-1 Ls


    // find the PCs
    PCA(SS, EE);
    //for (int i=0; i<min(R1,R2); ++i) Messenger << EE[i] << " "; Messenger << endl;

    for (int m=0; m<min(R1,R2); ++m) EE[m] = 1./EE[m]; // energies come from procedure inverted
}




int SolvePaired (multivec & ZS, multivec & ZA, const multivec & pcs, const multivec & pca, const int NNW, const double Ethresh=100.) {

    const double thresh = 1.e-10; // 1.e-13;


    const int nO = pcs.no;
    const int nV = pcs.nv;
    const int nOV = nO*nV;



    tensor2 es(1,nOV),ea(1,nOV);

    // perform an SVD of the half-inner product to preserve numerical precision
    // (compared to building the inner product and diagonalizing)
    {
        for (int ia=0; ia<nOV; ++ia)
            es[0][ia] = sqrt(pcs.V[0][ia]);
        for (int ia=0; ia<nOV; ++ia)
            ea[0][ia] = sqrt(pca.V[0][ia]);

        tensor2 VV(NNW, nOV);
        tensor2 U(NNW);

        for (int nw=0; nw<NNW; ++nw)
            for (int ia=0; ia<nOV; ++ia)
                VV(nw,ia) = ZS.V(nw,ia) * es(0,ia);

        SVD (VV,U,es[0]);
        U.Transpose();
        ZS.rot(U);

        for (int nw=0; nw<NNW; ++nw)
            for (int ia=0; ia<nOV; ++ia)
                VV(nw,ia) = ZA.V(nw,ia) * ea(0,ia);

        SVD (VV,U,ea[0]);
        U.Transpose();
        ZA.rot(U);
    }


    // find an appropriate dimension (discard linear dependencies)
    int N; {
        for (N=0; N<NNW; ++N)
            if ( ( es[0][N] < thresh*es[0][0]) || ( ea[0][N] < thresh*ea[0][0]) ) break;

        es.m = ea.m = ZS.V.n = ZA.V.n = N; // mask useless vecs
        Messenger << "N = "<< N << " / " << NNW << endl;
    }


    for (int n=0; n<N; ++n) es[0][n] = 1./es[0][n];
    for (int n=0; n<N; ++n) ea[0][n] = 1./ea[0][n];

    ZS.scale(es[0]);
    ZA.scale(ea[0]);

    /*
    {
        tensor2 ES, EA;

        ES = ZS.inner(pcs); // these should be diagonal
        EA = ZA.inner(pca); //

        Messenger << "ES :" << endl << ES << endl;
        Messenger << "EA :" << endl << EA << endl;

        {
            tensor2 ee(1,N);
            DiagonalizeE(ES,ee[0],true);
            Messenger << "eigen s " << ee << endl;
            DiagonalizeE(EA,ee[0],true);
            Messenger << "eigen a " << ee << endl;
        }
    }
    */

    // solve GEV system
    {
        tensor2 SA;
        tensor2 UT(N), VT(N), ss(1,N);

        SA = ZS * ZA; // inner prod.

        //Messenger << "SA :" << endl << SA << endl;

        SVD (SA, UT, VT, ss[0]);
        UT.Transpose();

        // maybe it's the other way around
        ZS.rot(UT);
        ZA.rot(VT);

        Messenger << "svd  " << ss << endl; // inverse energies

        for (int n=0; n<N; ++n) ss[0][n] = 1./(ss[0][n]);
        Messenger << "energies :  " << ss << endl;

        // now we must check for (quasi) linear dependencies:
        const int M=N;
        for (N=0; N<M; ++N)
            if ( ss[0][N] > Ethresh) break;

        for (int n=0; n<N; ++n) ss[0][n] = sqrt(ss[0][n]);
        Messenger << "N = " << N << endl;


        ss.m = ZS.V.n = ZA.V.n = N; // mask useless vecs
        ZS.scale(ss[0]);
        ZA.scale(ss[0]);
    }


    /*
    {
        tensor2 ES, EA;

        ES = ZS.inner(pcs);
        EA = ZA.inner(pca);

        tensor2 ee(2,NNW);
        DiagonalizeE(ES, ee[0],true);
        DiagonalizeE(EA, ee[1],true);

        Messenger.precision(15);
        Messenger << "eigenvalues ES. EA : " << endl << ee << endl;
        Messenger.precision(7);
    }
    {
        tensor2 SA;
        SA = ZS * ZA; // inner prod.
        Messenger << "SA :" << endl << SA << endl;
    }

    {
        tensor2 AS;
        AS = ZA * ZS; // inner prod.
        Messenger << "AS :" << endl << AS << endl;
    }
    */



    // restore correct dimensions
    ZS.V.n = ZA.V.n = NNW;

    return N;
}



void FinalPreconditioner(
        const double * es, const double * ea, const double * wr, const double * wi, int NOV, int NK,
        const multivec & bs, const multivec & ba, const multivec & rs, const multivec & ra, const double * EE, int RD,
        multivec & ZS, multivec & ZA
        ) {

        tensor2 US, UA, dus(1,2*RD), dua(1,2*RD);

        const tensor2 RS(RD, rs.V, 0);
        const tensor2 RA(RD, ra.V, 0);
        const tensor2 BS(RD, bs.V, 0);
        const tensor2 BA(RD, ba.V, 0);

        ComputeUpdate (US,dus[0],RS, BS, EE, es, RD, NOV);
        ComputeUpdate (UA,dua[0],RA, BA, EE, ea, RD, NOV);

        BlockUpdate (es,ea,wr,wi,NOV,NK,
                     US,UA,dus[0],dua[0], RD, ZS.V, ZA.V);
}



int CheckLinear (multivec & Z, const double athresh, const double rthresh, const multivec * inner=NULL, const multivec * outer=NULL)  {

    // first look for linear dependency of some kind
    const int NW = Z.V.n;
    const int OV = Z.nov;

    tensor2 U(NW),u(1,NW);

    if      (inner==NULL) U = Z.inner();
    else if (outer==NULL) U = Z.inner(*inner);
    else                  U = Z.inner(*inner, *outer);

    DiagonalizeV (U,u[0],true); // sort eigenvalues in decreasing order

    for (int nw=0; nw<NW; ++nw) u[0][nw] = sqrt(max(u[0][nw],0.));

    U.Transpose();

    // this was done before with an SVD to increase
    // accuracy for very degenerate basis, which occur frequently
    // however SVD is much more expensive and not scalable for large, parallel calculations
    /*
    {
        tensor2 & ES = US;
        ES = ZS.inner(pcs,pcf);
        DiagonalizeV (ES, es[0], true);
        for (int nw=0; nw<NNW; ++nw) es[0][nw] = sqrt(es[0][nw]);

        US.Transpose();
    }
    {
        for (int ia=0; ia<nOV; ++ia)
            fa[0][ia] = sqrt(pca.V[0][ia]);

        tensor2 VV(NNW, nOV);
        tensor2 & U = UA;

        for (int nw=0; nw<NNW; ++nw)
            for (int ia=0; ia<nOV; ++ia)
                VV(nw,ia) = ZA.V(nw,ia) * fa(0,ia);

        SVD (VV,U,ea[0]);
        U.Transpose();
    }
    */



    // now select the principal components and rotate the space accordingly
    const double sthr = max(athresh, rthresh*u[0][0]);

    int N;

    for (N=1; N<NW; ++N)
        if ( (u[0][N] < sthr)) break;

    u.m = U.n = N;
    Messenger << "max dim = "<< N << " / " << NW << endl;

    Messenger << " SVD of basis: " << u << endl;

    Z.rot(U);

    return N;
}

int Biorthonormalize (tensor2 & U, tensor2 & V, const multivec & ZS, const multivec & ZA, const double athresh, const double rthresh=1.) {

    const int NS = ZS.V.n;
    const int NA = ZA.V.n;
    const int NRW = min(NS,NA);

    // test HOSVD
    /*
    if (0) {
        tensor3 TS (NRW, nO, nV, ZS.V[0]);
        tensor3 TA (NRW, nO, nV, ZA.V[0]);

        HOSVD(TS);
        HOSVD(TA);
    }
    */

    // make inner product
    tensor2 SS;

    SS = ZS * ZA; //

    // do the SVD
    U.setsize(NS);
    V.setsize(NA);

    tensor2 ss(1,NRW);
    ss.zeroize();

    int NRD;


    if (NodeGroup.IsMaster()) {

        SVD (SS, U, V, ss[0]);
        U.Transpose();

        // select optimal trial vector linear combinations
        // and discard the ones that contributed the least to the
        // Frobenius norm ^2 of the matrix
        {
            double tr2 = ss.frobenius();

            const double thresh = max (athresh*athresh, tr2*rthresh*rthresh);

            /*
            double ts2 =0;

            for (NRD=0; NRD<NRW;) {
                if (ss[0][NRD]>0) {
                    ts2 += ss[0][NRD]*ss[0][NRD];
                    ++NRD;
                    if ( (tr2-ts2) < thresh) // Sthresh*tr2
                        break;
                }
                else break;
            }
            */

            double ts2=0;

            for (NRD=NRW; NRD>0; --NRD) {
                ts2 += ss[0][NRD-1]*ss[0][NRD-1];
                if ( ts2 >= thresh)
                    break;
            }

            Messenger << "biorthogonal single values: " << ss << endl;
        }

        Messenger << "  selected " << NRD << " trial pairs" << endl;

        // split the binormalization between S/A
        for (int n=0; n<NRD; ++n) ss[0][n] = (ss[0][n]>0)?1./sqrt(ss[0][n]):0.;

        U.n = V.n = NRD; // only use the relevant ones

        DiagonalScaling (ss[0], U);
        DiagonalScaling (ss[0], V);
    }

    // share results with all other processes
    {
        //NodeGroup.Broadcast(ss[0], NRW);

        NodeGroup.Broadcast(NRD);

        Tensor2Broadcast(U);
        Tensor2Broadcast(V);
    }

    return NRD;
}



// ================================================================
// for EV problems, check ELOBP4dCG. doi: 10.1007/s11425-016-0297-1
// ================================================================

// on input, RR contains the right hand sides (gradients), D0 contains the converged density
// on output, ZZ contains the solutions
// order of vectors is : RS, IS, RA, IA
void MolElData::LinearMultiRSP (multivec * ZZ, const multivec * RR, const double * wr, const double * wi, const int NW, const int NR,   const tensor2 & D0) {


    // pointers to ERI/DFT modules
    Echidna * efs = &EchidnaSolver;
    DFTgrid * dft = &DFT;


    // number of equations, frequencies
    bool DoCPP, DoRA, DoIA, DoIS;

    multivec ZS, ZA; // pointers to R+I subspaces, necessary for building common S, A subspaces after preconditiong

    switch (RSP.mode) {

        case RSPparams::RSPmode::REAL:
            DoCPP = false;
            DoRA = true;
            DoIA = DoIS = false;
            ZS.setsize(ZZ[0], 0, NW);
            ZA.setsize(ZZ[2], 0, NW);
            break;

        case RSPparams::RSPmode::IMAG:
            DoCPP = false;
            DoIA = true;
            DoRA = DoIS = false;
            ZS.setsize(ZZ[0], 0, NW);
            ZA.setsize(ZZ[3], 0, NW);
            break;

        case RSPparams::RSPmode::CPP :
            DoCPP = true;
            DoRA = DoIA = DoIS = true;
            ZS.setsize(ZZ[0], 0, 2*NW);
            ZA.setsize(ZZ[2], 0, 2*NW);
            break;

        // this should be an error
        case RSPparams::RSPmode::NONE:
            return;

    }

    // dimensions, number of orbitals, etc.
    const int nN = (RSP.NA>0)? min(RSP.NA, int(C.n)) : C.n + RSP.NA;    // NMO
    const int nA = C.m;    // NAO
    const int nO = npairs;
    const int nV = nN - nO;
    const int nOV = nO*nV;
    const int lOV = RR[0].l; // the number of entries per node ; this should be the VALID length of the array or else we may get division by zero

    const bool UseCholesky = (RSP.CDthresh>0.);

    // subspaces of occupied and virtual orbitals
    tensor2 CO (nO,nA,   C[0]),  CV (nV,nA,   C[nO]);


    // all this should be in a class, SolverParameters or something
    const double Dthresh = RSP.Dthresh; // CS Fock threshold
    const double Cthresh = RSP.Cthresh; // convergence threshold
    const double Sthresh = RSP.Sthresh; // SVD threshold

    const int NT  = RSP.NT;             // maximum number of cycles
    const int MNW = (DoCPP?2:1)*2*NW;   // maximum allowed number of simultaneous equations


    // pseudodensity and pseudofock matrices
    tensor2 * DD = new tensor2[2*MNW]; // should be at most 2*NW symmetric and 2*NW antisymmetric
    tensor2 * FF = new tensor2[3*MNW];


    // print dimensions and frequencies in the header
    {
        Messenger << "NW = " << NW << "  MNW = " << MNW << "    &RR = " << RR << endl;

        Messenger.precision(7);
        Messenger.width(3);

        Messenger << "   wr = ";
        for (int nw=0; nw<NW; ++nw) Messenger << wr[nw];
        Messenger << endl;
        Messenger << "   wi = ";
        for (int nw=0; nw<NW; ++nw) Messenger << wi[nw];
        Messenger << "  Ntrial" << endl;
    }

    int nt  = 0;           // cycle (iteration)
    int RD  = 0;           // total dimension of reduced space

    int MRD; {

        int NV; // number of vecs per iteration

        NV = (DoCPP?2:1) * NW;                // overkill, considering the large amount of linear dependence
        if (RSP.MV>0) NV = min (NV, RSP.MV);  // if there is a maimum number of vectors per iteration set

        // set default
        MRD = NT * NV;  // maximum RS dimension (realloc if needed)

        if (RSP.MD>0) MRD = min(MRD, RSP.MD); // do not surpass the maximum allowed number

    }


    // trial & fock vectors; contiguous memory allocation
    multivec bs, ba, rhos, rhoa;

    bs.setsize    (MRD, nO,nV);
    ba.setsize    (MRD, nO,nV);
    rhos.setsize  (MRD, nO,nV);
    rhoa.setsize  (MRD, nO,nV);


    // reduced subspaces
    double  EE[MRD], G1s[MRD], G1a[MRD];
    tensor2 Xrs, Xra, Xis, Xia; // coefficients


    // if using cholesky / SVD, transform to MO basis
    if (UseCholesky) {
        //Messenger.Push();

        efs->CDtransform(C);

        //Messenger.Pop();
    }

    bool TestHOSVD = true && UseCholesky;

    bool UseWoodbury = false;

    UseWoodbury &= UseCholesky;

    // diagonal energies; preconditioner
    multivec ae, pcs, pca, pcf;
    tensor2 * WE = NULL, * WG = NULL;
    {
        ae.setsize  (1,nO,nV);
        pcs.setsize (1,nO,nV);
        pca.setsize (1,nO,nV);

        tensor2 AE(nO,nV); {
            // molecular electron data consts
            const double * en = SCF.GetEnergies();

            {
                for (int i=0; i<nO; ++i)
                    for (int a=0; a<nV; ++a)
                        AE(i,a) = en[nO+a]-en[i];
            }

            //Messenger << "diagonal energies" << endl <<  AE << endl;
        }
        // distribute
        ae.set(0, AE);


        // test diagonal energies in MO
        if (UseCholesky && !TestHOSVD) {

            // the diagonal E - K preconditioner seems to be a significant improvement for HF RSP
            // however trying to add extra K contributions is costly and unpractical for two reasons:
            // 1) the (ii|ab) & (ij|aa) matrices do not admit a low rank decomposition
            // 2) using full block-matrices makes the Woodbury formula prohibitively expensive
            //
            // on the other hand it looks like adding a few CD vectors to get a low order approximation
            // to (ai|bj) is simple (using Woodbury) and potentially much more effective than only correcting
            // the diagonal, given that the individual contributions are smaller
            //
            // to do that we must precompute a


            // test for the rank of K
            if (0) {
                tensor2 KK (nN);
                tensor2 KA (nV);
                tensor2 kk (nO, nV+1);
                kk.zeroize();

                for (int i=0; i<nO; ++i) {
                    efs->GetIJNN (KK, i);

                    // copy only the active^2 subblock
                    for (int a=0; a<nV; ++a)
                        for (int b=0; b<nV; ++b)
                            KA(a,b) = KK(nO+a,nO+b);

                    DiagonalizeE(KA, kk[i]);
                }

                // how the hell are we getting negative eigenvalues????
                // deosn't make any sense
                Messenger << "Eigenvalues of (ii|ab)" << endl << kk << endl;
            }

            tensor2 CC (nN, nA, C[0]);

            tensor2 AJ(nO,nV); {

                tensor2 JJ(nN);
                efs->GetIJIJ (JJ);

                // scale & copy relevant submatrix
                for (int i=0; i<nO; ++i)
                    for (int a=0; a<nV; ++a)
                        AJ(i,a) = JJ(i, nO+a);

                Messenger << "(ia|ia) energies from Cholesky; " << endl << AJ << endl;
            }

            tensor2 AK(nO,nV); {

                tensor2 KK(nN);
                efs->GetIIJJ (KK);

                // scale & copy relevant submatrix
                for (int i=0; i<nO; ++i)
                    for (int a=0; a<nV; ++a)
                        AK(i,a) = KK(i, nO+a);
                Messenger << "(ii|aa) energies from Cholesky; " << endl << AK << endl;
            }



            // S = (AE + 2J - K1) + (2J - K2) = AE + 4J - K1 - K2
            // A = (AE + 2J - K1) - (2J - K2) = AE      - K1 + K2
            // K1 =     AK = (ii|aa)
            // K2 = J = AJ = (ia|ia)
            {
                tensor2 DS, DA;

                DS = AE; DS -= AK; DS -= AJ; // subtle bug fixed
                DA = AE; DA -= AK; DA += AJ;

                //AJ *= 4; DS += AJ; // seems to work better without it
                //Messenger << "diagonal 4J" << endl << AJ << endl;

                // if E0 is non-SDP, it cannot be used as metric later
                {
                    for (int i=0; i<nO; ++i)
                        for (int a=0; a<nV; ++a)
                            if (DS(i,a)<=0.)
                                Messenger << "negative element in E0S! " << i << ":" << a << "   " << DS(i,a) << endl;
                    for (int i=0; i<nO; ++i)
                        for (int a=0; a<nV; ++a)
                            if (DS(i,a)<=0.)
                                Messenger << "negative element in E0A! " << i << ":" << a << "   " << DA(i,a) << endl;
                }

                pcs.set(0, DS);
                pca.set(0, DA);
            }

        }
        else {
            pcs.set(0, AE);
            pca.set(0, AE);
        }
    }


    if (UseWoodbury) {
        // low-rank decomposition of (ai|bj)
        {
            int M = 512;
            double thresh = 1e-4;

            tensor2 * V = new tensor2[M];

            efs->GetCholesky(V,nO,nV, M, thresh);

            Messenger << "Rank of (ai|bj) approximation = " << M << endl;

            for (int m=0; m<M; ++m) V[m] *= 2; // we need 4J, so we split the factor over the PCs

            if (0) {
                tensor2 VV(nO,nV);
                VV.zeroize();

                for (int m=0; m<M; ++m) {
                    for (int ia=0; ia<nOV; ++ia)
                        VV[0][ia] += V[m][0][ia]*V[m][0][ia];
                }

                Messenger << "diagonal 4J" << endl << VV << endl;
            }


            pcf.setsize (M,nO,nV);

            for (int m=0; m<M; ++m) pcf.set(m,V[m]);

            delete[] V;
        }

        // build the Woodbury blocks
        {
            Messenger.Push("computing Woodbury blocks for preconditioning");

            WE = new tensor2[2*NW];
            WG = WE+NW;

            const double * es = pcs.V[0];
            const double * ea = pca.V[0];

            // it would be more efficient if we packed the frequencies and used
            // only one
            for (int nw=0; nw<NW/NR; ++nw) WoodburyBlocks (WE[nw], WG[nw], pcf.V, es, ea, wr[nw], wi[nw], lOV);

            // this is an ugly hack, but will do until we change the order of the equations
            for (int nr=1; nr<NR; ++nr) {
                for (int nw=0; nw<NW/NR; ++nw) {
                    WE[nr*(NW/NR) + nw] = WE[nw];
                    WG[nr*(NW/NR) + nw] = WG[nw];
                }
            }

            Messenger.Pop();
        }
    }


    // set initial residuals to full rhs;
    // use symmetrization scheme for CPP
              ZZ[0] -= RR[0];
    if (DoIS) ZZ[1] += RR[1];
    if (DoRA) ZZ[2] -= RR[2];
    if (DoIA) ZZ[3] += RR[3];

    tensor2 & ZRS = ZZ[0].V;
    tensor2 & ZIS = ZZ[1].V;
    tensor2 & ZRA = ZZ[2].V;
    tensor2 & ZIA = ZZ[3].V;


    // copy of residuals
    multivec RRC[2];
    RRC[0].setsize(2*NW, nO, nV);
    RRC[1].setsize(2*NW, nO, nV);


    // gradient norms
    tensor2 gg(1,NW); {

        tensor2 N2(4,NW);

                  RR[0].norm(N2[0]);
        if (DoIS) RR[1].norm(N2[1]);
        if (DoRA) RR[2].norm(N2[2]);
        if (DoIA) RR[3].norm(N2[3]);

        gg.zeroize();

                  gg += tensor2(1,NW, N2[0]);
        if (DoIS) gg += tensor2(1,NW, N2[1]);
        if (DoRA) gg += tensor2(1,NW, N2[2]);
        if (DoIA) gg += tensor2(1,NW, N2[3]);

        for (int nw=0; nw<NW; ++nw) gg[0][nw] = std::sqrt(gg[0][nw]);
    }

    const double Emax       = 5000.; // maximum AE to include (discard very high energies, which are usually the result of numerical instability)


    // pointers to existing subspace vectors
    multivec ors, ora, obs, oba;

    // solve iteratively
    while (true) {

        // handles for new trial vecs & their transforms
        multivec nbs,nba, nrs, nra;


        //    PRECONDITION THE RESIDUALS
        //    PROJECT OLD TRIAL VECTORS FROM NEW ONES
        //    FIND PRINCIPAL COMPONENTS OF RESIDUALS
        //    BALANCE SYMMETRIC & ANTISYMMETRIC COMPONENTS

        int NRD; // number of new trial vector pairs
        int NNW; // initial number of new trial vectors per symmetry
        {

            const double * ee  = ae.V[0]; // ae.V[0];

            const double * es = pcs.V[0]; // ae.V[0];
            const double * ea = pca.V[0]; // ae.V[0];

            Messenger.Push("preconditioning trial vectors");


            // obtain trial vectors from preconditioning
            // the residual; pair the vectors

            switch (RSP.mode) {

                case RSPparams::RSPmode::CPP :

                    // copy the residuals before overwritting ZZ
                    RRC[0] = ZS;
                    RRC[1] = ZA;

                    for (int nw=0; nw<NW; ++nw)

                        if      (UseWoodbury) Wprecond   (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], WE[nw], WG[nw], pcf.V, es, ea, wr[nw], wi[nw], lOV);
                        else if (UseCholesky) InverseCPP (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], es, ea, wr[nw], wi[nw], lOV);
                        else                  InverseCPP (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], ee,     wr[nw], wi[nw], lOV);

                        /*
                        if (UseCholesky) //SDPprecond (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], es, ea, wr[nw], wi[nw], lOV); // this is incompatible with AE residual trick
                                         //InverseCPP (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], es, ea, wr[nw], wi[nw], lOV);
                                         //SDPprecond (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw],                    ee, wr[nw], wi[nw], lOV);
                                         Wprecond (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], WE[nw], WG[nw], pcf.V, es, ea, wr[nw], wi[nw], lOV);

                        else             //SDPprecond (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw],     ee, wr[NW-1], wi[nw], lOV);
                                         //SDPprecond (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw],     ee, wr[nw], wi[nw], lOV);
                                         InverseCPP (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw], ee,     wr[nw], wi[nw], lOV);
                                         //SDPprecond (ZRS[nw], ZRA[nw], ZIS[nw], ZIA[nw],     ee, wr[nw], wi[nw], lOV, 0.05);
                        */

                    NNW = 2*NW; // use R/I when building the trial vectors
                    break;

                case RSPparams::RSPmode::REAL:
                    for (int nw=0; nw<NW; ++nw)
                        InverseReal (ZRS[nw], ZRA[nw], ee, wr[nw], lOV);
                    NNW = NW;
                    break;

                case RSPparams::RSPmode::IMAG:
                    for (int nw=0; nw<NW; ++nw)
                        InverseImag (ZRS[nw], ZIA[nw], ee, wi[nw], lOV);
                    NNW = NW;
                    break;
            }

            Messenger.Pop();


            // quick test the HOSVD
            if (TestHOSVD) {
                const double CPthresh = 1.e-4;

                tensor3 TS, TA;
                TS.Set(NNW, nO,nV, ZS.V[0]);
                TA.Set(NNW, nO,nV, ZA.V[0]);
                //HOSVD(TS);

                tensor2 US,VS, UA,VA;
                CPdecomp(US,VS,  TS,CPthresh);
                CPdecomp(UA,VA,  TA,CPthresh);

                const int NRS = US.n;
                const int NRA = UA.n;

                ZS.V.n = NRS;
                ZA.V.n = NRA;

                Messenger << ZS.V.n << " "  << ZS.V.m << "   "  << US.n << " " << VS.n << " " << US.m*VS.m << endl;
                Messenger << ZA.V.n << " "  << ZA.V.m << "   "  << UA.n << " " << VA.n << " " << UA.m*VA.m << endl;

                KhatriRao(ZS.V, US,VS);
                KhatriRao(ZA.V, UA,VA);

                NRD = min (NRS,NRA);



                // sanity check (memory, new subspace dimension, etc.)
                {
                    if (RSP.MV>0)   NRD = min(NRD, RSP.MV); // do not surpass the maximum number of vectors
                    if (RD+NRD>MRD) NRD = MRD-RD;           // use remaining vectors in a last attempt

                    Messenger << "  Number of new trial vectors = " << NRD << endl;

                    // this should resize arrays instead
                    // check if joint subspace exceeds dimension
                    if (RD+NRD>MRD) break;

                    // check that there are enough Ds and Fs to treat this
                    if (NRD>MNW)    break;

                    // no new trial pairs !!!
                    if (NRD<=0)     break;
                }

                // copy the data to the trial subspace
                {
                    nbs.setsize (bs, RD, NRS);
                    nba.setsize (ba, RD, NRA);

                    ZS.V.n = ZA.V.n = NRD;

                    nbs = ZS;
                    nba = ZA;

                    // restore full logical dimension for future iterations
                    ZS.V.n = ZA.V.n = NNW;
                }
            }


            else if (TestHOSVD) {

                const double CPthresh = 1.e-3;

                tensor3 TS, TA;
                TS.Set(NNW, nO,nV, ZS.V[0]);
                TA.Set(NNW, nO,nV, ZA.V[0]);
                //HOSVD(TS);

                tensor2 US,VS, UA,VA;
                CPdecomp(US,VS,  TS,CPthresh);
                CPdecomp(UA,VA,  TA,CPthresh);

                tensor2 e0o(1,nO),e0v(1,nV);

                {
                    const double * en = SCF.GetEnergies();
                    for (int i=0; i<nO; ++i)
                        e0o(0,i) = en[i];
                    for (int a=0; a<nV; ++a)
                        e0v(0,a) = en[nO+a];
                }

                tensor2 ES, EA, SA;

                // solve E0-wS in the basis
                {
                    tensor2 ESO, ESV, EAO, EAV;
                    tensor2 ISO, ISV, IAO, IAV;
                    tensor2 SAO, SAV;

                    WeightedInnerProduct (ESO, US, e0o[0]);
                    WeightedInnerProduct (ESV, VS, e0v[0]);
                    WeightedInnerProduct (EAO, UA, e0o[0]);
                    WeightedInnerProduct (EAV, VA, e0v[0]);

                    TensorProductNNT (ISO, US, US);
                    TensorProductNNT (ISV, VS, VS);
                    TensorProductNNT (IAO, UA, UA);
                    TensorProductNNT (IAV, VA, VA);

                    TensorProductNNT (SAO, US, UA);
                    TensorProductNNT (SAV, VS, VA);

                    ESO *= ISV;
                    ESV *= ISO;

                    EAO *= IAV;
                    EAV *= IAO;

                    // block energies
                    ESV -= ESO;
                    EAV -= EAO;

                    // overlap
                    SAV *= SAO;

                    SAV.Transpose();

                    ES = ESV;
                    EA = EAV;
                    SA = SAV;
                }

                {
                    tensor2 ee(1,min(ES.n,EA.n));

                    SolvePaired (ee[0], ES, EA, SA);

                    Messenger << "E0 GEV eigenvalues of tensor-decomposed basis: " << ee << endl;
                }

                // now compute 4J, K1, K2;

                const int RRS = US.n;
                const int RRA = UA.n;

                tensor2    USMO(RRS, nO+nV),VSMO(RRS,nO+nV);
                tensor2    UAMO(RRA, nO+nV),VAMO(RRA,nO+nV);

                {
                    USMO.zeroize();
                    VSMO.zeroize();
                    UAMO.zeroize();
                    VAMO.zeroize();

                    for (int r=0; r<RRS; ++r)
                        for (int i=0; i<nO; ++i)
                            USMO(r,i) = US(r,i);

                    for (int r=0; r<RRS; ++r)
                        for (int a=0; a<nV; ++a)
                            VSMO(r,nO+a) = VS(r,a);

                    for (int r=0; r<RRA; ++r)
                        for (int i=0; i<nO; ++i)
                            UAMO(r,i) = UA(r,i);

                    for (int r=0; r<RRA; ++r)
                        for (int a=0; a<nV; ++a)
                            VAMO(r,nO+a) = VA(r,a);
                }

                {
                    tensor2 W;

                    efs->CoulombCholesky(W, USMO,VSMO);
                    W *= 2; // factor of 4, split

                    // update ES <- ES + 4 J
                    TensorProductNNT(ES, W,W, 1.);
                }


                if (0) {
                    tensor2 ee(1,min(ES.n,EA.n));

                    SolvePaired (ee[0], ES, EA, SA);

                    Messenger << "+4J GEV of tensor-decomposed basis: " << ee << endl;
                }



                {
                    tensor3 KSia;
                    efs->ExchangeCholesky(KSia, USMO,VSMO);
                    ES -= KSia * KSia;
                }

                {
                    tensor3 KSii, KSaa;
                    efs->ExchangeCholesky(KSii, USMO,USMO);
                    efs->ExchangeCholesky(KSaa, VSMO,VSMO);
                    ES -= KSii * KSaa;
                }

                if (0) {
                    tensor2 ee(1,min(ES.n,EA.n));

                    SolvePaired (ee[0], ES, EA, SA);

                    Messenger << "+4J +KS GEV of tensor-decomposed basis: " << ee << endl;
                }


                {
                    tensor3 KAia;
                    efs->ExchangeCholesky(KAia, UAMO,VAMO);
                    EA += KAia * KAia;
                }

                {
                    tensor3 KAii, KAaa;
                    efs->ExchangeCholesky(KAii, UAMO,UAMO);
                    efs->ExchangeCholesky(KAaa, VAMO,VAMO);
                    EA -= KAii * KAaa;
                }

                {
                    tensor2 ee(1,min(ES.n,EA.n));

                    SolvePaired (ee[0], ES, EA, SA);

                    Messenger << "E[2] GEV of tensor-decomposed basis: " << ee << endl;
                }

                // TODO :

                // increase subspaces
                // project rhs & solve equations
                // improve CP & rank selection





                // project gradients (rhs)
                // onto total basis
                Xrs = RR[0] * obs;
                Xis = RR[1] * obs;
                Xra = RR[2] * oba;
                Xia = RR[3] * oba;



                // compute new residuals
                //      E.Rs , - w S.Ra        Xs      gs     rs
                // (                     ) . (    ) - (  ) = (  )
                //   -w S.Rs ,    E Ra         Xa      ga     ra

                // by left-projection onto the Rs/Ra basis
                {

                    // solve reduced space equations in diagonal form
                    // ===============================================
                    for (int nw=0; nw<NW; ++nw)
                        InverseCPP (Xrs[nw], Xra[nw], Xis[nw], Xia[nw], EE, wr[nw], wi[nw], RD);

                    tensor2 Yrs(NW, RD), Yra(NW, RD), Yis(NW, RD), Yia(NW, RD);

                    for (int nw=0; nw<NW; ++nw) {
                        for (int n=0; n<RD; ++n) {
                            Yrs(nw,n) = -Xra(nw,n) * wr[nw] + Xia(nw,n) * wi[nw];
                            Yra(nw,n) = -Xrs(nw,n) * wr[nw] + Xis(nw,n) * wi[nw];
                            Yis(nw,n) =  Xia(nw,n) * wr[nw] + Xra(nw,n) * wi[nw];
                            Yia(nw,n) =  Xis(nw,n) * wr[nw] + Xrs(nw,n) * wi[nw];
                        }
                    }

                    ZZ[0].V.zeroize();
                    ZZ[1].V.zeroize();
                    ZZ[2].V.zeroize();
                    ZZ[3].V.zeroize();

                    ZZ[0].mul (Yrs, oba); //    E[2] * Xrs - w S[2] * Xra    +    g S[2] * Xia
                    ZZ[0].madd(Xrs,ors); //

                    ZZ[1].mul (Yis, oba); //  g S[2] * Xra                   -      E[2] * Xis + w S[2] * Xia
                    ZZ[1].msub(Xis,ors); //

                    ZZ[2].mul (Yra, obs); // -w S[2] * Xrs +   E[2] * Xra    +                   g S[2] * Xis
                    ZZ[2].madd(Xra,ora); //

                    ZZ[3].mul (Yia, obs); //                 g S[2] * Xrs    +    w S[2] * Xis   - E[2] * Xia
                    ZZ[3].msub(Xia,ora); //

                }




            }

            // quick one
            if (0)
            if (RD>0) {

                tensor2 WS, WA;

                {
                    tensor2 ES, EA, es(1,RD),ea(1,RD);

                    ES = obs.inner(pcs);
                    EA = oba.inner(pca);
                    for (int r=0; r<RD;++r) ES(r,r) -= EE[r];
                    for (int r=0; r<RD;++r) EA(r,r) -= EE[r];

                    DiagonalizeE(ES,es[0]);
                    DiagonalizeE(EA,ea[0]);

                    Messenger << "E0-E s : " << es << endl;
                    Messenger << "E0-E a : " << ea << endl;

                    WS = ES;
                    WA = EA;
                    Invert(WS);
                    Invert(WA);
                }

                {
                    tensor2 ES, EA, es(1,RD),ea(1,RD);

                    ES = ZS*ors;
                    EA = ZA*ora;

                    DiagonalScaling(RRC[0].V, pcs.V[0]);
                    DiagonalScaling(RRC[1].V, pca.V[0]);

                    ES -= RRC[0]*obs;
                    EA -= RRC[1]*oba;

                    tensor2 CS,CA;
                    TensorProduct(CS,ES,WS);
                    TensorProduct(CA,EA,WS);

                    ZS.msub(CS,obs);
                    ZA.msub(CA,oba);
                }
            }


            // find the largest coupling between old basis and new trial wrt E-E0
            if (0) {

                tensor2 TS(NNW+RD,NNW),TA(NNW+RD,NNW);

                tensor2 RS(NNW,NNW,TS[0]),RA(NNW,NNW,TA[0]);


                //RS = ZS*RRC[0];
                //RA = ZA*RRC[1];

                RS = ZS.inner(pcs);
                RA = ZA.inner(pca);

                if (RD>0) {
                    tensor2 ES(RD,NNW,TS[NNW]), EA(RD,NNW,TA[NNW]);

                    ES = obs.inner2(ZS, pcs);
                    EA = oba.inner2(ZA, pca);

                    ES -= ors*ZS;
                    EA -= ora*ZA;
                }


                tensor2 es(1,NNW+RD),ea(1,NNW+RD),US(NNW+RD),UA(NNW+RD);

                US.zeroize();
                UA.zeroize();
                es.zeroize();
                ea.zeroize();

                SVD (TS,US ,es[0]);
                SVD (TA,UA ,ea[0]);

                Messenger << "svd s : " << es << endl;
                Messenger << "svd a : " << ea << endl;
            }


            // this makes the residuals orthogonal to <b |E-w_k S |
            // which is a slight improvement over the E0 preconditioner
            if (0)
            if (RD>0) {

                tensor2 ES, EA;

                // compute inner product with E0
                {
                    tensor2 E0S, E0A;

                    ES = ZS * ors;
                    EA = ZA * ora;

                    E0S = ZS.inner2(obs,pcs);
                    E0A = ZA.inner2(oba,pca);

                    ES -= E0S;
                    EA -= E0A;
                }

                tensor2 ERS(NW, RD, ES[0]), EIS(NW, RD, ES[NW]),
                        ERA(NW, RD, EA[0]), EIA(NW, RD, EA[NW]);

                // these E blocks have opposite sign in the E-wS matrix,
                // and it's simpler to correct the here
                // THIS SEGFAULTS FOR SOME DAMN REASON
                EIS *= -1;
                EIA *= -1;

                // solves the updated coefficients required
                for (int nw=0; nw<NW; ++nw)
                    InverseCPP (ERS[nw], ERA[nw], EIS[nw], EIA[nw], EE, wr[nw], wi[nw], RD);

                // update
                ZZ[0].msub(ERS, obs);
                ZZ[1].msub(EIS, obs);
                ZZ[2].msub(ERA, oba);
                ZZ[3].msub(EIA, oba);
            }


            // THIS IS THE ONE
            // ===============
            if (1) {
                // all thresholds refer to the vector norms or their ratios, not their inner products
                const double cthresh = 1e-7;  // relative threshold for discarding components during initial basis compression

                // we should include more vectors in the first iteration

                // this refers to the SVD, which is an inner product
                const double rthresh = (nt==0) ?
                                       1.e-10 : 1.e-6;  // SVD relative threshold for discarding leftover space
                const double athresh = 1e-12;  // absolute threshold for discarding leftover space; this should depend on the requested convergence




                // 0: pre-compress the basis by removing linear combinations
                // according to some inner product

                // this splits all four subspaces, then merges R/I
                if (0) {
                    CheckLinear (ZZ[0], athresh, rthresh, &pcs);
                    CheckLinear (ZZ[1], athresh, rthresh, &pcs);
                    CheckLinear (ZZ[2], athresh, rthresh, &pca);
                    CheckLinear (ZZ[3], athresh, rthresh, &pca);

                    tensor2 U, V; // discard these
                    Biorthonormalize(U,V, ZZ[0],ZZ[2],athresh,rthresh); // find the basis
                    Biorthonormalize(U,V, ZZ[0],ZZ[3],athresh,rthresh); // find the basis
                    Biorthonormalize(U,V, ZZ[1],ZZ[2],athresh,rthresh); // find the basis
                    Biorthonormalize(U,V, ZZ[1],ZZ[3],athresh,rthresh); // find the basis

                    // copy contiguous
                    {
                        multivec SI, AI;
                        SI.setsize(ZS, ZZ[0].V.n, ZZ[1].V.n);
                        AI.setsize(ZA, ZZ[2].V.n, ZZ[3].V.n);

                        SI = ZZ[1];
                        AI = ZZ[3];

                        ZS.V.n = ZZ[0].V.n + ZZ[1].V.n;
                        ZA.V.n = ZZ[2].V.n + ZZ[3].V.n;

                        ZZ[0].V.n = ZZ[1].V.n = ZZ[2].V.n = ZZ[3].V.n = NW;
                    }
                }

                // otherwise treat R+I together from the start
                else {
                    CheckLinear (ZS, 0., cthresh, &pcs, &pcf);
                    CheckLinear (ZA, 0., cthresh, &pca);
                }

                // 1: make trial vectors conjugate wrt E[2] to the previous basis
                if (RD>0) {

                    tensor2 ES, EA;

                    ES = ZS * ors;
                    EA = ZA * ora;

                    for (int n=0; n<ES.n; ++n)
                        for (int r=0; r<RD; ++r)
                            ES(n,r) /= EE[r];

                    for (int n=0; n<EA.n; ++n)
                        for (int r=0; r<RD; ++r)
                            EA(n,r) /= EE[r];

                    ZS.msub(ES, obs);
                    ZA.msub(EA, oba);
                }


                if (0) {
                    tensor2 ee(1,min(ZS.V.n,ZA.V.n));
                    tensor2 ES, EA, SA; // contains the RS energy blocks

                    ES = ZS.inner(pcs,pcf);
                    EA = ZA.inner(pca);
                    SA = ZA * ZS;

                    SolvePaired (ee[0], ES, EA, SA);

                    Messenger << "first E0 GEV eigenvalues : " << ee << endl;
                }


                // 2,3: biorthinormalize the new basis; delete quasi linear dependences
                {
                    tensor2 U, V; // these contain the linear combinations

                    NRD = Biorthonormalize(U,V, ZS,ZA,athresh,rthresh); // find the basis

                    // rotate accordingly; logically resizes Z
                    ZS.rot(U);
                    ZA.rot(V);

                    Messenger << "rotation succeeded" << endl;
                }




                // 4: rotate and scale the new S & A subspaces and balance their vectors by
                //    solving the GEV for the preconfitioner E0
                {
                    tensor2 ee(1,NRD);
                    tensor2 RS(NRD), RA(NRD);
                    tensor2 ES(NRD), EA(NRD), SA(NRD); // contains the RS energy blocks

                    ES = ZS.inner(pcs,pcf);
                    EA = ZA.inner(pca);
                    SA = ZA * ZS;

                    if (NodeGroup.IsMaster()) {
                        SolvePaired (RS, RA, ee[0], ES, EA, SA, NRD);
                    }
                    {
                        Tensor2Broadcast    (RS);
                        Tensor2Broadcast    (RA);
                        NodeGroup.Broadcast (ee[0], NRD);
                    }

                    Messenger << "E0 GEV eigenvalues : " << ee << endl;

                    // discard too large energies, which will otherwise introduce excessive numerical error while not contributing
                    {
                        int NRT; for (NRT=0; NRT<NRD; ++NRT) if (ee[0][NRT]>Emax) break;

                        if (NRT<NRD) Messenger << "discarded NRT = " << NRD-NRT << " paired eigenvectors due to potential numerical instability;  new NRD = "<< NRT << " " << endl;

                        ee.m = NRT;
                        RS.n = RA.n = NRT;

                        // generate new approximate (Krylov) eigenvectors of the 2x2 GEV
                        // by rotating old and new subspaces
                        ZS.rot(RS);
                        ZA.rot(RA);
                    }
                }

                // sanity check (memory, new subspace dimension, etc.)
                {
                    if (RSP.MV>0)   NRD = min(NRD, RSP.MV); // do not surpass the maximum number of vectors
                    if (RD+NRD>MRD) NRD = MRD-RD;           // use remaining vectors in a last attempt

                    Messenger << "  Number of new trial vectors = " << NRD << endl;

                    // this should resize arrays instead
                    // check if joint subspace exceeds dimension
                    if (RD+NRD>MRD) break;

                    // check that there are enough Ds and Fs to treat this
                    if (NRD>MNW)    break;

                    // no new trial pairs !!!
                    if (NRD<=0)     break;
                }

                // copy the data to the trial subspace
                {
                    nbs.setsize (bs, RD, NRD);
                    nba.setsize (ba, RD, NRD);

                    // in case there has been a further reduction
                    ZS.V.n = ZA.V.n = NRD;

                    nbs = ZS;
                    nba = ZA;

                    // restore full logical dimension for future iterations
                    ZS.V.n = ZA.V.n = NNW;
                }




                // now all paired vectors are more or less on equal footing, being normalized wrt E / E0 and forming a biorthogonal set
                // however, when diagonalizing the total inner product S there are often a few spurious components which end up in the
                // basis producing spurious energy (pseudo)eigenvalues of very high energy; ideally we would like to get rid of them
                // before they cause any numerical trouble



                // we should probably check for linear dependence with the previous basis;
                // i.e. delete vectors / LC that overlap too much;
                // check this by computing the SVD of Sab
                if (0) {
                    multivec bbs; bbs.setsize (bs, 0, RD+ NRD);
                    multivec bba; bba.setsize (ba, 0, RD+ NRD);

                    tensor2 ss(1,RD+NRD);
                    tensor2 SA(RD+NRD); // contains the RS energy blocks

                    SA = bba * bbs;


                    tensor2 V(RD+NRD);
                    tensor2 U(RD+NRD);
                    SVD (SA,U,V,ss[0]);


                    //PCA(SA,ss[0]);
                    Messenger << "PCA of total overlap : " << ss << endl;

                    U.Transpose();

                    tensor2 u(1,RD+NRD, U[RD+NRD-1]);
                    tensor2 v(1,RD+NRD, V[RD+NRD-1]);

                    Messenger << "last column of U : " << u << endl;
                    Messenger << "last column of V : " << v << endl;

                    //should probably discard anything below 0.1
                }


                // the idea is to just extract the last PCs and make the occupied new space
                // by orthogonalizing against the PC projections on the new trial block
                {

                }



                // find S/A linear combinations coupling strongly to the old basis and delete them.
                // this removes some near useless vectors and improves numerical stability
                if (0) {
                    // first project out the previous subspace
                    if (RD>0) {

                        nbs.setsize (bs, RD, NRD);
                        nba.setsize (ba, RD, NRD);


                        tensor2 SA, AS;

                        SA = nbs * oba;
                        AS = nba * obs;

                        const int NRW = min(RD,NRD);

                        tensor2 sa(1,NRD);
                        tensor2 as(1,NRD);

                        {
                            tensor2 V(RD);
                            tensor2 U(NRD);
                            SVD (SA,U,V,sa[0]);
                            U.Transpose();
                            nbs.rot(U);
                        }

                        {
                            tensor2 V(RD);
                            tensor2 U(NRD);
                            SVD (AS,U,V,as[0]);
                            U.Transpose();
                            nba.rot(U);
                        }

                        // invert sigma
                        // NRD may be > RD during the initial iterates
                        for (int n=0; n<NRW; ++n) sa(0,n) = 1./sa(0,n);
                        for (int n=0; n<NRW; ++n) as(0,n) = 1./as(0,n);

                        for (int n=NRW; n<NRD; ++n) sa(0,n) = 1.;
                        for (int n=NRW; n<NRD; ++n) as(0,n) = 1.;

                        Messenger << "SA : " << endl << sa << endl;
                        Messenger << "AS : " << endl << as << endl;

                        nbs.scale(sa[0]);
                        nba.scale(as[0]);

                        // after this, the Snew block couples with the Sold = I through unitary blocks,
                        // meaning the coupling is trivial



                        // compute proper values & rotate basis
                        {
                            tensor2 SS;

                            SS = nbs * nba; //

                            // do the SVD
                            const int NRW = min(SS.n,SS.m);

                            tensor2 U(SS.n), V(SS.m);
                            tensor2 ss(1,NRW);


                            if (NodeGroup.IsMaster()) {
                                SVD (SS, U, V, ss[0]);
                                U.Transpose();
                            }
                            // share results with all other processes
                            {
                                NodeGroup.Broadcast(ss[0], NRW);

                                Tensor2Broadcast(U);
                                Tensor2Broadcast(V);
                            }

                            nbs.rot(U);
                            nba.rot(V);


                            // check everything ok
                            //SS = nbs * nba; //
                            //Messenger << "testing biorthogonality : " << endl << SS << endl;

                            Messenger << "new singular values : " << endl << ss << endl;



                            // now eliminate all pairs with singular values under 1,, which contribute to significant basis degeneracy
                            // note: this doesnt work as intended, and drops too many vectors; however it does seem to get rid of the spurious eigenvalues
                            if (1) {
                                int NRT;
                                for (NRT=0; NRT<NRD;++NRT) if (ss(0,NRT)<1.) break;

                                if (NRT<NRD) Messenger << NRD-NRT << " trial pairs have been discarded; new NRD = " << NRT << endl;

                                NRD = NRT;
                            }

                        }

                    }
                }



                // we should probably check for linear dependence with the previous basis;
                // i.e. delete vectors / LC that overlap too much;
                // check this by computing the SVD of Sab
                if (0) {
                    multivec bbs; bbs.setsize (bs, 0, RD+ NRD);
                    multivec bba; bba.setsize (ba, 0, RD+ NRD);

                    tensor2 ss(1,RD+NRD);
                    tensor2 SA(RD+NRD); // contains the RS energy blocks

                    SA = bba * bbs;

                    PCA(SA,ss[0]);
                    Messenger << "PCA of total overlap : " << ss << endl;

                    //should probably discard anything below 0.1
                }


                // test GSVD ?
                // make the principal components orthogonal wrt E0, i.e.
                // S = U s V+   U+ E0 U = I   V+ E0 V = I
            }

            // new method based on conjugation with E[2]
            else if (0) {

                /*
                    FURTHER IMPROVEMENT SHOULD PROBABLY INVOLVE A GOOD STRATEGY FOR SELECTING SUBSPACES AT EACH ITERATION;

                    IT LOOKS SENSIBLE TO SELECT MORE IN THE FIRST ITERATION THAN IN THE SUBSEQUENT ONES, SINCE THEY WILL POPULATE E2, WHICH IS THE MAIN CORRECTION TO THE PRECONDITIONED TRIAL VECTORS

                    THEN AS EQUATIONS GETS CLOSER TO CONVERGENCE, WE SHOULD SKIP MORE AND MORE PROPOSED VECTORS SINCE THE SOURCES OF RESIDUAL ERROR ARE 2-E AND DFT CONTRIBUTIONS THAT ARE POORLY SPANNED BY SOME EXTRA VECTORS,
                    REQUIRING AN ACTUAL CALL TO EFS/XCINT TO DETERMINE THEIR CONTRIBUTION

                    IN ANY CASE WE COULD EXAMINE THE DISTRIBUTION OF SINGULAR VALUES A BIT MORE CAREFULLY AND MORE THE CUTOFF IF NECESSARY TO AVOID CUTTING THROUGH CLUSTERS


                    WHEN AN EQUATION TAKES MANY MORE CYCLES TO CONVERGE THAN ANOTHER IT MEANS THAT THE 2E CONTRIBUTIONS AT THAT REGION ARE HARD TO SPAN USING POWERS OF THE PC, AND NEARBY EQUATIONS ARE MUCH DIFFERENT OR MISSING,
                    AS IN THE CASE OF THE HIGHEST FREQUENCIES
                */


                // find unphysical large energies
                const double invEthresh = 0.1; // small E^-1 creates numerical issues and is unphysical
                const double Emax       = 50.; // maximum AE to include
                // sibce it is an SVD rather than a orthonormalization, the cutoff is actually the square of this
                const double rthresh = 1.e-3; 1e-6; // SVD threshold relative to the largest component
                const double athresh = 1.e-4; 1e-6;  // absolute SVD threshold




                // this procedure projects out the components of the preconditioned residual
                // that would point in directions that have already been spanned, i.e. the trial vector should not contain components of:
                //
                //    <b | E-w_k S | x_k > = 0
                //
                // this presupposes the use of a diagonal (non-SDP) preconditioner that includes the correct signs

                // |r> = P |x'>
                //
                // <b| r> = <b| E-wS |x*> = 0  (x*= exact solution)
                //        = <b | P | x*> + <b | AE) | x*>
                //
                // so if we assume x* = x' + Ax we get
                //     0 =  <b | P | x'> + <b | P | Ax> + <b | AE | x'> + <b | AE | Ax >
                // first term vanishes; we use <b|P|Ax
                //     0 = <b | E-wS | Ax>  +  <b

                // which is usually not the case

                // ======================================
                // this seems to converge slightly faster
                // ======================================


                if (0) {
                    if (RD>0) {

                        tensor2 ES, EA;

                        ES = ZS * ors;
                        EA = ZA * ora;

                        // compute inner product with E0
                        {
                            tensor2 E0S, E0A;

                            for (int nw=0; nw<NNW; ++nw)
                                for (int ia=0; ia<nOV; ++ia)
                                    ZS.V(nw,ia) *= pcs.V(0,ia);

                            for (int nw=0; nw<NNW; ++nw)
                                for (int ia=0; ia<nOV; ++ia)
                                    ZA.V(nw,ia) *= pca.V(0,ia);

                            E0S = ZS * obs;
                            E0A = ZA * oba;

                            for (int nw=0; nw<NNW; ++nw)
                                for (int ia=0; ia<nOV; ++ia)
                                    ZS.V(nw,ia) *= 1./pcs.V(0,ia);

                            for (int nw=0; nw<NNW; ++nw)
                                for (int ia=0; ia<nOV; ++ia)
                                    ZA.V(nw,ia) *= 1./pca.V(0,ia);

                            ES -= E0S;
                            EA -= E0A;
                        }

                        tensor2 ERS(NW, RD, ES[0]), EIS(NW, RD, ES[NW]),
                                ERA(NW, RD, EA[0]), EIA(NW, RD, EA[NW]);


                        // these E blocks have opposite sign in the E-wS matrix,
                        // and it's simpler to correct the here
                        // THIS SEGFAULTS FOR SOME DAMN REASON
                        EIS *= -1;
                        EIA *= -1;

                        // solves the updated coefficients required
                        for (int nw=0; nw<NW; ++nw)
                            InverseCPP (ERS[nw], ERA[nw], EIS[nw], EIA[nw], EE, wr[nw], wi[nw], RD);

                        // update
                        ZZ[0].msub(ERS, obs);
                        ZZ[1].msub(EIS, obs);
                        ZZ[2].msub(ERA, oba);
                        ZZ[3].msub(EIA, oba);
                    }
                }
                else {

                    // make trial vectors conjugate wrt E[2] to the previous basis
                    if (RD>0) {

                        tensor2 ES, EA;

                        ES = ZS * ors;
                        EA = ZA * ora;

                        for (int n=0; n<NNW; ++n)
                            for (int r=0; r<RD; ++r) {
                                ES(n,r) /= EE[r];
                                EA(n,r) /= EE[r];
                            }

                        ZS.msub(ES, obs);
                        ZA.msub(EA, oba);
                    }
                }


                tensor2 ee; // approx. eigenvalues

                // orthonormalize vectors wrt E[2]_0
                {
                    tensor2 es(1,nOV),ea(1,nOV);

                    // this orthogonalizes the trial space with respect to E0 by means of a diagonalization;
                    // only instead perform an SVD of the half-inner product for higher numerical precision
                    // (compared to building the inner product and diagonalizing)
                    {
                        for (int ia=0; ia<nOV; ++ia)
                            es[0][ia] = sqrt(pcs.V[0][ia]);

                        tensor2 VV(NNW, nOV);
                        tensor2 U(NNW);

                        for (int nw=0; nw<NNW; ++nw)
                            for (int ia=0; ia<nOV; ++ia)
                                VV(nw,ia) = ZS.V(nw,ia) * es(0,ia);

                        SVD (VV,U,es[0]);
                        U.Transpose();
                        ZS.rot(U);
                    }

                    {
                        for (int ia=0; ia<nOV; ++ia)
                            ea[0][ia] = sqrt(pca.V[0][ia]);

                        tensor2 VV(NNW, nOV);
                        tensor2 U(NNW);

                        for (int nw=0; nw<NNW; ++nw)
                            for (int ia=0; ia<nOV; ++ia)
                                VV(nw,ia) = ZA.V(nw,ia) * ea(0,ia);

                        SVD (VV,U,ea[0]);
                        U.Transpose();
                        ZA.rot(U);
                    }

                    //Messenger.precision(15); Messenger << "es " << es << endl; Messenger << "ea " << ea << endl; Messenger.precision(7);


                    // TEST THIS
                    /*
                    {
                        tensor2 ee(1,NRD);
                        tensor2 RS(NRD), RA(NRD);
                        tensor2 ES(NRD), EA(NRD), SA(NRD); // contains the RS energy blocks


                        ES = ZS.inner(pcs);
                        EA = ZA.inner(pca);
                        SA = ZA * ZS;

                        if (NodeGroup.IsMaster()) {
                            SolvePaired (RS, RA, ee[0], ES, EA, SA, NRD);
                        }
                        {
                            Tensor2Broadcast    (RS);
                            Tensor2Broadcast    (RA);
                            NodeGroup.Broadcast (ee[0], RD);
                        }

                        // the solutions *should* span the residuals nicely

                        //Messenger << "RS" << endl << RS << endl;
                        //Messenger << "RA" << endl << RA << endl;

                        Messenger << "EE : " << ee << endl;


                        // generate new approximate (Krylov) eigenvectors of the 2x2 GEV
                        // by rotating old and new subspaces
                        ZS.rot(RS);
                        ZA.rot(RA);
                    }
                    */



                    // find subspace dimension (discard linear dependencies)
                    // this is dodgy to say the least; one can never be sure if the discard just happened to have low energy
                    {
                        const double sthr = max(athresh, rthresh*es[0][0]);
                        const double athr = max(athresh, rthresh*ea[0][0]);

                        for (NRD=1; NRD<NNW; ++NRD)
                            //if ( ( es[0][NRD] < thresh*es[0][0]) || ( ea[0][NRD] < thresh*ea[0][0]) ) break;
                            //if ( (es[0][NRD] < thresh) || (ea[0][NRD] < thresh) ) break;
                            if ( (es[0][NRD] < sthr) || (ea[0][NRD] < athr) ) break;

                        es.m = ea.m = ZS.V.n = ZA.V.n = NRD; // ignore linearly dependent subspaces
                        Messenger << "NRD = "<< NRD << " / " << NNW << endl;
                    }

                    // TEST THIS
                    {

                        tensor2 RS(NRD), RA(NRD);
                        tensor2 ES(NRD), EA(NRD), SA(NRD); // contains the RS energy blocks

                        ee.setsize(1,NRD);


                        ES = ZS.inner(pcs);
                        EA = ZA.inner(pca);
                        SA = ZA * ZS;

                        if (NodeGroup.IsMaster()) {
                            SolvePaired (RS, RA, ee[0], ES, EA, SA, NRD);
                        }
                        {
                            Tensor2Broadcast    (RS);
                            Tensor2Broadcast    (RA);
                            NodeGroup.Broadcast (ee[0], NRD);
                        }

                        // the solutions *should* span the residuals nicely

                        //Messenger << "RS" << endl << RS << endl;
                        //Messenger << "RA" << endl << RA << endl;

                        Messenger << "EE : " << ee << endl;

                        // discard too large energies, which will otherwise introduce excessive numerical error while not contributing
                        {
                            int NRT;
                            for (NRT=0; NRT<NRD; ++NRT) if (ee[0][NRT]<Emax) break;

                            tensor2 CS(NRD);
                            tensor2 CA(NRD);

                            tensor2 e2(1,NRD);

                            // goodenergies
                            for (int n=0; n<NRD-NRT; ++n) {
                                for (int m=0; m<NRD; ++m) CS(n,m) = RS(NRT+n,m);
                                for (int m=0; m<NRD; ++m) CA(n,m) = RA(NRT+n,m);
                                e2(0,n) = ee(0,NRT+n);
                            }
                            // bad energies
                            for (int n=0; n<NRT; ++n) {
                                for (int m=0; m<NRD; ++m) CS(NRD-NRT+n,m) = RS(n,m);
                                for (int m=0; m<NRD; ++m) CA(NRD-NRT+n,m) = RA(n,m);
                                e2(0,NRD-NRT+n) = ee(0,n);
                            }

                            ee = e2;




                            Messenger << "NRD = "<< NRD << " NRT = " << NRT << endl;



                            NRD -= NRT;
                            //ZS.V.n = ZA.V.n = NRD = NRT;

                            // generate new approximate (Krylov) eigenvectors of the 2x2 GEV
                            // by rotating old and new subspaces
                            ZS.rot(CS);
                            ZA.rot(CA);

                            ee.m = ZS.V.n = ZA.V.n = NRD;
                        }
                    }


                    // do not scale yet
                    for (int n=0; n<NRD; ++n) es[0][n] = 1./es[0][n];
                    for (int n=0; n<NRD; ++n) ea[0][n] = 1./ea[0][n];

                    // this is handled by the eigenroutines
                    //ZS.scale(es[0]);
                    //ZA.scale(ea[0]);
                }

                // this actually makes things harder since tyhe rhs is often degenerate;
                // must figure out the proper way
                if (0) {
                    // project gradients (rhs)
                    // onto total basis
                              Xrs = RRC[0] * ZS;
                    if (DoIS) Xis = RRC[1] * ZS;
                    if (DoRA) Xra = RRC[2] * ZA;
                    if (DoIA) Xia = RRC[3] * ZA;

                    //Messenger << " RS : " << Xrs << endl;

                    // Xrs+ * Xrs
                    tensor2 K, k(1,NRD);
                    TensorProductNTN (K,Xrs,Xrs);
                    TensorProductNTN (K,Xis,Xis,1.);
                    TensorProductNTN (K,Xra,Xra,1.);
                    TensorProductNTN (K,Xia,Xia,1.);

                    DiagonalizeV(K,k[0],true);
                    Messenger << "occupancies = " << k << endl;


                    // now project the ZS/ZA basis
                    ZS.rot(K);
                    ZA.rot(K);


                    const double thresh = 1e-6;

                    int NRT;
                    for (NRT=0; NRT<NRD; ++NRT) if (k[0][NRT]<thresh*k[0][0]) break;

                    ZS.V.n = ZA.V.n = NRD = NRT;

                    Messenger << "NRD = "<< NRD << " / " << NNW << endl;
                }



                // try solving;
                // not sure whether it's doing anything remotely like
                if (0) {


                              Xrs = RRC[0] * ZS;
                    if (DoIS) Xis = RRC[1] * ZS;
                    if (DoRA) Xra = RRC[2] * ZA;
                    if (DoIA) Xia = RRC[3] * ZA;

                    // solve reduced space equations in diagonal form
                    // ===============================================
                    for (int nw=0; nw<NW; ++nw)
                        InverseCPP (Xrs[nw], Xra[nw], Xis[nw], Xia[nw], ee[0], wr[nw], wi[nw], NRD);

                    tensor2 Yrs(NW, NRD), Yra(NW, NRD), Yis(NW, NRD), Yia(NW, NRD);

                    for (int nw=0; nw<NW; ++nw) {
                        for (int n=0; n<NRD; ++n) {
                            Yrs(nw,n) = -Xra(nw,n) * wr[nw] + Xia(nw,n) * wi[nw];
                            Yis(nw,n) =  Xia(nw,n) * wr[nw] + Xra(nw,n) * wi[nw];
                            Yra(nw,n) = -Xrs(nw,n) * wr[nw] + Xis(nw,n) * wi[nw];
                            Yia(nw,n) =  Xis(nw,n) * wr[nw] + Xrs(nw,n) * wi[nw];
                        }
                    }


                    // this finds the most "used" principal components
                    // it basically amounts to the same result as below

                    // if you don't have null contributions, you don't have a good subspace to orthogonalize AGAINST
                    {
                        // NW >= NRD
                        tensor2 U(NRD), m(1,NRD);
                        // NW >= NRD
                        tensor2 V(NRD), n(1,NRD);

                        {

                            tensor2 M  (2*NW, NRD);
                            tensor2 MR (NW,NRD, M[0]);
                            tensor2 MI (NW,NRD, M[NW]);

                            MR = Xrs; MI = Xis;  MI *= -1;
                            DiagonalScaling (M, ee[0]);
                            MR += Yrs; MI += Yis;


                            M.Transpose();

                            SVD (M, U, m[0]);

                            Messenger << " pc's of S block = " << m << endl;
                        }

                        {
                            tensor2 N  (2*NW, NRD);
                            tensor2 NR (NW,NRD, N[0]);
                            tensor2 NI (NW,NRD, N[NW]);

                            NR = Xra; NI = Xia; NI *= -1;
                            DiagonalScaling (N, ee[0]);
                            NR += Yra; NI += Yia;

                            N.Transpose();

                            SVD (N, V, n[0]);

                            Messenger << " pc's of A block = " << n << endl;
                        }

                        ZS.rot(U);
                        ZA.rot(V);


                        const double thresh = 1.e-5;

                        int NRT;
                        for (NRT=0; NRT<NRD; ++NRT) if ( (m[0][NRT]<thresh*m[0][0]) || (n[0][NRT]<thresh*n[0][0]) ) break;

                        ZS.V.n = ZA.V.n = NRD = NRT;

                        Messenger << "NRD = "<< NRD << " / " << NNW << endl;
                    }






                    // the Xs and Ys hold the "amount" of trial vecs;
                    // since we got the coefficients, we hardly need to build the solution
                    if (0) {
                        RRC[0].V.zeroize();
                        RRC[1].V.zeroize();
                        RRC[2].V.zeroize();
                        RRC[3].V.zeroize();

                        RRC[0].mul(Yrs, ZA); //    E[2] * Xrs - w S[2] * Xra    +    g S[2] * Xia
                        RRC[1].mul(Yis, ZA); //  g S[2] * Xra                   -      E[2] * Xis + w S[2] * Xia
                        RRC[2].mul(Yra, ZS); // -w S[2] * Xrs +   E[2] * Xra    +                   g S[2] * Xis
                        RRC[3].mul(Yia, ZS); //                 g S[2] * Xrs    +    w S[2] * Xis   - E[2] * Xia

                        // scale ZZ back and forward with the E0 diagonal to avoid more temporal data
                        tensor2 es(1,nOV),ea(1,nOV);
                        for (int ia=0; ia<nOV; ++ia) es(0,ia) = pcs.V(0,ia);
                        for (int ia=0; ia<nOV; ++ia) ea(0,ia) = pca.V(0,ia);


                        for (int nw=0; nw<NRD; ++nw)
                            for (int ia=0; ia<nOV; ++ia)
                                ZS.V(nw,ia) *= es(0,ia);

                        for (int nw=0; nw<NRD; ++nw)
                            for (int ia=0; ia<nOV; ++ia)
                                ZA.V(nw,ia) *= ea(0,ia);


                        RRC[0].madd(Xrs,ZS); //
                        RRC[1].msub(Xis,ZS); //
                        RRC[2].madd(Xra,ZA); //
                        RRC[3].msub(Xia,ZA); //

                        for (int ia=0; ia<nOV; ++ia) es(0,ia) = 1./es(0,ia);
                        for (int ia=0; ia<nOV; ++ia) ea(0,ia) = 1./ea(0,ia);


                        for (int nw=0; nw<NRD; ++nw)
                            for (int ia=0; ia<nOV; ++ia)
                                ZS.V(nw,ia) *= es(0,ia);

                        for (int nw=0; nw<NRD; ++nw)
                            for (int ia=0; ia<nOV; ++ia)
                                ZA.V(nw,ia) *= ea(0,ia);



                        // for the inner products
                        tensor2 MS, MA;

                        TensorProductNNT (MS, RRC[0].V, RRC[0].V);    // RS
                        TensorProductNNT (MS, RRC[1].V, ZZ[1].V,1.); // IS

                        TensorProductNNT (MA, RRC[2].V, RRC[2].V);    // RA
                        TensorProductNNT (MA, RRC[3].V, RRC[3].V,1.); // IA

                        // now find low
                        tensor2 ms(1,NW), ma(1,NW);

                        DiagonalizeV(MS,ms[0]);
                        DiagonalizeV(MA,ma[0]);

                        Messenger << " S evs = " << ms << endl;
                        Messenger << " A evs = " << ma << endl;
                    }



                    // now project the ZS/ZA basis
                    if (0) {

                        // FIXME:
                        // test; not sure it makes much sense or rather we must decouple A & S spaces and maybe even E2 * ZRS
                        tensor2 K, k(1,NRD);
                        TensorProductNTN (K,Xrs,Xrs);
                        TensorProductNTN (K,Xis,Xis,1.);
                        TensorProductNTN (K,Xra,Xra,1.);
                        TensorProductNTN (K,Xia,Xia,1.);

                        TensorProductNTN (K,Yrs,Yrs,1.);
                        TensorProductNTN (K,Yis,Yis,1.);
                        TensorProductNTN (K,Yra,Yra,1.);
                        TensorProductNTN (K,Yia,Yia,1.);

                        DiagonalizeV(K,k[0],true);
                        Messenger << "occupancies = " << k << endl;

                        ZS.rot(K);
                        ZA.rot(K);

                        const double thresh = 1.e-5;

                        int NRT;
                        for (NRT=0; NRT<NRD; ++NRT) if (k[0][NRT]<thresh*k[0][0]) break;

                        ZS.V.n = ZA.V.n = NRD = NRT;

                        Messenger << "NRD = "<< NRD << " / " << NNW << endl;
                    }


                }





                // find linear dependencies in the S matrix and remove them
                if (0) {
                    const double invEthresh = 0.1; // small E^-1 creates numerical issues and is unphysical
                    tensor2 ZZ; // this is used again later
                    ZZ = ZS*ZA;

                    // do the SVD of the joint inner product
                    tensor2 ss(1,NRD);
                    tensor2 U(NRD), V(NRD);

                    if (NodeGroup.IsMaster()) {
                        SVD (ZZ, U, V, ss[0]);
                    }

                    U.Transpose();
                    ZS.rot(U);
                    ZA.rot(V);

                    Messenger << "inverse energies according to trial subspace: " << ss << endl;

                    int NRT;
                    for (NRT=0; NRT<NRD; ++NRT) if (ss[0][NRT]<invEthresh) break;

                    ZS.V.n = ZA.V.n = NRD = NRT;

                    Messenger << "NRD = "<< NRD << " / " << NNW << endl;
                }



                const int RT = RD+NRD;

                // now build inner product S with previous basis (if relevant)
                if (0) {
                    // assemble the full S tensor
                    tensor2 SS (RT);


                    if (RD>0) {
                        SS.zeroize();

                        // block inner products
                        tensor2 BB, BZ, ZB, ZZ; {

                            // scale trial vectors with 1/sqrt(eigenenergy)
                            {
                                tensor2 isqee (1,RD);
                                for (int r=0; r<RD; ++r) isqee(0,r) = 1./sqrt(EE[r]);

                                obs.scale(isqee[0]);
                                oba.scale(isqee[0]);
                            }

                            BB = obs*oba; // should be diagonal
                            BZ = obs*ZA;
                            ZB = ZS*oba;
                            ZZ = ZS*ZA;

                            // scale trial vectors back
                            {
                                tensor2 sqee (1,RD);
                                for (int r=0; r<RD; ++r) sqee(0,r) = sqrt(EE[r]);

                                obs.scale(sqee[0]);
                                oba.scale(sqee[0]);
                            }
                        }

                        for (int r=0; r<RD; ++r)
                            for (int s=0; s<RD; ++s)
                                SS  (r,s) = BB(r,s);

                        for (int r=0; r<NRD; ++r)
                            for (int s=0; s<RD; ++s)
                                SS  (RD+r,s) = ZB(r,s);

                        for (int r=0; r<RD; ++r)
                            for (int s=0; s<NRD; ++s)
                                SS  (r,RD+s) = BZ(r,s);

                        for (int r=0; r<NRD; ++r)
                            for (int s=0; s<NRD; ++s)
                                SS  (RD+r,RD+s) = ZZ(r,s);
                    }
                    else {
                        SS = ZS*ZA;
                    }


                    // restore the real dimension
                    ZS.V.n = ZA.V.n = NNW;



                    tensor2 ss(1,RT);
                    tensor2 U(RT), V(RT);
                    {
                        if (NodeGroup.IsMaster()) {

                            SVD (SS, U, V, ss[0]);
                            U.Transpose();
                        }
                        // share results with all other processes
                        {
                            NodeGroup.Broadcast(ss[0], RT);

                            Tensor2Broadcast(U);
                            Tensor2Broadcast(V);
                        }

                        Messenger << "approximate inverse energies : " << ss << endl;
                    }


                    U.Transpose();
                    V.Transpose();



                    // now project the U & V modes onto the new basis subspace
                    // and discard old basis dependencies by computing the optimal occupations
                    tensor2 RS, RA;

                    {
                        // remove the references to the old basis
                        tensor2 UU (NRD, RT, U[RD]);
                        tensor2 VV (NRD, RT, V[RD]);

                        // build inner product
                        tensor2 PS, PA; // projectors on the E2S and E2A new trial subspaces (notice that ES = EA = I)

                        TensorProductNTN (PS, UU,UU);
                        TensorProductNTN (PA, VV,VV);

                        // occupation numbers
                        tensor2 os(1,RT), oa(1,RT);

                        DiagonalizeV(PS,os[0],true);
                        DiagonalizeV(PA,oa[0],true);

                        tensor2 PPS (NRD, RT, PS[0]);
                        tensor2 PPA (NRD, RT, PA[0]);

                        // now construct the final trial vecs from linear combination with PS U  & PA V
                        TensorProductNNT (RS,PPS,UU); // maaybe NNT
                        TensorProductNNT (RA,PPA,VV);


                        // 50/50
                        RS.Transpose();
                        RA.Transpose();

                        //Messenger << "RS : " << endl << RS << endl;
                        //Messenger << "RA : " << endl << RA << endl;
                    }


                    /*

                    // try simply projecting out the previous subspace & doing the SVD of the resulting matrix
                    if (0) {
                        tensor2 SN(RT); {
                            tensor2 AZZ; // ZB * BB^-1 * BZ

                            tensor2 EBZ;
                            EBZ = BZ;

                            DiagonalScaling(EE, EBZ); // EE == BB^-1

                            TensorProduct(AZZ, ZB, EBZ);


                            SN.zeroize();

                            for (int r=0; r<NRD; ++r)
                                for (int s=0; s<NRD; ++s)
                                    SN  (RD+r,RD+s) = ZZ(r,s) - AZZ(r,s);
                        }

                        // do the SVD of the joint inner product
                        tensor2 ss(1,RT);
                        tensor2 U(RT), V(RT);
                        {
                            if (NodeGroup.IsMaster()) {

                                SVD (SN, U, V, ss[0]);
                                V.Transpose();
                            }
                            // share results with all other processes
                            {
                                NodeGroup.Broadcast(ss[0], RT);

                                Tensor2Broadcast(U);
                                Tensor2Broadcast(V);
                            }

                            Messenger << "inverse energies of projected subspace: " << ss << endl;
                        }
                    }

                    // build the subspace spanned by the old basis plus the closest interactions in the new basis
                    if (0) {
                        tensor2 SSS (RT); {
                            SSS.zeroize();

                            // only copy the old basis block + interactions
                            for (int r=0; r<RD; ++r)
                                for (int s=0; s<RD; ++s)
                                    SSS (r,s) = BB(r,s);

                            for (int r=0; r<NRD; ++r)
                                for (int s=0; s<RD; ++s)
                                    SSS  (RD+r,s) = ZB(r,s);

                            for (int r=0; r<RD; ++r)
                                for (int s=0; s<NRD; ++s)
                                    SSS  (r,RD+s) = BZ(r,s);


                            // ZB * BB^-1 * BZ
                            tensor2 AZZ;
                            {
                                tensor2 EBZ;
                                EBZ = BZ;

                                DiagonalScaling(EE, EBZ); // EE == BB^-1

                                TensorProduct(AZZ, ZB, EBZ);
                            }

                            for (int r=0; r<NRD; ++r)
                                for (int s=0; s<NRD; ++s)
                                    SSS  (RD+r,RD+s) = AZZ(r,s);
                        }

                        // do the SVD of the joint inner product
                        tensor2 ss(1,RT);
                        tensor2 U(RT), V(RT);
                        {
                            if (NodeGroup.IsMaster()) {

                                SVD (SSS, U, V, ss[0]);
                                V.Transpose();
                            }
                            // share results with all other processes
                            {
                                NodeGroup.Broadcast(ss[0], RT);

                                Tensor2Broadcast(U);
                                Tensor2Broadcast(V);
                            }

                            Messenger << "inverse energies of old basis + update: " << ss << endl;
                        }
                    }



                    // alternative to the following steps:
                    // ===================================

                    // 1) builds a low rank decomposition by scaling U & V with s = sqrt(sigma);  SS ~ (U s) (V s)+
                    // 2) now we have an arbitrary choice of rotation of the scaled vectors;      SS ~ (U s) W+ W (V s)+
                    // 3) find a projector P of rank K>=N such that when introduced between the scaled U & V, the basis spans the old basis; in particular, the diagonal of inverse energies looks AOK
                    //     P = W+ W
                    // 4) minimizing the Frobenius norm of the difference looks like a valid strategy; this probably looks a lot like the code below, which was derived from a different reasoning

                    //    maybe it is possible to use a non-unitary matrix P = M M^-1 instead of P = W W+, considering we have lost properties duting the basis mixing

                    //    perhaps we also need to make sure the basis spans not just BB, but also the principal components of BZ and ZB; i.e. the optimal LCs must span exactly the same subspace as the old basis

                    //    once this is working we can optimize the orthogonal components to the old basis by maximizing occupancy


                    // now check old basis occupation in the new approximate eigenvectors:
                    {
                        tensor2 SSS (RT); {
                            SSS.zeroize();

                            // only copy the old basis block + interactions
                            for (int r=0; r<RD; ++r)
                                for (int s=0; s<RD; ++s)
                                    SSS (r,s) = BB(r,s);

                            for (int r=0; r<NRD; ++r)
                                for (int s=0; s<RD; ++s)
                                    SSS  (RD+r,s) = ZB(r,s);

                            for (int r=0; r<RD; ++r)
                                for (int s=0; s<NRD; ++s)
                                    SSS  (r,RD+s) = BZ(r,s);


                            // ZB * BB^-1 * BZ
                            tensor2 AZZ;
                            {
                                tensor2 EBZ;
                                EBZ = BZ;

                                DiagonalScaling(EE, EBZ); // EE == BB^-1

                                TensorProduct(AZZ, ZB, EBZ);
                            }

                            for (int r=0; r<NRD; ++r)
                                for (int s=0; s<NRD; ++s)
                                    SSS  (RD+r,RD+s) = AZZ(r,s);

                            // rotate to SVD basis
                            tensor2 TT;
                            TensorProductNTN (TT, U, SSS);
                            TensorProduct    (SSS, TT, V);
                        }

                        // needs scaling first
                        for (int r=0; r<RT; ++r)
                            for (int s=0; s<RT; ++s)
                                SSS(r,s) *= 1./sqrt ( ss(0,r) * ss(0,s) );

                        // test occupation
                        {
                            tensor2 sss(1,RT);
                            tensor2 UU(RT), VV(RT);

                            SVD (SSS, UU, VV, sss[0]);

                            Messenger.precision(16);
                            Messenger << "occupation : " << sss << endl;
                            Messenger.precision(7);
                        }
                    }

                    // check rest of space
                    {
                        tensor2 SN(RT); {
                            tensor2 AZZ; // ZB * BB^-1 * BZ

                            tensor2 EBZ;
                            EBZ = BZ;

                            DiagonalScaling(EE, EBZ); // EE == BB^-1

                            TensorProduct(AZZ, ZB, EBZ);


                            SN.zeroize();

                            for (int r=0; r<NRD; ++r)
                                for (int s=0; s<NRD; ++s)
                                    SN  (RD+r,RD+s) = ZZ(r,s) - AZZ(r,s);
                        }


                        // rotate to SVD basis
                        tensor2 TT;
                        TensorProductNTN (TT, U, SN);
                        TensorProduct    (SN, TT, V);

                        // scale
                        for (int r=0; r<RT; ++r)
                            for (int s=0; s<RT; ++s)
                                SN(r,s) *= 1./sqrt ( ss(0,r) * ss(0,s) );

                        // test occupation
                        {
                            tensor2 sss(1,RT);
                            tensor2 UU(RT), VV(RT);

                            SVD (SN, UU, VV, sss[0]);

                            Messenger.precision(16);
                            Messenger << "occupation : " << sss << endl;
                            Messenger.precision(7);
                        }

                    }

                    // project SS onto subspace orthonormal to UU & VV ;
                    // find optimal trial vector rotations from PCA
                    {

                    }
                    */

                }

                ZS.V.n = ZA.V.n = NNW;

                // this should resize arrays instead
                // check if joint subspace exceeds dimension
                if (RD+NRD>MRD) break;

                // check that there are enough Ds and Fs to treat this
                if (NRD>MNW)    break;

                // find the optimal linear combinations
                {
                    multivec nbs; nbs.setsize (bs, RD, NRD);
                    multivec nba; nba.setsize (ba, RD, NRD);

                    // this may be the transpose
                    multivec zs; zs.setsize (ZS, 0, NRD);
                    multivec za; za.setsize (ZA, 0, NRD);

                    nbs = zs;
                    nba = za;

                    //nbs.set(RS, zs);
                    //nba.set(RA, za);
                }

            }

            // biorthonormalize with respect to the energy and previous basis
            else if (nt==0) {

                /*
                //
                if (RD>0) {
                    multivec zs; zs.setsize (ZS, 0, NNW);
                    multivec za; za.setsize (ZA, 0, NNW);

                    // residuals in the new basis
                    tensor2 RRS, RRA, RIS, RIA;

                    //RRS = zs*ZZZ[0];
                    //RIS = zs*ZZZ[1];
                    //RRA = za*ZZZ[2];
                    //RIA = za*ZZZ[3];

                    // gradients in the new basis
                    tensor2 GRS, GRA, GIS, GIA;

                    GRS = zs*RR[0];
                    GIS = zs*RR[1];
                    GRA = za*RR[2];
                    GIA = za*RR[3];

                    // old solutions projected onto new subspace
                    RRS += GRS;
                    RIS += GIS;
                    RRA += GRA;
                    RIA += GIA;

                    // this has been already computed, but redo just in case
                    //tensor2 Xrs, Xis, Xra, Xia;
                    //          Xrs = RR[0] * obs;
                    //if (DoIS) Xis = RR[1] * obs;
                    //if (DoRA) Xra = RR[2] * oba;
                    //if (DoIA) Xia = RR[3] * oba;
                    //PseudoInverse(Xrs);
                    //tensor2 Ars, Ais, Ara, Aia;
                    //TensorProductNNT (Ars , RRS, Xrs);
                }

                // also, make  them orthogonal to S[2]
                if (RD>0) {

                    tensor2 AS, SA;

                    AS = ZA * obs;
                    SA = ZS * oba;

                    Messenger << "cross-overlap with previous basis : " << endl << SA << endl << AS << endl << endl;

                    //ZS.msub(ES, obs);
                    //ZA.msub(EA, oba);
                }
                */

                // make trial vectors conjugate wrt E[2] to the previous basis
                if (RD>0) {
                    tensor2 ES, EA;

                    ES = ZS * ors;
                    EA = ZA * ora;

                    for (int n=0; n<NNW; ++n)
                        for (int r=0; r<RD; ++r) {
                            ES(n,r) /= EE[r];
                            EA(n,r) /= EE[r];
                        }

                    //Messenger << "orthogonalization weights with previous basis : " << endl << ES << endl << EA << endl << endl;
                    ZS.msub(ES, obs);
                    ZA.msub(EA, oba);
                }


                // select optimal trial vector linear combinations
                {
                    // diagonalize the system
                    NRD = SolvePaired (ZS, ZA, pcs, pca, NNW);
                    Messenger << "  Number of new trial vectors = " << NRD << endl;
                }

                // build overlap cross-terms with the old basis
                /*
                if (RD>0) {
                    multivec zs; zs.setsize (ZS, 0, NRD);
                    multivec za; za.setsize (ZA, 0, NRD);

                    tensor2 AS, SA;

                    SA = zs * oba;
                    AS = za * obs;

                    //Messenger << "cross-overlap with previous basis : " << endl << SA << endl << AS << endl << endl;

                    zs.msub(SA, obs);
                    za.msub(AS, oba);


                    tensor2 SS;

                    SS = zs * za; //

                    // do the SVD
                    tensor2 U(NRD), V(NRD), ss(1,NRD);

                    if (NodeGroup.IsMaster()) {

                        SVD (SS, V, U, ss[0]);
                        V.Transpose();

                        Messenger << "SVD : " << ss << endl;
                    }
                    // share results with all other processes
                    {
                        Tensor2Broadcast(ss);
                        Tensor2Broadcast(U);
                        Tensor2Broadcast(V);
                    }



                    // select optimal trial vector linear combinations
                    {
                        double tr2 =0;

                        for (int n=0; n<NRD; ++n)
                            if (ss[n]>0)
                                tr2 += ss[0][n]; // positive trace

                        double ts2 =0;

                        int NNRD;

                        for (NNRD=0; NNRD<NRD;) {
                            if (ss[NRD]>0) {
                                ts2 += ss[0][NNRD];
                                ++NNRD;
                                if ( (tr2-ts2) < Sthresh*tr2)
                                    break;
                            }
                            else break;
                        }
                        NRD = NNRD;

                        Messenger << "  New number of new trial vectors = " << NRD << endl;
                    }

                    // rotate to the appropriate basis
                    zs.rot(V);
                    za.rot(U);
                }
                */

                //NRD = NNW;

                // select optimal trial vector linear combinations
                {
                    Messenger << "  Number of new trial vectors = " << NRD << endl;
                }

                // this should resize arrays instead
                // check if joint subspace exceeds dimension
                if (RD+NRD>MRD) break;

                // check that there are enough Ds and Fs to treat this
                if (NRD>MNW)    break;

                // copy the block
                {
                    nbs.setsize (bs, RD, NRD);
                    nba.setsize (ba, RD, NRD);

                    multivec zs; zs.setsize (ZS, 0, NRD);
                    multivec za; za.setsize (ZA, 0, NRD);

                    nbs = zs;
                    nba = za;
                }

            }

            // biorthonormalize preconditioned basis with previous basis; pair the vectors
            else if (0) {
                // first project out the previous subspace
                if (RD>0) {
                    tensor2 SA, AS;

                    SA = ZS * oba;
                    AS = ZA * obs;

                    ZS.msub(SA, obs);
                    ZA.msub(AS, oba);
                }

                // test HOSVD
                if (0) {
                    tensor3 TS (NNW, nO, nV, ZS.V[0]);
                    tensor3 TA (NNW, nO, nV, ZA.V[0]);

                    HOSVD(TS);
                    HOSVD(TA);
                }

                // transform
                tensor2 SS;

                SS = ZS * ZA; //

                // do the SVD
                tensor2 U(NNW), V(NNW);
                double ss[NNW];

                if (NodeGroup.IsMaster()) {

                    SVD (SS, V, U, ss);
                    V.Transpose();
                }
                // share results with all other processes
                {
                    NodeGroup.Broadcast(ss, NNW);

                    Tensor2Broadcast(U);
                    Tensor2Broadcast(V);
                }


                // select optimal trial vector linear combinations
                {
                    double tr2 =0;

                    for (int n=0; n<NNW; ++n)
                        if (ss[n]>0)
                            tr2 += ss[n]; // positive trace

                    double ts2 =0;

                    for (NRD=0; NRD<NNW;) {
                        if (ss[NRD]>0) {
                            ts2 += ss[NRD];
                            ++NRD;
                            if ( (tr2-ts2) < Sthresh*tr2)
                                break;
                        }
                        else break;
                    }

                    if (RSP.MV>0)   NRD = min(NRD, RSP.MV); // do not surpass the maximum number of vectors
                    if (RD+NRD>MRD) NRD = MRD-RD;           // use remaining vectors in a last attempt

                    Messenger << "  Number of new trial vectors = " << NRD << endl;
                }

                // this should resize arrays instead
                // check if joint subspace exceeds dimension
                if (RD+NRD>MRD) break;

                // check that there are enough Ds and Fs to treat this
                if (NRD>MNW)    break;

                // find the optimal linear combinations
                {
                    nbs.setsize (bs, RD, NRD);
                    nba.setsize (ba, RD, NRD);

                    U.n = V.n = NRD;

                    nbs.mul (V, ZS);
                    nba.mul (U, ZA);
                }

            }




            // "normalize" in the sense that s*a = 1 ; s^2 = a^2
            if (0) {
                double sa[NRD], is[NRD], ia[NRD];

                nbs.norm(is);
                nba.norm(ia);
                nbs.inner(sa, nba); // this should be 1 from the previous step

                for (int n=0; n<NRD; ++n) {

                    double isa = 1./sqrt(fabs(sa[n])); // split the burden of the scaling equally; the factor of 2 comes from the definition of the inner product

                    double fs = sqrt(sqrt(is[n])); // ^1/4
                    double fa = sqrt(sqrt(ia[n])); // ^1/4

                    is[n] = isa * (fa / fs);
                    ia[n] = isa * (fs / fa);

                    // make s*a positive
                    if (sa[n]<0.) ia[n] = -ia[n];
                }

                nbs.scale(is);
                nba.scale(ia);

                // check OK
                //nbs.norm(is);
                //nba.norm(ia);
                //nbs.inner(sa, nba);
                //for (int n=0; n<NRD; ++n) Messenger << n << " : " <<  is[n] << " " << ia[n] << "   " << sa[n] << endl;
            }
            // this was unphysical (although harmless):

            // normalize with S and "balance" the norm of the vectors wrt E0
            else if (0) {
                double sa[NRD], is[NRD], ia[NRD];

                nbs.norm(is, pcs);
                nba.norm(ia, pca);

                nbs.inner(sa, nba);

                for (int n=0; n<NRD; ++n) {
                    is[n] = 1./sqrt(is[n]);
                    ia[n] = 1./sqrt(ia[n]);
                    sa[n] *= is[n]*ia[n];

                    double isa = 1./sqrt(fabs(sa[n])); // to scale both vecs by the same factor

                    is[n] *= isa;
                    ia[n] *= isa;

                    if (sa[n]<0) ia[n] = -ia[n];
                }

                nbs.scale(is);
                nba.scale(ia);
            }


            // find occupations in residual
            /*
            if (1) {

                tensor2 Xrs, Xra, Xis, Xia; // override global definition

                          Xrs = ZZZ[0] * nbs; // NNW x NRD
                if (DoIS) Xis = ZZZ[1] * nbs;
                if (DoRA) Xra = ZZZ[2] * nba;
                if (DoIA) Xia = ZZZ[3] * nba;

                // add contributions;
                tensor2 TT;

                TensorProductNTN (TT,Xrs,Xrs);
                TensorProductNTN (TT,Xis,Xis,1.);
                TensorProductNTN (TT,Xra,Xra,1.);
                TensorProductNTN (TT,Xia,Xia,1.);

                tensor2 tt(1,TT.n);

                DiagonalizeV(TT,tt[0],true);

                Messenger << "gradient occupation "  << endl << tt<< endl;

                // rotate to maximum occupancy

                nbs.rot(TT);
                nba.rot(TT);

                // discard NRD
                {
                    double tr2 =0;

                    for (int n=0; n<NRD; ++n)
                        if (tt[0][n]>0)
                            tr2 += tt[0][n]; // positive trace

                    double ts2 =0;

                    for (NRD=0; NRD<NNW;) {
                        if (tt[0][NRD]>0) {
                            ts2 += tt[0][NRD];
                            ++NRD;
                            if ( (tr2-ts2) < Sthresh*tr2)
                                break;
                        }
                        else break;
                    }
                }

                Messenger << "new NRD = " << NRD << endl;
            }
            */

        }


        //    TRANSFORM TRIAL PERTURBATIONS FROM MO TO AO BASIS
        //    COMPUTE CONTRIBUTIONS
        //    TRANSFORM E[2] PERTURBATIONS FROM AO TO MO BASIS
        {
            // temporary half-transformed tensor
            tensor2 HT(nO,nA);

            tensor2 BS(nO,nV), BA(nO,nV);

            nrs.setsize(rhos, RD, NRD);
            nra.setsize(rhoa, RD, NRD);

            // transform to AO and add symmetric and antisymmetric parts
            for (int d=0; d<NRD; ++d) {

                // gather the vectors
                nbs.get(BS, d);
                nba.get(BA, d);

                /*
                // initialize matrices (if needed)
                tensor2 & DS = DD[d];
                tensor2 & DA = DD[NRD+d];

                DS.setsize(nA);
                DA.setsize(nA);
                */
                tensor2 DS(nA), DA(nA);

                // transform to AO
                TensorProduct    (HT, BS, CV);  // i,m = bs(i,a)*CV(a,m)
                TensorProductNTN (DS, CO, HT);  // n,m = CO(i,n)+ * HT(i,m)

                TensorProduct    (HT, BA, CV);  // i,m = ba(i,a)*CV(a,m)
                TensorProductNTN (DA, CO, HT);  // n,m = CO(i,n)+ * HT(i,m)

                // a/symmetrize & combine
                // ()
                DS.Symmetrize();
                DA.AntiSymmetrize();

                DS *= 0.5;
                DA *= 0.5;

                //DS += DA;
                DD[d]  = DS;
                DD[d] += DA;

                // avoid divergence
                Tensor2Broadcast(DD[d]);
            }

            // compute 2e- contributions from ERIs and DFT
            {
                for (int n=0; n<3*NRD; ++n) FF[n].setsize(nA); // the matrices are zeroed in EFS
                //for (int n=0; n<NRD; ++n) FF[n].setsize(nA); // the matrices are zeroed in EFS

                tensor2 * JJ = FF;
                tensor2 * KK = FF+NRD;
                tensor2 * AA = FF+2*NRD;

                //const double fxc = dft->GetHFK();

                /*
                // assemble Fock/KS matrices
                {
                    //Messenger << "Computing Coulomb and HF exchange contributions" << endl;

                    efs->Contraction(JJ, KK, AA, DD, NRD, Dthresh, Dthresh);

                    // F = H + 2J - K
                    for (int n=0; n<NRD; ++n) JJ[n] *=    2; // the first half are symmetric, the other, antisymmetric, so their J=0
                    for (int n=0; n<NRD; ++n) KK[n] *= -fxc;
                    for (int n=0; n<NRD; ++n) AA[n] *= -fxc;
                }
                */

                // deactivate until MO transformation is working
                //if (!UseCholesky) {
                if (true) {

                    //efs->Contraction(FF, NULL, NULL, DD, NRD, Dthresh, Dthresh, false);
                    efs->Contraction(JJ, KK, AA, DD, NRD, Dthresh, Dthresh);

                    // account for the factor of 2 missing in EFS for J-0.5*K
                    //for (int n=0; n<NRD; ++n) FF[n] *= 2;
                }
                else {
                    efs->ContractionCholesky (FF, DD, NRD, RSP.CDthresh);
                    efs->Contraction         (NULL, KK, AA, DD, NRD, Dthresh, Dthresh);
                }

                // F = H + 2J - x K
                {
                    const double fxc = dft->GetHFK();

                    for (int n=0; n<NRD; ++n) JJ[n] *=    2; // the first half are symmetric, the other, antisymmetric, so their J=0

                    if (fxc>0) {
                        for (int n=0; n<NRD; ++n) KK[n] += AA[n];           // add S/A HF exchange contributions
                        for (int n=0; n<NRD; ++n) FF[n].FMA (KK[n], -fxc);  // add the weighted contributions to the pseudoFock/KS matrices
                    }
                }


                if (dft->IsInit()) {
                    Messenger << "Computing Exchange-correlation contributions"; Messenger.Push();
                    //dft->CalcXCrsp(KK, DD, NRD, D0);    // compute DFT contribution to XC; use only symmetric densities (first half)
                    dft->CalcXCrsp(FF, DD, NRD, D0);    // compute DFT contribution to XC; use only symmetric densities (first half)
                    Messenger.Pop();
                }

                // first NRD contain symmetric components; the second, antisymmetric
                //for (int n=0; n<NRD; ++n) JJ[n] += KK[n];

                // scale by 2, according to the A+B, A-B rotation
                //for (int n=0; n<2*NRD; ++n) FF[n] *= 2;

                for (int n=0; n<NRD; ++n) FF[n] *= 2;
            }

            // transform back to MO
            for (int d=0; d<NRD; ++d) {

                tensor2 FS = FF[d];
                tensor2 FA = FF[d];

                FS.Symmetrize();     FS*=0.5;
                FA.AntiSymmetrize(); FA*=0.5;

                TensorProduct    (HT, CO, FS);  // i,m = CO(i,n)*FS(n,m)
                TensorProductNNT (BS, HT, CV);  // i,a = HT(i,m)+ * CV(m,a)+

                TensorProduct    (HT, CO, FA);  // i,m = CO(i,n)*FS(n,m)
                TensorProductNNT (BA, HT, CV);  // i,a = HT(i,m)+ * CV(m,a)+

                // avoid divergence
                Tensor2Broadcast(BS);
                Tensor2Broadcast(BA);

                // "scatter"
                nrs.set(d,BS);
                nra.set(d,BA);
            }

            // add the diagonal contribution
            // (delete in the long term)
            nrs.fma(ae, nbs);
            nra.fma(ae, nba);
        }

        // increment solver iteration counter
        // & update subspace dimension
        ++nt;
        RD += NRD;

        obs.setsize(bs, 0, RD);
        oba.setsize(ba, 0, RD);
        ors.setsize(rhos, 0, RD);
        ora.setsize(rhoa, 0, RD);


        //    BUILD THE REDUCED SPACE
        //    DIAGONALIZE THE REDUCED SPACE
        //    ROTATE BASIS ACCORDINGLY
        {
            Messenger.Push("Rebuilding reduced space");

            // this should be an update rather than recomputing everything;
            // and so should the diagonalization step
            tensor2 ES(RD), EA(RD), SA(RD); // contains the RS energy blocks

            ES = ors * obs;
            EA = ora * oba;
            SA = oba * obs;


            tensor2 RS(RD), RA(RD);

            if (NodeGroup.IsMaster()) {
                SolvePaired (RS,RA,EE,  ES,EA,SA,RD);
            }
            {
                Tensor2Broadcast    (RS);
                Tensor2Broadcast    (RA);
                NodeGroup.Broadcast (EE, RD);
            }


            // generate new approximate (Krylov) eigenvectors
            // by rotating old and new subspaces
            obs.rot(RS);
            oba.rot(RA);
            ors.rot(RS);
            ora.rot(RA);


            // cutoff high energies
            // unless the energy excluded is a real case of bad numerical noise,
            // this  will typically mess up the calculation by stopping convergence;
            // seemingly "bad" eigenpairs will "fill" the place and prevent others from
            // going towards the same direction
            if (0) {
                int R; for (R=0; R<RD; ++R) if (EE[R]>Emax) break;
                if (R<RD) Messenger << "discarded " << RD-R << " high energy eigenpairs during diagonalization" << endl;
                RD = R;
            }

            Messenger.Pop();
        }



        // what's the spectrum of (E,E0) ?
        // (how bad is the preconditioner?)
        {
            tensor2 E0S, E0A;

            E0S = obs.inner(pcs,pcf);
            E0A = oba.inner(pca);

            double f[RD];
            for (int r=0; r<RD; ++r) f[r] = 1./sqrt(EE[r]);

            for (int r=0; r<RD; ++r)
                for (int s=0; s<RD; ++s) {
                    E0S(r,s) *= f[r] * f[s];
                    E0A(r,s) *= f[r] * f[s];
                }

            {
                tensor2 gs(1,RD), ga(1,RD);
                DiagonalizeE(E0S,gs[0]);
                DiagonalizeE(E0A,ga[0]);
                for (int r=0; r<RD; ++r) gs[0][r] = 1./gs[0][r];
                for (int r=0; r<RD; ++r) ga[0][r] = 1./ga[0][r];

                Messenger << "(E0,E) S evs : " << gs << endl;
                Messenger << "(E0,E) A evs : " << ga << endl;
            }
        }


        //    SOLVE THE REDUCED SPACE IN DIAGONAL FORM
        //    COMPUTE NEW RESIDUALS
        //    TEST FOR CONVERGENCE
        {
            Messenger.Push("Computing residuals");

            // MINRES
            // ======

            // this is a not very efficient implementation of MINRES
            // used for debugging purposes only; its convergence in
            // practice is similar but slightly worse than the pure
            // projection method
            if (0) {
                tensor2 eess, eeaa, ebsa, ebas, besa, beas, bbss, bbaa;

                eess = ors*ors;
                eeaa = ora*ora;
                ebsa = ors*oba;
                ebas = ora*obs;
                besa = obs*ora;
                beas = oba*ors;
                bbss = obs*obs;
                bbaa = oba*oba;

                ZZ[0].V.zeroize();
                ZZ[1].V.zeroize();
                ZZ[2].V.zeroize();
                ZZ[3].V.zeroize();


                tensor2 ges, gba;

                // only RS component
                ges = RR[0] * ors;
                gba = RR[0] * oba;


                // for all equations
                for (int n=0; n<NW; ++n) {

                    // right hand side
                    tensor2 rr(4,RD);

                    for (int r=0; r<RD; ++r) {
                        rr(0,r) =          ges(n,r); // RS
                        rr(1,r) = 0;                 // IS
                        rr(2,r) = -wr[n] * gba(n,r); // RA
                        rr(3,r) =  wi[n] * gba(n,r); // IA
                    }

                    // build all the (A bi) (A bj) inner products


                    tensor2 M(4*RD);
                    M.zeroize();

                    for (int r=0; r<RD; ++r) {
                        for (int s=0; s<RD; ++s) {
                            M (0*RD+r,0*RD+s) = eess(r,s) + (wr[n]*wr[n]+wi[n]*wi[n]) * bbss(r,s);
                            M (0*RD+r,1*RD+s) = 0;
                            M (0*RD+r,2*RD+s) = -wr[n] * (ebsa(r,s) + besa(r,s));
                            M (0*RD+r,3*RD+s) =  wi[n] * (ebsa(r,s) - besa(r,s));

                            M (1*RD+r,0*RD+s) = 0;
                            M (1*RD+r,1*RD+s) = eess(r,s) + (wr[n]*wr[n]+wi[n]*wi[n]) * bbss(r,s);
                            M (1*RD+r,2*RD+s) = -wi[n] * (ebsa(r,s) - besa(r,s));
                            M (1*RD+r,3*RD+s) = -wr[n] * (ebsa(r,s) + besa(r,s));

                            M (2*RD+r,0*RD+s) = -wr[n] * (beas(r,s) + ebas(r,s));
                            M (2*RD+r,1*RD+s) = -wi[n] * (beas(r,s) - ebas(r,s));
                            M (2*RD+r,2*RD+s) = eeaa(r,s) + (wr[n]*wr[n]+wi[n]*wi[n]) * bbaa(r,s);
                            M (2*RD+r,3*RD+s) = 0;

                            M (3*RD+r,0*RD+s) =  wi[n] * (beas(r,s) - ebas(r,s));
                            M (3*RD+r,1*RD+s) = -wr[n] * (beas(r,s) + ebas(r,s));
                            M (3*RD+r,2*RD+s) = 0;
                            M (3*RD+r,3*RD+s) = eeaa(r,s) + (wr[n]*wr[n]+wi[n]*wi[n]) * bbaa(r,s);
                        }
                    }

                    // solve the system
                    SolveLinear(M, rr[0]);


                    // now build the solutions
                    for (int r=0; r<RD; ++r) {
                        for (int ia=0; ia<nOV; ++ia) {
                            // RS coefficients
                            ZZ[0].V(n, ia) += rr(0,r) * ors.V(r,ia);
                            ZZ[1].V(n, ia); // no contribution
                            ZZ[2].V(n, ia) -= rr(0,r) * obs.V(r,ia) * wr[n];
                            ZZ[3].V(n, ia) += rr(0,r) * obs.V(r,ia) * wi[n];

                            // IS coefficients
                            ZZ[0].V(n, ia); // no contribution
                            ZZ[1].V(n, ia) -= rr(1,r) * ors.V(r,ia);
                            ZZ[2].V(n, ia) += rr(1,r) * obs.V(r,ia) * wi[n];
                            ZZ[3].V(n, ia) += rr(1,r) * obs.V(r,ia) * wr[n];

                            // RA coefficients
                            ZZ[0].V(n, ia) -= rr(2,r) * oba.V(r,ia) * wr[n];
                            ZZ[1].V(n, ia) += rr(2,r) * oba.V(r,ia) * wi[n];
                            ZZ[2].V(n, ia) += rr(2,r) * ora.V(r,ia);
                            ZZ[3].V(n, ia); // no contribution

                            // IA coefficients
                            ZZ[0].V(n, ia) += rr(3,r) * oba.V(r,ia) * wi[n];
                            ZZ[1].V(n, ia) += rr(3,r) * oba.V(r,ia) * wr[n];
                            ZZ[2].V(n, ia); // no contribution
                            ZZ[3].V(n, ia) -= rr(3,r) * ora.V(r,ia);
                        }
                    }

                }

                ZZ[1] *= -1.;
                ZZ[3] *= -1.;


                // add the rhs;
                          ZZ[0] -= RR[0];
                if (DoIS) ZZ[1] -= RR[1];
                if (DoRA) ZZ[2] -= RR[2];
                if (DoIA) ZZ[3] -= RR[3];

                // norm of residual
                {
                    // compute norms
                    tensor2 N2(4,NW);

                              ZZ[0].norm(N2[0]);
                    if (DoIS) ZZ[1].norm(N2[1]);
                    if (DoRA) ZZ[2].norm(N2[2]);
                    if (DoIA) ZZ[3].norm(N2[3]);

                    tensor2 R2(1,NW); R2.zeroize();

                              R2 += tensor2(1,NW, N2[0]);
                    if (DoIS) R2 += tensor2(1,NW, N2[1]);
                    if (DoRA) R2 += tensor2(1,NW, N2[2]);
                    if (DoIA) R2 += tensor2(1,NW, N2[3]);

                    for (int nw=0; nw<NW; ++nw) {
                        double rr = std::sqrt(R2[0][nw]);
                        double cc = rr/gg[0][nw];      // regular equations
                        Messenger << cc;
                    }

                    Messenger << endl << endl;
                }
            }


            // solve reduced space equations in diagonal form
            // ===============================================
            else {

                ZZ[0].V.zeroize();
                ZZ[1].V.zeroize();
                ZZ[2].V.zeroize();
                ZZ[3].V.zeroize();

                // project gradients (rhs)
                // onto total basis
                          Xrs = RR[0] * obs;
                if (DoIS) Xis = RR[1] * obs;
                if (DoRA) Xra = RR[2] * oba;
                if (DoIA) Xia = RR[3] * oba;


                //for (int nw=0; nw<NW; ++nw) for (int n=0; n<RD; ++n) Xrs(nw,n) = G1s[n];
                //for (int nw=0; nw<NW; ++nw) for (int n=0; n<RD; ++n) Xra(nw,n) = G1a[n];

                // compute new residuals
                //      E.Rs , - w S.Ra        Xs      gs     rs
                // (                     ) . (    ) - (  ) = (  )
                //   -w S.Rs ,    E Ra         Xa      ga     ra

                // by left-projection onto the Rs/Ra basis

                switch (RSP.mode) {

                  case RSPparams::RSPmode::CPP : {

                    // solve reduced space equations in diagonal form
                    // ===============================================
                    for (int nw=0; nw<NW; ++nw)
                        InverseCPP (Xrs[nw], Xra[nw], Xis[nw], Xia[nw], EE, wr[nw], wi[nw], RD);

                    tensor2 Yrs(NW, RD), Yra(NW, RD), Yis(NW, RD), Yia(NW, RD);

                    for (int nw=0; nw<NW; ++nw) {
                        for (int n=0; n<RD; ++n) {
                            Yrs(nw,n) = -Xra(nw,n) * wr[nw] + Xia(nw,n) * wi[nw];
                            Yra(nw,n) = -Xrs(nw,n) * wr[nw] + Xis(nw,n) * wi[nw];
                            Yis(nw,n) =  Xia(nw,n) * wr[nw] + Xra(nw,n) * wi[nw];
                            Yia(nw,n) =  Xis(nw,n) * wr[nw] + Xrs(nw,n) * wi[nw];
                        }
                    }

                    ZZ[0].mul (Yrs, oba); //    E[2] * Xrs - w S[2] * Xra    +    g S[2] * Xia
                    ZZ[0].madd(Xrs,ors); //

                    ZZ[1].mul (Yis, oba); //  g S[2] * Xra                   -      E[2] * Xis + w S[2] * Xia
                    ZZ[1].msub(Xis,ors); //

                    ZZ[2].mul (Yra, obs); // -w S[2] * Xrs +   E[2] * Xra    +                   g S[2] * Xis
                    ZZ[2].madd(Xra,ora); //

                    ZZ[3].mul (Yia, obs); //                 g S[2] * Xrs    +    w S[2] * Xis   - E[2] * Xia
                    ZZ[3].msub(Xia,ora); //

                    // remember to rotate the imaginary components
                    // according to the symmetrization scheme

                    //ZZ[1] *= -1.;
                    //ZZ[3] *= -1.;

                    break;
                  }


                  case RSPparams::RSPmode::REAL: {

                    for (int nw=0; nw<NW; ++nw)
                        InverseReal (Xrs[nw], Xra[nw], EE, wr[nw], RD);

                    tensor2 Yrs(NW, RD), Yra(NW, RD);

                    for (int nw=0; nw<NW; ++nw) {
                        for (int n=0; n<RD; ++n) {
                            Yrs(nw,n) = -Xra(nw,n) * wr[nw];
                            Yra(nw,n) = -Xrs(nw,n) * wr[nw];
                        }
                    }

                    ZZ[0].mul (Yrs, oba);
                    ZZ[0].madd(Xrs,ors);

                    ZZ[2].mul (Yra, obs);
                    ZZ[2].madd(Xra,ora);

                    break;
                  }

                  case RSPparams::RSPmode::IMAG: {

                    for (int nw=0; nw<NW; ++nw)
                        InverseImag (Xrs[nw], Xia[nw], EE, wi[nw], RD);

                    tensor2 Yrs(NW, RD), Yia(NW, RD);

                    for (int nw=0; nw<NW; ++nw) {
                        for (int n=0; n<RD; ++n) {
                            Yrs(nw,n) =  Xia(nw,n) * wi[nw];
                            Yia(nw,n) =  Xrs(nw,n) * wi[nw];
                        }
                    }

                    ZZ[0].mul (Yrs, oba);
                    ZZ[0].madd(Xrs,ors);

                    ZZ[3].mul (Yia, obs);
                    ZZ[3].msub(Xia,ora);

                    //ZZ[3] *= -1.;

                    break;
                  }

                }

                // add the rhs;
                          ZZ[0] -= RR[0];
                if (DoIS) ZZ[1] += RR[1];
                if (DoRA) ZZ[2] -= RR[2];
                if (DoIA) ZZ[3] += RR[3];
            }



            tensor2 rms(1,NW);

            // check convergence (only for supplied ones)
            NNW = NW; // reset total funcs
            {
                // compute norms
                tensor2 N2(4,NW);

                          ZZ[0].norm(N2[0]);
                if (DoIS) ZZ[1].norm(N2[1]);
                if (DoRA) ZZ[2].norm(N2[2]);
                if (DoIA) ZZ[3].norm(N2[3]);

                tensor2 R2(1,NW); R2.zeroize();

                          R2 += tensor2(1,NW, N2[0]);
                if (DoIS) R2 += tensor2(1,NW, N2[1]);
                if (DoRA) R2 += tensor2(1,NW, N2[2]);
                if (DoIA) R2 += tensor2(1,NW, N2[3]);

                for (int nw=0; nw<NW; ++nw) {
                    double rr = std::sqrt(R2[0][nw]);
                    double cc = rr/gg[0][nw];      // regular equations
                    rms(0,nw) = cc;
                }
            }

            Messenger.Pop();

            // print cycle followed by convergence ratios
            {
                Messenger << "     " << nt;

                bool conv = true;

                for (int nw=0; nw<NW; ++nw) {
                    Messenger << rms(0,nw);
                    if (rms(0,nw)>Cthresh) conv = false;
                }
                Messenger << endl;

                if (conv) {
                    Messenger << endl;
                    break;
                }
                else if (nt==NT-1) {
                    Messenger << endl;
                    Messenger << "Response solver did not converge in " << NT <<  " iterations!" << endl;
                    break;
                }
            }

        }

    }

    Messenger << "Response solver converged in " << nt <<  " iterations " << endl;
    Messenger << "Dimension of solution subspace = " << RD << endl;


    // what's the spectrum of (E,E0) ?
    // (how bad is the preconditioner?)
    {
        tensor2 E0S, E0A;

        E0S = obs.inner(pcs,pcf);
        E0A = oba.inner(pca);

        double f[RD];
        for (int r=0; r<RD; ++r) f[r] = 1./sqrt(EE[r]);

        for (int r=0; r<RD; ++r)
            for (int s=0; s<RD; ++s) {
                E0S(r,s) *= f[r] * f[s];
                E0A(r,s) *= f[r] * f[s];
            }

        {
            tensor2 gs(1,RD), ga(1,RD);
            DiagonalizeE(E0S,gs[0]);
            DiagonalizeE(E0A,ga[0]);
            Messenger << "(E0,E) S evs : " << gs << endl;
            Messenger << "(E0,E) A evs : " << ga << endl;
        }
    }

    // this can be used to determine convergence properties
    {
        tensor2 SS, SA;

        SS = obs.inner();
        SA = oba.inner();

        /*
        double f[RD];
        for (int r=0; r<RD; ++r) f[r] = 1./sqrt(EE[r]);

        for (int r=0; r<RD; ++r)
            for (int s=0; s<RD; ++s) {
                SS(r,s) *= f[r] * f[s];
                SA(r,s) *= f[r] * f[s];
            }
        */

        tensor2 E0S, E0A;

        E0S = obs.inner(pcs,pcf);
        E0A = oba.inner(pca);

        for (int r=0; r<RD; ++r) E0S(r,r) -= EE[r];
        for (int r=0; r<RD; ++r) E0A(r,r) -= EE[r];


        {
            tensor2 gs(1,RD), ga(1,RD);
            DiagonalizeGV(E0S, SS,gs[0]);
            DiagonalizeGV(E0A, SA,ga[0]);

            Messenger << "spectrum of E0-E (S) in the euclidean basis : " << gs << endl;
            Messenger << "spectrum of E0-E (A) in the euclidean basis : " << ga << endl;
        }
    }

    // this is extraordinarily expensive and unsuitable for large calculations
    // also, it doesnt work with E0S J update
    if (0) {
        ComputeCondition ( pcs.V[0], pca.V[0], wr, wi, nOV, NW/NR,
                           obs.V, oba.V, ors.V, ora.V);
    }

    // these are largely close to 1, which is quite good


    // do some preconditioner analysis for each equation;
    // copmutes the spèctral radius of (E0-wS)^-1 (E-E0)
    // in the basis we have computed
    if (0) {

        // diagonalize the projection of E0, which simplifies the many inverses
        tensor2 E0S, E0A;

        E0S = obs.inner(pcs,pcf);
        E0A = oba.inner(pca);


        tensor2 GS, GA; // 2e- energy terms
        GS = E0S;
        GA = E0A;
        for (int r=0; r<RD; ++r) GS(r,r) -= EE[r];
        for (int r=0; r<RD; ++r) GA(r,r) -= EE[r];


        // find a basis for GS & GA so their inverse is simplified
        tensor2 CS, CA, gs(1,RD), ga(1,RD);

        CS = GS;
        CA = GA;

        DiagonalizeV (CS, gs[0]);
        DiagonalizeV (CA, ga[0]);

        // rotate E0 to the new basis
    }
    else {

        // diagonalize the projection of E0, which simplifies the many inverses

        tensor2 E0S, E0A;

        E0S = obs.inner(pcs,pcf);
        E0A = oba.inner(pca);

        tensor2 CS, CA, es(1,RD), ea(1,RD);

        CS = E0S;
        CA = E0A;

        DiagonalizeV (CS, es[0]);
        DiagonalizeV (CA, ea[0]);


        tensor2 GS, GA; // 2e- energy terms

        WeightedInnerProduct (GS, CS, EE);
        WeightedInnerProduct (GA, CA, EE);

        GS *= -1;
        GA *= -1;

        for (int r=0; r<RD; ++r) GS(r,r) += es(0,r);
        for (int r=0; r<RD; ++r) GA(r,r) += ea(0,r);


        // check something real quick
        {
            tensor2 gs(1,RD), ga(1,RD);
            DiagonalizeE(GS,gs[0]);
            DiagonalizeE(GA,ga[0]);
            Messenger << "gs evs : " << gs << endl;
            Messenger << "ga evs : " << ga << endl;
        }
        // ok, gs contains a few negative eigenvalues

        // solve for every equation
        //for (int nw=0; nw<NW; ++nw)
        {
            // let's just try a simpler NxN case first
            tensor2 W(RD);
            tensor2 ie(1,RD);

            for (int r=0; r<RD; ++r) ie(0,r)=1./es(0,r);

            W = GS;
            DiagonalScaling(ie[0],W);

            // now diagonalize
            tensor2 w(2,RD);

            DiagonalizeNE (W, w[0],w[1]);

            Messenger << "eigenvalues of P^-1 G :" << endl << w << endl;
        }
    }


    // generate solution
    // *****************

    tensor2 R2(1,NW);

    {
                  ZZ[0].mul(Xrs, obs);
        if (DoIS) ZZ[1].mul(Xis, obs);
        if (DoRA) ZZ[2].mul(Xra, oba);
        if (DoIA) ZZ[3].mul(Xia, oba);

        // compute norms
        tensor2 N2(4,NW);

                  ZZ[0].norm(N2[0]);
        if (DoIS) ZZ[1].norm(N2[1]);
        if (DoRA) ZZ[2].norm(N2[2]);
        if (DoIA) ZZ[3].norm(N2[3]);

        R2.zeroize();

                  R2 += tensor2(1,NW, N2[0]);
        if (DoIS) R2 += tensor2(1,NW, N2[1]);
        if (DoRA) R2 += tensor2(1,NW, N2[2]);
        if (DoIA) R2 += tensor2(1,NW, N2[3]);
    }

    Messenger << " Z norms";
    for (int nw=0; nw<NW; ++nw) Messenger << sqrt(R2[0][nw]);
    Messenger << endl << endl;


    Messenger << " E[2]-wS[2] projected eigenvalues : ";
    for (int n=0; n<RD; ++n) Messenger << EE[n] << " ";
    Messenger << endl << endl;

    {
        tensor2 ee(1,RD);
        tensor2 RS(RD), RA(RD);
        tensor2 ES(RD), EA(RD), SA(RD); // contains the RS energy blocks

        ES = obs.inner(pcs,pcf);
        EA = oba.inner(pca);
        SA = oba * obs;

        if (NodeGroup.IsMaster()) {
            SolvePaired (RS, RA, ee[0], ES, EA, SA, RD);
        }

        Messenger << "E0[2]-wS[2] projected eigenvalues : " << ee << endl;
    }

    {
        obs.scale(EE);
        oba.scale(EE);

        ors -= oba;
        ora -= obs;

        tensor2 ns(2,RD);

        ors.norm(ns[0]);
        ora.norm(ns[1]);

        for (int n=0; n<RD; ++n) ns[0][n] = sqrt(ns[0][n]);
        for (int n=0; n<RD; ++n) ns[1][n] = sqrt(ns[1][n]);

        Messenger << " E[2]-wS[2] projected eigenvector residual norms : " << endl << ns << endl;
    }

    // free memory
    // ===========
    for (int nw=0; nw<NW; ++nw) {
        DD[nw].clear();
        FF[nw].clear();
    }

    delete[] DD, FF;

    if (UseWoodbury) delete[] WE; // this takes care of both E & G
}




#include "orbitals/OAObasis.hpp"

void MolElData::DoRSP () {


    if (RSP.mode == RSPparams::RSPmode::NONE) return; // no response


    NodeGroup.Sync();

    Messenger << "Initializing response" << endl;
    {
        C  = SCF.GetMOs();
    }

    // dimensions, number of orbitals, etc.


    const int nN = (RSP.NA>0)? min(RSP.NA, int(C.n)) : C.n + RSP.NA;    // NMO
    const int nA = C.m;    // NAO
    const int nO = npairs;
    const int nV = nN - nO;

    const int NW   = RSP.NW;

    Messenger << "NT, NA    = " << nA << " " << nN << endl;
    Messenger << "NO,NV,  NOV = " << nO << " " << nV << " " << nO*nV << endl;

    if (nV<0) throw "Bad dimension of active subspace!";


    // ===============================
    //  initialize property gradients
    // ===============================

    Messenger << "Computing property gradients" << endl;

    tensor3 Rmo(3,nO,nV);
    tensor2 & Xmo = Rmo[0];
    tensor2 & Ymo = Rmo[1];
    tensor2 & Zmo = Rmo[2];

    // compute transition dipoles for Fock canonical orbitals
    {

        if (0) {
            tensor2 XX,YY,ZZ;
            basis2AO (XX, X, C);
            basis2AO (YY, Y, C);
            basis2AO (ZZ, Z, C);

            Messenger << "X :" << endl << XX << endl;
            Messenger << "Y :" << endl << YY << endl;
            Messenger << "Z :" << endl << ZZ << endl;
        }

        // set two dummy tensors to map the occupied
        // and virtual orbitals of C
        tensor2 CO (nO,nA,   C[ 0] );
        tensor2 CV (nV,nA,   C[nO] );
        tensor2 Tmp; // temporary half-transformed tensor

        TensorProduct    (Tmp, CO,  X);  // nO,nA = nO,nA * nA,nA
        TensorProductNNT (Xmo, Tmp, CV); // nO,nV = nO,nA * (nV,nA)+

        TensorProduct    (Tmp, CO,  Y);
        TensorProductNNT (Ymo, Tmp, CV);

        TensorProduct    (Tmp, CO,  Z);
        TensorProductNNT (Zmo, Tmp, CV);


        // avoid potential numerical divergence across nodes
        Tensor2Broadcast (Xmo);
        Tensor2Broadcast (Ymo);
        Tensor2Broadcast (Zmo);

        /*
        // this will be useful for HOSVD later on
        if (0) {
            tensor2 R2(nO,nV);
            for (int ia=0; ia<nO*nV; ++ia) R2[0][ia] = Xmo[0][ia]*Xmo[0][ia] + Ymo[0][ia]*Ymo[0][ia] + Zmo[0][ia]*Zmo[0][ia];

            Messenger << "Norm^2 of gradients :" << endl << R2 << endl;
        }
        */

        //HOSVD (Rmo);
    }

    Messenger << endl << endl;


    // =====================================
    //  solve ( E[2] - w_k S[2] ) Z_k = R_k
    // =====================================

    const int NRW = 3; // number of cartesian coordinates, i.e. number of response gradients per frequency

    double  * wr = new double[NW*NRW];
    double  * wi = new double[NW*NRW];


    multivec R, R3[4], RR [NRW][4]; // right hand sides (gradients)
    multivec Z, Z3[4], ZZ [NRW][4]; // solutions

    R.setsize (4*NRW*NW, nO,nV);
    Z.setsize (4*NRW*NW, nO,nV);

    // loop over cartesian coords
    if (RSP.mode==RSPparams::RSPmode::CPP) {

        //for (int nw=0; nw<NW; ++nw) wr[nw] = RSP.ww[nw];
        //for (int nw=0; nw<NW; ++nw) wi[nw] = RSP.gg;

        /*
        // loop over components, i.e. RS IS RA IA
        RR[k][0].setsize (R, (3*0+k) * NW, NW);
        ZZ[k][0].setsize (Z, (3*0+k) * NW, NW);
        RR[k][2].setsize (R, (3*1+k) * NW, NW);
        ZZ[k][2].setsize (Z, (3*1+k) * NW, NW);
        */


        // concatenates the list of NW freqs NRW times
        for (int k=0; k<NRW; ++k) {
            for (int nw=0; nw<NW; ++nw)
                wr [k*NW + nw] = RSP.ww[nw];
            for (int nw=0; nw<NW; ++nw)
                wi [k*NW + nw] = RSP.gg;
        }

        // do the same with the merged vectors
        R3[0].setsize (R, 0*NRW*NW, NRW*NW); // RS
        R3[1].setsize (R, 1*NRW*NW, NRW*NW); // IS
        R3[2].setsize (R, 2*NRW*NW, NRW*NW); // RA
        R3[3].setsize (R, 3*NRW*NW, NRW*NW); // IA

        Z3[0].setsize (Z, 0*NRW*NW, NRW*NW); // RS
        Z3[1].setsize (Z, 1*NRW*NW, NRW*NW); // IS
        Z3[2].setsize (Z, 2*NRW*NW, NRW*NW); // RA
        Z3[3].setsize (Z, 3*NRW*NW, NRW*NW); // IA


        // make RR and ZZ point to the proper addresses in memory
        for (int k=0; k<NRW; ++k) {

            RR [k][0].setsize (R, (0*NRW + k) * NW, NW); // RS
            RR [k][1].setsize (R, (1*NRW + k) * NW, NW); // IS
            RR [k][2].setsize (R, (2*NRW + k) * NW, NW); // RA
            RR [k][3].setsize (R, (3*NRW + k) * NW, NW); // IA

            ZZ [k][0].setsize (Z, (0*NRW + k) * NW, NW);
            ZZ [k][1].setsize (Z, (1*NRW + k) * NW, NW);
            ZZ [k][2].setsize (Z, (2*NRW + k) * NW, NW);
            ZZ [k][3].setsize (Z, (3*NRW + k) * NW, NW);
        }

    }
    else if (RSP.mode==RSPparams::RSPmode::REAL) {

        //for (int nw=0; nw<NW; ++nw) wr[nw] = RSP.ww[nw];
        //for (int nw=0; nw<NW; ++nw) wi[nw] = 0;

        // concatenates the list of NW freqs NRW times
        for (int k=0; k<NRW; ++k) {
            for (int nw=0; nw<NW; ++nw)
                wr [k*NW + nw] = RSP.ww[nw];
            for (int nw=0; nw<NW; ++nw)
                wi [k*NW + nw] = 0;
        }


        // do the same with the merged vectors
        R3[0].setsize (R, 0*NRW*NW, NRW*NW); // RS
        R3[2].setsize (R, 1*NRW*NW, NRW*NW); // RA

        Z3[0].setsize (Z, 0*NRW*NW, NRW*NW); // RS
        Z3[2].setsize (Z, 1*NRW*NW, NRW*NW); // RA


        for (int k=0; k<NRW; ++k) {
            /*
            // loop over components, i.e. RS IS RA IA
            RR[k][0].setsize (R, (3*0+k) * NW, NW);
            ZZ[k][0].setsize (Z, (3*0+k) * NW, NW);
            RR[k][2].setsize (R, (3*1+k) * NW, NW);
            ZZ[k][2].setsize (Z, (3*1+k) * NW, NW);
            */

            RR [k][0].setsize (R, (0*NRW + k) * NW, NW);
            RR [k][2].setsize (R, (1*NRW + k) * NW, NW);

            ZZ [k][0].setsize (Z, (0*NRW + k) * NW, NW);
            ZZ [k][2].setsize (Z, (1*NRW + k) * NW, NW);
        }    }
    else if (RSP.mode==RSPparams::RSPmode::IMAG) {

        //for (int nw=0; nw<NW; ++nw) wr[nw] = 0;
        //for (int nw=0; nw<NW; ++nw) wi[nw] = RSP.ww[nw];

        // concatenates the list of NW freqs NRW times
        for (int k=0; k<NRW; ++k) {
            for (int nw=0; nw<NW; ++nw)
                wr [k*NW + nw] = 0;
            for (int nw=0; nw<NW; ++nw)
                wi [k*NW + nw] = RSP.ww[nw];
        }


        // do the same with the merged vectors
        R3[0].setsize (R, 0*NRW*NW, NRW*NW); // RS
        R3[3].setsize (R, 1*NRW*NW, NRW*NW); // IA

        Z3[0].setsize (Z, 0*NRW*NW, NRW*NW); // RS
        Z3[3].setsize (Z, 1*NRW*NW, NRW*NW); // IA


        for (int k=0; k<NRW; ++k) {
        /*
            // loop over components, i.e. RS IS RA IA
            RR[k][0].setsize (R, (3*0+k) * NW, NW);
            ZZ[k][0].setsize (Z, (3*0+k) * NW, NW);
            RR[k][3].setsize (R, (3*1+k) * NW, NW);
            ZZ[k][3].setsize (Z, (3*1+k) * NW, NW);
            */
            RR [k][0].setsize (R, (0*NRW + k) * NW, NW);
            RR [k][3].setsize (R, (1*NRW + k) * NW, NW);

            ZZ [k][0].setsize (Z, (0*NRW + k) * NW, NW);
            ZZ [k][3].setsize (Z, (1*NRW + k) * NW, NW);
        }    }
    else {
        throw 13412; // RSP MODE IS UNSET !
    }

    // set the real, symmetric RHS components for each frequency and polarization direction
    for (int nw=0; nw<NW; ++nw) RR[0][0].set(nw,Xmo);
    for (int nw=0; nw<NW; ++nw) RR[1][0].set(nw,Ymo);
    for (int nw=0; nw<NW; ++nw) RR[2][0].set(nw,Zmo);


    // compute all freqs at once
    LinearMultiRSP (Z3, R3, wr, wi, NRW*NW, NRW, D2);




    // =====================================
    //  compute response functions <A,B>_wk
    // =====================================

    if (RSP.mode==RSPparams::RSPmode::CPP) {
        double rr[4][2][NW];

        RR[0][0].inner (rr[0][0],  ZZ[0][0]); // X  R
        RR[0][0].inner (rr[0][1],  ZZ[0][1]); // X  I

        RR[1][0].inner (rr[1][0],  ZZ[1][0]); // Y  R
        RR[1][0].inner (rr[1][1],  ZZ[1][1]); // Y  I

        RR[2][0].inner (rr[2][0],  ZZ[2][0]); // Z  R
        RR[2][0].inner (rr[2][1],  ZZ[2][1]); // Z  I


        for (int nw=0; nw<NW; ++nw) rr[0][0] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[0][1] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[1][0] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[1][1] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[2][0] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[2][1] [nw] *= 4;

        for (int nw=0; nw<NW; ++nw) rr[3][0] [nw] = (1./3.) * (rr[0][0] [nw] + rr[1][0] [nw] + rr[2][0] [nw]);
        for (int nw=0; nw<NW; ++nw) rr[3][1] [nw] = (1./3.) * (rr[0][1] [nw] + rr[1][1] [nw] + rr[2][1] [nw]);



        Messenger.precision(7);
        Messenger.width(3);

        Messenger << "Final complex polarizabilities" << endl;
        Messenger << endl;
        Messenger << "  ww  = "; for (int nw=0; nw<NW; ++nw) Messenger << RSP.ww[nw]; Messenger << endl;
        Messenger << " <x,x> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[0][0] [nw]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << rr[0][1] [nw]; Messenger << endl;
        Messenger << " <y,y> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[1][0] [nw]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << rr[1][1] [nw]; Messenger << endl;
        Messenger << " <z,z> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[2][0] [nw]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << rr[2][1] [nw]; Messenger << endl;

        Messenger << " <r^2> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[3][0] [nw]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << rr[3][1] [nw]; Messenger << endl;

        Messenger << endl;

        // write solutions to disk
        if (RSP.savepath != "") {

            tensor2 T(nO,nV), S(nO,nV);

            // gather the results to the master node and
            // write the solutions in binary format
            for (int nw=0; nw<NW; ++nw) {
                for (int k=0; k<NRW; ++k) {
                    {
                        ZZ[k][0].get(T,nw);
                        string file = RSP.savepath + "/zrs_" + std::to_string(nw) + "_" + std::to_string(k) + ".bin";
                        WriteTensorBin(file, T);
                    }
                    {
                        ZZ[k][1].get(T,nw);
                        string file = RSP.savepath + "/zri_" + std::to_string(nw) + "_" + std::to_string(k) + ".bin";
                        WriteTensorBin(file, T);
                    }
                    {
                        ZZ[k][2].get(T,nw);
                        string file = RSP.savepath + "/zas_" + std::to_string(nw) + "_" + std::to_string(k) + ".bin";
                        WriteTensorBin(file, T);
                    }
                    {
                        ZZ[k][3].get(T,nw);
                        string file = RSP.savepath + "/zai_" + std::to_string(nw) + "_" + std::to_string(k) + ".bin";
                        WriteTensorBin(file, T);
                    }
                }
            }




            tensor2 ZRI(nO,nO), ZRA(nV,nV);

            if (0) // don't do any of this
            for (int nw=0; nw<NW; ++nw) {

                // do the HOSVD over the different contraction indices of
                // the spherically averaged imaginary polarizability
                // (skip the NRW index)


                ZRI.zeroize();
                ZRA.zeroize();

                // adds all components
                for (int k=0; k<NRW; ++k) {

                    ZZ[k][1].get(T,nw); // get IS component of solution
                    //RR[k][0].get(S,nw); // get RS component of operator

                    //TensorProductNNT ( ZRI, T, S, 1.);
                    //TensorProductNTN ( ZRA, T, S, 1.);
                    TensorProductNNT ( ZRI, T, T, 1.);
                    TensorProductNTN ( ZRA, T, T, 1.);
                }


                // usual case
                if (nV>=nO) {
                    ZRI.Symmetrize(); ZRI *= 0.5;
                    ZRA.Symmetrize(); ZRA *= 0.5;

                    ZRI *= 4./3.;
                    ZRA *= 4./3.;

                    tensor2 zri(1,nO), zra(1,nV);

                    DiagonalizeV(ZRI,zri[0],true);
                    DiagonalizeV(ZRA,zra[0],true);

                    //for (int i=0; i<nO; ++i) zri[0][i] = sqrt(max(0.,zri[0][i]));
                    //for (int a=0; a<nV; ++a) zra[0][a] = sqrt(max(0.,zra[0][a]));
                    //DiagonalScaling (zri[0], ZRI);
                    //DiagonalScaling (zra[0], ZRA);



                    //Messenger << "equation " << nw << " :" << endl << zri << endl << zra << endl << endl << ZRI.trace() << " " << ZRA.trace() << endl << endl;

                    //ZRI.Transpose();
                    //ZRA.Transpose();

                    {
                        string file = RSP.savepath + "/zis_pca_o_" + std::to_string(nw) + ".mat";
                        WriteTensor (file, ZRI);
                    }
                    {
                        string file = RSP.savepath + "/zis_pca_v_" + std::to_string(nw) + ".mat";
                        WriteTensor (file, ZRA);
                    }


                    /*
                    const int nR = min(nO,nV); // number of principal components

                    tensor2 Ut(nO,nR), ss(nR,1);
                    tensor2 & Vt = AI;

                    SVD(AI,Ut,ss[0]);
                    Ut.Transpose();

                    tensor2 O(nR, 1+nO+nV);
                    for (int r=0; r<nR; ++r) {
                        O(r,0) = ss(r,0);
                        for (int i=0; i<nO; ++i) O(r,1+i)    = Ut(r,i);
                        for (int a=0; a<nV; ++a) O(r,1+nO+a) = Vt(r,a);
                    }

                    O.Transpose();

                    string file = "a_i_pca" + std::to_string(nw) + ".mat";
                    WriteTensor (file, O);
                    */
                }


                /*
                // average dipole polarizability
                tensor2 AI(nO,nV);
                AI.zeroize();

                for (int k=0; k<NRW; ++k) {
                    ZZ[k][1].get(T,nw); // get IS component of solution
                    RR[k][0].get(S,nw); // get RS component of operator

                    // add direct poduct
                    for (int ia=0; ia<nO*nV; ++ia) AI[0][ia] += T[0][ia] * S[0][ia];
                }

                // adds all components
                AI *= 4.;

                // average
                AI *= (1./3.);

                if (0) {
                    string file = "alpha_im_" + std::to_string(nw) + ".mat";
                    WriteTensor (file ,AI);
                }

                const int nR = min(nO,nV); // number of principal components

                // usual case
                if (nV>=nO) {
                    tensor2 Ut(nO,nR), ss(nR,1);
                    tensor2 & Vt = AI;

                    SVD(AI,Ut,ss[0]);
                    Ut.Transpose();

                    tensor2 O(nR, 1+nO+nV);
                    for (int r=0; r<nR; ++r) {
                        O(r,0) = ss(r,0);
                        for (int i=0; i<nO; ++i) O(r,1+i)    = Ut(r,i);
                        for (int a=0; a<nV; ++a) O(r,1+nO+a) = Vt(r,a);
                    }

                    O.Transpose();

                    string file = "a_i_pca" + std::to_string(nw) + ".mat";
                    WriteTensor (file, O);
                }
                */


                /*
                SVD(AI,U,V,ss[0]);
                U.Transpose();

                {
                    string file = "alpha_im_occ" + std::to_string(nw) + ".mat";
                    WriteTensor (file ,U);
                }
                {
                    string file = "alpha_im_vir" + std::to_string(nw) + ".mat";
                    WriteTensor (file ,V);
                }
                */

            }


        }

        // print occupation analysis
        if (0) {

            Messenger << "Response vector occupation analysis :" << endl << endl;

            // compute norms
            tensor2 Z2R(NRW,NW), Z2I(NRW,NW); // R / I

            {
                tensor2 N2(4,NRW*NW);

                Z3[0].norm(N2[0]); // RS
                Z3[1].norm(N2[1]); // IS
                Z3[2].norm(N2[2]); // RA
                Z3[3].norm(N2[3]); // IA

                Z2R.zeroize();
                Z2I.zeroize();

                Z2R += tensor2(NRW,NW, N2[0]); // RS
                Z2I += tensor2(NRW,NW, N2[1]); // IS
                Z2R += tensor2(NRW,NW, N2[2]); // RA
                Z2I += tensor2(NRW,NW, N2[3]); // IA
            }

            Messenger << "real norms^2 = " << endl << Z2R << endl;
            Messenger << "imag norms^2 = " << endl << Z2I << endl;



            const int NIDX = min(min(4,nO),nV); // number of indices to remember

            tensor3 O2R(NW,NRW,nO), O2I(NW,NRW,nO);
            tensor3 V2R(NW,NRW,nV), V2I(NW,NRW,nV);

            {
                tensor2 TS(nO,nV),TA(nO,nV); // for fetching

                for (int nw=0; nw<NW; ++nw) {
                    for (int k=0; k<NRW; ++k) {

                        // RS & RA
                        ZZ[k][0].get(TS,nw);
                        ZZ[k][2].get(TA,nw);

                        for (int i=0; i<nO; ++i) {
                            double si = 0;
                            for (int a=0; a<nV; ++a)
                                si += TS(i,a)*TS(i,a) + TA(i,a)*TA(i,a);
                            O2R(nw,k,i) = si;
                        }
                        for (int a=0; a<nV; ++a) {
                            double sa = 0;
                            for (int i=0; i<nO; ++i)
                                sa += TS(i,a)*TS(i,a) + TA(i,a)*TA(i,a);
                            V2R(nw,k,a) = sa;
                        }

                        // IS & IA
                        ZZ[k][1].get(TS,nw);
                        ZZ[k][3].get(TA,nw);

                        for (int i=0; i<nO; ++i) {
                            double si = 0;
                            for (int a=0; a<nV; ++a)
                                si += TS(i,a)*TS(i,a) + TA(i,a)*TA(i,a);
                            O2I(nw,k,i) = si;
                        }
                        for (int a=0; a<nV; ++a) {
                            double sa = 0;
                            for (int i=0; i<nO; ++i)
                                sa += TS(i,a)*TS(i,a) + TA(i,a)*TA(i,a);
                            V2I(nw,k,a) = sa;
                        }

                    }
                }
            }

            string opname[3] = {"Xdip", "Ydip", "Zdip"};
            Messenger.precision(4);

            for (int nw=0; nw<NW; ++nw) {

                Messenger << "w =" << RSP.ww[nw] << " + " << RSP.gg << " i" << endl;

                for (int k=0; k<NRW; ++k) {
                    int    idxs[4][NIDX+1]; // noi[NIDX], nvr[NIDX], nvi[NIDX];
                    double vals[4][NIDX+1];

                    for (int p=0; p<4; ++p)
                        for (int n=0; n<NIDX+1; ++n) {
                            idxs[p][n] = 0;  // doesn't matter
                            vals[p][n] = 0.; // minimum possible value
                        }

                    for (int i=0; i<nO; ++i) {
                        vals[0][NIDX] = O2R(nw,k,i);
                        idxs[0][NIDX] = i;

                        for (int k=NIDX; k>0; --k) {
                            if (vals[0][k-1]<=vals[0][k]) {
                                swap(vals[0][k-1], vals[0][k]);
                                swap(idxs[0][k-1], idxs[0][k]);
                            }
                            else break;
                        }
                    }

                    for (int i=0; i<nO; ++i) {
                        vals[1][NIDX] = O2I(nw,k,i);
                        idxs[1][NIDX] = i;

                        for (int k=NIDX; k>0; --k) {
                            if (vals[1][k-1]<=vals[1][k]) {
                                swap(vals[1][k-1], vals[1][k]);
                                swap(idxs[1][k-1], idxs[1][k]);
                            }
                            else break;
                        }
                    }

                    for (int a=0; a<nV; ++a) {
                        vals[2][NIDX] = V2R(nw,k,a);
                        idxs[2][NIDX] = a+nO;

                        for (int k=NIDX; k>0; --k) {
                            if (vals[2][k-1]<=vals[2][k]) {
                                swap(vals[2][k-1], vals[2][k]);
                                swap(idxs[2][k-1], idxs[2][k]);
                            }
                            else break;
                        }
                    }

                    for (int a=0; a<nV; ++a) {
                        vals[3][NIDX] = V2I(nw,k,a);
                        idxs[3][NIDX] = a+nO;

                        for (int k=NIDX; k>0; --k) {
                            if (vals[3][k-1]<=vals[3][k]) {
                                swap(vals[3][k-1], vals[3][k]);
                                swap(idxs[3][k-1], idxs[3][k]);
                            }
                            else break;
                        }
                    }

                    //Messenger << "w =" << RSP.ww[nw] << " + " << RSP.gg << " i    " << opname[k] << endl;
                    Messenger << "   "  << opname[k] << "     |Zre|^2 = " << Z2R(k, nw) << "     |Zim|^2 = " << Z2I(k,nw) << endl;

                    Messenger << "      occ, re : "; for (int n=0; n<NIDX; ++n) Messenger << idxs[0][n] << ": " << vals[0][n]/Z2R(k, nw) << "  "; Messenger << endl;
                    Messenger << "      occ, im : "; for (int n=0; n<NIDX; ++n) Messenger << idxs[1][n] << ": " << vals[1][n]/Z2I(k, nw) << "  "; Messenger  << endl;
                    Messenger << "      vir, re : "; for (int n=0; n<NIDX; ++n) Messenger << idxs[2][n] << ": " << vals[2][n]/Z2R(k, nw) << "  "; Messenger  << endl;
                    Messenger << "      vir, im : "; for (int n=0; n<NIDX; ++n) Messenger << idxs[3][n] << ": " << vals[3][n]/Z2I(k, nw) << "  "; Messenger  << endl;
                    Messenger << endl;
                }
            }
        }

    }
    // REAL || IMAG
    else {
        double rr[4][NW];

        RR[0][0].inner (rr[0],  ZZ[0][0]); // X  R
        RR[1][0].inner (rr[1],  ZZ[1][0]); // Y  R
        RR[2][0].inner (rr[2],  ZZ[2][0]); // Z  R


        for (int nw=0; nw<NW; ++nw) rr[0] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[1] [nw] *= 4;
        for (int nw=0; nw<NW; ++nw) rr[2] [nw] *= 4;

        for (int nw=0; nw<NW; ++nw) rr[3] [nw] = (1./3.) * (rr[0] [nw] + rr[1] [nw] + rr[2] [nw]);



        Messenger.precision(7);
        Messenger.width(3);

        if (RSP.mode==RSPparams::RSPmode::REAL)  Messenger << "Final polarizabilities" << endl;
        if (RSP.mode==RSPparams::RSPmode::IMAG)  Messenger << "Final imaginary polarizabilities" << endl;
        Messenger << endl;
        Messenger << "  ww  = "; for (int nw=0; nw<NW; ++nw) Messenger << RSP.ww[nw]; Messenger << endl;
        Messenger << " <x,x> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[0] [nw]; Messenger << endl;
        Messenger << " <y,y> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[1] [nw]; Messenger << endl;
        Messenger << " <z,z> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[2] [nw]; Messenger << endl;

        Messenger << " <r^2> "; for (int nw=0; nw<NW; ++nw) Messenger << rr[3] [nw]; Messenger << endl;

        Messenger << endl;
    }


    // =========================
    //  print/output properties
    // =========================


// THIS IS KEPT FOR DEBUGGING PURPOSES FOR THE MOMENT


/*
    if (RSP.mode==RSPparams::CPP) {
        tensor2 xx3, yy3, zz3; // output everything

        LinearMultiRSP (xx3, Xmo, D2);
        LinearMultiRSP (yy3, Ymo, D2);
        LinearMultiRSP (zz3, Zmo, D2);


        tensor2 xs(2*NW, nO*nV, xx3[0]);  // RS IS
        tensor2 ys(2*NW, nO*nV, yy3[0]);
        tensor2 zs(2*NW, nO*nV, zz3[0]);


        tensor2 axx, ayy, azz, ar2; // polarizabilities

        tensor2 Xov(nO*nV,1, Xmo[0]);
        tensor2 Yov(nO*nV,1, Ymo[0]);
        tensor2 Zov(nO*nV,1, Zmo[0]);

        TensorProduct (axx, xs, Xov);
        TensorProduct (ayy, ys, Yov);
        TensorProduct (azz, zs, Zov);

        axx*=4; ayy*=4; azz*=4;
        ar2 = axx; ar2 += ayy; ar2 += azz; ar2 *= (1./3.);

        Messenger.precision(7);
        Messenger.width(3);

        Messenger << "Final complex polarizabilities" << endl;
        Messenger << endl;
        Messenger << "  ww  = "; for (int nw=0; nw<NW; ++nw) Messenger << RSP.ww[nw]; Messenger << endl;
        Messenger << " <x,x> "; for (int nw=0; nw<NW; ++nw) Messenger << axx[nw][0]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << axx[NW+nw][0]; Messenger << endl;
        Messenger << " <y,y> "; for (int nw=0; nw<NW; ++nw) Messenger << ayy[nw][0]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << ayy[NW+nw][0]; Messenger << endl;
        Messenger << " <z,z> "; for (int nw=0; nw<NW; ++nw) Messenger << azz[nw][0]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << azz[NW+nw][0]; Messenger << endl;
        Messenger << " <r^2> "; for (int nw=0; nw<NW; ++nw) Messenger << ar2[nw][0]; Messenger << endl;
        Messenger << "       "; for (int nw=0; nw<NW; ++nw) Messenger << ar2[NW+nw][0]; Messenger << endl;

        Messenger << endl;
    }

    // do pure real or pure imaginary RSP
    else {
        tensor2 xx3, yy3, zz3; // output everything

        LinearMultiRSP (xx3, Xmo, D2);
        LinearMultiRSP (yy3, Ymo, D2);
        LinearMultiRSP (zz3, Zmo, D2);


        tensor2 xs(NW, nO*nV, xx3[0]);  // RS
        tensor2 ys(NW, nO*nV, yy3[0]);
        tensor2 zs(NW, nO*nV, zz3[0]);


        tensor2 axx, ayy, azz, ar2; // polarizabilities

        tensor2 Xov(nO*nV,1, Xmo[0]);
        tensor2 Yov(nO*nV,1, Ymo[0]);
        tensor2 Zov(nO*nV,1, Zmo[0]);

        TensorProduct (axx, xs, Xov);
        TensorProduct (ayy, ys, Yov);
        TensorProduct (azz, zs, Zov);

        axx*=4; ayy*=4; azz*=4;
        ar2 = axx; ar2 += ayy; ar2 += azz; ar2 *= (1./3.);


        Messenger.precision(7);
        Messenger.width(3);

        Messenger << "Final polarizabilities" << endl;
        Messenger << endl;
        Messenger << "   w = "; for (int nw=0; nw<NW; ++nw) Messenger << RSP.ww[nw]; Messenger << endl;
        Messenger << " <x,x> "; for (int nw=0; nw<NW; ++nw) Messenger << axx[nw][0]; Messenger << endl;
        Messenger << " <y,y> "; for (int nw=0; nw<NW; ++nw) Messenger << ayy[nw][0]; Messenger << endl;
        Messenger << " <z,z> "; for (int nw=0; nw<NW; ++nw) Messenger << azz[nw][0]; Messenger << endl;
        Messenger << " <r^2> "; for (int nw=0; nw<NW; ++nw) Messenger << ar2[nw][0]; Messenger << endl;
        Messenger << endl;
    }
*/


}


    /*
              THIS WAS A TEST FOR CMM; WILL PROBABLY USE A SIMILAR APPROACH IN THE END

    {
        int NOV = nO*nV;
        int NO = nO;
        int NV = nV;

        Messenger << NO << " " << NV << "  " << NOV << endl;

        double wx[max(NO,NV)];
        double wy[max(NO,NV)];
        double wz[max(NO,NV)];

        tensor2 U(NV,NV), V(NO,NO);

        char all[] = "A";
        int lwork = -1;
        double wokpt;
        double * work;
        int info;


        dgesvd_( all, all, &NV, &NO, Xmo.c2, &NV, wx, U.c2, &NV, V.c2, &NO,  &wokpt, &lwork, &info);

        if( info > 0 ) {
            return;
        }

        lwork = int(wokpt);

        Messenger << "lwork " << lwork << endl;

        work = new double[max(1,lwork)];

        dgesvd_( all, all, &NV, &NO, Xmo.c2, &NV, wx, U.c2, &NV, V.c2, &NO,  work, &lwork, &info);

        for (int n=0; n<NO; ++n) Messenger << wx[n] << " "; Messenger << endl;

        dgesvd_( all, all, &NV, &NO, Ymo.c2, &NV, wy, U.c2, &NV, V.c2, &NO,  work, &lwork, &info);

        for (int n=0; n<NO; ++n) Messenger << wy[n] << " "; Messenger << endl;

        dgesvd_( all, all, &NV, &NO, Zmo.c2, &NV, wz, U.c2, &NV, V.c2, &NO,  work, &lwork, &info);

        for (int n=0; n<NO; ++n) Messenger << wz[n] << " "; Messenger << endl;

        delete[] work;
    }
    */


