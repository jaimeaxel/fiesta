/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "IIC.hpp"

#include <string.h>

#include "2eints/quimera.hpp"
#include "libquimera/ERIgeom.hpp"
#include "libquimera/libquimera.hpp"
using namespace LibQuimera;

static const uint8_t PF = 8; //prefetch distance
static const int DPB = DOUBLES_PER_BLOCK;



template<int N>
void ERIroutine::TransformABCD(const cachelineN<N> * __restrict__  mem, const ERIgeometriesN<N> & varsN, cachelineN<N> * __restrict__ ERIN, p_ERIbuffer & buffer) const {

    const cachelineN<N> & ABz = varsN.ABz;
    const cachelineN<N> & CDy = varsN.CDy;
    const cachelineN<N> & CDz = varsN.CDz;

    const cachelineN<N> & ACx = varsN.ACx;
    const cachelineN<N> & ACy = varsN.ACy;
    const cachelineN<N> & ACz = varsN.ACz;


    register Op_MIRROR * s2 = eseq;

    cachelineN<N> * bufferN = (cachelineN<N>*)buffer.bufferMIRROR;

    memset((void*)bufferN, 0, sizeof(cachelineN<N>)*MaxMem);


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(bufferN + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        bufferN[i] = mem[i];
    }

    for (; s2<nseq[MMDZ]; ++s2) {

        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t zzz = s2->aux;

        //for (int i=0; i<8; ++i)
        //dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACz * loadN<N>(op3) - ABz * loadN<N>(op1) + CDz * loadN<N>(op2);

        if (zzz>0) {
            double z = zzz;
            tmp -= loadN<N>(op4) * z;
        }

        bufferN[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */
        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + ABz * loadN<N>(op2) + loadN<N>(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + CDz * loadN<N>(op2) - loadN<N>(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t yyy = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACy * loadN<N>(op3) + CDy * loadN<N>(op2);

        if (yyy>0) {
            double y = yyy;
            tmp -= loadN<N>(op4) * y;
        }

        bufferN[s2->dest] = tmp;
    }

    for (; s2<nseq[MMDX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t xxx = s2->aux;
        //double x = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACx[i] * op3[i] - x * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACx * loadN<N>(op3);

        if (xxx>0) {
            double x = xxx;
            tmp -= loadN<N>(op4) * x;
        }

        bufferN[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y + CDy * loadN<N>(op2) - loadN<N>(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x - loadN<N>(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y + loadN<N>(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x + loadN<N>(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - ABz * loadN<N>(op2));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (la>1) {
            cachelineN<N> sum;
            if (fCa[m][0]!=0) sum  = bufferN[s2->op1] * fCa[m][0];
            if (fCa[m][1]!=0) sum += bufferN[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += bufferN[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += bufferN[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += bufferN[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += bufferN[s2->op6] * fCa[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[SPHB]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (lb>1) {
            cachelineN<N> sum;
            if (fCb[m][0]!=0) sum  = bufferN[s2->op1] * fCb[m][0];
            if (fCb[m][1]!=0) sum += bufferN[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += bufferN[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += bufferN[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += bufferN[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += bufferN[s2->op6] * fCb[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - CDz * loadN<N>(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - CDy * loadN<N>(op2));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (lc>1) {
            cachelineN<N> sum;
            if (fCc[m][0]!=0) sum  = bufferN[s2->op1] * fCc[m][0];
            if (fCc[m][1]!=0) sum += bufferN[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += bufferN[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += bufferN[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += bufferN[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += bufferN[s2->op6] * fCc[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[SPHD]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (ld>1) {
            cachelineN<N> sum;
            if (fCd[m][0]!=0) sum  = bufferN[s2->op1] * fCd[m][0];
            if (fCd[m][1]!=0) sum += bufferN[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += bufferN[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += bufferN[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += bufferN[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += bufferN[s2->op6] * fCd[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[REORDER]; ++s2) {
        double * dest = (double*)(ERIN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(ERIN + s2[PF].dest, 1, 1);

        ERIN[s2->dest] = bufferN[s2->op1];
    }
}

template<int N>
void ERIroutine::TransformAACD(const cachelineN<N> * __restrict__  mem, const ERIgeometriesN<N> & varsN, cachelineN<N> * __restrict__ ERIN, p_ERIbuffer & buffer) const {

    const cachelineN<N> & CDz = varsN.CDz;

    const cachelineN<N> & ACy = varsN.ACy;
    const cachelineN<N> & ACz = varsN.ACz;


    register Op_MIRROR * s2 = eseq;

    cachelineN<N> * bufferN = (cachelineN<N>*)buffer.bufferMIRROR;

    memset((void*)bufferN, 0, sizeof(cachelineN<N>)*MaxMem);


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(bufferN + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        bufferN[i] = mem[i];
    }


    for (; s2<nseq[MMDZ]; ++s2) {

        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t zzz = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACz * loadN<N>(op3) + CDz * loadN<N>(op2);

        if (zzz>0) {
            double z = zzz;
            tmp -= loadN<N>(op4) * z;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + loadN<N>(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + CDz * loadN<N>(op2) - loadN<N>(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t yyy = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACy * loadN<N>(op3);

        if (yyy>0) {
            double y = yyy;
            tmp -= loadN<N>(op4) * y;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[MMDX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t xxx = s2->aux;
        //double x = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACx[i] * op3[i] - x * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp;
        tmp.set(0.f);

        if (xxx>0) {
            double x = xxx;
            tmp -= loadN<N>(op4) * x;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y - loadN<N>(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x - loadN<N>(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y + loadN<N>(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x + loadN<N>(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (la>1) {
            sum *= fCa[m][0];

            if (fCa[m][1]!=0) sum += bufferN[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += bufferN[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += bufferN[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += bufferN[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += bufferN[s2->op6] * fCa[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[SPHB]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (lb>1) {
            sum *= fCb[m][0];

            if (fCb[m][1]!=0) sum += bufferN[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += bufferN[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += bufferN[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += bufferN[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += bufferN[s2->op6] * fCb[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - CDz * loadN<N>(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (lc>1) {
            sum *= fCc[m][0];

            if (fCc[m][1]!=0) sum += bufferN[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += bufferN[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += bufferN[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += bufferN[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += bufferN[s2->op6] * fCc[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[SPHD]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (ld>1) {
            sum *= fCd[m][0];

            if (fCd[m][1]!=0) sum += bufferN[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += bufferN[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += bufferN[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += bufferN[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += bufferN[s2->op6] * fCd[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[REORDER]; ++s2) {
        double * dest = (double*)(ERIN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(ERIN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

}

template<int N>
void ERIroutine::TransformAACC(const cachelineN<N> * __restrict__  mem, const ERIgeometriesN<N> & varsN, cachelineN<N> * __restrict__ ERIN, p_ERIbuffer & buffer) const {


    register Op_MIRROR * s2 = eseq;

    cachelineN<N> * bufferN = (cachelineN<N>*)buffer.bufferMIRROR;

    memset((void*)bufferN, 0, sizeof(cachelineN<N>)*MaxMem);


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(bufferN + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        bufferN[i] = mem[i];
    }


    for (; s2<nseq[MMDZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t zzz = s2->aux;
        //double x = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp;
        tmp = bufferN[s2->op3] * varsN.ACz;

        if (zzz>0) {
            double z = zzz;
            tmp -= loadN<N>(op4) * z;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + loadN<N>(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z  - loadN<N>(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t yyy = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp;
        tmp.set(0.f);

        if (yyy>0) {
            double y = yyy;
            tmp -= loadN<N>(op4) * y;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[MMDX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t xxx = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp;
        tmp.set(0.f);

        if (xxx>0) {
            double x = xxx;
            tmp -= loadN<N>(op4) * x;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y - loadN<N>(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x - loadN<N>(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y + loadN<N>(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x + loadN<N>(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (la>1) {
            sum *= fCa[m][0];

            if (fCa[m][1]!=0) sum += bufferN[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += bufferN[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += bufferN[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += bufferN[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += bufferN[s2->op6] * fCa[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[SPHB]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (lb>1) {
            sum *= fCb[m][0];

            if (fCb[m][1]!=0) sum += bufferN[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += bufferN[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += bufferN[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += bufferN[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += bufferN[s2->op6] * fCb[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (lc>1) {
            sum *= fCc[m][0];

            if (fCc[m][1]!=0) sum += bufferN[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += bufferN[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += bufferN[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += bufferN[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += bufferN[s2->op6] * fCc[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[SPHD]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> sum = bufferN[s2->op1];

        if (ld>1) {
            sum *= fCd[m][0];

            if (fCd[m][1]!=0) sum += bufferN[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += bufferN[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += bufferN[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += bufferN[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += bufferN[s2->op6] * fCd[m][5];
        }

        bufferN[s2->dest] = sum;
    }

    for (; s2<nseq[REORDER]; ++s2) {
        double * dest = (double*)(ERIN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(ERIN + s2[PF].dest, 1, 1);

        ERIN[s2->dest] = bufferN[s2->op1];
    }

}

template<int N>
void ERIroutine::TransformABAB(const cachelineN<N> * __restrict__  mem, const ERIgeometriesN<N> & varsN, cachelineN<N> * __restrict__ ERIN, p_ERIbuffer & buffer) const {
    const cachelineN<N> & ABz = varsN.ABz;
    const cachelineN<N> & CDy = varsN.CDy;
    const cachelineN<N> & CDz = varsN.CDz;

    const cachelineN<N> & ACx = varsN.ACx;
    const cachelineN<N> & ACy = varsN.ACy;
    const cachelineN<N> & ACz = varsN.ACz;


    register Op_MIRROR * s2 = eseq;

    cachelineN<N> * bufferN = (cachelineN<N>*)buffer.bufferMIRROR;

    memset((void*)bufferN, 0, sizeof(cachelineN<N>)*MaxMem);


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(bufferN + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        bufferN[i] = mem[i];
    }


    for (; s2<nseq[MMDZ]; ++s2) {

        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t zzz = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACz * loadN<N>(op3) - ABz * loadN<N>(op1) + CDz * loadN<N>(op2);

        if (zzz>0) {
            double z = zzz;
            tmp -= loadN<N>(op4) * z;
        }

        bufferN[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */
        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + ABz * loadN<N>(op2) + loadN<N>(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double z = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * z + CDz * loadN<N>(op2) - loadN<N>(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t yyy = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACy * loadN<N>(op3) + CDy * loadN<N>(op2);

        if (yyy>0) {
            double y = yyy;
            tmp -= loadN<N>(op4) * y;
        }

        bufferN[s2->dest] = tmp;
    }

    for (; s2<nseq[MMDX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        uint8_t xxx = s2->aux;
        //double x = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACx[i] * op3[i] - x * op4[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op4);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        cachelineN<N> tmp = ACx * loadN<N>(op3);

        if (xxx>0) {
            double x = xxx;
            tmp -= loadN<N>(op4) * x;
        }

        bufferN[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y + CDy * loadN<N>(op2) - loadN<N>(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = -double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x - loadN<N>(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double y = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * y + loadN<N>(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op3  = (double*)(bufferN + s2->op3);
        double x = double(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op3);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) * x + loadN<N>(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - ABz * loadN<N>(op2));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (la>1) {
            cachelineN<N> sum;
            if (fCa[m][0]!=0) sum  = bufferN[s2->op1] * fCa[m][0];
            if (fCa[m][1]!=0) sum += bufferN[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += bufferN[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += bufferN[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += bufferN[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += bufferN[s2->op6] * fCa[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[SPHB]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (lb>1) {
            cachelineN<N> sum;
            if (fCb[m][0]!=0) sum  = bufferN[s2->op1] * fCb[m][0];
            if (fCb[m][1]!=0) sum += bufferN[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += bufferN[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += bufferN[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += bufferN[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += bufferN[s2->op6] * fCb[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - CDz * loadN<N>(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].op2);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1) - CDy * loadN<N>(op2));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const double * op1  = (double*)(bufferN + s2->op1);
        double * dest = (double*)(bufferN + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        store(dest, loadN<N>(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (lc>1) {
            cachelineN<N> sum;
            if (fCc[m][0]!=0) sum  = bufferN[s2->op1] * fCc[m][0];
            if (fCc[m][1]!=0) sum += bufferN[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += bufferN[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += bufferN[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += bufferN[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += bufferN[s2->op6] * fCc[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[SPHD]; ++s2) {
        double * dest = (double*)(bufferN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);
        const double * op2  = (double*)(bufferN + s2->op2);
        const double * op3  = (double*)(bufferN + s2->op3);
        const double * op4  = (double*)(bufferN + s2->op4);
        const double * op5  = (double*)(bufferN + s2->op5);
        const double * op6  = (double*)(bufferN + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(bufferN + s2[PF].dest, 1, 1);

        if (ld>1) {
            cachelineN<N> sum;
            if (fCd[m][0]!=0) sum  = bufferN[s2->op1] * fCd[m][0];
            if (fCd[m][1]!=0) sum += bufferN[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += bufferN[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += bufferN[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += bufferN[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += bufferN[s2->op6] * fCd[m][5];
            bufferN[s2->dest] = sum;
        }
        else
            bufferN[s2->dest] = bufferN[s2->op1];
    }

    for (; s2<nseq[REORDER]; ++s2) {
        double * dest = (double*)(ERIN + s2->dest);
        const double * op1  = (double*)(bufferN + s2->op1);

        __builtin_prefetch(bufferN + s2[PF].op1);
        __builtin_prefetch(ERIN + s2[PF].dest, 1, 1);

        ERIN[s2->dest] = bufferN[s2->op1];
    }
}

#include "low/chrono.hpp"

void p_Qalgorithm::MIRROR(const cachelineN<DOUBLES_PER_BLOCK> * mem,  const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, cachelineN<DOUBLES_PER_BLOCK> * ERI, p_ERIbuffer & buffer) const {

    Chronometer chronoT;

    if (BenchmarkAlgorithms)
        chronoT.Start();

    if      (eritype.geometry==ABCD) IC->TransformABCD(mem, vars, ERI, buffer);
    else if (eritype.geometry==AACD) IC->TransformAACD(mem, vars, ERI, buffer);
    else if (eritype.geometry==AACC) IC->TransformAACC(mem, vars, ERI, buffer);
    else if (eritype.geometry==ABAB) IC->TransformABCD(mem, vars, ERI, buffer);
    else                             IC->TransformABCD(mem, vars, ERI, buffer);

    if (BenchmarkAlgorithms) {
        chronoT.Stop();

        double At = chronoT.GetTotalTime();

        K4benchmark * bench = p_Q.Benchmarks[eritype];

        #pragma omp atomic
        ++bench->ncallsMIRROR;

        #pragma omp atomic
        bench->deltaMIRROR += At;
    }
}
