
#include <iostream>
#include <string.h>
using namespace std;

#include "IIC.hpp"

#include "math/gamma.hpp"
using namespace LibIGamma;

#include "basis/SPprototype.hpp"
#include "2eints/quimera.hpp"
#include "libquimera/ERIgeom.hpp"
#include "libquimera/libquimera.hpp"
using namespace LibQuimera;


const int DPB = DOUBLES_PER_BLOCK;


/* *************************** */
/*     INCOMPLETE GAMMA        */
/* *************************** */

// 64*8*8 =  4Kb constant memory < 8Kb cached constant memory
__constant__ double gamma_cfs[4*LMAXG+1][NVIGF][NEXPC]; // (4*3 + 1) * 4kb = 52kb < 64kb

void InitDeviceGammaCheb() {

    size_t gsize = NVIGF*NEXPC*sizeof(double);

    int NumGPUs;
    cudaGetDeviceCount(&NumGPUs);

    for (int n=0; n<NumGPUs; ++n) {
        //select device
        cudaSetDevice(n);

        //copy all tables
        for (int l=0; l<=4*LMAXG; ++l)
            cudaMemcpyToSymbol (gamma_cfs, IGammaChebyshev[l].gamma_cfs, gsize, l*gsize);
    }
}


template <int Labcd>
__device__ void GammaLR(double & m, const double R2) {

    const double PI3  = 31.0062766802998202;   // = PI^3

    const double iii[] =
    {1, 3, 15, 105, 945, 10395, 135135, 2027025, 34459425, 654729075,
     13749310575., 316234143225., 7905853580625., 213458046676875.,
     6190283353629375., 191898783962510625., 6332659870762850625.,
     221643095476699771875., 8200794532637891559375.,
     319830986772877770815625., 13113070457687988603440625.,
     563862029680583509947946875., 25373791335626257947657609375.,
     1192568192774434123539907640625., 58435841445947272053455474390625.};


    double ir = rsqrt(R2);

    m = PI3 * ir;

    if (Labcd>0) {
        double ir2  = ir   * ir;
        double ir4  = ir2  * ir2;
        double ir8  = ir4  * ir4;
        double ir16 = ir8  * ir8;
        double ir32 = ir16 * ir16;
        double ir64 = ir32 * ir32;

        if (Labcd&1)  m *= ir2;
        if (Labcd&2)  m *= ir4;
        if (Labcd&4)  m *= ir8;
        if (Labcd&8)  m *= ir16;
        if (Labcd&16) m *= ir32;
        if (Labcd&32) m *= ir64;

        m *= iii[Labcd-1];
        // !!! compiler warning: subscript out of range [Labcd] !!!
    }
}

template <int Labcd>
__device__ void GammaSR(double & e, double & m, const double KpqR2) {

    double p = IFSTEP * KpqR2;
    int pos = int(p+0.5);
    double x0 = FSTEP*double(pos);
    double Ax = KpqR2-x0;

    m = 0; //gamma_cfs[pos][NEXPC-1];
    #pragma unroll
    for (int p=NEXPC-1; p>=0; --p) m = gamma_cfs[Labcd][pos][p] + Ax*m;
    // !!! compiler warning: subscript out of range [Labcd] !!!

    if (Labcd>0) e = exp(-KpqR2); //too expensive?
    else         e = 0;           //doesn't really matter
}



template <int Labcd>
__device__ void GammaT2(double & e, double & m, const double m0, const double iKpq, const double R2, const double W) {

    if (R2 > iKpq*(double(MAXZ)-FSTEP) ) {
        m = W*m0;
        e = 0;
    }
    else {
        const double PI54 = 34.986836655249725693; // = 2*PI^(5/2)

        double Kpq = 1. / iKpq;
        double KpqR2 = Kpq * R2;

        GammaSR<Labcd>(e,m, KpqR2);

        //this could be precomputed and stored
        double Kpqn = sqrt(Kpq); {
            Kpq += Kpq; // (2*Kpq)^(m+1/2)

            double Kpq2  = Kpq   * Kpq;
            double Kpq4  = Kpq2  * Kpq2;
            double Kpq8  = Kpq4  * Kpq4;
            double Kpq16 = Kpq8  * Kpq8;
            double Kpq32 = Kpq16 * Kpq16;

            if (Labcd&1)  Kpqn *= Kpq;
            if (Labcd&2)  Kpqn *= Kpq2;
            if (Labcd&4)  Kpqn *= Kpq4;
            if (Labcd&8)  Kpqn *= Kpq8;
            if (Labcd&16) Kpqn *= Kpq16;
            if (Labcd&32) Kpqn *= Kpq32;
        }

        m *= PI54*Kpqn*W;
        if (Labcd>0)
        e *= PI54*Kpqn*W;
    }
}

template <int Labcd>
__device__ void GammaT(double & e, double & m, const double iKpq, const double R2, const double W) {

    if (R2 > iKpq*(double(MAXZ)-FSTEP) ) {
        GammaLR<Labcd> (m, R2);
        m *= W;
        e = 0;
    }
    else {
        const double PI54 = 34.986836655249725693; // = 2*PI^(5/2)

        double Kpq = 1. / iKpq;
        double KpqR2 = Kpq * R2;

        GammaSR<Labcd>(e,m, KpqR2);

        //this could be precomputed and stored
        double Kpqn = sqrt(Kpq); {
            Kpq += Kpq; // (2*Kpq)^(m+1/2)

            double Kpq2  = Kpq   * Kpq;
            double Kpq4  = Kpq2  * Kpq2;
            double Kpq8  = Kpq4  * Kpq4;
            double Kpq16 = Kpq8  * Kpq8;
            double Kpq32 = Kpq16 * Kpq16;

            if (Labcd&1)  Kpqn *= Kpq;
            if (Labcd&2)  Kpqn *= Kpq2;
            if (Labcd&4)  Kpqn *= Kpq4;
            if (Labcd&8)  Kpqn *= Kpq8;
            if (Labcd&16) Kpqn *= Kpq16;
            if (Labcd&32) Kpqn *= Kpq32;
        }

        m *= PI54*Kpqn*W;
        if (Labcd>0)
        e *= PI54*Kpqn*W; //too expensive?
    }
}



template <bool SAB, bool SCD, int Labcd>
__global__ void LoopGammaK (const double * vars,   double * d_F0,
                            const double * d_ab, const double * d_cd,
                            const uint8_t KA, const uint8_t KB, const uint8_t KC, const uint8_t KD,
                            const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd,
                            const double * iKa, const double * iKb, const double * iKc, const double * iKd,
                            const double * iKba, const double * iKdc,
                            uint32_t K4NTX,  uint32_t KABNTX,  uint32_t KCDNTX, uint32_t maxTiles, double ilogTh) {

    int i = threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    if (j>=maxTiles) return;

    //const double * KAB = d_ab + j*KABNTX;
    //const double * KCD = d_cd + j*KCDNTX;



    double ABz = vars[j*6*NTX +       i];
    double CDy = vars[j*6*NTX +   NTX+i];
    double CDz = vars[j*6*NTX + 2*NTX+i];
    double ACx = vars[j*6*NTX + 3*NTX+i];
    double ACy = vars[j*6*NTX + 4*NTX+i];
    double ACz = vars[j*6*NTX + 5*NTX+i];


    double AC2, AB2, CD2;

    if      ( SAB &&  SCD) AC2  = ACx*ACx;
    else if (!SAB &&  SCD) AC2  = ACy*ACy;
    else if (!SAB && !SCD) AC2  = ACz*ACz;

    if      ( SAB &&  SCD) AB2  = ABz*ABz;

    if      ( SAB &&  SCD) CD2  = CDy*CDy + CDz*CDz;
    else if (!SAB &&  SCD) CD2  = CDz*CDz;



    double ikmab, ikmcd; //this is different for every thread, so warps might diverge!!!

    if (SAB && SCD) {
        ikmab = AB2 * ilogTh;
        ikmcd = CD2 * ilogTh;
    }
    else if (!SAB && SCD) {
        ikmcd = CD2 * ilogTh;
    }

    double wab[maxK2];

    int ba=0;

    for (uint16_t b=0; b<KB; ++b) {
        for (uint16_t a=0; a<KA; ++a) {
            if (SAB) if (iKa[a] + iKb[b] < ikmab) break;
            const double ikab = iKba[b*KA+a]; //1./(Ka[a] + Kb[b]);
            wab[ba] = (ikab) * sqrt(ikab);
            //if (SAB) wab[ba] *= KAB[(b*KA+a)*NTX + i];
            if (SAB) wab[ba] *= exp(- Ka[a]*Kb[b]*iKba[b*KA+a] * AB2);
            ++ba;
        }
    }


    double m00; if (!SAB && !SCD) GammaLR<Labcd> (m00, AC2);

    double * F0 = d_F0 + 2*j*K4NTX;

    for (uint16_t d=0; d<KD; ++d) {
        double ikmc; if (SCD) ikmc = ikmcd-iKd[d];

        double * F1 = F0 + d*KC*KB*KA * 2*NTX;

        for (uint16_t c=0; c<KC; ++c) {
            if (SCD) if (iKc[c] < ikmc) break;

            const double ikcd = iKdc[d*KC+c]; //1./(Kc[c] + Kd[d]);
            const double rcd = Kd[d] * ikcd;

            double AQy, AQz, AQ2;

            if (SAB && SCD) {
                AQy = ACy + CDy * rcd;
                AQz = ACz + CDz * rcd;
                AQ2 = AC2 + AQy*AQy;
            }
            else if (!SAB && SCD) {
                AQz = ACz + CDz * rcd;
                AQ2 = AC2 + AQz*AQz;
            }

            double wcd = (ikcd) * sqrt(ikcd); if (SCD) wcd *= exp(-Kc[c]*Kd[d]*ikcd * CD2);
            //if (SCD) wcd *= KCD[(d*KC+c)*NTX + i];

            double m01; if (!SAB && SCD) GammaLR<Labcd> (m01, AQ2);


            double * F2 = F1 + c*KB*KA * 2*NTX;

            int ba=0;

            for (uint16_t b=0; b<KB; ++b) {
                double ikma; if (SAB) ikma = ikmab-iKb[b];

                double * F3 = F2 + b*KA * 2*NTX;

                for (uint16_t a=0; a<KA; ++a) {
                    if (SAB) if (iKa[a] < ikma) break;

                    const double ikab = iKba[b*KA+a]; // 1./(Ka[a] + Kb[b]);
                    const double iKpq  =  ikab + ikcd;

                    double ww = wcd*wab[ba];

                    double * F4 = F3 + a * 2*NTX;

                    if      ( SAB &&  SCD) {
                        const double rab = Kb[b] * ikab;
                        double PQz = AQz - ABz*rab;
                        double PQ2 = AQ2 + PQz*PQz;

                        GammaT <Labcd>  (F4[i], F4[NTX+i],      iKpq,  PQ2,  ww);
                    }
                    else if (!SAB &&  SCD) {
                        GammaT2 <Labcd> (F4[i], F4[NTX+i], m01, iKpq,  AQ2,  ww);
                    }
                    else if (!SAB && !SCD) {
                        GammaT2 <Labcd> (F4[i], F4[NTX+i], m00, iKpq,  AC2,  ww);
                    }

                    //F0 += 2*NTX;

                    ++ba;
                }
            }

        }
    }


}


void CallKernelLoopGammaK (GEOM geometry, int Lt, dim3 Nbl, dim3 Nth,    double * d_varsN,     double * d_F0,
                            double * d_ab, double * d_cd,
                            const uint8_t KA, const uint8_t KB, const uint8_t KC, const uint8_t KD,
                            const double *  Ka, const double *  Kb, const double *  Kc, const double *  Kd,
                            const double * iKa, const double * iKb, const double * iKc, const double * iKd,
                            const double * iKba, const double * iKdc,
                            uint32_t K4NTX,  uint32_t KABNTX,  uint32_t KCDNTX,
                            cudaStream_t * stream, uint32_t maxTiles, double ilogTh) {

    if      (geometry==AACC) {
        if      (Lt== 0) LoopGammaK <false,false, 0> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 1) LoopGammaK <false,false, 1> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 2) LoopGammaK <false,false, 2> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 3) LoopGammaK <false,false, 3> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 4) LoopGammaK <false,false, 4> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 5) LoopGammaK <false,false, 5> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 6) LoopGammaK <false,false, 6> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 7) LoopGammaK <false,false, 7> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 8) LoopGammaK <false,false, 8> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 9) LoopGammaK <false,false, 9> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==10) LoopGammaK <false,false,10> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==11) LoopGammaK <false,false,11> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==12) LoopGammaK <false,false,12> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
    }
    else if (geometry==AACD) {
        if      (Lt== 0) LoopGammaK <false,true, 0> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 1) LoopGammaK <false,true, 1> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 2) LoopGammaK <false,true, 2> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 3) LoopGammaK <false,true, 3> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 4) LoopGammaK <false,true, 4> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 5) LoopGammaK <false,true, 5> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 6) LoopGammaK <false,true, 6> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 7) LoopGammaK <false,true, 7> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 8) LoopGammaK <false,true, 8> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 9) LoopGammaK <false,true, 9> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==10) LoopGammaK <false,true,10> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==11) LoopGammaK <false,true,11> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==12) LoopGammaK <false,true,12> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
    }
    else {
        if      (Lt== 0) LoopGammaK <true,true, 0> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 1) LoopGammaK <true,true, 1> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 2) LoopGammaK <true,true, 2> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 3) LoopGammaK <true,true, 3> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 4) LoopGammaK <true,true, 4> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 5) LoopGammaK <true,true, 5> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 6) LoopGammaK <true,true, 6> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 7) LoopGammaK <true,true, 7> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 8) LoopGammaK <true,true, 8> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt== 9) LoopGammaK <true,true, 9> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==10) LoopGammaK <true,true,10> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==11) LoopGammaK <true,true,11> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
        else if (Lt==12) LoopGammaK <true,true,12> <<<Nbl,Nth,0,*stream>>> (d_varsN, d_F0,   d_ab, d_cd,  KA,KB,KC,KD,   Ka,Kb,Kc,Kd,  iKa,iKb,iKc,iKd,  iKba,iKdc,  K4NTX, KABNTX, KCDNTX,  maxTiles, ilogTh);
    }
}





__global__ void GenK4Consts (double * F0, const double * iKba, const double * iKdc, int Labcd) {

    int a = threadIdx.x;
    int b = threadIdx.y;
    int c = blockIdx.x;
    int d = blockIdx.y;

    int KA = blockDim.x;
    int KB = blockDim.y;
    int KC = gridDim.x;
    int KD = gridDim.y;

    const double ikcd = iKdc[d*KC+c];
    const double ikab = iKba[b*KA+a]; // 1./(Ka[a] + Kb[b]);
    const double iKpq = ikab + ikcd;

    const double PI54 = 34.986836655249725693; // = 2*PI^(5/2)

    const double Kpq = 1. / iKpq;

    double Kpqn = PI54 * sqrt(Kpq); {
        double Kpq1  = 2*Kpq;
        double Kpq2  = Kpq1  * Kpq1;
        double Kpq4  = Kpq2  * Kpq2;
        double Kpq8  = Kpq4  * Kpq4;
        double Kpq16 = Kpq8  * Kpq8;
        double Kpq32 = Kpq16 * Kpq16;

        if (Labcd&1)  Kpqn *= Kpq1;
        if (Labcd&2)  Kpqn *= Kpq2;
        if (Labcd&4)  Kpqn *= Kpq4;
        if (Labcd&8)  Kpqn *= Kpq8;
        if (Labcd&16) Kpqn *= Kpq16;
        if (Labcd&32) Kpqn *= Kpq32;
    }

    /*
    double * F1 = F0 + d*KC*KB*KA * 2;
    double * F2 = F1 +    c*KB*KA * 2;
    double * F3 = F2 +       b*KA * 2;
    double * F4 = F3 +          a * 2;
    */

    F0[(((d*KC+c)*KB+b)*KA+a)*2  ] = Kpq;
    F0[(((d*KC+c)*KB+b)*KA+a)*2+1] = Kpqn;
}


/* ********************* */
/*       WRAPPERS        */
/* ********************* */

#include <cmath>

void p_Qalgorithm::K4       (double * d_vars,  double * d_F0, double * d_ab, double * d_cd, const ERITile<DOUBLES_PER_BLOCK> * ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * d_uvmst, uint32_t Ntiles, cudaStream_t * stream) const {

    //SET EVERYTHING THAT REQUIRES SETTING
    const int LA = ABp.l1;
    const int LB = ABp.l2;
    const int LC = CDp.l1;
    const int LD = CDp.l2;

    const int KA = ABp.Ka;
    const int KB = ABp.Kb;
    const int KC = CDp.Ka;
    const int KD = CDp.Kb;


    const int Lt = LA+LB+LC+LD;

    uint16_t nJ1 =      ABp.Ja;
    uint16_t nJ2 = nJ1* ABp.Jb;
    uint16_t nJ3 = nJ2* CDp.Ja;
    uint16_t nJ4 = nJ3* CDp.Jb;

    uint32_t nKab = ABp.Ka * ABp.Kb;
    uint32_t nKcd = CDp.Ka * CDp.Kb;

    uint32_t nK4 = ABp.Ka * ABp.Kb * CDp.Ka * CDp.Kb;

    double ilogTh = 1./(15*log(10)); //1e-15


    int nGPU;
    cudaGetDevice(&nGPU);

    const BCP_GPU & AB = ABp.BCPgpu[nGPU];
    const BCP_GPU & CD = CDp.BCPgpu[nGPU];

    cudaEvent_t start, stop;

    if (BenchmarkAlgorithms) {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    }


    //compute the gammas by a call to the appropriate kernel
    {

        dim3 Nth (int(ABp.Ka),int(ABp.Kb));
        dim3 Nbl (int(CDp.Ka),int(CDp.Kb));

        GenK4Consts <<<Nbl, Nth, 0,*stream>>> (d_F0,     AB.dikab, CD.dikab, Lt);
    }


    const int NNY = 2;
    const int yNtiles = ((Ntiles+NNY-1)/NNY)*NNY;

    if (BenchmarkAlgorithms) cudaEventRecord(start, *stream);
    //if (BenchmarkAlgorithms) C.Start(stream); // this will not block in practice, since it's supposed to be already synchronize
    {
        dim3 Nbl (  1, yNtiles/NNY);
        dim3 Nth (NTX, NNY);

        IC->GPUK4ContractionRoutine <<<Nbl,Nth,0,*stream>>> (d_vars, d_F0, d_uvmst, ABp.Ka,ABp.Kb,CDp.Ka,CDp.Kb,  ABp.Ja,ABp.Jb,CDp.Ja,CDp.Jb,  nJ1,nJ2,nJ3,nJ4,
                                                                 AB.dka,  AB.dkb,  CD.dka,  CD.dkb,
                                                                 AB.dika, AB.dikb, CD.dika, CD.dikb,
                                                                 AB.dikab, CD.dikab,
                                                                 AB.diks3, CD.diks3,
                                                                 AB.dkab,  CD.dkab,
                                                                 AB.dNva, AB.dNub, CD.dNva, CD.dNub,
                                                                 AB.dNa,  AB.dNb,  CD.dNa,  CD.dNb,
                                                             Ntiles, ilogTh);
    }
    if (BenchmarkAlgorithms) cudaEventRecord(stop, *stream);
    //if (BenchmarkAlgorithms) C.Stop(stream);


    /*
    cudaError_t cudaResult;

    cudaResult = cudaGetLastError();
    if (cudaResult != cudaSuccess) {
        cout << endl<< "Some error ocurred before IG gpu:  " << eritype.geometry << "    " << int(LA) << " " << int(LB) << " " << int(LC) << " " << int(LD) << endl << cudaGetErrorString(cudaResult) << endl;
    }
    cudaResult = cudaGetLastError();
    if (cudaResult != cudaSuccess) {
        cout << endl<< "Some error ocurred in IG gpu:  " << eritype.geometry << "    " << int(LA) << " " << int(LB) << " " << int(LC) << " " << int(LD) << endl << cudaGetErrorString(cudaResult) << endl;
    }

    cudaResult = cudaGetLastError();
    if (cudaResult != cudaSuccess) {
        cout << endl<< "Some error ocurred in K4 gpu:  " << eritype.geometry << "    " << int(LA) << " " << int(LB) << " " << int(LC) << " " << int(LD) << endl << cudaGetErrorString(cudaResult) << endl;
    }
    */

    // synchronization so close to the kernel call is a big performance hindrance
    if (BenchmarkAlgorithms) {
        //cudaEventSynchronize(stop);
        //float ms = 0;
        //cudaEventElapsedTime(&ms, start, stop);


        K4benchmark * bench = p_Q.Benchmarks[eritype];
        bench->AddCallK4 (yNtiles, ABp,CDp);

        //#pragma omp atomic
        //bench->deltaK4 += 0.001*ms;
    }
}
