/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "quimera.hpp"

#include <iostream>
using namespace std;

#include "libquimera/libquimera.hpp"
using namespace LibQuimera;

#include "2eints/IIC.hpp"


#include "basis/SPprototype.hpp"
#include "low/chrono.hpp"
#include "low/cache.hpp"


K4benchmark::K4benchmark() {
    ncallsGAMMA = ncallsK4 = ncallsMIRROR = 0;
    deltaGAMMA  = deltaK4  = deltaMIRROR  = 0;
    tflopsGAMMA = tflopsK4 = tflopsMIRROR = 0;

    aK4 = aKa = aKb = aKc = aKd = 0;
}


void K4benchmark::AddCallK4 (const ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp)  {

    const PrimitiveSet & PSab = ABp.Psets[ET.nKab];
    const PrimitiveSet & PScd = CDp.Psets[ET.nKcd];

    int K1 = PScd.nKb;
    int K2 = ET.nKcd;

    int K3 = PSab.nKb;
    int K4 = ET.nKab;

    K3*=K2;
    K4*=K2;

    int Ka = PSab.nKa[0];
    int Kb = PSab.nKb;
    int Kc = PScd.nKa[0];
    int Kd = PScd.nKb;

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    uint32_t NFLOPS = (5 + nK4J1*JA) * K4 + (2 + nK3J1*JA + nK3J2*JA*JB) * K3 + (6 + nK2J2*JA*JB + nK2J3*JA*JB*JC) * K2 + (3 + nK1J3*JA*JB*JC + nK1J4*JA*JB*JC*JD) * K1 + (nK0J4*JA*JB*JC*JD + 17);
    NFLOPS  *= DOUBLES_PER_CACHE_LINE;


    #pragma omp atomic
    tflopsK4 += NFLOPS;

    #pragma omp atomic
    aK4     += K4;
    #pragma omp atomic
    aKa     += Ka;
    #pragma omp atomic
    aKb     += Kb;
    #pragma omp atomic
    aKc     += Kc;
    #pragma omp atomic
    aKd     += Kd;


    #pragma omp atomic
    ++ncallsK4;
}

void K4benchmark::AddCallK4 (const ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp)  {

    const PrimitiveSet & PSab = ABp.Psets[ET.nKab];
    const PrimitiveSet & PScd = CDp.Psets[ET.nKcd];

    int K1 = PScd.nKb;
    int K2 = ET.nKcd;

    int K3 = PSab.nKb;
    int K4 = ET.nKab;

    K3*=K2;
    K4*=K2;

    int Ka = PSab.nKa[0];
    int Kb = PSab.nKb;
    int Kc = PScd.nKa[0];
    int Kd = PScd.nKb;

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    uint32_t NFLOPS = (5 + nK4J1*JA) * K4 + (2 + nK3J1*JA + nK3J2*JA*JB) * K3 + (6 + nK2J2*JA*JB + nK2J3*JA*JB*JC) * K2 + (3 + nK1J3*JA*JB*JC + nK1J4*JA*JB*JC*JD) * K1 + (nK0J4*JA*JB*JC*JD + 17);
    NFLOPS  *= FLOATS_PER_CACHE_LINE;


    #pragma omp atomic
    tflopsK4 += NFLOPS;

    #pragma omp atomic
    aK4     += K4;
    #pragma omp atomic
    aKa     += Ka;
    #pragma omp atomic
    aKb     += Kb;
    #pragma omp atomic
    aKc     += Kc;
    #pragma omp atomic
    aKd     += Kd;


    #pragma omp atomic
    ++ncallsK4;
}

void K4benchmark::AddCallK4 (const ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp)  {

    const PrimitiveSet & PSab = ABp.Psets[ET.nKab];
    const PrimitiveSet & PScd = CDp.Psets[ET.nKcd];

    int K1 = PScd.nKb;
    int K2 = ET.nKcd;

    int K3 = PSab.nKb;
    int K4 = ET.nKab;

    K3*=K2;
    K4*=K2;

    int Ka = PSab.nKa[0];
    int Kb = PSab.nKb;
    int Kc = PScd.nKa[0];
    int Kd = PScd.nKb;

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    uint32_t NFLOPS = (5 + nK4J1*JA) * K4 + (2 + nK3J1*JA + nK3J2*JA*JB) * K3 + (6 + nK2J2*JA*JB + nK2J3*JA*JB*JC) * K2 + (3 + nK1J3*JA*JB*JC + nK1J4*JA*JB*JC*JD) * K1 + (nK0J4*JA*JB*JC*JD + 17);
    NFLOPS  *= DOUBLES_PER_BLOCK;


    #pragma omp atomic
    tflopsK4 += NFLOPS;

    #pragma omp atomic
    aK4     += K4;
    #pragma omp atomic
    aKa     += Ka;
    #pragma omp atomic
    aKb     += Kb;
    #pragma omp atomic
    aKc     += Kc;
    #pragma omp atomic
    aKd     += Kd;


    #pragma omp atomic
    ++ncallsK4;
}

void K4benchmark::AddCallK4 (uint32_t NT, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp)  {

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    /*
    for (int n=0; n<NT; ++n) {
        const PrimitiveSet & PSab = ABp.Psets[ET[n].nKab];
        const PrimitiveSet & PScd = CDp.Psets[ET[n].nKcd];

        int K1 = PScd.nKb;
        int K2 = ET[n].nKcd;

        int K3 = PSab.nKb;
        int K4 = ET[n].nKab;

        K3*=K2;
        K4*=K2;

        int Ka = PSab.nKa[0];
        int Kb = PSab.nKb;
        int Kc = PScd.nKa[0];
        int Kd = PScd.nKb;


        uint32_t NFLOPS = (5 + nK4J1*JA) * K4 + (2 + nK3J1*JA + nK3J2*JA*JB) * K3 + (6 + nK2J2*JA*JB + nK2J3*JA*JB*JC) * K2 + (3 + nK1J3*JA*JB*JC + nK1J4*JA*JB*JC*JD) * K1 + (nK0J4*JA*JB*JC*JD + 17);
        NFLOPS  *= DOUBLES_PER_BLOCK;


        #pragma omp atomic
        tflopsK4 += NFLOPS;

        #pragma omp atomic
        aK4     += K4;
        #pragma omp atomic
        aKa     += Ka;
        #pragma omp atomic
        aKb     += Kb;
        #pragma omp atomic
        aKc     += Kc;
        #pragma omp atomic
        aKd     += Kd;


        #pragma omp atomic
        ++ncallsK4;
    }
    */


    const int KA = ABp.Ka;
    const int KB = ABp.Kb;
    const int KC = CDp.Ka;
    const int KD = CDp.Kb;

    //for (int n=0; n<NT; ++n)
    {
        int K1 = KD;
        int K2 = KD*KC;
        int K3 = K2 * KB;
        int K4 = K3 * KA;


        uint32_t NFLOPS = (5 + nK4J1*JA) * K4 + (2 + nK3J1*JA + nK3J2*JA*JB) * K3 + (6 + nK2J2*JA*JB + nK2J3*JA*JB*JC) * K2 + (3 + nK1J3*JA*JB*JC + nK1J4*JA*JB*JC*JD) * K1 + (nK0J4*JA*JB*JC*JD + 17);
        NFLOPS  *= DOUBLES_PER_BLOCK;


        #pragma omp atomic
        tflopsK4 += NFLOPS*NT;

        #pragma omp atomic
        aK4     += K4*NT;
        #pragma omp atomic
        aKa     += KA*NT;
        #pragma omp atomic
        aKb     += KB*NT;
        #pragma omp atomic
        aKc     += KC*NT;
        #pragma omp atomic
        aKd     += KD*NT;


        #pragma omp atomic
        ncallsK4+=NT;
    }

}

void K4benchmark::AddCallM  (uint32_t NTJ4)  {

    #pragma omp atomic
    ncallsMIRROR += NTJ4;
}



void K4benchmark::Statistics(double & AtG, double & AtK, double & AtT) const {

    if (ncallsK4==0 && ncallsMIRROR==0) return;

    LibQuimera::Quimera::QBenchmarker << "ERI batch: ";
    if      (type.geometry==ABCD) LibQuimera::Quimera::QBenchmarker << "ABCD  ";
    else if (type.geometry==AACD) LibQuimera::Quimera::QBenchmarker << "AACD  ";
    else if (type.geometry==AACC) LibQuimera::Quimera::QBenchmarker << "AACC  ";
    else                          LibQuimera::Quimera::QBenchmarker << "? :" << type.geometry << "  ";

    LibQuimera::Quimera::QBenchmarker << int(type.La) << " " << int(type.Lb) << " " << int(type.Lc) << " " << int(type.Ld) << " :" << endl;

    {
        LibQuimera::Quimera::QBenchmarker << " Gamma evaluation: ";
        LibQuimera::Quimera::QBenchmarker << endl;
        LibQuimera::Quimera::QBenchmarker << "  Calls:       " << ncallsGAMMA << endl;

        double At = deltaGAMMA;

        LibQuimera::Quimera::QBenchmarker << "   Total time:     " << At << " sec" << endl;
        LibQuimera::Quimera::QBenchmarker << "   Average time:   " << At/double(1000000*ncallsGAMMA)   << " usec" << endl;

        AtG += At;
    }

    {
        LibQuimera::Quimera::QBenchmarker << " K4 contraction code: ";
        if (type.isCDR) LibQuimera::Quimera::QBenchmarker << "CDR  ";
        else            LibQuimera::Quimera::QBenchmarker << "NO CDR  ";

        //if (mC8!=NULL) LibQuimera::Quimera::QBenchmarker << " static" << endl;
        //else           LibQuimera::Quimera::QBenchmarker << " interpreted" << endl;
        LibQuimera::Quimera::QBenchmarker << endl;
        LibQuimera::Quimera::QBenchmarker << "  Calls:       " << ncallsK4 << endl;

        double At = deltaK4;
        double MFlops = double(tflopsK4)/(At*1000000);
        double time   = (1000000*At)/double(DOUBLES_PER_CACHE_LINE*ncallsK4);

        double aFLOPs = double(tflopsK4)/double(DOUBLES_PER_CACHE_LINE*ncallsK4);

        LibQuimera::Quimera::QBenchmarker << "   Total time:     " << At << " sec" << endl;
        LibQuimera::Quimera::QBenchmarker << "   Total FLOPS:    " << tflopsK4 << endl;
        LibQuimera::Quimera::QBenchmarker << "   Performance:    " << MFlops << " MFflops" << endl;
        LibQuimera::Quimera::QBenchmarker << "   FLOP breakdown: " << nK4J0 << " K4J0 + " << nK4J1 << " K4J1 + " << nK3J1 << " K3J1 + " << nK3J2 << " K3J2 + " << nK2J2 << " K2J2 + " << nK2J3 << " K2J3 + " << nK1J3 << " K1J3 + " << nK1J4 << " K1J4 + " << nK0J4 << " K0J4 FLOPS" << endl;

        LibQuimera::Quimera::QBenchmarker << "   Average FLOPs:  " << aFLOPs << " FLOPs" << endl;

        LibQuimera::Quimera::QBenchmarker << "   Average K4:     " << double(aK4)/double(ncallsK4) << endl;
        LibQuimera::Quimera::QBenchmarker << "   Average Ks:     " << double(aKa)/double(ncallsK4) << ", " << double(aKb)/double(ncallsK4) << ", " << double(aKc)/double(ncallsK4) << ", " << double(aKd)/double(ncallsK4) << endl;
        LibQuimera::Quimera::QBenchmarker << "   Average time:   " << time   << " usec" << endl;

        AtK += At;
    }

    {
        LibQuimera::Quimera::QBenchmarker << " MIRROR transformation code: ";
        //if (mC8!=NULL) LibQuimera::Quimera::QBenchmarker << " static" << endl;
        //else
        LibQuimera::Quimera::QBenchmarker << " interpreted" << endl;

        LibQuimera::Quimera::QBenchmarker << "  Calls:       " << ncallsMIRROR << endl;

        double At = deltaMIRROR;
        uint64_t tflopsMIRROR = (DOUBLES_PER_CACHE_LINE*ncallsMIRROR)*nMIRROR;
        double MFlops = double(tflopsMIRROR)/(At*1000000);
        double time   = (1000000*At)/double(DOUBLES_PER_CACHE_LINE*ncallsMIRROR);

        //LibQuimera::Quimera::QBenchmarker << " Kernel transformations:" << endl;
        LibQuimera::Quimera::QBenchmarker << "   Total time:     " << At << " sec" << endl;
        LibQuimera::Quimera::QBenchmarker << "   Total FLOPs:    " << tflopsMIRROR << endl;
        LibQuimera::Quimera::QBenchmarker << "   Performance:    " << MFlops << " MFflops" << endl;
        LibQuimera::Quimera::QBenchmarker << "   FLOP count:     " << nMIRROR << " FLOPs" << endl;
        LibQuimera::Quimera::QBenchmarker << "   Average time:   " << time   << " usec" << endl;

        AtT += At;
    }

    LibQuimera::Quimera::QBenchmarker << endl;
}

void K4benchmark::Reset() {
    ncallsK4 = ncallsMIRROR = 0;
    deltaK4  = deltaMIRROR  = 0;
    tflopsK4 = tflopsMIRROR = 0;

    aK4 = aKa = aKb = aKc = aKd = 0;
}


size_t p_ERIbuffer::SetBuffers   (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & algo) {

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    const int KA = ABp.Ka;
    const int KB = ABp.Kb;
    const int KC = CDp.Ka;
    const int KD = CDp.Kb;


    int maxK4 = KA * KB * KC * KD;

    int mF0  = 4 * maxK4;  // use twice the space in order to fit short range integrals
    int mF0e = algo.memF0e;
    int mF0f = algo.memF0f;

    int mJ1e = algo.memK4J1e * JA;
    int mJ1f = algo.memK4J1f * JA;

    int mJ2e = algo.memK3J2e * JA*JB;
    int mJ2f = algo.memK3J2f * JA*JB;

    int mJ3e = algo.memK2J3e * JA*JB*JC;
    int mJ3f = algo.memK2J3f * JA*JB*JC;

    int mJ4e = algo.memK1J4e * JA*JB*JC*JD;
    int mJ4f = algo.memK1J4f * JA*JB*JC*JD;



    size_t offset = 0;
    uint8_t * pmem = (uint8_t*)mem;

    KAB      = pmem + offset; offset += (KA * KB)*ssize;
    KCD      = pmem + offset; offset += (KC * KD)*ssize;



    F0    = pmem + offset; offset += mF0*ssize;

    F0e   = pmem + offset; offset += mF0e*ssize;
    F0f   = pmem + offset; offset += mF0f*ssize;

    K4J1e = pmem + offset; offset += mJ1e*ssize;
    K4J1f = pmem + offset; offset += mJ1f*ssize;

    K3J2e = pmem + offset; offset += mJ2e*ssize;
    K3J2f = pmem + offset; offset += mJ2f*ssize;

    K2J3e = pmem + offset; offset += mJ3e*ssize;
    K2J3f = pmem + offset; offset += mJ3f*ssize;

    K1J4e = pmem + offset; offset += mJ4e*ssize;
    K1J4f = pmem + offset; offset += mJ4f*ssize;

    bufferMIRROR  = pmem + offset; offset += algo.MaxMem*ssize; //to store intermediate values in transformations

    // for one-center integrals

    //bufferk4 = (double*)bufferK4;
    f0    = (double*) F0;
    f0e   = (double*) F0e;
    f0f   = (double*) F0f;
    k4j1e = (double*) K4J1e;
    k4j1f = (double*) K4J1f;
    k3j2e = (double*) K3J2e;
    k3j2f = (double*) K3J2f;
    k2j3e = (double*) K2J3e;
    k2j3f = (double*) K2J3f;
    k1j4e = (double*) K1J4e;
    k1j4f = (double*) K1J4f;

    buffer   = (double*)bufferMIRROR;


    return (offset);
}

size_t p_ERIbuffer::SetBuffersK4 (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & algo) {

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    const int KA = ABp.Ka;
    const int KB = ABp.Kb;
    const int KC = CDp.Ka;
    const int KD = CDp.Kb;


    int maxK4 = KA * KB * KC * KD;

    int mF0  = 2 * maxK4;
    int mF0e = algo.memF0e;
    int mF0f = algo.memF0f;

    int mJ1e = algo.memK4J1e * JA;
    int mJ1f = algo.memK4J1f * JA;

    int mJ2e = algo.memK3J2e * JA*JB;
    int mJ2f = algo.memK3J2f * JA*JB;

    int mJ3e = algo.memK2J3e * JA*JB*JC;
    int mJ3f = algo.memK2J3f * JA*JB*JC;

    int mJ4e = algo.memK1J4e * JA*JB*JC*JD;
    int mJ4f = algo.memK1J4f * JA*JB*JC*JD;



    size_t offset = 0;
    uint8_t * pmem = (uint8_t*)mem;

    KAB      = pmem + offset; offset += (KA * KB)*ssize;
    KCD      = pmem + offset; offset += (KC * KD)*ssize;

    F0    = pmem + offset; offset += mF0*ssize;

    F0e   = pmem + offset; offset += mF0e*ssize;
    F0f   = pmem + offset; offset += mF0f*ssize;

    K4J1e = pmem + offset; offset += mJ1e*ssize;
    K4J1f = pmem + offset; offset += mJ1f*ssize;

    K3J2e = pmem + offset; offset += mJ2e*ssize;
    K3J2f = pmem + offset; offset += mJ2f*ssize;

    K2J3e = pmem + offset; offset += mJ3e*ssize;
    K2J3f = pmem + offset; offset += mJ3f*ssize;

    K1J4e = pmem + offset; offset += mJ4e*ssize;
    K1J4f = pmem + offset; offset += mJ4f*ssize;

    //for one-center integrals / whatever reason
    f0    = (double*) F0;
    f0e   = (double*) F0e;
    f0f   = (double*) F0f;
    k4j1e = (double*) K4J1e;
    k4j1f = (double*) K4J1f;
    k3j2e = (double*) K3J2e;
    k3j2f = (double*) K3J2f;
    k2j3e = (double*) K2J3e;
    k2j3f = (double*) K2J3f;
    k1j4e = (double*) K1J4e;
    k1j4f = (double*) K1J4f;

    return (offset);
}

size_t p_ERIbuffer::SetBuffersM  (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & algo) {

    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    size_t offset = 0;
    uint8_t * pmem = (uint8_t*)mem;

    bufferMIRROR  = pmem + offset; offset += algo.MaxMem*ssize; //to store intermediate values in transformations

    // for one-center EFS/ whatever
    buffer   = (double*)bufferMIRROR;

    return (offset);
}


bool p_Qalgorithm::BenchmarkAlgorithms = false;

//generic ERI algorithm
p_Qalgorithm::p_Qalgorithm() {
    IC = NULL;
    SC = NULL;
    //bench = NULL;
    IsDynamic = false;
}

p_Qalgorithm::~p_Qalgorithm() {
    delete IC;
    //delete bench;
}


uint32_t p_Qalgorithm::GetNKernels() const {
    return nKernels;
}

size_t p_Qalgorithm::MemSize(const ShellPairPrototype & ABp, const ShellPairPrototype & CDp) const {


    const int JA = ABp.Ja;
    const int JB = ABp.Jb;
    const int JC = CDp.Ja;
    const int JD = CDp.Jb;

    const int KA = ABp.Ka;
    const int KB = ABp.Kb;
    const int KC = CDp.Ka;
    const int KD = CDp.Kb;

    /*
    int mF0 = algo.memF0;
    int mJ1 = algo.memJ1*JA;
    int mJ2 = algo.memJ2*JA*JB;
    int mJ3 = algo.memJ3*JA*JB*JC;
    int mJ4 = algo.memJ4*JA*JB*JC*JD;
    */


    int mF0  = memF0;
    int mF0e = memF0e;
    int mF0f = memF0f;

    int mJ1e = memK4J1e * JA;
    int mJ1f = memK4J1f * JA;

    int mJ2e = memK3J2e * JA*JB;
    int mJ2f = memK3J2f * JA*JB;

    int mJ3e = memK2J3e * JA*JB*JC;
    int mJ3f = memK2J3f * JA*JB*JC;

    int mJ4e = memK1J4e * JA*JB*JC*JD;
    int mJ4f = memK1J4f * JA*JB*JC*JD;


    int maxK4 = KA * KB * KC * KD;


    size_t offset = 0;

    //for cacheline64
    offset += MaxMem; //to store intermediate values in transformations
    //offset += K4Mem;

    offset += KA * KB;
    offset += KC * KD;

    offset += mF0*maxK4;

    //wasteful, but necessary for atypical 2-electron integrals
    offset += mF0e*maxK4; // mF0e
    offset += mF0f*maxK4; // mF0f

    offset += mJ1e;
    offset += mJ1f;

    offset += mJ2e;
    offset += mJ2f;

    offset += mJ3e;
    offset += mJ3f;

    offset += mJ4e;
    offset += mJ4f;


    return (offset * sizeof(cacheline));
}



p_Qalgorithm::p_Qalgorithm(K4routine & routine) {
    IC = NULL;
    SC = &routine;
    IsDynamic = false;
}

void p_Qalgorithm::SetStatic(ERItype   & type, K4routine & routine) {
    if (IC   !=NULL) delete IC;
    //if (bench!=NULL) delete bench;

    IC = NULL;
    SC = &routine;
    //bench = NULL;

    IsDynamic = false;

    eritype = type;
}

void p_Qalgorithm::SetIC     (ERItype   & type) {
    if (IC   !=NULL) delete IC;
    //if (bench!=NULL) delete bench;

    IC = NULL;
    SC = NULL;
    //bench = NULL;

    IsDynamic = true;

    eritype = type;
}


void p_Qalgorithm::Initialize() {

    //initialized already

    //if (IsDynamic)
    {
        //complete initialization
        IC = new ERIroutine;

        IC->Set(eritype.La, eritype.Lb, eritype.Lc, eritype.Ld, eritype.geometry, eritype.isCDR);
        IC->Init();


        //memory, etc.
        memF0    = IC->memF0;
        memF0e   = IC->memF0e;
        memF0f   = IC->memF0f;
        memK4J1e = IC->memK4J1e;
        memK4J1f = IC->memK4J1f;
        memK3J2e = IC->memK3J2e;
        memK3J2f = IC->memK3J2f;
        memK2J3e = IC->memK2J3e;
        memK2J3f = IC->memK2J3f;
        memK1J4e = IC->memK1J4e;
        memK1J4f = IC->memK1J4f;

        K4Mem    = IC->K4Mem;
        MaxMem   = IC->MaxMem;
        nKernels = IC->nKernels;

        //performance
        /*
        bench->nK4J0   = IC->nK4J0;
        bench->nK4J1   = IC->nK4J1;
        bench->nK3J1   = IC->nK3J1;
        bench->nK3J2   = IC->nK3J2;
        bench->nK2J2   = IC->nK2J2;
        bench->nK2J3   = IC->nK2J3;
        bench->nK1J3   = IC->nK1J3;
        bench->nK1J4   = IC->nK1J4;
        bench->nK0J4   = IC->nK0J4;
        bench->nMIRROR = IC->NFLOPS;
        */
    }

}

void p_Qalgorithm::Clear() {
    delete IC;
    delete SC;

    IC    = NULL;
    SC    = NULL;
}


// K4 contractions


void p_Qalgorithm::K4(const ERIgeometries32 & vars16, const ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline32 * uv_m_st16, p_ERIbuffer & buffer, bool OnlyJ) const {

    Chronometer chronoC;

    IC->CalcGammas     (vars16, ET, ABp, CDp, buffer, OnlyJ);

    if (BenchmarkAlgorithms)
        chronoC.Start();

    IC->ContractCDR_GC (vars16, ET, ABp, CDp, uv_m_st16, buffer);

    if (BenchmarkAlgorithms) {
        chronoC.Stop();
        double At = chronoC.GetTotalTime();

        K4benchmark * bench = p_Q.Benchmarks[eritype];
        bench->AddCallK4 (ET,ABp,CDp);

        #pragma omp atomic
        bench->deltaK4 += At;
    }
}


//MIRROR transformations

void p_Qalgorithm::MIRROR(const double * __restrict__ uv_m_st, double * W, p_ERIbuffer & buffer) const {
    IC->TransformAAAA(uv_m_st, W, buffer);
}

void p_Qalgorithm::MIRROR(const cacheline64 * __restrict__  mem, const ERIgeometries64 & vars8, cacheline64 * __restrict__ ERI8, p_ERIbuffer & buffer) const {

    Chronometer chronoT;

    if (BenchmarkAlgorithms)
        chronoT.Start();

    if      (eritype.geometry==ABCD) IC->TransformABCD(mem, vars8, ERI8, buffer);
    else if (eritype.geometry==AACD) IC->TransformAACD(mem, vars8, ERI8, buffer);
    else if (eritype.geometry==AACC) IC->TransformAACC(mem, vars8, ERI8, buffer);
    else if (eritype.geometry==ABAB) IC->TransformABAB(mem, vars8, ERI8, buffer);
    else                             IC->TransformABCD(mem, vars8, ERI8, buffer);

    if (BenchmarkAlgorithms) {
        chronoT.Stop();

        double At = chronoT.GetTotalTime();

        K4benchmark * bench = p_Q.Benchmarks[eritype];

        #pragma omp atomic
        ++bench->ncallsMIRROR;

        #pragma omp atomic
        bench->deltaMIRROR += At;
    }
}

void p_Qalgorithm::MIRROR(const cacheline32 * __restrict__  mem, const ERIgeometries32 & vars16, cacheline32 * __restrict__ ERI16, p_ERIbuffer & buffer) const {

    Chronometer chronoT;

    if (BenchmarkAlgorithms)
        chronoT.Start();

    if      (eritype.geometry==ABCD) IC->TransformABCD(mem, vars16, ERI16, buffer);
    else if (eritype.geometry==AACD) IC->TransformAACD(mem, vars16, ERI16, buffer);
    else if (eritype.geometry==AACC) IC->TransformAACC(mem, vars16, ERI16, buffer);
    else if (eritype.geometry==ABAB) IC->TransformABAB(mem, vars16, ERI16, buffer);
    else                             IC->TransformABCD(mem, vars16, ERI16, buffer);

    if (BenchmarkAlgorithms) {
        chronoT.Stop();

        double At = chronoT.GetTotalTime();

        K4benchmark * bench = p_Q.Benchmarks[eritype];

        #pragma omp atomic
        ++bench->ncallsMIRROR;

        #pragma omp atomic
        bench->deltaMIRROR += At;
    }
}




p_Quimera p_Q;

p_Quimera::p_Quimera() {
    //link all static code
    LinkStatic(true);
}

p_Quimera::~p_Quimera() {

    map<ERItype, Qalgorithm*>::iterator it;

    for (it = Solvers.begin(); it!=Solvers.end(); ++it) {
        //remember to destroy the pointers
        delete it->second;
        it->second = NULL;
    }

    map<ERItype, K4benchmark*>::iterator itb;

    for (itb = Benchmarks.begin(); itb!=Benchmarks.end(); ++itb) {
        //remember to destroy the pointers
        delete itb->second;
        itb->second = NULL;
    }
}

void p_Quimera::SetStatic(GEOM geometry, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld,    K4routine routine, bool overwrite=false) {

    bool UseCASE = Quimera::GetCASE();
    bool UseGC   = Quimera::GetGC();
    bool UseCDR  = Quimera::GetCDR();

    ERItype type;
    type(geometry, la, lb, lc, ld, UseGC, false, UseCDR);

    //check whether an algorithm already exists
    if (!overwrite && Solvers.count(type) == 1) return;

    //clear if already existing
    if (Solvers.count(type) == 1) Solvers[type]->pQalgorithm->Clear();

    //set the algorithm as a static link to the routine
    Solvers[type]->pQalgorithm->SetStatic(type, routine);
}


//#include "2eints/K4GC_abcd/K4GC_ABCD.hpp"
//#include "2eints/CDRGC_abcd/CDRGC_ABCD.hpp"

void p_Quimera::LinkStatic(bool overwrite = false) {

    //some static libraries
    //ERItype GC_CDR;
    //GC_CDR(ABCD, 0,0,0,0, false, false, true);
    //SetStatic ( GC_CDR , CDRGC_ABCD_SSSS, overwrite);

    /*
    SetStatic ( ABCD, 0,0,0,0, false, false, true , K4GC_ABCD_SSSS, overwrite);
    SetStatic ( ABCD, 1,0,0,0, false, false, true , K4GC_ABCD_PSSS, overwrite);
    SetStatic ( ABCD, 1,0,1,0, false, false, true , K4GC_ABCD_PSPS, overwrite);
    SetStatic ( ABCD, 1,1,0,0, false, false, true , K4GC_ABCD_PPSS, overwrite);
    SetStatic ( ABCD, 1,1,1,0, false, false, true , K4GC_ABCD_PPPS, overwrite);
    SetStatic ( ABCD, 1,1,1,1, false, false, true , K4GC_ABCD_PPPP, overwrite);
    */

    /*
    SetStatic ( ABCD, 2,0,0,0, false, false, true , K4GC_ABCD_DSSS, overwrite);
    SetStatic ( ABCD, 2,0,1,0, false, false, true , K4GC_ABCD_DSPS, overwrite);
    SetStatic ( ABCD, 2,0,1,1, false, false, true , K4GC_ABCD_DSPP, overwrite);
    SetStatic ( ABCD, 2,0,2,0, false, false, true , K4GC_ABCD_DSDS, overwrite);
    SetStatic ( ABCD, 2,1,0,0, false, false, true , K4GC_ABCD_DPSS, overwrite);
    SetStatic ( ABCD, 2,1,1,0, false, false, true , K4GC_ABCD_DPPS, overwrite);
    SetStatic ( ABCD, 2,1,1,1, false, false, true , K4GC_ABCD_DPPP, overwrite);
    SetStatic ( ABCD, 2,1,2,0, false, false, true , K4GC_ABCD_DPDS, overwrite);
    SetStatic ( ABCD, 2,1,2,1, false, false, true , K4GC_ABCD_DPDP, overwrite);
    SetStatic ( ABCD, 2,2,0,0, false, false, true , K4GC_ABCD_DDSS, overwrite);
    SetStatic ( ABCD, 2,2,1,0, false, false, true , K4GC_ABCD_DDPS, overwrite);
    SetStatic ( ABCD, 2,2,1,1, false, false, true , K4GC_ABCD_DDPP, overwrite);
    SetStatic ( ABCD, 2,2,2,0, false, false, true , K4GC_ABCD_DDDS, overwrite);
    SetStatic ( ABCD, 2,2,2,1, false, false, true , K4GC_ABCD_DDDP, overwrite);
    SetStatic ( ABCD, 2,2,2,2, false, false, true , K4GC_ABCD_DDDD, overwrite);
    */
}


void p_Quimera::ListNeeded  (GEOM geometry, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld) {

    bool UseCASE = Quimera::GetCASE();
    bool UseGC   = Quimera::GetGC();
    bool UseCDR  = Quimera::GetCDR();

    ERItype type;
    type(geometry, la, lb, lc, ld, UseGC, false, UseCDR);

    SetLater.push(type);
}

void p_Quimera::GenerateNeeded(bool overwrite) {

    //fill the map with the needed keys for later initialization
    while (SetLater.size() != 0) {
        ERItype type;
        type = SetLater.top();
        SetLater.pop();

        //marks the algorithm for later initialization
        if (overwrite || Solvers.count(type)==0) {
            Solvers[type] = new Qalgorithm; //->pQalgorithm->SetIC(type);
            Solvers[type]->pQalgorithm->SetIC(type);
        }
    }

    Quimera::QMessenger << "Initializing interpreted routines";
    Quimera::QMessenger.Push(); {
        map<ERItype, Qalgorithm*>::iterator it = Solvers.begin();
        int n = Solvers.size();

        //#pr#agma omp parallel for schedule(dynamic)
        for (uint32_t i=0; i<n; ++i) {

            Qalgorithm * algo;

            //#pra#gma omp critical
            {
                algo = (it->second);
                ++it;
            }

            algo->pQalgorithm->Initialize();

            //Quimera::QMessenger.Out("*");
        }
    } Quimera::QMessenger.Pop();


    std::map <ERItype, Qalgorithm*>::iterator it;

    for (it=Solvers.begin(); it!=Solvers.end(); ++it) {
        ERItype type = it->first;

        Benchmarks[type] = new K4benchmark;
        Benchmarks[type]->type = type;

        //performance
        Benchmarks[type]->nK4J0   = it->second->pQalgorithm->IC->nK4J0;
        Benchmarks[type]->nK4J1   = it->second->pQalgorithm->IC->nK4J1;
        Benchmarks[type]->nK3J1   = it->second->pQalgorithm->IC->nK3J1;
        Benchmarks[type]->nK3J2   = it->second->pQalgorithm->IC->nK3J2;
        Benchmarks[type]->nK2J2   = it->second->pQalgorithm->IC->nK2J2;
        Benchmarks[type]->nK2J3   = it->second->pQalgorithm->IC->nK2J3;
        Benchmarks[type]->nK1J3   = it->second->pQalgorithm->IC->nK1J3;
        Benchmarks[type]->nK1J4   = it->second->pQalgorithm->IC->nK1J4;
        Benchmarks[type]->nK0J4   = it->second->pQalgorithm->IC->nK0J4;
        Benchmarks[type]->nMIRROR = it->second->pQalgorithm->IC->NFLOPS;
    }

}

void p_Quimera::Statistics() const {

    LibQuimera::Quimera::QBenchmarker << "ERI statistics:" << endl;


    /*
    #ifdef USE_GPU
    // if we are using streams, we still need a last synchronization and adding the times
    {
        map<ERItype, LibQuimera::Qalgorithm*>::const_iterator it;

        for (it = Solvers.begin(); it!=Solvers.end(); ++it) {
            p_Qalgorithm * pQa = it->second->p_Qalgorithm;
            double T = p_Qa->C.GetTotalTime();
            benchmark * bench = p_Q.Benchmarks[p_Qa->eritype];

            #pragma omp atomic
            bench->deltaK4 += T;
        }
    }
    #endif
    */


    map<ERItype, K4benchmark*>::const_iterator it;

    double ATG = 0;
    double ATK = 0;
    double ATT = 0;

    for (it = Benchmarks.begin(); it!=Benchmarks.end(); ++it)
        it->second->Statistics(ATG, ATK, ATT);

    LibQuimera::Quimera::QBenchmarker << endl;
    //LibQuimera::Quimera::QBenchmarker << "Total time spent in contractions + tranformations: " << AT << endl;
    LibQuimera::Quimera::QBenchmarker << "Total time spent in gamma:          " << ATG << endl;
    LibQuimera::Quimera::QBenchmarker << "Total time spent in contractions:   " << ATK << endl;
    LibQuimera::Quimera::QBenchmarker << "Total time spent in tranformations: " << ATT << endl;
}

const Qalgorithm * p_Quimera::SelectAlgorithm (GEOM geometry, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld) {

    bool UseCASE = Quimera::GetCASE();
    bool UseGC   = Quimera::GetGC();
    bool UseCDR  = Quimera::GetCDR();


    ERItype type;
    type(geometry, la, lb, lc, ld, UseGC, false, UseCDR);

    //if there is no algorithm in the pool
    if (Solvers.count(type) == 0) {
        //lazy initialization
        {
            Solvers[type] = new Qalgorithm;
            Solvers[type]->pQalgorithm->SetIC(type);
            Solvers[type]->pQalgorithm->Initialize();
        }

        {
            Benchmarks[type] = new K4benchmark;
            Benchmarks[type]->type = type;

            //performance
            Benchmarks[type]->nK4J0   = Solvers[type]->pQalgorithm->IC->nK4J0;
            Benchmarks[type]->nK4J1   = Solvers[type]->pQalgorithm->IC->nK4J1;
            Benchmarks[type]->nK3J1   = Solvers[type]->pQalgorithm->IC->nK3J1;
            Benchmarks[type]->nK3J2   = Solvers[type]->pQalgorithm->IC->nK3J2;
            Benchmarks[type]->nK2J2   = Solvers[type]->pQalgorithm->IC->nK2J2;
            Benchmarks[type]->nK2J3   = Solvers[type]->pQalgorithm->IC->nK2J3;
            Benchmarks[type]->nK1J3   = Solvers[type]->pQalgorithm->IC->nK1J3;
            Benchmarks[type]->nK1J4   = Solvers[type]->pQalgorithm->IC->nK1J4;
            Benchmarks[type]->nK0J4   = Solvers[type]->pQalgorithm->IC->nK0J4;
            Benchmarks[type]->nMIRROR = Solvers[type]->pQalgorithm->IC->NFLOPS;
        }
    }

    return (Solvers[type]);
}
/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __QUIMERA__
#define __QUIMERA__

#include <map>
#include <queue>

#include "defs.hpp"

class ShellPairPrototype;
class ShellPair;

class ERIroutine;
class ERItype;
class K4benchmark;

class cacheline;
class cacheline64;
class cacheline32;
template <int N> class cachelineN;

class ERIgeometries64;
class ERIgeometries32;
template <int N> class ERIgeometriesN;

class p_Qalgorithm;
class p_ERIbuffer;
class p_Quimera;

namespace LibQuimera {
    struct ERITile64;
    struct ERITile32;
    template <int N> struct ERITile;

    class Quimera;
    class Qalgorithm;
    class ERIbuffer;
}


typedef void (*K4routine)  (p_ERIbuffer &, const ERIgeometries64 &, const LibQuimera::ERITile64 &, const ShellPairPrototype &, const ShellPairPrototype &, cacheline64 *);
typedef void (*K4routineF) (p_ERIbuffer &, const ERIgeometries32 &, const LibQuimera::ERITile32 &, const ShellPairPrototype &, const ShellPairPrototype &, cacheline32 *);


struct ERItype {
    //basic information
    GEOM geometry;
    uint8_t  La;
    uint8_t  Lb;
    uint8_t  Lc;
    uint8_t  Ld;

    bool isGC;  //general contraction
    bool isLR;  //long range integrals
    bool isCDR; //CDR in K4
    bool inGPU;

    ERItype & operator()(GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld, bool gc, bool lr, bool cdr) {
        geometry = geo;
        La = la;
        Lb = lb;
        Lc = lc;
        Ld = ld;
        isGC  = gc;
        isLR  = lr;
        isCDR = cdr;
        inGPU = false;

        return *this;
    }

    //to enumerate them
    inline bool operator<(const ERItype & rhs) const {
        if (geometry!=rhs.geometry) return geometry<rhs.geometry;
        if (La      !=rhs.La      ) return La      <rhs.La;
        if (Lb      !=rhs.Lb      ) return Lb      <rhs.Lb;
        if (Lc      !=rhs.Lc      ) return Lc      <rhs.Lc;
        if (Ld      !=rhs.Ld      ) return Ld      <rhs.Ld;

        if (isGC  != rhs.isGC)  return isGC;
        if (isCDR != rhs.isCDR) return isCDR;
        if (isLR  != rhs.isLR)  return isLR;
        if (inGPU != rhs.inGPU) return inGPU;

        return false;
    }
};


//some class should have performance data
//also, variable precision
struct K4benchmark {

    ERItype type;

    double deltaGAMMA;
    uint64_t ncallsGAMMA;
    uint64_t tflopsGAMMA;

    double deltaK4;
    uint64_t ncallsK4;
    uint64_t tflopsK4;

    double deltaMIRROR;
    uint64_t ncallsMIRROR;
    uint64_t tflopsMIRROR;


    //average Ks
    uint64_t aK4;

    uint64_t aKa;
    uint64_t aKb;
    uint64_t aKc;
    uint64_t aKd;

    //FLOPs in each loop
    int nK4J0;
    int nK4J1;
    int nK3J1;
    int nK3J2;
    int nK2J2;
    int nK2J3;
    int nK1J3;
    int nK1J4;
    int nK0J4;

    int nMIRROR;



    K4benchmark();
    void   AddCallK4 (const LibQuimera::ERITile64 & ET                 , const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);
    void   AddCallK4 (const LibQuimera::ERITile32 & ET                 , const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);
    void   AddCallK4 (const LibQuimera::ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);
    void   AddCallK4 (uint32_t NT,                           const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);

    void   AddCallM  (uint32_t NTJ4);

    void   Statistics(double & AtG, double & AtK, double & AtT) const;
    void   Reset();
};


class p_ERIbuffer {
    friend class ERIroutine;
    friend class LibQuimera::ERIbuffer;

  public:
    double * buffer;
    double * bufferk4;
    double * v_m;
    double * uv_m;
    double * uv_m_t;

    double * f0;
    double * f0e;
    double * f0f;
    double * k4j1e;
    double * k4j1f;
    double * k3j2e;
    double * k3j2f;
    double * k2j3e;
    double * k2j3f;
    double * k1j4e;
    double * k1j4f;


    void * bufferMIRROR;
    void * bufferK4;
    void * v_m8;
    void * uv_m8;
    void * uv_m_t8;

    void * KAB;
    void * KCD;

  public:

    void * F0;
    void * F0e;
    void * F0f;
    void * K4J1e;
    void * K4J1f;
    void * K3J2e;
    void * K3J2f;
    void * K2J3e;
    void * K2J3f;
    void * K1J4e;
    void * K1J4f;

    size_t SetBuffers   (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & routine);
    size_t SetBuffersK4 (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & routine);
    size_t SetBuffersM  (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & routine);
};

#ifndef __CUDACC__
class cudaStream_t;
#endif

class p_Qalgorithm {

    friend class p_ERIbuffer;
    friend class p_Quimera;
    friend class LibQuimera::Quimera;    // this will set BenchmarkAlgorithms at its inizialization step
    friend class LibQuimera::Qalgorithm; // this class accesses its methods directly

    static bool BenchmarkAlgorithms;

    //private:
    ERItype eritype;

    ERIroutine * IC; //interpreted code
    K4routine  * SC; //actual routine
    bool IsDynamic;

    //K4benchmark * bench;

    //for K4buffer
    uint32_t MaxMem;
    uint32_t K4Mem;

    uint32_t memF0;
    uint32_t memF0e;
    uint32_t memF0f;
    uint32_t memK4J1e;
    uint32_t memK4J1f;
    uint32_t memK3J2e;
    uint32_t memK3J2f;
    uint32_t memK2J3e;
    uint32_t memK2J3f;
    uint32_t memK1J4e;
    uint32_t memK1J4f;

    uint32_t nKernels;


    //init
    p_Qalgorithm(K4routine & routine);
    void SetStatic (ERItype   & type, K4routine & routine);
    void SetIC     (ERItype   & type);
    void Initialize();
    void Clear();

    //public
    p_Qalgorithm();
   ~p_Qalgorithm();

    uint32_t GetNKernels() const;

    size_t MemSize    (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp) const;

    //1 center routines
    void K4     (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;
    void MIRROR (const double *  uv_m_st, double * W, p_ERIbuffer & buffer) const;

    //packed double routines
    void K4     (const ERIgeometries64 & vars8, const LibQuimera::ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;
    void MIRROR (const cacheline64 *   mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, p_ERIbuffer & buffer) const;

    //packed single routines
    void K4     (const ERIgeometries32 & vars16, const LibQuimera::ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline32 * uv_m_st16, p_ERIbuffer & buffer, bool OnlyJ = false) const;
    void MIRROR (const cacheline32 *   mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, p_ERIbuffer & buffer) const;

    //free vector routines
    void K4     (const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, const LibQuimera::ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cachelineN<DOUBLES_PER_BLOCK> * uv_m_st, p_ERIbuffer & buffer, bool OnlyJ = false) const;
    void MIRROR (const cachelineN<DOUBLES_PER_BLOCK> * mem,  const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, cachelineN<DOUBLES_PER_BLOCK> * ERI, p_ERIbuffer & buffer) const;

    //GPU routines
    void K4     (double * d_geom,  double * d_F0, double * d_ab, double * d_cd, const LibQuimera::ERITile<DOUBLES_PER_BLOCK> * ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, uint32_t Ntiles, cudaStream_t * stream) const;
    void MIRROR (double * d_kern,  double * d_geom,  double * d_ERI, uint32_t Ntiles, int J4, cudaStream_t * stream) const;
};

// declare template member function specializations in header



class p_Quimera {

    friend class LibQuimera::Quimera;
    friend class p_Qalgorithm;

  private:

    //private:
    std::map             <ERItype, LibQuimera::Qalgorithm*>  Solvers;
    std::map             <ERItype, K4benchmark*> Benchmarks;

    std::priority_queue <ERItype>              SetLater;

    void SetStatic      (GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld, K4routine routine, bool overwrite);
    void SetInterpreted (GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld,                    bool overwrite);

    void LinkStatic     (bool overwrite);
    void LinkLibrary    (bool overwrite);

    const LibQuimera::Qalgorithm * SelectAlgorithm (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld);

    void ListNeeded     (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld);
    void GenerateNeeded (bool overwrite);

    void Statistics() const;

  public:

    p_Quimera();
   ~p_Quimera();

} extern p_Q;



#endif


