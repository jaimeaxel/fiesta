/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    VILIC: Variable Instruction Length Interpreted Code
*/

#ifndef __VILIC__
#define __VILIC__

#include "defs.hpp"

/*

AERR4/MOVE      F0e[s2->dest] = F0e[s2->op1] * iKpq;


K4E,K4F,K3E ... dest, op1, aux

AERR3/AERR2/AERR1/AERR0        store(dest, load(op1) + load(op2)

CDR4          F0f[s2->dest] = (R2 * F0f[s2->op1] + F0e[s2->ope]) * im2[s2->aux];
CDR3/CDR2         store(dest, (AQ2 * load(op1) - AQABb * load(op2) + AB2b2 * load(op3) + load(ope)) * im2[aux]);
CDR1/CDR0         store(dest, (AC2 * load(op1) - ACAB * load(op2) + ACCDd * load(op3) +
                         AB2 * load(op4) - ABCDd * load(op5) + CD2d2 * load(op6) +
                        load(ope)) * im2[aux]);

*/


class Op_K4;


struct VILsimple {
    uint16_t dest;
    uint16_t op1;
};

struct VILaerr {
    uint16_t dest;
    uint16_t op1;
    uint16_t op2;
};

struct VILcdr4 {
    uint16_t dest;
    uint16_t op1;
    uint16_t ope;
    uint16_t aux;
};

struct VILcdr23 {
    uint16_t dest;
    uint16_t op1;
    uint16_t op2;
    uint16_t op3;
    uint16_t ope;
    uint16_t aux;
};

struct VILcdr01 {
    uint16_t dest;
    uint16_t op1;
    uint16_t op2;
    uint16_t op3;
    uint16_t op4;
    uint16_t op5;
    uint16_t op6;
    uint16_t ope;
    uint16_t aux;
};

struct VILfma {
    uint16_t dest;
    uint16_t op1;
    uint16_t aux;
};


struct VILcode {

    uint32_t nbi[32];

//BOYS,
//ADRR4, AERR4, CDR4, K4D, K4E, K4F,
    VILsimple * pAERR4;
    VILcdr4   * pCDR4;

    VILfma    * pK4E;
    VILfma    * pK4F;

//ADRR3, AERR3, CDR3, K3D, K3E, K3F,
    VILaerr   * pAERR3;
    VILcdr23  * pCDR3;

    VILfma    * pK3E;
    VILfma    * pK3F;

//ADRR2, AERR2, CDR2, K2D, K2E, K2F,
    VILaerr   * pAERR2;
    VILcdr23  * pCDR2;

    VILfma    * pK2E;
    VILfma    * pK2F;

//ADRR1, AERR1, CDR1, K1D, K1E, K1F,

    VILaerr   * pAERR1;
    VILcdr01  * pCDR1;

    VILfma    * pK1E;
    VILfma    * pK1F;

//ADRR0, AERR0, CDR0,

    VILaerr   * pAERR0;
    VILcdr01  * pCDR0;

    //VILsimple * pMOVE;

//NADA
    uint16_t * code;
    int    size;
    int    ninstr;


    VILcode() {
        code = 0;
        size = 0;
        ninstr = 0;

        for (int i=0; i<32; ++i) nbi[i] = 0;
    }

    ~VILcode() {
        delete[] code;
    }

    void Init (const uint32_t ninstrK4[32], const Op_K4 * const nseqK4[32]);
};




#endif
