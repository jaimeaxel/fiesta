
#include <iostream>
using namespace std;

#include "IIC.hpp"

#include <string.h>

#include "math/angular.hpp"
using namespace LibAngular;

#include "2eints/quimera.hpp"
#include "libquimera/ERIgeom.hpp"
#include "libquimera/libquimera.hpp"
using namespace LibQuimera;

const int DPB = DOUBLES_PER_BLOCK;

// 3168 bytes
__constant__ double cCL[LMAX+1][2*LMAX+1][6];

void InitDeviceSphH() {

    size_t size = (2*LMAX+1)*6*sizeof(double);

    for (int l=0; l<=LMAX; ++l) {

        double CL[11][6];

        for (int m=0; m<2*LMAX+1; ++m)
            for (int n=0; n<6; ++n)
                CL[m][n] = 0;

        for (int m=0; m<2*l+1; ++m)
            for (int n=0; n<SHList[l][m].nps; ++n)
                CL[m][n] = SHList[l][m].T[n].cN;

        cudaMemcpyToSymbol (cCL, &(CL[0][0]), size, l*size);
    }
}


__global__ void KernelABCD (const double  * mem, const double * vars, double * ERIN, double * buffer,
                            const Op_MIRROR * d_eseq, const uint32_t * d_ninstr,
                            uint8_t cula, uint8_t culb, uint8_t culc, uint8_t culd,
                            uint32_t cuKernels, uint32_t cuMaxMem, uint32_t cuM4, uint32_t cuJ4,
                            int Ntiles) {

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;


    double ABz = vars[k*6*NTX +     i];
    double CDy = vars[k*6*NTX +   NTX+i];
    double CDz = vars[k*6*NTX + 2*NTX+i];
    double ACx = vars[k*6*NTX + 3*NTX+i];
    double ACy = vars[k*6*NTX + 4*NTX+i];
    double ACz = vars[k*6*NTX + 5*NTX+i];


    const Op_MIRROR * s2 = d_eseq;
    double * bufferN = buffer + (k*cuJ4+j)*cuMaxMem +  i;

    //copy kernels to new memory
    for (int p=0; p<d_ninstr[KERNELS]; ++s2,++p) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        buffer[(k*cuJ4+j)*cuMaxMem + p*NTX + i] = mem[(k*cuJ4+j)*cuKernels + p*NTX + i];
    }

    for (int p=0; p<d_ninstr[MMDZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = ACz * *op3 - ABz * *op1 + CDz * *op2 - z* *op4;
    }

    for (int p=0; p<d_ninstr[CTEBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = *op1 * z + ABz * *op2 + * op3;
    }

    for (int p=0; p<d_ninstr[CTEKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = -s2->aux;
        double z = -double(s2->aux);

        *dest = *op1 * z + CDz * *op2 - *op3;
    }

    for (int p=0; p<d_ninstr[MMDY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = ACy * *op3 + CDy * *op2 - y * *op4;
    }

    for (int p=0; p<d_ninstr[MMDX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t xxx = s2->aux;
        double x = double(s2->aux);

        *dest = ACx * *op3 - x* *op4;
    }

    for (int p=0; p<d_ninstr[CTEKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = -s2->aux;
        double y = -double(s2->aux);

        *dest = *op1 * y + CDy * *op2 - * op3;
    }

    for (int p=0; p<d_ninstr[CTEKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = -s2->aux;
        double x = -double(s2->aux);

        *dest = *op1 * x - * op3;
    }

    for (int p=0; p<d_ninstr[CTEBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = *op1 * y + * op3;
    }

    for (int p=0; p<d_ninstr[CTEBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = s2->aux;
        double x = double(s2->aux);

        *dest = *op1 * x + * op3;
    }

    for (int p=0; p<d_ninstr[HRRBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - ABz * *op2;
    }

    for (int p=0; p<d_ninstr[HRRBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHA]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (cula>1) {
            const double * cCa = cCL[cula][m];
            double sum = 0;
            if (cCa[0]!=0) sum  = *op1 * cCa[0];
            if (cCa[1]!=0) sum += *op2 * cCa[1];
            if (cCa[2]!=0) sum += *op3 * cCa[2];
            if (cCa[3]!=0) sum += *op4 * cCa[3];
            if (cCa[4]!=0) sum += *op5 * cCa[4];
            if (cCa[5]!=0) sum += *op6 * cCa[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHB]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culb>1) {
            const double * cCb = cCL[culb][m];
            double sum = 0;
            if (cCb[0]!=0) sum  = *op1 * cCb[0];
            if (cCb[1]!=0) sum += *op2 * cCb[1];
            if (cCb[2]!=0) sum += *op3 * cCb[2];
            if (cCb[3]!=0) sum += *op4 * cCb[3];
            if (cCb[4]!=0) sum += *op5 * cCb[4];
            if (cCb[5]!=0) sum += *op6 * cCb[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - CDz * *op2;
    }

    for (int p=0; p<d_ninstr[HRRKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - CDy * *op2;
    }

    for (int p=0; p<d_ninstr[HRRKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHC]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culc>1) {
            const double * cCc = cCL[culc][m];
            double sum = 0;
            if (cCc[0]!=0) sum  = *op1 * cCc[0];
            if (cCc[1]!=0) sum += *op2 * cCc[1];
            if (cCc[2]!=0) sum += *op3 * cCc[2];
            if (cCc[3]!=0) sum += *op4 * cCc[3];
            if (cCc[4]!=0) sum += *op5 * cCc[4];
            if (cCc[5]!=0) sum += *op6 * cCc[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHD]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culd>1) {
            const double * cCd = cCL[culd][m];
            double sum = 0;
            if (cCd[0]!=0) sum  = *op1 * cCd[0];
            if (cCd[1]!=0) sum += *op2 * cCd[1];
            if (cCd[2]!=0) sum += *op3 * cCd[2];
            if (cCd[3]!=0) sum += *op4 * cCd[3];
            if (cCd[4]!=0) sum += *op5 * cCd[4];
            if (cCd[5]!=0) sum += *op6 * cCd[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[REORDER]; ++s2,++p) {
        double       * dest = ERIN   + (k*cuJ4+j)*cuM4     + NTX*s2->dest + i;
        const double * op1  = buffer + (k*cuJ4+j)*cuMaxMem + NTX*s2->op1 + i;

        *dest = *op1;
    }

}

__global__ void KernelAACD (const double  * mem, const double * vars, double * ERIN, double * buffer,
                            const Op_MIRROR * d_eseq, const uint32_t * d_ninstr,
                            uint8_t cula, uint8_t culb, uint8_t culc, uint8_t culd,
                            uint32_t cuKernels, uint32_t cuMaxMem, uint32_t cuM4, uint32_t cuJ4,
                            int Ntiles) {

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;


    double CDz = vars[k*6*NTX + 2*NTX+i];
    double ACy = vars[k*6*NTX + 4*NTX+i];
    double ACz = vars[k*6*NTX + 5*NTX+i];


    const Op_MIRROR * s2 = d_eseq;
    double * bufferN = buffer + (k*cuJ4+j)*cuMaxMem +  i;

    //copy kernels to new memory
    for (int p=0; p<d_ninstr[KERNELS]; ++s2,++p) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        buffer[(k*cuJ4+j)*cuMaxMem + p*NTX + i] = mem[(k*cuJ4+j)*cuKernels + p*NTX + i];
    }

    for (int p=0; p<d_ninstr[MMDZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = ACz * *op3 + CDz * *op2 - z* *op4;
    }

    for (int p=0; p<d_ninstr[CTEBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = *op1 * z + * op3;
    }

    for (int p=0; p<d_ninstr[CTEKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = -s2->aux;
        double z = -double(s2->aux);

        *dest = *op1 * z + CDz * *op2 - *op3;
    }

    for (int p=0; p<d_ninstr[MMDY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = ACy * *op3 - y * *op4;
    }

    for (int p=0; p<d_ninstr[MMDX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t xxx = s2->aux;
        double x = -double(s2->aux);

        *dest =  x * *op4;
    }

    for (int p=0; p<d_ninstr[CTEKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = -s2->aux;
        double y = -double(s2->aux);

        *dest = *op1 * y  - * op3;
    }

    for (int p=0; p<d_ninstr[CTEKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = -s2->aux;
        double x = -double(s2->aux);

        *dest = *op1 * x - * op3;
    }

    for (int p=0; p<d_ninstr[CTEBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = *op1 * y + * op3;
    }

    for (int p=0; p<d_ninstr[CTEBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = s2->aux;
        double x = double(s2->aux);

        *dest = *op1 * x + * op3;
    }

    for (int p=0; p<d_ninstr[HRRBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHA]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (cula>1) {
            const double * cCa = cCL[cula][m];
            double sum = 0;
            if (cCa[0]!=0) sum  = *op1 * cCa[0];
            if (cCa[1]!=0) sum += *op2 * cCa[1];
            if (cCa[2]!=0) sum += *op3 * cCa[2];
            if (cCa[3]!=0) sum += *op4 * cCa[3];
            if (cCa[4]!=0) sum += *op5 * cCa[4];
            if (cCa[5]!=0) sum += *op6 * cCa[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHB]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culb>1) {
            const double * cCb = cCL[culb][m];
            double sum = 0;
            if (cCb[0]!=0) sum  = *op1 * cCb[0];
            if (cCb[1]!=0) sum += *op2 * cCb[1];
            if (cCb[2]!=0) sum += *op3 * cCb[2];
            if (cCb[3]!=0) sum += *op4 * cCb[3];
            if (cCb[4]!=0) sum += *op5 * cCb[4];
            if (cCb[5]!=0) sum += *op6 * cCb[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - CDz * *op2;
    }

    for (int p=0; p<d_ninstr[HRRKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHC]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culc>1) {
            const double * cCc = cCL[culc][m];
            double sum = 0;
            if (cCc[0]!=0) sum  = *op1 * cCc[0];
            if (cCc[1]!=0) sum += *op2 * cCc[1];
            if (cCc[2]!=0) sum += *op3 * cCc[2];
            if (cCc[3]!=0) sum += *op4 * cCc[3];
            if (cCc[4]!=0) sum += *op5 * cCc[4];
            if (cCc[5]!=0) sum += *op6 * cCc[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHD]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culd>1) {
            const double * cCd = cCL[culd][m];
            double sum = 0;
            if (cCd[0]!=0) sum  = *op1 * cCd[0];
            if (cCd[1]!=0) sum += *op2 * cCd[1];
            if (cCd[2]!=0) sum += *op3 * cCd[2];
            if (cCd[3]!=0) sum += *op4 * cCd[3];
            if (cCd[4]!=0) sum += *op5 * cCd[4];
            if (cCd[5]!=0) sum += *op6 * cCd[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[REORDER]; ++s2,++p) {
        double       * dest = ERIN   + (k*cuJ4+j)*cuM4     + NTX*s2->dest + i;
        const double * op1  = buffer + (k*cuJ4+j)*cuMaxMem + NTX*s2->op1 + i;

        *dest = *op1;
    }

}

__global__ void KernelAACC (const double  * mem, const double * vars, double * ERIN, double * buffer,
                            const Op_MIRROR * d_eseq, const uint32_t * d_ninstr,
                            uint8_t cula, uint8_t culb, uint8_t culc, uint8_t culd,
                            uint32_t cuKernels, uint32_t cuMaxMem, uint32_t cuM4, uint32_t cuJ4,
                            int Ntiles) {

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;


    double ACz = vars[k*6*NTX + 5*NTX+i];

    const Op_MIRROR * s2 = d_eseq;
    double * bufferN = buffer + (k*cuJ4+j)*cuMaxMem +  i;

    //copy kernels to new memory
    for (int p=0; p<d_ninstr[KERNELS]; ++s2,++p) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        buffer[(k*cuJ4+j)*cuMaxMem + p*NTX + i] = mem[(k*cuJ4+j)*cuKernels + p*NTX + i];
    }

    for (int p=0; p<d_ninstr[MMDZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = ACz * *op3 - z * *op4;
    }

    for (int p=0; p<d_ninstr[CTEBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = *op1 * z + * op3;
    }

    for (int p=0; p<d_ninstr[CTEKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = -s2->aux;
        double z = -double(s2->aux);

        *dest = *op1 * z - *op3;
    }

    for (int p=0; p<d_ninstr[MMDY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t yyy = s2->aux;
        double y = -double(s2->aux);

        *dest = y * *op4;
    }

    for (int p=0; p<d_ninstr[MMDX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t xxx = s2->aux;
        double x = -double(s2->aux);

        *dest =  x * *op4;
    }

    for (int p=0; p<d_ninstr[CTEKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = -s2->aux;
        double y = -double(s2->aux);

        *dest = *op1 * y  - * op3;
    }

    for (int p=0; p<d_ninstr[CTEKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = -s2->aux;
        double x = -double(s2->aux);

        *dest = *op1 * x - * op3;
    }

    for (int p=0; p<d_ninstr[CTEBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = *op1 * y + * op3;
    }

    for (int p=0; p<d_ninstr[CTEBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = s2->aux;
        double x = double(s2->aux);

        *dest = *op1 * x + * op3;
    }

    for (int p=0; p<d_ninstr[HRRBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHA]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (cula>1) {
            const double * cCa = cCL[cula][m];
            double sum = 0;
            if (cCa[0]!=0) sum  = *op1 * cCa[0];
            if (cCa[1]!=0) sum += *op2 * cCa[1];
            if (cCa[2]!=0) sum += *op3 * cCa[2];
            if (cCa[3]!=0) sum += *op4 * cCa[3];
            if (cCa[4]!=0) sum += *op5 * cCa[4];
            if (cCa[5]!=0) sum += *op6 * cCa[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHB]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culb>1) {
            const double * cCb = cCL[culb][m];
            double sum = 0;
            if (cCb[0]!=0) sum  = *op1 * cCb[0];
            if (cCb[1]!=0) sum += *op2 * cCb[1];
            if (cCb[2]!=0) sum += *op3 * cCb[2];
            if (cCb[3]!=0) sum += *op4 * cCb[3];
            if (cCb[4]!=0) sum += *op5 * cCb[4];
            if (cCb[5]!=0) sum += *op6 * cCb[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHC]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culc>1) {
            const double * cCc = cCL[culc][m];
            double sum = 0;
            if (cCc[0]!=0) sum  = *op1 * cCc[0];
            if (cCc[1]!=0) sum += *op2 * cCc[1];
            if (cCc[2]!=0) sum += *op3 * cCc[2];
            if (cCc[3]!=0) sum += *op4 * cCc[3];
            if (cCc[4]!=0) sum += *op5 * cCc[4];
            if (cCc[5]!=0) sum += *op6 * cCc[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHD]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culd>1) {
            const double * cCd = cCL[culd][m];
            double sum = 0;
            if (cCd[0]!=0) sum  = *op1 * cCd[0];
            if (cCd[1]!=0) sum += *op2 * cCd[1];
            if (cCd[2]!=0) sum += *op3 * cCd[2];
            if (cCd[3]!=0) sum += *op4 * cCd[3];
            if (cCd[4]!=0) sum += *op5 * cCd[4];
            if (cCd[5]!=0) sum += *op6 * cCd[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[REORDER]; ++s2,++p) {
        double       * dest = ERIN   + (k*cuJ4+j)*cuM4     + NTX*s2->dest + i;
        const double * op1  = buffer + (k*cuJ4+j)*cuMaxMem + NTX*s2->op1 + i;

        *dest = *op1;
    }

}

//this one is identical to ABCD;
//there shouculd be a way of guarantee the order, so that AC = 0, CDy = 0, ABx=CDx
__global__ void KernelABAB (const double  * mem, const double * vars, double * ERIN, double * buffer,
                            const Op_MIRROR * d_eseq, const uint32_t * d_ninstr,
                            uint8_t cula, uint8_t culb, uint8_t culc, uint8_t culd,
                            uint32_t cuKernels, uint32_t cuMaxMem, uint32_t cuM4, uint32_t cuJ4,
                            int Ntiles) {

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    if (k>=Ntiles) return;


    double ABz = vars[k*6*NTX +       i];
    double CDy = vars[k*6*NTX +   NTX+i];
    double CDz = vars[k*6*NTX + 2*NTX+i];
    double ACx = vars[k*6*NTX + 3*NTX+i];
    double ACy = vars[k*6*NTX + 4*NTX+i];
    double ACz = vars[k*6*NTX + 5*NTX+i];


    const Op_MIRROR * s2 = d_eseq;
    double * bufferN = buffer + (k*cuJ4+j)*cuMaxMem +  i;

    //copy kernels to new memory
    for (int p=0; p<d_ninstr[KERNELS]; ++s2,++p) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        buffer[(k*cuJ4+j)*cuMaxMem + p*NTX + i] = mem[(k*cuJ4+j)*cuKernels + p*NTX + i];
    }

    for (int p=0; p<d_ninstr[MMDZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = ACz * *op3 - ABz * *op1 + CDz * *op2 - z* *op4;
    }

    for (int p=0; p<d_ninstr[CTEBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = s2->aux;
        double z = double(s2->aux);

        *dest = *op1 * z + ABz * *op2 + * op3;
    }

    for (int p=0; p<d_ninstr[CTEKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t zzz = -s2->aux;
        double z = -double(s2->aux);

        *dest = *op1 * z + CDz * *op2 - *op3;
    }

    for (int p=0; p<d_ninstr[MMDY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = ACy * *op3 + CDy * *op2 - y * *op4;
    }

    for (int p=0; p<d_ninstr[MMDX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        //uint8_t xxx = s2->aux;
        double x = double(s2->aux);

        *dest = ACx * *op3 - x* *op4;
    }

    for (int p=0; p<d_ninstr[CTEKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = -s2->aux;
        double y = -double(s2->aux);

        *dest = *op1 * y + CDy * *op2 - * op3;
    }

    for (int p=0; p<d_ninstr[CTEKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = -s2->aux;
        double x = -double(s2->aux);

        *dest = *op1 * x - * op3;
    }

    for (int p=0; p<d_ninstr[CTEBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t yyy = s2->aux;
        double y = double(s2->aux);

        *dest = *op1 * y + * op3;
    }

    for (int p=0; p<d_ninstr[CTEBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op3  = bufferN + NTX*s2->op3;
        //uint8_t xxx = s2->aux;
        double x = double(s2->aux);

        *dest = *op1 * x + * op3;
    }

    for (int p=0; p<d_ninstr[HRRBZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - ABz * *op2;
    }

    for (int p=0; p<d_ninstr[HRRBY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRBX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHA]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (cula>1) {
            const double * cCa = cCL[cula][m];
            double sum = 0;
            if (cCa[0]!=0) sum  = *op1 * cCa[0];
            if (cCa[1]!=0) sum += *op2 * cCa[1];
            if (cCa[2]!=0) sum += *op3 * cCa[2];
            if (cCa[3]!=0) sum += *op4 * cCa[3];
            if (cCa[4]!=0) sum += *op5 * cCa[4];
            if (cCa[5]!=0) sum += *op6 * cCa[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHB]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culb>1) {
            const double * cCb = cCL[culb][m];
            double sum = 0;
            if (cCb[0]!=0) sum  = *op1 * cCb[0];
            if (cCb[1]!=0) sum += *op2 * cCb[1];
            if (cCb[2]!=0) sum += *op3 * cCb[2];
            if (cCb[3]!=0) sum += *op4 * cCb[3];
            if (cCb[4]!=0) sum += *op5 * cCb[4];
            if (cCb[5]!=0) sum += *op6 * cCb[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[HRRKZ]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - CDz * *op2;
    }

    for (int p=0; p<d_ninstr[HRRKY]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;

        *dest = *op1 - CDy * *op2;
    }

    for (int p=0; p<d_ninstr[HRRKX]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;

        *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHC]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culc>1) {
            const double * cCc = cCL[culc][m];
            double sum = 0;
            if (cCc[0]!=0) sum  = *op1 * cCc[0];
            if (cCc[1]!=0) sum += *op2 * cCc[1];
            if (cCc[2]!=0) sum += *op3 * cCc[2];
            if (cCc[3]!=0) sum += *op4 * cCc[3];
            if (cCc[4]!=0) sum += *op5 * cCc[4];
            if (cCc[5]!=0) sum += *op6 * cCc[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[SPHD]; ++s2,++p) {
        double       * dest = bufferN + NTX*s2->dest;
        const double * op1  = bufferN + NTX*s2->op1;
        const double * op2  = bufferN + NTX*s2->op2;
        const double * op3  = bufferN + NTX*s2->op3;
        const double * op4  = bufferN + NTX*s2->op4;
        const double * op5  = bufferN + NTX*s2->op5;
        const double * op6  = bufferN + NTX*s2->op6;
        uint16_t m    = s2->aux;

        if (culd>1) {
            const double * cCd = cCL[culd][m];
            double sum = 0;
            if (cCd[0]!=0) sum  = *op1 * cCd[0];
            if (cCd[1]!=0) sum += *op2 * cCd[1];
            if (cCd[2]!=0) sum += *op3 * cCd[2];
            if (cCd[3]!=0) sum += *op4 * cCd[3];
            if (cCd[4]!=0) sum += *op5 * cCd[4];
            if (cCd[5]!=0) sum += *op6 * cCd[5];
            *dest = sum;
        }
        else
            *dest = *op1;
    }

    for (int p=0; p<d_ninstr[REORDER]; ++s2,++p) {
        double       * dest = ERIN   + (k*cuJ4+j)*cuM4     + NTX*s2->dest + i;
        const double * op1  = buffer + (k*cuJ4+j)*cuMaxMem + NTX*s2->op1 + i;

        *dest = *op1;
    }

}


void p_Qalgorithm::MIRROR (double * d_uvmst,  double * d_vars, double * d_ERI, uint32_t Ntiles, int J4, cudaStream_t * stream) const {

    uint8_t la = eritype.La;
    uint8_t lb = eritype.Lb;
    uint8_t lc = eritype.Lc;
    uint8_t ld = eritype.Ld;

    uint16_t NM4 = uint16_t(2*la+1) * (2*lb+1) * (2*lc+1) * (2*ld+1);

    cudaEvent_t start, stop;

    if (BenchmarkAlgorithms) {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    }

    if (IC->GPUMirrorTransformation != NULL) {

        const int NNY = 4;
        const int yNtiles = (Ntiles+NNY-1)/NNY;

        //CALL THE KERNEL
        dim3 Nbl ( J4, yNtiles);
        dim3 Nth (NTX,     NNY);

        if (BenchmarkAlgorithms) cudaEventRecord(start, *stream);
        IC->GPUMirrorTransformation <<<Nbl,Nth,0,*stream>>> (d_uvmst, d_vars, d_ERI, Ntiles);
        if (BenchmarkAlgorithms) cudaEventRecord(stop, *stream);
    }
    else {

        const int yNtiles = NTY*((Ntiles+NTY-1)/NTY);

        uint32_t iKernels, iMaxMem, iM4, iJ4;
        iKernels = IC->nKernels * NTX;
        iMaxMem  = MaxMem * NTX;
        iM4      = NM4 * NTX;
        iJ4      = J4;

        // all the following should be part of the Qalgorithm class;
        // the Mallocs should be done in advance for all algorithms
        // based on the worst case; IC->eseq and IC->ninstr should
        // be declared on pinned memory, so the Memcpy's can be
        // pèrformed asynchronously

        //copy interpreted code (instructions)
        Op_MIRROR * d_eseq;   // instructions (interpreted)
        uint32_t  * d_ninstr; // number of instructions per block
        double    * d_buffer; // data buffer
        cudaMalloc((void **) &d_eseq, IC->Ntotal*sizeof(Op_MIRROR)); //copy instructions
        cudaMalloc((void **) &d_ninstr,       32*sizeof(uint32_t));  //number of instructions per phase
        cudaMalloc((void **) &d_buffer, J4*IC->MaxMem*yNtiles*sizeof(cachelineN<NTX>));

        // must make eseq  pinned to use asynchronous copy;
        // it might also be possible to keep a copy of every function in device
        cudaMemcpy (d_eseq ,  IC->eseq ,   IC->Ntotal*sizeof(Op_MIRROR) , cudaMemcpyHostToDevice);
        cudaMemcpy (d_ninstr, IC->ninstr,          32*sizeof(uint32_t)  , cudaMemcpyHostToDevice);
        //cudaMemset (d_buffer, 0,   J4*IC->MaxMem * yNtiles*sizeof(cachelineN<NTX>)); //zero the buffers

        //cudaMemcpyAsync (d_eseq ,  IC->eseq ,   IC->Ntotal*sizeof(Op_MIRROR) , cudaMemcpyHostToDevice, *stream);
        //cudaMemcpyAsync (d_ninstr, IC->ninstr,          32*sizeof(uint32_t)  , cudaMemcpyHostToDevice, *stream);
        cudaMemsetAsync (d_buffer, 0,   J4*IC->MaxMem * yNtiles*sizeof(cachelineN<NTX>), *stream);

        {
            dim3 Nbl ( J4, yNtiles/NTY);
            dim3 Nth (NTX,         NTY);

            if (BenchmarkAlgorithms) cudaEventRecord(start, *stream);

            if      (eritype.geometry==ABCD) KernelABCD <<<Nbl,Nth,0,*stream>>> (d_uvmst, d_vars, d_ERI, d_buffer,  d_eseq, d_ninstr,   la,lb,lc,ld,  iKernels,iMaxMem,iM4,iJ4,  Ntiles);
            else if (eritype.geometry==AACD) KernelAACD <<<Nbl,Nth,0,*stream>>> (d_uvmst, d_vars, d_ERI, d_buffer,  d_eseq, d_ninstr,   la,lb,lc,ld,  iKernels,iMaxMem,iM4,iJ4,  Ntiles);
            else if (eritype.geometry==AACC) KernelAACC <<<Nbl,Nth,0,*stream>>> (d_uvmst, d_vars, d_ERI, d_buffer,  d_eseq, d_ninstr,   la,lb,lc,ld,  iKernels,iMaxMem,iM4,iJ4,  Ntiles);
            else if (eritype.geometry==ABAB) KernelABAB <<<Nbl,Nth,0,*stream>>> (d_uvmst, d_vars, d_ERI, d_buffer,  d_eseq, d_ninstr,   la,lb,lc,ld,  iKernels,iMaxMem,iM4,iJ4,  Ntiles);
            else                             KernelABCD <<<Nbl,Nth,0,*stream>>> (d_uvmst, d_vars, d_ERI, d_buffer,  d_eseq, d_ninstr,   la,lb,lc,ld,  iKernels,iMaxMem,iM4,iJ4,  Ntiles);

            if (BenchmarkAlgorithms) cudaEventRecord(stop, *stream);
        }

        //free the memory
        //===============
        cudaFree(d_buffer);
        cudaFree(d_eseq);
        cudaFree(d_ninstr);
    }

    cudaError_t cudaResult = cudaGetLastError();
    if (cudaResult != cudaSuccess) {
        cout << endl<< "Some error ocurred in M gpu:  " << eritype.geometry << "    " << int(la) << " " << int(lb) << " " << int(lc) << " " << int(ld) << endl << cudaGetErrorString(cudaResult) << endl;
    }

    if (BenchmarkAlgorithms) {
        // move this somewhere else; otherwise the CPU busy-waits for nothing
        cudaEventSynchronize(stop);
        float ms = 0;
        cudaEventElapsedTime(&ms, start, stop);

        K4benchmark * bench = p_Q.Benchmarks[eritype];
        bench->AddCallM (Ntiles*J4);

        #pragma omp atomic
        bench->deltaMIRROR += 0.001*ms;
    }
}



