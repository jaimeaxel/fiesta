/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    This is an implementation of the K4+MIRROR algorithm to compute 2-electron integrals.
    If you use it in your research, please cite the original  papers:

    "An algorithm for the efficient evaluation of two-electron repulsion integrals over contracted Gaussian-type basis functions",
    Jaime Axel Rosal Sandberg, Zilvinas Rinkevicius, J. Chem. Phys. 137, 234105 (2012); http://dx.doi.org/10.1063/1.4769730

    "New recurrence relations for analytic evaluation of two-electron repulsion integrals over highly contracted gaussian-type orbitals",
    Jaime Axel Rosal Sandberg, Zilvinas Rinkevicius, In preparation
*/

#include "IIC.hpp"

#include <string.h>

#include "2eints/quimera.hpp"
#include "libquimera/ERIgeom.hpp"
#include "libquimera/libquimera.hpp"
using namespace LibQuimera;

static const uint8_t PF = 8; //prefetch distance

// VECTOR FLOAT ROUTINES
// *********************

void ERIroutine::TransformABCD(const cacheline32 * __restrict__  mem, const ERIgeometries32 & vars16, cacheline32 * __restrict__ ERI16, p_ERIbuffer & buffer) const {
    const cacheline32 & ABz = vars16.ABz;
    const cacheline32 & CDy = vars16.CDy;
    const cacheline32 & CDz = vars16.CDz;

    const cacheline32 & ACx = vars16.ACx;
    const cacheline32 & ACy = vars16.ACy;
    const cacheline32 & ACz = vars16.ACz;


    register Op_MIRROR * s2 = eseq;

    cacheline32 * buffer32 = (cacheline32*)buffer.bufferMIRROR;


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(buffer32 + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        buffer32[i] = mem[i];
    }

    for (; s2<nseq[MMDZ]; ++s2) {

        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t zzz = s2->aux;

        //for (int i=0; i<8; ++i)
        //dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACz * load(op3) - ABz * load(op1) + CDz * load(op2);

        if (zzz>0) {
            float z = zzz;
            tmp -= load(op4) * z;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */
        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + ABz * load(op2) + load(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + CDz * load(op2) - load(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t yyy = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACy * load(op3) + CDy * load(op2);

        if (yyy>0) {
            float y = yyy;
            tmp -= load(op4) * y;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[MMDX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t xxx = s2->aux;
        //float x = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACx[i] * op3[i] - x * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACx * load(op3);

        if (xxx>0) {
            float x = xxx;
            tmp -= load(op4) * x;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y + CDy * load(op2) - load(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x - load(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y + load(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x + load(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - ABz * load(op2));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (la>1) {
            cacheline32 sum;
            if (fCa[m][0]!=0) sum  = buffer32[s2->op1] * fCa[m][0];
            if (fCa[m][1]!=0) sum += buffer32[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += buffer32[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += buffer32[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += buffer32[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += buffer32[s2->op6] * fCa[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[SPHB]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (lb>1) {
            cacheline32 sum;
            if (fCb[m][0]!=0) sum  = buffer32[s2->op1] * fCb[m][0];
            if (fCb[m][1]!=0) sum += buffer32[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += buffer32[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += buffer32[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += buffer32[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += buffer32[s2->op6] * fCb[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - CDz * load(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - CDy * load(op2));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (lc>1) {
            cacheline32 sum;
            if (fCc[m][0]!=0) sum  = buffer32[s2->op1] * fCc[m][0];
            if (fCc[m][1]!=0) sum += buffer32[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += buffer32[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += buffer32[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += buffer32[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += buffer32[s2->op6] * fCc[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[SPHD]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (ld>1) {
            cacheline32 sum;
            if (fCd[m][0]!=0) sum  = buffer32[s2->op1] * fCd[m][0];
            if (fCd[m][1]!=0) sum += buffer32[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += buffer32[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += buffer32[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += buffer32[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += buffer32[s2->op6] * fCd[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[REORDER]; ++s2) {
        float * dest = (float*)(ERI16 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(ERI16 + s2[PF].dest, 1, 1);

        ERI16[s2->dest] = buffer32[s2->op1];
    }
}

void ERIroutine::TransformAACD(const cacheline32 * __restrict__  mem, const ERIgeometries32 & vars16, cacheline32 * __restrict__ ERI16, p_ERIbuffer & buffer) const {

    const cacheline32 & CDz = vars16.CDz;

    const cacheline32 & ACy = vars16.ACy;
    const cacheline32 & ACz = vars16.ACz;


    register Op_MIRROR * s2 = eseq;

    cacheline32 * buffer32 = (cacheline32*)buffer.bufferMIRROR;

    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(buffer32 + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        buffer32[i] = mem[i];
    }


    for (; s2<nseq[MMDZ]; ++s2) {

        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t zzz = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACz * load(op3) + CDz * load(op2);

        if (zzz>0) {
            float z = zzz;
            tmp -= load(op4) * z;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + load(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + CDz * load(op2) - load(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t yyy = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACy * load(op3);

        if (yyy>0) {
            float y = yyy;
            tmp -= load(op4) * y;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[MMDX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t xxx = s2->aux;
        //float x = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACx[i] * op3[i] - x * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp;
        tmp.set(0.f);

        if (xxx>0) {
            float x = xxx;
            tmp -= load(op4) * x;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y - load(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x - load(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y + load(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x + load(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (la>1) {
            sum *= fCa[m][0];

            if (fCa[m][1]!=0) sum += buffer32[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += buffer32[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += buffer32[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += buffer32[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += buffer32[s2->op6] * fCa[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[SPHB]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (lb>1) {
            sum *= fCb[m][0];

            if (fCb[m][1]!=0) sum += buffer32[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += buffer32[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += buffer32[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += buffer32[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += buffer32[s2->op6] * fCb[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - CDz * load(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (lc>1) {
            sum *= fCc[m][0];

            if (fCc[m][1]!=0) sum += buffer32[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += buffer32[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += buffer32[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += buffer32[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += buffer32[s2->op6] * fCc[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[SPHD]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (ld>1) {
            sum *= fCd[m][0];

            if (fCd[m][1]!=0) sum += buffer32[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += buffer32[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += buffer32[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += buffer32[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += buffer32[s2->op6] * fCd[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[REORDER]; ++s2) {
        float * dest = (float*)(ERI16 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(ERI16 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

}

void ERIroutine::TransformAACC(const cacheline32 * __restrict__  mem, const ERIgeometries32 & vars16, cacheline32 * __restrict__ ERI16, p_ERIbuffer & buffer) const {


    register Op_MIRROR * s2 = eseq;

    cacheline32 * buffer32 = (cacheline32*)buffer.bufferMIRROR;

    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(buffer32 + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        buffer32[i] = mem[i];
    }


    for (; s2<nseq[MMDZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t zzz = s2->aux;
        //float x = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp;
        tmp = buffer32[s2->op3] * vars16.ACz;

        if (zzz>0) {
            float z = zzz;
            tmp -= load(op4) * z;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + load(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z  - load(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t yyy = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp;
        tmp.set(0.f);

        if (yyy>0) {
            float y = yyy;
            tmp -= load(op4) * y;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[MMDX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t xxx = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp;
        tmp.set(0.f);

        if (xxx>0) {
            float x = xxx;
            tmp -= load(op4) * x;
        }

        store(dest, tmp);
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y - load(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x - load(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y + load(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x + load(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (la>1) {
            sum *= fCa[m][0];

            if (fCa[m][1]!=0) sum += buffer32[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += buffer32[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += buffer32[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += buffer32[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += buffer32[s2->op6] * fCa[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[SPHB]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (lb>1) {
            sum *= fCb[m][0];

            if (fCb[m][1]!=0) sum += buffer32[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += buffer32[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += buffer32[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += buffer32[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += buffer32[s2->op6] * fCb[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (lc>1) {
            sum *= fCc[m][0];

            if (fCc[m][1]!=0) sum += buffer32[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += buffer32[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += buffer32[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += buffer32[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += buffer32[s2->op6] * fCc[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[SPHD]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 sum = buffer32[s2->op1];

        if (ld>1) {
            sum *= fCd[m][0];

            if (fCd[m][1]!=0) sum += buffer32[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += buffer32[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += buffer32[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += buffer32[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += buffer32[s2->op6] * fCd[m][5];
        }

        buffer32[s2->dest] = sum;
    }

    for (; s2<nseq[REORDER]; ++s2) {
        float * dest = (float*)(ERI16 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(ERI16 + s2[PF].dest, 1, 1);

        ERI16[s2->dest] = buffer32[s2->op1];
    }

}

void ERIroutine::TransformABAB(const cacheline32 * __restrict__  mem, const ERIgeometries32 & vars16, cacheline32 * __restrict__ ERI16, p_ERIbuffer & buffer) const {
    const cacheline32 & ABz = vars16.ABz;
    const cacheline32 & CDy = vars16.CDy;
    const cacheline32 & CDz = vars16.CDz;

    const cacheline32 & ACx = vars16.ACx;
    const cacheline32 & ACy = vars16.ACy;
    const cacheline32 & ACz = vars16.ACz;


    register Op_MIRROR * s2 = eseq;

    cacheline32 * buffer32 = (cacheline32*)buffer.bufferMIRROR;


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        uint32_t dest = s2->dest;
        uint32_t op1  = s2->op1;

        //__builtin_prefetch(buffer32 + s2[PF].op1);
        //__builtin_prefetch(mem            + s2[PF].dest, 1, 1);

        buffer32[i] = mem[i];
    }


    for (; s2<nseq[MMDZ]; ++s2) {

        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t zzz = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  ACz[i] * op3[i] - ABz[i] * op1[i] + CDz[i] * op2[i] - z * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACz * load(op3) - ABz * load(op1) + CDz * load(op2);

        if (zzz>0) {
            float z = zzz;
            tmp -= load(op4) * z;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = z * op1[i] + ABz[i] * op2[i] + op3[i];
        */
        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + ABz * load(op2) + load(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -z * op1[i] + CDz[i] * op2[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * z + CDz * load(op2) - load(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t yyy = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACy[i] * op3[i] + CDy[i] * op2[i] - y * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACy * load(op3) + CDy * load(op2);

        if (yyy>0) {
            float y = yyy;
            tmp -= load(op4) * y;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[MMDX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        uint8_t xxx = s2->aux;
        //float x = s2->aux;

        /*
        for (int i=0; i<8; ++i)
        dest[i] = ACx[i] * op3[i] - x * op4[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op4);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        cacheline32 tmp = ACx * load(op3);

        if (xxx>0) {
            float x = xxx;
            tmp -= load(op4) * x;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = CDy[i] * op2[i] - y * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y + CDy * load(op2) - load(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = -float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = -x * op1[i] - op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x - load(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] =  y * op1[i] + op3[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * y + load(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = float(s2->aux);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = x * op1[i] + op3[i];
        */
        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op3);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) * x + load(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - ABz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - ABz * load(op2));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (la>1) {
            cacheline32 sum;
            if (fCa[m][0]!=0) sum  = buffer32[s2->op1] * fCa[m][0];
            if (fCa[m][1]!=0) sum += buffer32[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += buffer32[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += buffer32[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += buffer32[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += buffer32[s2->op6] * fCa[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[SPHB]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (lb>1) {
            cacheline32 sum;
            if (fCb[m][0]!=0) sum  = buffer32[s2->op1] * fCb[m][0];
            if (fCb[m][1]!=0) sum += buffer32[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += buffer32[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += buffer32[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += buffer32[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += buffer32[s2->op6] * fCb[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDz[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - CDz * load(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i] - CDy[i] * op2[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].op2);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1) - CDy * load(op2));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        /*
        for (int i=0; i<8; ++i)
        dest[i] = op1[i];
        */

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (lc>1) {
            cacheline32 sum;
            if (fCc[m][0]!=0) sum  = buffer32[s2->op1] * fCc[m][0];
            if (fCc[m][1]!=0) sum += buffer32[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += buffer32[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += buffer32[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += buffer32[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += buffer32[s2->op6] * fCc[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[SPHD]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        uint16_t m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (ld>1) {
            cacheline32 sum;
            if (fCd[m][0]!=0) sum  = buffer32[s2->op1] * fCd[m][0];
            if (fCd[m][1]!=0) sum += buffer32[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += buffer32[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += buffer32[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += buffer32[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += buffer32[s2->op6] * fCd[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[REORDER]; ++s2) {
        float * dest = (float*)(ERI16 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(ERI16 + s2[PF].dest, 1, 1);

        ERI16[s2->dest] = buffer32[s2->op1];
    }
}



