
#include "../fiesta/defs.hpp"
#include "../basis/SPprototype.hpp"
#include "../basis/shellpair.hpp"
#include "../2eints/IICint.hpp"
#include "../2eints/quimera.hpp"
using namespace std;

using namespace LibQuimera;


//NTiles32 * TheBatch.msize4 * sizeof(cacheline32);
//NTiles32 * sizeof(ERIgeometries32);
//NTiles32 * TheBatch.wsize4 * sizeof(cacheline32);


const int PF = 8;

__global__ void cudaMMDZtst(float * buffer, float * ERIgeom, Op_MIRROR * s2, int nops) {


    float ABz = ERIgeom[               threadIdx.x];
    float CDz = ERIgeom[2*blockDim.x + threadIdx.x];
    float ACz = ERIgeom[5*blockDim.x + threadIdx.x];

    for (int i=0; i<nops; ++i) {
        unsigned short int dest = s2[i].dest * blockDim.x + threadIdx.x;
        unsigned short int op1  = s2[i].op1  * blockDim.x + threadIdx.x;
        unsigned short int op2  = s2[i].op2  * blockDim.x + threadIdx.x;
        unsigned short int op3  = s2[i].op3  * blockDim.x + threadIdx.x;
        unsigned short int op4  = s2[i].op4  * blockDim.x + threadIdx.x;
        unsigned short int zzz  = s2[i].aux;
        float z = zzz;

        buffer[dest] = ACz * buffer[op3] - ABz * buffer[op1] + CDz * buffer[op2] - buffer[op4] * z;
    }

}

void cudaMMDZ(float * buffer, int maxmem, float * ERIgeom, Op_MIRROR * s2, int nop) {

    size_t size;

    float * d_ERIgeo;
    size = 6*64; //sizeof(ERIgeometries32);
    cudaMalloc(&d_ERIgeo, size);
    cudaMemcpy(d_ERIgeo, ERIgeom, size, cudaMemcpyHostToDevice);


    Op_MIRROR * d_op;
    size = nop * sizeof(Op_MIRROR);
    cudaMalloc(&d_op, size);
    cudaMemcpy(d_op, s2, size, cudaMemcpyHostToDevice);


    float * d_buffer;
    size = maxmem * 64; //sizeof(cacheline32);
    cudaMalloc(&d_buffer, size);
    cudaMemcpy(d_buffer, buffer, size, cudaMemcpyHostToDevice);


    cudaMMDZtst <<<1,16>>> (d_buffer, d_ERIgeo, d_op, nop);


    cudaMemcpy((float*)buffer, d_buffer, size, cudaMemcpyDeviceToHost);

    // Free device memory
    cudaFree(d_buffer);
    cudaFree(d_ERIgeo);
}




/*


__global__ void cudaMMDZ(float * buffer, float * ERIgeom, Op_MIRROR * s2, int nops) {


    float ABz = ERIgeom[ 6 * blockIdx.x      * blockDim.x + threadIdx.x];
    float CDz = ERIgeom[(6 * blockIdx.x + 2) * blockDim.x + threadIdx.x];
    float ACz = ERIgeom[(6 * blockIdx.x + 5) * blockDim.x + threadIdx.x];


    int geoStr  =      6 * blockIdx.x * blockDim.x + threadIdx.x;
    int buffStr = MaxMem * blockIdx.x * blockDim.x;


    for (int i=0; i<nops; ++i) {
        unsigned short int dest = s2[i].dest * blockIdx.x * blockDim.x + buffStr + threadIdx.x;
        unsigned short int op1  = s2[i].op1  * blockIdx.x * blockDim.x + buffStr + threadIdx.x;
        unsigned short int op2  = s2[i].op2  * blockIdx.x * blockDim.x + buffStr + threadIdx.x;
        unsigned short int op3  = s2[i].op3  * blockIdx.x * blockDim.x + buffStr + threadIdx.x;
        unsigned short int op4  = s2[i].op4  * blockIdx.x * blockDim.x + buffStr + threadIdx.x;
        unsigned short int zzz  = s2[i].aux;
        float z = zzz;

        buffer[dest] = ACz * buffer[op3] - ABz * buffer[op1] + CDz * buffer[op2] - buffer[op4] * z;
    }

}


void ERIroutine::TransformCUDA(const cacheline32 * __restrict__  kernel, const ERIgeometries32 * ERIgeo, cacheline32 * __restrict__ ERIs, int NTILES) const {

    size_t size;

    float * d_ERIgeo;
    size = NTILES * sizeof(ERIgeometries32);
    cudaMalloc(&d_ERIgeo, size);
    cudaMemcpy(d_ERIgeo, (float*)ERIgeo, size, cudaMemcpyHostToDevice);


    float * d_kernel;
    size = NTILES * nKernels * sizeof(ERIgeometries32);
    cudaMalloc(&d_kernel, size);
    cudaMemcpy(d_kernel, (float*)kernel, size, cudaMemcpyHostToDevice);


    float * d_ERIs;
    size = NTILES * (2*la+1)*(2*lb+1)*(2*lc+1)*(2*ld+1) * sizeof(ERIgeometries32);
    cudaMalloc(&d_ERIs, size);


    float * d_buffer;
    size = NTILES * MaxMem * sizeof(ERIgeometries32);
    cudaMalloc(&d_buffer, size);

    //also the instructions!!!



/*
    const cacheline32 & ABz = vars16.ABz;
    const cacheline32 & CDy = vars16.CDy;
    const cacheline32 & CDz = vars16.CDz;

    const cacheline32 & ACx = vars16.ACx;
    const cacheline32 & ACy = vars16.ACy;
    const cacheline32 & ACz = vars16.ACz;


    register Op_MIRROR * s2 = eseq;

    cacheline32 * buffer32 = (cacheline32*)buffer.bufferMIRROR;


    //copy kernels to new memory
    for (int i=0; s2<nseq[KERNELS]; ++s2,++i) {
        UI32 dest = s2->dest;
        UI32 op1  = s2->op1;

        buffer32[i] = mem[i];
    }
*/

  //  cudaMMDZ<<<16,16>>>(d_buffer, d_ERIgeo, nseq[MMDZ-1], ninstr[MMDZ-1]);


/*
    for (; s2<nseq[MMDZ]; ++s2) {

        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        unsigned short int zzz = s2->aux;

        cacheline32 tmp = ACz * load(op3) - ABz * load(op1) + CDz * load(op2);

        if (zzz>0) {
            float z = zzz;
            tmp -= load(op4) * z;
        }

        buffer32[s2->dest] = tmp;
    }
*/

/*
    for (; s2<nseq[CTEBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = float(s2->aux);

        store(dest, load(op1) * z + ABz * load(op2) + load(op3));
    }

    for (; s2<nseq[CTEKZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float z = -float(s2->aux);

        store(dest, load(op1) * z + CDz * load(op2) - load(op3));
    }

    for (; s2<nseq[MMDY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        unsigned short int yyy = s2->aux;

        cacheline32 tmp = ACy * load(op3) + CDy * load(op2);

        if (yyy>0) {
            float y = yyy;
            tmp -= load(op4) * y;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[MMDX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        unsigned short int xxx = s2->aux;
        //float x = s2->aux;

        cacheline32 tmp = ACx * load(op3);

        if (xxx>0) {
            float x = xxx;
            tmp -= load(op4) * x;
        }

        buffer32[s2->dest] = tmp;
    }

    for (; s2<nseq[CTEKY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = -float(s2->aux);

        store(dest, load(op1) * y + CDy * load(op2) - load(op3));
    }

    for (; s2<nseq[CTEKX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = -float(s2->aux);

        store(dest, load(op1) * x - load(op3));
    }

    for (; s2<nseq[CTEBY]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float y = float(s2->aux);

        store(dest, load(op1) * y + load(op3));
    }

    for (; s2<nseq[CTEBX]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op3  = (float*)(buffer32 + s2->op3);
        float x = float(s2->aux);

        store(dest, load(op1) * x + load(op3));
    }

    for (; s2<nseq[HRRBZ]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);

        store(dest, load(op1) - ABz * load(op2));
    }

    for (; s2<nseq[HRRBY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        store(dest, load(op1));
    }

    for (; s2<nseq[HRRBX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);

        store(dest, load(op1));
    }

    for (; s2<nseq[SPHA]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        UI16 m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (la>1) {
            cacheline32 sum;
            if (fCa[m][0]!=0) sum  = buffer32[s2->op1] * fCa[m][0];
            if (fCa[m][1]!=0) sum += buffer32[s2->op2] * fCa[m][1];
            if (fCa[m][2]!=0) sum += buffer32[s2->op3] * fCa[m][2];
            if (fCa[m][3]!=0) sum += buffer32[s2->op4] * fCa[m][3];
            if (fCa[m][4]!=0) sum += buffer32[s2->op5] * fCa[m][4];
            if (fCa[m][5]!=0) sum += buffer32[s2->op6] * fCa[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[SPHB]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        UI16 m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (lb>1) {
            cacheline32 sum;
            if (fCb[m][0]!=0) sum  = buffer32[s2->op1] * fCb[m][0];
            if (fCb[m][1]!=0) sum += buffer32[s2->op2] * fCb[m][1];
            if (fCb[m][2]!=0) sum += buffer32[s2->op3] * fCb[m][2];
            if (fCb[m][3]!=0) sum += buffer32[s2->op4] * fCb[m][3];
            if (fCb[m][4]!=0) sum += buffer32[s2->op5] * fCb[m][4];
            if (fCb[m][5]!=0) sum += buffer32[s2->op6] * fCb[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[HRRKZ]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        store(dest, load(op1) - CDz * load(op2));
    }

    for (; s2<nseq[HRRKY]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        float * dest = (float*)(buffer32 + s2->dest);

        store(dest, load(op1) - CDy * load(op2));
    }

    for (; s2<nseq[HRRKX]; ++s2) {
        const float * op1  = (float*)(buffer32 + s2->op1);
        float * dest = (float*)(buffer32 + s2->dest);


        store(dest, load(op1));
    }

    for (; s2<nseq[SPHC]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        UI16 m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (lc>1) {
            cacheline32 sum;
            if (fCc[m][0]!=0) sum  = buffer32[s2->op1] * fCc[m][0];
            if (fCc[m][1]!=0) sum += buffer32[s2->op2] * fCc[m][1];
            if (fCc[m][2]!=0) sum += buffer32[s2->op3] * fCc[m][2];
            if (fCc[m][3]!=0) sum += buffer32[s2->op4] * fCc[m][3];
            if (fCc[m][4]!=0) sum += buffer32[s2->op5] * fCc[m][4];
            if (fCc[m][5]!=0) sum += buffer32[s2->op6] * fCc[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[SPHD]; ++s2) {
        float * dest = (float*)(buffer32 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);
        const float * op2  = (float*)(buffer32 + s2->op2);
        const float * op3  = (float*)(buffer32 + s2->op3);
        const float * op4  = (float*)(buffer32 + s2->op4);
        const float * op5  = (float*)(buffer32 + s2->op5);
        const float * op6  = (float*)(buffer32 + s2->op6);
        UI16 m    = s2->aux;

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(buffer32 + s2[PF].dest, 1, 1);

        if (ld>1) {
            cacheline32 sum;
            if (fCd[m][0]!=0) sum  = buffer32[s2->op1] * fCd[m][0];
            if (fCd[m][1]!=0) sum += buffer32[s2->op2] * fCd[m][1];
            if (fCd[m][2]!=0) sum += buffer32[s2->op3] * fCd[m][2];
            if (fCd[m][3]!=0) sum += buffer32[s2->op4] * fCd[m][3];
            if (fCd[m][4]!=0) sum += buffer32[s2->op5] * fCd[m][4];
            if (fCd[m][5]!=0) sum += buffer32[s2->op6] * fCd[m][5];
            buffer32[s2->dest] = sum;
        }
        else
            buffer32[s2->dest] = buffer32[s2->op1];
    }

    for (; s2<nseq[REORDER]; ++s2) {
        float * dest = (float*)(ERI16 + s2->dest);
        const float * op1  = (float*)(buffer32 + s2->op1);

        __builtin_prefetch(buffer32 + s2[PF].op1);
        __builtin_prefetch(ERI16 + s2[PF].dest, 1, 1);

        ERI16[s2->dest] = buffer32[s2->op1];
    }
    */

    /*

    //copy the ERIs back
    size = NTILES * (2*la+1)*(2*lb+1)*(2*lc+1)*(2*ld+1) * sizeof(ERIgeometries32);
    cudaMemcpy(ERIs, d_ERIs, size, cudaMemcpyDeviceToHost);

    // Free device memory
    cudaFree(d_buffer);
    cudaFree(d_ERIgeo);
    cudaFree(d_ERIs);
    cudaFree(d_kernel);
}

*/
