/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __IIC_INIT__
#define __IIC_INIT__

#include <string>

#include "defs.hpp"
#include "VILIC.hpp"


class cacheline64;
class cacheline32;
template <int N> class cachelineN;

class ERIgeometries64;
class ERIgeometries32;
template <int N> class ERIgeometriesN;

class ShellPairPrototype;
class ShellPair;
class p_ERIbuffer;
class PrimitiveSet;

namespace LibQuimera {
    struct ERITile64;
    struct ERITile32;
    template <int N> struct ERITile;
}



enum RRTYPE  {KERNELS, MMDZ, CTEBZ, CTEKZ, MMDY, MMDX, CTEKY, CTEKX, CTEBY, CTEBX, HRRBZ, HRRBY, HRRBX, SPHA, SPHB, HRRKZ, HRRKY, HRRKX, SPHC, SPHD, REORDER, OTHER};

enum RRTYPE2 {BOYS,
                ADRR4, AERR4, CDR4, K4D, K4E, K4F,
                ADRR3, AERR3, CDR3, K3D, K3E, K3F,
                ADRR2, AERR2, CDR2, K2D, K2E, K2F,
                ADRR1, AERR1, CDR1, K1D, K1E, K1F,
                ADRR0, AERR0, CDR0,
                NADA};  // 29 < 32




//this is HUGE!!!
class Op_K4 {
  public:
    uint16_t dest;
    uint16_t op1;
    uint16_t op2;
    uint16_t op3;
    uint16_t op4;
    uint16_t op5;
    uint16_t op6;
    uint16_t ope;
    uint16_t aux;
    uint16_t pad[7];
} __attribute__((aligned(32)));

class Op_MIRROR16 {
  public:
    uint16_t dest;
    uint16_t op1;
    uint16_t op2;
    uint16_t op3;
    uint16_t op4;
    uint16_t op5;
    uint16_t op6;
    uint16_t aux; //also ope
} __attribute__((aligned(16)));

class Op_MIRROR {
  public:
    uint32_t dest;
    uint32_t op1;
    uint32_t op2;
    uint32_t op3;
    uint32_t op4;
    uint32_t op5;
    uint32_t op6;
    uint32_t aux; //also ope
} __attribute__((aligned(32)));


#ifndef __CUDACC__
class cudaStream_t;
#endif

class ERIroutine {
    friend class p_Qalgorithm;
    friend class p_Quimera;

  private:
    //state
    bool IsSet;
    bool IsInitialized;


    //type of ERI batch
    uint8_t la;
    uint8_t lb;
    uint8_t lc;
    uint8_t ld;
    uint8_t Am;
    GEOM geometry;
    bool useCDR;
    bool useGC;

    //instructions/pointer to function
    uint32_t ninstrK4[32];
    uint32_t ninstr  [32];

    Op_K4 * nseqK4[32];
    Op_K4 * eseqK4;

    Op_MIRROR * nseq[32];
    Op_MIRROR * eseq;

    VILcode K4VILcode;
    VILcode M0VILcode;

    int Ntotal;   //number of instructions
    int NtotalK4;


    //copy the coefficients to a local, more compact representation
    double Ca[11][6];
    double Cb[11][6];
    double Cc[11][6];
    double Cd[11][6];

    float fCa[11][6];
    float fCb[11][6];
    float fCc[11][6];
    float fCd[11][6];

    //useful info
    uint32_t MaxMem;
    uint32_t K4Mem;
    uint32_t nKernels;

    uint8_t Lt;

    int memF0, memF0e, memF0f;
    int memK4J1e, memK4J1f;
    int memK3J2e, memK3J2f;
    int memK2J3e, memK2J3f;
    int memK1J4e, memK1J4f;

    uint16_t maxV, maxU, maxT, maxS;


    //for benchmarking
    //****************
    uint64_t NFLOPS;
    uint64_t NFLOPSK;
    uint64_t NK4;

    //FLOPs in each loop
    int nK4J0,  nK4J1,  nK3J1,  nK3J2,  nK2J2,  nK2J3,  nK1J3,  nK1J4,  nK0J4;
    int nK4J0e, nK4J1e, nK3J1e, nK3J2e, nK2J2e, nK2J3e, nK1J3e, nK1J4e, nK0J4e;
    int nK4J0f, nK4J1f, nK3J1f, nK3J2f, nK2J2f, nK2J3f, nK1J3f, nK1J4f, nK0J4f;


    //routines not accessed outside the class
    std::string IdString(bool fuse=false) const;

    template <bool SAB, bool SCD, bool SR, bool LR> void InnerContractCDR_GC (const ERIgeometries64 & vars8, const LibQuimera::ERITile64 & ET, const ShellPairPrototype & ABp, double ikcd, double rcd, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;
    template <bool SAB, bool SCD, bool SR, bool LR> void InnerContractCDR_GC (cacheline64 * (&F0), const ERIgeometries64 & vars8, const PrimitiveSet & PSab,
                                                            const ShellPairPrototype & ABp, double ikcd, double rcd, p_ERIbuffer & buffer,
                                                            cacheline64 & AB2, cacheline64 & X2, double iw2sr, double iw2lr) const;

    void                               InnerContractCDR_GC (cacheline32 * (&F0), const ERIgeometries32 & vars8, const PrimitiveSet & PSab,
                                                            const ShellPairPrototype & ABp, float ikcd, float rcd, p_ERIbuffer & buffer,
                                                            cacheline32 & AB2, cacheline32 & X2) const;
    template<bool SAB, bool SCD,  int N>
    void                               InnerContractCDR_GC (cachelineN<N> * (&F0), const ERIgeometriesN<N> & vars, const PrimitiveSet & PSab,
                                                            const ShellPairPrototype & ABp, double ikcd, double rcd, p_ERIbuffer & buffer,
                                                            cachelineN<N> & AB2, cachelineN<N> & X2) const;

  public:

    ERIroutine();
   ~ERIroutine();

    //pointers to metaprogrammed routines!
    void (*InnerContractionRoutine)  (cacheline64 * (&F0), const ERIgeometries64 & vars8, const PrimitiveSet & PSab, const ShellPairPrototype & ABp, double ikcd, double rcd, p_ERIbuffer & buffer, cacheline64 & AB2, cacheline64 & X2, double iw2);

    void (*GPUK4ContractionRoutine)  (const double * vars, const double * d_F0,   double * uv_m_st,
                                      uint8_t KA, uint8_t KB, uint8_t KC, uint8_t KD,
                                      uint8_t JA, uint8_t JB, uint8_t JC, uint8_t JD,
                                      uint16_t nJ1, uint16_t nJ2, uint16_t nJ3, uint16_t nJ4,
                                      const double * Ka, const double * Kb, const double * Kc, const double * Kd,
                                      const double * iKa, const double * iKb, const double * iKc, const double * iKd,
                                      const double * iKba, const double * iKdc,
                                      const double * iKs3, const double * iKt3,
                                      const double *  Kba, const double *  Kdc,

                                      const double * Nav, const double * Nbu, const double * Nct, const double * Nds,
                                      const double *  Na, const double *  Nb, const double *  Nc, const double *  Nd,
                                      uint32_t maxTiles, double ilogTh); //this one is a CUDA kernel

    void (*GPUMirrorTransformation)  (const double  * mem, const double * vars, double * ERIN, int maxTiles); //this one is a CUDA kernel


    //loaders for specialized routines (these methods are automatically generated by ICgen)
    void LoadQIC();
    void LoadK2C();
    void LoadK4GPU();
    void LoadMGPU();



    //setter methods
    void Init();

    void Set(uint8_t La, uint8_t Lb, uint8_t Lc, uint8_t Ld, GEOM geom, bool cdr);

    //sets the memory buffers and returns the size used
    size_t SetMemoryBuffers(const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * mem, p_ERIbuffer & buffer) const;


    //ONE-CENTER ROUTINES
    //*******************

    void TransformAAAA(const double    *  mem,                             double    * ERI , p_ERIbuffer & buffer) const; //only need one for each atom
    template<bool SR, bool LR>
    void ContractCDR_GC (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * __restrict__ uv_m_st, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;

    //PACKED DOUBLE ROUTINES
    //**********************

    //MIRROR transformations
    void TransformABCD(const cacheline64 *  mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, p_ERIbuffer & buffer) const;
    void TransformAACD(const cacheline64 *  mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, p_ERIbuffer & buffer) const;
    void TransformAACC(const cacheline64 *  mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, p_ERIbuffer & buffer) const;
    void TransformABAB(const cacheline64 *  mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, p_ERIbuffer & buffer) const;

    //K4/CDR general ontractions
    template <bool SAB, bool SCD, bool SR, bool LR> void ContractCDR_GC (const ERIgeometries64 & vars8, const LibQuimera::ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * __restrict__ uv_m_st8, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;
    template <bool SAB, bool SCD, bool SR, bool LR> void CalcGammas     (const ERIgeometries64 & vars8, const LibQuimera::ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;



    //PACKED FLOAT ROUTINES
    //*********************

    //MIRROR transformations
    void TransformABCD(const cacheline32 *  mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, p_ERIbuffer & buffer) const;
    void TransformAACD(const cacheline32 *  mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, p_ERIbuffer & buffer) const;
    void TransformAACC(const cacheline32 *  mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, p_ERIbuffer & buffer) const;
    void TransformABAB(const cacheline32 *  mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, p_ERIbuffer & buffer) const;


    //K4/CDR general ontractions
    void ContractCDR_GC (const ERIgeometries32 & vars16, const LibQuimera::ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline32 * __restrict__ uv_m_st8, p_ERIbuffer & buffer) const;
    void CalcGammas     (const ERIgeometries32 & vars16, const LibQuimera::ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, p_ERIbuffer & buffer, bool OnlyJ = false) const;


    //NEW ROUTINES FOR ARBITRARY VECTORIZATION CODE
    //*********************************************
    template <int N> void TransformABCD(const cachelineN<N> *  mem, const ERIgeometriesN<N> & vars, cachelineN<N> * ERI, p_ERIbuffer & buffer) const;
    template <int N> void TransformAACD(const cachelineN<N> *  mem, const ERIgeometriesN<N> & vars, cachelineN<N> * ERI, p_ERIbuffer & buffer) const;
    template <int N> void TransformAACC(const cachelineN<N> *  mem, const ERIgeometriesN<N> & vars, cachelineN<N> * ERI, p_ERIbuffer & buffer) const;
    template <int N> void TransformABAB(const cachelineN<N> *  mem, const ERIgeometriesN<N> & vars, cachelineN<N> * ERI, p_ERIbuffer & buffer) const;


    //K4/CDR general ontractions
    template <bool SAB, bool SCD, int N> void ContractCDR_GC (const ERIgeometriesN<N> & vars16, const LibQuimera::ERITile<N> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cachelineN<N> * __restrict__ uv_m_st8, p_ERIbuffer & buffer) const;
    template <bool SAB, bool SCD, int N> void CalcGammas     (const ERIgeometriesN<N> & vars16, const LibQuimera::ERITile<N> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, p_ERIbuffer & buffer, bool OnlyJ = false) const;
};

#endif
