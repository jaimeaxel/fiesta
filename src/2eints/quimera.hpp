/*
    Copyright 2013,2014,2015 Jaime Axel Rosal Sandberg

    This file is part of the EFS library.

    The EFS library is free software:  you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The EFS library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the EFS library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __QUIMERA__
#define __QUIMERA__

#include <map>
#include <queue>

#include "defs.hpp"

class ShellPairPrototype;
class ShellPair;

class ERIroutine;
class ERItype;
class K4benchmark;

class cacheline;
class cacheline64;
class cacheline32;
template <int N> class cachelineN;

class ERIgeometries64;
class ERIgeometries32;
template <int N> class ERIgeometriesN;

class p_Qalgorithm;
class p_ERIbuffer;
class p_Quimera;

namespace LibQuimera {
    struct ERITile64;
    struct ERITile32;
    template <int N> struct ERITile;

    class Quimera;
    class Qalgorithm;
    class ERIbuffer;
}


typedef void (*K4routine)  (p_ERIbuffer &, const ERIgeometries64 &, const LibQuimera::ERITile64 &, const ShellPairPrototype &, const ShellPairPrototype &, cacheline64 *);
typedef void (*K4routineF) (p_ERIbuffer &, const ERIgeometries32 &, const LibQuimera::ERITile32 &, const ShellPairPrototype &, const ShellPairPrototype &, cacheline32 *);


struct ERItype {
    //basic information
    GEOM geometry;
    uint8_t  La;
    uint8_t  Lb;
    uint8_t  Lc;
    uint8_t  Ld;

    bool isGC;  //general contraction
    bool isLR;  //long range integrals
    bool isCDR; //CDR in K4
    bool inGPU;

    ERItype & operator()(GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld, bool gc, bool lr, bool cdr) {
        geometry = geo;
        La = la;
        Lb = lb;
        Lc = lc;
        Ld = ld;
        isGC  = gc;
        isLR  = lr;
        isCDR = cdr;
        inGPU = false;

        return *this;
    }

    //to enumerate them
    inline bool operator<(const ERItype & rhs) const {
        if (geometry!=rhs.geometry) return geometry<rhs.geometry;
        if (La      !=rhs.La      ) return La      <rhs.La;
        if (Lb      !=rhs.Lb      ) return Lb      <rhs.Lb;
        if (Lc      !=rhs.Lc      ) return Lc      <rhs.Lc;
        if (Ld      !=rhs.Ld      ) return Ld      <rhs.Ld;

        if (isGC  != rhs.isGC)  return isGC;
        if (isCDR != rhs.isCDR) return isCDR;
        if (isLR  != rhs.isLR)  return isLR;
        if (inGPU != rhs.inGPU) return inGPU;

        return false;
    }
};


//some class should have performance data
//also, variable precision
struct K4benchmark {

    ERItype type;

    double deltaGAMMA;
    uint64_t ncallsGAMMA;
    uint64_t tflopsGAMMA;

    double deltaK4;
    uint64_t ncallsK4;
    uint64_t tflopsK4;

    double deltaMIRROR;
    uint64_t ncallsMIRROR;
    uint64_t tflopsMIRROR;


    //average Ks
    uint64_t aK4;

    uint64_t aKa;
    uint64_t aKb;
    uint64_t aKc;
    uint64_t aKd;

    //FLOPs in each loop
    int nK4J0;
    int nK4J1;
    int nK3J1;
    int nK3J2;
    int nK2J2;
    int nK2J3;
    int nK1J3;
    int nK1J4;
    int nK0J4;

    int nMIRROR;



    K4benchmark();
    void   AddCallK4 (const LibQuimera::ERITile64 & ET                 , const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);
    void   AddCallK4 (const LibQuimera::ERITile32 & ET                 , const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);
    void   AddCallK4 (const LibQuimera::ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);
    void   AddCallK4 (uint32_t NT,                           const ShellPairPrototype & ABp, const ShellPairPrototype & CDp);

    void   AddCallM  (uint32_t NTJ4);

    void   Statistics(double & AtG, double & AtK, double & AtT) const;
    void   Reset();
};


class p_ERIbuffer {
    friend class ERIroutine;
    friend class LibQuimera::ERIbuffer;

  public:
    double * buffer;
    double * bufferk4;
    double * v_m;
    double * uv_m;
    double * uv_m_t;

    double * f0;
    double * f0e;
    double * f0f;
    double * k4j1e;
    double * k4j1f;
    double * k3j2e;
    double * k3j2f;
    double * k2j3e;
    double * k2j3f;
    double * k1j4e;
    double * k1j4f;


    void * bufferMIRROR;
    void * bufferK4;
    void * v_m8;
    void * uv_m8;
    void * uv_m_t8;

    void * KAB;
    void * KCD;

  public:

    void * F0;
    void * F0e;
    void * F0f;
    void * K4J1e;
    void * K4J1f;
    void * K3J2e;
    void * K3J2f;
    void * K2J3e;
    void * K2J3f;
    void * K1J4e;
    void * K1J4f;

    size_t SetBuffers   (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & routine);
    size_t SetBuffersK4 (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & routine);
    size_t SetBuffersM  (void * mem, size_t ssize, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp,  const p_Qalgorithm & routine);
};

#ifndef __CUDACC__
class cudaStream_t;
#endif

class p_Qalgorithm {

    friend class p_ERIbuffer;
    friend class p_Quimera;
    friend class LibQuimera::Quimera;    // this will set BenchmarkAlgorithms at its inizialization step
    friend class LibQuimera::Qalgorithm; // this class accesses its methods directly

    static bool BenchmarkAlgorithms;

    //private:
    ERItype eritype;

    ERIroutine * IC; //interpreted code
    K4routine  * SC; //actual routine
    bool IsDynamic;

    //K4benchmark * bench;

    //for K4buffer
    uint32_t MaxMem;
    uint32_t K4Mem;

    uint32_t memF0;
    uint32_t memF0e;
    uint32_t memF0f;
    uint32_t memK4J1e;
    uint32_t memK4J1f;
    uint32_t memK3J2e;
    uint32_t memK3J2f;
    uint32_t memK2J3e;
    uint32_t memK2J3f;
    uint32_t memK1J4e;
    uint32_t memK1J4f;

    uint32_t nKernels;


    //init
    p_Qalgorithm(K4routine & routine);
    void SetStatic (ERItype   & type, K4routine & routine);
    void SetIC     (ERItype   & type);
    void Initialize();
    void Clear();

    //public
    p_Qalgorithm();
   ~p_Qalgorithm();

    uint32_t GetNKernels() const;

    size_t MemSize    (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp) const;

    //1 center routines
    void K4     (const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;
    void MIRROR (const double *  uv_m_st, double * W, p_ERIbuffer & buffer) const;

    //packed double routines
    void K4     (const ERIgeometries64 & vars8, const LibQuimera::ERITile64 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline64 * uv_m_st8, p_ERIbuffer & buffer, double iw2sr, double iw2lr) const;
    void MIRROR (const cacheline64 *   mem, const ERIgeometries64 & vars8, cacheline64 * ERI8, p_ERIbuffer & buffer) const;

    //packed single routines
    void K4     (const ERIgeometries32 & vars16, const LibQuimera::ERITile32 & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cacheline32 * uv_m_st16, p_ERIbuffer & buffer, bool OnlyJ = false) const;
    void MIRROR (const cacheline32 *   mem, const ERIgeometries32 & vars16, cacheline32 * ERI16, p_ERIbuffer & buffer) const;

    //free vector routines
    void K4     (const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, const LibQuimera::ERITile<DOUBLES_PER_BLOCK> & ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, cachelineN<DOUBLES_PER_BLOCK> * uv_m_st, p_ERIbuffer & buffer, bool OnlyJ = false) const;
    void MIRROR (const cachelineN<DOUBLES_PER_BLOCK> * mem,  const ERIgeometriesN<DOUBLES_PER_BLOCK> & vars, cachelineN<DOUBLES_PER_BLOCK> * ERI, p_ERIbuffer & buffer) const;

    //GPU routines
    void K4     (double * d_geom,  double * d_F0, double * d_ab, double * d_cd, const LibQuimera::ERITile<DOUBLES_PER_BLOCK> * ET, const ShellPairPrototype & ABp, const ShellPairPrototype & CDp, double * uv_m_st, uint32_t Ntiles, cudaStream_t * stream) const;
    void MIRROR (double * d_kern,  double * d_geom,  double * d_ERI, uint32_t Ntiles, int J4, cudaStream_t * stream) const;
};

// declare template member function specializations in header



class p_Quimera {

    friend class LibQuimera::Quimera;
    friend class p_Qalgorithm;

  private:

    //private:
    std::map             <ERItype, LibQuimera::Qalgorithm*>  Solvers;
    std::map             <ERItype, K4benchmark*> Benchmarks;

    std::priority_queue <ERItype>              SetLater;

    void SetStatic      (GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld, K4routine routine, bool overwrite);
    void SetInterpreted (GEOM geo, uint8_t la, uint8_t lb, uint8_t lc, uint8_t ld,                    bool overwrite);

    void LinkStatic     (bool overwrite);
    void LinkLibrary    (bool overwrite);

    const LibQuimera::Qalgorithm * SelectAlgorithm (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld);

    void ListNeeded     (GEOM geometry, uint8_t  La, uint8_t  Lb, uint8_t  Lc, uint8_t  Ld);
    void GenerateNeeded (bool overwrite);

    void Statistics() const;

  public:

    p_Quimera();
   ~p_Quimera();

} extern p_Q;



#endif


