
#include <Python.h>

static PyObject * efs_fock2e(PyObject * self, PyObject * args) {
    const char *command;
    int sts;

    if (!PyArg_ParseTuple(args, "s", &command))
        return NULL;
    sts = system(command);
    return Py_BuildValue("i", sts);
}


static PyMethodDef EfsMethods[] = {
    {"fock2e",  efs_fock2e, METH_VARARGS, "Compute 2-electronic contributions to the Fock operator"},
    {NULL, NULL, 0, NULL}
};
