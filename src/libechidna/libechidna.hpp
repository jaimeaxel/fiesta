/*
    ECHIDNA library:    Exchange + Coulomb Hartree-fock by Integral Direct Numerical Algorithms
                     or 2-Electron Contributions for Hartree-fock by Integral Direct Numerical Algorithms

    by Jaime Axel Rosal Sandberg, 19 February 2013
*/

#ifndef __LIB_ECHIDNA__
#define __LIB_ECHIDNA__

#include <cstdlib>
#include <string>

#include "low/plotters.hpp"

#include "linear/newsparse.hpp"

#include "basis/GTO.hpp"
#include "basis/atomprod.hpp"
#include "contraction/contractions.hpp"
#include "EFS/fock2e.hpp"

class rAtom;

class rAtomPrototype {
  public:
    std::string name; // name
    double mass;      // mass
    double Z;         // charge

    uint16_t DId;           // Id in the original definition list
    uint16_t RId;           // Id in the reduced element list
    r1tensor<GTO>  basis;

    rAtom *  Atoms;
    uint32_t nAtoms;

    rAtomPrototype();
   ~rAtomPrototype();
};

class rAtomPrototypes {
  public:
    r1tensor<rAtomPrototype> prototypes;
};

class rAtom {
  public:
    point c;
    rAtomPrototype * rAP;
}; //256 bits

class Echidna {
    friend class BatchInfo;
    friend class BatchEvaluator;
    static DMD dmd;

  public:
    static MessagePlotter EMessenger;
    static MessagePlotter EBenchmarker;
    static MessagePlotter EDebugger;

    static size_t MAXMEM;
    static size_t MAXMEMGPU;
    static size_t MEMBUFFER;

    static bool UseGPU;
    static int  nGPUs;

  private:
    rAtom                   * AtomListGpus[MAX_GPUS];
    rAtom                   * Atoms;
    int                       nAtoms;
    Fock2e                    WW;
    sparsetensorpattern       SparseTensorPattern;
    AtomProdPrototypes        APprototypes;
    APlists                   AtomProductLists;
    r1tensor<rAtomPrototype*> AtomTypeList;
    uint64_t                  TotAtomPairs;

    void InitGPUs             (const rAtom * Atoms, int nAtoms);
    void ClearGPUs            ();

  public:
    Echidna();
   ~Echidna();

    // initialization routines

    void Init                 (const rAtom * Atoms, int nAtoms);
    void ComputeAtomPairs     (const r2tensor<int> & AtomPairs, double logGDO);
    void InitFock2e           (double xs=1.);                    // for HF and hybrid DFT
    void InitFock2e           (double a, double b, double m);    // for range-separated hybrid DFT functionals such as CAM-B3LYP

    void SetExchange          (double xs);
    void SetExchange          (double a, double b, double m);



    // compute the CD / SVD ERI basis
    void InitCholesky         (double thresh=1.e-10);

    void FockUpdate           (tensor2 & F, const tensor2 & AD,           double Dthresh, double Sthresh, bool SplitJX = false);
    void FockUpdate           (tensor2 * F, const tensor2 *  D, int nnat, double Dthresh, double Sthresh, bool SplitJX = false);

    void Contraction          (tensor2 * J, tensor2 * X, tensor2 * A, const tensor2 *  D, int nmat, double Dthresh, double Sthresh, bool SplitJX = true);


    // compute J(D) using the CD / SVD ERI basis  (in AO basis)
    void ContractionCholesky  (tensor2 * J, const tensor2 *  D, int nmat, double thresh=1.e-10);

    // compute the (ij|ij) energies (in AO basis)
    void DiagonalEnergies     (tensor2 & D, const tensor2 &  C,           double thresh=1.e-10);

    // compute the (ii|jj) energies (in AO basis)
    void DiagonalExchange     (tensor2 & D, const tensor2 &  C,           double thresh=1.e-10);

    // transform the CD to MO basis; get (nn|kl) integral matrix
    void CDtransform          (const tensor2 & CMO); // transform to MO

    // get specific integral subblocks in MO basis
    void GetIJNN              (tensor2 & D, int64_t n, double precision = 1.e-10);
    void GetIJIJ              (tensor2 & D,            double precision = 1.e-10);
    void GetIIJJ              (tensor2 & D,            double precision = 1.e-10);

    // get a bunch of scaled CD/SVD matrices in MO, for RSP preconditioning
    void GetCholesky          (tensor2 * V, int nO, int nV, int & M, double precision=1.e-10);


    void CoulombCholesky  (tensor2 & W, const tensor2 & U, const tensor2 & V, double precision = 1.e-10);
    void ExchangeCholesky (tensor3 & W, const tensor2 & U, const tensor2 & V, double precision = 1.e-10);
    void ExchangeCholesky (tensor3 & W, const tensor2 & U, double precision = 1.e-10);



    // get the full ERI tensor
    void GetFullTensor        (tensor4 & W2, double thresh);
    void GetFullTensor        (tensor4 & W2, double thresh, double wf);



    // getters
    int nShellPairs () const;
    const ShellPair * GetShellPairs() const;
    const rAtom     * GetAtoms     () const;

}; // __attribute__((visibility ("default")));


#endif
