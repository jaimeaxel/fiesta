#include <map>

#include "math/angular.hpp"
using namespace LibAngular;

#include "libquimera/libquimera.hpp"
#include "libechidna.hpp"

DMD     Echidna::dmd;

MessagePlotter Echidna::EMessenger;
MessagePlotter Echidna::EBenchmarker;
MessagePlotter Echidna::EDebugger;

size_t Echidna::MAXMEM     =    1*Gword;
size_t Echidna::MAXMEMGPU  =  768*Mword;
size_t Echidna::MEMBUFFER  =  512*Mword;

bool   Echidna::UseGPU     =  true;
int    Echidna::nGPUs      =  1;


rAtomPrototype::rAtomPrototype() {Atoms=NULL;}
rAtomPrototype::~rAtomPrototype() {/*delete[] Atoms;*/}

//echidna
Echidna::Echidna() {
    LibQuimera::Quimera::QMessenger.SetParent(&EMessenger);
    LibQuimera::Quimera::QBenchmarker.SetParent(&EBenchmarker);

    EMessenger.SetModule   ("Echidna module");
    EBenchmarker.SetModule ("Echidna module benchmarker");
    EDebugger.SetModule    ("Echidna module debugger");

    Atoms = NULL;
    for (int n=0; n<MAX_GPUS; ++n) AtomListGpus[n] = NULL;
}

Echidna::~Echidna() {

    //sparsetensorpattern::UnSetBlocks();
    SparseTensorPattern.UnSetBlocks();

    WW.Clear();
    delete[] Atoms;
    Atoms = NULL;

    #ifdef USE_GPU
    ClearGPUs();
    #endif
}

void Echidna::Init (const rAtom * AtomList, int nAtomsL) {

    EBenchmarker.SetFile("echidna.benchmark.out");
    EDebugger.SetFile   ("echidna.debug.out");


    // makes a hard copy of the atom list
    nAtoms = nAtomsL;
    Atoms  = new rAtom[nAtoms];

    for (int at1=0; at1<nAtoms; ++at1)
        Atoms[at1] = AtomList[at1];


    #ifdef USE_GPU
    //copy the atoms' list to the GPU(s)
    InitGPUs (Atoms, nAtoms);
    #endif


    // generates an index of elements in the molecule
    std::map<uint16_t, rAtomPrototype*> ATList;
    std::map<uint16_t, rAtomPrototype*>::iterator it;

    for (int at1=0; at1<nAtoms; ++at1) {
        ATList[Atoms[at1].rAP->RId] =  Atoms[at1].rAP; //Atoms[at1].rAP->DId;
    }

    AtomTypeList.set(ATList.size());

    for (it=ATList.begin(); it!=ATList.end(); ++it) {
        uint16_t             rid = it->first;
        rAtomPrototype * pAt = it->second;

        AtomTypeList[rid] = pAt;
    }

    // initialize the sparse pattern in fock2e
    WW.Init (Atoms, nAtoms);

    // initializes the sparsetensor block pattern
    //sparsetensorpattern::SetBlocks(Atoms, nAtoms, AtomTypeList);
    SparseTensorPattern.SetBlocks(Atoms, nAtoms, AtomTypeList);

    // initializes LibAngular (needed to compute the batch constants)
    LibAngular::InitAngular();

    // generates the atom pair prototypes
    //Echidna::EMessenger << "Generating elemental basis pairs";

    //Echidna::EMessenger.Push(); {
        APprototypes.GenerateFrom(AtomTypeList);
    //} Echidna::EMessenger.Pop();
}

void Echidna::ComputeAtomPairs (const r2tensor<int> & AtomPairs, double logGDO) {

    // counts the total number of atom interactions
    TotAtomPairs = 0;

    for (int i=0; i<AtomPairs.N(); ++i) {
        TotAtomPairs += AtomPairs.M(i);
    }

    // generates the atom pair information
    AtomProductLists.From (AtomTypeList, APprototypes, AtomPairs, Atoms, nAtoms, logGDO);
}

void Echidna::InitFock2e (double xs) {

    LibQuimera::Quimera::QBenchmarker.SetFile("quimera.benchmark.out");

    // initializes the Fock 2e solver
    WW.Init (TotAtomPairs, AtomProductLists, SparseTensorPattern, AtomListGpus);

    // computes the cauchy-schwarz parameters of the surviving ShellPairs
    WW.CalcCauchySwarz();

    // set exchange scaling
    WW.SetXS(xs);
}

void Echidna::InitFock2e (double a, double b, double m) {

    LibQuimera::Quimera::QBenchmarker.SetFile("quimera.benchmark.out");

    // initializes the Fock 2e solver
    WW.Init (TotAtomPairs, AtomProductLists, SparseTensorPattern, AtomListGpus);

    // computes the cauchy-schwarz parameters of the surviving ShellPairs
    WW.CalcCauchySwarz();

    // set exchange scaling
    WW.SetCAM(a,b,m);
}

void Echidna::SetExchange (double xs) {
    // set exchange
    WW.SetXS(xs);
}

void Echidna::SetExchange (double a, double b, double m) {
    // set exchange scaling
    WW.SetCAM(a,b,m);
}


// get methods
int Echidna::nShellPairs () const {
    return AtomProductLists.nGTOps;
}

const ShellPair * Echidna::GetShellPairs() const {
    return AtomProductLists.SPpool;
}

const rAtom * Echidna::GetAtoms() const {
    return Atoms;
}

// computes the 2e contributions to the fock matrix
void Echidna::FockUpdate (tensor2 & F, const tensor2 & AD, double Dthresh, double Sthresh, bool SplitJX) {
    WW.FockUpdate (&F, &AD,   1, Dthresh, Sthresh, SplitJX);
}

void Echidna::FockUpdate (tensor2 * F, const tensor2 * D, int nmat, double Dthresh, double Sthresh, bool SplitJX) {
    WW.FockUpdate ( F,  D, nmat, Dthresh, Sthresh, SplitJX);
}

void Echidna::Contraction          (tensor2 * J, tensor2 * X, tensor2 * A, const tensor2 *  D, int nmat, double Dthresh, double Sthresh, bool split) {
    WW.ContractDensities (J, X, A,  D, nmat, Dthresh, Sthresh, split);
}

void Echidna::GetFullTensor (tensor4 & W2, double thresh) {
    LibQuimera::Quimera::SetCASE(false);
    WW.Get2eTensor (W2, thresh);
}

void Echidna::GetFullTensor (tensor4 & W2, double thresh, double wf) {
    LibQuimera::Quimera::SetW2(wf);
    LibQuimera::Quimera::SetCASE(true);
    WW.Get2eTensor (W2, thresh);
}

void Echidna::InitCholesky             (double thresh) {
    LibQuimera::Quimera::SetCASE(false);
    WW.FactorCholesky (thresh);
}

void Echidna::ContractionCholesky  (tensor2 * J, const tensor2 *  D, int nmat, double thresh) {
    WW.ContractCholesky        (J, D, nmat, thresh);
}

void Echidna::DiagonalEnergies     (tensor2 & D, const tensor2 &  C,           double thresh) {
    WW.ComputeDiagonal         (D, C, thresh);
}

void Echidna::DiagonalExchange     (tensor2 & D, const tensor2 &  C,  double thresh) {
    WW.ComputeDiagonalExchange (D, C, thresh);
}

void Echidna::CDtransform          (const tensor2 & CMO) {
    WW.CDtransform (CMO);
}

void Echidna::GetIJNN              (tensor2 & D, int64_t n, double precision) {
    WW.GetIJNN (D, n, precision);
}

void Echidna::GetIJIJ              (tensor2 & D,            double precision) {
    WW.GetIJIJ (D,    precision);
}

void Echidna::GetIIJJ              (tensor2 & D,            double precision) {
    WW.GetIIJJ (D,    precision);
}

// get a bunch of scaled CD/SVD matrices in MO, for RSP preconditioning
void Echidna::GetCholesky          (tensor2 * V, int nO, int nV, int & M, double precision) {
    WW.GetCholesky(V,nO,nV,M,precision);
}

void Echidna::CoulombCholesky  (tensor2 & W, const tensor2 & U, const tensor2 & V, double precision) {
    WW.CoulombCholesky (W,U,V,precision);
}

void Echidna::ExchangeCholesky (tensor3 & W, const tensor2 & U, const tensor2 & V, double precision) {
    WW.ExchangeCholesky (W,U,V,precision);
}

void Echidna::ExchangeCholesky (tensor3 & W, const tensor2 & U, double precision) {
    WW.ExchangeCholesky (W,U,precision);
}

// set sparse tensor based on the atom list
void sparsetensorpattern::SetBlocks(const rAtom * Atoms, int nAtoms, const r1tensor<rAtomPrototype*> & AtomTypeList) {

    natoms = nAtoms;

    //count how many different 'elements' really are; set a correspondence between elements and a local identifier
    ids = new uint16_t[natoms];

    //map<uint16_t, uint16_t> mapIds;
    //uint16_t nid = 0;
    for (uint32_t at=0; at<natoms; ++at) {
        uint16_t id = Atoms[at].rAP->DId;
        ids[at] = id;
        /*
        if (mapIds.count(id)==0) {
            mapIds[id] = nid;
            nid++;
        }
        */
    }
    nids = AtomTypeList.N(); //mapIds.size();

    //hace el mapa inverso

    /*
    uint16_t * types = new uint16_t[nids];
    for (map<uint16_t, uint16_t>::iterator it=mapIds.begin(); it!=mapIds.end(); ++it) {
        types[it->second] = it->first;
    }
    */


    //ATOM BLOCK LEVEL
    //****************

    //compute the width/length of the element block
    alen  = new uint16_t [nids];

    for (uint16_t id=0; id<nids; ++id) {
        uint16_t w = 0;
        //uint16_t type = types[id];

        //sum all basis functions
        for (uint8_t b=0; b<AtomTypeList[id]->basis.N(); ++b) {
            uint8_t l = AtomTypeList[id]->basis[b].l;
            w += (2*l+1) * AtomTypeList[id]->basis[b].J;
        }
        /*
        for (uint8_t b=0; b<BasisSet[type].n; ++b) {
            uint8_t l = BasisSet[type][b].l;
            w += (2*l+1) * BasisSet[type][b].J;
        }
        */
        alen[id] = w;
    }


    //offsets from the pointer to the linear array
    a2pos.set(natoms, natoms);
    a1pos = new uint32_t[natoms];

    uint32_t Apos2 = 0; //total accumulated offset
    uint32_t Apos1 = 0; //total accumulated offset

    for (uint32_t ati=0; ati<natoms; ++ati) {
        a1pos[ati] = Apos1;
        uint16_t id1   = Atoms[ati].rAP->RId;  //mapIds[ids[ati]];
        Apos1 += alen[id1];

        for (uint32_t atj=0; atj<natoms; ++atj) {
            a2pos[ati][atj] = Apos2;
            uint16_t id2   = Atoms[atj].rAP->RId; //mapIds[ids[atj]];
            Apos2 += alen[id1]*alen[id2];
        }
    }


    //GC FUNCTION BLOCK LEVEL
    //***********************

    //MemAllocator palloc;

    //first initialize the function block lengths' array
    nfs  = new uint16_t [nids];
    for (uint32_t id=0; id<nids; ++id) nfs[id] = AtomTypeList[id]->basis.N(); //BasisSet[types[id]].n;

    flen.set(nfs, nids);
    js.set(nfs, nids);
    ms.set(nfs, nids);

    for (uint16_t id=0; id<nids; ++id) {

        //sum all basis functions
        for (uint8_t b=0; b<AtomTypeList[id]->basis.N(); ++b) {
            uint8_t l =AtomTypeList[id]->basis[b].l;
            flen[id][b] = nmS[l] * AtomTypeList[id]->basis[b].J;
            js[id][b] = AtomTypeList[id]->basis[b].J;
            ms[id][b] = nmS[l];
        }

        /*
        uint16_t type = types[id];

        //sum all basis functions
        for (uint8_t b=0; b<BasisSet[type].n; ++b) {
            uint8_t l = BasisSet[type][b].l;
            flen[id][b] = nmS[l] * BasisSet[type][b].J;
            js[id][b] = BasisSet[type][b].J;
            ms[id][b] = nmS[l];
        }
        */
    }

    f2pos.set(nids, nids);

    //for each element pair combination
    for (uint16_t id1=0; id1<nids; ++id1) {
        for (uint16_t id2=0; id2<nids; ++id2) {
            f2pos[id1][id2].set(nfs[id1], nfs[id2]);

            uint32_t ww = 0;

            //loop over both atom's function blocks
            for (int b1=0; b1<nfs[id1]; ++b1) {
                for (int b2=0; b2<nfs[id2]; ++b2) {
                    f2pos[id1][id2][b1][b2] = ww;
                    ww += flen[id1][b1]*flen[id2][b2];
                }
            }
        }
    }


    f1pos.set(nfs, nids);
    for (uint16_t id1=0; id1<nids; ++id1) {
        uint32_t ww = 0;
        for (int b1=0; b1<nfs[id1]; ++b1) {
            f1pos[id1][b1] = ww;
            ww += flen[id1][b1];
        }
    }



    //change the identifiers  for the inner definition
    for (uint32_t at=0; at<natoms; ++at)
        ids[at] = Atoms[at].rAP->RId; //mapIds[ids[at]];


    //calcula el numero final de filas/columnas y valores que se deben almacenar
    ntotal = 0;
    for (uint32_t at=0; at<natoms; ++at)
        ntotal += alen[ids[at]];

    ntotal2 = ntotal*ntotal;

    //delete[] types;
}



void sparsetensorpattern::UnSetBlocks() {

    delete[] ids;
    delete[] alen;
    delete[] a1pos;
    delete[] nfs;

    ids   = NULL;
    alen  = NULL;
    a1pos = NULL;
    nfs   = NULL;
}
