#include "libechidna.hpp"

//copy the atoms' list to the GPU(s)
void Echidna::InitGPUs (const rAtom * AtomList, int nAtoms) {

    int nGPUs;
    cudaGetDeviceCount(&nGPUs);

    for (int n=0; n<nGPUs; ++n) {
        cudaSetDevice(n);
        cudaMalloc ( (void **)&(AtomListGpus[n]),            nAtoms*sizeof(rAtom));
        cudaMemcpy (            AtomListGpus[n], AtomList,   nAtoms*sizeof(rAtom), cudaMemcpyHostToDevice);
    }

}


void Echidna::ClearGPUs() {

    int nGPUs;
    cudaGetDeviceCount(&nGPUs);

    for (int n=0; n<nGPUs; ++n) {
        cudaSetDevice(n);
        cudaFree(AtomListGpus[n]);
    }
}
