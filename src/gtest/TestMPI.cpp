#include <iostream>
#include <fstream>
#include <string>
#include <gtest/gtest.h>

#include "low/MPIwrap.hpp"
#include "fiesta/fiesta.hpp"

using namespace std;
using namespace Fiesta;

TEST(MPI, AllReduce)
{
    // AllReduce == 1+2+...+n
    double n = (double)(NodeGroup.GetTotalNodes());
    double i = (double)(NodeGroup.GetRank() + 1);
    NodeGroup.AllReduce(&i, 1);
    EXPECT_NEAR(i, n*(n+1)/2, 1.0e-8);
}

TEST(MPI, BroadcastAllReduce)
{
    // Broadcast_AllReduce == 1+1+...+1 = n
    double n = (double)(NodeGroup.GetTotalNodes());
    double i = (double)(NodeGroup.GetRank() + 1);
    NodeGroup.Broadcast(&i, 1);
    NodeGroup.AllReduce(&i, 1);
    EXPECT_NEAR(i, n, 1.0e-8);
}

TEST(MPI, ReduceBroadcast)
{
    // Reduce_Broadcast == AllReduce
    double a = (double)(NodeGroup.GetRank() + 1);
    NodeGroup.Reduce(&a, 1);
    NodeGroup.Broadcast(&a, 1);
    double b = (double)(NodeGroup.GetRank() + 1);
    NodeGroup.AllReduce(&b, 1);
    EXPECT_NEAR(a, b, 1.0e-8);
}
