#include <iostream>
#include <fstream>
#include <string>
#include <gtest/gtest.h>

#include "low/MPIwrap.hpp"
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"

using namespace std;
using namespace Fiesta;

// create xyz file for test
void write_xyz(string filename, int charge, int spinmult, string method, string xcfun="")
{
    // only master process writes to file
    if (NodeGroup.IsMaster()) {
        ofstream f_xyz (filename);
        f_xyz << "#BASIS = basis/STO-3G.bs" << endl;
        f_xyz << "#CHARGE = " << charge << endl;
        f_xyz << "#MULTIPLICITY = " << spinmult << endl;
        f_xyz << "#METHOD = " << method << endl;
        if (xcfun.size() > 0) {
            f_xyz << "#XCFUN = " << xcfun << endl;
        }
        f_xyz << "O          0.000000000000    -0.075791838099     0.000000000000" << endl;
        f_xyz << "H          0.866811766181     0.601435735706     0.000000000000" << endl;
        f_xyz << "H         -0.866811766181     0.601435735706     0.000000000000" << endl;
        f_xyz.close();
    }
    NodeGroup.Sync();
}

TEST(Fiesta, RHFWater)
{
    // create googletest input file
    write_xyz(InputFiles[0], 0, 1, "RHF");

    // run calculation
    Calculation* TestJob = new Calculation;
    TestJob->Load (InputFiles, Nfiles);
    TestJob->LoadBasis();
    TestJob->Start();

    // check scf energy
    double e_scf = TestJob->SystemData.SCF.GetTotalEnergy();
    EXPECT_NEAR(e_scf, -74.9420799505654713, 1.0e-8);
    delete TestJob;
}

TEST(Fiesta, UHFWater)
{
    // create googletest input file
    write_xyz(InputFiles[0], 1, 2, "UHF");

    // run calculation
    Calculation* TestJob = new Calculation;
    TestJob->Load (InputFiles, Nfiles);
    TestJob->LoadBasis();
    TestJob->Start();

    // check scf energy
    double e_scf = TestJob->SystemData.SCF.GetTotalEnergy();
    EXPECT_NEAR(e_scf, -74.66178437669134, 1.0e-8);
    delete TestJob;
}

TEST(Fiesta, ROHFWater)
{
    // create googletest input file
    write_xyz(InputFiles[0], 1, 2, "ROHF");

    // run calculation
    Calculation* TestJob = new Calculation;
    TestJob->Load (InputFiles, Nfiles);
    TestJob->LoadBasis();
    TestJob->Start();

    // check scf energy
    double e_scf = TestJob->SystemData.SCF.GetTotalEnergy();
    EXPECT_NEAR(e_scf, -74.65925178044536, 1.0e-8);
    delete TestJob;
}
