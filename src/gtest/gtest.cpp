#include <iostream>
#include <fstream>
#include <string>

#include "low/MPIwrap.hpp"
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "math/angular.hpp"
#include "math/gamma.hpp"
#include "libquimera/libquimera.hpp"

#include <gtest/gtest.h>

using namespace std;
using namespace Fiesta;

// main program of gtest
int main(int argc, char* argv[]) {

    // initialize google test before MPI
    int result = 0;
    testing::InitGoogleTest(&argc, argv);

    // initialize MPI and Fiesta
    Init(argc, argv);

    // leave only one listener for printing
    // https://stackoverflow.com/questions/16526664/unit-testing-mpi-programs-with-gtest
    ::testing::TestEventListeners& listeners =
        ::testing::UnitTest::GetInstance()->listeners();
    if (! NodeGroup.IsMaster()) {
        delete listeners.Release(listeners.default_result_printer());
    }

    // disable fiesta output
    Messenger.Disable();
    Benchmarker.Disable();
    Debugger.Disable();

    // Run Google Test
    result = RUN_ALL_TESTS();

    // finalize Fiesta and MPI
    Finalize();

    return result;
}
