#include <iostream>
#include <fstream>
#include <string>
#include <gtest/gtest.h>

#include "low/MPIwrap.hpp"
#include "fiesta/fiesta.hpp"
#include "fiesta/calculate.hpp"
#include "basis/GTO.hpp"

using namespace std;
using namespace Fiesta;

// create xyz file for test
void write_basis_input(string filename, string basis)
{
    // only master process writes to file
    if (NodeGroup.IsMaster()) {
        ofstream f_xyz (filename);
        f_xyz << "#BASIS = basis/" << basis << ".bs" << endl;
        f_xyz << "#CHARGE = 0" << endl;
        f_xyz << "#MULTIPLICITY = 1" << endl;
        f_xyz << "#METHOD = RHF" << endl;
        f_xyz << "O          0.000000000000    -0.075791838099     0.000000000000" << endl;
        f_xyz << "H          0.866811766181     0.601435735706     0.000000000000" << endl;
        f_xyz << "H         -0.866811766181     0.601435735706     0.000000000000" << endl;
        f_xyz.close();
    }
    NodeGroup.Sync();
}

TEST(Basis, ccpVDZ)
{
    // create input file
    write_basis_input(InputFiles[0], "cc-pVDZ");

    // load basis set
    Calculation* TestJob = new Calculation;
    TestJob->Load (InputFiles, Nfiles);
    TestJob->LoadBasis();

    const char* shell_name[] = { "S", "P", "D", "F", "G", "H" };

    // Hydrogen: shells
    ElementBasis & H_basis = TestJob->gBasis.AtomBasis[1];
    EXPECT_EQ(H_basis.Nshells(), 3);
    EXPECT_EQ(shell_name[H_basis.shell[0].l], "S");
    EXPECT_EQ(shell_name[H_basis.shell[1].l], "S");
    EXPECT_EQ(shell_name[H_basis.shell[2].l], "P");

    // Hydrogen: first shell
    GTO & H_shell_0 = H_basis.shell[0];
    EXPECT_EQ(H_shell_0.K, 3);
    EXPECT_EQ(H_shell_0.J, 1);

    // Hydrogen: exponents of first shell
    EXPECT_EQ(H_shell_0.k[2], 13.0100000);
    EXPECT_EQ(H_shell_0.k[1],  1.9620000);
    EXPECT_EQ(H_shell_0.k[0],  0.4446000);

    // Oxygen: shells
    ElementBasis & O_basis = TestJob->gBasis.AtomBasis[8];
    EXPECT_EQ(O_basis.Nshells(), 5);
    EXPECT_EQ(shell_name[O_basis.shell[0].l], "S");
    EXPECT_EQ(shell_name[O_basis.shell[1].l], "S");
    EXPECT_EQ(shell_name[O_basis.shell[2].l], "P");
    EXPECT_EQ(shell_name[O_basis.shell[3].l], "P");
    EXPECT_EQ(shell_name[O_basis.shell[4].l], "D");

    // Oxygen: first shell
    GTO & O_shell_0 = O_basis.shell[0];
    EXPECT_EQ(O_shell_0.K, 8);
    EXPECT_EQ(O_shell_0.J, 2);

    // Oxygen: exponents of first shell
    EXPECT_EQ(O_shell_0.k[7], 11720.0000000);
    EXPECT_EQ(O_shell_0.k[6],  1759.0000000);
    EXPECT_EQ(O_shell_0.k[5],   400.8000000);
    EXPECT_EQ(O_shell_0.k[4],   113.7000000);
    EXPECT_EQ(O_shell_0.k[3],    37.0300000);
    EXPECT_EQ(O_shell_0.k[2],    13.2700000);
    EXPECT_EQ(O_shell_0.k[1],     5.0250000);
    EXPECT_EQ(O_shell_0.k[0],     1.0130000);

    delete TestJob;
}
