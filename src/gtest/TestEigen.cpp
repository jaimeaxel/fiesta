#include <cmath>
#include <cstring>
#include <gtest/gtest.h>

#include "linear/eigen.hpp"
#include "linear/tensors.hpp"

using namespace std;

TEST(Eigen, SolveLinear)
{
    const int dim = 4;
    double data[dim*dim] = {
         9.0, -8.0,  7.0, -6.0,
        -8.0,  6.0,  5.0,  4.0,
         7.0,  5.0,  3.0,  2.0,
        -6.0,  4.0,  2.0,  1.0,
    };
    double rhs[dim] = {
         1.0,
         6.0,
         8.0,
        10.0
    };

    // double check symmetry
    for (int row = 0, ind = 0; row < dim; row++) {
        for (int col = row+1; col < dim; col++, ind++) {
            EXPECT_EQ(data[col * dim + row], data[row * dim + col]);
        }
    }

    // SolveLinear example 1
    // column-majored low-triangular matrix
    double* m1 = new double [dim * (dim+1) / 2];
    for (int row = 0, ind = 0; row < dim; row++) {
        for (int col = row; col < dim; col++, ind++) {
            m1[ind] = data[col * dim + row];
        }
    }
    double* v1 = new double [dim];
    memcpy(v1, rhs, dim*sizeof(double));
    SolveLinear('L', dim, m1, v1);
    EXPECT_NEAR(v1[0],  -249.0 / 2015.0, 1.0e-08);
    EXPECT_NEAR(v1[1],  6179.0 / 2015.0, 1.0e-08);
    EXPECT_NEAR(v1[2],   912.0 / 2015.0, 1.0e-08);
    EXPECT_NEAR(v1[3], -7884.0 / 2015.0, 1.0e-08);
    delete[] m1;
    delete[] v1;

    // SolveLinear example 2
    // full matrix in tensor form
    tensor2 m2 (dim, dim);
    memcpy(m2.c[0], data, sizeof(double)*dim*dim);
    double* v2 = new double [dim];
    memcpy(v2, rhs, dim*sizeof(double));
    SolveLinear(m2, v2);
    EXPECT_NEAR(v2[0],  -249.0 / 2015.0, 1.0e-08);
    EXPECT_NEAR(v2[1],  6179.0 / 2015.0, 1.0e-08);
    EXPECT_NEAR(v2[2],   912.0 / 2015.0, 1.0e-08);
    EXPECT_NEAR(v2[3], -7884.0 / 2015.0, 1.0e-08);
    delete[] v2;
}

TEST(Eigen, Transpose)
{
    const int n=3, m=4;
    double data[n*m] = {
        1.0, 2.0, 3.0, 4.0,
        5.0, 6.0, 7.0, 8.0,
        9.0,10.0,11.0,12.0
    };

    tensor2 matrix (n,m);
    memcpy(matrix.c[0], data, sizeof(double)*n*m);

    tensor2 transp = Transpose(matrix);
    for (int i = 0; i < n; i ++) {
        for (int k = 0; k < m; k ++) {
            EXPECT_EQ(matrix[i][k], transp[k][i]);
        }
    }
}

TEST(Eigen, Contract)
{
    const int n=3, m=2;
    double A[n*m] = {
        1.0, 2.0,
        5.0, 6.0,
        9.0,10.0
    };
    double B[n*m] = {
         3.0, 4.0,
         7.0, 8.0,
        11.0,12.0
    };

    tensor2 MA (n,m);
    tensor2 MB (n,m);
    memcpy(MA.c[0], A, sizeof(double)*n*m);
    memcpy(MB.c[0], B, sizeof(double)*n*m);

    double dot_prod = Contract(MA,MB);
    EXPECT_NEAR(dot_prod, 313.0, 1.0e-08);
    EXPECT_NEAR(dot_prod, DotProduct(MA,MB), 1.0e-08);
}

TEST(Eigen, TensorProduct)
{
    const int n=2, m=3;
    double A[n*m] = {
        1.0, 2.0, 3.0,
        4.0, 5.0, 6.0
    };
    double B[m*n] = {
         7.0, 8.0,
         9.0,10.0,
        11.0,12.0
    };

    tensor2 MA (n,m);
    tensor2 MB (m,n);
    memcpy(MA.c[0], A, sizeof(double)*n*m);
    memcpy(MB.c[0], B, sizeof(double)*m*n);

    tensor2 MC (n,n);

    TensorProduct(MC, MA, MB);
    EXPECT_NEAR(MC[0][0],  58.0, 1.0e-08);
    EXPECT_NEAR(MC[0][1],  64.0, 1.0e-08);
    EXPECT_NEAR(MC[1][0], 139.0, 1.0e-08);
    EXPECT_NEAR(MC[1][1], 154.0, 1.0e-08);

    TensorProductNTN(MC, Transpose(MA), MB);
    EXPECT_NEAR(MC[0][0],  58.0, 1.0e-08);
    EXPECT_NEAR(MC[0][1],  64.0, 1.0e-08);
    EXPECT_NEAR(MC[1][0], 139.0, 1.0e-08);
    EXPECT_NEAR(MC[1][1], 154.0, 1.0e-08);

    TensorProductNNT(MC, MA, Transpose(MB));
    EXPECT_NEAR(MC[0][0],  58.0, 1.0e-08);
    EXPECT_NEAR(MC[0][1],  64.0, 1.0e-08);
    EXPECT_NEAR(MC[1][0], 139.0, 1.0e-08);
    EXPECT_NEAR(MC[1][1], 154.0, 1.0e-08);

    TensorProductNTT(MC, Transpose(MA), Transpose(MB));
    EXPECT_NEAR(MC[0][0],  58.0, 1.0e-08);
    EXPECT_NEAR(MC[0][1],  64.0, 1.0e-08);
    EXPECT_NEAR(MC[1][0], 139.0, 1.0e-08);
    EXPECT_NEAR(MC[1][1], 154.0, 1.0e-08);
}

TEST(Eigen, DiagonalizeGV)
{
    int nao = 7;

    const double data_F[nao*nao] = {
      -20.2546001485719955, -5.1637802551199856,  0.0000000000000000,  0.0000000000000000, -0.0254794549582162,  -0.8533423277265844, -0.8533423277265840,
       -5.1637802551199847, -2.4106788803233097,  0.0000000000000000,  0.0000000000000000, -0.0743881698286541,  -0.8015376362004600, -0.8015376362004609,
        0.0000000000000000,  0.0000000000000000, -0.3875867131964708,  0.0000000000000000,  0.0000000000000000,   0.0000000000000000,  0.0000000000000000,
        0.0000000000000000,  0.0000000000000000,  0.0000000000000000, -0.2336399257898210,  0.0000000000000000,  -0.3577852097766888,  0.3577852097766891,
       -0.0254794549582162, -0.0743881698286540,  0.0000000000000000,  0.0000000000000000, -0.2755353502149362,  -0.3246810485869647, -0.3246810485869646,
       -0.8533423277265844, -0.8015376362004600,  0.0000000000000000, -0.3577852097766888, -0.3246810485869648,  -0.4182076184528701, -0.2522908308155771,
       -0.8533423277265840, -0.8015376362004608,  0.0000000000000000,  0.3577852097766890, -0.3246810485869644,  -0.2522908308155772, -0.4182076184528699
    };

    const double data_S[nao*nao] = {
        0.9999999999999993,  0.2367039365108476,  0.0000000000000000,  0.0000000000000000,  0.0000000000000000,  0.0384056071150126,  0.0384056071150126,
        0.2367039365108476,  0.9999999999999996,  0.0000000000000000,  0.0000000000000000,  0.0000000000000000,  0.3861388872313655,  0.3861388872313655,
        0.0000000000000000,  0.0000000000000000,  0.9999999999999987,  0.0000000000000000,  0.0000000000000000,  0.0000000000000000,  0.0000000000000000,
        0.0000000000000000,  0.0000000000000000,  0.0000000000000000,  0.9999999999999987,  0.0000000000000000,  0.2684382679121757, -0.2684382679121757,
        0.0000000000000000,  0.0000000000000000,  0.0000000000000000,  0.0000000000000000,  0.9999999999999987,  0.2097269603244158,  0.2097269603244158,
        0.0384056071150126,  0.3861388872313655,  0.0000000000000000,  0.2684382679121757,  0.2097269603244158,  0.9999999999999999,  0.1817599213649755,
        0.0384056071150126,  0.3861388872313655,  0.0000000000000000, -0.2684382679121757,  0.2097269603244158,  0.1817599213649755,  0.9999999999999999
    };

    const double data_C_column_wise[nao*nao] = {
       -0.9944345898582191,  0.2391588458174503,  0.0000000000000002, -0.0936832543127217, -0.0000000000000004,  0.1116399418332700,  0.0000000000000007,
       -0.0240970434716842, -0.8857355613217623, -0.0000000000000011,  0.4795859219235331,  0.0000000000000021, -0.6695792020267737, -0.0000000000000046,
        0.0000000000000000, -0.0000000000000000,  0.0000000000000005, -0.0000000000000044,  1.0000000000000009,  0.0000000000000000, -0.0000000000000002,
       -0.0000000000000000,  0.0000000000000006,  0.6072848332814893,  0.0000000000000021, -0.0000000000000004, -0.0000000000000066,  0.9192343054378209,
       -0.0031615495610895, -0.0858962320014375,  0.0000000000000023, -0.7474314251140016, -0.0000000000000031, -0.7384885886291446, -0.0000000000000048,
        0.0045937441598047, -0.1440395712827174,  0.4529977388204907, -0.3294711269132059, -0.0000000000000016,  0.7098495439436099, -0.7324607140077732,
        0.0045937441598046, -0.1440395712827168, -0.4529977388204889, -0.3294711269132081, -0.0000000000000011,  0.7098495439435999,  0.7324607140077830
    };

    const double data_E[nao*nao] = {
      -20.2628916031135695,
       -1.2096973997261302,
       -0.5479646853545328,
       -0.4365272126704163,
       -0.3875867169620569,
        0.4776187884712874,
        0.5881393595962406
   };

    tensor2 F (nao);
    memcpy(F.c[0], data_F, sizeof(double)*nao*nao);

    tensor2 S (nao);
    memcpy(S.c[0], data_S, sizeof(double)*nao*nao);

    tensor2 ref_C (nao);
    memcpy(ref_C.c[0], data_C_column_wise, sizeof(double)*nao*nao);
    ref_C.Transpose();

    tensor2 C (nao);
    C = F;

    double* E = new double [nao];
    DiagonalizeGV(C, S, E);

    double max_diff = 0.0;
    for (int row = 0; row < nao; row ++) {
        // determine sign from inner product
        double inn_prod = 0.0;
        for (int col = 0; col < nao; col ++) {
            inn_prod += C(row,col) * ref_C(row,col);
        }
        double factor = inn_prod < 0 ? -1.0 : 1.0;
        for (int col = 0; col < nao; col ++) {
            double diff = fabs(C(row,col) - ref_C(row,col) * factor);
            if (max_diff < diff) {
                max_diff = diff;
            }
        }
    }
    EXPECT_NEAR(max_diff, 0.0, 1.0e-08);

    double max_E_diff = 0.0;
    for (int i = 0; i < nao; i ++) {
        double diff = fabs(E[i] - data_E[i]);
        if (max_E_diff < diff) {
            max_E_diff = diff;
        }
    }
    EXPECT_NEAR(max_E_diff, 0.0, 1.0e-08);

    delete[] E;
}
