#include <string>
#include <cstring>
#include <gtest/gtest.h>

#include "low/strings.hpp"

using namespace std;

TEST(String, IsNumeric)
{
    EXPECT_TRUE(is_numeric("+1.03"));
    EXPECT_TRUE(is_numeric("-1.03"));
    EXPECT_TRUE(is_numeric("+1.03e-01"));
    EXPECT_TRUE(is_numeric("-1.03e+02"));
    EXPECT_FALSE(is_numeric("1ax3"));
}

TEST(String, Conversion)
{
    EXPECT_EQ(s2d("10"),   10.0);
    EXPECT_EQ(s2d("10.0"), 10.0);

    EXPECT_EQ(s2i("10"),   10);
    EXPECT_EQ(s2i("10.0"), 10);

    EXPECT_EQ(s2b("1"), true);
    EXPECT_EQ(s2b("0"), false);
}

TEST(String, Uppercase)
{
    EXPECT_EQ(ucase("b"), "B");
    EXPECT_EQ(ucase("Ag"), "AG");
}
