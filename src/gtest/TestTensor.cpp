#include <cstring>
#include <gtest/gtest.h>

#include "low/MPIwrap.hpp"
#include "linear/tensors.hpp"
#include "linear/eigen.hpp"

using namespace std;

TEST(Tensor2, BroadcastReduce)
{
    const int dim = 3;
    double data[dim*dim] = {
        1.0, 2.0, 3.0,
        4.0, 5.0, 6.0,
        7.0, 8.0, 9.0
    };

    tensor2 matrix (dim);
    matrix.zeroize();
    if (NodeGroup.IsMaster()) {
        memcpy(matrix.c[0], data, sizeof(double)*dim*dim);
    }
    NodeGroup.Sync();

    Tensor2Broadcast(matrix);
    Tensor2Reduce(matrix);

    int n = NodeGroup.GetTotalNodes();
    for (int i = 0; i < dim; i ++) {
        for (int k = 0; k < dim; k ++) {
            EXPECT_NEAR(matrix[i][k], data[i*dim+k]*n, 1.0e-08);
        }
    }
}

TEST(Tensor2, Constructor)
{
    const int n=3, m=4;
    tensor2* matrix = NULL;

    matrix = new tensor2;
    EXPECT_EQ(matrix->c,  nullptr);
    EXPECT_EQ(matrix->c2, nullptr);
    EXPECT_EQ(matrix->n, 0);
    EXPECT_EQ(matrix->m, 0);
    delete matrix;

    matrix = new tensor2 (n);
    EXPECT_NE(matrix->c,  nullptr);
    EXPECT_NE(matrix->c2, nullptr);
    EXPECT_EQ(matrix->c2, matrix->c[0]);
    EXPECT_EQ(matrix->n, n);
    EXPECT_EQ(matrix->m, n);
    delete matrix;

    matrix = new tensor2 (n,m);
    EXPECT_NE(matrix->c,  nullptr);
    EXPECT_NE(matrix->c2, nullptr);
    EXPECT_EQ(matrix->c2, matrix->c[0]);
    EXPECT_EQ(matrix->n, n);
    EXPECT_EQ(matrix->m, m);

    matrix->setsize(m,n);
    EXPECT_NE(matrix->c,  nullptr);
    EXPECT_NE(matrix->c2, nullptr);
    EXPECT_EQ(matrix->c2, matrix->c[0]);
    EXPECT_EQ(matrix->n, m);
    EXPECT_EQ(matrix->m, n);

    matrix->clear();
    EXPECT_EQ(matrix->c,  nullptr);
    EXPECT_EQ(matrix->c2, nullptr);
    EXPECT_EQ(matrix->n, 0);
    EXPECT_EQ(matrix->m, 0);
    delete matrix;
}

TEST(Tensor2, CopyTranspose)
{
    const int n=3, m=4;
    double data[n*m] = {
        1.0, 2.0, 3.0, 4.0,
        5.0, 6.0, 7.0, 8.0,
        9.0,10.0,11.0,12.0
    };

    tensor2 matrix (n,m);
    memcpy(matrix.c[0], data, sizeof(double)*n*m);

    tensor2 matrix_c, matrix_t;
    matrix_c = matrix;
    matrix_t = matrix;
    matrix_t.Transpose();

    for (int i = 0; i < n; i ++) {
        for (int k = 0; k < m; k ++) {
            EXPECT_EQ(matrix[i][k], matrix_c[i][k]);
            EXPECT_EQ(matrix[i][k], matrix_t[k][i]);
        }
    }
}
