# ==> ICgen parameters <==
# LMAX_T_K2C negative will skip

LMAX           = 3
LMAX_T_K2C     = 12
LMAX_T_CUDA_K4 = 4
LMAX_T_CUDA_M  = 4

# ==> XCINT/BALBOA parameters <==

MAX_L_VALUE        = 4
AO_CHUNK_LENGTH    = 32
AO_BLOCK_LENGTH    = 128
MAX_GEO_DIFF_ORDER = 2

# ==> system-specific settings <==
# architectural features, paths, etc

include arch.mk
include host.mk

# ==> Compilers <==

ifdef ENABLE_CRAY
 CXX = CC
else
 ifdef ENABLE_MPI
  CXX = $(MPICXX)
 else
  ifdef ENABLE_INTEL
   CXX = icpc
  else
   CXX = g++
  endif
 endif
endif    
LD = $(CXX)
AR = ar
NVCC = nvcc
NVCCFLAGS = -I$(CUDA_PATH)/include -arch=sm_$(CUDA_ARCH)

# ==> Compiler flags <==

# general flags
BUILD_DIR = $(shell pwd)
CXXFLAGS  = -I$(BUILD_DIR)/obj
CXXFLAGS += -I$(BUILD_DIR)/external/googletest/googletest/include
CXXFLAGS += -std=c++11
LDFLAGS   =
NVCCFLAGS += -I$(BUILD_DIR)/obj -I$(BUILD_DIR)/external/googletest/googletest/include -std=c++11

# default dirs, target static libraries
LIB    = libechidna.a libquimera.a libcontractions.a libxcint.a libbalboa.a \
		 $(BUILD_DIR)/lib/libxcfun.a
LIBDIR = -L$(BUILD_DIR)/lib

# debugger: DDT must link before any conflicting symbol
ifdef ENABLE_DDT
  CXXFLAGS +=-g3
  LIB      += -L$(DDT_PATH)/lib/64 -ldmallocthcxx -Wl,--allow-multiple-definition
endif

# linear algebra libs
ifdef ENABLE_MKL
  ifndef MKLROOT
    MKLROOT  = $(MKL_PATH)
  endif
  CBLAS_PATH = $(MKLROOT)/include/mkl_cblas.h
  CXXFLAGS  += -m64 -I$(MKLROOT)/include
  NVCCFLAGS += -m64 -I$(MKLROOT)/include
  ifdef ENABLE_OPENMP
    LIB     += -Wl,--start-group \
			   ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a \
			   ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a \
			   ${MKLROOT}/lib/intel64/libmkl_core.a \
			   -Wl,--end-group \
			   -lgomp -lpthread -lm -ldl
  else
    LIB     += -Wl,--start-group \
			   ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a \
			   ${MKLROOT}/lib/intel64/libmkl_sequential.a \
			   ${MKLROOT}/lib/intel64/libmkl_core.a \
			   -Wl,--end-group \
			   -lpthread -lm -ldl
  endif
else
  CBLAS_PATH = $(ATLAS_PATH)/include/cblas.h
  LIBDIR += -L$(ATLAS_PATH)/lib/atlas-base
  LIB    += -lcblas -latlas -llapack
endif

# python flags
ifdef ENABLE_PYTHON
  CXXFLAGS += -fPIC
  NVCCFLAGS += -fPIC
  PYTHON_INC  = $(shell $(PYTHON)-config --includes)
  PYTHON_INC += -I$(BOOST_PATH)/include
  PYTHON_INC += -I$(shell $(PYTHON) -c "import mpi4py; print(mpi4py.get_include())")
  PYTHON_LD   = $(shell $(PYTHON)-config --ldflags)
  PYTHON_LD  += -L$(BOOST_PATH)/lib -lboost_python3
endif

# profiling options (pg)
ifdef ENABLE_PROFILER
  CXXFLAGS +=  -g3
  LDFLAGS  +=  -g3
endif

# optimization flags
ifdef ENABLE_INTEL
  CXXFLAGS += -Ofast -xHost -mavx -I$(INTEL_PATH)/include
  NVCCFLAGS += --compiler-options "-Ofast -xHost -mavx" -I$(INTEL_PATH)/include
else
  CXXFLAGS += -Ofast 
  NVCCFLAGS += --compiler-options "-Ofast"
endif

# openmp flags
ifdef ENABLE_OPENMP
  ifdef ENABLE_INTEL
    CXXFLAGS += -qopenmp
    LDFLAGS  += -qopenmp
	NVCCFLAGS += --compiler-options "-qopenmp"
  else
    CXXFLAGS += -fopenmp
    LDFLAGS  += -fopenmp
	NVCCFLAGS += --compiler-options "-fopenmp"
  endif
endif

# MPI flags (via preprocessing)
ifdef ENABLE_MPI
  MPIFLAGS += -DC_MPI
endif

# ==> ICgen settings <==

DK2C  = k2c
DQIC  = qic
DK4CU = k4cu
DMCU  = mcu
DTMP  = obj/tmp

ICGEN_ARGS = -L=$(LMAX) -QIC=$(DTMP)/$(DQIC)/

ifndef DISABLE_K2C
  ICGEN_ARGS += -LK2C=$(LMAX_T_K2C) -K2C=$(DTMP)/$(DK2C)/
endif

ifdef ENABLE_GPU
  LIBDIR     += -L$(CUDA_PATH)/lib64
  LIB        += -lcudart
  ICGEN_ARGS += -LK4CU=$(LMAX_T_CUDA_K4) -LMCU=$(LMAX_T_CUDA_M) -K4CU=$(DTMP)/$(DK4CU)/ -MCU=$(DTMP)/$(DMCU)/
  CXXFLAGS   += -DUSE_GPU
  NVCCFLAGS  += -DUSE_GPU
endif

# ==> git commit hash <==

GITREV = $(shell git rev-parse HEAD)

# ==> export environmental variables <==

# compilers, linkers, etc.
export CXX
export LD
export AR
export NVCC
export ENABLE_INTEL

# compiler flags
export CXXFLAGS
export LDFLAGS
export NVCCFLAGS
export ENABLE_GPU
export PYTHON_INC
export PYTHON_LD

# libraries
export LIB
export LIBDIR

# environment variables for code generation
export ICGEN_ARGS
export LMAX
export DK2C
export DQIC
export DK4CU
export DMCU
export DTMP

# include paths
export CBLAS_PATH

# preprocessing flags
export GITREV
export MPIFLAGS

# ==> make rules <==

ERISTAMP = obj/eri.stamp.$(LMAX).$(LMAX_T_K2C).$(LMAX_T_CUDA_K4).$(LMAX_T_CUDA_M)
DFTSTAMP = obj/dft.stamp.${MAX_L_VALUE}.${AO_CHUNK_LENGTH}.${AO_BLOCK_LENGTH}.${MAX_GEO_DIFF_ORDER}

fiesta: $(DFTSTAMP) $(ERISTAMP) | objdir 
	#sync ERI code & QIC files
	rsync -ur $(DTMP)/$(DQIC)  .
	rsync -ur $(DTMP)/$(DK2C)  obj
	rsync -ur $(DTMP)/$(DK4CU) obj
	rsync -ur $(DTMP)/$(DMCU)  obj
	#print info
	@echo Git revision: $(GITREV)
	@#echo Compilation parameters: $(MACHINE) $(CUDA_ARCH) $(CUDA_VER) $(ENABLE_GPU) $(ENABLE_MKL)
	@echo NVCC flags : $(NVCCFLAGS)
	@echo ICgen arguments: $(ICGEN_ARGS)
	@echo CBLAS path: $(CBLAS_PATH)
	@echo $(ENABLE_INTEL)
	@#make everything in the obj subdirectory
	@test -d bin || mkdir -p bin
	+$(MAKE) -C obj fiesta
	+$(MAKE) -C obj fiesta-gtest
ifdef ENABLE_PYTHON
	+$(MAKE) -C obj fiesta.so
	+$(MAKE) -C obj exciton.so
	@#+$(MAKE) -C obj typhon
endif

$(DFTSTAMP): | objdir
	rm -f obj/dft.stamp.*
	python obj/dft/xcint/generate_ave_contributions.py > obj/dft/xcint/ave_contributions.h
	python obj/dft/xcint/generate.py obj/dft/xcint ${MAX_L_VALUE} ${AO_CHUNK_LENGTH} ${AO_BLOCK_LENGTH} ${MAX_GEO_DIFF_ORDER}
	python obj/dft/balboa/generate.py obj/dft/balboa ${MAX_L_VALUE} ${AO_CHUNK_LENGTH} ${AO_BLOCK_LENGTH} ${MAX_GEO_DIFF_ORDER}
	#OBS! notice there are two different versions of the generator in BALBOA and XCINT
	touch $(DFTSTAMP)

$(ERISTAMP): obj/ICgen.x
	rm -f obj/eri.stamp.*
	./obj/ICgen.x $(ICGEN_ARGS)
	touch $(ERISTAMP)

obj/ICgen.x: | objdir
	+$(MAKE) -C obj/ICgen
	@test -d  $(DQIC)  || mkdir -p  $(DQIC)
	@test -d  $(DTMP)  || mkdir -p  $(DTMP)
	@test -d  $(DTMP)/$(DQIC)  || mkdir -p  $(DTMP)/$(DQIC)
	@test -d  $(DTMP)/$(DK2C)  || mkdir -p  $(DTMP)/$(DK2C)
	@test -d  $(DTMP)/$(DK4CU) || mkdir -p  $(DTMP)/$(DK4CU)
	@test -d  $(DTMP)/$(DMCU)  || mkdir -p  $(DTMP)/$(DMCU)
	@test -d  obj/$(DK2C)  || mkdir -p   obj/$(DK2C)
	@test -d  obj/$(DK4CU) || mkdir -p   obj/$(DK4CU)
	@test -d  obj/$(DMCU)  || mkdir -p   obj/$(DMCU)

objdir: 
	@#copy files but do not overwrite if unchanged
	@test -d obj || mkdir -p obj
	@rsync -ru src/* obj
	@rsync -u src/libquimera/makefile obj/makelq

cleanf:
	@rm -rf obj

clean:
	@rm -rf obj
	@rm -rf qic
	@rm -rf bin

.PHONY: fiesta cleanf clean
