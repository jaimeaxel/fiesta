#!/bin/bash

TMP_TEST_DIR=./scratch_python
test -d $TMP_TEST_DIR || mkdir -p $TMP_TEST_DIR
cd $TMP_TEST_DIR

job_list=$(ls ../python_test/*.sh)
echo "=========="

fail=0
for job in $job_list; do
    bash $job
    if [[ $? -eq 1 ]]; then
        ((fail++))
    fi
done

if [ $fail -eq 0 ]; then
    echo "=========="
    echo "All tests passed!"
    exit 0
else
    echo "=========="
    if [ $fail -eq 1 ]; then
        echo "$fail test failed!"
    else
        echo "$fail tests failed!"
    fi
    exit 1
fi
