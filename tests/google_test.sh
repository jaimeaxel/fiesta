#!/bin/bash

TMP_TEST_DIR=./scratch_google
test -d $TMP_TEST_DIR || mkdir -p $TMP_TEST_DIR
cd $TMP_TEST_DIR

MPIRUN_CMD="mpirun -np 2"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 2"
fi

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 6
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

cat > test.xyz << EOF
#BASIS = basis/STO-3G.bs
#CHARGE = 0
#MULTIPLICITY = 1
#METHOD = RHF
//H2O
O          0.000000000000    -0.075791838099     0.000000000000
H          0.866811766181     0.601435735706     0.000000000000
H         -0.866811766181     0.601435735706     0.000000000000
EOF

export FIESTA_DIR=$(pwd)/../..
$MPIRUN_CMD $FIESTA_DIR/bin/fiesta-gtest test.xyz host.hw
