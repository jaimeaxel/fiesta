#!/bin/bash

# comparator

function FuzzyEqual() # ARG1 Ref Value, ARG2 Calced Val, AGR3 Tol
{
    BC_SCRIPT="scale = 10
               d = $1 - $2
               if (d<0.0) d = -d
               d < $3"
    return $(echo "$BC_SCRIPT" | bc)
}

# mpi command

MPIRUN_CMD="mpirun -np 6"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 8"
fi

# reference value

ref_arr=(
     0.412053
     0.415777
     0.484909
     0.559711
     0.594074
     0.669985
     0.770172
     0.795719
     0.834173
     0.923494
     0.965303
     1.046449
)

ref_size=${#ref_arr[@]}

tol=0.0003

# input files and calculation

job=py_exmod_v

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 2
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

cat > ${job}.xyz << EOF
#RUN = EXMOD
#CIS:NSTATES = 2
#CIS:CT_NOCC = 1
#CIS:CT_NVIR = 1
#FRAGINFO = frag.info
#BASIS = basis/STO-3G.bs
#CHARGE = 0
#MULTIPLICITY = 1
#METHOD = RHF
//NH3+CH4+C2H4
N         -1.96309        1.59755       -0.01963
H         -1.95876        2.61528        0.03109
H         -2.48929        1.27814        0.79244
H         -2.52930        1.35928       -0.83265
C         -0.85782       -2.68804        0.23504
H         -0.43365       -3.54435       -0.24632
H         -1.80549       -2.46285       -0.20781
H         -0.20141       -1.85123        0.11765
H         -0.99072       -2.89372        1.27664
H         -1.83266       -0.66217       -1.69931
C         -2.80597       -0.90302       -1.28194
H         -3.55431       -1.15099       -2.02901
C         -3.05692       -0.89514        0.01595
H         -2.30890       -0.64722        0.76335
H         -4.03033       -1.13600        0.43314
EOF

cat > frag.info << EOF
3
4 0 1
5 0 1
6 0 1
EOF

cat > ${job}.py << EOF
from mpi4py import MPI
import fiesta
from exciton import ExcitonModel
import numpy

comm = MPI.COMM_WORLD
rank, size = comm.Get_rank(), comm.Get_size()

sub_size = 2
sub_num  = size // sub_size

sub_group = rank // sub_size
sub_comm = MPI.Comm.Split(comm, sub_group, rank)
fiesta.init(["${job}.xyz","host.hw"], sub_comm, sub_group==0)

exciton = ExcitonModel()
exciton.init(fiesta.info(), sub_group==0)

# run monomer calculations
for frag in range(sub_group, exciton.total_nfrags, sub_num):
    task_id = frag

    task = exciton.request(task_id, "")
    result = fiesta.process_task(task)
    exciton.finalize_task(task_id, "", result);

    if (not exciton.has_dft):
        task = exciton.request(task_id, "cation")
        result = fiesta.process_task(task)
        exciton.finalize_task(task_id, "cation", result);

        task = exciton.request(task_id, "anion")
        result = fiesta.process_task(task)
        exciton.finalize_task(task_id, "anion", result);

comm.Barrier()

# find local monomers on subcomm
local_monomers = []
for pair in range(sub_group, exciton.total_npairs, sub_num):
    frag_a = exciton.pair_index_a(pair)
    frag_b = exciton.pair_index_b(pair)
    if (not frag_a in local_monomers):
        local_monomers.append(frag_a)
    if (not frag_b in local_monomers):
        local_monomers.append(frag_b)

# read necessary monomers from file
for frag in local_monomers:
    if (not frag in range(sub_group, exciton.total_nfrags, sub_num)):
        exciton.read_monomer_file(frag)

# run dimer calculations
for pair in range(sub_group, exciton.total_npairs, sub_num):
    task_id = pair + exciton.total_nfrags

    task = exciton.request(task_id, "")
    result = fiesta.process_task(task)
    exciton.finalize_task(task_id, "", result);

    if (not exciton.has_dft):
        task = exciton.request(task_id, "ca")
        result = fiesta.process_task(task)
        exciton.finalize_task(task_id, "ca", result);

        task = exciton.request(task_id, "ac")
        result = fiesta.process_task(task)
        exciton.finalize_task(task_id, "ac", result);

comm.Barrier()

# collect Hamiltonian and adiabatic transition dipoles
if rank == 0:
    for i in range(1, size):
        recvdata = comm.recv(source=i, tag=i)
        exciton.update_data(list(recvdata))
else:
    senddata = numpy.array(exciton.get_data())
    comm.send(senddata, dest=0, tag=rank)

# diagonalize Hamiltonian
exciton.finish()

fiesta.finalize(sub_comm)
sub_comm.Disconnect()
EOF

export PYTHONPATH=$(pwd)/../../bin:$PYTHONPATH
export FIESTA_DIR=$(pwd)/../..

$MPIRUN_CMD python3 ${job}.py > ${job}.out

# process and print result

val_str=`grep 'E\[' ${job}.out | awk '{print $2}'`

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    FuzzyEqual "$ref" "$val" "$tol"
    if [[ $? -eq 1 ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
