#!/bin/bash

# comparator

function FuzzyEqual() # ARG1 Ref Value, ARG2 Calced Val, AGR3 Tol
{
    BC_SCRIPT="scale = 10
               d = $1 - $2
               if (d<0.0) d = -d
               d < $3"
    return $(echo "$BC_SCRIPT" | bc)
}

# mpi command

MPIRUN_CMD="mpirun -np 6"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 8"
fi

# reference value

ref_arr=(
    -74.94207995
)

ref_size=${#ref_arr[@]}

tol=0.00000001

# input files and calculation

job=py_h2o

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 2
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

cat > ${job}.xyz << EOF
#BASIS = basis/STO-3G.bs
#CHARGE = 0
#MULTIPLICITY = 1
#METHOD = RHF
//H2O
O          0.000000000000    -0.075791838099     0.000000000000
H          0.866811766181     0.601435735706     0.000000000000
H         -0.866811766181     0.601435735706     0.000000000000
EOF

cat > ${job}.py << EOF
from mpi4py import MPI
import fiesta
comm = MPI.COMM_WORLD
fiesta.run(["${job}.xyz","host.hw"], comm)
EOF

export PYTHONPATH=$(pwd)/../../bin:$PYTHONPATH
export FIESTA_DIR=$(pwd)/../..

$MPIRUN_CMD python3 ${job}.py > ${job}.out

# process and print result

val_str=`grep 'Total energy' ${job}.out | awk '{print $4}'`

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    FuzzyEqual "$ref" "$val" "$tol"
    if [[ $? -eq 1 ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
