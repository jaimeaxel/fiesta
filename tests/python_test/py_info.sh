#!/bin/bash

# mpi command

MPIRUN_CMD="mpirun -np 6"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 8"
fi

# input files and calculation

job=py_info

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 2
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

cat > ${job}.xyz << EOF
#BASIS = basis/STO-3G.bs
#CHARGE = 0
#MULTIPLICITY = 1
#METHOD = RHF
//H2O
O          0.000000000000    -0.075791838099     0.000000000000
H          0.866811766181     0.601435735706     0.000000000000
H         -0.866811766181     0.601435735706     0.000000000000
EOF

cat > ${job}.py << EOF
from mpi4py import MPI
import fiesta
comm = MPI.COMM_WORLD
fiesta.init(["${job}.xyz","host.hw"], comm)
if (comm.Get_rank() == 0):
    info = fiesta.info()
    print("Info num_atoms: %d" % info.get_num_atoms())
    print("Info input_xyz: %s" % info.get_input_xyz())
EOF

export PYTHONPATH=$(pwd)/../../bin:$PYTHONPATH
export FIESTA_DIR=$(pwd)/../..

$MPIRUN_CMD python3 ${job}.py > ${job}.out

# reference value

ref_arr=(
    3
    ${job}.xyz
)

ref_size=${#ref_arr[@]}

# process and print result

val_str=`grep '^Info ' ${job}.out | awk '{print $3}'`

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    if [[ $val == $ref ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
