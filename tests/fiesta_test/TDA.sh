#!/bin/bash

# comparator

function FuzzyEqual() # ARG1 Ref Value, ARG2 Calced Val, AGR3 Tol
{
    BC_SCRIPT="scale = 10
               d = $1 - $2
               if (d<0.0) d = -d
               d < $3"
    return $(echo "$BC_SCRIPT" | bc)
}

# mpi command

MPIRUN_CMD="mpirun -np 2"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 2"
fi

# reference value

ref_arr=(
     0.431353
     0.505825
     0.547510
     0.371664
     0.422011
     0.437302
     0.208438
     0.249436
     0.293950
)

ref_size=${#ref_arr[@]}

tol=0.00005

# input files and calculation

job=TDA
echo > ${job}.out

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 6
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

# process molecules

mols="h2o ethene thiazole"

echo "[ Run... ] ${job}"

for mol in $mols; do

    echo "  $mol"
    echo -n "" > ${mol}.xyz

    echo "#RUN = TDA"                >> ${mol}.xyz
    echo "#CIS:NSTATES = 3"          >> ${mol}.xyz

    echo "#METHOD = RKS"             >> ${mol}.xyz
    echo "#XCFUN  = CAMB3LYP"        >> ${mol}.xyz
    echo "#BASIS  = basis/STO-3G.bs" >> ${mol}.xyz
    echo "#CHARGE = 0"               >> ${mol}.xyz
    echo "#MULTIPLICITY = 1"         >> ${mol}.xyz

    echo "#SCF:CS = 14"              >> ${mol}.xyz
    echo "#GRID:WTHRESH = 1.0e-14"   >> ${mol}.xyz

    tail -n +3 ../molecules/${mol}.xyz >> ${mol}.xyz

    export FIESTA_DIR=$(pwd)/../..
    $MPIRUN_CMD $FIESTA_DIR/bin/fiesta ${mol}.xyz host.hw >> ${job}.out

done

# process results

val_str=`grep 'CIS Excitation Energy' ${job}.out | awk '{print $5}'`

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    FuzzyEqual "$ref" "$val" "$tol"
    if [[ $? -eq 1 ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
