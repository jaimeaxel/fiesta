#!/bin/bash

# comparator

function FuzzyEqual() # ARG1 Ref Value, ARG2 Calced Val, AGR3 Tol
{
    BC_SCRIPT="scale = 10
               d = $1 - $2
               if (d<0.0) d = -d
               d < $3"
    return $(echo "$BC_SCRIPT" | bc)
}

# mpi command

MPIRUN_CMD="mpirun -np 2"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 2"
fi

# reference value

ref_arr=(
     0.169240
     0.169536
     0.189088
     0.198778
     0.199267
     0.205669
     0.212444
     0.213106

     0.000600
     0.000180
     0.000030
     0.004210
     0.001300
     0.000070
     0.010940
     0.019890

     0.010080
     0.008420
    -0.002630
     0.006520
     0.002290
     0.000060
    -0.013710
    -0.036480
)

ref_size=${#ref_arr[@]}

tol=0.0003

# input files and calculation

job=EXMOD_V_RKS

echo "[ Run... ] ${job}"

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 6
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

cat > ${job}.xyz << EOF
#RUN = EXMOD
#SCF:CS = 14
#GRID:WTHRESH = 1.0e-14
#CIS:NSTATES = 3
#CIS:CT_NOCC = 1
#CIS:CT_NVIR = 1
#FRAGINFO = frag.info
#BASIS = basis/STO-3G.bs
#CHARGE = 0
#MULTIPLICITY = 1
#METHOD = RKS
#XCFUN = PBE0
C        -6.660852       -0.537882        1.153909
N        -5.464736       -1.279376        0.755959
C        -5.401171       -2.560318        0.247397
N        -4.174046       -3.023620        0.133321
C        -3.381917       -1.986143        0.602941
C        -1.992881       -1.833262        0.772320
N        -1.082150       -2.773804        0.430539
N        -1.561016       -0.663937        1.284758
C        -2.433159        0.310896        1.615380
N        -3.755877        0.269284        1.515634
C        -4.169954       -0.898312        1.003955
H        -6.251667        0.367975        1.602217
H        -6.307739       -3.093503        0.000510
H        -0.105552       -2.647436        0.711625
H        -1.428402       -3.703654        0.256126
H        -1.977553        1.222109        1.990935
H        -7.232028       -0.272547        0.264277
H        -7.198697       -1.104105        1.914241
C        -4.314093        3.585540       -0.745962
N        -3.776236        2.345248       -1.222725
C        -4.429311        1.234574       -1.724072
N        -3.619950        0.263430       -2.082104
C        -2.355203        0.754373       -1.794323
C        -1.062681        0.209500       -1.941029
N        -0.829700       -1.011471       -2.484749
N        -0.016323        0.966928       -1.550945
C        -0.232699        2.193134       -1.027881
N        -1.398793        2.802091       -0.844023
C        -2.426789        2.040798       -1.247281
H        -3.476672        4.252611       -0.541847
H        -5.507670        1.210043       -1.788692
H         0.097505       -1.414764       -2.327103
H        -1.616525       -1.643972       -2.460000
H         0.667576        2.728356       -0.738854
H        -4.949051        4.025189       -1.515114
H        -4.870540        3.409636        0.174609
EOF

cat > frag.info << EOF
2
18 0 1
18 0 1
EOF

export FIESTA_DIR=$(pwd)/../..

$MPIRUN_CMD $FIESTA_DIR/bin/fiesta ${job}.xyz host.hw > ${job}.out

# process and print result

exc_ene=`grep 'E\[' ${job}.out | awk '{print $2}'`
osc_str=`grep 'E\[' ${job}.out | awk '{print $7}'`
rot_str=`grep 'E\[' ${job}.out | awk '{print $9}'`
val_str="$exc_ene $osc_str $rot_str"

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    FuzzyEqual "$ref" "$val" "$tol"
    if [[ $? -eq 1 ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
