#!/bin/bash

# comparator

function FuzzyEqual() # ARG1 Ref Value, ARG2 Calced Val, AGR3 Tol
{
    BC_SCRIPT="scale = 10
               d = $1 - $2
               if (d<0.0) d = -d
               d < $3"
    return $(echo "$BC_SCRIPT" | bc)
}

# mpi command

MPIRUN_CMD="mpirun -np 2"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 2"
fi

# reference value

ref_arr=(
     0.412053
     0.415777
     0.484909
     0.559711
     0.594074
     0.669985
     0.770172
     0.795719
     0.834173
     0.923494
     0.965303
     1.046449
)

ref_size=${#ref_arr[@]}

tol=0.0003

# input files and calculation

job=EXMOD_V_RHF

echo "[ Run... ] ${job}"

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 6
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

cat > ${job}.xyz << EOF
#RUN = EXMOD
#SCF:CS = 14
#CIS:NSTATES = 2
#CIS:CT_NOCC = 1
#CIS:CT_NVIR = 1
#FRAGINFO = frag.info
#BASIS = basis/STO-3G.bs
#CHARGE = 0
#MULTIPLICITY = 1
#METHOD = RHF
//NH3+CH4+C2H4
N         -1.96309        1.59755       -0.01963
H         -1.95876        2.61528        0.03109
H         -2.48929        1.27814        0.79244
H         -2.52930        1.35928       -0.83265
C         -0.85782       -2.68804        0.23504
H         -0.43365       -3.54435       -0.24632
H         -1.80549       -2.46285       -0.20781
H         -0.20141       -1.85123        0.11765
H         -0.99072       -2.89372        1.27664
H         -1.83266       -0.66217       -1.69931
C         -2.80597       -0.90302       -1.28194
H         -3.55431       -1.15099       -2.02901
C         -3.05692       -0.89514        0.01595
H         -2.30890       -0.64722        0.76335
H         -4.03033       -1.13600        0.43314
EOF

cat > frag.info << EOF
3
4 0 1
5 0 1
6 0 1
EOF

export FIESTA_DIR=$(pwd)/../..

$MPIRUN_CMD $FIESTA_DIR/bin/fiesta ${job}.xyz host.hw > ${job}.out

# process and print result

val_str=`grep 'E\[' ${job}.out | awk '{print $2}'`

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    FuzzyEqual "$ref" "$val" "$tol"
    if [[ $? -eq 1 ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
