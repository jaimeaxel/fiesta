#!/bin/bash

# comparator

function FuzzyEqual() # ARG1 Ref Value, ARG2 Calced Val, AGR3 Tol
{
    BC_SCRIPT="scale = 10
               d = $1 - $2
               if (d<0.0) d = -d
               d < $3"
    return $(echo "$BC_SCRIPT" | bc)
}

# mpi command

MPIRUN_CMD="mpirun -np 2"
if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    MPIRUN_CMD="aprun -n 2"
fi

# reference value

ref_arr=(
    -76.026219198
   -115.049155233
    -78.039666875
   -115.873625172
   -567.306415240
   -551.532069079
   -247.000354252
   -451.532713746
   -321.890556199
   -383.310851389
)

ref_size=${#ref_arr[@]}

tol=0.0000001

# input files and calculation

job=RHF
echo > ${job}.out

cat > host.hw << EOF
#GPU     = OFF
#THREADS = 6
#ERIMEM  = 16384
#BUFFMEM = 2048
EOF

# process molecules

mols="h2o methanol ethene propyne thiazole
      dmso dmf thymine alanine azulene"

echo "[ Run... ] ${job}"

for mol in $mols; do

    echo "  $mol"
    echo -n "" > ${mol}.xyz

    echo "#METHOD = RHF"             >> ${mol}.xyz
    echo "#BASIS = basis/cc-pVDZ.bs" >> ${mol}.xyz
    echo "#CHARGE = 0"               >> ${mol}.xyz
    echo "#MULTIPLICITY = 1"         >> ${mol}.xyz

    echo "#SCF:CS = 14"              >> ${mol}.xyz
    echo "#SCF:GUESS = SAD"          >> ${mol}.xyz

    tail -n +3 ../molecules/${mol}.xyz >> ${mol}.xyz

    export FIESTA_DIR=$(pwd)/../..
    $MPIRUN_CMD $FIESTA_DIR/bin/fiesta ${mol}.xyz host.hw >> ${job}.out

done

# process results

val_str=`grep 'Total energy' ${job}.out | awk '{print $4}'`

index=0
count=0
for val in $val_str
do
    ref=${ref_arr[$index]}
    ((index++))

    FuzzyEqual "$ref" "$val" "$tol"
    if [[ $? -eq 1 ]]; then
        ((count++))
    else
        echo "error: ref=$ref, calc=$val"
    fi
done

if [ $index -eq $ref_size ] && [ $count -eq $ref_size ]; then
    printf "[ Passed ] %s\n" "${job}"
    exit 0
else
    printf "[ Failed ] %s\n" "${job}"
    exit 1
fi
