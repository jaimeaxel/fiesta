#!/usr/bin/env bash

export FIESTA_DIR=/cfs/klemming/nobackup/l/lxin/fiesta-qmcmm
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta h2o_sto3g.xyz beskow.hw > h2o_sto3g.out
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta h2o_def2svp.xyz beskow.hw > h2o_def2svp.out
python check.py
