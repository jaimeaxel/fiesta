#!/usr/bin/env python

# read matrices from file
def read_matrices(file, nao, matrices, names):
    with open(file, 'r') as f_out:
        while True:
            line = f_out.readline()
            if not line:
                break
            for name in names:
                if ('===== ' + name[0]) in line:
                    symbol = name[1]
                    matrices[symbol] = [[]]*nao
                    for i in range(nao):
                        line = f_out.readline()
                        content = line.split()
                        assert len(content) == nao
                        for j in range(len(content)):
                            matrices[symbol][i].append(float(content[j]))
                        line = f_out.readline()

# compare two matrices
def compare_matrix(mat_A, mat_B, nao, order_BA, is_mo_coeff):
    maxd = 0.0
    assert len(order_BA) == nao
    for i in range(nao):
        for j in range(nao):
            if is_mo_coeff:
                diff = abs(abs(mat_B[j][order_BA[i]]) - abs(mat_A[i][j]))
            else:
                diff = abs(mat_B[order_BA[i]][order_BA[j]] - mat_A[i][j])
            if maxd < diff:
                maxd = diff
    return maxd

# print matrix
def print_mat(name, mat, nao):
    print name
    for i in range(nao):
        for j in range(nao):
            print '%20.15f' % mat[i][j],
        print
    print

# print reordered matrix
def print_mat2(name, mat, nao, order, is_mo_coeff):
    print name
    for i in range(nao):
        for j in range(nao):
            if is_mo_coeff:
                print '%20.15f' % mat[j][order[i]],
            else:
                print '%20.15f' % mat[order[i]][order[j]],
        print
    print

# test
def run_test():

    tol = 1.0e-09

    names = [
        ['Overlap',           'S'],
        ['Kinetic energy',    'T'],
        ['Nuclear potential', 'V'],
        #['Density',          'D'],
        ['Coulomb',           'J'],
        ['Exchange',          'K'],
        #['Fock',             'F'],
        ]

    for basis in ['sto3g', 'def2svp']:

        if basis == 'sto3g':
            #        S,S,P-1,P0,P1,S,S
            order_12 = [0,1,  3, 4, 2,5,6]
            order_21 = [0]*len(order_12)
            for ind,val in enumerate(order_12):
                order_21[val] = ind
            nao = 7
            print '=== STO-3G ==='

        elif basis == 'def2svp':
            #        S, S, S,P-1,P0,P1,P-1,P0,P1,D-2, D-1, D0, D1, D2,  S,  S, P-1, P0, P1,  S,  S, P-1, P0, P1 
            order_12 = [0, 1, 2,  4, 5, 3,  7, 8, 6, 11,  12, 10, 13,  9, 14, 15,  17, 18, 16, 19, 20,  22, 23, 21]
            order_21 = [0]*len(order_12)
            for ind,val in enumerate(order_12):
                order_21[val] = ind
            nao = 24
            print '=== def2-SVP ==='

        calc = {}
        ref  = {}

        read_matrices('h2o_' + basis + '.out', nao, calc, names)
        read_matrices('h2o_' + basis + '.ref', nao, ref,  names)

        for name in names:

            symbol = name[1]
            is_mo_coeff = (symbol == 'C')
            maxd = compare_matrix(calc[symbol], ref[symbol], nao, order_21, is_mo_coeff)

            if maxd <= tol:
                print '%5s%25.15e' % (symbol, maxd)
            else:
                print '%5s%25.15e%5s' % (symbol, maxd, '<<<\n')
                print_mat ('calc_'+symbol, calc[symbol], nao)
                print_mat2('ref_'+symbol,  ref[symbol],  nao, order_21, is_mo_coeff)
                return False

    return True

# main function
if __name__ == '__main__':
    passed = run_test()
    if passed:
        print '=== Passed! ==='
    else:
        print '>>> Failed! <<<\n'
