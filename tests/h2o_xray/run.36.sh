#!/bin/bash

#SBATCH --time=4:00:00
#SBATCH --nodes=4
#SBATCH --account=pdc.staff
#SBATCH --ntasks-per-node=1
#SBATCH --mem=128000

INPUT=h2o_xray.xyz

# load the necessary modules (for DL libraries)
module unload PrgEnv-cray
module unload cray-libsci
module load PrgEnv-gnu/5.2.82
module swap gcc gcc/7.2.0
module load craype-hugepages8M

# install directory
export FIESTA_DIR=/cfs/klemming/nobackup/j/jars2/fiesta.beskow

# executable
FIESTA=$FIESTA_DIR/fiesta
 
# hardware configurations
ARCH=$FIESTA_DIR/arch/beskow.36.hw

# set number of threads per node & launcher
export OMP_MAX_THREADS=72
export OMP_NUM_THREADS=72
export MKL_NUM_THREADS=36

# print debug info
echo "SLURM_JOBID="$SLURM_JOBID
echo "SLURM_JOB_NODELIST"=$SLURM_JOB_NODELIST
echo "SLURM_NNODES"=$SLURM_NNODES

# launch
aprun -n $SLURM_NNODES -N 1 -d 36 -j 2 $FIESTA $INPUT $ARCH

