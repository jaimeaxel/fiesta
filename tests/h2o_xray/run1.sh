#!/bin/bash

#SBATCH --time=4:00:00
#SBATCH --nodes=1
#SBATCH --account=pdc.staff
#SBATCH --ntasks-per-node=1

INPUT=h2o_xray.xyz

# load the necessary modules (for DL libraries)
module unload PrgEnv-cray
module load PrgEnv-gnu/5.2.82
module unload cray-libsci
module swap gcc gcc/5.1.0

# install directory
export FIESTA_DIR=/cfs/klemming/nobackup/j/jars2/fiesta.beskow

# executable
FIESTA=$FIESTA_DIR/fiesta
 
# hardware configurations
ARCH=$FIESTA_DIR/arch/beskow.hw

# set number of threads per node & launcher
export OMP_MAX_THREADS=64
export OMP_NUM_THREADS=64
export MKL_NUM_THREADS=32

# print debug info
echo "SLURM_JOBID="$SLURM_JOBID
echo "SLURM_JOB_NODELIST"=$SLURM_JOB_NODELIST
echo "SLURM_NNODES"=$SLURM_NNODES

# launch
aprun -n $SLURM_NNODES -N 1 -d 32 -j 2 $FIESTA $INPUT $ARCH

