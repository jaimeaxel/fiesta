#!/usr/bin/env python

# read energy from file
def read_energy(file):
    with open(file, 'r') as f_out:
        while True:
            line = f_out.readline()
            if not line:
                break
            if 'Total energy' in line:
                return float(line.split()[-1])
    return None

# test
def run_test():

    tol = 1.0e-07

    energies = {
        'rhf'   :  -74.94207995787853,
        'rohf1' :  -74.65925178044536,
        'rohf2' :  -74.48426012403949,
        'rohf3' :  -74.68625521599024,
        'uhf1'  :  -74.66178437669134,
        'uhf2'  :  -74.48785020668309,
        'uhf3'  :  -74.68932022449465,
    }

    print '=== Energies ==='

    for name in sorted(energies.keys()):

        ref  = energies[name]
        calc = read_energy(name + '.out')

        diff = abs(calc - ref)

        if diff <= tol:
            print '%5s%25.15e' % (name.upper(), diff)
        else:
            print '%5s%25.15e%5s' % (name.upper(), diff, '<<<\n')
            print 'calc_energy', calc
            print 'ref_energy ', ref
            return False

    return True

# main function
if __name__ == '__main__':
    passed = run_test()
    if passed:
        print '=== Passed! ==='
    else:
        print '>>> Failed! <<<\n'
