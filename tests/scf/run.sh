#!/usr/bin/env bash

export FIESTA_DIR=/cfs/klemming/nobackup/l/lxin/fiesta-qmcmm

aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta rhf.xyz   beskow.hw > rhf.out
                                                        
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta uhf1.xyz  beskow.hw > uhf1.out
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta uhf2.xyz  beskow.hw > uhf2.out
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta uhf3.xyz  beskow.hw > uhf3.out

aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta rohf1.xyz beskow.hw > rohf1.out
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta rohf2.xyz beskow.hw > rohf2.out
aprun -n 1 -N 1 -d 32 -j 2 $FIESTA_DIR/fiesta rohf3.xyz beskow.hw > rohf3.out

python check.py
