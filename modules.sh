#!/bin/bash

FC=gfortran
CC=gcc
CXX=g++

if [ "$HOSTNAME" == "beskow-login2.pdc.kth.se" ]; then
    FC=ftn
    CC=cc
    CXX=CC
fi

git submodule update --init --recursive

test -d lib || mkdir -p lib

cd external/xcfun
rm -rf build
./setup --fc=${FC} --cc=${CC} --cpp=${CXX} -D ENABLE_STATIC_LINKING=1
cd build
make -j 4
cd ../../..
cp external/xcfun/build/libxcfun.a lib/.

cd external/googletest
export GTEST_DIR=$(pwd)/googletest
${CXX} -isystem ${GTEST_DIR}/include -I${GTEST_DIR} -pthread -c ${GTEST_DIR}/src/gtest-all.cc
ar -rv libgtest.a gtest-all.o
rm -f gtest-all.o
cd ../..
mv external/googletest/libgtest.a lib/.
