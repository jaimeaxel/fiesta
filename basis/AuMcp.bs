!
BASIS="1-electron Au Mcp basis - simply a modified copy of AuPP so far to test the code, no physics in "
SEGMENTED
AU   78
S  1  1
     0.10488556      1.00000
P  1  1
     0.20977112      1.00000
D  1  1
     0.41954224      1.00000

****
AU   0
AU-MCP  1 
Exp
  1
0    0.25000000      1.00000   
ExpR
  1
0    0.25000000      1.00000   
CoreOrbs
  1 
3  1 1 
0    0.41954224      1.00000
c     0.1 0.2 0.3 0.4 0.5
****

