#! /bin/bash

# script to manually build the external submodules
# and copy the necessary libraries FIESTA needs to be linked against

# set modules on Tegner

if [ $HOSTNAME = "tegner-login-1.pdc.kth.se" ] || [ $HOSTNAME = "tegner-login-2.pdc.kth.se" ]; then
  module add gcc/6.2.0
  module add i-compilers/17.0.1
  module add intelmpi/5.1.3
  module add cuda/9.1  
  export CC=gcc
  export CXX=g++
  export FC=gfortran
elif [ $HOSTNAME = "beskow-login1.pdc.kth.se" ] || [ $HOSTNAME = "beskow-login2.pdc.kth.se" ]; then
  module unload PrgEnv-cray
  module load PrgEnv-gnu/5.2.82
  module unload cray-libsci
  module swap gcc gcc/7.2.0
  #module load allinea-forge/7.0
  export CC=cc
  export CXX=CC
  export FC=ftn
else
  export CC=gcc
  export CXX=g++
  export FC=gfortran
fi

export OMP_MAX_THREADS=32
export OMP_NUM_THREADS=32

make clean
rm -rf lib
mkdir lib

git submodule update --init --recursive

cd external/xcfun
rm -rf build
./setup --fc=${FC} --cc=${CC} --cpp=${CXX} -D ENABLE_STATIC_LINKING=1
cd build
make -j 8
cd ../../..
cp external/xcfun/build/libxcfun.a lib/.

cd external/googletest
export GTEST_DIR=$(pwd)/googletest
CC -isystem ${GTEST_DIR}/include -I${GTEST_DIR} -pthread -c ${GTEST_DIR}/src/gtest-all.cc
ar -rv libgtest.a gtest-all.o
rm -f gtest-all.o
cd ../..
mv external/googletest/libgtest.a lib/.

make -j 8

